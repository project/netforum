<?php

/**
 * @file
 * netForum xWeb API Loader and Factory
 *
 * This file contains the logic for setting up lazy loading of proxy objects and
 * factories for creating instances of the Drupal netForum xWeb API.
 *
 */

use Drupal\netForum\xWeb\DrupalNetForumApiConfig;
use Drupal\netForum\xWeb\Exceptions\netForumxWebException;
use Drupal\netForum\xWeb\netForumxWebApiConfig;
use Drupal\netForum\xWeb\Xml\Enterprise\NetForumOutlook;
use Drupal\netForum\xWeb\Xml\Enterprise\NetForumXML;
use Drupal\netForum\xWeb\Xml\Pro\NetForumXMLOnDemand;
use Drupal\netForum\xWeb\Xml\Pro\SignOn;

// Simple class autoloader that depends on namespaces and directories that
// classes are in being the same.
function netForumxWebApi_AutoLoader($classPath) {
  if (preg_match('/^Drupal\x5cnetForum\x5c.*/', $classPath) === 1) {
    require_once __DIR__ . '/' . str_replace('\\', '/', $classPath) . '.php';
  }
}

spl_autoload_register('netForumxWebApi_AutoLoader');

// Lazy load netForum xWeb Proxy classes
require_once 'Drupal/netForum/xWeb/Xml/Enterprise/Generated/Secure/autoload.php';
require_once 'Drupal/netForum/xWeb/Xml/Enterprise/Generated/Outlook/autoload.php';
require_once 'Drupal/netForum/xWeb/Xml/Pro/Generated/OnDemand/autoload.php';
require_once 'Drupal/netForum/xWeb/Xml/Pro/Generated/SignOn/autoload.php';

/**
 * Class netForumApiFactory
 *
 * Factory for getting an instance of an xWeb API and taking care of
 * configuration. Based on configuration settings, this factory will
 * automatically create an instance of the API for netFORUM Enterprise or Pro.
 */
class netForumApiFactory {

  private static $netForumXMLInstance = NULL;

  private static $netForumOutlookInstance = NULL;

  private static $netForumProXMLInstance = NULL;

  private static $netForumProSignOnInstance = NULL;

  private function __construct() {

  }

  /**
   * Creates a new instance of NetForumXML for netFORUM Enterprise. If no
   * configuration is passed, the settings specified in the module
   * configuration
   * are used.
   *
   * @param \Drupal\netForum\xWeb\netForumxWebApiConfig|NULL $drupalNetForumApiConfig
   *
   * @return \Drupal\netForum\xWeb\Xml\Enterprise\NetForumXML
   * @throws \Drupal\netForum\xWeb\Exceptions\netForumxWebException
   */
  public static function createNetForumXMLInstance(netForumxWebApiConfig $drupalNetForumApiConfig = NULL) {
    if ($drupalNetForumApiConfig == NULL) {
      $drupalNetForumApiConfig = new DrupalNetForumApiConfig();
    }

    if (!($drupalNetForumApiConfig instanceof netForumxWebApiConfig)) {
      throw new netForumxWebException('Drupal netFORUM xWeb API settings is not an instance of netForumxWebApiConfig.');
    }

    if (!netForumApiFactory::isConfiguredNetForumEnterprise()) {
      throw new netForumxWebException('Drupal netFORUM xWeb API is configured for netFORUM Pro. Attempting to use Enterprise.');
    }

    return new NetForumXML($drupalNetForumApiConfig);
  }

  /**
   * Returns an existing instance of NetForumXML for netFORUM Enterprise or
   * creates a new one if one does not exist.
   *
   * @return \Drupal\netForum\xWeb\Xml\Enterprise\NetForumXML
   */
  public static function getNetForumXMLInstance() {
    if (netForumApiFactory::$netForumXMLInstance == NULL) {
      netForumApiFactory::$netForumXMLInstance = netForumApiFactory::createNetForumXMLInstance();
    }

    return netForumApiFactory::$netForumXMLInstance;
  }

  /**
   * Creates a new instance of NetForumOutlook for netFORUM Enterprise. If no
   * configuration is passed then an exception will be thrown. This API uses
   * the LDAP provider configured in netFORUM for authentication. To use this
   * API, you will need to specify a LDAP Username and Password.
   *
   * @param \Drupal\netForum\xWeb\netForumxWebApiConfig|NULL $drupalNetForumApiConfig
   *
   * @return \Drupal\netForum\xWeb\Xml\Enterprise\NetForumOutlook
   * @throws \Drupal\netForum\xWeb\Exceptions\netForumxWebException
   */
  public static function createNetForumOutlookInstance(netForumxWebApiConfig $drupalNetForumApiConfig = NULL) {
    if (!($drupalNetForumApiConfig instanceof netForumxWebApiConfig)) {
      throw new netForumxWebException('Drupal netFORUM xWeb Outlook API settings is not an instance of netForumxWebApiConfig.');
    }

    if (!netForumApiFactory::isConfiguredNetForumEnterprise()) {
      throw new netForumxWebException('Drupal netFORUM xWeb API is configured for netFORUM Pro. Attempting to use Enterprise.');
    }

    return new NetForumOutlook($drupalNetForumApiConfig);
  }

  /**
   * Returns an existing instance of NetForumOutlook for netFORUM Enterprise or
   * creates a new one if one does not exist.
   *
   * @return \Drupal\netForum\xWeb\Xml\Enterprise\NetForumOutlook
   */
  public static function getNetForumOutlookInstance() {
    if (netForumApiFactory::$netForumOutlookInstance == NULL) {
      netForumApiFactory::$netForumOutlookInstance = netForumApiFactory::createNetForumOutlookInstance();
    }

    return netForumApiFactory::$netForumOutlookInstance;
  }

  /**
   * Creates a new instance of NetForumXMLOnDemand for netFORUM Pro. If no
   * configuration is passed, the settings specified in the module
   * configuration
   * are used.
   *
   * @param \Drupal\netForum\xWeb\netForumxWebApiConfig|NULL $drupalNetForumApiConfig
   *
   * @return \Drupal\netForum\xWeb\Xml\Pro\NetForumXMLOnDemand
   * @throws \Drupal\netForum\xWeb\Exceptions\netForumxWebException
   */
  public static function createNetForumProXMLInstance(netForumxWebApiConfig $drupalNetForumApiConfig = NULL) {
    if ($drupalNetForumApiConfig == NULL) {
      $drupalNetForumApiConfig = new DrupalNetForumApiConfig();
    }

    if (!($drupalNetForumApiConfig instanceof netForumxWebApiConfig)) {
      throw new netForumxWebException('Drupal netFORUM xWeb Pro API settings is not an instance of netForumxWebApiConfig.');
    }

    if (!netForumApiFactory::isConfiguredNetForumPro()) {
      throw new netForumxWebException('Drupal netFORUM xWeb API is configured for netFORUM Enterprise. Attempting to use Pro.');
    }

    return new NetForumXMLOnDemand($drupalNetForumApiConfig);
  }

  /**
   * Returns an existing instance of NetForumXMLOnDemand for netFORUM Pro or
   * creates a new one if one does not exist.
   *
   * @return \Drupal\netForum\xWeb\Xml\Pro\NetForumXMLOnDemand|null
   */
  public static function getNetForumXMLProInstance() {
    if (netForumApiFactory::$netForumProXMLInstance == NULL) {
      netForumApiFactory::$netForumProXMLInstance = netForumApiFactory::createNetForumProXMLInstance();
    }

    return netForumApiFactory::$netForumProXMLInstance;
  }

  /**
   * Creates a new instance of SignOn for netFORUM Pro. If no configuration is
   * passed, the settings specified in the module configuration are used.
   *
   * @param \Drupal\netForum\xWeb\netForumxWebApiConfig|NULL $drupalNetForumApiConfig
   *
   * @return \Drupal\netForum\xWeb\Xml\Pro\SignOn
   * @throws \Drupal\netForum\xWeb\Exceptions\netForumxWebException
   */
  public static function createNetForumProSignOnInstance(netForumxWebApiConfig $drupalNetForumApiConfig = NULL) {
    if ($drupalNetForumApiConfig == NULL) {
      $drupalNetForumApiConfig = new DrupalNetForumApiConfig();
    }

    if (!($drupalNetForumApiConfig instanceof netForumxWebApiConfig)) {
      throw new netForumxWebException('Drupal netFORUM xWeb Pro SignOn API settings is not an instance of netForumxWebApiConfig.');
    }

    throw new netForumxWebException('Drupal netFORUM xWeb API is configured for netFORUM Enterprise. Attempting to use Pro.');

    return new SignOn($drupalNetForumApiConfig);
  }

  /**
   * Returns an existing instance of SignOn for netFORUM Pro or creates a new
   * one if one does not exist.
   *
   * @return \Drupal\netForum\xWeb\Xml\Pro\SignOn|null
   */
  public static function getNetForumProSignOnInstance() {
    if (netForumApiFactory::$netForumProSignOnInstance == NULL) {
      netForumApiFactory::$netForumProSignOnInstance = netForumApiFactory::createNetForumProSignOnInstance();
    }

    return netForumApiFactory::$netForumProSignOnInstance;
  }

  /**
   * Check if the configured instance of netFORUM is Enterprise. If netFORUM
   * has not been configured, NULL is returned.
   *
   * @return bool|null
   */
  public static function isConfiguredNetForumEnterprise() {
    $netforumXwebOd = variable_get('netforum_xweb_od', NULL);

    if ($netforumXwebOd === NULL) {
      return NULL;
    }

    return $netforumXwebOd == 0;
  }

  /**
   * Check if the configured instance of netFORUM is Pro. If netFORUM has not
   * been configured, NULL is returned.
   *
   * @return bool|null
   */
  public static function isConfiguredNetForumPro() {
    $netforumXwebOd = variable_get('netforum_xweb_od', NULL);

    if ($netforumXwebOd === NULL) {
      return NULL;
    }

    return $netforumXwebOd == 1;
  }
}
