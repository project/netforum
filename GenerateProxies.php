<?php

namespace Drupal\netForum\xWeb;

use Drupal\netForum\xWeb\Xml\netForumSoapClient;
use Wsdl2PhpGenerator\Config;
use Wsdl2PhpGenerator\Generator;

require_once 'Drupal/netForum/xWeb/Xml/netForumSoapClient.php';
require_once 'vendor/autoload.php';

/**
 * Class GenerateProxies
 *
 * This script will generate PHP proxy classes for the netFORUM xWeb XML Web
 * Service using Wsdl2PhpGenerator.
 *
 * WARNING
 *
 * You will need to download Wsdl2PhpGenerator using Composer before you can
 * use this script.
 *
 * Please not that after generating proxies, you will need to edit the
 * generated file and add a namespace use statement containing
 * (Drupal\netForum\xWeb\Xml\netForumSoapClient). Unfortunately, this is
 * currently required, otherwise PHP will not be able to find the
 * netForumSoapClient class.
 *
 * WARNING
 *
 */
class GenerateProxies {

  private static $GENERATED_ENTERPRISE_DIRECTORY = 'Drupal/netForum/xWeb/Xml/Enterprise/Generated';

  private static $GENERATED_ENTERPRISE_NAMESPACE = 'Drupal\netForum\xWeb\Xml\Enterprise\Generated';

  private static $GENERATED_PRO_DIRECTORY = 'Drupal/netForum/xWeb/Xml/Pro/Generated';

  private static $GENERATED_PRO_NAMESPACE = 'Drupal\netForum\xWeb\Xml\Pro\Generated';

  public function __construct() {
    $cliOptions = getopt('', array('url:'));
    if (array_key_exists('url', $cliOptions)) {
      $wsdlUrl = $cliOptions['url'];

      $generatorConfigArgs = array(
        'inputFile' => $wsdlUrl,
        'soapClientClass' => 'Drupal\netForum\xWeb\Xml\netForumSoapClient',
      );

      if (strripos($wsdlUrl, '/xWeb/Secure/netForumXML.asmx?WSDL') !== FALSE) {
        $generatorConfigArgs['outputDir'] = GenerateProxies::$GENERATED_ENTERPRISE_DIRECTORY . '/Secure';
        $generatorConfigArgs['namespaceName'] = GenerateProxies::$GENERATED_ENTERPRISE_NAMESPACE . '\Secure';
      }
      elseif (strripos($wsdlUrl, '/xWeb/Outlook/Outlook.asmx?WSDL') !== FALSE) {
        $generatorConfigArgs['outputDir'] = GenerateProxies::$GENERATED_ENTERPRISE_DIRECTORY . '/Outlook';
        $generatorConfigArgs['namespaceName'] = GenerateProxies::$GENERATED_ENTERPRISE_NAMESPACE . '\Outlook';
      }
      elseif (strripos($wsdlUrl, '/xWeb/netForumXMLOnDemand.asmx') !== FALSE) {
        $generatorConfigArgs['outputDir'] = GenerateProxies::$GENERATED_PRO_DIRECTORY . '/OnDemand';
        $generatorConfigArgs['namespaceName'] = GenerateProxies::$GENERATED_PRO_NAMESPACE . '\OnDemand';
      }
      elseif (strripos($wsdlUrl, '/xWeb/Signon.asmx?WSDL') !== FALSE) {
        $generatorConfigArgs['outputDir'] = GenerateProxies::$GENERATED_PRO_DIRECTORY . '/SignOn';
        $generatorConfigArgs['namespaceName'] = GenerateProxies::$GENERATED_PRO_NAMESPACE . '\SignOn';
      }
      else {
        printf("The url specified does not appear to be a valid netFORUM xWeb WSDL URL.");
      }

      $generatorConfig = new Config($generatorConfigArgs);

      $generator = new Generator();
      $generator->generate($generatorConfig);
    }
    else {
      GenerateProxies::displayUsage();
    }
  }

  public static function displayUsage() {
    printf("GenerateProxies.php\n\n");
    printf("This script will fill the generated directory with PHP proxy classes\n");
    printf("for the netFORUM (Enterprise | Pro) xWeb Web Services.\n\n");
    printf("Usage:\n");
    printf("php GenerateProxies.php --url WSDL_URL\n\n");
  }
}

new GenerateProxies();
