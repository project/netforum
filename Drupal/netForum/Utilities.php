<?php

namespace Drupal\netForum;

/**
 * Class Utilities
 *
 * @package Drupal\netForum
 */
class Utilities {

  private function __construct() {

  }

  /**
   * @param $wsdlUrl URL to netFORUM xWeb WSDl.
   *
   * @return bool
   */
  public static function isNetForumEnterprise($wsdlUrl) {
    if (strripos($wsdlUrl, '/xWeb/Secure/netForumXML.asmx?WSDL') !== FALSE) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * @param $wsdlUrl URL to netFORUM xWeb WSDl.
   *
   * @return bool
   */
  public static function isNetForumPro($wsdlUrl) {
    if (strripos($wsdlUrl, '/xWeb/netForumXMLOnDemand.asmx?WSDL') !== FALSE) {
      return TRUE;
    }

    if (strripos($wsdlUrl, '/xWeb/Signon.asmx?WSDL') !== FALSE) {
      return TRUE;
    }

    return FALSE;
  }
}
