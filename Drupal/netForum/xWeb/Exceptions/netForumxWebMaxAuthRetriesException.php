<?php

namespace Drupal\netForum\xWeb\Exceptions;

/**
 * Class netForumxWebMaxAuthRetriesException
 *
 * @package Drupal\netForum\xWeb\Exceptions
 */
class netForumxWebMaxAuthRetriesException extends netForumxWebException {

  public function __construct($message = "", $code = 0, \Exception $previous = NULL) {
    parent::__construct($message, $code, $previous);
  }
}
