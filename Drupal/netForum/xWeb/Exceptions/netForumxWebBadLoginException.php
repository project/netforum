<?php

namespace Drupal\netForum\xWeb\Exceptions;

/**
 * Class netForumxWebBadLoginException
 *
 * @package Drupal\netForum\xWeb\Exceptions
 */
class netForumxWebBadLoginException extends netForumxWebException {

  public function __construct($message = "", $code = 0, \Exception $previous = NULL) {
    parent::__construct($message, $code, $previous);
  }
}
