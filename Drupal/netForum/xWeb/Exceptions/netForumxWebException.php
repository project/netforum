<?php

namespace Drupal\netForum\xWeb\Exceptions;

use Drupal\netForum\xWeb\Logging\LogHandler;

/**
 * Class netForumxWebException
 *
 * @package Drupal\netForum\xWeb\Exceptions
 */
class netForumxWebException extends \Exception {

  public function __construct($message = "", $code = 0, \Exception $previous = NULL) {
    parent::__construct($message, $code, $previous);

    LogHandler::LogException($this);
  }
}
