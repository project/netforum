<?php

namespace Drupal\netForum\xWeb\Xml\Pro;

use Drupal\netForum\xWeb\netForumxWebApiConfig;
use Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\Authenticate;
use Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\NetForumXML_x0020_OnDemand as GeneratedNetForumXMLOnDemand;

class NetForumXMLOnDemand extends GeneratedNetForumXMLOnDemand {

  private static $DoNotUseHeadersFor = array(
    'Authenticate',
  );

  /**
   * Avectra XML Namespace for netFORUM Pro OnDemand.
   */
  const AVECTRA_NS = 'http://www.avectra.com/OnDemand/2005/';

  public function __construct(netForumxWebApiConfig $netForumxWebApiConfig) {
    $this->netForumxWebApiConfig = $netForumxWebApiConfig;

    $this->SetXMLNamespace(self::AVECTRA_NS);

    $this->setAuthenticationHandler(function () {
      $this->authenticationHandler();
    });

    parent::__construct(array(), NULL);

    $this->authenticationHandler();
  }

  protected function authenticationHandler() {
    $this->Authenticate(new Authenticate($this->netForumxWebApiConfig->getUsername(),
      $this->netForumxWebApiConfig->getPassword()));
  }
}
