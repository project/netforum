<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetAwardEntriesByCustomerResponse
{

    /**
     * @var GetAwardEntriesByCustomerResult $GetAwardEntriesByCustomerResult
     */
    protected $GetAwardEntriesByCustomerResult = null;

    /**
     * @param GetAwardEntriesByCustomerResult $GetAwardEntriesByCustomerResult
     */
    public function __construct($GetAwardEntriesByCustomerResult)
    {
      $this->GetAwardEntriesByCustomerResult = $GetAwardEntriesByCustomerResult;
    }

    /**
     * @return GetAwardEntriesByCustomerResult
     */
    public function getGetAwardEntriesByCustomerResult()
    {
      return $this->GetAwardEntriesByCustomerResult;
    }

    /**
     * @param GetAwardEntriesByCustomerResult $GetAwardEntriesByCustomerResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetAwardEntriesByCustomerResponse
     */
    public function setGetAwardEntriesByCustomerResult($GetAwardEntriesByCustomerResult)
    {
      $this->GetAwardEntriesByCustomerResult = $GetAwardEntriesByCustomerResult;
      return $this;
    }

}
