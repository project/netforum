<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetAwardJudges
{

    /**
     * @var string $szAwardNumber
     */
    protected $szAwardNumber = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $szAwardNumber
     * @param string $szRecordDate
     */
    public function __construct($szAwardNumber, $szRecordDate)
    {
      $this->szAwardNumber = $szAwardNumber;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getSzAwardNumber()
    {
      return $this->szAwardNumber;
    }

    /**
     * @param string $szAwardNumber
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetAwardJudges
     */
    public function setSzAwardNumber($szAwardNumber)
    {
      $this->szAwardNumber = $szAwardNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetAwardJudges
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
