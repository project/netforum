<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCertificationProgramsResponse
{

    /**
     * @var GetCertificationProgramsResult $GetCertificationProgramsResult
     */
    protected $GetCertificationProgramsResult = null;

    /**
     * @param GetCertificationProgramsResult $GetCertificationProgramsResult
     */
    public function __construct($GetCertificationProgramsResult)
    {
      $this->GetCertificationProgramsResult = $GetCertificationProgramsResult;
    }

    /**
     * @return GetCertificationProgramsResult
     */
    public function getGetCertificationProgramsResult()
    {
      return $this->GetCertificationProgramsResult;
    }

    /**
     * @param GetCertificationProgramsResult $GetCertificationProgramsResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCertificationProgramsResponse
     */
    public function setGetCertificationProgramsResult($GetCertificationProgramsResult)
    {
      $this->GetCertificationProgramsResult = $GetCertificationProgramsResult;
      return $this;
    }

}
