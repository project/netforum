<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByRadiusLatLonResponse
{

    /**
     * @var GetCustomerByRadiusLatLonResult $GetCustomerByRadiusLatLonResult
     */
    protected $GetCustomerByRadiusLatLonResult = null;

    /**
     * @param GetCustomerByRadiusLatLonResult $GetCustomerByRadiusLatLonResult
     */
    public function __construct($GetCustomerByRadiusLatLonResult)
    {
      $this->GetCustomerByRadiusLatLonResult = $GetCustomerByRadiusLatLonResult;
    }

    /**
     * @return GetCustomerByRadiusLatLonResult
     */
    public function getGetCustomerByRadiusLatLonResult()
    {
      return $this->GetCustomerByRadiusLatLonResult;
    }

    /**
     * @param GetCustomerByRadiusLatLonResult $GetCustomerByRadiusLatLonResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRadiusLatLonResponse
     */
    public function setGetCustomerByRadiusLatLonResult($GetCustomerByRadiusLatLonResult)
    {
      $this->GetCustomerByRadiusLatLonResult = $GetCustomerByRadiusLatLonResult;
      return $this;
    }

}
