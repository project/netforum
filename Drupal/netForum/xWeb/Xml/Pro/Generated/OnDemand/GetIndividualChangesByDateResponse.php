<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetIndividualChangesByDateResponse
{

    /**
     * @var GetIndividualChangesByDateResult $GetIndividualChangesByDateResult
     */
    protected $GetIndividualChangesByDateResult = null;

    /**
     * @param GetIndividualChangesByDateResult $GetIndividualChangesByDateResult
     */
    public function __construct($GetIndividualChangesByDateResult)
    {
      $this->GetIndividualChangesByDateResult = $GetIndividualChangesByDateResult;
    }

    /**
     * @return GetIndividualChangesByDateResult
     */
    public function getGetIndividualChangesByDateResult()
    {
      return $this->GetIndividualChangesByDateResult;
    }

    /**
     * @param GetIndividualChangesByDateResult $GetIndividualChangesByDateResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetIndividualChangesByDateResponse
     */
    public function setGetIndividualChangesByDateResult($GetIndividualChangesByDateResult)
    {
      $this->GetIndividualChangesByDateResult = $GetIndividualChangesByDateResult;
      return $this;
    }

}
