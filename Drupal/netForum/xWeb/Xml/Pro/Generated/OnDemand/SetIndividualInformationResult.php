<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class SetIndividualInformationResult
{

    /**
     * @var string $any
     */
    protected $any = null;

    /**
     * @param string $any
     */
    public function __construct($any)
    {
      $this->any = $any;
    }

    /**
     * @return string
     */
    public function getAny()
    {
      return $this->any;
    }

    /**
     * @param string $any
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetIndividualInformationResult
     */
    public function setAny($any)
    {
      $this->any = $any;
      return $this;
    }

}
