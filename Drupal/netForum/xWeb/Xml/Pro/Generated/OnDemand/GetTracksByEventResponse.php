<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetTracksByEventResponse
{

    /**
     * @var GetTracksByEventResult $GetTracksByEventResult
     */
    protected $GetTracksByEventResult = null;

    /**
     * @param GetTracksByEventResult $GetTracksByEventResult
     */
    public function __construct($GetTracksByEventResult)
    {
      $this->GetTracksByEventResult = $GetTracksByEventResult;
    }

    /**
     * @return GetTracksByEventResult
     */
    public function getGetTracksByEventResult()
    {
      return $this->GetTracksByEventResult;
    }

    /**
     * @param GetTracksByEventResult $GetTracksByEventResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetTracksByEventResponse
     */
    public function setGetTracksByEventResult($GetTracksByEventResult)
    {
      $this->GetTracksByEventResult = $GetTracksByEventResult;
      return $this;
    }

}
