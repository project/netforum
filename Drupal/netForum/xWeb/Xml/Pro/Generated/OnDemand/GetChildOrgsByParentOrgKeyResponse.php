<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetChildOrgsByParentOrgKeyResponse
{

    /**
     * @var GetChildOrgsByParentOrgKeyResult $GetChildOrgsByParentOrgKeyResult
     */
    protected $GetChildOrgsByParentOrgKeyResult = null;

    /**
     * @param GetChildOrgsByParentOrgKeyResult $GetChildOrgsByParentOrgKeyResult
     */
    public function __construct($GetChildOrgsByParentOrgKeyResult)
    {
      $this->GetChildOrgsByParentOrgKeyResult = $GetChildOrgsByParentOrgKeyResult;
    }

    /**
     * @return GetChildOrgsByParentOrgKeyResult
     */
    public function getGetChildOrgsByParentOrgKeyResult()
    {
      return $this->GetChildOrgsByParentOrgKeyResult;
    }

    /**
     * @param GetChildOrgsByParentOrgKeyResult $GetChildOrgsByParentOrgKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetChildOrgsByParentOrgKeyResponse
     */
    public function setGetChildOrgsByParentOrgKeyResult($GetChildOrgsByParentOrgKeyResult)
    {
      $this->GetChildOrgsByParentOrgKeyResult = $GetChildOrgsByParentOrgKeyResult;
      return $this;
    }

}
