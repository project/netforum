<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetPropertyByOwnerResponse
{

    /**
     * @var GetPropertyByOwnerResult $GetPropertyByOwnerResult
     */
    protected $GetPropertyByOwnerResult = null;

    /**
     * @param GetPropertyByOwnerResult $GetPropertyByOwnerResult
     */
    public function __construct($GetPropertyByOwnerResult)
    {
      $this->GetPropertyByOwnerResult = $GetPropertyByOwnerResult;
    }

    /**
     * @return GetPropertyByOwnerResult
     */
    public function getGetPropertyByOwnerResult()
    {
      return $this->GetPropertyByOwnerResult;
    }

    /**
     * @param GetPropertyByOwnerResult $GetPropertyByOwnerResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetPropertyByOwnerResponse
     */
    public function setGetPropertyByOwnerResult($GetPropertyByOwnerResult)
    {
      $this->GetPropertyByOwnerResult = $GetPropertyByOwnerResult;
      return $this;
    }

}
