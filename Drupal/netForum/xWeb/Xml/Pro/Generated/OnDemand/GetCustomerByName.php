<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByName
{

    /**
     * @var string $szName
     */
    protected $szName = null;

    /**
     * @param string $szName
     */
    public function __construct($szName)
    {
      $this->szName = $szName;
    }

    /**
     * @return string
     */
    public function getSzName()
    {
      return $this->szName;
    }

    /**
     * @param string $szName
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByName
     */
    public function setSzName($szName)
    {
      $this->szName = $szName;
      return $this;
    }

}
