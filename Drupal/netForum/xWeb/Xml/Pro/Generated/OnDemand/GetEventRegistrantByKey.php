<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetEventRegistrantByKey
{

    /**
     * @var string $regKey
     */
    protected $regKey = null;

    /**
     * @param string $regKey
     */
    public function __construct($regKey)
    {
      $this->regKey = $regKey;
    }

    /**
     * @return string
     */
    public function getRegKey()
    {
      return $this->regKey;
    }

    /**
     * @param string $regKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetEventRegistrantByKey
     */
    public function setRegKey($regKey)
    {
      $this->regKey = $regKey;
      return $this;
    }

}
