<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetSessionCustomerList
{

    /**
     * @var string $szSessionKey
     */
    protected $szSessionKey = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $szSessionKey
     * @param string $szRecordDate
     */
    public function __construct($szSessionKey, $szRecordDate)
    {
      $this->szSessionKey = $szSessionKey;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getSzSessionKey()
    {
      return $this->szSessionKey;
    }

    /**
     * @param string $szSessionKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetSessionCustomerList
     */
    public function setSzSessionKey($szSessionKey)
    {
      $this->szSessionKey = $szSessionKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetSessionCustomerList
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
