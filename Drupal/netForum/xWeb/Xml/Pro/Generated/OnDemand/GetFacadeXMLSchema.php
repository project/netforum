<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetFacadeXMLSchema
{

    /**
     * @var string $szObjectName
     */
    protected $szObjectName = null;

    /**
     * @param string $szObjectName
     */
    public function __construct($szObjectName)
    {
      $this->szObjectName = $szObjectName;
    }

    /**
     * @return string
     */
    public function getSzObjectName()
    {
      return $this->szObjectName;
    }

    /**
     * @param string $szObjectName
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetFacadeXMLSchema
     */
    public function setSzObjectName($szObjectName)
    {
      $this->szObjectName = $szObjectName;
      return $this;
    }

}
