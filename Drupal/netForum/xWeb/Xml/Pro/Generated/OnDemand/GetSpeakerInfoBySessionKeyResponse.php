<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetSpeakerInfoBySessionKeyResponse
{

    /**
     * @var GetSpeakerInfoBySessionKeyResult $GetSpeakerInfoBySessionKeyResult
     */
    protected $GetSpeakerInfoBySessionKeyResult = null;

    /**
     * @param GetSpeakerInfoBySessionKeyResult $GetSpeakerInfoBySessionKeyResult
     */
    public function __construct($GetSpeakerInfoBySessionKeyResult)
    {
      $this->GetSpeakerInfoBySessionKeyResult = $GetSpeakerInfoBySessionKeyResult;
    }

    /**
     * @return GetSpeakerInfoBySessionKeyResult
     */
    public function getGetSpeakerInfoBySessionKeyResult()
    {
      return $this->GetSpeakerInfoBySessionKeyResult;
    }

    /**
     * @param GetSpeakerInfoBySessionKeyResult $GetSpeakerInfoBySessionKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetSpeakerInfoBySessionKeyResponse
     */
    public function setGetSpeakerInfoBySessionKeyResult($GetSpeakerInfoBySessionKeyResult)
    {
      $this->GetSpeakerInfoBySessionKeyResult = $GetSpeakerInfoBySessionKeyResult;
      return $this;
    }

}
