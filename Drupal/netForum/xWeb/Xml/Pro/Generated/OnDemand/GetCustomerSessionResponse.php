<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerSessionResponse
{

    /**
     * @var GetCustomerSessionResult $GetCustomerSessionResult
     */
    protected $GetCustomerSessionResult = null;

    /**
     * @param GetCustomerSessionResult $GetCustomerSessionResult
     */
    public function __construct($GetCustomerSessionResult)
    {
      $this->GetCustomerSessionResult = $GetCustomerSessionResult;
    }

    /**
     * @return GetCustomerSessionResult
     */
    public function getGetCustomerSessionResult()
    {
      return $this->GetCustomerSessionResult;
    }

    /**
     * @param GetCustomerSessionResult $GetCustomerSessionResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerSessionResponse
     */
    public function setGetCustomerSessionResult($GetCustomerSessionResult)
    {
      $this->GetCustomerSessionResult = $GetCustomerSessionResult;
      return $this;
    }

}
