<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetActiveProductListResponse
{

    /**
     * @var GetActiveProductListResult $GetActiveProductListResult
     */
    protected $GetActiveProductListResult = null;

    /**
     * @param GetActiveProductListResult $GetActiveProductListResult
     */
    public function __construct($GetActiveProductListResult)
    {
      $this->GetActiveProductListResult = $GetActiveProductListResult;
    }

    /**
     * @return GetActiveProductListResult
     */
    public function getGetActiveProductListResult()
    {
      return $this->GetActiveProductListResult;
    }

    /**
     * @param GetActiveProductListResult $GetActiveProductListResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetActiveProductListResponse
     */
    public function setGetActiveProductListResult($GetActiveProductListResult)
    {
      $this->GetActiveProductListResult = $GetActiveProductListResult;
      return $this;
    }

}
