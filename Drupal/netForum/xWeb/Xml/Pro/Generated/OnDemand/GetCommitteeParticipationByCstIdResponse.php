<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCommitteeParticipationByCstIdResponse
{

    /**
     * @var GetCommitteeParticipationByCstIdResult $GetCommitteeParticipationByCstIdResult
     */
    protected $GetCommitteeParticipationByCstIdResult = null;

    /**
     * @param GetCommitteeParticipationByCstIdResult $GetCommitteeParticipationByCstIdResult
     */
    public function __construct($GetCommitteeParticipationByCstIdResult)
    {
      $this->GetCommitteeParticipationByCstIdResult = $GetCommitteeParticipationByCstIdResult;
    }

    /**
     * @return GetCommitteeParticipationByCstIdResult
     */
    public function getGetCommitteeParticipationByCstIdResult()
    {
      return $this->GetCommitteeParticipationByCstIdResult;
    }

    /**
     * @param GetCommitteeParticipationByCstIdResult $GetCommitteeParticipationByCstIdResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCommitteeParticipationByCstIdResponse
     */
    public function setGetCommitteeParticipationByCstIdResult($GetCommitteeParticipationByCstIdResult)
    {
      $this->GetCommitteeParticipationByCstIdResult = $GetCommitteeParticipationByCstIdResult;
      return $this;
    }

}
