<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetEventRegistrantByKeyResponse
{

    /**
     * @var GetEventRegistrantByKeyResult $GetEventRegistrantByKeyResult
     */
    protected $GetEventRegistrantByKeyResult = null;

    /**
     * @param GetEventRegistrantByKeyResult $GetEventRegistrantByKeyResult
     */
    public function __construct($GetEventRegistrantByKeyResult)
    {
      $this->GetEventRegistrantByKeyResult = $GetEventRegistrantByKeyResult;
    }

    /**
     * @return GetEventRegistrantByKeyResult
     */
    public function getGetEventRegistrantByKeyResult()
    {
      return $this->GetEventRegistrantByKeyResult;
    }

    /**
     * @param GetEventRegistrantByKeyResult $GetEventRegistrantByKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetEventRegistrantByKeyResponse
     */
    public function setGetEventRegistrantByKeyResult($GetEventRegistrantByKeyResult)
    {
      $this->GetEventRegistrantByKeyResult = $GetEventRegistrantByKeyResult;
      return $this;
    }

}
