<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetSessionsByTrackCodeResponse
{

    /**
     * @var GetSessionsByTrackCodeResult $GetSessionsByTrackCodeResult
     */
    protected $GetSessionsByTrackCodeResult = null;

    /**
     * @param GetSessionsByTrackCodeResult $GetSessionsByTrackCodeResult
     */
    public function __construct($GetSessionsByTrackCodeResult)
    {
      $this->GetSessionsByTrackCodeResult = $GetSessionsByTrackCodeResult;
    }

    /**
     * @return GetSessionsByTrackCodeResult
     */
    public function getGetSessionsByTrackCodeResult()
    {
      return $this->GetSessionsByTrackCodeResult;
    }

    /**
     * @param GetSessionsByTrackCodeResult $GetSessionsByTrackCodeResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetSessionsByTrackCodeResponse
     */
    public function setGetSessionsByTrackCodeResult($GetSessionsByTrackCodeResult)
    {
      $this->GetSessionsByTrackCodeResult = $GetSessionsByTrackCodeResult;
      return $this;
    }

}
