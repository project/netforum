<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class AddIndOrgRelationshipResponse
{

    /**
     * @var AddIndOrgRelationshipResult $AddIndOrgRelationshipResult
     */
    protected $AddIndOrgRelationshipResult = null;

    /**
     * @param AddIndOrgRelationshipResult $AddIndOrgRelationshipResult
     */
    public function __construct($AddIndOrgRelationshipResult)
    {
      $this->AddIndOrgRelationshipResult = $AddIndOrgRelationshipResult;
    }

    /**
     * @return AddIndOrgRelationshipResult
     */
    public function getAddIndOrgRelationshipResult()
    {
      return $this->AddIndOrgRelationshipResult;
    }

    /**
     * @param AddIndOrgRelationshipResult $AddIndOrgRelationshipResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\AddIndOrgRelationshipResponse
     */
    public function setAddIndOrgRelationshipResult($AddIndOrgRelationshipResult)
    {
      $this->AddIndOrgRelationshipResult = $AddIndOrgRelationshipResult;
      return $this;
    }

}
