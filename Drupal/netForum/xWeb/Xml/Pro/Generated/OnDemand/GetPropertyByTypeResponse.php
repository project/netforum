<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetPropertyByTypeResponse
{

    /**
     * @var GetPropertyByTypeResult $GetPropertyByTypeResult
     */
    protected $GetPropertyByTypeResult = null;

    /**
     * @param GetPropertyByTypeResult $GetPropertyByTypeResult
     */
    public function __construct($GetPropertyByTypeResult)
    {
      $this->GetPropertyByTypeResult = $GetPropertyByTypeResult;
    }

    /**
     * @return GetPropertyByTypeResult
     */
    public function getGetPropertyByTypeResult()
    {
      return $this->GetPropertyByTypeResult;
    }

    /**
     * @param GetPropertyByTypeResult $GetPropertyByTypeResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetPropertyByTypeResponse
     */
    public function setGetPropertyByTypeResult($GetPropertyByTypeResult)
    {
      $this->GetPropertyByTypeResult = $GetPropertyByTypeResult;
      return $this;
    }

}
