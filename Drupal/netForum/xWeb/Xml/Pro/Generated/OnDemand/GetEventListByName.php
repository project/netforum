<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetEventListByName
{

    /**
     * @var string $szName
     */
    protected $szName = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $szName
     * @param string $szRecordDate
     */
    public function __construct($szName, $szRecordDate)
    {
      $this->szName = $szName;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getSzName()
    {
      return $this->szName;
    }

    /**
     * @param string $szName
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetEventListByName
     */
    public function setSzName($szName)
    {
      $this->szName = $szName;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetEventListByName
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
