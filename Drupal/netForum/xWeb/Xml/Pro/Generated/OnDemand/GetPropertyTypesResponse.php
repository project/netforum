<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetPropertyTypesResponse
{

    /**
     * @var GetPropertyTypesResult $GetPropertyTypesResult
     */
    protected $GetPropertyTypesResult = null;

    /**
     * @param GetPropertyTypesResult $GetPropertyTypesResult
     */
    public function __construct($GetPropertyTypesResult)
    {
      $this->GetPropertyTypesResult = $GetPropertyTypesResult;
    }

    /**
     * @return GetPropertyTypesResult
     */
    public function getGetPropertyTypesResult()
    {
      return $this->GetPropertyTypesResult;
    }

    /**
     * @param GetPropertyTypesResult $GetPropertyTypesResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetPropertyTypesResponse
     */
    public function setGetPropertyTypesResult($GetPropertyTypesResult)
    {
      $this->GetPropertyTypesResult = $GetPropertyTypesResult;
      return $this;
    }

}
