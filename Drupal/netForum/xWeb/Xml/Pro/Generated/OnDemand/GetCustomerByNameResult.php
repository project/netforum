<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByNameResult
{

    /**
     * @var string $any
     */
    protected $any = null;

    /**
     * @param string $any
     */
    public function __construct($any)
    {
      $this->any = $any;
    }

    /**
     * @return string
     */
    public function getAny()
    {
      return $this->any;
    }

    /**
     * @param string $any
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByNameResult
     */
    public function setAny($any)
    {
      $this->any = $any;
      return $this;
    }

}
