<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByMembershipType
{

    /**
     * @var string $productKey
     */
    protected $productKey = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @var boolean $bMemberOnly
     */
    protected $bMemberOnly = null;

    /**
     * @var boolean $bIncludeIndividual
     */
    protected $bIncludeIndividual = null;

    /**
     * @var boolean $bIncludeOrganization
     */
    protected $bIncludeOrganization = null;

    /**
     * @var boolean $bNotTerminated
     */
    protected $bNotTerminated = null;

    /**
     * @var boolean $bNotExpired
     */
    protected $bNotExpired = null;

    /**
     * @var string $szJoinedAfterDate
     */
    protected $szJoinedAfterDate = null;

    /**
     * @param string $productKey
     * @param string $szRecordDate
     * @param boolean $bMemberOnly
     * @param boolean $bIncludeIndividual
     * @param boolean $bIncludeOrganization
     * @param boolean $bNotTerminated
     * @param boolean $bNotExpired
     * @param string $szJoinedAfterDate
     */
    public function __construct($productKey, $szRecordDate, $bMemberOnly, $bIncludeIndividual, $bIncludeOrganization, $bNotTerminated, $bNotExpired, $szJoinedAfterDate)
    {
      $this->productKey = $productKey;
      $this->szRecordDate = $szRecordDate;
      $this->bMemberOnly = $bMemberOnly;
      $this->bIncludeIndividual = $bIncludeIndividual;
      $this->bIncludeOrganization = $bIncludeOrganization;
      $this->bNotTerminated = $bNotTerminated;
      $this->bNotExpired = $bNotExpired;
      $this->szJoinedAfterDate = $szJoinedAfterDate;
    }

    /**
     * @return string
     */
    public function getProductKey()
    {
      return $this->productKey;
    }

    /**
     * @param string $productKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByMembershipType
     */
    public function setProductKey($productKey)
    {
      $this->productKey = $productKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByMembershipType
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBMemberOnly()
    {
      return $this->bMemberOnly;
    }

    /**
     * @param boolean $bMemberOnly
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByMembershipType
     */
    public function setBMemberOnly($bMemberOnly)
    {
      $this->bMemberOnly = $bMemberOnly;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBIncludeIndividual()
    {
      return $this->bIncludeIndividual;
    }

    /**
     * @param boolean $bIncludeIndividual
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByMembershipType
     */
    public function setBIncludeIndividual($bIncludeIndividual)
    {
      $this->bIncludeIndividual = $bIncludeIndividual;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBIncludeOrganization()
    {
      return $this->bIncludeOrganization;
    }

    /**
     * @param boolean $bIncludeOrganization
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByMembershipType
     */
    public function setBIncludeOrganization($bIncludeOrganization)
    {
      $this->bIncludeOrganization = $bIncludeOrganization;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBNotTerminated()
    {
      return $this->bNotTerminated;
    }

    /**
     * @param boolean $bNotTerminated
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByMembershipType
     */
    public function setBNotTerminated($bNotTerminated)
    {
      $this->bNotTerminated = $bNotTerminated;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBNotExpired()
    {
      return $this->bNotExpired;
    }

    /**
     * @param boolean $bNotExpired
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByMembershipType
     */
    public function setBNotExpired($bNotExpired)
    {
      $this->bNotExpired = $bNotExpired;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzJoinedAfterDate()
    {
      return $this->szJoinedAfterDate;
    }

    /**
     * @param string $szJoinedAfterDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByMembershipType
     */
    public function setSzJoinedAfterDate($szJoinedAfterDate)
    {
      $this->szJoinedAfterDate = $szJoinedAfterDate;
      return $this;
    }

}
