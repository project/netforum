<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCommitteeParticipationChangesByDateResponse
{

    /**
     * @var GetCommitteeParticipationChangesByDateResult $GetCommitteeParticipationChangesByDateResult
     */
    protected $GetCommitteeParticipationChangesByDateResult = null;

    /**
     * @param GetCommitteeParticipationChangesByDateResult $GetCommitteeParticipationChangesByDateResult
     */
    public function __construct($GetCommitteeParticipationChangesByDateResult)
    {
      $this->GetCommitteeParticipationChangesByDateResult = $GetCommitteeParticipationChangesByDateResult;
    }

    /**
     * @return GetCommitteeParticipationChangesByDateResult
     */
    public function getGetCommitteeParticipationChangesByDateResult()
    {
      return $this->GetCommitteeParticipationChangesByDateResult;
    }

    /**
     * @param GetCommitteeParticipationChangesByDateResult $GetCommitteeParticipationChangesByDateResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCommitteeParticipationChangesByDateResponse
     */
    public function setGetCommitteeParticipationChangesByDateResult($GetCommitteeParticipationChangesByDateResult)
    {
      $this->GetCommitteeParticipationChangesByDateResult = $GetCommitteeParticipationChangesByDateResult;
      return $this;
    }

}
