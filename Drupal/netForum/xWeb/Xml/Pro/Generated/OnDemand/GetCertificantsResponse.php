<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCertificantsResponse
{

    /**
     * @var GetCertificantsResult $GetCertificantsResult
     */
    protected $GetCertificantsResult = null;

    /**
     * @param GetCertificantsResult $GetCertificantsResult
     */
    public function __construct($GetCertificantsResult)
    {
      $this->GetCertificantsResult = $GetCertificantsResult;
    }

    /**
     * @return GetCertificantsResult
     */
    public function getGetCertificantsResult()
    {
      return $this->GetCertificantsResult;
    }

    /**
     * @param GetCertificantsResult $GetCertificantsResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCertificantsResponse
     */
    public function setGetCertificantsResult($GetCertificantsResult)
    {
      $this->GetCertificantsResult = $GetCertificantsResult;
      return $this;
    }

}
