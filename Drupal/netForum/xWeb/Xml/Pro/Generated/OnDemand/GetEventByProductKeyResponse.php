<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetEventByProductKeyResponse
{

    /**
     * @var GetEventByProductKeyResult $GetEventByProductKeyResult
     */
    protected $GetEventByProductKeyResult = null;

    /**
     * @param GetEventByProductKeyResult $GetEventByProductKeyResult
     */
    public function __construct($GetEventByProductKeyResult)
    {
      $this->GetEventByProductKeyResult = $GetEventByProductKeyResult;
    }

    /**
     * @return GetEventByProductKeyResult
     */
    public function getGetEventByProductKeyResult()
    {
      return $this->GetEventByProductKeyResult;
    }

    /**
     * @param GetEventByProductKeyResult $GetEventByProductKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetEventByProductKeyResponse
     */
    public function setGetEventByProductKeyResult($GetEventByProductKeyResult)
    {
      $this->GetEventByProductKeyResult = $GetEventByProductKeyResult;
      return $this;
    }

}
