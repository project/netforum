<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetMembershipProxyListResponse
{

    /**
     * @var GetMembershipProxyListResult $GetMembershipProxyListResult
     */
    protected $GetMembershipProxyListResult = null;

    /**
     * @param GetMembershipProxyListResult $GetMembershipProxyListResult
     */
    public function __construct($GetMembershipProxyListResult)
    {
      $this->GetMembershipProxyListResult = $GetMembershipProxyListResult;
    }

    /**
     * @return GetMembershipProxyListResult
     */
    public function getGetMembershipProxyListResult()
    {
      return $this->GetMembershipProxyListResult;
    }

    /**
     * @param GetMembershipProxyListResult $GetMembershipProxyListResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetMembershipProxyListResponse
     */
    public function setGetMembershipProxyListResult($GetMembershipProxyListResult)
    {
      $this->GetMembershipProxyListResult = $GetMembershipProxyListResult;
      return $this;
    }

}
