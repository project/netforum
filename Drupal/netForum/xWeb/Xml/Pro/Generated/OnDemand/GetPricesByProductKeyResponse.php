<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetPricesByProductKeyResponse
{

    /**
     * @var GetPricesByProductKeyResult $GetPricesByProductKeyResult
     */
    protected $GetPricesByProductKeyResult = null;

    /**
     * @param GetPricesByProductKeyResult $GetPricesByProductKeyResult
     */
    public function __construct($GetPricesByProductKeyResult)
    {
      $this->GetPricesByProductKeyResult = $GetPricesByProductKeyResult;
    }

    /**
     * @return GetPricesByProductKeyResult
     */
    public function getGetPricesByProductKeyResult()
    {
      return $this->GetPricesByProductKeyResult;
    }

    /**
     * @param GetPricesByProductKeyResult $GetPricesByProductKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetPricesByProductKeyResponse
     */
    public function setGetPricesByProductKeyResult($GetPricesByProductKeyResult)
    {
      $this->GetPricesByProductKeyResult = $GetPricesByProductKeyResult;
      return $this;
    }

}
