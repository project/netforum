<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class IndexedResultListOfOrganizationByTypeResult extends ResultListOfOrganizationByTypeResult
{

    /**
     * @var string $lastIndex
     */
    protected $lastIndex = null;

    /**
     * @param string $userName
     * @param int $recordReturn
     * @param string $lastIndex
     */
    public function __construct($userName, $recordReturn, $lastIndex)
    {
      parent::__construct($userName, $recordReturn);
      $this->lastIndex = $lastIndex;
    }

    /**
     * @return string
     */
    public function getLastIndex()
    {
      return $this->lastIndex;
    }

    /**
     * @param string $lastIndex
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\IndexedResultListOfOrganizationByTypeResult
     */
    public function setLastIndex($lastIndex)
    {
      $this->lastIndex = $lastIndex;
      return $this;
    }

}
