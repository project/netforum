<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetActiveSubscriptionByCustomerResponse
{

    /**
     * @var GetActiveSubscriptionByCustomerResult $GetActiveSubscriptionByCustomerResult
     */
    protected $GetActiveSubscriptionByCustomerResult = null;

    /**
     * @param GetActiveSubscriptionByCustomerResult $GetActiveSubscriptionByCustomerResult
     */
    public function __construct($GetActiveSubscriptionByCustomerResult)
    {
      $this->GetActiveSubscriptionByCustomerResult = $GetActiveSubscriptionByCustomerResult;
    }

    /**
     * @return GetActiveSubscriptionByCustomerResult
     */
    public function getGetActiveSubscriptionByCustomerResult()
    {
      return $this->GetActiveSubscriptionByCustomerResult;
    }

    /**
     * @param GetActiveSubscriptionByCustomerResult $GetActiveSubscriptionByCustomerResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetActiveSubscriptionByCustomerResponse
     */
    public function setGetActiveSubscriptionByCustomerResult($GetActiveSubscriptionByCustomerResult)
    {
      $this->GetActiveSubscriptionByCustomerResult = $GetActiveSubscriptionByCustomerResult;
      return $this;
    }

}
