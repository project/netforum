<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetAwardEntriesByAwardResponse
{

    /**
     * @var GetAwardEntriesByAwardResult $GetAwardEntriesByAwardResult
     */
    protected $GetAwardEntriesByAwardResult = null;

    /**
     * @param GetAwardEntriesByAwardResult $GetAwardEntriesByAwardResult
     */
    public function __construct($GetAwardEntriesByAwardResult)
    {
      $this->GetAwardEntriesByAwardResult = $GetAwardEntriesByAwardResult;
    }

    /**
     * @return GetAwardEntriesByAwardResult
     */
    public function getGetAwardEntriesByAwardResult()
    {
      return $this->GetAwardEntriesByAwardResult;
    }

    /**
     * @param GetAwardEntriesByAwardResult $GetAwardEntriesByAwardResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetAwardEntriesByAwardResponse
     */
    public function setGetAwardEntriesByAwardResult($GetAwardEntriesByAwardResult)
    {
      $this->GetAwardEntriesByAwardResult = $GetAwardEntriesByAwardResult;
      return $this;
    }

}
