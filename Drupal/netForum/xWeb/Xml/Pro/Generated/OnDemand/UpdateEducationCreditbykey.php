<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class UpdateEducationCreditbykey
{

    /**
     * @var string $educationCreditCustomerKey
     */
    protected $educationCreditCustomerKey = null;

    /**
     * @var string $earnedDate
     */
    protected $earnedDate = null;

    /**
     * @var string $creditValue
     */
    protected $creditValue = null;

    /**
     * @var string $canceledDate
     */
    protected $canceledDate = null;

    /**
     * @var string $groupCode
     */
    protected $groupCode = null;

    /**
     * @var string $courseCode
     */
    protected $courseCode = null;

    /**
     * @var string $courseNumber
     */
    protected $courseNumber = null;

    /**
     * @var string $sponsoringOrg
     */
    protected $sponsoringOrg = null;

    /**
     * @param string $educationCreditCustomerKey
     * @param string $earnedDate
     * @param string $creditValue
     * @param string $canceledDate
     * @param string $groupCode
     * @param string $courseCode
     * @param string $courseNumber
     * @param string $sponsoringOrg
     */
    public function __construct($educationCreditCustomerKey, $earnedDate, $creditValue, $canceledDate, $groupCode, $courseCode, $courseNumber, $sponsoringOrg)
    {
      $this->educationCreditCustomerKey = $educationCreditCustomerKey;
      $this->earnedDate = $earnedDate;
      $this->creditValue = $creditValue;
      $this->canceledDate = $canceledDate;
      $this->groupCode = $groupCode;
      $this->courseCode = $courseCode;
      $this->courseNumber = $courseNumber;
      $this->sponsoringOrg = $sponsoringOrg;
    }

    /**
     * @return string
     */
    public function getEducationCreditCustomerKey()
    {
      return $this->educationCreditCustomerKey;
    }

    /**
     * @param string $educationCreditCustomerKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\UpdateEducationCreditbykey
     */
    public function setEducationCreditCustomerKey($educationCreditCustomerKey)
    {
      $this->educationCreditCustomerKey = $educationCreditCustomerKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getEarnedDate()
    {
      return $this->earnedDate;
    }

    /**
     * @param string $earnedDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\UpdateEducationCreditbykey
     */
    public function setEarnedDate($earnedDate)
    {
      $this->earnedDate = $earnedDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getCreditValue()
    {
      return $this->creditValue;
    }

    /**
     * @param string $creditValue
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\UpdateEducationCreditbykey
     */
    public function setCreditValue($creditValue)
    {
      $this->creditValue = $creditValue;
      return $this;
    }

    /**
     * @return string
     */
    public function getCanceledDate()
    {
      return $this->canceledDate;
    }

    /**
     * @param string $canceledDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\UpdateEducationCreditbykey
     */
    public function setCanceledDate($canceledDate)
    {
      $this->canceledDate = $canceledDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getGroupCode()
    {
      return $this->groupCode;
    }

    /**
     * @param string $groupCode
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\UpdateEducationCreditbykey
     */
    public function setGroupCode($groupCode)
    {
      $this->groupCode = $groupCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCourseCode()
    {
      return $this->courseCode;
    }

    /**
     * @param string $courseCode
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\UpdateEducationCreditbykey
     */
    public function setCourseCode($courseCode)
    {
      $this->courseCode = $courseCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCourseNumber()
    {
      return $this->courseNumber;
    }

    /**
     * @param string $courseNumber
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\UpdateEducationCreditbykey
     */
    public function setCourseNumber($courseNumber)
    {
      $this->courseNumber = $courseNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getSponsoringOrg()
    {
      return $this->sponsoringOrg;
    }

    /**
     * @param string $sponsoringOrg
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\UpdateEducationCreditbykey
     */
    public function setSponsoringOrg($sponsoringOrg)
    {
      $this->sponsoringOrg = $sponsoringOrg;
      return $this;
    }

}
