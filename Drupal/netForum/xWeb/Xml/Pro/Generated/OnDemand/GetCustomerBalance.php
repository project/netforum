<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerBalance
{

    /**
     * @var string $szCstKey
     */
    protected $szCstKey = null;

    /**
     * @param string $szCstKey
     */
    public function __construct($szCstKey)
    {
      $this->szCstKey = $szCstKey;
    }

    /**
     * @return string
     */
    public function getSzCstKey()
    {
      return $this->szCstKey;
    }

    /**
     * @param string $szCstKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerBalance
     */
    public function setSzCstKey($szCstKey)
    {
      $this->szCstKey = $szCstKey;
      return $this;
    }

}
