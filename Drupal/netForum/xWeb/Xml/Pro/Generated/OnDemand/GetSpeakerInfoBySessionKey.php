<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetSpeakerInfoBySessionKey
{

    /**
     * @var string $szSesKey
     */
    protected $szSesKey = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $szSesKey
     * @param string $szRecordDate
     */
    public function __construct($szSesKey, $szRecordDate)
    {
      $this->szSesKey = $szSesKey;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getSzSesKey()
    {
      return $this->szSesKey;
    }

    /**
     * @param string $szSesKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetSpeakerInfoBySessionKey
     */
    public function setSzSesKey($szSesKey)
    {
      $this->szSesKey = $szSesKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetSpeakerInfoBySessionKey
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
