<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetQuery2Response
{

    /**
     * @var GetQuery2Result $GetQuery2Result
     */
    protected $GetQuery2Result = null;

    /**
     * @param GetQuery2Result $GetQuery2Result
     */
    public function __construct($GetQuery2Result)
    {
      $this->GetQuery2Result = $GetQuery2Result;
    }

    /**
     * @return GetQuery2Result
     */
    public function getGetQuery2Result()
    {
      return $this->GetQuery2Result;
    }

    /**
     * @param GetQuery2Result $GetQuery2Result
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetQuery2Response
     */
    public function setGetQuery2Result($GetQuery2Result)
    {
      $this->GetQuery2Result = $GetQuery2Result;
      return $this;
    }

}
