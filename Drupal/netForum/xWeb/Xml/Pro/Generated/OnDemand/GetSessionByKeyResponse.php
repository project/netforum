<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetSessionByKeyResponse
{

    /**
     * @var GetSessionByKeyResult $GetSessionByKeyResult
     */
    protected $GetSessionByKeyResult = null;

    /**
     * @param GetSessionByKeyResult $GetSessionByKeyResult
     */
    public function __construct($GetSessionByKeyResult)
    {
      $this->GetSessionByKeyResult = $GetSessionByKeyResult;
    }

    /**
     * @return GetSessionByKeyResult
     */
    public function getGetSessionByKeyResult()
    {
      return $this->GetSessionByKeyResult;
    }

    /**
     * @param GetSessionByKeyResult $GetSessionByKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetSessionByKeyResponse
     */
    public function setGetSessionByKeyResult($GetSessionByKeyResult)
    {
      $this->GetSessionByKeyResult = $GetSessionByKeyResult;
      return $this;
    }

}
