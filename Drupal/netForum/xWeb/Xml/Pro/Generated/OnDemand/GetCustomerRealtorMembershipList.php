<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerRealtorMembershipList
{

    /**
     * @var string $CstKey
     */
    protected $CstKey = null;

    /**
     * @param string $CstKey
     */
    public function __construct($CstKey)
    {
      $this->CstKey = $CstKey;
    }

    /**
     * @return string
     */
    public function getCstKey()
    {
      return $this->CstKey;
    }

    /**
     * @param string $CstKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerRealtorMembershipList
     */
    public function setCstKey($CstKey)
    {
      $this->CstKey = $CstKey;
      return $this;
    }

}
