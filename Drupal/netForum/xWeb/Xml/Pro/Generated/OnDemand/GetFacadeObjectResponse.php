<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetFacadeObjectResponse
{

    /**
     * @var GetFacadeObjectResult $GetFacadeObjectResult
     */
    protected $GetFacadeObjectResult = null;

    /**
     * @param GetFacadeObjectResult $GetFacadeObjectResult
     */
    public function __construct($GetFacadeObjectResult)
    {
      $this->GetFacadeObjectResult = $GetFacadeObjectResult;
    }

    /**
     * @return GetFacadeObjectResult
     */
    public function getGetFacadeObjectResult()
    {
      return $this->GetFacadeObjectResult;
    }

    /**
     * @param GetFacadeObjectResult $GetFacadeObjectResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetFacadeObjectResponse
     */
    public function setGetFacadeObjectResult($GetFacadeObjectResult)
    {
      $this->GetFacadeObjectResult = $GetFacadeObjectResult;
      return $this;
    }

}
