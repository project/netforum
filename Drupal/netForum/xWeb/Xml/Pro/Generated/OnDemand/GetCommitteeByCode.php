<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCommitteeByCode
{

    /**
     * @var string $szCode
     */
    protected $szCode = null;

    /**
     * @param string $szCode
     */
    public function __construct($szCode)
    {
      $this->szCode = $szCode;
    }

    /**
     * @return string
     */
    public function getSzCode()
    {
      return $this->szCode;
    }

    /**
     * @param string $szCode
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCommitteeByCode
     */
    public function setSzCode($szCode)
    {
      $this->szCode = $szCode;
      return $this;
    }

}
