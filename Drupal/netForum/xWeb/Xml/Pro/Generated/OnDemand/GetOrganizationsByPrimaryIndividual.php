<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetOrganizationsByPrimaryIndividual
{

    /**
     * @var string $individualKey
     */
    protected $individualKey = null;

    /**
     * @var string $recordDate
     */
    protected $recordDate = null;

    /**
     * @param string $individualKey
     * @param string $recordDate
     */
    public function __construct($individualKey, $recordDate)
    {
      $this->individualKey = $individualKey;
      $this->recordDate = $recordDate;
    }

    /**
     * @return string
     */
    public function getIndividualKey()
    {
      return $this->individualKey;
    }

    /**
     * @param string $individualKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetOrganizationsByPrimaryIndividual
     */
    public function setIndividualKey($individualKey)
    {
      $this->individualKey = $individualKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getRecordDate()
    {
      return $this->recordDate;
    }

    /**
     * @param string $recordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetOrganizationsByPrimaryIndividual
     */
    public function setRecordDate($recordDate)
    {
      $this->recordDate = $recordDate;
      return $this;
    }

}
