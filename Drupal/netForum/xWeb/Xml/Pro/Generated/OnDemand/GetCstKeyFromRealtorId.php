<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCstKeyFromRealtorId
{

    /**
     * @var string $szRealtorId
     */
    protected $szRealtorId = null;

    /**
     * @var boolean $bIncludeIndividuals
     */
    protected $bIncludeIndividuals = null;

    /**
     * @var boolean $bIncludeOrganizations
     */
    protected $bIncludeOrganizations = null;

    /**
     * @param string $szRealtorId
     * @param boolean $bIncludeIndividuals
     * @param boolean $bIncludeOrganizations
     */
    public function __construct($szRealtorId, $bIncludeIndividuals, $bIncludeOrganizations)
    {
      $this->szRealtorId = $szRealtorId;
      $this->bIncludeIndividuals = $bIncludeIndividuals;
      $this->bIncludeOrganizations = $bIncludeOrganizations;
    }

    /**
     * @return string
     */
    public function getSzRealtorId()
    {
      return $this->szRealtorId;
    }

    /**
     * @param string $szRealtorId
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCstKeyFromRealtorId
     */
    public function setSzRealtorId($szRealtorId)
    {
      $this->szRealtorId = $szRealtorId;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBIncludeIndividuals()
    {
      return $this->bIncludeIndividuals;
    }

    /**
     * @param boolean $bIncludeIndividuals
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCstKeyFromRealtorId
     */
    public function setBIncludeIndividuals($bIncludeIndividuals)
    {
      $this->bIncludeIndividuals = $bIncludeIndividuals;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBIncludeOrganizations()
    {
      return $this->bIncludeOrganizations;
    }

    /**
     * @param boolean $bIncludeOrganizations
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCstKeyFromRealtorId
     */
    public function setBIncludeOrganizations($bIncludeOrganizations)
    {
      $this->bIncludeOrganizations = $bIncludeOrganizations;
      return $this;
    }

}
