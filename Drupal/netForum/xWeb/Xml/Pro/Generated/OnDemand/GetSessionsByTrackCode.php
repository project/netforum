<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetSessionsByTrackCode
{

    /**
     * @var string $szEvtKey
     */
    protected $szEvtKey = null;

    /**
     * @var string $szTrackCode
     */
    protected $szTrackCode = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $szEvtKey
     * @param string $szTrackCode
     * @param string $szRecordDate
     */
    public function __construct($szEvtKey, $szTrackCode, $szRecordDate)
    {
      $this->szEvtKey = $szEvtKey;
      $this->szTrackCode = $szTrackCode;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getSzEvtKey()
    {
      return $this->szEvtKey;
    }

    /**
     * @param string $szEvtKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetSessionsByTrackCode
     */
    public function setSzEvtKey($szEvtKey)
    {
      $this->szEvtKey = $szEvtKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzTrackCode()
    {
      return $this->szTrackCode;
    }

    /**
     * @param string $szTrackCode
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetSessionsByTrackCode
     */
    public function setSzTrackCode($szTrackCode)
    {
      $this->szTrackCode = $szTrackCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetSessionsByTrackCode
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
