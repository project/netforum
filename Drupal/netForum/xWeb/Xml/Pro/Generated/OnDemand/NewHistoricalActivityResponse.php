<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class NewHistoricalActivityResponse
{

    /**
     * @var NewHistoricalActivityResult $NewHistoricalActivityResult
     */
    protected $NewHistoricalActivityResult = null;

    /**
     * @param NewHistoricalActivityResult $NewHistoricalActivityResult
     */
    public function __construct($NewHistoricalActivityResult)
    {
      $this->NewHistoricalActivityResult = $NewHistoricalActivityResult;
    }

    /**
     * @return NewHistoricalActivityResult
     */
    public function getNewHistoricalActivityResult()
    {
      return $this->NewHistoricalActivityResult;
    }

    /**
     * @param NewHistoricalActivityResult $NewHistoricalActivityResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\NewHistoricalActivityResponse
     */
    public function setNewHistoricalActivityResult($NewHistoricalActivityResult)
    {
      $this->NewHistoricalActivityResult = $NewHistoricalActivityResult;
      return $this;
    }

}
