<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByRecordDate
{

    /**
     * @var boolean $bIncludeIndividuals
     */
    protected $bIncludeIndividuals = null;

    /**
     * @var boolean $bIncludeOrganizations
     */
    protected $bIncludeOrganizations = null;

    /**
     * @var boolean $bMembersOnly
     */
    protected $bMembersOnly = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param boolean $bIncludeIndividuals
     * @param boolean $bIncludeOrganizations
     * @param boolean $bMembersOnly
     * @param string $szRecordDate
     */
    public function __construct($bIncludeIndividuals, $bIncludeOrganizations, $bMembersOnly, $szRecordDate)
    {
      $this->bIncludeIndividuals = $bIncludeIndividuals;
      $this->bIncludeOrganizations = $bIncludeOrganizations;
      $this->bMembersOnly = $bMembersOnly;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return boolean
     */
    public function getBIncludeIndividuals()
    {
      return $this->bIncludeIndividuals;
    }

    /**
     * @param boolean $bIncludeIndividuals
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRecordDate
     */
    public function setBIncludeIndividuals($bIncludeIndividuals)
    {
      $this->bIncludeIndividuals = $bIncludeIndividuals;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBIncludeOrganizations()
    {
      return $this->bIncludeOrganizations;
    }

    /**
     * @param boolean $bIncludeOrganizations
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRecordDate
     */
    public function setBIncludeOrganizations($bIncludeOrganizations)
    {
      $this->bIncludeOrganizations = $bIncludeOrganizations;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBMembersOnly()
    {
      return $this->bMembersOnly;
    }

    /**
     * @param boolean $bMembersOnly
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRecordDate
     */
    public function setBMembersOnly($bMembersOnly)
    {
      $this->bMembersOnly = $bMembersOnly;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRecordDate
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
