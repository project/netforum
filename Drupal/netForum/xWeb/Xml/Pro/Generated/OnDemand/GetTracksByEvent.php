<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetTracksByEvent
{

    /**
     * @var string $szEvtKey
     */
    protected $szEvtKey = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $szEvtKey
     * @param string $szRecordDate
     */
    public function __construct($szEvtKey, $szRecordDate)
    {
      $this->szEvtKey = $szEvtKey;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getSzEvtKey()
    {
      return $this->szEvtKey;
    }

    /**
     * @param string $szEvtKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetTracksByEvent
     */
    public function setSzEvtKey($szEvtKey)
    {
      $this->szEvtKey = $szEvtKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetTracksByEvent
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
