<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class IndividualRelationshipResult
{

    /**
     * @var string $ind_cst_key
     */
    protected $ind_cst_key = null;

    /**
     * @var string $cst_sort_name_dn
     */
    protected $cst_sort_name_dn = null;

    /**
     * @var string $cst_id
     */
    protected $cst_id = null;

    /**
     * @var boolean $cst_member_flag
     */
    protected $cst_member_flag = null;

    /**
     * @var boolean $cst_receives_benefits_flag
     */
    protected $cst_receives_benefits_flag = null;

    /**
     * @var string $ixo_rlt_code
     */
    protected $ixo_rlt_code = null;

    /**
     * @var string $ixo_title
     */
    protected $ixo_title = null;

    /**
     * @var string $ixo_start_date
     */
    protected $ixo_start_date = null;

    /**
     * @var string $ixo_end_date
     */
    protected $ixo_end_date = null;

    /**
     * @param boolean $cst_member_flag
     * @param boolean $cst_receives_benefits_flag
     */
    public function __construct($cst_member_flag, $cst_receives_benefits_flag)
    {
      $this->cst_member_flag = $cst_member_flag;
      $this->cst_receives_benefits_flag = $cst_receives_benefits_flag;
    }

    /**
     * @return string
     */
    public function getInd_cst_key()
    {
      return $this->ind_cst_key;
    }

    /**
     * @param string $ind_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\IndividualRelationshipResult
     */
    public function setInd_cst_key($ind_cst_key)
    {
      $this->ind_cst_key = $ind_cst_key;
      return $this;
    }

    /**
     * @return string
     */
    public function getCst_sort_name_dn()
    {
      return $this->cst_sort_name_dn;
    }

    /**
     * @param string $cst_sort_name_dn
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\IndividualRelationshipResult
     */
    public function setCst_sort_name_dn($cst_sort_name_dn)
    {
      $this->cst_sort_name_dn = $cst_sort_name_dn;
      return $this;
    }

    /**
     * @return string
     */
    public function getCst_id()
    {
      return $this->cst_id;
    }

    /**
     * @param string $cst_id
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\IndividualRelationshipResult
     */
    public function setCst_id($cst_id)
    {
      $this->cst_id = $cst_id;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCst_member_flag()
    {
      return $this->cst_member_flag;
    }

    /**
     * @param boolean $cst_member_flag
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\IndividualRelationshipResult
     */
    public function setCst_member_flag($cst_member_flag)
    {
      $this->cst_member_flag = $cst_member_flag;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCst_receives_benefits_flag()
    {
      return $this->cst_receives_benefits_flag;
    }

    /**
     * @param boolean $cst_receives_benefits_flag
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\IndividualRelationshipResult
     */
    public function setCst_receives_benefits_flag($cst_receives_benefits_flag)
    {
      $this->cst_receives_benefits_flag = $cst_receives_benefits_flag;
      return $this;
    }

    /**
     * @return string
     */
    public function getIxo_rlt_code()
    {
      return $this->ixo_rlt_code;
    }

    /**
     * @param string $ixo_rlt_code
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\IndividualRelationshipResult
     */
    public function setIxo_rlt_code($ixo_rlt_code)
    {
      $this->ixo_rlt_code = $ixo_rlt_code;
      return $this;
    }

    /**
     * @return string
     */
    public function getIxo_title()
    {
      return $this->ixo_title;
    }

    /**
     * @param string $ixo_title
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\IndividualRelationshipResult
     */
    public function setIxo_title($ixo_title)
    {
      $this->ixo_title = $ixo_title;
      return $this;
    }

    /**
     * @return string
     */
    public function getIxo_start_date()
    {
      return $this->ixo_start_date;
    }

    /**
     * @param string $ixo_start_date
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\IndividualRelationshipResult
     */
    public function setIxo_start_date($ixo_start_date)
    {
      $this->ixo_start_date = $ixo_start_date;
      return $this;
    }

    /**
     * @return string
     */
    public function getIxo_end_date()
    {
      return $this->ixo_end_date;
    }

    /**
     * @param string $ixo_end_date
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\IndividualRelationshipResult
     */
    public function setIxo_end_date($ixo_end_date)
    {
      $this->ixo_end_date = $ixo_end_date;
      return $this;
    }

}
