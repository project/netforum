<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerCertificationsResponse
{

    /**
     * @var GetCustomerCertificationsResult $GetCustomerCertificationsResult
     */
    protected $GetCustomerCertificationsResult = null;

    /**
     * @param GetCustomerCertificationsResult $GetCustomerCertificationsResult
     */
    public function __construct($GetCustomerCertificationsResult)
    {
      $this->GetCustomerCertificationsResult = $GetCustomerCertificationsResult;
    }

    /**
     * @return GetCustomerCertificationsResult
     */
    public function getGetCustomerCertificationsResult()
    {
      return $this->GetCustomerCertificationsResult;
    }

    /**
     * @param GetCustomerCertificationsResult $GetCustomerCertificationsResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerCertificationsResponse
     */
    public function setGetCustomerCertificationsResult($GetCustomerCertificationsResult)
    {
      $this->GetCustomerCertificationsResult = $GetCustomerCertificationsResult;
      return $this;
    }

}
