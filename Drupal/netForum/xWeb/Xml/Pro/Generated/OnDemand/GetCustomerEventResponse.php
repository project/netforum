<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerEventResponse
{

    /**
     * @var GetCustomerEventResult $GetCustomerEventResult
     */
    protected $GetCustomerEventResult = null;

    /**
     * @param GetCustomerEventResult $GetCustomerEventResult
     */
    public function __construct($GetCustomerEventResult)
    {
      $this->GetCustomerEventResult = $GetCustomerEventResult;
    }

    /**
     * @return GetCustomerEventResult
     */
    public function getGetCustomerEventResult()
    {
      return $this->GetCustomerEventResult;
    }

    /**
     * @param GetCustomerEventResult $GetCustomerEventResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerEventResponse
     */
    public function setGetCustomerEventResult($GetCustomerEventResult)
    {
      $this->GetCustomerEventResult = $GetCustomerEventResult;
      return $this;
    }

}
