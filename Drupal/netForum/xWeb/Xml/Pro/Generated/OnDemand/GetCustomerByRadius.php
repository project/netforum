<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByRadius
{

    /**
     * @var string $szAddressLine1
     */
    protected $szAddressLine1 = null;

    /**
     * @var string $szAddressLine2
     */
    protected $szAddressLine2 = null;

    /**
     * @var string $szCity
     */
    protected $szCity = null;

    /**
     * @var string $szState
     */
    protected $szState = null;

    /**
     * @var string $szZip
     */
    protected $szZip = null;

    /**
     * @var int $iRadius
     */
    protected $iRadius = null;

    /**
     * @var boolean $bIncludeIndividual
     */
    protected $bIncludeIndividual = null;

    /**
     * @var boolean $bIncludeOrganization
     */
    protected $bIncludeOrganization = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $szAddressLine1
     * @param string $szAddressLine2
     * @param string $szCity
     * @param string $szState
     * @param string $szZip
     * @param int $iRadius
     * @param boolean $bIncludeIndividual
     * @param boolean $bIncludeOrganization
     * @param string $szRecordDate
     */
    public function __construct($szAddressLine1, $szAddressLine2, $szCity, $szState, $szZip, $iRadius, $bIncludeIndividual, $bIncludeOrganization, $szRecordDate)
    {
      $this->szAddressLine1 = $szAddressLine1;
      $this->szAddressLine2 = $szAddressLine2;
      $this->szCity = $szCity;
      $this->szState = $szState;
      $this->szZip = $szZip;
      $this->iRadius = $iRadius;
      $this->bIncludeIndividual = $bIncludeIndividual;
      $this->bIncludeOrganization = $bIncludeOrganization;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getSzAddressLine1()
    {
      return $this->szAddressLine1;
    }

    /**
     * @param string $szAddressLine1
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRadius
     */
    public function setSzAddressLine1($szAddressLine1)
    {
      $this->szAddressLine1 = $szAddressLine1;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzAddressLine2()
    {
      return $this->szAddressLine2;
    }

    /**
     * @param string $szAddressLine2
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRadius
     */
    public function setSzAddressLine2($szAddressLine2)
    {
      $this->szAddressLine2 = $szAddressLine2;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzCity()
    {
      return $this->szCity;
    }

    /**
     * @param string $szCity
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRadius
     */
    public function setSzCity($szCity)
    {
      $this->szCity = $szCity;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzState()
    {
      return $this->szState;
    }

    /**
     * @param string $szState
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRadius
     */
    public function setSzState($szState)
    {
      $this->szState = $szState;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzZip()
    {
      return $this->szZip;
    }

    /**
     * @param string $szZip
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRadius
     */
    public function setSzZip($szZip)
    {
      $this->szZip = $szZip;
      return $this;
    }

    /**
     * @return int
     */
    public function getIRadius()
    {
      return $this->iRadius;
    }

    /**
     * @param int $iRadius
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRadius
     */
    public function setIRadius($iRadius)
    {
      $this->iRadius = $iRadius;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBIncludeIndividual()
    {
      return $this->bIncludeIndividual;
    }

    /**
     * @param boolean $bIncludeIndividual
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRadius
     */
    public function setBIncludeIndividual($bIncludeIndividual)
    {
      $this->bIncludeIndividual = $bIncludeIndividual;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBIncludeOrganization()
    {
      return $this->bIncludeOrganization;
    }

    /**
     * @param boolean $bIncludeOrganization
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRadius
     */
    public function setBIncludeOrganization($bIncludeOrganization)
    {
      $this->bIncludeOrganization = $bIncludeOrganization;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRadius
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
