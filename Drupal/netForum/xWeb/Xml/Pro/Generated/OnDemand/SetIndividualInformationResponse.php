<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class SetIndividualInformationResponse
{

    /**
     * @var SetIndividualInformationResult $SetIndividualInformationResult
     */
    protected $SetIndividualInformationResult = null;

    /**
     * @param SetIndividualInformationResult $SetIndividualInformationResult
     */
    public function __construct($SetIndividualInformationResult)
    {
      $this->SetIndividualInformationResult = $SetIndividualInformationResult;
    }

    /**
     * @return SetIndividualInformationResult
     */
    public function getSetIndividualInformationResult()
    {
      return $this->SetIndividualInformationResult;
    }

    /**
     * @param SetIndividualInformationResult $SetIndividualInformationResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetIndividualInformationResponse
     */
    public function setSetIndividualInformationResult($SetIndividualInformationResult)
    {
      $this->SetIndividualInformationResult = $SetIndividualInformationResult;
      return $this;
    }

}
