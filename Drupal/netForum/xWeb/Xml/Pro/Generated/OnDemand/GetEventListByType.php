<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetEventListByType
{

    /**
     * @var string $typeCode
     */
    protected $typeCode = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $typeCode
     * @param string $szRecordDate
     */
    public function __construct($typeCode, $szRecordDate)
    {
      $this->typeCode = $typeCode;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getTypeCode()
    {
      return $this->typeCode;
    }

    /**
     * @param string $typeCode
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetEventListByType
     */
    public function setTypeCode($typeCode)
    {
      $this->typeCode = $typeCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetEventListByType
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
