<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCommitteeByCodeResponse
{

    /**
     * @var GetCommitteeByCodeResult $GetCommitteeByCodeResult
     */
    protected $GetCommitteeByCodeResult = null;

    /**
     * @param GetCommitteeByCodeResult $GetCommitteeByCodeResult
     */
    public function __construct($GetCommitteeByCodeResult)
    {
      $this->GetCommitteeByCodeResult = $GetCommitteeByCodeResult;
    }

    /**
     * @return GetCommitteeByCodeResult
     */
    public function getGetCommitteeByCodeResult()
    {
      return $this->GetCommitteeByCodeResult;
    }

    /**
     * @param GetCommitteeByCodeResult $GetCommitteeByCodeResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCommitteeByCodeResponse
     */
    public function setGetCommitteeByCodeResult($GetCommitteeByCodeResult)
    {
      $this->GetCommitteeByCodeResult = $GetCommitteeByCodeResult;
      return $this;
    }

}
