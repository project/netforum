<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetOrganizationByTypeResponse
{

    /**
     * @var IndexedResultListOfOrganizationByTypeResult $GetOrganizationByTypeResult
     */
    protected $GetOrganizationByTypeResult = null;

    /**
     * @param IndexedResultListOfOrganizationByTypeResult $GetOrganizationByTypeResult
     */
    public function __construct($GetOrganizationByTypeResult)
    {
      $this->GetOrganizationByTypeResult = $GetOrganizationByTypeResult;
    }

    /**
     * @return IndexedResultListOfOrganizationByTypeResult
     */
    public function getGetOrganizationByTypeResult()
    {
      return $this->GetOrganizationByTypeResult;
    }

    /**
     * @param IndexedResultListOfOrganizationByTypeResult $GetOrganizationByTypeResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetOrganizationByTypeResponse
     */
    public function setGetOrganizationByTypeResult($GetOrganizationByTypeResult)
    {
      $this->GetOrganizationByTypeResult = $GetOrganizationByTypeResult;
      return $this;
    }

}
