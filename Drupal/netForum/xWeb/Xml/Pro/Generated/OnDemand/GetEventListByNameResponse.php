<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetEventListByNameResponse
{

    /**
     * @var GetEventListByNameResult $GetEventListByNameResult
     */
    protected $GetEventListByNameResult = null;

    /**
     * @param GetEventListByNameResult $GetEventListByNameResult
     */
    public function __construct($GetEventListByNameResult)
    {
      $this->GetEventListByNameResult = $GetEventListByNameResult;
    }

    /**
     * @return GetEventListByNameResult
     */
    public function getGetEventListByNameResult()
    {
      return $this->GetEventListByNameResult;
    }

    /**
     * @param GetEventListByNameResult $GetEventListByNameResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetEventListByNameResponse
     */
    public function setGetEventListByNameResult($GetEventListByNameResult)
    {
      $this->GetEventListByNameResult = $GetEventListByNameResult;
      return $this;
    }

}
