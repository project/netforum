<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetSessionListByNameResponse
{

    /**
     * @var GetSessionListByNameResult $GetSessionListByNameResult
     */
    protected $GetSessionListByNameResult = null;

    /**
     * @param GetSessionListByNameResult $GetSessionListByNameResult
     */
    public function __construct($GetSessionListByNameResult)
    {
      $this->GetSessionListByNameResult = $GetSessionListByNameResult;
    }

    /**
     * @return GetSessionListByNameResult
     */
    public function getGetSessionListByNameResult()
    {
      return $this->GetSessionListByNameResult;
    }

    /**
     * @param GetSessionListByNameResult $GetSessionListByNameResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetSessionListByNameResponse
     */
    public function setGetSessionListByNameResult($GetSessionListByNameResult)
    {
      $this->GetSessionListByNameResult = $GetSessionListByNameResult;
      return $this;
    }

}
