<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetIndividualByDemographicResponse
{

    /**
     * @var GetIndividualByDemographicResult $GetIndividualByDemographicResult
     */
    protected $GetIndividualByDemographicResult = null;

    /**
     * @param GetIndividualByDemographicResult $GetIndividualByDemographicResult
     */
    public function __construct($GetIndividualByDemographicResult)
    {
      $this->GetIndividualByDemographicResult = $GetIndividualByDemographicResult;
    }

    /**
     * @return GetIndividualByDemographicResult
     */
    public function getGetIndividualByDemographicResult()
    {
      return $this->GetIndividualByDemographicResult;
    }

    /**
     * @param GetIndividualByDemographicResult $GetIndividualByDemographicResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetIndividualByDemographicResponse
     */
    public function setGetIndividualByDemographicResult($GetIndividualByDemographicResult)
    {
      $this->GetIndividualByDemographicResult = $GetIndividualByDemographicResult;
      return $this;
    }

}
