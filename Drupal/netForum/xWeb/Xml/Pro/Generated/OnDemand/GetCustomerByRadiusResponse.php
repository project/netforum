<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByRadiusResponse
{

    /**
     * @var GetCustomerByRadiusResult $GetCustomerByRadiusResult
     */
    protected $GetCustomerByRadiusResult = null;

    /**
     * @param GetCustomerByRadiusResult $GetCustomerByRadiusResult
     */
    public function __construct($GetCustomerByRadiusResult)
    {
      $this->GetCustomerByRadiusResult = $GetCustomerByRadiusResult;
    }

    /**
     * @return GetCustomerByRadiusResult
     */
    public function getGetCustomerByRadiusResult()
    {
      return $this->GetCustomerByRadiusResult;
    }

    /**
     * @param GetCustomerByRadiusResult $GetCustomerByRadiusResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRadiusResponse
     */
    public function setGetCustomerByRadiusResult($GetCustomerByRadiusResult)
    {
      $this->GetCustomerByRadiusResult = $GetCustomerByRadiusResult;
      return $this;
    }

}
