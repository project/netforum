<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCertificationProductsResponse
{

    /**
     * @var GetCertificationProductsResult $GetCertificationProductsResult
     */
    protected $GetCertificationProductsResult = null;

    /**
     * @param GetCertificationProductsResult $GetCertificationProductsResult
     */
    public function __construct($GetCertificationProductsResult)
    {
      $this->GetCertificationProductsResult = $GetCertificationProductsResult;
    }

    /**
     * @return GetCertificationProductsResult
     */
    public function getGetCertificationProductsResult()
    {
      return $this->GetCertificationProductsResult;
    }

    /**
     * @param GetCertificationProductsResult $GetCertificationProductsResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCertificationProductsResponse
     */
    public function setGetCertificationProductsResult($GetCertificationProductsResult)
    {
      $this->GetCertificationProductsResult = $GetCertificationProductsResult;
      return $this;
    }

}
