<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetIndividualListByChangeDateRelationshipResponse
{

    /**
     * @var IndexedResultListOfIndividualRelationshipResult $GetIndividualListByChangeDateRelationshipResult
     */
    protected $GetIndividualListByChangeDateRelationshipResult = null;

    /**
     * @param IndexedResultListOfIndividualRelationshipResult $GetIndividualListByChangeDateRelationshipResult
     */
    public function __construct($GetIndividualListByChangeDateRelationshipResult)
    {
      $this->GetIndividualListByChangeDateRelationshipResult = $GetIndividualListByChangeDateRelationshipResult;
    }

    /**
     * @return IndexedResultListOfIndividualRelationshipResult
     */
    public function getGetIndividualListByChangeDateRelationshipResult()
    {
      return $this->GetIndividualListByChangeDateRelationshipResult;
    }

    /**
     * @param IndexedResultListOfIndividualRelationshipResult $GetIndividualListByChangeDateRelationshipResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetIndividualListByChangeDateRelationshipResponse
     */
    public function setGetIndividualListByChangeDateRelationshipResult($GetIndividualListByChangeDateRelationshipResult)
    {
      $this->GetIndividualListByChangeDateRelationshipResult = $GetIndividualListByChangeDateRelationshipResult;
      return $this;
    }

}
