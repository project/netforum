<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetSessionCustomerListResponse
{

    /**
     * @var GetSessionCustomerListResult $GetSessionCustomerListResult
     */
    protected $GetSessionCustomerListResult = null;

    /**
     * @param GetSessionCustomerListResult $GetSessionCustomerListResult
     */
    public function __construct($GetSessionCustomerListResult)
    {
      $this->GetSessionCustomerListResult = $GetSessionCustomerListResult;
    }

    /**
     * @return GetSessionCustomerListResult
     */
    public function getGetSessionCustomerListResult()
    {
      return $this->GetSessionCustomerListResult;
    }

    /**
     * @param GetSessionCustomerListResult $GetSessionCustomerListResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetSessionCustomerListResponse
     */
    public function setGetSessionCustomerListResult($GetSessionCustomerListResult)
    {
      $this->GetSessionCustomerListResult = $GetSessionCustomerListResult;
      return $this;
    }

}
