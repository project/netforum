<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetOrganizationsByPrimaryIndividualResponse
{

    /**
     * @var IndexedResultListOfOrganizationByPrimaryInd $GetOrganizationsByPrimaryIndividualResult
     */
    protected $GetOrganizationsByPrimaryIndividualResult = null;

    /**
     * @param IndexedResultListOfOrganizationByPrimaryInd $GetOrganizationsByPrimaryIndividualResult
     */
    public function __construct($GetOrganizationsByPrimaryIndividualResult)
    {
      $this->GetOrganizationsByPrimaryIndividualResult = $GetOrganizationsByPrimaryIndividualResult;
    }

    /**
     * @return IndexedResultListOfOrganizationByPrimaryInd
     */
    public function getGetOrganizationsByPrimaryIndividualResult()
    {
      return $this->GetOrganizationsByPrimaryIndividualResult;
    }

    /**
     * @param IndexedResultListOfOrganizationByPrimaryInd $GetOrganizationsByPrimaryIndividualResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetOrganizationsByPrimaryIndividualResponse
     */
    public function setGetOrganizationsByPrimaryIndividualResult($GetOrganizationsByPrimaryIndividualResult)
    {
      $this->GetOrganizationsByPrimaryIndividualResult = $GetOrganizationsByPrimaryIndividualResult;
      return $this;
    }

}
