<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByZipResponse
{

    /**
     * @var GetCustomerByZipResult $GetCustomerByZipResult
     */
    protected $GetCustomerByZipResult = null;

    /**
     * @param GetCustomerByZipResult $GetCustomerByZipResult
     */
    public function __construct($GetCustomerByZipResult)
    {
      $this->GetCustomerByZipResult = $GetCustomerByZipResult;
    }

    /**
     * @return GetCustomerByZipResult
     */
    public function getGetCustomerByZipResult()
    {
      return $this->GetCustomerByZipResult;
    }

    /**
     * @param GetCustomerByZipResult $GetCustomerByZipResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByZipResponse
     */
    public function setGetCustomerByZipResult($GetCustomerByZipResult)
    {
      $this->GetCustomerByZipResult = $GetCustomerByZipResult;
      return $this;
    }

}
