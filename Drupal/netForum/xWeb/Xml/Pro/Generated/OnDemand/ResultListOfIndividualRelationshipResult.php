<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class ResultListOfIndividualRelationshipResult
{

    /**
     * @var IndividualRelationshipResult[] $Result
     */
    protected $Result = null;

    /**
     * @var string $userName
     */
    protected $userName = null;

    /**
     * @var int $recordReturn
     */
    protected $recordReturn = null;

    /**
     * @param string $userName
     * @param int $recordReturn
     */
    public function __construct($userName, $recordReturn)
    {
      $this->userName = $userName;
      $this->recordReturn = $recordReturn;
    }

    /**
     * @return IndividualRelationshipResult[]
     */
    public function getResult()
    {
      return $this->Result;
    }

    /**
     * @param IndividualRelationshipResult[] $Result
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\ResultListOfIndividualRelationshipResult
     */
    public function setResult(array $Result = null)
    {
      $this->Result = $Result;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
      return $this->userName;
    }

    /**
     * @param string $userName
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\ResultListOfIndividualRelationshipResult
     */
    public function setUserName($userName)
    {
      $this->userName = $userName;
      return $this;
    }

    /**
     * @return int
     */
    public function getRecordReturn()
    {
      return $this->recordReturn;
    }

    /**
     * @param int $recordReturn
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\ResultListOfIndividualRelationshipResult
     */
    public function setRecordReturn($recordReturn)
    {
      $this->recordReturn = $recordReturn;
      return $this;
    }

}
