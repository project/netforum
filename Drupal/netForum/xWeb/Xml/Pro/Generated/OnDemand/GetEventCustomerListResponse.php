<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetEventCustomerListResponse
{

    /**
     * @var GetEventCustomerListResult $GetEventCustomerListResult
     */
    protected $GetEventCustomerListResult = null;

    /**
     * @param GetEventCustomerListResult $GetEventCustomerListResult
     */
    public function __construct($GetEventCustomerListResult)
    {
      $this->GetEventCustomerListResult = $GetEventCustomerListResult;
    }

    /**
     * @return GetEventCustomerListResult
     */
    public function getGetEventCustomerListResult()
    {
      return $this->GetEventCustomerListResult;
    }

    /**
     * @param GetEventCustomerListResult $GetEventCustomerListResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetEventCustomerListResponse
     */
    public function setGetEventCustomerListResult($GetEventCustomerListResult)
    {
      $this->GetEventCustomerListResult = $GetEventCustomerListResult;
      return $this;
    }

}
