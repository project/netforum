<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetChildOrgsByParentOrgKey
{

    /**
     * @var string $szParentOrgKey
     */
    protected $szParentOrgKey = null;

    /**
     * @param string $szParentOrgKey
     */
    public function __construct($szParentOrgKey)
    {
      $this->szParentOrgKey = $szParentOrgKey;
    }

    /**
     * @return string
     */
    public function getSzParentOrgKey()
    {
      return $this->szParentOrgKey;
    }

    /**
     * @param string $szParentOrgKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetChildOrgsByParentOrgKey
     */
    public function setSzParentOrgKey($szParentOrgKey)
    {
      $this->szParentOrgKey = $szParentOrgKey;
      return $this;
    }

}
