<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class SetHistoricalActivity
{

    /**
     * @var string $HistoricalActivityKey
     */
    protected $HistoricalActivityKey = null;

    /**
     * @var oUpdateNode $oUpdateNode
     */
    protected $oUpdateNode = null;

    /**
     * @param string $HistoricalActivityKey
     * @param oUpdateNode $oUpdateNode
     */
    public function __construct($HistoricalActivityKey, $oUpdateNode)
    {
      $this->HistoricalActivityKey = $HistoricalActivityKey;
      $this->oUpdateNode = $oUpdateNode;
    }

    /**
     * @return string
     */
    public function getHistoricalActivityKey()
    {
      return $this->HistoricalActivityKey;
    }

    /**
     * @param string $HistoricalActivityKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetHistoricalActivity
     */
    public function setHistoricalActivityKey($HistoricalActivityKey)
    {
      $this->HistoricalActivityKey = $HistoricalActivityKey;
      return $this;
    }

    /**
     * @return oUpdateNode
     */
    public function getOUpdateNode()
    {
      return $this->oUpdateNode;
    }

    /**
     * @param oUpdateNode $oUpdateNode
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetHistoricalActivity
     */
    public function setOUpdateNode($oUpdateNode)
    {
      $this->oUpdateNode = $oUpdateNode;
      return $this;
    }

}
