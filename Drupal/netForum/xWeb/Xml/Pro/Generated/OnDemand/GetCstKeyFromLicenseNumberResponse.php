<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCstKeyFromLicenseNumberResponse
{

    /**
     * @var GetCstKeyFromLicenseNumberResult $GetCstKeyFromLicenseNumberResult
     */
    protected $GetCstKeyFromLicenseNumberResult = null;

    /**
     * @param GetCstKeyFromLicenseNumberResult $GetCstKeyFromLicenseNumberResult
     */
    public function __construct($GetCstKeyFromLicenseNumberResult)
    {
      $this->GetCstKeyFromLicenseNumberResult = $GetCstKeyFromLicenseNumberResult;
    }

    /**
     * @return GetCstKeyFromLicenseNumberResult
     */
    public function getGetCstKeyFromLicenseNumberResult()
    {
      return $this->GetCstKeyFromLicenseNumberResult;
    }

    /**
     * @param GetCstKeyFromLicenseNumberResult $GetCstKeyFromLicenseNumberResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCstKeyFromLicenseNumberResponse
     */
    public function setGetCstKeyFromLicenseNumberResult($GetCstKeyFromLicenseNumberResult)
    {
      $this->GetCstKeyFromLicenseNumberResult = $GetCstKeyFromLicenseNumberResult;
      return $this;
    }

}
