<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetRegistrantsByKeyResponse
{

    /**
     * @var GetRegistrantsByKeyResult $GetRegistrantsByKeyResult
     */
    protected $GetRegistrantsByKeyResult = null;

    /**
     * @param GetRegistrantsByKeyResult $GetRegistrantsByKeyResult
     */
    public function __construct($GetRegistrantsByKeyResult)
    {
      $this->GetRegistrantsByKeyResult = $GetRegistrantsByKeyResult;
    }

    /**
     * @return GetRegistrantsByKeyResult
     */
    public function getGetRegistrantsByKeyResult()
    {
      return $this->GetRegistrantsByKeyResult;
    }

    /**
     * @param GetRegistrantsByKeyResult $GetRegistrantsByKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetRegistrantsByKeyResponse
     */
    public function setGetRegistrantsByKeyResult($GetRegistrantsByKeyResult)
    {
      $this->GetRegistrantsByKeyResult = $GetRegistrantsByKeyResult;
      return $this;
    }

}
