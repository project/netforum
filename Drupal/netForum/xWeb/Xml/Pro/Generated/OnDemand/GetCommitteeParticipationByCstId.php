<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCommitteeParticipationByCstId
{

    /**
     * @var string $szCstId
     */
    protected $szCstId = null;

    /**
     * @param string $szCstId
     */
    public function __construct($szCstId)
    {
      $this->szCstId = $szCstId;
    }

    /**
     * @return string
     */
    public function getSzCstId()
    {
      return $this->szCstId;
    }

    /**
     * @param string $szCstId
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCommitteeParticipationByCstId
     */
    public function setSzCstId($szCstId)
    {
      $this->szCstId = $szCstId;
      return $this;
    }

}
