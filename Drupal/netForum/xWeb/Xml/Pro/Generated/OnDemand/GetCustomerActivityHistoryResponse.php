<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerActivityHistoryResponse
{

    /**
     * @var GetCustomerActivityHistoryResult $GetCustomerActivityHistoryResult
     */
    protected $GetCustomerActivityHistoryResult = null;

    /**
     * @param GetCustomerActivityHistoryResult $GetCustomerActivityHistoryResult
     */
    public function __construct($GetCustomerActivityHistoryResult)
    {
      $this->GetCustomerActivityHistoryResult = $GetCustomerActivityHistoryResult;
    }

    /**
     * @return GetCustomerActivityHistoryResult
     */
    public function getGetCustomerActivityHistoryResult()
    {
      return $this->GetCustomerActivityHistoryResult;
    }

    /**
     * @param GetCustomerActivityHistoryResult $GetCustomerActivityHistoryResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerActivityHistoryResponse
     */
    public function setGetCustomerActivityHistoryResult($GetCustomerActivityHistoryResult)
    {
      $this->GetCustomerActivityHistoryResult = $GetCustomerActivityHistoryResult;
      return $this;
    }

}
