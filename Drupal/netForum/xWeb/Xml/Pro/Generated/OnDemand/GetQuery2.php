<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetQuery2
{

    /**
     * @var string $szObjectName
     */
    protected $szObjectName = null;

    /**
     * @var string $szColumnList
     */
    protected $szColumnList = null;

    /**
     * @var string $szWhereClause
     */
    protected $szWhereClause = null;

    /**
     * @var string $szOrderBy
     */
    protected $szOrderBy = null;

    /**
     * @var string $szUserKey
     */
    protected $szUserKey = null;

    /**
     * @param string $szObjectName
     * @param string $szColumnList
     * @param string $szWhereClause
     * @param string $szOrderBy
     * @param string $szUserKey
     */
    public function __construct($szObjectName, $szColumnList, $szWhereClause, $szOrderBy, $szUserKey)
    {
      $this->szObjectName = $szObjectName;
      $this->szColumnList = $szColumnList;
      $this->szWhereClause = $szWhereClause;
      $this->szOrderBy = $szOrderBy;
      $this->szUserKey = $szUserKey;
    }

    /**
     * @return string
     */
    public function getSzObjectName()
    {
      return $this->szObjectName;
    }

    /**
     * @param string $szObjectName
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetQuery2
     */
    public function setSzObjectName($szObjectName)
    {
      $this->szObjectName = $szObjectName;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzColumnList()
    {
      return $this->szColumnList;
    }

    /**
     * @param string $szColumnList
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetQuery2
     */
    public function setSzColumnList($szColumnList)
    {
      $this->szColumnList = $szColumnList;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzWhereClause()
    {
      return $this->szWhereClause;
    }

    /**
     * @param string $szWhereClause
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetQuery2
     */
    public function setSzWhereClause($szWhereClause)
    {
      $this->szWhereClause = $szWhereClause;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzOrderBy()
    {
      return $this->szOrderBy;
    }

    /**
     * @param string $szOrderBy
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetQuery2
     */
    public function setSzOrderBy($szOrderBy)
    {
      $this->szOrderBy = $szOrderBy;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzUserKey()
    {
      return $this->szUserKey;
    }

    /**
     * @param string $szUserKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetQuery2
     */
    public function setSzUserKey($szUserKey)
    {
      $this->szUserKey = $szUserKey;
      return $this;
    }

}
