<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCommitteeListByCode
{

    /**
     * @var string $szCode
     */
    protected $szCode = null;

    /**
     * @var string $szActiveInactiveType
     */
    protected $szActiveInactiveType = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $szCode
     * @param string $szActiveInactiveType
     * @param string $szRecordDate
     */
    public function __construct($szCode, $szActiveInactiveType, $szRecordDate)
    {
      $this->szCode = $szCode;
      $this->szActiveInactiveType = $szActiveInactiveType;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getSzCode()
    {
      return $this->szCode;
    }

    /**
     * @param string $szCode
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCommitteeListByCode
     */
    public function setSzCode($szCode)
    {
      $this->szCode = $szCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzActiveInactiveType()
    {
      return $this->szActiveInactiveType;
    }

    /**
     * @param string $szActiveInactiveType
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCommitteeListByCode
     */
    public function setSzActiveInactiveType($szActiveInactiveType)
    {
      $this->szActiveInactiveType = $szActiveInactiveType;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCommitteeListByCode
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
