<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetPricesByProductKey
{

    /**
     * @var string $prdkey
     */
    protected $prdkey = null;

    /**
     * @param string $prdkey
     */
    public function __construct($prdkey)
    {
      $this->prdkey = $prdkey;
    }

    /**
     * @return string
     */
    public function getPrdkey()
    {
      return $this->prdkey;
    }

    /**
     * @param string $prdkey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetPricesByProductKey
     */
    public function setPrdkey($prdkey)
    {
      $this->prdkey = $prdkey;
      return $this;
    }

}
