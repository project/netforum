<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class NewOrganizationInformation
{

    /**
     * @var oNode $oNode
     */
    protected $oNode = null;

    /**
     * @param oNode $oNode
     */
    public function __construct($oNode)
    {
      $this->oNode = $oNode;
    }

    /**
     * @return oNode
     */
    public function getONode()
    {
      return $this->oNode;
    }

    /**
     * @param oNode $oNode
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\NewOrganizationInformation
     */
    public function setONode($oNode)
    {
      $this->oNode = $oNode;
      return $this;
    }

}
