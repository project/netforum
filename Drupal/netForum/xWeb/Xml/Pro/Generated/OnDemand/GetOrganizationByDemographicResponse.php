<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetOrganizationByDemographicResponse
{

    /**
     * @var GetOrganizationByDemographicResult $GetOrganizationByDemographicResult
     */
    protected $GetOrganizationByDemographicResult = null;

    /**
     * @param GetOrganizationByDemographicResult $GetOrganizationByDemographicResult
     */
    public function __construct($GetOrganizationByDemographicResult)
    {
      $this->GetOrganizationByDemographicResult = $GetOrganizationByDemographicResult;
    }

    /**
     * @return GetOrganizationByDemographicResult
     */
    public function getGetOrganizationByDemographicResult()
    {
      return $this->GetOrganizationByDemographicResult;
    }

    /**
     * @param GetOrganizationByDemographicResult $GetOrganizationByDemographicResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetOrganizationByDemographicResponse
     */
    public function setGetOrganizationByDemographicResult($GetOrganizationByDemographicResult)
    {
      $this->GetOrganizationByDemographicResult = $GetOrganizationByDemographicResult;
      return $this;
    }

}
