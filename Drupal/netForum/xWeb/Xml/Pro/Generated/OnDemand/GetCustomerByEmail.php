<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByEmail
{

    /**
     * @var string $szEmailAddress
     */
    protected $szEmailAddress = null;

    /**
     * @param string $szEmailAddress
     */
    public function __construct($szEmailAddress)
    {
      $this->szEmailAddress = $szEmailAddress;
    }

    /**
     * @return string
     */
    public function getSzEmailAddress()
    {
      return $this->szEmailAddress;
    }

    /**
     * @param string $szEmailAddress
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByEmail
     */
    public function setSzEmailAddress($szEmailAddress)
    {
      $this->szEmailAddress = $szEmailAddress;
      return $this;
    }

}
