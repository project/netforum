<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetPropertyCategoriesResponse
{

    /**
     * @var GetPropertyCategoriesResult $GetPropertyCategoriesResult
     */
    protected $GetPropertyCategoriesResult = null;

    /**
     * @param GetPropertyCategoriesResult $GetPropertyCategoriesResult
     */
    public function __construct($GetPropertyCategoriesResult)
    {
      $this->GetPropertyCategoriesResult = $GetPropertyCategoriesResult;
    }

    /**
     * @return GetPropertyCategoriesResult
     */
    public function getGetPropertyCategoriesResult()
    {
      return $this->GetPropertyCategoriesResult;
    }

    /**
     * @param GetPropertyCategoriesResult $GetPropertyCategoriesResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetPropertyCategoriesResponse
     */
    public function setGetPropertyCategoriesResult($GetPropertyCategoriesResult)
    {
      $this->GetPropertyCategoriesResult = $GetPropertyCategoriesResult;
      return $this;
    }

}
