<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerEvent
{

    /**
     * @var string $szCstKey
     */
    protected $szCstKey = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $szCstKey
     * @param string $szRecordDate
     */
    public function __construct($szCstKey, $szRecordDate)
    {
      $this->szCstKey = $szCstKey;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getSzCstKey()
    {
      return $this->szCstKey;
    }

    /**
     * @param string $szCstKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerEvent
     */
    public function setSzCstKey($szCstKey)
    {
      $this->szCstKey = $szCstKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerEvent
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
