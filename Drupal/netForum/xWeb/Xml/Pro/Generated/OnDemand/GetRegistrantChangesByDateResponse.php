<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetRegistrantChangesByDateResponse
{

    /**
     * @var GetRegistrantChangesByDateResult $GetRegistrantChangesByDateResult
     */
    protected $GetRegistrantChangesByDateResult = null;

    /**
     * @param GetRegistrantChangesByDateResult $GetRegistrantChangesByDateResult
     */
    public function __construct($GetRegistrantChangesByDateResult)
    {
      $this->GetRegistrantChangesByDateResult = $GetRegistrantChangesByDateResult;
    }

    /**
     * @return GetRegistrantChangesByDateResult
     */
    public function getGetRegistrantChangesByDateResult()
    {
      return $this->GetRegistrantChangesByDateResult;
    }

    /**
     * @param GetRegistrantChangesByDateResult $GetRegistrantChangesByDateResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetRegistrantChangesByDateResponse
     */
    public function setGetRegistrantChangesByDateResult($GetRegistrantChangesByDateResult)
    {
      $this->GetRegistrantChangesByDateResult = $GetRegistrantChangesByDateResult;
      return $this;
    }

}
