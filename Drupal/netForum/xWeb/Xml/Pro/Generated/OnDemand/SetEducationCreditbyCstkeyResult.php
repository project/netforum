<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class SetEducationCreditbyCstkeyResult
{

    /**
     * @var string $any
     */
    protected $any = null;

    /**
     * @param string $any
     */
    public function __construct($any)
    {
      $this->any = $any;
    }

    /**
     * @return string
     */
    public function getAny()
    {
      return $this->any;
    }

    /**
     * @param string $any
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetEducationCreditbyCstkeyResult
     */
    public function setAny($any)
    {
      $this->any = $any;
      return $this;
    }

}
