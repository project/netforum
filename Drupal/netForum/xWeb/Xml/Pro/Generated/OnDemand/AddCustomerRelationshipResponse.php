<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class AddCustomerRelationshipResponse
{

    /**
     * @var AddCustomerRelationshipResult $AddCustomerRelationshipResult
     */
    protected $AddCustomerRelationshipResult = null;

    /**
     * @param AddCustomerRelationshipResult $AddCustomerRelationshipResult
     */
    public function __construct($AddCustomerRelationshipResult)
    {
      $this->AddCustomerRelationshipResult = $AddCustomerRelationshipResult;
    }

    /**
     * @return AddCustomerRelationshipResult
     */
    public function getAddCustomerRelationshipResult()
    {
      return $this->AddCustomerRelationshipResult;
    }

    /**
     * @param AddCustomerRelationshipResult $AddCustomerRelationshipResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\AddCustomerRelationshipResponse
     */
    public function setAddCustomerRelationshipResult($AddCustomerRelationshipResult)
    {
      $this->AddCustomerRelationshipResult = $AddCustomerRelationshipResult;
      return $this;
    }

}
