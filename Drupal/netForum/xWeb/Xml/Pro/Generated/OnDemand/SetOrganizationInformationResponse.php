<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class SetOrganizationInformationResponse
{

    /**
     * @var SetOrganizationInformationResult $SetOrganizationInformationResult
     */
    protected $SetOrganizationInformationResult = null;

    /**
     * @param SetOrganizationInformationResult $SetOrganizationInformationResult
     */
    public function __construct($SetOrganizationInformationResult)
    {
      $this->SetOrganizationInformationResult = $SetOrganizationInformationResult;
    }

    /**
     * @return SetOrganizationInformationResult
     */
    public function getSetOrganizationInformationResult()
    {
      return $this->SetOrganizationInformationResult;
    }

    /**
     * @param SetOrganizationInformationResult $SetOrganizationInformationResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetOrganizationInformationResponse
     */
    public function setSetOrganizationInformationResult($SetOrganizationInformationResult)
    {
      $this->SetOrganizationInformationResult = $SetOrganizationInformationResult;
      return $this;
    }

}
