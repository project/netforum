<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class SetCustomerRelationship
{

    /**
     * @var string $pxcKey
     */
    protected $pxcKey = null;

    /**
     * @var string $startDate
     */
    protected $startDate = null;

    /**
     * @var string $endDate
     */
    protected $endDate = null;

    /**
     * @var boolean $bDeleteFlag
     */
    protected $bDeleteFlag = null;

    /**
     * @param string $pxcKey
     * @param string $startDate
     * @param string $endDate
     * @param boolean $bDeleteFlag
     */
    public function __construct($pxcKey, $startDate, $endDate, $bDeleteFlag)
    {
      $this->pxcKey = $pxcKey;
      $this->startDate = $startDate;
      $this->endDate = $endDate;
      $this->bDeleteFlag = $bDeleteFlag;
    }

    /**
     * @return string
     */
    public function getPxcKey()
    {
      return $this->pxcKey;
    }

    /**
     * @param string $pxcKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetCustomerRelationship
     */
    public function setPxcKey($pxcKey)
    {
      $this->pxcKey = $pxcKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
      return $this->startDate;
    }

    /**
     * @param string $startDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetCustomerRelationship
     */
    public function setStartDate($startDate)
    {
      $this->startDate = $startDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getEndDate()
    {
      return $this->endDate;
    }

    /**
     * @param string $endDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetCustomerRelationship
     */
    public function setEndDate($endDate)
    {
      $this->endDate = $endDate;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBDeleteFlag()
    {
      return $this->bDeleteFlag;
    }

    /**
     * @param boolean $bDeleteFlag
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetCustomerRelationship
     */
    public function setBDeleteFlag($bDeleteFlag)
    {
      $this->bDeleteFlag = $bDeleteFlag;
      return $this;
    }

}
