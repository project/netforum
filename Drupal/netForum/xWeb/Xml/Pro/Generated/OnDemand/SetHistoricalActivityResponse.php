<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class SetHistoricalActivityResponse
{

    /**
     * @var SetHistoricalActivityResult $SetHistoricalActivityResult
     */
    protected $SetHistoricalActivityResult = null;

    /**
     * @param SetHistoricalActivityResult $SetHistoricalActivityResult
     */
    public function __construct($SetHistoricalActivityResult)
    {
      $this->SetHistoricalActivityResult = $SetHistoricalActivityResult;
    }

    /**
     * @return SetHistoricalActivityResult
     */
    public function getSetHistoricalActivityResult()
    {
      return $this->SetHistoricalActivityResult;
    }

    /**
     * @param SetHistoricalActivityResult $SetHistoricalActivityResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetHistoricalActivityResponse
     */
    public function setSetHistoricalActivityResult($SetHistoricalActivityResult)
    {
      $this->SetHistoricalActivityResult = $SetHistoricalActivityResult;
      return $this;
    }

}
