<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetIndividualByPrimaryOrganizationResponse
{

    /**
     * @var GetIndividualByPrimaryOrganizationResult $GetIndividualByPrimaryOrganizationResult
     */
    protected $GetIndividualByPrimaryOrganizationResult = null;

    /**
     * @param GetIndividualByPrimaryOrganizationResult $GetIndividualByPrimaryOrganizationResult
     */
    public function __construct($GetIndividualByPrimaryOrganizationResult)
    {
      $this->GetIndividualByPrimaryOrganizationResult = $GetIndividualByPrimaryOrganizationResult;
    }

    /**
     * @return GetIndividualByPrimaryOrganizationResult
     */
    public function getGetIndividualByPrimaryOrganizationResult()
    {
      return $this->GetIndividualByPrimaryOrganizationResult;
    }

    /**
     * @param GetIndividualByPrimaryOrganizationResult $GetIndividualByPrimaryOrganizationResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetIndividualByPrimaryOrganizationResponse
     */
    public function setGetIndividualByPrimaryOrganizationResult($GetIndividualByPrimaryOrganizationResult)
    {
      $this->GetIndividualByPrimaryOrganizationResult = $GetIndividualByPrimaryOrganizationResult;
      return $this;
    }

}
