<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetProductListByTypeResponse
{

    /**
     * @var GetProductListByTypeResult $GetProductListByTypeResult
     */
    protected $GetProductListByTypeResult = null;

    /**
     * @param GetProductListByTypeResult $GetProductListByTypeResult
     */
    public function __construct($GetProductListByTypeResult)
    {
      $this->GetProductListByTypeResult = $GetProductListByTypeResult;
    }

    /**
     * @return GetProductListByTypeResult
     */
    public function getGetProductListByTypeResult()
    {
      return $this->GetProductListByTypeResult;
    }

    /**
     * @param GetProductListByTypeResult $GetProductListByTypeResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetProductListByTypeResponse
     */
    public function setGetProductListByTypeResult($GetProductListByTypeResult)
    {
      $this->GetProductListByTypeResult = $GetProductListByTypeResult;
      return $this;
    }

}
