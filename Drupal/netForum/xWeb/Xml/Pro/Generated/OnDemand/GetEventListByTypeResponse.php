<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetEventListByTypeResponse
{

    /**
     * @var GetEventListByTypeResult $GetEventListByTypeResult
     */
    protected $GetEventListByTypeResult = null;

    /**
     * @param GetEventListByTypeResult $GetEventListByTypeResult
     */
    public function __construct($GetEventListByTypeResult)
    {
      $this->GetEventListByTypeResult = $GetEventListByTypeResult;
    }

    /**
     * @return GetEventListByTypeResult
     */
    public function getGetEventListByTypeResult()
    {
      return $this->GetEventListByTypeResult;
    }

    /**
     * @param GetEventListByTypeResult $GetEventListByTypeResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetEventListByTypeResponse
     */
    public function setGetEventListByTypeResult($GetEventListByTypeResult)
    {
      $this->GetEventListByTypeResult = $GetEventListByTypeResult;
      return $this;
    }

}
