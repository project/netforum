<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class SetEducationCreditbyCstkey
{

    /**
     * @var string $customerKey
     */
    protected $customerKey = null;

    /**
     * @var string $educationCreditCode
     */
    protected $educationCreditCode = null;

    /**
     * @var string $earnedDate
     */
    protected $earnedDate = null;

    /**
     * @var string $creditValue
     */
    protected $creditValue = null;

    /**
     * @var string $earnedBy
     */
    protected $earnedBy = null;

    /**
     * @var string $canceledDate
     */
    protected $canceledDate = null;

    /**
     * @var string $groupCode
     */
    protected $groupCode = null;

    /**
     * @var string $courseCode
     */
    protected $courseCode = null;

    /**
     * @var string $courseNumber
     */
    protected $courseNumber = null;

    /**
     * @var string $sponsoringOrg
     */
    protected $sponsoringOrg = null;

    /**
     * @param string $customerKey
     * @param string $educationCreditCode
     * @param string $earnedDate
     * @param string $creditValue
     * @param string $earnedBy
     * @param string $canceledDate
     * @param string $groupCode
     * @param string $courseCode
     * @param string $courseNumber
     * @param string $sponsoringOrg
     */
    public function __construct($customerKey, $educationCreditCode, $earnedDate, $creditValue, $earnedBy, $canceledDate, $groupCode, $courseCode, $courseNumber, $sponsoringOrg)
    {
      $this->customerKey = $customerKey;
      $this->educationCreditCode = $educationCreditCode;
      $this->earnedDate = $earnedDate;
      $this->creditValue = $creditValue;
      $this->earnedBy = $earnedBy;
      $this->canceledDate = $canceledDate;
      $this->groupCode = $groupCode;
      $this->courseCode = $courseCode;
      $this->courseNumber = $courseNumber;
      $this->sponsoringOrg = $sponsoringOrg;
    }

    /**
     * @return string
     */
    public function getCustomerKey()
    {
      return $this->customerKey;
    }

    /**
     * @param string $customerKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetEducationCreditbyCstkey
     */
    public function setCustomerKey($customerKey)
    {
      $this->customerKey = $customerKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getEducationCreditCode()
    {
      return $this->educationCreditCode;
    }

    /**
     * @param string $educationCreditCode
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetEducationCreditbyCstkey
     */
    public function setEducationCreditCode($educationCreditCode)
    {
      $this->educationCreditCode = $educationCreditCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getEarnedDate()
    {
      return $this->earnedDate;
    }

    /**
     * @param string $earnedDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetEducationCreditbyCstkey
     */
    public function setEarnedDate($earnedDate)
    {
      $this->earnedDate = $earnedDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getCreditValue()
    {
      return $this->creditValue;
    }

    /**
     * @param string $creditValue
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetEducationCreditbyCstkey
     */
    public function setCreditValue($creditValue)
    {
      $this->creditValue = $creditValue;
      return $this;
    }

    /**
     * @return string
     */
    public function getEarnedBy()
    {
      return $this->earnedBy;
    }

    /**
     * @param string $earnedBy
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetEducationCreditbyCstkey
     */
    public function setEarnedBy($earnedBy)
    {
      $this->earnedBy = $earnedBy;
      return $this;
    }

    /**
     * @return string
     */
    public function getCanceledDate()
    {
      return $this->canceledDate;
    }

    /**
     * @param string $canceledDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetEducationCreditbyCstkey
     */
    public function setCanceledDate($canceledDate)
    {
      $this->canceledDate = $canceledDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getGroupCode()
    {
      return $this->groupCode;
    }

    /**
     * @param string $groupCode
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetEducationCreditbyCstkey
     */
    public function setGroupCode($groupCode)
    {
      $this->groupCode = $groupCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCourseCode()
    {
      return $this->courseCode;
    }

    /**
     * @param string $courseCode
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetEducationCreditbyCstkey
     */
    public function setCourseCode($courseCode)
    {
      $this->courseCode = $courseCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCourseNumber()
    {
      return $this->courseNumber;
    }

    /**
     * @param string $courseNumber
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetEducationCreditbyCstkey
     */
    public function setCourseNumber($courseNumber)
    {
      $this->courseNumber = $courseNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getSponsoringOrg()
    {
      return $this->sponsoringOrg;
    }

    /**
     * @param string $sponsoringOrg
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetEducationCreditbyCstkey
     */
    public function setSponsoringOrg($sponsoringOrg)
    {
      $this->sponsoringOrg = $sponsoringOrg;
      return $this;
    }

}
