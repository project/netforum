<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetEventFeesByEventKeyResponse
{

    /**
     * @var GetEventFeesByEventKeyResult $GetEventFeesByEventKeyResult
     */
    protected $GetEventFeesByEventKeyResult = null;

    /**
     * @param GetEventFeesByEventKeyResult $GetEventFeesByEventKeyResult
     */
    public function __construct($GetEventFeesByEventKeyResult)
    {
      $this->GetEventFeesByEventKeyResult = $GetEventFeesByEventKeyResult;
    }

    /**
     * @return GetEventFeesByEventKeyResult
     */
    public function getGetEventFeesByEventKeyResult()
    {
      return $this->GetEventFeesByEventKeyResult;
    }

    /**
     * @param GetEventFeesByEventKeyResult $GetEventFeesByEventKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetEventFeesByEventKeyResponse
     */
    public function setGetEventFeesByEventKeyResult($GetEventFeesByEventKeyResult)
    {
      $this->GetEventFeesByEventKeyResult = $GetEventFeesByEventKeyResult;
      return $this;
    }

}
