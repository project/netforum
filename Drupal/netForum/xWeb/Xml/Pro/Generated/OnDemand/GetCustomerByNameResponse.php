<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByNameResponse
{

    /**
     * @var GetCustomerByNameResult $GetCustomerByNameResult
     */
    protected $GetCustomerByNameResult = null;

    /**
     * @param GetCustomerByNameResult $GetCustomerByNameResult
     */
    public function __construct($GetCustomerByNameResult)
    {
      $this->GetCustomerByNameResult = $GetCustomerByNameResult;
    }

    /**
     * @return GetCustomerByNameResult
     */
    public function getGetCustomerByNameResult()
    {
      return $this->GetCustomerByNameResult;
    }

    /**
     * @param GetCustomerByNameResult $GetCustomerByNameResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByNameResponse
     */
    public function setGetCustomerByNameResult($GetCustomerByNameResult)
    {
      $this->GetCustomerByNameResult = $GetCustomerByNameResult;
      return $this;
    }

}
