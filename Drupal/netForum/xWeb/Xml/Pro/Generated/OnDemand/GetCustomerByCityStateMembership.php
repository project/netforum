<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByCityStateMembership
{

    /**
     * @var string $szState
     */
    protected $szState = null;

    /**
     * @var string $szCity
     */
    protected $szCity = null;

    /**
     * @var boolean $bIncludeIndividuals
     */
    protected $bIncludeIndividuals = null;

    /**
     * @var boolean $bIncludeOrganizations
     */
    protected $bIncludeOrganizations = null;

    /**
     * @var boolean $bMembersOnly
     */
    protected $bMembersOnly = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $szState
     * @param string $szCity
     * @param boolean $bIncludeIndividuals
     * @param boolean $bIncludeOrganizations
     * @param boolean $bMembersOnly
     * @param string $szRecordDate
     */
    public function __construct($szState, $szCity, $bIncludeIndividuals, $bIncludeOrganizations, $bMembersOnly, $szRecordDate)
    {
      $this->szState = $szState;
      $this->szCity = $szCity;
      $this->bIncludeIndividuals = $bIncludeIndividuals;
      $this->bIncludeOrganizations = $bIncludeOrganizations;
      $this->bMembersOnly = $bMembersOnly;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getSzState()
    {
      return $this->szState;
    }

    /**
     * @param string $szState
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByCityStateMembership
     */
    public function setSzState($szState)
    {
      $this->szState = $szState;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzCity()
    {
      return $this->szCity;
    }

    /**
     * @param string $szCity
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByCityStateMembership
     */
    public function setSzCity($szCity)
    {
      $this->szCity = $szCity;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBIncludeIndividuals()
    {
      return $this->bIncludeIndividuals;
    }

    /**
     * @param boolean $bIncludeIndividuals
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByCityStateMembership
     */
    public function setBIncludeIndividuals($bIncludeIndividuals)
    {
      $this->bIncludeIndividuals = $bIncludeIndividuals;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBIncludeOrganizations()
    {
      return $this->bIncludeOrganizations;
    }

    /**
     * @param boolean $bIncludeOrganizations
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByCityStateMembership
     */
    public function setBIncludeOrganizations($bIncludeOrganizations)
    {
      $this->bIncludeOrganizations = $bIncludeOrganizations;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBMembersOnly()
    {
      return $this->bMembersOnly;
    }

    /**
     * @param boolean $bMembersOnly
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByCityStateMembership
     */
    public function setBMembersOnly($bMembersOnly)
    {
      $this->bMembersOnly = $bMembersOnly;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByCityStateMembership
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
