<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class OrganizationByTypeResult
{

    /**
     * @var string $org_cst_key
     */
    protected $org_cst_key = null;

    /**
     * @var string $org_name
     */
    protected $org_name = null;

    /**
     * @var string $org_ogt_code
     */
    protected $org_ogt_code = null;

    /**
     * @var string $cst_id
     */
    protected $cst_id = null;

    /**
     * @var boolean $cst_member_flag
     */
    protected $cst_member_flag = null;

    /**
     * @var boolean $cst_receives_benefits_flag
     */
    protected $cst_receives_benefits_flag = null;

    /**
     * @param boolean $cst_member_flag
     * @param boolean $cst_receives_benefits_flag
     */
    public function __construct($cst_member_flag, $cst_receives_benefits_flag)
    {
      $this->cst_member_flag = $cst_member_flag;
      $this->cst_receives_benefits_flag = $cst_receives_benefits_flag;
    }

    /**
     * @return string
     */
    public function getOrg_cst_key()
    {
      return $this->org_cst_key;
    }

    /**
     * @param string $org_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByTypeResult
     */
    public function setOrg_cst_key($org_cst_key)
    {
      $this->org_cst_key = $org_cst_key;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrg_name()
    {
      return $this->org_name;
    }

    /**
     * @param string $org_name
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByTypeResult
     */
    public function setOrg_name($org_name)
    {
      $this->org_name = $org_name;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrg_ogt_code()
    {
      return $this->org_ogt_code;
    }

    /**
     * @param string $org_ogt_code
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByTypeResult
     */
    public function setOrg_ogt_code($org_ogt_code)
    {
      $this->org_ogt_code = $org_ogt_code;
      return $this;
    }

    /**
     * @return string
     */
    public function getCst_id()
    {
      return $this->cst_id;
    }

    /**
     * @param string $cst_id
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByTypeResult
     */
    public function setCst_id($cst_id)
    {
      $this->cst_id = $cst_id;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCst_member_flag()
    {
      return $this->cst_member_flag;
    }

    /**
     * @param boolean $cst_member_flag
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByTypeResult
     */
    public function setCst_member_flag($cst_member_flag)
    {
      $this->cst_member_flag = $cst_member_flag;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCst_receives_benefits_flag()
    {
      return $this->cst_receives_benefits_flag;
    }

    /**
     * @param boolean $cst_receives_benefits_flag
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByTypeResult
     */
    public function setCst_receives_benefits_flag($cst_receives_benefits_flag)
    {
      $this->cst_receives_benefits_flag = $cst_receives_benefits_flag;
      return $this;
    }

}
