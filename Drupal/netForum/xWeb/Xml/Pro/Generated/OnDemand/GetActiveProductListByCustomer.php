<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetActiveProductListByCustomer
{

    /**
     * @var string $cstkey
     */
    protected $cstkey = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $cstkey
     * @param string $szRecordDate
     */
    public function __construct($cstkey, $szRecordDate)
    {
      $this->cstkey = $cstkey;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getCstkey()
    {
      return $this->cstkey;
    }

    /**
     * @param string $cstkey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetActiveProductListByCustomer
     */
    public function setCstkey($cstkey)
    {
      $this->cstkey = $cstkey;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetActiveProductListByCustomer
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
