<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByRadiusAddressTypeResponse
{

    /**
     * @var GetCustomerByRadiusAddressTypeResult $GetCustomerByRadiusAddressTypeResult
     */
    protected $GetCustomerByRadiusAddressTypeResult = null;

    /**
     * @param GetCustomerByRadiusAddressTypeResult $GetCustomerByRadiusAddressTypeResult
     */
    public function __construct($GetCustomerByRadiusAddressTypeResult)
    {
      $this->GetCustomerByRadiusAddressTypeResult = $GetCustomerByRadiusAddressTypeResult;
    }

    /**
     * @return GetCustomerByRadiusAddressTypeResult
     */
    public function getGetCustomerByRadiusAddressTypeResult()
    {
      return $this->GetCustomerByRadiusAddressTypeResult;
    }

    /**
     * @param GetCustomerByRadiusAddressTypeResult $GetCustomerByRadiusAddressTypeResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRadiusAddressTypeResponse
     */
    public function setGetCustomerByRadiusAddressTypeResult($GetCustomerByRadiusAddressTypeResult)
    {
      $this->GetCustomerByRadiusAddressTypeResult = $GetCustomerByRadiusAddressTypeResult;
      return $this;
    }

}
