<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetRelationshipsByIndiviualResponse
{

    /**
     * @var GetRelationshipsByIndiviualResult $GetRelationshipsByIndiviualResult
     */
    protected $GetRelationshipsByIndiviualResult = null;

    /**
     * @param GetRelationshipsByIndiviualResult $GetRelationshipsByIndiviualResult
     */
    public function __construct($GetRelationshipsByIndiviualResult)
    {
      $this->GetRelationshipsByIndiviualResult = $GetRelationshipsByIndiviualResult;
    }

    /**
     * @return GetRelationshipsByIndiviualResult
     */
    public function getGetRelationshipsByIndiviualResult()
    {
      return $this->GetRelationshipsByIndiviualResult;
    }

    /**
     * @param GetRelationshipsByIndiviualResult $GetRelationshipsByIndiviualResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetRelationshipsByIndiviualResponse
     */
    public function setGetRelationshipsByIndiviualResult($GetRelationshipsByIndiviualResult)
    {
      $this->GetRelationshipsByIndiviualResult = $GetRelationshipsByIndiviualResult;
      return $this;
    }

}
