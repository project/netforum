<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetMultiSelectValuesResponse
{

    /**
     * @var GetMultiSelectValuesResult $GetMultiSelectValuesResult
     */
    protected $GetMultiSelectValuesResult = null;

    /**
     * @param GetMultiSelectValuesResult $GetMultiSelectValuesResult
     */
    public function __construct($GetMultiSelectValuesResult)
    {
      $this->GetMultiSelectValuesResult = $GetMultiSelectValuesResult;
    }

    /**
     * @return GetMultiSelectValuesResult
     */
    public function getGetMultiSelectValuesResult()
    {
      return $this->GetMultiSelectValuesResult;
    }

    /**
     * @param GetMultiSelectValuesResult $GetMultiSelectValuesResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetMultiSelectValuesResponse
     */
    public function setGetMultiSelectValuesResult($GetMultiSelectValuesResult)
    {
      $this->GetMultiSelectValuesResult = $GetMultiSelectValuesResult;
      return $this;
    }

}
