<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class SetCustomerRelationshipResponse
{

    /**
     * @var SetCustomerRelationshipResult $SetCustomerRelationshipResult
     */
    protected $SetCustomerRelationshipResult = null;

    /**
     * @param SetCustomerRelationshipResult $SetCustomerRelationshipResult
     */
    public function __construct($SetCustomerRelationshipResult)
    {
      $this->SetCustomerRelationshipResult = $SetCustomerRelationshipResult;
    }

    /**
     * @return SetCustomerRelationshipResult
     */
    public function getSetCustomerRelationshipResult()
    {
      return $this->SetCustomerRelationshipResult;
    }

    /**
     * @param SetCustomerRelationshipResult $SetCustomerRelationshipResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetCustomerRelationshipResponse
     */
    public function setSetCustomerRelationshipResult($SetCustomerRelationshipResult)
    {
      $this->SetCustomerRelationshipResult = $SetCustomerRelationshipResult;
      return $this;
    }

}
