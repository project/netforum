<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetIndividualByPrimaryOrganization
{

    /**
     * @var string $organizationKey
     */
    protected $organizationKey = null;

    /**
     * @var boolean $bMembersOnly
     */
    protected $bMembersOnly = null;

    /**
     * @var string $recordDate
     */
    protected $recordDate = null;

    /**
     * @param string $organizationKey
     * @param boolean $bMembersOnly
     * @param string $recordDate
     */
    public function __construct($organizationKey, $bMembersOnly, $recordDate)
    {
      $this->organizationKey = $organizationKey;
      $this->bMembersOnly = $bMembersOnly;
      $this->recordDate = $recordDate;
    }

    /**
     * @return string
     */
    public function getOrganizationKey()
    {
      return $this->organizationKey;
    }

    /**
     * @param string $organizationKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetIndividualByPrimaryOrganization
     */
    public function setOrganizationKey($organizationKey)
    {
      $this->organizationKey = $organizationKey;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBMembersOnly()
    {
      return $this->bMembersOnly;
    }

    /**
     * @param boolean $bMembersOnly
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetIndividualByPrimaryOrganization
     */
    public function setBMembersOnly($bMembersOnly)
    {
      $this->bMembersOnly = $bMembersOnly;
      return $this;
    }

    /**
     * @return string
     */
    public function getRecordDate()
    {
      return $this->recordDate;
    }

    /**
     * @param string $recordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetIndividualByPrimaryOrganization
     */
    public function setRecordDate($recordDate)
    {
      $this->recordDate = $recordDate;
      return $this;
    }

}
