<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetIndividualChangesByDate
{

    /**
     * @var string $szStartDate
     */
    protected $szStartDate = null;

    /**
     * @var string $szEndDate
     */
    protected $szEndDate = null;

    /**
     * @param string $szStartDate
     * @param string $szEndDate
     */
    public function __construct($szStartDate, $szEndDate)
    {
      $this->szStartDate = $szStartDate;
      $this->szEndDate = $szEndDate;
    }

    /**
     * @return string
     */
    public function getSzStartDate()
    {
      return $this->szStartDate;
    }

    /**
     * @param string $szStartDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetIndividualChangesByDate
     */
    public function setSzStartDate($szStartDate)
    {
      $this->szStartDate = $szStartDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzEndDate()
    {
      return $this->szEndDate;
    }

    /**
     * @param string $szEndDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetIndividualChangesByDate
     */
    public function setSzEndDate($szEndDate)
    {
      $this->szEndDate = $szEndDate;
      return $this;
    }

}
