<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByKeyResponse
{

    /**
     * @var GetCustomerByKeyResult $GetCustomerByKeyResult
     */
    protected $GetCustomerByKeyResult = null;

    /**
     * @param GetCustomerByKeyResult $GetCustomerByKeyResult
     */
    public function __construct($GetCustomerByKeyResult)
    {
      $this->GetCustomerByKeyResult = $GetCustomerByKeyResult;
    }

    /**
     * @return GetCustomerByKeyResult
     */
    public function getGetCustomerByKeyResult()
    {
      return $this->GetCustomerByKeyResult;
    }

    /**
     * @param GetCustomerByKeyResult $GetCustomerByKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByKeyResponse
     */
    public function setGetCustomerByKeyResult($GetCustomerByKeyResult)
    {
      $this->GetCustomerByKeyResult = $GetCustomerByKeyResult;
      return $this;
    }

}
