<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class OrganizationByPrimaryInd
{

    /**
     * @var string $cst_id
     */
    protected $cst_id = null;

    /**
     * @var string $org_cst_key
     */
    protected $org_cst_key = null;

    /**
     * @var string $org_sort_name
     */
    protected $org_sort_name = null;

    /**
     * @var string $primary_ind_cst_key
     */
    protected $primary_ind_cst_key = null;

    /**
     * @var string $primary_ind_sort_name
     */
    protected $primary_ind_sort_name = null;

    /**
     * @var string $org_adr_line1
     */
    protected $org_adr_line1 = null;

    /**
     * @var string $org_adr_line2
     */
    protected $org_adr_line2 = null;

    /**
     * @var string $org_adr_line3
     */
    protected $org_adr_line3 = null;

    /**
     * @var string $org_adr_city
     */
    protected $org_adr_city = null;

    /**
     * @var string $org_adr_state
     */
    protected $org_adr_state = null;

    /**
     * @var string $org_adr_post_code
     */
    protected $org_adr_post_code = null;

    /**
     * @var string $org_adr_country
     */
    protected $org_adr_country = null;

    /**
     * @var string $org_phn_number
     */
    protected $org_phn_number = null;

    /**
     * @var string $org_add_date
     */
    protected $org_add_date = null;

    /**
     * @var string $org_add_user
     */
    protected $org_add_user = null;

    /**
     * @var string $org_change_date
     */
    protected $org_change_date = null;

    /**
     * @var string $org_change_user
     */
    protected $org_change_user = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCst_id()
    {
      return $this->cst_id;
    }

    /**
     * @param string $cst_id
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByPrimaryInd
     */
    public function setCst_id($cst_id)
    {
      $this->cst_id = $cst_id;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrg_cst_key()
    {
      return $this->org_cst_key;
    }

    /**
     * @param string $org_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByPrimaryInd
     */
    public function setOrg_cst_key($org_cst_key)
    {
      $this->org_cst_key = $org_cst_key;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrg_sort_name()
    {
      return $this->org_sort_name;
    }

    /**
     * @param string $org_sort_name
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByPrimaryInd
     */
    public function setOrg_sort_name($org_sort_name)
    {
      $this->org_sort_name = $org_sort_name;
      return $this;
    }

    /**
     * @return string
     */
    public function getPrimary_ind_cst_key()
    {
      return $this->primary_ind_cst_key;
    }

    /**
     * @param string $primary_ind_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByPrimaryInd
     */
    public function setPrimary_ind_cst_key($primary_ind_cst_key)
    {
      $this->primary_ind_cst_key = $primary_ind_cst_key;
      return $this;
    }

    /**
     * @return string
     */
    public function getPrimary_ind_sort_name()
    {
      return $this->primary_ind_sort_name;
    }

    /**
     * @param string $primary_ind_sort_name
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByPrimaryInd
     */
    public function setPrimary_ind_sort_name($primary_ind_sort_name)
    {
      $this->primary_ind_sort_name = $primary_ind_sort_name;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrg_adr_line1()
    {
      return $this->org_adr_line1;
    }

    /**
     * @param string $org_adr_line1
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByPrimaryInd
     */
    public function setOrg_adr_line1($org_adr_line1)
    {
      $this->org_adr_line1 = $org_adr_line1;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrg_adr_line2()
    {
      return $this->org_adr_line2;
    }

    /**
     * @param string $org_adr_line2
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByPrimaryInd
     */
    public function setOrg_adr_line2($org_adr_line2)
    {
      $this->org_adr_line2 = $org_adr_line2;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrg_adr_line3()
    {
      return $this->org_adr_line3;
    }

    /**
     * @param string $org_adr_line3
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByPrimaryInd
     */
    public function setOrg_adr_line3($org_adr_line3)
    {
      $this->org_adr_line3 = $org_adr_line3;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrg_adr_city()
    {
      return $this->org_adr_city;
    }

    /**
     * @param string $org_adr_city
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByPrimaryInd
     */
    public function setOrg_adr_city($org_adr_city)
    {
      $this->org_adr_city = $org_adr_city;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrg_adr_state()
    {
      return $this->org_adr_state;
    }

    /**
     * @param string $org_adr_state
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByPrimaryInd
     */
    public function setOrg_adr_state($org_adr_state)
    {
      $this->org_adr_state = $org_adr_state;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrg_adr_post_code()
    {
      return $this->org_adr_post_code;
    }

    /**
     * @param string $org_adr_post_code
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByPrimaryInd
     */
    public function setOrg_adr_post_code($org_adr_post_code)
    {
      $this->org_adr_post_code = $org_adr_post_code;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrg_adr_country()
    {
      return $this->org_adr_country;
    }

    /**
     * @param string $org_adr_country
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByPrimaryInd
     */
    public function setOrg_adr_country($org_adr_country)
    {
      $this->org_adr_country = $org_adr_country;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrg_phn_number()
    {
      return $this->org_phn_number;
    }

    /**
     * @param string $org_phn_number
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByPrimaryInd
     */
    public function setOrg_phn_number($org_phn_number)
    {
      $this->org_phn_number = $org_phn_number;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrg_add_date()
    {
      return $this->org_add_date;
    }

    /**
     * @param string $org_add_date
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByPrimaryInd
     */
    public function setOrg_add_date($org_add_date)
    {
      $this->org_add_date = $org_add_date;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrg_add_user()
    {
      return $this->org_add_user;
    }

    /**
     * @param string $org_add_user
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByPrimaryInd
     */
    public function setOrg_add_user($org_add_user)
    {
      $this->org_add_user = $org_add_user;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrg_change_date()
    {
      return $this->org_change_date;
    }

    /**
     * @param string $org_change_date
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByPrimaryInd
     */
    public function setOrg_change_date($org_change_date)
    {
      $this->org_change_date = $org_change_date;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrg_change_user()
    {
      return $this->org_change_user;
    }

    /**
     * @param string $org_change_user
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\OrganizationByPrimaryInd
     */
    public function setOrg_change_user($org_change_user)
    {
      $this->org_change_user = $org_change_user;
      return $this;
    }

}
