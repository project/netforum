<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetMembershipsByCustomerResponse
{

    /**
     * @var GetMembershipsByCustomerResult $GetMembershipsByCustomerResult
     */
    protected $GetMembershipsByCustomerResult = null;

    /**
     * @param GetMembershipsByCustomerResult $GetMembershipsByCustomerResult
     */
    public function __construct($GetMembershipsByCustomerResult)
    {
      $this->GetMembershipsByCustomerResult = $GetMembershipsByCustomerResult;
    }

    /**
     * @return GetMembershipsByCustomerResult
     */
    public function getGetMembershipsByCustomerResult()
    {
      return $this->GetMembershipsByCustomerResult;
    }

    /**
     * @param GetMembershipsByCustomerResult $GetMembershipsByCustomerResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetMembershipsByCustomerResponse
     */
    public function setGetMembershipsByCustomerResult($GetMembershipsByCustomerResult)
    {
      $this->GetMembershipsByCustomerResult = $GetMembershipsByCustomerResult;
      return $this;
    }

}
