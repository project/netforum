<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetMembershipChangesByDateResponse
{

    /**
     * @var GetMembershipChangesByDateResult $GetMembershipChangesByDateResult
     */
    protected $GetMembershipChangesByDateResult = null;

    /**
     * @param GetMembershipChangesByDateResult $GetMembershipChangesByDateResult
     */
    public function __construct($GetMembershipChangesByDateResult)
    {
      $this->GetMembershipChangesByDateResult = $GetMembershipChangesByDateResult;
    }

    /**
     * @return GetMembershipChangesByDateResult
     */
    public function getGetMembershipChangesByDateResult()
    {
      return $this->GetMembershipChangesByDateResult;
    }

    /**
     * @param GetMembershipChangesByDateResult $GetMembershipChangesByDateResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetMembershipChangesByDateResponse
     */
    public function setGetMembershipChangesByDateResult($GetMembershipChangesByDateResult)
    {
      $this->GetMembershipChangesByDateResult = $GetMembershipChangesByDateResult;
      return $this;
    }

}
