<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerTicketedSessionsByRegKey
{

    /**
     * @var string $szKey
     */
    protected $szKey = null;

    /**
     * @param string $szKey
     */
    public function __construct($szKey)
    {
      $this->szKey = $szKey;
    }

    /**
     * @return string
     */
    public function getSzKey()
    {
      return $this->szKey;
    }

    /**
     * @param string $szKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerTicketedSessionsByRegKey
     */
    public function setSzKey($szKey)
    {
      $this->szKey = $szKey;
      return $this;
    }

}
