<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetActiveEventListByDate
{

    /**
     * @var string $EventStartDate
     */
    protected $EventStartDate = null;

    /**
     * @var string $EventEndDate
     */
    protected $EventEndDate = null;

    /**
     * @var boolean $bActiveOnly
     */
    protected $bActiveOnly = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $EventStartDate
     * @param string $EventEndDate
     * @param boolean $bActiveOnly
     * @param string $szRecordDate
     */
    public function __construct($EventStartDate, $EventEndDate, $bActiveOnly, $szRecordDate)
    {
      $this->EventStartDate = $EventStartDate;
      $this->EventEndDate = $EventEndDate;
      $this->bActiveOnly = $bActiveOnly;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getEventStartDate()
    {
      return $this->EventStartDate;
    }

    /**
     * @param string $EventStartDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetActiveEventListByDate
     */
    public function setEventStartDate($EventStartDate)
    {
      $this->EventStartDate = $EventStartDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getEventEndDate()
    {
      return $this->EventEndDate;
    }

    /**
     * @param string $EventEndDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetActiveEventListByDate
     */
    public function setEventEndDate($EventEndDate)
    {
      $this->EventEndDate = $EventEndDate;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBActiveOnly()
    {
      return $this->bActiveOnly;
    }

    /**
     * @param boolean $bActiveOnly
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetActiveEventListByDate
     */
    public function setBActiveOnly($bActiveOnly)
    {
      $this->bActiveOnly = $bActiveOnly;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetActiveEventListByDate
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
