<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetProductBalancesResponse
{

    /**
     * @var GetProductBalancesResult $GetProductBalancesResult
     */
    protected $GetProductBalancesResult = null;

    /**
     * @param GetProductBalancesResult $GetProductBalancesResult
     */
    public function __construct($GetProductBalancesResult)
    {
      $this->GetProductBalancesResult = $GetProductBalancesResult;
    }

    /**
     * @return GetProductBalancesResult
     */
    public function getGetProductBalancesResult()
    {
      return $this->GetProductBalancesResult;
    }

    /**
     * @param GetProductBalancesResult $GetProductBalancesResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetProductBalancesResponse
     */
    public function setGetProductBalancesResult($GetProductBalancesResult)
    {
      $this->GetProductBalancesResult = $GetProductBalancesResult;
      return $this;
    }

}
