<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCommitteeByKeyResponse
{

    /**
     * @var GetCommitteeByKeyResult $GetCommitteeByKeyResult
     */
    protected $GetCommitteeByKeyResult = null;

    /**
     * @param GetCommitteeByKeyResult $GetCommitteeByKeyResult
     */
    public function __construct($GetCommitteeByKeyResult)
    {
      $this->GetCommitteeByKeyResult = $GetCommitteeByKeyResult;
    }

    /**
     * @return GetCommitteeByKeyResult
     */
    public function getGetCommitteeByKeyResult()
    {
      return $this->GetCommitteeByKeyResult;
    }

    /**
     * @param GetCommitteeByKeyResult $GetCommitteeByKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCommitteeByKeyResponse
     */
    public function setGetCommitteeByKeyResult($GetCommitteeByKeyResult)
    {
      $this->GetCommitteeByKeyResult = $GetCommitteeByKeyResult;
      return $this;
    }

}
