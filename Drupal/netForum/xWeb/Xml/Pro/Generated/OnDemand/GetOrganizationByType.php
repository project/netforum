<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetOrganizationByType
{

    /**
     * @var string $typeCode
     */
    protected $typeCode = null;

    /**
     * @var boolean $bMembersOnly
     */
    protected $bMembersOnly = null;

    /**
     * @var string $startIndex
     */
    protected $startIndex = null;

    /**
     * @param string $typeCode
     * @param boolean $bMembersOnly
     * @param string $startIndex
     */
    public function __construct($typeCode, $bMembersOnly, $startIndex)
    {
      $this->typeCode = $typeCode;
      $this->bMembersOnly = $bMembersOnly;
      $this->startIndex = $startIndex;
    }

    /**
     * @return string
     */
    public function getTypeCode()
    {
      return $this->typeCode;
    }

    /**
     * @param string $typeCode
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetOrganizationByType
     */
    public function setTypeCode($typeCode)
    {
      $this->typeCode = $typeCode;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBMembersOnly()
    {
      return $this->bMembersOnly;
    }

    /**
     * @param boolean $bMembersOnly
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetOrganizationByType
     */
    public function setBMembersOnly($bMembersOnly)
    {
      $this->bMembersOnly = $bMembersOnly;
      return $this;
    }

    /**
     * @return string
     */
    public function getStartIndex()
    {
      return $this->startIndex;
    }

    /**
     * @param string $startIndex
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetOrganizationByType
     */
    public function setStartIndex($startIndex)
    {
      $this->startIndex = $startIndex;
      return $this;
    }

}
