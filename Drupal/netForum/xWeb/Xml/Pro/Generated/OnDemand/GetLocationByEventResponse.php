<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetLocationByEventResponse
{

    /**
     * @var GetLocationByEventResult $GetLocationByEventResult
     */
    protected $GetLocationByEventResult = null;

    /**
     * @param GetLocationByEventResult $GetLocationByEventResult
     */
    public function __construct($GetLocationByEventResult)
    {
      $this->GetLocationByEventResult = $GetLocationByEventResult;
    }

    /**
     * @return GetLocationByEventResult
     */
    public function getGetLocationByEventResult()
    {
      return $this->GetLocationByEventResult;
    }

    /**
     * @param GetLocationByEventResult $GetLocationByEventResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetLocationByEventResponse
     */
    public function setGetLocationByEventResult($GetLocationByEventResult)
    {
      $this->GetLocationByEventResult = $GetLocationByEventResult;
      return $this;
    }

}
