<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByMembershipTypeResponse
{

    /**
     * @var GetCustomerByMembershipTypeResult $GetCustomerByMembershipTypeResult
     */
    protected $GetCustomerByMembershipTypeResult = null;

    /**
     * @param GetCustomerByMembershipTypeResult $GetCustomerByMembershipTypeResult
     */
    public function __construct($GetCustomerByMembershipTypeResult)
    {
      $this->GetCustomerByMembershipTypeResult = $GetCustomerByMembershipTypeResult;
    }

    /**
     * @return GetCustomerByMembershipTypeResult
     */
    public function getGetCustomerByMembershipTypeResult()
    {
      return $this->GetCustomerByMembershipTypeResult;
    }

    /**
     * @param GetCustomerByMembershipTypeResult $GetCustomerByMembershipTypeResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByMembershipTypeResponse
     */
    public function setGetCustomerByMembershipTypeResult($GetCustomerByMembershipTypeResult)
    {
      $this->GetCustomerByMembershipTypeResult = $GetCustomerByMembershipTypeResult;
      return $this;
    }

}
