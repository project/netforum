<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetOrganizationChangesByDateResponse
{

    /**
     * @var GetOrganizationChangesByDateResult $GetOrganizationChangesByDateResult
     */
    protected $GetOrganizationChangesByDateResult = null;

    /**
     * @param GetOrganizationChangesByDateResult $GetOrganizationChangesByDateResult
     */
    public function __construct($GetOrganizationChangesByDateResult)
    {
      $this->GetOrganizationChangesByDateResult = $GetOrganizationChangesByDateResult;
    }

    /**
     * @return GetOrganizationChangesByDateResult
     */
    public function getGetOrganizationChangesByDateResult()
    {
      return $this->GetOrganizationChangesByDateResult;
    }

    /**
     * @param GetOrganizationChangesByDateResult $GetOrganizationChangesByDateResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetOrganizationChangesByDateResponse
     */
    public function setGetOrganizationChangesByDateResult($GetOrganizationChangesByDateResult)
    {
      $this->GetOrganizationChangesByDateResult = $GetOrganizationChangesByDateResult;
      return $this;
    }

}
