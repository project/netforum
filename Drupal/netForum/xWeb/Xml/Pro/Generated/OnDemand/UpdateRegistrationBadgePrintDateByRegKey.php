<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class UpdateRegistrationBadgePrintDateByRegKey
{

    /**
     * @var string $regKey
     */
    protected $regKey = null;

    /**
     * @var string $registrationBadgePrintDate
     */
    protected $registrationBadgePrintDate = null;

    /**
     * @param string $regKey
     * @param string $registrationBadgePrintDate
     */
    public function __construct($regKey, $registrationBadgePrintDate)
    {
      $this->regKey = $regKey;
      $this->registrationBadgePrintDate = $registrationBadgePrintDate;
    }

    /**
     * @return string
     */
    public function getRegKey()
    {
      return $this->regKey;
    }

    /**
     * @param string $regKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\UpdateRegistrationBadgePrintDateByRegKey
     */
    public function setRegKey($regKey)
    {
      $this->regKey = $regKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getRegistrationBadgePrintDate()
    {
      return $this->registrationBadgePrintDate;
    }

    /**
     * @param string $registrationBadgePrintDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\UpdateRegistrationBadgePrintDateByRegKey
     */
    public function setRegistrationBadgePrintDate($registrationBadgePrintDate)
    {
      $this->registrationBadgePrintDate = $registrationBadgePrintDate;
      return $this;
    }

}
