<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class AuthorizationToken
{

    /**
     * @var string $Token
     */
    protected $Token = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getToken()
    {
      return $this->Token;
    }

    /**
     * @param string $Token
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\AuthorizationToken
     */
    public function setToken($Token)
    {
      $this->Token = $Token;
      return $this;
    }

}
