<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class UpdateRegistrationBadgePrintDateByRegKeyResponse
{

    /**
     * @var UpdateRegistrationBadgePrintDateByRegKeyResult $UpdateRegistrationBadgePrintDateByRegKeyResult
     */
    protected $UpdateRegistrationBadgePrintDateByRegKeyResult = null;

    /**
     * @param UpdateRegistrationBadgePrintDateByRegKeyResult $UpdateRegistrationBadgePrintDateByRegKeyResult
     */
    public function __construct($UpdateRegistrationBadgePrintDateByRegKeyResult)
    {
      $this->UpdateRegistrationBadgePrintDateByRegKeyResult = $UpdateRegistrationBadgePrintDateByRegKeyResult;
    }

    /**
     * @return UpdateRegistrationBadgePrintDateByRegKeyResult
     */
    public function getUpdateRegistrationBadgePrintDateByRegKeyResult()
    {
      return $this->UpdateRegistrationBadgePrintDateByRegKeyResult;
    }

    /**
     * @param UpdateRegistrationBadgePrintDateByRegKeyResult $UpdateRegistrationBadgePrintDateByRegKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\UpdateRegistrationBadgePrintDateByRegKeyResponse
     */
    public function setUpdateRegistrationBadgePrintDateByRegKeyResult($UpdateRegistrationBadgePrintDateByRegKeyResult)
    {
      $this->UpdateRegistrationBadgePrintDateByRegKeyResult = $UpdateRegistrationBadgePrintDateByRegKeyResult;
      return $this;
    }

}
