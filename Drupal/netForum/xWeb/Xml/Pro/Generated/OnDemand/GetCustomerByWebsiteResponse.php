<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByWebsiteResponse
{

    /**
     * @var GetCustomerByWebsiteResult $GetCustomerByWebsiteResult
     */
    protected $GetCustomerByWebsiteResult = null;

    /**
     * @param GetCustomerByWebsiteResult $GetCustomerByWebsiteResult
     */
    public function __construct($GetCustomerByWebsiteResult)
    {
      $this->GetCustomerByWebsiteResult = $GetCustomerByWebsiteResult;
    }

    /**
     * @return GetCustomerByWebsiteResult
     */
    public function getGetCustomerByWebsiteResult()
    {
      return $this->GetCustomerByWebsiteResult;
    }

    /**
     * @param GetCustomerByWebsiteResult $GetCustomerByWebsiteResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByWebsiteResponse
     */
    public function setGetCustomerByWebsiteResult($GetCustomerByWebsiteResult)
    {
      $this->GetCustomerByWebsiteResult = $GetCustomerByWebsiteResult;
      return $this;
    }

}
