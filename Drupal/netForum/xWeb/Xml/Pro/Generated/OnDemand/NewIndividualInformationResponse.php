<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class NewIndividualInformationResponse
{

    /**
     * @var NewIndividualInformationResult $NewIndividualInformationResult
     */
    protected $NewIndividualInformationResult = null;

    /**
     * @param NewIndividualInformationResult $NewIndividualInformationResult
     */
    public function __construct($NewIndividualInformationResult)
    {
      $this->NewIndividualInformationResult = $NewIndividualInformationResult;
    }

    /**
     * @return NewIndividualInformationResult
     */
    public function getNewIndividualInformationResult()
    {
      return $this->NewIndividualInformationResult;
    }

    /**
     * @param NewIndividualInformationResult $NewIndividualInformationResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\NewIndividualInformationResponse
     */
    public function setNewIndividualInformationResult($NewIndividualInformationResult)
    {
      $this->NewIndividualInformationResult = $NewIndividualInformationResult;
      return $this;
    }

}
