<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetRegistrantsByKey
{

    /**
     * @var string $eventkey
     */
    protected $eventkey = null;

    /**
     * @var string $firstname
     */
    protected $firstname = null;

    /**
     * @var string $lastname
     */
    protected $lastname = null;

    /**
     * @var string $orgname
     */
    protected $orgname = null;

    /**
     * @var string $regid
     */
    protected $regid = null;

    /**
     * @param string $eventkey
     * @param string $firstname
     * @param string $lastname
     * @param string $orgname
     * @param string $regid
     */
    public function __construct($eventkey, $firstname, $lastname, $orgname, $regid)
    {
      $this->eventkey = $eventkey;
      $this->firstname = $firstname;
      $this->lastname = $lastname;
      $this->orgname = $orgname;
      $this->regid = $regid;
    }

    /**
     * @return string
     */
    public function getEventkey()
    {
      return $this->eventkey;
    }

    /**
     * @param string $eventkey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetRegistrantsByKey
     */
    public function setEventkey($eventkey)
    {
      $this->eventkey = $eventkey;
      return $this;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
      return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetRegistrantsByKey
     */
    public function setFirstname($firstname)
    {
      $this->firstname = $firstname;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
      return $this->lastname;
    }

    /**
     * @param string $lastname
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetRegistrantsByKey
     */
    public function setLastname($lastname)
    {
      $this->lastname = $lastname;
      return $this;
    }

    /**
     * @return string
     */
    public function getOrgname()
    {
      return $this->orgname;
    }

    /**
     * @param string $orgname
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetRegistrantsByKey
     */
    public function setOrgname($orgname)
    {
      $this->orgname = $orgname;
      return $this;
    }

    /**
     * @return string
     */
    public function getRegid()
    {
      return $this->regid;
    }

    /**
     * @param string $regid
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetRegistrantsByKey
     */
    public function setRegid($regid)
    {
      $this->regid = $regid;
      return $this;
    }

}
