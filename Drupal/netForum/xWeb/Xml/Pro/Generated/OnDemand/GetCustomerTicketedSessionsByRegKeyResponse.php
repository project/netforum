<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerTicketedSessionsByRegKeyResponse
{

    /**
     * @var GetCustomerTicketedSessionsByRegKeyResult $GetCustomerTicketedSessionsByRegKeyResult
     */
    protected $GetCustomerTicketedSessionsByRegKeyResult = null;

    /**
     * @param GetCustomerTicketedSessionsByRegKeyResult $GetCustomerTicketedSessionsByRegKeyResult
     */
    public function __construct($GetCustomerTicketedSessionsByRegKeyResult)
    {
      $this->GetCustomerTicketedSessionsByRegKeyResult = $GetCustomerTicketedSessionsByRegKeyResult;
    }

    /**
     * @return GetCustomerTicketedSessionsByRegKeyResult
     */
    public function getGetCustomerTicketedSessionsByRegKeyResult()
    {
      return $this->GetCustomerTicketedSessionsByRegKeyResult;
    }

    /**
     * @param GetCustomerTicketedSessionsByRegKeyResult $GetCustomerTicketedSessionsByRegKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerTicketedSessionsByRegKeyResponse
     */
    public function setGetCustomerTicketedSessionsByRegKeyResult($GetCustomerTicketedSessionsByRegKeyResult)
    {
      $this->GetCustomerTicketedSessionsByRegKeyResult = $GetCustomerTicketedSessionsByRegKeyResult;
      return $this;
    }

}
