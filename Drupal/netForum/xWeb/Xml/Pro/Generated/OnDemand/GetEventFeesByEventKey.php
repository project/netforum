<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetEventFeesByEventKey
{

    /**
     * @var string $szEventkey
     */
    protected $szEventkey = null;

    /**
     * @var string $szSessionKey
     */
    protected $szSessionKey = null;

    /**
     * @var string $szAvailableAsOfDate
     */
    protected $szAvailableAsOfDate = null;

    /**
     * @var boolean $bOnlineOnly
     */
    protected $bOnlineOnly = null;

    /**
     * @var boolean $bExcludeInactive
     */
    protected $bExcludeInactive = null;

    /**
     * @param string $szEventkey
     * @param string $szSessionKey
     * @param string $szAvailableAsOfDate
     * @param boolean $bOnlineOnly
     * @param boolean $bExcludeInactive
     */
    public function __construct($szEventkey, $szSessionKey, $szAvailableAsOfDate, $bOnlineOnly, $bExcludeInactive)
    {
      $this->szEventkey = $szEventkey;
      $this->szSessionKey = $szSessionKey;
      $this->szAvailableAsOfDate = $szAvailableAsOfDate;
      $this->bOnlineOnly = $bOnlineOnly;
      $this->bExcludeInactive = $bExcludeInactive;
    }

    /**
     * @return string
     */
    public function getSzEventkey()
    {
      return $this->szEventkey;
    }

    /**
     * @param string $szEventkey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetEventFeesByEventKey
     */
    public function setSzEventkey($szEventkey)
    {
      $this->szEventkey = $szEventkey;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzSessionKey()
    {
      return $this->szSessionKey;
    }

    /**
     * @param string $szSessionKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetEventFeesByEventKey
     */
    public function setSzSessionKey($szSessionKey)
    {
      $this->szSessionKey = $szSessionKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzAvailableAsOfDate()
    {
      return $this->szAvailableAsOfDate;
    }

    /**
     * @param string $szAvailableAsOfDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetEventFeesByEventKey
     */
    public function setSzAvailableAsOfDate($szAvailableAsOfDate)
    {
      $this->szAvailableAsOfDate = $szAvailableAsOfDate;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBOnlineOnly()
    {
      return $this->bOnlineOnly;
    }

    /**
     * @param boolean $bOnlineOnly
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetEventFeesByEventKey
     */
    public function setBOnlineOnly($bOnlineOnly)
    {
      $this->bOnlineOnly = $bOnlineOnly;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBExcludeInactive()
    {
      return $this->bExcludeInactive;
    }

    /**
     * @param boolean $bExcludeInactive
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetEventFeesByEventKey
     */
    public function setBExcludeInactive($bExcludeInactive)
    {
      $this->bExcludeInactive = $bExcludeInactive;
      return $this;
    }

}
