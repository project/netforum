<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetIndividualListByChangeDateRelationship
{

    /**
     * @var string $recordDate
     */
    protected $recordDate = null;

    /**
     * @var string $typeCode
     */
    protected $typeCode = null;

    /**
     * @param string $recordDate
     * @param string $typeCode
     */
    public function __construct($recordDate, $typeCode)
    {
      $this->recordDate = $recordDate;
      $this->typeCode = $typeCode;
    }

    /**
     * @return string
     */
    public function getRecordDate()
    {
      return $this->recordDate;
    }

    /**
     * @param string $recordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetIndividualListByChangeDateRelationship
     */
    public function setRecordDate($recordDate)
    {
      $this->recordDate = $recordDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getTypeCode()
    {
      return $this->typeCode;
    }

    /**
     * @param string $typeCode
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetIndividualListByChangeDateRelationship
     */
    public function setTypeCode($typeCode)
    {
      $this->typeCode = $typeCode;
      return $this;
    }

}
