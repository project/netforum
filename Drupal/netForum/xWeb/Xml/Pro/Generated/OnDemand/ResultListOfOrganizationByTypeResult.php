<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class ResultListOfOrganizationByTypeResult
{

    /**
     * @var OrganizationByTypeResult[] $Result
     */
    protected $Result = null;

    /**
     * @var string $userName
     */
    protected $userName = null;

    /**
     * @var int $recordReturn
     */
    protected $recordReturn = null;

    /**
     * @param string $userName
     * @param int $recordReturn
     */
    public function __construct($userName, $recordReturn)
    {
      $this->userName = $userName;
      $this->recordReturn = $recordReturn;
    }

    /**
     * @return OrganizationByTypeResult[]
     */
    public function getResult()
    {
      return $this->Result;
    }

    /**
     * @param OrganizationByTypeResult[] $Result
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\ResultListOfOrganizationByTypeResult
     */
    public function setResult(array $Result = null)
    {
      $this->Result = $Result;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
      return $this->userName;
    }

    /**
     * @param string $userName
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\ResultListOfOrganizationByTypeResult
     */
    public function setUserName($userName)
    {
      $this->userName = $userName;
      return $this;
    }

    /**
     * @return int
     */
    public function getRecordReturn()
    {
      return $this->recordReturn;
    }

    /**
     * @param int $recordReturn
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\ResultListOfOrganizationByTypeResult
     */
    public function setRecordReturn($recordReturn)
    {
      $this->recordReturn = $recordReturn;
      return $this;
    }

}
