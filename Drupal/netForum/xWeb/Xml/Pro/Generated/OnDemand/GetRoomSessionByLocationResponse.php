<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetRoomSessionByLocationResponse
{

    /**
     * @var GetRoomSessionByLocationResult $GetRoomSessionByLocationResult
     */
    protected $GetRoomSessionByLocationResult = null;

    /**
     * @param GetRoomSessionByLocationResult $GetRoomSessionByLocationResult
     */
    public function __construct($GetRoomSessionByLocationResult)
    {
      $this->GetRoomSessionByLocationResult = $GetRoomSessionByLocationResult;
    }

    /**
     * @return GetRoomSessionByLocationResult
     */
    public function getGetRoomSessionByLocationResult()
    {
      return $this->GetRoomSessionByLocationResult;
    }

    /**
     * @param GetRoomSessionByLocationResult $GetRoomSessionByLocationResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetRoomSessionByLocationResponse
     */
    public function setGetRoomSessionByLocationResult($GetRoomSessionByLocationResult)
    {
      $this->GetRoomSessionByLocationResult = $GetRoomSessionByLocationResult;
      return $this;
    }

}
