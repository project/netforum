<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByCityStateResponse
{

    /**
     * @var GetCustomerByCityStateResult $GetCustomerByCityStateResult
     */
    protected $GetCustomerByCityStateResult = null;

    /**
     * @param GetCustomerByCityStateResult $GetCustomerByCityStateResult
     */
    public function __construct($GetCustomerByCityStateResult)
    {
      $this->GetCustomerByCityStateResult = $GetCustomerByCityStateResult;
    }

    /**
     * @return GetCustomerByCityStateResult
     */
    public function getGetCustomerByCityStateResult()
    {
      return $this->GetCustomerByCityStateResult;
    }

    /**
     * @param GetCustomerByCityStateResult $GetCustomerByCityStateResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByCityStateResponse
     */
    public function setGetCustomerByCityStateResult($GetCustomerByCityStateResult)
    {
      $this->GetCustomerByCityStateResult = $GetCustomerByCityStateResult;
      return $this;
    }

}
