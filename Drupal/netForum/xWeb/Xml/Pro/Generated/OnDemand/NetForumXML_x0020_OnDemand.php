<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

use Drupal\netForum\xWeb\Xml\netForumSoapClient;


/**
 * netForum OnDemand Xml Web Service
 */
class NetForumXML_x0020_OnDemand extends netForumSoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'Authenticate' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\Authenticate',
      'AuthenticateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\AuthenticateResponse',
      'AuthorizationToken' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\AuthorizationToken',
      'GetFacadeXMLSchema' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetFacadeXMLSchema',
      'GetFacadeXMLSchemaResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetFacadeXMLSchemaResponse',
      'GetFacadeXMLSchemaResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetFacadeXMLSchemaResult',
      'GetFacadeObject' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetFacadeObject',
      'GetFacadeObjectResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetFacadeObjectResponse',
      'GetFacadeObjectResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetFacadeObjectResult',
      'GetQuery' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetQuery',
      'GetQueryResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetQueryResponse',
      'GetQueryResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetQueryResult',
      'GetQuery2' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetQuery2',
      'GetQuery2Response' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetQuery2Response',
      'GetQuery2Result' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetQuery2Result',
      'GetCustomerBalance' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerBalance',
      'GetCustomerBalanceResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerBalanceResponse',
      'GetCustomerBalanceResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerBalanceResult',
      'GetCustomerMembership' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerMembership',
      'GetCustomerMembershipResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerMembershipResponse',
      'GetCustomerMembershipResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerMembershipResult',
      'GetCustomerByZip' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByZip',
      'GetCustomerByZipResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByZipResponse',
      'GetCustomerByZipResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByZipResult',
      'GetCustomerByCityState' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByCityState',
      'GetCustomerByCityStateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByCityStateResponse',
      'GetCustomerByCityStateResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByCityStateResult',
      'GetCustomerByCityStateMembership' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByCityStateMembership',
      'GetCustomerByCityStateMembershipResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByCityStateMembershipResponse',
      'GetCustomerByCityStateMembershipResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByCityStateMembershipResult',
      'GetCustomerByRadius' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByRadius',
      'GetCustomerByRadiusResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByRadiusResponse',
      'GetCustomerByRadiusResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByRadiusResult',
      'GetCustomerByRadiusAddressType' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByRadiusAddressType',
      'GetCustomerByRadiusAddressTypeResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByRadiusAddressTypeResponse',
      'GetCustomerByRadiusAddressTypeResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByRadiusAddressTypeResult',
      'GetCustomerByRadiusLatLon' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByRadiusLatLon',
      'GetCustomerByRadiusLatLonResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByRadiusLatLonResponse',
      'GetCustomerByRadiusLatLonResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByRadiusLatLonResult',
      'GetCustomerByRecordDate' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByRecordDate',
      'GetCustomerByRecordDateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByRecordDateResponse',
      'GetCustomerByRecordDateResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByRecordDateResult',
      'GetCustomerByName' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByName',
      'GetCustomerByNameResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByNameResponse',
      'GetCustomerByNameResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByNameResult',
      'GetCustomerDirectoryByName' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerDirectoryByName',
      'GetCustomerDirectoryByNameResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerDirectoryByNameResponse',
      'GetCustomerDirectoryByNameResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerDirectoryByNameResult',
      'GetCustomerByKey' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByKey',
      'GetCustomerByKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByKeyResponse',
      'GetCustomerByKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByKeyResult',
      'GetCustomerById' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerById',
      'GetCustomerByIdResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByIdResponse',
      'GetCustomerByIdResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByIdResult',
      'GetCustomerByEmail' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByEmail',
      'GetCustomerByEmailResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByEmailResponse',
      'GetCustomerByEmailResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByEmailResult',
      'GetCustomerByWebsite' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByWebsite',
      'GetCustomerByWebsiteResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByWebsiteResponse',
      'GetCustomerByWebsiteResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByWebsiteResult',
      'GetCustomerEvent' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerEvent',
      'GetCustomerEventResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerEventResponse',
      'GetCustomerEventResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerEventResult',
      'GetEventRegistrantByKey' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventRegistrantByKey',
      'GetEventRegistrantByKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventRegistrantByKeyResponse',
      'GetEventRegistrantByKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventRegistrantByKeyResult',
      'GetRegistrantsByKey' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetRegistrantsByKey',
      'GetRegistrantsByKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetRegistrantsByKeyResponse',
      'GetRegistrantsByKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetRegistrantsByKeyResult',
      'GetCustomerSession' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerSession',
      'GetCustomerSessionResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerSessionResponse',
      'GetCustomerSessionResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerSessionResult',
      'GetEventListByName' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventListByName',
      'GetEventListByNameResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventListByNameResponse',
      'GetEventListByNameResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventListByNameResult',
      'GetActiveEventListByName' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveEventListByName',
      'GetActiveEventListByNameResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveEventListByNameResponse',
      'GetActiveEventListByNameResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveEventListByNameResult',
      'GetActiveEventListByDate' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveEventListByDate',
      'GetActiveEventListByDateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveEventListByDateResponse',
      'GetActiveEventListByDateResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveEventListByDateResult',
      'GetEventListByKey' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventListByKey',
      'GetEventListByKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventListByKeyResponse',
      'GetEventListByKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventListByKeyResult',
      'GetEventByProductKey' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventByProductKey',
      'GetEventByProductKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventByProductKeyResponse',
      'GetEventByProductKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventByProductKeyResult',
      'GetSessionListByName' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetSessionListByName',
      'GetSessionListByNameResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetSessionListByNameResponse',
      'GetSessionListByNameResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetSessionListByNameResult',
      'GetSessionByKey' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetSessionByKey',
      'GetSessionByKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetSessionByKeyResponse',
      'GetSessionByKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetSessionByKeyResult',
      'GetCustomerTicketedSessionsByRegKey' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerTicketedSessionsByRegKey',
      'GetCustomerTicketedSessionsByRegKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerTicketedSessionsByRegKeyResponse',
      'GetCustomerTicketedSessionsByRegKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerTicketedSessionsByRegKeyResult',
      'GetSessionListByEvent' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetSessionListByEvent',
      'GetSessionListByEventResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetSessionListByEventResponse',
      'GetSessionListByEventResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetSessionListByEventResult',
      'GetEventCustomerList' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventCustomerList',
      'GetEventCustomerListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventCustomerListResponse',
      'GetEventCustomerListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventCustomerListResult',
      'GetSessionCustomerList' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetSessionCustomerList',
      'GetSessionCustomerListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetSessionCustomerListResponse',
      'GetSessionCustomerListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetSessionCustomerListResult',
      'GetProductBalances' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetProductBalances',
      'GetProductBalancesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetProductBalancesResponse',
      'GetProductBalancesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetProductBalancesResult',
      'CheckEWebUser' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\CheckEWebUser',
      'CheckEWebUserResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\CheckEWebUserResponse',
      'CheckEWebUserResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\CheckEWebUserResult',
      'GetCommitteeByKey' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCommitteeByKey',
      'GetCommitteeByKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCommitteeByKeyResponse',
      'GetCommitteeByKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCommitteeByKeyResult',
      'GetCommitteeByCode' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCommitteeByCode',
      'GetCommitteeByCodeResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCommitteeByCodeResponse',
      'GetCommitteeByCodeResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCommitteeByCodeResult',
      'GetCommitteeListByCode' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCommitteeListByCode',
      'GetCommitteeListByCodeResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCommitteeListByCodeResponse',
      'GetCommitteeListByCodeResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCommitteeListByCodeResult',
      'GetCommitteeListByCstId' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCommitteeListByCstId',
      'GetCommitteeListByCstIdResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCommitteeListByCstIdResponse',
      'GetCommitteeListByCstIdResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCommitteeListByCstIdResult',
      'GetCommitteeParticipationByCstId' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCommitteeParticipationByCstId',
      'GetCommitteeParticipationByCstIdResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCommitteeParticipationByCstIdResponse',
      'GetCommitteeParticipationByCstIdResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCommitteeParticipationByCstIdResult',
      'GetActiveProductList' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveProductList',
      'GetActiveProductListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveProductListResponse',
      'GetActiveProductListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveProductListResult',
      'GetActiveProductListByIndividual' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveProductListByIndividual',
      'GetActiveProductListByIndividualResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveProductListByIndividualResponse',
      'GetActiveProductListByIndividualResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveProductListByIndividualResult',
      'GetPricesByProductKey' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetPricesByProductKey',
      'GetPricesByProductKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetPricesByProductKeyResponse',
      'GetPricesByProductKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetPricesByProductKeyResult',
      'GetActiveProductListByCustomer' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveProductListByCustomer',
      'GetActiveProductListByCustomerResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveProductListByCustomerResponse',
      'GetActiveProductListByCustomerResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveProductListByCustomerResult',
      'GetActiveSubscriptionByCustomer' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveSubscriptionByCustomer',
      'GetActiveSubscriptionByCustomerResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveSubscriptionByCustomerResponse',
      'GetActiveSubscriptionByCustomerResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveSubscriptionByCustomerResult',
      'GetMembershipProxyList' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetMembershipProxyList',
      'GetMembershipProxyListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetMembershipProxyListResponse',
      'GetMembershipProxyListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetMembershipProxyListResult',
      'GetMembershipsByCustomer' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetMembershipsByCustomer',
      'GetMembershipsByCustomerResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetMembershipsByCustomerResponse',
      'GetMembershipsByCustomerResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetMembershipsByCustomerResult',
      'NewOrganizationInformation' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\NewOrganizationInformation',
      'oNode' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\oNode',
      'NewOrganizationInformationResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\NewOrganizationInformationResponse',
      'NewOrganizationInformationResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\NewOrganizationInformationResult',
      'SetOrganizationInformation' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\SetOrganizationInformation',
      'oUpdateNode' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\oUpdateNode',
      'SetOrganizationInformationResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\SetOrganizationInformationResponse',
      'SetOrganizationInformationResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\SetOrganizationInformationResult',
      'NewIndividualInformation' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\NewIndividualInformation',
      'NewIndividualInformationResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\NewIndividualInformationResponse',
      'NewIndividualInformationResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\NewIndividualInformationResult',
      'SetIndividualInformation' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\SetIndividualInformation',
      'SetIndividualInformationResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\SetIndividualInformationResponse',
      'SetIndividualInformationResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\SetIndividualInformationResult',
      'AddCustomerRelationship' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\AddCustomerRelationship',
      'AddCustomerRelationshipResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\AddCustomerRelationshipResponse',
      'AddCustomerRelationshipResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\AddCustomerRelationshipResult',
      'AddIndOrgRelationship' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\AddIndOrgRelationship',
      'AddIndOrgRelationshipResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\AddIndOrgRelationshipResponse',
      'AddIndOrgRelationshipResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\AddIndOrgRelationshipResult',
      'SetCustomerRelationship' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\SetCustomerRelationship',
      'SetCustomerRelationshipResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\SetCustomerRelationshipResponse',
      'SetCustomerRelationshipResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\SetCustomerRelationshipResult',
      'GetCustomerActivityHistory' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerActivityHistory',
      'GetCustomerActivityHistoryResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerActivityHistoryResponse',
      'GetCustomerActivityHistoryResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerActivityHistoryResult',
      'NewHistoricalActivity' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\NewHistoricalActivity',
      'NewHistoricalActivityResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\NewHistoricalActivityResponse',
      'NewHistoricalActivityResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\NewHistoricalActivityResult',
      'SetHistoricalActivity' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\SetHistoricalActivity',
      'SetHistoricalActivityResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\SetHistoricalActivityResponse',
      'SetHistoricalActivityResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\SetHistoricalActivityResult',
      'GetCertificationProducts' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCertificationProducts',
      'GetCertificationProductsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCertificationProductsResponse',
      'GetCertificationProductsResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCertificationProductsResult',
      'GetCertificants' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCertificants',
      'GetCertificantsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCertificantsResponse',
      'GetCertificantsResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCertificantsResult',
      'GetCertificationPrograms' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCertificationPrograms',
      'GetCertificationProgramsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCertificationProgramsResponse',
      'GetCertificationProgramsResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCertificationProgramsResult',
      'GetCertificantsByProgram' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCertificantsByProgram',
      'GetCertificantsByProgramResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCertificantsByProgramResponse',
      'GetCertificantsByProgramResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCertificantsByProgramResult',
      'GetPropertyByOwner' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetPropertyByOwner',
      'GetPropertyByOwnerResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetPropertyByOwnerResponse',
      'GetPropertyByOwnerResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetPropertyByOwnerResult',
      'GetPropertyByCategory' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetPropertyByCategory',
      'GetPropertyByCategoryResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetPropertyByCategoryResponse',
      'GetPropertyByCategoryResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetPropertyByCategoryResult',
      'GetPropertyCategories' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetPropertyCategories',
      'GetPropertyCategoriesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetPropertyCategoriesResponse',
      'GetPropertyCategoriesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetPropertyCategoriesResult',
      'GetPropertyByType' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetPropertyByType',
      'GetPropertyByTypeResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetPropertyByTypeResponse',
      'GetPropertyByTypeResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetPropertyByTypeResult',
      'GetPropertyTypes' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetPropertyTypes',
      'GetPropertyTypesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetPropertyTypesResponse',
      'GetPropertyTypesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetPropertyTypesResult',
      'GetMultiSelectValues' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetMultiSelectValues',
      'GetMultiSelectValuesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetMultiSelectValuesResponse',
      'GetMultiSelectValuesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetMultiSelectValuesResult',
      'GetAwardEntriesByCustomer' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetAwardEntriesByCustomer',
      'GetAwardEntriesByCustomerResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetAwardEntriesByCustomerResponse',
      'GetAwardEntriesByCustomerResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetAwardEntriesByCustomerResult',
      'GetAwardEntriesByAward' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetAwardEntriesByAward',
      'GetAwardEntriesByAwardResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetAwardEntriesByAwardResponse',
      'GetAwardEntriesByAwardResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetAwardEntriesByAwardResult',
      'GetAwardJudges' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetAwardJudges',
      'GetAwardJudgesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetAwardJudgesResponse',
      'GetAwardJudgesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetAwardJudgesResult',
      'GetLocationByEvent' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetLocationByEvent',
      'GetLocationByEventResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetLocationByEventResponse',
      'GetLocationByEventResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetLocationByEventResult',
      'GetTracksByEvent' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetTracksByEvent',
      'GetTracksByEventResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetTracksByEventResponse',
      'GetTracksByEventResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetTracksByEventResult',
      'GetSessionsByTrackCode' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetSessionsByTrackCode',
      'GetSessionsByTrackCodeResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetSessionsByTrackCodeResponse',
      'GetSessionsByTrackCodeResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetSessionsByTrackCodeResult',
      'GetRoomByLocation' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetRoomByLocation',
      'GetRoomByLocationResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetRoomByLocationResponse',
      'GetRoomByLocationResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetRoomByLocationResult',
      'GetRoomSessionByLocation' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetRoomSessionByLocation',
      'GetRoomSessionByLocationResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetRoomSessionByLocationResponse',
      'GetRoomSessionByLocationResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetRoomSessionByLocationResult',
      'GetSpeakerInfoBySessionKey' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetSpeakerInfoBySessionKey',
      'GetSpeakerInfoBySessionKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetSpeakerInfoBySessionKeyResponse',
      'GetSpeakerInfoBySessionKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetSpeakerInfoBySessionKeyResult',
      'GetIndividualChangesByDate' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetIndividualChangesByDate',
      'GetIndividualChangesByDateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetIndividualChangesByDateResponse',
      'GetIndividualChangesByDateResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetIndividualChangesByDateResult',
      'GetOrganizationChangesByDate' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetOrganizationChangesByDate',
      'GetOrganizationChangesByDateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetOrganizationChangesByDateResponse',
      'GetOrganizationChangesByDateResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetOrganizationChangesByDateResult',
      'GetMembershipChangesByDate' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetMembershipChangesByDate',
      'GetMembershipChangesByDateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetMembershipChangesByDateResponse',
      'GetMembershipChangesByDateResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetMembershipChangesByDateResult',
      'GetRegistrantChangesByDate' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetRegistrantChangesByDate',
      'GetRegistrantChangesByDateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetRegistrantChangesByDateResponse',
      'GetRegistrantChangesByDateResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetRegistrantChangesByDateResult',
      'GetCommitteeParticipationChangesByDate' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCommitteeParticipationChangesByDate',
      'GetCommitteeParticipationChangesByDateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCommitteeParticipationChangesByDateResponse',
      'GetCommitteeParticipationChangesByDateResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCommitteeParticipationChangesByDateResult',
      'GetIndividualByDemographic' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetIndividualByDemographic',
      'GetIndividualByDemographicResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetIndividualByDemographicResponse',
      'GetIndividualByDemographicResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetIndividualByDemographicResult',
      'GetOrganizationByDemographic' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetOrganizationByDemographic',
      'GetOrganizationByDemographicResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetOrganizationByDemographicResponse',
      'GetOrganizationByDemographicResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetOrganizationByDemographicResult',
      'GetEventFeesByEventKey' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventFeesByEventKey',
      'GetEventFeesByEventKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventFeesByEventKeyResponse',
      'GetEventFeesByEventKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventFeesByEventKeyResult',
      'GetCstKeyFromLicenseNumber' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCstKeyFromLicenseNumber',
      'GetCstKeyFromLicenseNumberResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCstKeyFromLicenseNumberResponse',
      'GetCstKeyFromLicenseNumberResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCstKeyFromLicenseNumberResult',
      'GetCstKeyFromRealtorId' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCstKeyFromRealtorId',
      'GetCstKeyFromRealtorIdResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCstKeyFromRealtorIdResponse',
      'GetCstKeyFromRealtorIdResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCstKeyFromRealtorIdResult',
      'GetCustomerByMembershipType' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByMembershipType',
      'GetCustomerByMembershipTypeResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByMembershipTypeResponse',
      'GetCustomerByMembershipTypeResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerByMembershipTypeResult',
      'GetChildOrgsByParentOrgKey' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetChildOrgsByParentOrgKey',
      'GetChildOrgsByParentOrgKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetChildOrgsByParentOrgKeyResponse',
      'GetChildOrgsByParentOrgKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetChildOrgsByParentOrgKeyResult',
      'GetCustomerRealtorMembershipList' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerRealtorMembershipList',
      'GetCustomerRealtorMembershipListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerRealtorMembershipListResponse',
      'GetCustomerRealtorMembershipListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerRealtorMembershipListResult',
      'GetOrganizationByType' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetOrganizationByType',
      'GetOrganizationByTypeResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetOrganizationByTypeResponse',
      'IndexedResultListOfOrganizationByTypeResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\IndexedResultListOfOrganizationByTypeResult',
      'ResultListOfOrganizationByTypeResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\ResultListOfOrganizationByTypeResult',
      'OrganizationByTypeResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\OrganizationByTypeResult',
      'GetEventListByType' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventListByType',
      'GetEventListByTypeResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventListByTypeResponse',
      'GetEventListByTypeResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetEventListByTypeResult',
      'GetProductListByType' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetProductListByType',
      'GetProductListByTypeResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetProductListByTypeResponse',
      'GetProductListByTypeResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetProductListByTypeResult',
      'GetIndividualListByOrganizationRelationship' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetIndividualListByOrganizationRelationship',
      'GetIndividualListByOrganizationRelationshipResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetIndividualListByOrganizationRelationshipResponse',
      'IndexedResultListOfIndividualRelationshipResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\IndexedResultListOfIndividualRelationshipResult',
      'ResultListOfIndividualRelationshipResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\ResultListOfIndividualRelationshipResult',
      'IndividualRelationshipResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\IndividualRelationshipResult',
      'GetIndividualListByChangeDateRelationship' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetIndividualListByChangeDateRelationship',
      'GetIndividualListByChangeDateRelationshipResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetIndividualListByChangeDateRelationshipResponse',
      'GetRelationshipsByIndiviual' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetRelationshipsByIndiviual',
      'GetRelationshipsByIndiviualResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetRelationshipsByIndiviualResponse',
      'GetRelationshipsByIndiviualResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetRelationshipsByIndiviualResult',
      'GetCustomerCertifications' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerCertifications',
      'GetCustomerCertificationsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerCertificationsResponse',
      'GetCustomerCertificationsResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetCustomerCertificationsResult',
      'GetIndividualByPrimaryOrganization' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetIndividualByPrimaryOrganization',
      'GetIndividualByPrimaryOrganizationResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetIndividualByPrimaryOrganizationResponse',
      'GetIndividualByPrimaryOrganizationResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetIndividualByPrimaryOrganizationResult',
      'GetActiveSubscriptionByDate' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveSubscriptionByDate',
      'GetActiveSubscriptionByDateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveSubscriptionByDateResponse',
      'GetActiveSubscriptionByDateResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetActiveSubscriptionByDateResult',
      'UpdateRegistrationBadgePrintDateByRegKey' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\UpdateRegistrationBadgePrintDateByRegKey',
      'UpdateRegistrationBadgePrintDateByRegKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\UpdateRegistrationBadgePrintDateByRegKeyResponse',
      'UpdateRegistrationBadgePrintDateByRegKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\UpdateRegistrationBadgePrintDateByRegKeyResult',
      'GetOrganizationsByPrimaryIndividual' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetOrganizationsByPrimaryIndividual',
      'GetOrganizationsByPrimaryIndividualResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetOrganizationsByPrimaryIndividualResponse',
      'IndexedResultListOfOrganizationByPrimaryInd' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\IndexedResultListOfOrganizationByPrimaryInd',
      'ResultListOfOrganizationByPrimaryInd' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\ResultListOfOrganizationByPrimaryInd',
      'OrganizationByPrimaryInd' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\OrganizationByPrimaryInd',
      'GetOptOutCategories' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetOptOutCategories',
      'GetOptOutCategoriesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetOptOutCategoriesResponse',
      'GetOptOutCategoriesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\GetOptOutCategoriesResult',
      'SetEducationCreditbyCstkey' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\SetEducationCreditbyCstkey',
      'SetEducationCreditbyCstkeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\SetEducationCreditbyCstkeyResponse',
      'SetEducationCreditbyCstkeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\SetEducationCreditbyCstkeyResult',
      'UpdateEducationCreditbykey' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\UpdateEducationCreditbykey',
      'UpdateEducationCreditbykeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\UpdateEducationCreditbykeyResponse',
      'UpdateEducationCreditbykeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\OnDemand\\UpdateEducationCreditbykeyResult',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'https://netforum.avectra.com/xWeb/netForumXMLOnDemand.asmx?WSDL';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * Passing correct credentials to this method will return an authentication token - without an authentication token, the rest of the xWeb web methods will be inoperable.  The authentication token is governed by the group privileges assigned to the account invoking the xWeb web methods.  Please consult with the administrator of the netForum database to ensure your level of authorization.
     *
     * @param Authenticate $parameters
     * @return AuthenticateResponse
     */
    public function Authenticate(Authenticate $parameters)
    {
      return $this->__soapCall('Authenticate', array($parameters));
    }

    /**
     * Provides a W3C XML Schema for any netFORUM Façade object.  Parameters: szObjectName - the textual display name of the object for which the schema information is being Requested (individual, address, etc.).
     *
     * @param GetFacadeXMLSchema $parameters
     * @return GetFacadeXMLSchemaResponse
     */
    public function GetFacadeXMLSchema(GetFacadeXMLSchema $parameters)
    {
      return $this->__soapCall('GetFacadeXMLSchema', array($parameters));
    }

    /**
     * Returns all the fields and related data for the requested object.  Parameters: szObjectName, szObjectKey.  szObjectName - the textual display name of the object for which the schema information is being Requested (individual, address, etc.).  szObjectKey - the GUID representing an object's unique identity (primary key).
     *
     * @param GetFacadeObject $parameters
     * @return GetFacadeObjectResponse
     */
    public function GetFacadeObject(GetFacadeObject $parameters)
    {
      return $this->__soapCall('GetFacadeObject', array($parameters));
    }

    /**
     * <H3 style="color:red">This method is not supported for new integrations.  It is being deprecated and listed here only for backwards compatibility.</H3>Provides a means of retrieving single or multiple data records and returns either the specific columns (the keyfield of the facade is also returned) or the complete Facade object record.Parameters (4): szObjectName, szColumnList, szWhereClause, szOrderBy.  szObjectName - the textual display name of the object type requested.  szColumnList - the Façade Object fields to be returned from the query.  szWhereClause - the filter to be used in the query.  szOrderBy - the field(s) used to order the results.
     *
     * @param GetQuery $parameters
     * @return GetQueryResponse
     */
    public function GetQuery(GetQuery $parameters)
    {
      return $this->__soapCall('GetQuery', array($parameters));
    }

    /**
     * <H3 style="color:red">This method is not supported for new integrations.  It is being deprecated and listed here only for backwards compatibility.</H3>Provides a means of retrieving single or multiple data records and returns either the specific columns (the keyfield of the facade is also returned) or the complete Facade object record.Parameters (4): szObjectName, szColumnList, szWhereClause, szOrderBy.  szObjectName - the textual display name of the object type requested.  szColumnList - the Façade Object fields to be returned from the query.  szWhereClause - the filter to be used in the query.  szOrderBy - the field(s) used to order the results.
     *
     * @param GetQuery2 $parameters
     * @return GetQuery2Response
     */
    public function GetQuery2(GetQuery2 $parameters)
    {
      return $this->__soapCall('GetQuery2', array($parameters));
    }

    /**
     * Get Customer Balance.
     *
     * @param GetCustomerBalance $parameters
     * @return GetCustomerBalanceResponse
     */
    public function GetCustomerBalance(GetCustomerBalance $parameters)
    {
      return $this->__soapCall('GetCustomerBalance', array($parameters));
    }

    /**
     * Get Customer Membership Information.
     *
     * @param GetCustomerMembership $parameters
     * @return GetCustomerMembershipResponse
     */
    public function GetCustomerMembership(GetCustomerMembership $parameters)
    {
      return $this->__soapCall('GetCustomerMembership', array($parameters));
    }

    /**
     * Get Customer Address and Phone Information by Zip.
     *
     * @param GetCustomerByZip $parameters
     * @return GetCustomerByZipResponse
     */
    public function GetCustomerByZip(GetCustomerByZip $parameters)
    {
      return $this->__soapCall('GetCustomerByZip', array($parameters));
    }

    /**
     * Get Customer Address and Phone Information by City State.
     *
     * @param GetCustomerByCityState $parameters
     * @return GetCustomerByCityStateResponse
     */
    public function GetCustomerByCityState(GetCustomerByCityState $parameters)
    {
      return $this->__soapCall('GetCustomerByCityState', array($parameters));
    }

    /**
     * Get Customer Address and Phone Information by City State.
     *
     * @param GetCustomerByCityStateMembership $parameters
     * @return GetCustomerByCityStateMembershipResponse
     */
    public function GetCustomerByCityStateMembership(GetCustomerByCityStateMembership $parameters)
    {
      return $this->__soapCall('GetCustomerByCityStateMembership', array($parameters));
    }

    /**
     * Get Customers within a set radius.
     *
     * @param GetCustomerByRadius $parameters
     * @return GetCustomerByRadiusResponse
     */
    public function GetCustomerByRadius(GetCustomerByRadius $parameters)
    {
      return $this->__soapCall('GetCustomerByRadius', array($parameters));
    }

    /**
     * Get Customers within a set radius using a specific address type.
     *
     * @param GetCustomerByRadiusAddressType $parameters
     * @return GetCustomerByRadiusAddressTypeResponse
     */
    public function GetCustomerByRadiusAddressType(GetCustomerByRadiusAddressType $parameters)
    {
      return $this->__soapCall('GetCustomerByRadiusAddressType', array($parameters));
    }

    /**
     * Get Customers within a set radius from Latitude and Longitude.
     *
     * @param GetCustomerByRadiusLatLon $parameters
     * @return GetCustomerByRadiusLatLonResponse
     */
    public function GetCustomerByRadiusLatLon(GetCustomerByRadiusLatLon $parameters)
    {
      return $this->__soapCall('GetCustomerByRadiusLatLon', array($parameters));
    }

    /**
     * Get Customer by record date.
     *
     * @param GetCustomerByRecordDate $parameters
     * @return GetCustomerByRecordDateResponse
     */
    public function GetCustomerByRecordDate(GetCustomerByRecordDate $parameters)
    {
      return $this->__soapCall('GetCustomerByRecordDate', array($parameters));
    }

    /**
     * Get Customer By Name
     *
     * @param GetCustomerByName $parameters
     * @return GetCustomerByNameResponse
     */
    public function GetCustomerByName(GetCustomerByName $parameters)
    {
      return $this->__soapCall('GetCustomerByName', array($parameters));
    }

    /**
     * Get Customer By Name
     *
     * @param GetCustomerDirectoryByName $parameters
     * @return GetCustomerDirectoryByNameResponse
     */
    public function GetCustomerDirectoryByName(GetCustomerDirectoryByName $parameters)
    {
      return $this->__soapCall('GetCustomerDirectoryByName', array($parameters));
    }

    /**
     * Get Customer By Key
     *
     * @param GetCustomerByKey $parameters
     * @return GetCustomerByKeyResponse
     */
    public function GetCustomerByKey(GetCustomerByKey $parameters)
    {
      return $this->__soapCall('GetCustomerByKey', array($parameters));
    }

    /**
     * Get Customer By Id
     *
     * @param GetCustomerById $parameters
     * @return GetCustomerByIdResponse
     */
    public function GetCustomerById(GetCustomerById $parameters)
    {
      return $this->__soapCall('GetCustomerById', array($parameters));
    }

    /**
     * Get Customer By Email
     *
     * @param GetCustomerByEmail $parameters
     * @return GetCustomerByEmailResponse
     */
    public function GetCustomerByEmail(GetCustomerByEmail $parameters)
    {
      return $this->__soapCall('GetCustomerByEmail', array($parameters));
    }

    /**
     * Get Customer based on website address or domain.
     *
     * @param GetCustomerByWebsite $parameters
     * @return GetCustomerByWebsiteResponse
     */
    public function GetCustomerByWebsite(GetCustomerByWebsite $parameters)
    {
      return $this->__soapCall('GetCustomerByWebsite', array($parameters));
    }

    /**
     * Get Customer Event and Registrant Information.
     *
     * @param GetCustomerEvent $parameters
     * @return GetCustomerEventResponse
     */
    public function GetCustomerEvent(GetCustomerEvent $parameters)
    {
      return $this->__soapCall('GetCustomerEvent', array($parameters));
    }

    /**
     * Get Registrant Information.
     *
     * @param GetEventRegistrantByKey $parameters
     * @return GetEventRegistrantByKeyResponse
     */
    public function GetEventRegistrantByKey(GetEventRegistrantByKey $parameters)
    {
      return $this->__soapCall('GetEventRegistrantByKey', array($parameters));
    }

    /**
     * Get Registrants by event key with optional name and id parameters
     *
     * @param GetRegistrantsByKey $parameters
     * @return GetRegistrantsByKeyResponse
     */
    public function GetRegistrantsByKey(GetRegistrantsByKey $parameters)
    {
      return $this->__soapCall('GetRegistrantsByKey', array($parameters));
    }

    /**
     * Get Customer Session and Registrant Information.
     *
     * @param GetCustomerSession $parameters
     * @return GetCustomerSessionResponse
     */
    public function GetCustomerSession(GetCustomerSession $parameters)
    {
      return $this->__soapCall('GetCustomerSession', array($parameters));
    }

    /**
     * Get Event List By Name
     *
     * @param GetEventListByName $parameters
     * @return GetEventListByNameResponse
     */
    public function GetEventListByName(GetEventListByName $parameters)
    {
      return $this->__soapCall('GetEventListByName', array($parameters));
    }

    /**
     * Get Active Event List By Name, for all active events leave the name blank.
     *
     * @param GetActiveEventListByName $parameters
     * @return GetActiveEventListByNameResponse
     */
    public function GetActiveEventListByName(GetActiveEventListByName $parameters)
    {
      return $this->__soapCall('GetActiveEventListByName', array($parameters));
    }

    /**
     * Get Active Event List By Date
     *
     * @param GetActiveEventListByDate $parameters
     * @return GetActiveEventListByDateResponse
     */
    public function GetActiveEventListByDate(GetActiveEventListByDate $parameters)
    {
      return $this->__soapCall('GetActiveEventListByDate', array($parameters));
    }

    /**
     * Get Event By Key
     *
     * @param GetEventListByKey $parameters
     * @return GetEventListByKeyResponse
     */
    public function GetEventByKey(GetEventListByKey $parameters)
    {
      return $this->__soapCall('GetEventByKey', array($parameters));
    }

    /**
     * Get Event By Product Key
     *
     * @param GetEventByProductKey $parameters
     * @return GetEventByProductKeyResponse
     */
    public function GetEventByProductKey(GetEventByProductKey $parameters)
    {
      return $this->__soapCall('GetEventByProductKey', array($parameters));
    }

    /**
     * Get Session List By Name
     *
     * @param GetSessionListByName $parameters
     * @return GetSessionListByNameResponse
     */
    public function GetSessionListByName(GetSessionListByName $parameters)
    {
      return $this->__soapCall('GetSessionListByName', array($parameters));
    }

    /**
     * Get Session By Key
     *
     * @param GetSessionByKey $parameters
     * @return GetSessionByKeyResponse
     */
    public function GetSessionByKey(GetSessionByKey $parameters)
    {
      return $this->__soapCall('GetSessionByKey', array($parameters));
    }

    /**
     * Return a list of ticketed sessions by registration key
     *
     * @param GetCustomerTicketedSessionsByRegKey $parameters
     * @return GetCustomerTicketedSessionsByRegKeyResponse
     */
    public function GetCustomerTicketedSessionsByRegKey(GetCustomerTicketedSessionsByRegKey $parameters)
    {
      return $this->__soapCall('GetCustomerTicketedSessionsByRegKey', array($parameters));
    }

    /**
     * Get Session List By Event
     *
     * @param GetSessionListByEvent $parameters
     * @return GetSessionListByEventResponse
     */
    public function GetSessionListByEvent(GetSessionListByEvent $parameters)
    {
      return $this->__soapCall('GetSessionListByEvent', array($parameters));
    }

    /**
     * Get Customer List for a specific Event
     *
     * @param GetEventCustomerList $parameters
     * @return GetEventCustomerListResponse
     */
    public function GetEventCustomerList(GetEventCustomerList $parameters)
    {
      return $this->__soapCall('GetEventCustomerList', array($parameters));
    }

    /**
     * Get Customer List for a specific Session
     *
     * @param GetSessionCustomerList $parameters
     * @return GetSessionCustomerListResponse
     */
    public function GetSessionCustomerList(GetSessionCustomerList $parameters)
    {
      return $this->__soapCall('GetSessionCustomerList', array($parameters));
    }

    /**
     * Get Product Balance.
     *
     * @param GetProductBalances $parameters
     * @return GetProductBalancesResponse
     */
    public function GetProductBalances(GetProductBalances $parameters)
    {
      return $this->__soapCall('GetProductBalances', array($parameters));
    }

    /**
     * Check eWeb Login
     *
     * @param CheckEWebUser $parameters
     * @return CheckEWebUserResponse
     */
    public function CheckEWebUser(CheckEWebUser $parameters)
    {
      return $this->__soapCall('CheckEWebUser', array($parameters));
    }

    /**
     * Get Committee By Key
     *
     * @param GetCommitteeByKey $parameters
     * @return GetCommitteeByKeyResponse
     */
    public function GetCommitteeByKey(GetCommitteeByKey $parameters)
    {
      return $this->__soapCall('GetCommitteeByKey', array($parameters));
    }

    /**
     * Get Committee By Code
     *
     * @param GetCommitteeByCode $parameters
     * @return GetCommitteeByCodeResponse
     */
    public function GetCommitteeByCode(GetCommitteeByCode $parameters)
    {
      return $this->__soapCall('GetCommitteeByCode', array($parameters));
    }

    /**
     * Get Customer List for a specific Committee
     *
     * @param GetCommitteeListByCode $parameters
     * @return GetCommitteeListByCodeResponse
     */
    public function GetCommitteeListByCode(GetCommitteeListByCode $parameters)
    {
      return $this->__soapCall('GetCommitteeListByCode', array($parameters));
    }

    /**
     * Get Customer List for a specific Customer
     *
     * @param GetCommitteeListByCstId $parameters
     * @return GetCommitteeListByCstIdResponse
     */
    public function GetCommitteeListByCstId(GetCommitteeListByCstId $parameters)
    {
      return $this->__soapCall('GetCommitteeListByCstId', array($parameters));
    }

    /**
     * Get Committee List the Customer is participant of
     *
     * @param GetCommitteeParticipationByCstId $parameters
     * @return GetCommitteeParticipationByCstIdResponse
     */
    public function GetCommitteePatricipationByCstId(GetCommitteeParticipationByCstId $parameters)
    {
      return $this->__soapCall('GetCommitteePatricipationByCstId', array($parameters));
    }

    /**
     * Get Active Product List
     *
     * @param GetActiveProductList $parameters
     * @return GetActiveProductListResponse
     */
    public function GetActiveProductList(GetActiveProductList $parameters)
    {
      return $this->__soapCall('GetActiveProductList', array($parameters));
    }

    /**
     * Get Active Product List By Individual
     *
     * @param GetActiveProductListByIndividual $parameters
     * @return GetActiveProductListByIndividualResponse
     */
    public function GetActiveProductListByIndividual(GetActiveProductListByIndividual $parameters)
    {
      return $this->__soapCall('GetActiveProductListByIndividual', array($parameters));
    }

    /**
     * Get all prices and qualifiers for a product
     *
     * @param GetPricesByProductKey $parameters
     * @return GetPricesByProductKeyResponse
     */
    public function GetPricesByProductKey(GetPricesByProductKey $parameters)
    {
      return $this->__soapCall('GetPricesByProductKey', array($parameters));
    }

    /**
     * Get Active Product List By Customer
     *
     * @param GetActiveProductListByCustomer $parameters
     * @return GetActiveProductListByCustomerResponse
     */
    public function GetActiveProductListByCustomer(GetActiveProductListByCustomer $parameters)
    {
      return $this->__soapCall('GetActiveProductListByCustomer', array($parameters));
    }

    /**
     * Get Active Subscription by customer
     *
     * @param GetActiveSubscriptionByCustomer $parameters
     * @return GetActiveSubscriptionByCustomerResponse
     */
    public function GetActiveSubscriptionByCustomer(GetActiveSubscriptionByCustomer $parameters)
    {
      return $this->__soapCall('GetActiveSubscriptionByCustomer', array($parameters));
    }

    /**
     * Get Membership Proxy and Receives benefit information
     *
     * @param GetMembershipProxyList $parameters
     * @return GetMembershipProxyListResponse
     */
    public function GetMembershipProxyList(GetMembershipProxyList $parameters)
    {
      return $this->__soapCall('GetMembershipProxyList', array($parameters));
    }

    /**
     * Get Memberships of customers by customer key
     *
     * @param GetMembershipsByCustomer $parameters
     * @return GetMembershipsByCustomerResponse
     */
    public function GetMembershipsByCustomer(GetMembershipsByCustomer $parameters)
    {
      return $this->__soapCall('GetMembershipsByCustomer', array($parameters));
    }

    /**
     * Returns basic contact information for the individual.
     *
     * @param NewOrganizationInformation $parameters
     * @return NewOrganizationInformationResponse
     */
    public function NewOrganizationInformation(NewOrganizationInformation $parameters)
    {
      return $this->__soapCall('NewOrganizationInformation', array($parameters));
    }

    /**
     * Updates basic contact information for the organization.
     *
     * @param SetOrganizationInformation $parameters
     * @return SetOrganizationInformationResponse
     */
    public function SetOrganizationInformation(SetOrganizationInformation $parameters)
    {
      return $this->__soapCall('SetOrganizationInformation', array($parameters));
    }

    /**
     * Returns basic contact information for the individual.
     *
     * @param NewIndividualInformation $parameters
     * @return NewIndividualInformationResponse
     */
    public function NewIndividualInformation(NewIndividualInformation $parameters)
    {
      return $this->__soapCall('NewIndividualInformation', array($parameters));
    }

    /**
     * Updates basic contact information for the individual.
     *
     * @param SetIndividualInformation $parameters
     * @return SetIndividualInformationResponse
     */
    public function SetIndividualInformation(SetIndividualInformation $parameters)
    {
      return $this->__soapCall('SetIndividualInformation', array($parameters));
    }

    /**
     * Add a related customer to another related customer.
     *
     * @param AddCustomerRelationship $parameters
     * @return AddCustomerRelationshipResponse
     */
    public function AddCustomerRelationship(AddCustomerRelationship $parameters)
    {
      return $this->__soapCall('AddCustomerRelationship', array($parameters));
    }

    /**
     * Add an individual to organization relationship.
     *
     * @param AddIndOrgRelationship $parameters
     * @return AddIndOrgRelationshipResponse
     */
    public function AddIndOrgRelationship(AddIndOrgRelationship $parameters)
    {
      return $this->__soapCall('AddIndOrgRelationship', array($parameters));
    }

    /**
     * Update An Existing Customer Relationship .
     *
     * @param SetCustomerRelationship $parameters
     * @return SetCustomerRelationshipResponse
     */
    public function SetCustomerRelationship(SetCustomerRelationship $parameters)
    {
      return $this->__soapCall('SetCustomerRelationship', array($parameters));
    }

    /**
     * Get Customer Activity History
     *
     * @param GetCustomerActivityHistory $parameters
     * @return GetCustomerActivityHistoryResponse
     */
    public function GetCustomerActivityHistory(GetCustomerActivityHistory $parameters)
    {
      return $this->__soapCall('GetCustomerActivityHistory', array($parameters));
    }

    /**
     * New Historical Activity
     *
     * @param NewHistoricalActivity $parameters
     * @return NewHistoricalActivityResponse
     */
    public function NewHistoricalActivity(NewHistoricalActivity $parameters)
    {
      return $this->__soapCall('NewHistoricalActivity', array($parameters));
    }

    /**
     * Updates Historical Activity.
     *
     * @param SetHistoricalActivity $parameters
     * @return SetHistoricalActivityResponse
     */
    public function SetHistoricalActivity(SetHistoricalActivity $parameters)
    {
      return $this->__soapCall('SetHistoricalActivity', array($parameters));
    }

    /**
     * Get Certification Product List
     *
     * @param GetCertificationProducts $parameters
     * @return GetCertificationProductsResponse
     */
    public function GetCertificationProducts(GetCertificationProducts $parameters)
    {
      return $this->__soapCall('GetCertificationProducts', array($parameters));
    }

    /**
     * Get Certificant List
     *
     * @param GetCertificants $parameters
     * @return GetCertificantsResponse
     */
    public function GetCertificants(GetCertificants $parameters)
    {
      return $this->__soapCall('GetCertificants', array($parameters));
    }

    /**
     * Pull a list of all available programs
     *
     * @param GetCertificationPrograms $parameters
     * @return GetCertificationProgramsResponse
     */
    public function GetCertificationPrograms(GetCertificationPrograms $parameters)
    {
      return $this->__soapCall('GetCertificationPrograms', array($parameters));
    }

    /**
     * Pull a list of all certificants (by status – optional) within a designated program
     *
     * @param GetCertificantsByProgram $parameters
     * @return GetCertificantsByProgramResponse
     */
    public function GetCertificantsByProgram(GetCertificantsByProgram $parameters)
    {
      return $this->__soapCall('GetCertificantsByProgram', array($parameters));
    }

    /**
     * Get list of properties by customer id
     *
     * @param GetPropertyByOwner $parameters
     * @return GetPropertyByOwnerResponse
     */
    public function GetPropertyByOwner(GetPropertyByOwner $parameters)
    {
      return $this->__soapCall('GetPropertyByOwner', array($parameters));
    }

    /**
     * Get list of properties by category
     *
     * @param GetPropertyByCategory $parameters
     * @return GetPropertyByCategoryResponse
     */
    public function GetPropertyByCategory(GetPropertyByCategory $parameters)
    {
      return $this->__soapCall('GetPropertyByCategory', array($parameters));
    }

    /**
     * Returns a list of all property categories
     *
     * @param GetPropertyCategories $parameters
     * @return GetPropertyCategoriesResponse
     */
    public function GetPropertyCategories(GetPropertyCategories $parameters)
    {
      return $this->__soapCall('GetPropertyCategories', array($parameters));
    }

    /**
     * Get list of properties by type
     *
     * @param GetPropertyByType $parameters
     * @return GetPropertyByTypeResponse
     */
    public function GetPropertyByType(GetPropertyByType $parameters)
    {
      return $this->__soapCall('GetPropertyByType', array($parameters));
    }

    /**
     * Returns a list of all property types
     *
     * @param GetPropertyTypes $parameters
     * @return GetPropertyTypesResponse
     */
    public function GetPropertyTypes(GetPropertyTypes $parameters)
    {
      return $this->__soapCall('GetPropertyTypes', array($parameters));
    }

    /**
     * Return the values in a custom text column
     *
     * @param GetMultiSelectValues $parameters
     * @return GetMultiSelectValuesResponse
     */
    public function GetMultiSelectValues(GetMultiSelectValues $parameters)
    {
      return $this->__soapCall('GetMultiSelectValues', array($parameters));
    }

    /**
     * Get Award Entries By Customer Name
     *
     * @param GetAwardEntriesByCustomer $parameters
     * @return GetAwardEntriesByCustomerResponse
     */
    public function GetAwardEntriesByCustomer(GetAwardEntriesByCustomer $parameters)
    {
      return $this->__soapCall('GetAwardEntriesByCustomer', array($parameters));
    }

    /**
     * Get Award Entries By Award Name
     *
     * @param GetAwardEntriesByAward $parameters
     * @return GetAwardEntriesByAwardResponse
     */
    public function GetAwardEntriesByAward(GetAwardEntriesByAward $parameters)
    {
      return $this->__soapCall('GetAwardEntriesByAward', array($parameters));
    }

    /**
     * Get Award Judges by Award Entry number
     *
     * @param GetAwardJudges $parameters
     * @return GetAwardJudgesResponse
     */
    public function GetAwardJudges(GetAwardJudges $parameters)
    {
      return $this->__soapCall('GetAwardJudges', array($parameters));
    }

    /**
     * Get Location By Event
     *
     * @param GetLocationByEvent $parameters
     * @return GetLocationByEventResponse
     */
    public function GetLocationByEvent(GetLocationByEvent $parameters)
    {
      return $this->__soapCall('GetLocationByEvent', array($parameters));
    }

    /**
     * Get Tracks By Event
     *
     * @param GetTracksByEvent $parameters
     * @return GetTracksByEventResponse
     */
    public function GetTracksByEvent(GetTracksByEvent $parameters)
    {
      return $this->__soapCall('GetTracksByEvent', array($parameters));
    }

    /**
     * Get Session By Track
     *
     * @param GetSessionsByTrackCode $parameters
     * @return GetSessionsByTrackCodeResponse
     */
    public function GetSessionsByTrackCode(GetSessionsByTrackCode $parameters)
    {
      return $this->__soapCall('GetSessionsByTrackCode', array($parameters));
    }

    /**
     * Get Location Room(s)
     *
     * @param GetRoomByLocation $parameters
     * @return GetRoomByLocationResponse
     */
    public function GetRoomByLocation(GetRoomByLocation $parameters)
    {
      return $this->__soapCall('GetRoomByLocation', array($parameters));
    }

    /**
     * Get Session Location Room(s)
     *
     * @param GetRoomSessionByLocation $parameters
     * @return GetRoomSessionByLocationResponse
     */
    public function GetRoomSessionByLocation(GetRoomSessionByLocation $parameters)
    {
      return $this->__soapCall('GetRoomSessionByLocation', array($parameters));
    }

    /**
     * Get Speaker by Session
     *
     * @param GetSpeakerInfoBySessionKey $parameters
     * @return GetSpeakerInfoBySessionKeyResponse
     */
    public function GetSpeakerInfoBySessionKey(GetSpeakerInfoBySessionKey $parameters)
    {
      return $this->__soapCall('GetSpeakerInfoBySessionKey', array($parameters));
    }

    /**
     * Get a list of individual records that have changed during the given date range
     *
     * @param GetIndividualChangesByDate $parameters
     * @return GetIndividualChangesByDateResponse
     */
    public function GetIndividualChangesByDate(GetIndividualChangesByDate $parameters)
    {
      return $this->__soapCall('GetIndividualChangesByDate', array($parameters));
    }

    /**
     * Get a list of organization records that have changed during the given date range
     *
     * @param GetOrganizationChangesByDate $parameters
     * @return GetOrganizationChangesByDateResponse
     */
    public function GetOrganizationChangesByDate(GetOrganizationChangesByDate $parameters)
    {
      return $this->__soapCall('GetOrganizationChangesByDate', array($parameters));
    }

    /**
     * Get a list of membership records that have changed during the given date range
     *
     * @param GetMembershipChangesByDate $parameters
     * @return GetMembershipChangesByDateResponse
     */
    public function GetMembershipChangesByDate(GetMembershipChangesByDate $parameters)
    {
      return $this->__soapCall('GetMembershipChangesByDate', array($parameters));
    }

    /**
     * Get a list of registration records that have changed during the given date range
     *
     * @param GetRegistrantChangesByDate $parameters
     * @return GetRegistrantChangesByDateResponse
     */
    public function GetRegistrantChangesByDate(GetRegistrantChangesByDate $parameters)
    {
      return $this->__soapCall('GetRegistrantChangesByDate', array($parameters));
    }

    /**
     * Get a list of committee participation records that have changed during the given date range
     *
     * @param GetCommitteeParticipationChangesByDate $parameters
     * @return GetCommitteeParticipationChangesByDateResponse
     */
    public function GetCommitteeParticipationChangesByDate(GetCommitteeParticipationChangesByDate $parameters)
    {
      return $this->__soapCall('GetCommitteeParticipationChangesByDate', array($parameters));
    }

    /**
     * Get a list of individual customers who match the demographic value
     *
     * @param GetIndividualByDemographic $parameters
     * @return GetIndividualByDemographicResponse
     */
    public function GetIndividualByDemographic(GetIndividualByDemographic $parameters)
    {
      return $this->__soapCall('GetIndividualByDemographic', array($parameters));
    }

    /**
     * Get a list of organization customers who match the demographic value
     *
     * @param GetOrganizationByDemographic $parameters
     * @return GetOrganizationByDemographicResponse
     */
    public function GetOrganizationByDemographic(GetOrganizationByDemographic $parameters)
    {
      return $this->__soapCall('GetOrganizationByDemographic', array($parameters));
    }

    /**
     * Returns a list of event fees based on the given event and session key
     *
     * @param GetEventFeesByEventKey $parameters
     * @return GetEventFeesByEventKeyResponse
     */
    public function GetEventFeesByEventKey(GetEventFeesByEventKey $parameters)
    {
      return $this->__soapCall('GetEventFeesByEventKey', array($parameters));
    }

    /**
     * Returns a list of customer keys for individuals matching a realtor license number
     *
     * @param GetCstKeyFromLicenseNumber $parameters
     * @return GetCstKeyFromLicenseNumberResponse
     */
    public function GetCstKeyFromLicenseNumber(GetCstKeyFromLicenseNumber $parameters)
    {
      return $this->__soapCall('GetCstKeyFromLicenseNumber', array($parameters));
    }

    /**
     * Returns a list of customer keys for individuals and organizations matching a realtor id (NAR ID)
     *
     * @param GetCstKeyFromRealtorId $parameters
     * @return GetCstKeyFromRealtorIdResponse
     */
    public function GetCstKeyFromRealtorId(GetCstKeyFromRealtorId $parameters)
    {
      return $this->__soapCall('GetCstKeyFromRealtorId', array($parameters));
    }

    /**
     * Returns customer and membership information for specific membership types
     *
     * @param GetCustomerByMembershipType $parameters
     * @return GetCustomerByMembershipTypeResponse
     */
    public function GetCustomerByMembershipType(GetCustomerByMembershipType $parameters)
    {
      return $this->__soapCall('GetCustomerByMembershipType', array($parameters));
    }

    /**
     * Returns information for Child Orgs linked to the Parent Org Key
     *
     * @param GetChildOrgsByParentOrgKey $parameters
     * @return GetChildOrgsByParentOrgKeyResponse
     */
    public function GetChildOrgsByParentOrgKey(GetChildOrgsByParentOrgKey $parameters)
    {
      return $this->__soapCall('GetChildOrgsByParentOrgKey', array($parameters));
    }

    /**
     * Get supplimental realtor membership information for a given customer
     *
     * @param GetCustomerRealtorMembershipList $parameters
     * @return GetCustomerRealtorMembershipListResponse
     */
    public function GetCustomerRealtorMembershipList(GetCustomerRealtorMembershipList $parameters)
    {
      return $this->__soapCall('GetCustomerRealtorMembershipList', array($parameters));
    }

    /**
     * Returns a list of organizations matching a given organization type string
     *
     * @param GetOrganizationByType $parameters
     * @return GetOrganizationByTypeResponse
     */
    public function GetOrganizationByType(GetOrganizationByType $parameters)
    {
      return $this->__soapCall('GetOrganizationByType', array($parameters));
    }

    /**
     * Returns a list of events matching a given event type string
     *
     * @param GetEventListByType $parameters
     * @return GetEventListByTypeResponse
     */
    public function GetEventListByType(GetEventListByType $parameters)
    {
      return $this->__soapCall('GetEventListByType', array($parameters));
    }

    /**
     * Returns a list of products matching a given product type string
     *
     * @param GetProductListByType $parameters
     * @return GetProductListByTypeResponse
     */
    public function GetProductListByType(GetProductListByType $parameters)
    {
      return $this->__soapCall('GetProductListByType', array($parameters));
    }

    /**
     * Returns a list of individuals with a matching relationship to a given organization
     *
     * @param GetIndividualListByOrganizationRelationship $parameters
     * @return GetIndividualListByOrganizationRelationshipResponse
     */
    public function GetIndividualListByOrganizationRelationship(GetIndividualListByOrganizationRelationship $parameters)
    {
      return $this->__soapCall('GetIndividualListByOrganizationRelationship', array($parameters));
    }

    /**
     * Returns a list of individuals with a matching relationship that have been added or changed based on date
     *
     * @param GetIndividualListByChangeDateRelationship $parameters
     * @return GetIndividualListByChangeDateRelationshipResponse
     */
    public function GetIndividualListByChangedDateRelationship(GetIndividualListByChangeDateRelationship $parameters)
    {
      return $this->__soapCall('GetIndividualListByChangedDateRelationship', array($parameters));
    }

    /**
     * Returns a list of relationships for an individual
     *
     * @param GetRelationshipsByIndiviual $parameters
     * @return GetRelationshipsByIndiviualResponse
     */
    public function GetRelationshipsByIndividual(GetRelationshipsByIndiviual $parameters)
    {
      return $this->__soapCall('GetRelationshipsByIndividual', array($parameters));
    }

    /**
     * Returns a list of all certifications earned by customer
     *
     * @param GetCustomerCertifications $parameters
     * @return GetCustomerCertificationsResponse
     */
    public function GetCustomerCertifications(GetCustomerCertifications $parameters)
    {
      return $this->__soapCall('GetCustomerCertifications', array($parameters));
    }

    /**
     * Return a list of individuals whose primary organization is the organization provided.
     *
     * @param GetIndividualByPrimaryOrganization $parameters
     * @return GetIndividualByPrimaryOrganizationResponse
     */
    public function GetIndividualByPrimaryOrganization(GetIndividualByPrimaryOrganization $parameters)
    {
      return $this->__soapCall('GetIndividualByPrimaryOrganization', array($parameters));
    }

    /**
     * Get a list of active subscriptions that were created during a given time period.
     *
     * @param GetActiveSubscriptionByDate $parameters
     * @return GetActiveSubscriptionByDateResponse
     */
    public function GetActiveSubscriptionByDate(GetActiveSubscriptionByDate $parameters)
    {
      return $this->__soapCall('GetActiveSubscriptionByDate', array($parameters));
    }

    /**
     * Updated Registrant's Badge Print Date
     *
     * @param UpdateRegistrationBadgePrintDateByRegKey $parameters
     * @return UpdateRegistrationBadgePrintDateByRegKeyResponse
     */
    public function UpdateRegistrationBadgePrintDateByRegKey(UpdateRegistrationBadgePrintDateByRegKey $parameters)
    {
      return $this->__soapCall('UpdateRegistrationBadgePrintDateByRegKey', array($parameters));
    }

    /**
     * Return a list of organization whose primary contact is the individual provided.
     *
     * @param GetOrganizationsByPrimaryIndividual $parameters
     * @return GetOrganizationsByPrimaryIndividualResponse
     */
    public function GetOrganizationsByPrimaryIndividual(GetOrganizationsByPrimaryIndividual $parameters)
    {
      return $this->__soapCall('GetOrganizationsByPrimaryIndividual', array($parameters));
    }

    /**
     * Pull a list of email opt out categories
     *
     * @param GetOptOutCategories $parameters
     * @return GetOptOutCategoriesResponse
     */
    public function GetOptOutCategories(GetOptOutCategories $parameters)
    {
      return $this->__soapCall('GetOptOutCategories', array($parameters));
    }

    /**
     * Insert education credit to a customers record
     *
     * @param SetEducationCreditbyCstkey $parameters
     * @return SetEducationCreditbyCstkeyResponse
     */
    public function SetEducationCreditbyCstkey(SetEducationCreditbyCstkey $parameters)
    {
      return $this->__soapCall('SetEducationCreditbyCstkey', array($parameters));
    }

    /**
     * Update an existing education credit of an individual's record
     *
     * @param UpdateEducationCreditbykey $parameters
     * @return UpdateEducationCreditbykeyResponse
     */
    public function UpdateEducationCreditbykey(UpdateEducationCreditbykey $parameters)
    {
      return $this->__soapCall('UpdateEducationCreditbykey', array($parameters));
    }

}
