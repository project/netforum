<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerMembershipResponse
{

    /**
     * @var GetCustomerMembershipResult $GetCustomerMembershipResult
     */
    protected $GetCustomerMembershipResult = null;

    /**
     * @param GetCustomerMembershipResult $GetCustomerMembershipResult
     */
    public function __construct($GetCustomerMembershipResult)
    {
      $this->GetCustomerMembershipResult = $GetCustomerMembershipResult;
    }

    /**
     * @return GetCustomerMembershipResult
     */
    public function getGetCustomerMembershipResult()
    {
      return $this->GetCustomerMembershipResult;
    }

    /**
     * @param GetCustomerMembershipResult $GetCustomerMembershipResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerMembershipResponse
     */
    public function setGetCustomerMembershipResult($GetCustomerMembershipResult)
    {
      $this->GetCustomerMembershipResult = $GetCustomerMembershipResult;
      return $this;
    }

}
