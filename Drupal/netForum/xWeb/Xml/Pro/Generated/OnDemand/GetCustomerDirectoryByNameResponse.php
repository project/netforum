<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerDirectoryByNameResponse
{

    /**
     * @var GetCustomerDirectoryByNameResult $GetCustomerDirectoryByNameResult
     */
    protected $GetCustomerDirectoryByNameResult = null;

    /**
     * @param GetCustomerDirectoryByNameResult $GetCustomerDirectoryByNameResult
     */
    public function __construct($GetCustomerDirectoryByNameResult)
    {
      $this->GetCustomerDirectoryByNameResult = $GetCustomerDirectoryByNameResult;
    }

    /**
     * @return GetCustomerDirectoryByNameResult
     */
    public function getGetCustomerDirectoryByNameResult()
    {
      return $this->GetCustomerDirectoryByNameResult;
    }

    /**
     * @param GetCustomerDirectoryByNameResult $GetCustomerDirectoryByNameResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerDirectoryByNameResponse
     */
    public function setGetCustomerDirectoryByNameResult($GetCustomerDirectoryByNameResult)
    {
      $this->GetCustomerDirectoryByNameResult = $GetCustomerDirectoryByNameResult;
      return $this;
    }

}
