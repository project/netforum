<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class SetEducationCreditbyCstkeyResponse
{

    /**
     * @var SetEducationCreditbyCstkeyResult $SetEducationCreditbyCstkeyResult
     */
    protected $SetEducationCreditbyCstkeyResult = null;

    /**
     * @param SetEducationCreditbyCstkeyResult $SetEducationCreditbyCstkeyResult
     */
    public function __construct($SetEducationCreditbyCstkeyResult)
    {
      $this->SetEducationCreditbyCstkeyResult = $SetEducationCreditbyCstkeyResult;
    }

    /**
     * @return SetEducationCreditbyCstkeyResult
     */
    public function getSetEducationCreditbyCstkeyResult()
    {
      return $this->SetEducationCreditbyCstkeyResult;
    }

    /**
     * @param SetEducationCreditbyCstkeyResult $SetEducationCreditbyCstkeyResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\SetEducationCreditbyCstkeyResponse
     */
    public function setSetEducationCreditbyCstkeyResult($SetEducationCreditbyCstkeyResult)
    {
      $this->SetEducationCreditbyCstkeyResult = $SetEducationCreditbyCstkeyResult;
      return $this;
    }

}
