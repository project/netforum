<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByRecordDateResponse
{

    /**
     * @var GetCustomerByRecordDateResult $GetCustomerByRecordDateResult
     */
    protected $GetCustomerByRecordDateResult = null;

    /**
     * @param GetCustomerByRecordDateResult $GetCustomerByRecordDateResult
     */
    public function __construct($GetCustomerByRecordDateResult)
    {
      $this->GetCustomerByRecordDateResult = $GetCustomerByRecordDateResult;
    }

    /**
     * @return GetCustomerByRecordDateResult
     */
    public function getGetCustomerByRecordDateResult()
    {
      return $this->GetCustomerByRecordDateResult;
    }

    /**
     * @param GetCustomerByRecordDateResult $GetCustomerByRecordDateResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRecordDateResponse
     */
    public function setGetCustomerByRecordDateResult($GetCustomerByRecordDateResult)
    {
      $this->GetCustomerByRecordDateResult = $GetCustomerByRecordDateResult;
      return $this;
    }

}
