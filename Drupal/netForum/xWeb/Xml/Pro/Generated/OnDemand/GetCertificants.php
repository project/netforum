<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCertificants
{

    /**
     * @var string $szCertName
     */
    protected $szCertName = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $szCertName
     * @param string $szRecordDate
     */
    public function __construct($szCertName, $szRecordDate)
    {
      $this->szCertName = $szCertName;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getSzCertName()
    {
      return $this->szCertName;
    }

    /**
     * @param string $szCertName
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCertificants
     */
    public function setSzCertName($szCertName)
    {
      $this->szCertName = $szCertName;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCertificants
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
