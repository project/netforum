<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCstKeyFromLicenseNumber
{

    /**
     * @var string $szLicenseNumber
     */
    protected $szLicenseNumber = null;

    /**
     * @param string $szLicenseNumber
     */
    public function __construct($szLicenseNumber)
    {
      $this->szLicenseNumber = $szLicenseNumber;
    }

    /**
     * @return string
     */
    public function getSzLicenseNumber()
    {
      return $this->szLicenseNumber;
    }

    /**
     * @param string $szLicenseNumber
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCstKeyFromLicenseNumber
     */
    public function setSzLicenseNumber($szLicenseNumber)
    {
      $this->szLicenseNumber = $szLicenseNumber;
      return $this;
    }

}
