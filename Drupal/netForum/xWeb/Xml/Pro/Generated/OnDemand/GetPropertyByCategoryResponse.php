<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetPropertyByCategoryResponse
{

    /**
     * @var GetPropertyByCategoryResult $GetPropertyByCategoryResult
     */
    protected $GetPropertyByCategoryResult = null;

    /**
     * @param GetPropertyByCategoryResult $GetPropertyByCategoryResult
     */
    public function __construct($GetPropertyByCategoryResult)
    {
      $this->GetPropertyByCategoryResult = $GetPropertyByCategoryResult;
    }

    /**
     * @return GetPropertyByCategoryResult
     */
    public function getGetPropertyByCategoryResult()
    {
      return $this->GetPropertyByCategoryResult;
    }

    /**
     * @param GetPropertyByCategoryResult $GetPropertyByCategoryResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetPropertyByCategoryResponse
     */
    public function setGetPropertyByCategoryResult($GetPropertyByCategoryResult)
    {
      $this->GetPropertyByCategoryResult = $GetPropertyByCategoryResult;
      return $this;
    }

}
