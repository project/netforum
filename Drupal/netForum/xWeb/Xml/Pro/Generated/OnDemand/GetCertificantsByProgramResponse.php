<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCertificantsByProgramResponse
{

    /**
     * @var GetCertificantsByProgramResult $GetCertificantsByProgramResult
     */
    protected $GetCertificantsByProgramResult = null;

    /**
     * @param GetCertificantsByProgramResult $GetCertificantsByProgramResult
     */
    public function __construct($GetCertificantsByProgramResult)
    {
      $this->GetCertificantsByProgramResult = $GetCertificantsByProgramResult;
    }

    /**
     * @return GetCertificantsByProgramResult
     */
    public function getGetCertificantsByProgramResult()
    {
      return $this->GetCertificantsByProgramResult;
    }

    /**
     * @param GetCertificantsByProgramResult $GetCertificantsByProgramResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCertificantsByProgramResponse
     */
    public function setGetCertificantsByProgramResult($GetCertificantsByProgramResult)
    {
      $this->GetCertificantsByProgramResult = $GetCertificantsByProgramResult;
      return $this;
    }

}
