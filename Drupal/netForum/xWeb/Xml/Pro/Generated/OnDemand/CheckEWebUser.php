<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class CheckEWebUser
{

    /**
     * @var string $szEmail
     */
    protected $szEmail = null;

    /**
     * @var string $szPassword
     */
    protected $szPassword = null;

    /**
     * @param string $szEmail
     * @param string $szPassword
     */
    public function __construct($szEmail, $szPassword)
    {
      $this->szEmail = $szEmail;
      $this->szPassword = $szPassword;
    }

    /**
     * @return string
     */
    public function getSzEmail()
    {
      return $this->szEmail;
    }

    /**
     * @param string $szEmail
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\CheckEWebUser
     */
    public function setSzEmail($szEmail)
    {
      $this->szEmail = $szEmail;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzPassword()
    {
      return $this->szPassword;
    }

    /**
     * @param string $szPassword
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\CheckEWebUser
     */
    public function setSzPassword($szPassword)
    {
      $this->szPassword = $szPassword;
      return $this;
    }

}
