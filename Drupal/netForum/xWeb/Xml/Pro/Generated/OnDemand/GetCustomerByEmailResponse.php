<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByEmailResponse
{

    /**
     * @var GetCustomerByEmailResult $GetCustomerByEmailResult
     */
    protected $GetCustomerByEmailResult = null;

    /**
     * @param GetCustomerByEmailResult $GetCustomerByEmailResult
     */
    public function __construct($GetCustomerByEmailResult)
    {
      $this->GetCustomerByEmailResult = $GetCustomerByEmailResult;
    }

    /**
     * @return GetCustomerByEmailResult
     */
    public function getGetCustomerByEmailResult()
    {
      return $this->GetCustomerByEmailResult;
    }

    /**
     * @param GetCustomerByEmailResult $GetCustomerByEmailResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByEmailResponse
     */
    public function setGetCustomerByEmailResult($GetCustomerByEmailResult)
    {
      $this->GetCustomerByEmailResult = $GetCustomerByEmailResult;
      return $this;
    }

}
