<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCstKeyFromRealtorIdResponse
{

    /**
     * @var GetCstKeyFromRealtorIdResult $GetCstKeyFromRealtorIdResult
     */
    protected $GetCstKeyFromRealtorIdResult = null;

    /**
     * @param GetCstKeyFromRealtorIdResult $GetCstKeyFromRealtorIdResult
     */
    public function __construct($GetCstKeyFromRealtorIdResult)
    {
      $this->GetCstKeyFromRealtorIdResult = $GetCstKeyFromRealtorIdResult;
    }

    /**
     * @return GetCstKeyFromRealtorIdResult
     */
    public function getGetCstKeyFromRealtorIdResult()
    {
      return $this->GetCstKeyFromRealtorIdResult;
    }

    /**
     * @param GetCstKeyFromRealtorIdResult $GetCstKeyFromRealtorIdResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCstKeyFromRealtorIdResponse
     */
    public function setGetCstKeyFromRealtorIdResult($GetCstKeyFromRealtorIdResult)
    {
      $this->GetCstKeyFromRealtorIdResult = $GetCstKeyFromRealtorIdResult;
      return $this;
    }

}
