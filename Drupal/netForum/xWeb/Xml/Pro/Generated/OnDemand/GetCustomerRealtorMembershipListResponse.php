<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerRealtorMembershipListResponse
{

    /**
     * @var GetCustomerRealtorMembershipListResult $GetCustomerRealtorMembershipListResult
     */
    protected $GetCustomerRealtorMembershipListResult = null;

    /**
     * @param GetCustomerRealtorMembershipListResult $GetCustomerRealtorMembershipListResult
     */
    public function __construct($GetCustomerRealtorMembershipListResult)
    {
      $this->GetCustomerRealtorMembershipListResult = $GetCustomerRealtorMembershipListResult;
    }

    /**
     * @return GetCustomerRealtorMembershipListResult
     */
    public function getGetCustomerRealtorMembershipListResult()
    {
      return $this->GetCustomerRealtorMembershipListResult;
    }

    /**
     * @param GetCustomerRealtorMembershipListResult $GetCustomerRealtorMembershipListResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerRealtorMembershipListResponse
     */
    public function setGetCustomerRealtorMembershipListResult($GetCustomerRealtorMembershipListResult)
    {
      $this->GetCustomerRealtorMembershipListResult = $GetCustomerRealtorMembershipListResult;
      return $this;
    }

}
