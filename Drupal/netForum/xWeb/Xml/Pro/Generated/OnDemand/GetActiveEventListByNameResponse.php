<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetActiveEventListByNameResponse
{

    /**
     * @var GetActiveEventListByNameResult $GetActiveEventListByNameResult
     */
    protected $GetActiveEventListByNameResult = null;

    /**
     * @param GetActiveEventListByNameResult $GetActiveEventListByNameResult
     */
    public function __construct($GetActiveEventListByNameResult)
    {
      $this->GetActiveEventListByNameResult = $GetActiveEventListByNameResult;
    }

    /**
     * @return GetActiveEventListByNameResult
     */
    public function getGetActiveEventListByNameResult()
    {
      return $this->GetActiveEventListByNameResult;
    }

    /**
     * @param GetActiveEventListByNameResult $GetActiveEventListByNameResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetActiveEventListByNameResponse
     */
    public function setGetActiveEventListByNameResult($GetActiveEventListByNameResult)
    {
      $this->GetActiveEventListByNameResult = $GetActiveEventListByNameResult;
      return $this;
    }

}
