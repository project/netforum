<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetOptOutCategoriesResponse
{

    /**
     * @var GetOptOutCategoriesResult $GetOptOutCategoriesResult
     */
    protected $GetOptOutCategoriesResult = null;

    /**
     * @param GetOptOutCategoriesResult $GetOptOutCategoriesResult
     */
    public function __construct($GetOptOutCategoriesResult)
    {
      $this->GetOptOutCategoriesResult = $GetOptOutCategoriesResult;
    }

    /**
     * @return GetOptOutCategoriesResult
     */
    public function getGetOptOutCategoriesResult()
    {
      return $this->GetOptOutCategoriesResult;
    }

    /**
     * @param GetOptOutCategoriesResult $GetOptOutCategoriesResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetOptOutCategoriesResponse
     */
    public function setGetOptOutCategoriesResult($GetOptOutCategoriesResult)
    {
      $this->GetOptOutCategoriesResult = $GetOptOutCategoriesResult;
      return $this;
    }

}
