<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetSessionListByEventResponse
{

    /**
     * @var GetSessionListByEventResult $GetSessionListByEventResult
     */
    protected $GetSessionListByEventResult = null;

    /**
     * @param GetSessionListByEventResult $GetSessionListByEventResult
     */
    public function __construct($GetSessionListByEventResult)
    {
      $this->GetSessionListByEventResult = $GetSessionListByEventResult;
    }

    /**
     * @return GetSessionListByEventResult
     */
    public function getGetSessionListByEventResult()
    {
      return $this->GetSessionListByEventResult;
    }

    /**
     * @param GetSessionListByEventResult $GetSessionListByEventResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetSessionListByEventResponse
     */
    public function setGetSessionListByEventResult($GetSessionListByEventResult)
    {
      $this->GetSessionListByEventResult = $GetSessionListByEventResult;
      return $this;
    }

}
