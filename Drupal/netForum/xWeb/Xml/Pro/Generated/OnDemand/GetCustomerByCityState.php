<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByCityState
{

    /**
     * @var string $szState
     */
    protected $szState = null;

    /**
     * @var string $szCity
     */
    protected $szCity = null;

    /**
     * @var boolean $bIncludeIndividuals
     */
    protected $bIncludeIndividuals = null;

    /**
     * @var boolean $bIncludeOrganizations
     */
    protected $bIncludeOrganizations = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $szState
     * @param string $szCity
     * @param boolean $bIncludeIndividuals
     * @param boolean $bIncludeOrganizations
     * @param string $szRecordDate
     */
    public function __construct($szState, $szCity, $bIncludeIndividuals, $bIncludeOrganizations, $szRecordDate)
    {
      $this->szState = $szState;
      $this->szCity = $szCity;
      $this->bIncludeIndividuals = $bIncludeIndividuals;
      $this->bIncludeOrganizations = $bIncludeOrganizations;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getSzState()
    {
      return $this->szState;
    }

    /**
     * @param string $szState
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByCityState
     */
    public function setSzState($szState)
    {
      $this->szState = $szState;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzCity()
    {
      return $this->szCity;
    }

    /**
     * @param string $szCity
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByCityState
     */
    public function setSzCity($szCity)
    {
      $this->szCity = $szCity;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBIncludeIndividuals()
    {
      return $this->bIncludeIndividuals;
    }

    /**
     * @param boolean $bIncludeIndividuals
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByCityState
     */
    public function setBIncludeIndividuals($bIncludeIndividuals)
    {
      $this->bIncludeIndividuals = $bIncludeIndividuals;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBIncludeOrganizations()
    {
      return $this->bIncludeOrganizations;
    }

    /**
     * @param boolean $bIncludeOrganizations
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByCityState
     */
    public function setBIncludeOrganizations($bIncludeOrganizations)
    {
      $this->bIncludeOrganizations = $bIncludeOrganizations;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByCityState
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
