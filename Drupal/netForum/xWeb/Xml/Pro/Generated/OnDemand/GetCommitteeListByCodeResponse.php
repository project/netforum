<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCommitteeListByCodeResponse
{

    /**
     * @var GetCommitteeListByCodeResult $GetCommitteeListByCodeResult
     */
    protected $GetCommitteeListByCodeResult = null;

    /**
     * @param GetCommitteeListByCodeResult $GetCommitteeListByCodeResult
     */
    public function __construct($GetCommitteeListByCodeResult)
    {
      $this->GetCommitteeListByCodeResult = $GetCommitteeListByCodeResult;
    }

    /**
     * @return GetCommitteeListByCodeResult
     */
    public function getGetCommitteeListByCodeResult()
    {
      return $this->GetCommitteeListByCodeResult;
    }

    /**
     * @param GetCommitteeListByCodeResult $GetCommitteeListByCodeResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCommitteeListByCodeResponse
     */
    public function setGetCommitteeListByCodeResult($GetCommitteeListByCodeResult)
    {
      $this->GetCommitteeListByCodeResult = $GetCommitteeListByCodeResult;
      return $this;
    }

}
