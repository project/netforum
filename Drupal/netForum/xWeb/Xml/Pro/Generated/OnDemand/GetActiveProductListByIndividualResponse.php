<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetActiveProductListByIndividualResponse
{

    /**
     * @var GetActiveProductListByIndividualResult $GetActiveProductListByIndividualResult
     */
    protected $GetActiveProductListByIndividualResult = null;

    /**
     * @param GetActiveProductListByIndividualResult $GetActiveProductListByIndividualResult
     */
    public function __construct($GetActiveProductListByIndividualResult)
    {
      $this->GetActiveProductListByIndividualResult = $GetActiveProductListByIndividualResult;
    }

    /**
     * @return GetActiveProductListByIndividualResult
     */
    public function getGetActiveProductListByIndividualResult()
    {
      return $this->GetActiveProductListByIndividualResult;
    }

    /**
     * @param GetActiveProductListByIndividualResult $GetActiveProductListByIndividualResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetActiveProductListByIndividualResponse
     */
    public function setGetActiveProductListByIndividualResult($GetActiveProductListByIndividualResult)
    {
      $this->GetActiveProductListByIndividualResult = $GetActiveProductListByIndividualResult;
      return $this;
    }

}
