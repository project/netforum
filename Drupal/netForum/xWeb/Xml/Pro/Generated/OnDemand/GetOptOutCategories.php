<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetOptOutCategories
{

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $szRecordDate
     */
    public function __construct($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetOptOutCategories
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
