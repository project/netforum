<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetOrganizationByDemographic
{

    /**
     * @var string $szDemographicField
     */
    protected $szDemographicField = null;

    /**
     * @var string $szDemographicValue
     */
    protected $szDemographicValue = null;

    /**
     * @param string $szDemographicField
     * @param string $szDemographicValue
     */
    public function __construct($szDemographicField, $szDemographicValue)
    {
      $this->szDemographicField = $szDemographicField;
      $this->szDemographicValue = $szDemographicValue;
    }

    /**
     * @return string
     */
    public function getSzDemographicField()
    {
      return $this->szDemographicField;
    }

    /**
     * @param string $szDemographicField
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetOrganizationByDemographic
     */
    public function setSzDemographicField($szDemographicField)
    {
      $this->szDemographicField = $szDemographicField;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzDemographicValue()
    {
      return $this->szDemographicValue;
    }

    /**
     * @param string $szDemographicValue
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetOrganizationByDemographic
     */
    public function setSzDemographicValue($szDemographicValue)
    {
      $this->szDemographicValue = $szDemographicValue;
      return $this;
    }

}
