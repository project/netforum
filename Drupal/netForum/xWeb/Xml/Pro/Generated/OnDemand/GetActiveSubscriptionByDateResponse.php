<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetActiveSubscriptionByDateResponse
{

    /**
     * @var GetActiveSubscriptionByDateResult $GetActiveSubscriptionByDateResult
     */
    protected $GetActiveSubscriptionByDateResult = null;

    /**
     * @param GetActiveSubscriptionByDateResult $GetActiveSubscriptionByDateResult
     */
    public function __construct($GetActiveSubscriptionByDateResult)
    {
      $this->GetActiveSubscriptionByDateResult = $GetActiveSubscriptionByDateResult;
    }

    /**
     * @return GetActiveSubscriptionByDateResult
     */
    public function getGetActiveSubscriptionByDateResult()
    {
      return $this->GetActiveSubscriptionByDateResult;
    }

    /**
     * @param GetActiveSubscriptionByDateResult $GetActiveSubscriptionByDateResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetActiveSubscriptionByDateResponse
     */
    public function setGetActiveSubscriptionByDateResult($GetActiveSubscriptionByDateResult)
    {
      $this->GetActiveSubscriptionByDateResult = $GetActiveSubscriptionByDateResult;
      return $this;
    }

}
