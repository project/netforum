<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCommitteeListByCstIdResponse
{

    /**
     * @var GetCommitteeListByCstIdResult $GetCommitteeListByCstIdResult
     */
    protected $GetCommitteeListByCstIdResult = null;

    /**
     * @param GetCommitteeListByCstIdResult $GetCommitteeListByCstIdResult
     */
    public function __construct($GetCommitteeListByCstIdResult)
    {
      $this->GetCommitteeListByCstIdResult = $GetCommitteeListByCstIdResult;
    }

    /**
     * @return GetCommitteeListByCstIdResult
     */
    public function getGetCommitteeListByCstIdResult()
    {
      return $this->GetCommitteeListByCstIdResult;
    }

    /**
     * @param GetCommitteeListByCstIdResult $GetCommitteeListByCstIdResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCommitteeListByCstIdResponse
     */
    public function setGetCommitteeListByCstIdResult($GetCommitteeListByCstIdResult)
    {
      $this->GetCommitteeListByCstIdResult = $GetCommitteeListByCstIdResult;
      return $this;
    }

}
