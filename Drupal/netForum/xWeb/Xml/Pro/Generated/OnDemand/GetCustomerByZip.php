<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByZip
{

    /**
     * @var string $zip1
     */
    protected $zip1 = null;

    /**
     * @var string $zip2
     */
    protected $zip2 = null;

    /**
     * @var string $zip3
     */
    protected $zip3 = null;

    /**
     * @var string $zip4
     */
    protected $zip4 = null;

    /**
     * @var string $zip5
     */
    protected $zip5 = null;

    /**
     * @var string $zip6
     */
    protected $zip6 = null;

    /**
     * @var string $zip7
     */
    protected $zip7 = null;

    /**
     * @var string $zip8
     */
    protected $zip8 = null;

    /**
     * @var string $zip9
     */
    protected $zip9 = null;

    /**
     * @var boolean $bIncludeIndividuals
     */
    protected $bIncludeIndividuals = null;

    /**
     * @var boolean $bIncludeOrganizations
     */
    protected $bIncludeOrganizations = null;

    /**
     * @param string $zip1
     * @param string $zip2
     * @param string $zip3
     * @param string $zip4
     * @param string $zip5
     * @param string $zip6
     * @param string $zip7
     * @param string $zip8
     * @param string $zip9
     * @param boolean $bIncludeIndividuals
     * @param boolean $bIncludeOrganizations
     */
    public function __construct($zip1, $zip2, $zip3, $zip4, $zip5, $zip6, $zip7, $zip8, $zip9, $bIncludeIndividuals, $bIncludeOrganizations)
    {
      $this->zip1 = $zip1;
      $this->zip2 = $zip2;
      $this->zip3 = $zip3;
      $this->zip4 = $zip4;
      $this->zip5 = $zip5;
      $this->zip6 = $zip6;
      $this->zip7 = $zip7;
      $this->zip8 = $zip8;
      $this->zip9 = $zip9;
      $this->bIncludeIndividuals = $bIncludeIndividuals;
      $this->bIncludeOrganizations = $bIncludeOrganizations;
    }

    /**
     * @return string
     */
    public function getZip1()
    {
      return $this->zip1;
    }

    /**
     * @param string $zip1
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByZip
     */
    public function setZip1($zip1)
    {
      $this->zip1 = $zip1;
      return $this;
    }

    /**
     * @return string
     */
    public function getZip2()
    {
      return $this->zip2;
    }

    /**
     * @param string $zip2
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByZip
     */
    public function setZip2($zip2)
    {
      $this->zip2 = $zip2;
      return $this;
    }

    /**
     * @return string
     */
    public function getZip3()
    {
      return $this->zip3;
    }

    /**
     * @param string $zip3
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByZip
     */
    public function setZip3($zip3)
    {
      $this->zip3 = $zip3;
      return $this;
    }

    /**
     * @return string
     */
    public function getZip4()
    {
      return $this->zip4;
    }

    /**
     * @param string $zip4
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByZip
     */
    public function setZip4($zip4)
    {
      $this->zip4 = $zip4;
      return $this;
    }

    /**
     * @return string
     */
    public function getZip5()
    {
      return $this->zip5;
    }

    /**
     * @param string $zip5
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByZip
     */
    public function setZip5($zip5)
    {
      $this->zip5 = $zip5;
      return $this;
    }

    /**
     * @return string
     */
    public function getZip6()
    {
      return $this->zip6;
    }

    /**
     * @param string $zip6
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByZip
     */
    public function setZip6($zip6)
    {
      $this->zip6 = $zip6;
      return $this;
    }

    /**
     * @return string
     */
    public function getZip7()
    {
      return $this->zip7;
    }

    /**
     * @param string $zip7
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByZip
     */
    public function setZip7($zip7)
    {
      $this->zip7 = $zip7;
      return $this;
    }

    /**
     * @return string
     */
    public function getZip8()
    {
      return $this->zip8;
    }

    /**
     * @param string $zip8
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByZip
     */
    public function setZip8($zip8)
    {
      $this->zip8 = $zip8;
      return $this;
    }

    /**
     * @return string
     */
    public function getZip9()
    {
      return $this->zip9;
    }

    /**
     * @param string $zip9
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByZip
     */
    public function setZip9($zip9)
    {
      $this->zip9 = $zip9;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBIncludeIndividuals()
    {
      return $this->bIncludeIndividuals;
    }

    /**
     * @param boolean $bIncludeIndividuals
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByZip
     */
    public function setBIncludeIndividuals($bIncludeIndividuals)
    {
      $this->bIncludeIndividuals = $bIncludeIndividuals;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBIncludeOrganizations()
    {
      return $this->bIncludeOrganizations;
    }

    /**
     * @param boolean $bIncludeOrganizations
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByZip
     */
    public function setBIncludeOrganizations($bIncludeOrganizations)
    {
      $this->bIncludeOrganizations = $bIncludeOrganizations;
      return $this;
    }

}
