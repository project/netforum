<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetRoomByLocation
{

    /**
     * @var string $szLocKey
     */
    protected $szLocKey = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $szLocKey
     * @param string $szRecordDate
     */
    public function __construct($szLocKey, $szRecordDate)
    {
      $this->szLocKey = $szLocKey;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getSzLocKey()
    {
      return $this->szLocKey;
    }

    /**
     * @param string $szLocKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetRoomByLocation
     */
    public function setSzLocKey($szLocKey)
    {
      $this->szLocKey = $szLocKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetRoomByLocation
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
