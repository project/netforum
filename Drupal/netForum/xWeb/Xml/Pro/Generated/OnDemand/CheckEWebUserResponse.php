<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class CheckEWebUserResponse
{

    /**
     * @var CheckEWebUserResult $CheckEWebUserResult
     */
    protected $CheckEWebUserResult = null;

    /**
     * @param CheckEWebUserResult $CheckEWebUserResult
     */
    public function __construct($CheckEWebUserResult)
    {
      $this->CheckEWebUserResult = $CheckEWebUserResult;
    }

    /**
     * @return CheckEWebUserResult
     */
    public function getCheckEWebUserResult()
    {
      return $this->CheckEWebUserResult;
    }

    /**
     * @param CheckEWebUserResult $CheckEWebUserResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\CheckEWebUserResponse
     */
    public function setCheckEWebUserResult($CheckEWebUserResult)
    {
      $this->CheckEWebUserResult = $CheckEWebUserResult;
      return $this;
    }

}
