<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetEventListByKeyResponse
{

    /**
     * @var GetEventListByKeyResult $GetEventListByKeyResult
     */
    protected $GetEventListByKeyResult = null;

    /**
     * @param GetEventListByKeyResult $GetEventListByKeyResult
     */
    public function __construct($GetEventListByKeyResult)
    {
      $this->GetEventListByKeyResult = $GetEventListByKeyResult;
    }

    /**
     * @return GetEventListByKeyResult
     */
    public function getGetEventListByKeyResult()
    {
      return $this->GetEventListByKeyResult;
    }

    /**
     * @param GetEventListByKeyResult $GetEventListByKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetEventListByKeyResponse
     */
    public function setGetEventListByKeyResult($GetEventListByKeyResult)
    {
      $this->GetEventListByKeyResult = $GetEventListByKeyResult;
      return $this;
    }

}
