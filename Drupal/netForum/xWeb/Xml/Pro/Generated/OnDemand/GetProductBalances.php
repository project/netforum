<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetProductBalances
{

    /**
     * @var string $szCstKey
     */
    protected $szCstKey = null;

    /**
     * @var string $szPrdKey
     */
    protected $szPrdKey = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $szCstKey
     * @param string $szPrdKey
     * @param string $szRecordDate
     */
    public function __construct($szCstKey, $szPrdKey, $szRecordDate)
    {
      $this->szCstKey = $szCstKey;
      $this->szPrdKey = $szPrdKey;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getSzCstKey()
    {
      return $this->szCstKey;
    }

    /**
     * @param string $szCstKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetProductBalances
     */
    public function setSzCstKey($szCstKey)
    {
      $this->szCstKey = $szCstKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzPrdKey()
    {
      return $this->szPrdKey;
    }

    /**
     * @param string $szPrdKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetProductBalances
     */
    public function setSzPrdKey($szPrdKey)
    {
      $this->szPrdKey = $szPrdKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetProductBalances
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
