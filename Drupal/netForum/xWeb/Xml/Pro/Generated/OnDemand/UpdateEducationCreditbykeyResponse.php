<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class UpdateEducationCreditbykeyResponse
{

    /**
     * @var UpdateEducationCreditbykeyResult $UpdateEducationCreditbykeyResult
     */
    protected $UpdateEducationCreditbykeyResult = null;

    /**
     * @param UpdateEducationCreditbykeyResult $UpdateEducationCreditbykeyResult
     */
    public function __construct($UpdateEducationCreditbykeyResult)
    {
      $this->UpdateEducationCreditbykeyResult = $UpdateEducationCreditbykeyResult;
    }

    /**
     * @return UpdateEducationCreditbykeyResult
     */
    public function getUpdateEducationCreditbykeyResult()
    {
      return $this->UpdateEducationCreditbykeyResult;
    }

    /**
     * @param UpdateEducationCreditbykeyResult $UpdateEducationCreditbykeyResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\UpdateEducationCreditbykeyResponse
     */
    public function setUpdateEducationCreditbykeyResult($UpdateEducationCreditbykeyResult)
    {
      $this->UpdateEducationCreditbykeyResult = $UpdateEducationCreditbykeyResult;
      return $this;
    }

}
