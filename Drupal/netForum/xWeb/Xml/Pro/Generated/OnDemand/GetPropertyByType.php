<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetPropertyByType
{

    /**
     * @var string $szType
     */
    protected $szType = null;

    /**
     * @param string $szType
     */
    public function __construct($szType)
    {
      $this->szType = $szType;
    }

    /**
     * @return string
     */
    public function getSzType()
    {
      return $this->szType;
    }

    /**
     * @param string $szType
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetPropertyByType
     */
    public function setSzType($szType)
    {
      $this->szType = $szType;
      return $this;
    }

}
