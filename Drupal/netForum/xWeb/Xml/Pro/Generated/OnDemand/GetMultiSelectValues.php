<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetMultiSelectValues
{

    /**
     * @var string $szType
     */
    protected $szType = null;

    /**
     * @var int $iNumber
     */
    protected $iNumber = null;

    /**
     * @param string $szType
     * @param int $iNumber
     */
    public function __construct($szType, $iNumber)
    {
      $this->szType = $szType;
      $this->iNumber = $iNumber;
    }

    /**
     * @return string
     */
    public function getSzType()
    {
      return $this->szType;
    }

    /**
     * @param string $szType
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetMultiSelectValues
     */
    public function setSzType($szType)
    {
      $this->szType = $szType;
      return $this;
    }

    /**
     * @return int
     */
    public function getINumber()
    {
      return $this->iNumber;
    }

    /**
     * @param int $iNumber
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetMultiSelectValues
     */
    public function setINumber($iNumber)
    {
      $this->iNumber = $iNumber;
      return $this;
    }

}
