<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetRelationshipsByIndiviual
{

    /**
     * @var string $cstKey
     */
    protected $cstKey = null;

    /**
     * @param string $cstKey
     */
    public function __construct($cstKey)
    {
      $this->cstKey = $cstKey;
    }

    /**
     * @return string
     */
    public function getCstKey()
    {
      return $this->cstKey;
    }

    /**
     * @param string $cstKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetRelationshipsByIndiviual
     */
    public function setCstKey($cstKey)
    {
      $this->cstKey = $cstKey;
      return $this;
    }

}
