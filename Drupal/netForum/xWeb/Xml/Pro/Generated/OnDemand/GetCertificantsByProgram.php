<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCertificantsByProgram
{

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @var string $szPrdCode
     */
    protected $szPrdCode = null;

    /**
     * @var string $szStatus
     */
    protected $szStatus = null;

    /**
     * @param string $szRecordDate
     * @param string $szPrdCode
     * @param string $szStatus
     */
    public function __construct($szRecordDate, $szPrdCode, $szStatus)
    {
      $this->szRecordDate = $szRecordDate;
      $this->szPrdCode = $szPrdCode;
      $this->szStatus = $szStatus;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCertificantsByProgram
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzPrdCode()
    {
      return $this->szPrdCode;
    }

    /**
     * @param string $szPrdCode
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCertificantsByProgram
     */
    public function setSzPrdCode($szPrdCode)
    {
      $this->szPrdCode = $szPrdCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzStatus()
    {
      return $this->szStatus;
    }

    /**
     * @param string $szStatus
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCertificantsByProgram
     */
    public function setSzStatus($szStatus)
    {
      $this->szStatus = $szStatus;
      return $this;
    }

}
