<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetActiveEventListByDateResponse
{

    /**
     * @var GetActiveEventListByDateResult $GetActiveEventListByDateResult
     */
    protected $GetActiveEventListByDateResult = null;

    /**
     * @param GetActiveEventListByDateResult $GetActiveEventListByDateResult
     */
    public function __construct($GetActiveEventListByDateResult)
    {
      $this->GetActiveEventListByDateResult = $GetActiveEventListByDateResult;
    }

    /**
     * @return GetActiveEventListByDateResult
     */
    public function getGetActiveEventListByDateResult()
    {
      return $this->GetActiveEventListByDateResult;
    }

    /**
     * @param GetActiveEventListByDateResult $GetActiveEventListByDateResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetActiveEventListByDateResponse
     */
    public function setGetActiveEventListByDateResult($GetActiveEventListByDateResult)
    {
      $this->GetActiveEventListByDateResult = $GetActiveEventListByDateResult;
      return $this;
    }

}
