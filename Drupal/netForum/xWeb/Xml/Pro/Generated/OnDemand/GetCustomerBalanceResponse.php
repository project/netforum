<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerBalanceResponse
{

    /**
     * @var GetCustomerBalanceResult $GetCustomerBalanceResult
     */
    protected $GetCustomerBalanceResult = null;

    /**
     * @param GetCustomerBalanceResult $GetCustomerBalanceResult
     */
    public function __construct($GetCustomerBalanceResult)
    {
      $this->GetCustomerBalanceResult = $GetCustomerBalanceResult;
    }

    /**
     * @return GetCustomerBalanceResult
     */
    public function getGetCustomerBalanceResult()
    {
      return $this->GetCustomerBalanceResult;
    }

    /**
     * @param GetCustomerBalanceResult $GetCustomerBalanceResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerBalanceResponse
     */
    public function setGetCustomerBalanceResult($GetCustomerBalanceResult)
    {
      $this->GetCustomerBalanceResult = $GetCustomerBalanceResult;
      return $this;
    }

}
