<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetActiveProductListByIndividual
{

    /**
     * @var string $cstid
     */
    protected $cstid = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $cstid
     * @param string $szRecordDate
     */
    public function __construct($cstid, $szRecordDate)
    {
      $this->cstid = $cstid;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getCstid()
    {
      return $this->cstid;
    }

    /**
     * @param string $cstid
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetActiveProductListByIndividual
     */
    public function setCstid($cstid)
    {
      $this->cstid = $cstid;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetActiveProductListByIndividual
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
