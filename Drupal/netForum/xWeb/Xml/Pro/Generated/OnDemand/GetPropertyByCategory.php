<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetPropertyByCategory
{

    /**
     * @var string $szCategory
     */
    protected $szCategory = null;

    /**
     * @param string $szCategory
     */
    public function __construct($szCategory)
    {
      $this->szCategory = $szCategory;
    }

    /**
     * @return string
     */
    public function getSzCategory()
    {
      return $this->szCategory;
    }

    /**
     * @param string $szCategory
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetPropertyByCategory
     */
    public function setSzCategory($szCategory)
    {
      $this->szCategory = $szCategory;
      return $this;
    }

}
