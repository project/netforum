<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetEventCustomerList
{

    /**
     * @var string $szEventKey
     */
    protected $szEventKey = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $szEventKey
     * @param string $szRecordDate
     */
    public function __construct($szEventKey, $szRecordDate)
    {
      $this->szEventKey = $szEventKey;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getSzEventKey()
    {
      return $this->szEventKey;
    }

    /**
     * @param string $szEventKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetEventCustomerList
     */
    public function setSzEventKey($szEventKey)
    {
      $this->szEventKey = $szEventKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetEventCustomerList
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
