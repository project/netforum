<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetAwardJudgesResponse
{

    /**
     * @var GetAwardJudgesResult $GetAwardJudgesResult
     */
    protected $GetAwardJudgesResult = null;

    /**
     * @param GetAwardJudgesResult $GetAwardJudgesResult
     */
    public function __construct($GetAwardJudgesResult)
    {
      $this->GetAwardJudgesResult = $GetAwardJudgesResult;
    }

    /**
     * @return GetAwardJudgesResult
     */
    public function getGetAwardJudgesResult()
    {
      return $this->GetAwardJudgesResult;
    }

    /**
     * @param GetAwardJudgesResult $GetAwardJudgesResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetAwardJudgesResponse
     */
    public function setGetAwardJudgesResult($GetAwardJudgesResult)
    {
      $this->GetAwardJudgesResult = $GetAwardJudgesResult;
      return $this;
    }

}
