<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetActiveProductListByCustomerResponse
{

    /**
     * @var GetActiveProductListByCustomerResult $GetActiveProductListByCustomerResult
     */
    protected $GetActiveProductListByCustomerResult = null;

    /**
     * @param GetActiveProductListByCustomerResult $GetActiveProductListByCustomerResult
     */
    public function __construct($GetActiveProductListByCustomerResult)
    {
      $this->GetActiveProductListByCustomerResult = $GetActiveProductListByCustomerResult;
    }

    /**
     * @return GetActiveProductListByCustomerResult
     */
    public function getGetActiveProductListByCustomerResult()
    {
      return $this->GetActiveProductListByCustomerResult;
    }

    /**
     * @param GetActiveProductListByCustomerResult $GetActiveProductListByCustomerResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetActiveProductListByCustomerResponse
     */
    public function setGetActiveProductListByCustomerResult($GetActiveProductListByCustomerResult)
    {
      $this->GetActiveProductListByCustomerResult = $GetActiveProductListByCustomerResult;
      return $this;
    }

}
