<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class AddIndOrgRelationship
{

    /**
     * @var string $OrgKey
     */
    protected $OrgKey = null;

    /**
     * @var string $IndKey
     */
    protected $IndKey = null;

    /**
     * @var string $rltCode
     */
    protected $rltCode = null;

    /**
     * @var string $rltTitle
     */
    protected $rltTitle = null;

    /**
     * @var string $startDate
     */
    protected $startDate = null;

    /**
     * @var string $endDate
     */
    protected $endDate = null;

    /**
     * @param string $OrgKey
     * @param string $IndKey
     * @param string $rltCode
     * @param string $rltTitle
     * @param string $startDate
     * @param string $endDate
     */
    public function __construct($OrgKey, $IndKey, $rltCode, $rltTitle, $startDate, $endDate)
    {
      $this->OrgKey = $OrgKey;
      $this->IndKey = $IndKey;
      $this->rltCode = $rltCode;
      $this->rltTitle = $rltTitle;
      $this->startDate = $startDate;
      $this->endDate = $endDate;
    }

    /**
     * @return string
     */
    public function getOrgKey()
    {
      return $this->OrgKey;
    }

    /**
     * @param string $OrgKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\AddIndOrgRelationship
     */
    public function setOrgKey($OrgKey)
    {
      $this->OrgKey = $OrgKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getIndKey()
    {
      return $this->IndKey;
    }

    /**
     * @param string $IndKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\AddIndOrgRelationship
     */
    public function setIndKey($IndKey)
    {
      $this->IndKey = $IndKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getRltCode()
    {
      return $this->rltCode;
    }

    /**
     * @param string $rltCode
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\AddIndOrgRelationship
     */
    public function setRltCode($rltCode)
    {
      $this->rltCode = $rltCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRltTitle()
    {
      return $this->rltTitle;
    }

    /**
     * @param string $rltTitle
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\AddIndOrgRelationship
     */
    public function setRltTitle($rltTitle)
    {
      $this->rltTitle = $rltTitle;
      return $this;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
      return $this->startDate;
    }

    /**
     * @param string $startDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\AddIndOrgRelationship
     */
    public function setStartDate($startDate)
    {
      $this->startDate = $startDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getEndDate()
    {
      return $this->endDate;
    }

    /**
     * @param string $endDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\AddIndOrgRelationship
     */
    public function setEndDate($endDate)
    {
      $this->endDate = $endDate;
      return $this;
    }

}
