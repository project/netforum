<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByRadiusLatLon
{

    /**
     * @var string $szLat
     */
    protected $szLat = null;

    /**
     * @var string $szLon
     */
    protected $szLon = null;

    /**
     * @var int $iRadius
     */
    protected $iRadius = null;

    /**
     * @var boolean $bIncludeIndividual
     */
    protected $bIncludeIndividual = null;

    /**
     * @var boolean $bIncludeOrganization
     */
    protected $bIncludeOrganization = null;

    /**
     * @var string $szRecordDate
     */
    protected $szRecordDate = null;

    /**
     * @param string $szLat
     * @param string $szLon
     * @param int $iRadius
     * @param boolean $bIncludeIndividual
     * @param boolean $bIncludeOrganization
     * @param string $szRecordDate
     */
    public function __construct($szLat, $szLon, $iRadius, $bIncludeIndividual, $bIncludeOrganization, $szRecordDate)
    {
      $this->szLat = $szLat;
      $this->szLon = $szLon;
      $this->iRadius = $iRadius;
      $this->bIncludeIndividual = $bIncludeIndividual;
      $this->bIncludeOrganization = $bIncludeOrganization;
      $this->szRecordDate = $szRecordDate;
    }

    /**
     * @return string
     */
    public function getSzLat()
    {
      return $this->szLat;
    }

    /**
     * @param string $szLat
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRadiusLatLon
     */
    public function setSzLat($szLat)
    {
      $this->szLat = $szLat;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzLon()
    {
      return $this->szLon;
    }

    /**
     * @param string $szLon
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRadiusLatLon
     */
    public function setSzLon($szLon)
    {
      $this->szLon = $szLon;
      return $this;
    }

    /**
     * @return int
     */
    public function getIRadius()
    {
      return $this->iRadius;
    }

    /**
     * @param int $iRadius
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRadiusLatLon
     */
    public function setIRadius($iRadius)
    {
      $this->iRadius = $iRadius;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBIncludeIndividual()
    {
      return $this->bIncludeIndividual;
    }

    /**
     * @param boolean $bIncludeIndividual
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRadiusLatLon
     */
    public function setBIncludeIndividual($bIncludeIndividual)
    {
      $this->bIncludeIndividual = $bIncludeIndividual;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBIncludeOrganization()
    {
      return $this->bIncludeOrganization;
    }

    /**
     * @param boolean $bIncludeOrganization
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRadiusLatLon
     */
    public function setBIncludeOrganization($bIncludeOrganization)
    {
      $this->bIncludeOrganization = $bIncludeOrganization;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzRecordDate()
    {
      return $this->szRecordDate;
    }

    /**
     * @param string $szRecordDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByRadiusLatLon
     */
    public function setSzRecordDate($szRecordDate)
    {
      $this->szRecordDate = $szRecordDate;
      return $this;
    }

}
