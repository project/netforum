<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByCityStateMembershipResponse
{

    /**
     * @var GetCustomerByCityStateMembershipResult $GetCustomerByCityStateMembershipResult
     */
    protected $GetCustomerByCityStateMembershipResult = null;

    /**
     * @param GetCustomerByCityStateMembershipResult $GetCustomerByCityStateMembershipResult
     */
    public function __construct($GetCustomerByCityStateMembershipResult)
    {
      $this->GetCustomerByCityStateMembershipResult = $GetCustomerByCityStateMembershipResult;
    }

    /**
     * @return GetCustomerByCityStateMembershipResult
     */
    public function getGetCustomerByCityStateMembershipResult()
    {
      return $this->GetCustomerByCityStateMembershipResult;
    }

    /**
     * @param GetCustomerByCityStateMembershipResult $GetCustomerByCityStateMembershipResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByCityStateMembershipResponse
     */
    public function setGetCustomerByCityStateMembershipResult($GetCustomerByCityStateMembershipResult)
    {
      $this->GetCustomerByCityStateMembershipResult = $GetCustomerByCityStateMembershipResult;
      return $this;
    }

}
