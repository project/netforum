<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class NewOrganizationInformationResponse
{

    /**
     * @var NewOrganizationInformationResult $NewOrganizationInformationResult
     */
    protected $NewOrganizationInformationResult = null;

    /**
     * @param NewOrganizationInformationResult $NewOrganizationInformationResult
     */
    public function __construct($NewOrganizationInformationResult)
    {
      $this->NewOrganizationInformationResult = $NewOrganizationInformationResult;
    }

    /**
     * @return NewOrganizationInformationResult
     */
    public function getNewOrganizationInformationResult()
    {
      return $this->NewOrganizationInformationResult;
    }

    /**
     * @param NewOrganizationInformationResult $NewOrganizationInformationResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\NewOrganizationInformationResponse
     */
    public function setNewOrganizationInformationResult($NewOrganizationInformationResult)
    {
      $this->NewOrganizationInformationResult = $NewOrganizationInformationResult;
      return $this;
    }

}
