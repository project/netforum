<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByIdResponse
{

    /**
     * @var GetCustomerByIdResult $GetCustomerByIdResult
     */
    protected $GetCustomerByIdResult = null;

    /**
     * @param GetCustomerByIdResult $GetCustomerByIdResult
     */
    public function __construct($GetCustomerByIdResult)
    {
      $this->GetCustomerByIdResult = $GetCustomerByIdResult;
    }

    /**
     * @return GetCustomerByIdResult
     */
    public function getGetCustomerByIdResult()
    {
      return $this->GetCustomerByIdResult;
    }

    /**
     * @param GetCustomerByIdResult $GetCustomerByIdResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByIdResponse
     */
    public function setGetCustomerByIdResult($GetCustomerByIdResult)
    {
      $this->GetCustomerByIdResult = $GetCustomerByIdResult;
      return $this;
    }

}
