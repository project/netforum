<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerByWebsite
{

    /**
     * @var string $szWebsite
     */
    protected $szWebsite = null;

    /**
     * @var string $szIncludeCustomersWithName
     */
    protected $szIncludeCustomersWithName = null;

    /**
     * @var boolean $bIncludeIndividuals
     */
    protected $bIncludeIndividuals = null;

    /**
     * @var boolean $bIncludeOrganizations
     */
    protected $bIncludeOrganizations = null;

    /**
     * @var boolean $bMembersOnly
     */
    protected $bMembersOnly = null;

    /**
     * @param string $szWebsite
     * @param string $szIncludeCustomersWithName
     * @param boolean $bIncludeIndividuals
     * @param boolean $bIncludeOrganizations
     * @param boolean $bMembersOnly
     */
    public function __construct($szWebsite, $szIncludeCustomersWithName, $bIncludeIndividuals, $bIncludeOrganizations, $bMembersOnly)
    {
      $this->szWebsite = $szWebsite;
      $this->szIncludeCustomersWithName = $szIncludeCustomersWithName;
      $this->bIncludeIndividuals = $bIncludeIndividuals;
      $this->bIncludeOrganizations = $bIncludeOrganizations;
      $this->bMembersOnly = $bMembersOnly;
    }

    /**
     * @return string
     */
    public function getSzWebsite()
    {
      return $this->szWebsite;
    }

    /**
     * @param string $szWebsite
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByWebsite
     */
    public function setSzWebsite($szWebsite)
    {
      $this->szWebsite = $szWebsite;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzIncludeCustomersWithName()
    {
      return $this->szIncludeCustomersWithName;
    }

    /**
     * @param string $szIncludeCustomersWithName
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByWebsite
     */
    public function setSzIncludeCustomersWithName($szIncludeCustomersWithName)
    {
      $this->szIncludeCustomersWithName = $szIncludeCustomersWithName;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBIncludeIndividuals()
    {
      return $this->bIncludeIndividuals;
    }

    /**
     * @param boolean $bIncludeIndividuals
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByWebsite
     */
    public function setBIncludeIndividuals($bIncludeIndividuals)
    {
      $this->bIncludeIndividuals = $bIncludeIndividuals;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBIncludeOrganizations()
    {
      return $this->bIncludeOrganizations;
    }

    /**
     * @param boolean $bIncludeOrganizations
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByWebsite
     */
    public function setBIncludeOrganizations($bIncludeOrganizations)
    {
      $this->bIncludeOrganizations = $bIncludeOrganizations;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBMembersOnly()
    {
      return $this->bMembersOnly;
    }

    /**
     * @param boolean $bMembersOnly
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerByWebsite
     */
    public function setBMembersOnly($bMembersOnly)
    {
      $this->bMembersOnly = $bMembersOnly;
      return $this;
    }

}
