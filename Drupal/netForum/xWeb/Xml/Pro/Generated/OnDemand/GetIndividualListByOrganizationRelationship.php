<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetIndividualListByOrganizationRelationship
{

    /**
     * @var string $organizationKey
     */
    protected $organizationKey = null;

    /**
     * @var string $typeCode
     */
    protected $typeCode = null;

    /**
     * @var boolean $bMembersOnly
     */
    protected $bMembersOnly = null;

    /**
     * @var string $startIndex
     */
    protected $startIndex = null;

    /**
     * @param string $organizationKey
     * @param string $typeCode
     * @param boolean $bMembersOnly
     * @param string $startIndex
     */
    public function __construct($organizationKey, $typeCode, $bMembersOnly, $startIndex)
    {
      $this->organizationKey = $organizationKey;
      $this->typeCode = $typeCode;
      $this->bMembersOnly = $bMembersOnly;
      $this->startIndex = $startIndex;
    }

    /**
     * @return string
     */
    public function getOrganizationKey()
    {
      return $this->organizationKey;
    }

    /**
     * @param string $organizationKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetIndividualListByOrganizationRelationship
     */
    public function setOrganizationKey($organizationKey)
    {
      $this->organizationKey = $organizationKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getTypeCode()
    {
      return $this->typeCode;
    }

    /**
     * @param string $typeCode
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetIndividualListByOrganizationRelationship
     */
    public function setTypeCode($typeCode)
    {
      $this->typeCode = $typeCode;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBMembersOnly()
    {
      return $this->bMembersOnly;
    }

    /**
     * @param boolean $bMembersOnly
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetIndividualListByOrganizationRelationship
     */
    public function setBMembersOnly($bMembersOnly)
    {
      $this->bMembersOnly = $bMembersOnly;
      return $this;
    }

    /**
     * @return string
     */
    public function getStartIndex()
    {
      return $this->startIndex;
    }

    /**
     * @param string $startIndex
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetIndividualListByOrganizationRelationship
     */
    public function setStartIndex($startIndex)
    {
      $this->startIndex = $startIndex;
      return $this;
    }

}
