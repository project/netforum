<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class AddCustomerRelationship
{

    /**
     * @var string $ParentKey
     */
    protected $ParentKey = null;

    /**
     * @var string $ChildKey
     */
    protected $ChildKey = null;

    /**
     * @var string $rltCode
     */
    protected $rltCode = null;

    /**
     * @var string $startDate
     */
    protected $startDate = null;

    /**
     * @var string $endDate
     */
    protected $endDate = null;

    /**
     * @param string $ParentKey
     * @param string $ChildKey
     * @param string $rltCode
     * @param string $startDate
     * @param string $endDate
     */
    public function __construct($ParentKey, $ChildKey, $rltCode, $startDate, $endDate)
    {
      $this->ParentKey = $ParentKey;
      $this->ChildKey = $ChildKey;
      $this->rltCode = $rltCode;
      $this->startDate = $startDate;
      $this->endDate = $endDate;
    }

    /**
     * @return string
     */
    public function getParentKey()
    {
      return $this->ParentKey;
    }

    /**
     * @param string $ParentKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\AddCustomerRelationship
     */
    public function setParentKey($ParentKey)
    {
      $this->ParentKey = $ParentKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getChildKey()
    {
      return $this->ChildKey;
    }

    /**
     * @param string $ChildKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\AddCustomerRelationship
     */
    public function setChildKey($ChildKey)
    {
      $this->ChildKey = $ChildKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getRltCode()
    {
      return $this->rltCode;
    }

    /**
     * @param string $rltCode
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\AddCustomerRelationship
     */
    public function setRltCode($rltCode)
    {
      $this->rltCode = $rltCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
      return $this->startDate;
    }

    /**
     * @param string $startDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\AddCustomerRelationship
     */
    public function setStartDate($startDate)
    {
      $this->startDate = $startDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getEndDate()
    {
      return $this->endDate;
    }

    /**
     * @param string $endDate
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\AddCustomerRelationship
     */
    public function setEndDate($endDate)
    {
      $this->endDate = $endDate;
      return $this;
    }

}
