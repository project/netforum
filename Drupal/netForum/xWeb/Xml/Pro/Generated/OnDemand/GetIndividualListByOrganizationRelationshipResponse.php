<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetIndividualListByOrganizationRelationshipResponse
{

    /**
     * @var IndexedResultListOfIndividualRelationshipResult $GetIndividualListByOrganizationRelationshipResult
     */
    protected $GetIndividualListByOrganizationRelationshipResult = null;

    /**
     * @param IndexedResultListOfIndividualRelationshipResult $GetIndividualListByOrganizationRelationshipResult
     */
    public function __construct($GetIndividualListByOrganizationRelationshipResult)
    {
      $this->GetIndividualListByOrganizationRelationshipResult = $GetIndividualListByOrganizationRelationshipResult;
    }

    /**
     * @return IndexedResultListOfIndividualRelationshipResult
     */
    public function getGetIndividualListByOrganizationRelationshipResult()
    {
      return $this->GetIndividualListByOrganizationRelationshipResult;
    }

    /**
     * @param IndexedResultListOfIndividualRelationshipResult $GetIndividualListByOrganizationRelationshipResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetIndividualListByOrganizationRelationshipResponse
     */
    public function setGetIndividualListByOrganizationRelationshipResult($GetIndividualListByOrganizationRelationshipResult)
    {
      $this->GetIndividualListByOrganizationRelationshipResult = $GetIndividualListByOrganizationRelationshipResult;
      return $this;
    }

}
