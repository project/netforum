<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetRoomByLocationResponse
{

    /**
     * @var GetRoomByLocationResult $GetRoomByLocationResult
     */
    protected $GetRoomByLocationResult = null;

    /**
     * @param GetRoomByLocationResult $GetRoomByLocationResult
     */
    public function __construct($GetRoomByLocationResult)
    {
      $this->GetRoomByLocationResult = $GetRoomByLocationResult;
    }

    /**
     * @return GetRoomByLocationResult
     */
    public function getGetRoomByLocationResult()
    {
      return $this->GetRoomByLocationResult;
    }

    /**
     * @param GetRoomByLocationResult $GetRoomByLocationResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetRoomByLocationResponse
     */
    public function setGetRoomByLocationResult($GetRoomByLocationResult)
    {
      $this->GetRoomByLocationResult = $GetRoomByLocationResult;
      return $this;
    }

}
