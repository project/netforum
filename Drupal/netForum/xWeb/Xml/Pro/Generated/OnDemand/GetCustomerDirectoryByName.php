<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand;

class GetCustomerDirectoryByName
{

    /**
     * @var string $szName
     */
    protected $szName = null;

    /**
     * @var boolean $bIncludeIndividuals
     */
    protected $bIncludeIndividuals = null;

    /**
     * @var boolean $bIncludeOrganizations
     */
    protected $bIncludeOrganizations = null;

    /**
     * @var boolean $bMembersOnly
     */
    protected $bMembersOnly = null;

    /**
     * @param string $szName
     * @param boolean $bIncludeIndividuals
     * @param boolean $bIncludeOrganizations
     * @param boolean $bMembersOnly
     */
    public function __construct($szName, $bIncludeIndividuals, $bIncludeOrganizations, $bMembersOnly)
    {
      $this->szName = $szName;
      $this->bIncludeIndividuals = $bIncludeIndividuals;
      $this->bIncludeOrganizations = $bIncludeOrganizations;
      $this->bMembersOnly = $bMembersOnly;
    }

    /**
     * @return string
     */
    public function getSzName()
    {
      return $this->szName;
    }

    /**
     * @param string $szName
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerDirectoryByName
     */
    public function setSzName($szName)
    {
      $this->szName = $szName;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBIncludeIndividuals()
    {
      return $this->bIncludeIndividuals;
    }

    /**
     * @param boolean $bIncludeIndividuals
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerDirectoryByName
     */
    public function setBIncludeIndividuals($bIncludeIndividuals)
    {
      $this->bIncludeIndividuals = $bIncludeIndividuals;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBIncludeOrganizations()
    {
      return $this->bIncludeOrganizations;
    }

    /**
     * @param boolean $bIncludeOrganizations
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerDirectoryByName
     */
    public function setBIncludeOrganizations($bIncludeOrganizations)
    {
      $this->bIncludeOrganizations = $bIncludeOrganizations;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBMembersOnly()
    {
      return $this->bMembersOnly;
    }

    /**
     * @param boolean $bMembersOnly
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\OnDemand\GetCustomerDirectoryByName
     */
    public function setBMembersOnly($bMembersOnly)
    {
      $this->bMembersOnly = $bMembersOnly;
      return $this;
    }

}
