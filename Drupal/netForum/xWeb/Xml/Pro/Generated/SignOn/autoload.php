<?php


 function autoload_9d12db0853aec5c8b7674bbf0a4d8492($class)
{
    $classes = array(
        'Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\SignOn' => __DIR__ .'/SignOn.php',
        'Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\Authenticate' => __DIR__ .'/Authenticate.php',
        'Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\AuthenticateResponse' => __DIR__ .'/AuthenticateResponse.php',
        'Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\GetSignOnToken' => __DIR__ .'/GetSignOnToken.php',
        'Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\GetSignOnTokenResponse' => __DIR__ .'/GetSignOnTokenResponse.php',
        'Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\GetCstKeyFromSignOnToken' => __DIR__ .'/GetCstKeyFromSignOnToken.php',
        'Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\GetCstKeyFromSignOnTokenResponse' => __DIR__ .'/GetCstKeyFromSignOnTokenResponse.php',
        'Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\LogOutCst' => __DIR__ .'/LogOutCst.php',
        'Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\LogOutCstResponse' => __DIR__ .'/LogOutCstResponse.php',
        'Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\LogOutToken' => __DIR__ .'/LogOutToken.php',
        'Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\LogOutTokenResponse' => __DIR__ .'/LogOutTokenResponse.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_9d12db0853aec5c8b7674bbf0a4d8492');

// Do nothing. The rest is just leftovers from the code generation.
{
}
