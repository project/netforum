<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn;

class LogOutToken
{

    /**
     * @var string $AuthToken
     */
    protected $AuthToken = null;

    /**
     * @var string $szToken
     */
    protected $szToken = null;

    /**
     * @param string $AuthToken
     * @param string $szToken
     */
    public function __construct($AuthToken, $szToken)
    {
      $this->AuthToken = $AuthToken;
      $this->szToken = $szToken;
    }

    /**
     * @return string
     */
    public function getAuthToken()
    {
      return $this->AuthToken;
    }

    /**
     * @param string $AuthToken
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\LogOutToken
     */
    public function setAuthToken($AuthToken)
    {
      $this->AuthToken = $AuthToken;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzToken()
    {
      return $this->szToken;
    }

    /**
     * @param string $szToken
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\LogOutToken
     */
    public function setSzToken($szToken)
    {
      $this->szToken = $szToken;
      return $this;
    }

}
