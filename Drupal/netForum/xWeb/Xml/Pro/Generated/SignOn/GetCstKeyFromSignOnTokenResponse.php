<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn;

class GetCstKeyFromSignOnTokenResponse
{

    /**
     * @var string $GetCstKeyFromSignOnTokenResult
     */
    protected $GetCstKeyFromSignOnTokenResult = null;

    /**
     * @param string $GetCstKeyFromSignOnTokenResult
     */
    public function __construct($GetCstKeyFromSignOnTokenResult)
    {
      $this->GetCstKeyFromSignOnTokenResult = $GetCstKeyFromSignOnTokenResult;
    }

    /**
     * @return string
     */
    public function getGetCstKeyFromSignOnTokenResult()
    {
      return $this->GetCstKeyFromSignOnTokenResult;
    }

    /**
     * @param string $GetCstKeyFromSignOnTokenResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\GetCstKeyFromSignOnTokenResponse
     */
    public function setGetCstKeyFromSignOnTokenResult($GetCstKeyFromSignOnTokenResult)
    {
      $this->GetCstKeyFromSignOnTokenResult = $GetCstKeyFromSignOnTokenResult;
      return $this;
    }

}
