<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn;

class GetSignOnToken
{

    /**
     * @var string $Email
     */
    protected $Email = null;

    /**
     * @var string $Password
     */
    protected $Password = null;

    /**
     * @var string $AuthToken
     */
    protected $AuthToken = null;

    /**
     * @var int $Minutes
     */
    protected $Minutes = null;

    /**
     * @param string $Email
     * @param string $Password
     * @param string $AuthToken
     * @param int $Minutes
     */
    public function __construct($Email, $Password, $AuthToken, $Minutes)
    {
      $this->Email = $Email;
      $this->Password = $Password;
      $this->AuthToken = $AuthToken;
      $this->Minutes = $Minutes;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
      return $this->Email;
    }

    /**
     * @param string $Email
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\GetSignOnToken
     */
    public function setEmail($Email)
    {
      $this->Email = $Email;
      return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
      return $this->Password;
    }

    /**
     * @param string $Password
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\GetSignOnToken
     */
    public function setPassword($Password)
    {
      $this->Password = $Password;
      return $this;
    }

    /**
     * @return string
     */
    public function getAuthToken()
    {
      return $this->AuthToken;
    }

    /**
     * @param string $AuthToken
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\GetSignOnToken
     */
    public function setAuthToken($AuthToken)
    {
      $this->AuthToken = $AuthToken;
      return $this;
    }

    /**
     * @return int
     */
    public function getMinutes()
    {
      return $this->Minutes;
    }

    /**
     * @param int $Minutes
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\GetSignOnToken
     */
    public function setMinutes($Minutes)
    {
      $this->Minutes = $Minutes;
      return $this;
    }

}
