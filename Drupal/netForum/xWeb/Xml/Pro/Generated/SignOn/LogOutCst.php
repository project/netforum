<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn;

class LogOutCst
{

    /**
     * @var string $AuthToken
     */
    protected $AuthToken = null;

    /**
     * @var string $szCstKey
     */
    protected $szCstKey = null;

    /**
     * @param string $AuthToken
     * @param string $szCstKey
     */
    public function __construct($AuthToken, $szCstKey)
    {
      $this->AuthToken = $AuthToken;
      $this->szCstKey = $szCstKey;
    }

    /**
     * @return string
     */
    public function getAuthToken()
    {
      return $this->AuthToken;
    }

    /**
     * @param string $AuthToken
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\LogOutCst
     */
    public function setAuthToken($AuthToken)
    {
      $this->AuthToken = $AuthToken;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzCstKey()
    {
      return $this->szCstKey;
    }

    /**
     * @param string $szCstKey
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\LogOutCst
     */
    public function setSzCstKey($szCstKey)
    {
      $this->szCstKey = $szCstKey;
      return $this;
    }

}
