<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn;

class GetCstKeyFromSignOnToken
{

    /**
     * @var string $AuthToken
     */
    protected $AuthToken = null;

    /**
     * @var string $szEncryptedSingOnToken
     */
    protected $szEncryptedSingOnToken = null;

    /**
     * @param string $AuthToken
     * @param string $szEncryptedSingOnToken
     */
    public function __construct($AuthToken, $szEncryptedSingOnToken)
    {
      $this->AuthToken = $AuthToken;
      $this->szEncryptedSingOnToken = $szEncryptedSingOnToken;
    }

    /**
     * @return string
     */
    public function getAuthToken()
    {
      return $this->AuthToken;
    }

    /**
     * @param string $AuthToken
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\GetCstKeyFromSignOnToken
     */
    public function setAuthToken($AuthToken)
    {
      $this->AuthToken = $AuthToken;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzEncryptedSingOnToken()
    {
      return $this->szEncryptedSingOnToken;
    }

    /**
     * @param string $szEncryptedSingOnToken
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\GetCstKeyFromSignOnToken
     */
    public function setSzEncryptedSingOnToken($szEncryptedSingOnToken)
    {
      $this->szEncryptedSingOnToken = $szEncryptedSingOnToken;
      return $this;
    }

}
