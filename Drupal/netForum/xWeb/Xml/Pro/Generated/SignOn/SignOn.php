<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn;

use Drupal\netForum\xWeb\Xml\netForumSoapClient;

class SignOn extends netForumSoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'Authenticate' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\SignOn\\Authenticate',
      'AuthenticateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\SignOn\\AuthenticateResponse',
      'GetSignOnToken' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\SignOn\\GetSignOnToken',
      'GetSignOnTokenResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\SignOn\\GetSignOnTokenResponse',
      'GetCstKeyFromSignOnToken' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\SignOn\\GetCstKeyFromSignOnToken',
      'GetCstKeyFromSignOnTokenResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\SignOn\\GetCstKeyFromSignOnTokenResponse',
      'LogOutCst' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\SignOn\\LogOutCst',
      'LogOutCstResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\SignOn\\LogOutCstResponse',
      'LogOutToken' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\SignOn\\LogOutToken',
      'LogOutTokenResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Pro\\Generated\\SignOn\\LogOutTokenResponse',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'https://netforum.avectra.com/xWeb/Signon.asmx?WSDL';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * Passing correct credentials to this method will return an authentication token - without an authentication token, the rest of the xWeb web methods will be inoperable.  The authentication token is governed by the group privileges assigned to the account invoking the xWeb web methods.  Please consult with the administrator of the netForum database to ensure your level of authorization.
     *
     * @param Authenticate $parameters
     * @return AuthenticateResponse
     */
    public function Authenticate(Authenticate $parameters)
    {
      return $this->__soapCall('Authenticate', array($parameters));
    }

    /**
     * Get Sign-On Token
     *
     * @param GetSignOnToken $parameters
     * @return GetSignOnTokenResponse
     */
    public function GetSignOnToken(GetSignOnToken $parameters)
    {
      return $this->__soapCall('GetSignOnToken', array($parameters));
    }

    /**
     * Get CstKey From SignOn Token
     *
     * @param GetCstKeyFromSignOnToken $parameters
     * @return GetCstKeyFromSignOnTokenResponse
     */
    public function GetCstKeyFromSignOnToken(GetCstKeyFromSignOnToken $parameters)
    {
      return $this->__soapCall('GetCstKeyFromSignOnToken', array($parameters));
    }

    /**
     * Expires token by cst key
     *
     * @param LogOutCst $parameters
     * @return LogOutCstResponse
     */
    public function LogOutCst(LogOutCst $parameters)
    {
      return $this->__soapCall('LogOutCst', array($parameters));
    }

    /**
     * Expires token by token
     *
     * @param LogOutToken $parameters
     * @return LogOutTokenResponse
     */
    public function LogOutToken(LogOutToken $parameters)
    {
      return $this->__soapCall('LogOutToken', array($parameters));
    }

}
