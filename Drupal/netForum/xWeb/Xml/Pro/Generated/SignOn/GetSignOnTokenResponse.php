<?php

namespace Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn;

class GetSignOnTokenResponse
{

    /**
     * @var string $GetSignOnTokenResult
     */
    protected $GetSignOnTokenResult = null;

    /**
     * @param string $GetSignOnTokenResult
     */
    public function __construct($GetSignOnTokenResult)
    {
      $this->GetSignOnTokenResult = $GetSignOnTokenResult;
    }

    /**
     * @return string
     */
    public function getGetSignOnTokenResult()
    {
      return $this->GetSignOnTokenResult;
    }

    /**
     * @param string $GetSignOnTokenResult
     * @return \Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\GetSignOnTokenResponse
     */
    public function setGetSignOnTokenResult($GetSignOnTokenResult)
    {
      $this->GetSignOnTokenResult = $GetSignOnTokenResult;
      return $this;
    }

}
