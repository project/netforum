<?php

namespace Drupal\netForum\xWeb\Xml\Pro;

use Drupal\netForum\xWeb\netForumxWebApiConfig;
use Drupal\netForum\xWeb\Xml\Pro\Generated\SignOn\SignOn as GeneratedSignOn;

class SignOn extends GeneratedSignOn {

  /**
   * Avectra XML Namespace for netFORUM Pro SignOn.
   */
  const AVECTRA_NS = 'http://tempuri.org/';

  public function __construct(netForumxWebApiConfig $netForumxWebApiConfig) {
    // This API is special, we need to fix the URL for the endpoint.
    // We are doing this to maintain backwards compatibility with existing settings.
    $signonEndpoint = $netForumxWebApiConfig->getApiEndpoint();
    $signonEndpoint = str_ireplace('/netForumXMLOnDemand.asmx?WSDL', '/SignOn.asmx?WSDL', $signonEndpoint);
    $netForumxWebApiConfig->setApiEndpoint($signonEndpoint);

    $this->netForumxWebApiConfig = $netForumxWebApiConfig;

    $this->SetXMLNamespace(self::AVECTRA_NS);

    // This API does not use the usual Authentication Token scheme.
    $this->setDoNotUseAnyHeaders(TRUE);

    parent::__construct(array(), NULL);
  }
}
