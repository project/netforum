<?php

namespace Drupal\netForum\xWeb\Xml;

use Drupal\netForum\xWeb\Exceptions\netForumxWebBadLoginException;
use Drupal\netForum\xWeb\Exceptions\netForumxWebMaxAuthRetriesException;
use Drupal\netForum\xWeb\netForumxWebApiConfig;
use Drupal\netForum\xWeb\Logging\ILogHandler;
use Drupal\netForum\xWeb\Logging\LogHandler;

/**
 * Class netForumSoapClient
 *
 * @package Drupal\netForum\xWeb\Xml
 */
class netForumSoapClient extends \SoapClient {

  private $XmlNamespace;

  private $SoapHeaders;

  private $LogHandler;

  private $AuthorizationToken;

  private $AuthorizationMaxRetries = 3;

  private $Debugging = FALSE;

  private $DoNotUseHeaders = array();

  private $DoNotUseAnyHeaders = FALSE;

  private $AuthenticationHandler = NULL;

  /**
   * @var netForumxWebApiConfig
   */
  protected $netForumxWebApiConfig;

  public function __construct($wsdl = NULL, array $options = array()) {
    $soapOptions = array(
      'user_agent' => 'Drupal netFORUM xWeb API',
      'trace' => TRUE,
      'exceptions' => TRUE,
    );

    if ($this->netForumxWebApiConfig instanceof netForumxWebApiConfig) {
      $wsdl = $this->netForumxWebApiConfig->getApiEndpoint();

      if ($this->netForumxWebApiConfig->isEnableTCPKeepAlive()) {
        $soapOptions['keep_alive'] = TRUE;
      }
      else {
        $soapOptions['keep_alive'] = FALSE;
      }

      if ($this->netForumxWebApiConfig->isEnableHTTPSCompression()) {
        $soapOptions['compression'] = SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | 2;
      }

      if ($this->netForumxWebApiConfig->isIgnoreInvalidCertificate()) {
        $soapOptions['stream_context'] = stream_context_create(array(
          'ssl' => array(
            'verify_peer' => FALSE,
            'verify_peer_name' => FALSE,
            'allow_self_signed' => TRUE,
          ),
        ));
      }

      if ($this->netForumxWebApiConfig->isEnableProxy()) {
        $soapOptions['proxy_host'] = $this->netForumxWebApiConfig->getProxyHostname();
        $soapOptions['proxy_port'] = $this->netForumxWebApiConfig->getProxyPort();

        if (!empty($this->netForumxWebApiConfig->getProxyUsername())) {
          $soapOptions['proxy_login'] = $this->netForumxWebApiConfig->getProxyUsername();
        }

        if (!empty($this->netForumxWebApiConfig->getProxyPassword())) {
          $soapOptions['proxy_password'] = $this->netForumxWebApiConfig->getProxyPassword();
        }
      }
    }

    $options = array_merge($soapOptions, $options);

    parent::__construct($wsdl, $options);
  }

  public function __soapCall($function_name, $arguments, $options = NULL, $input_headers = NULL, &$output_headers = NULL) {
    $response = NULL;

    $previousHeaders = NULL;

    try {
      // stupid: Some web methods cause xWeb to throw an exception
      // if a authentication token is set and mustUnderstand is set to true on
      // the soap header.
      if (in_array($function_name, $this->DoNotUseHeaders, TRUE) || $this->DoNotUseAnyHeaders) {
        $previousHeaders = $this->SoapHeaders;

        $this->__setSoapHeaders(NULL);
      }

      $response = parent::__soapCall($function_name, $arguments, $options, $input_headers, $output_headers);

      if (in_array($function_name, $this->DoNotUseHeaders, TRUE) || $this->DoNotUseAnyHeaders) {
        $this->__setSoapHeaders($previousHeaders);
      }
    } catch (\SoapFault $e) {
      if (!isset($e->detail)) {
        throw Exceptions\netForumException::FromSoapFaultException($e);
      }
      else {
        // the netForum xWeb documentation is not clear on responses when
        // requests fail. Web methods have inconsistent error responses. Also
        // not sure if every field is constant or not. Better safe then sorry.

        //region netForum_xWeb_Exceptions
        // InvalidCredentialsExceptionInfo
        if (isset($e->detail->InvalidCredentialsExceptionInfo)
          && is_object($e->detail->InvalidCredentialsExceptionInfo)
        ) {
          $message = 'InvalidCredentialsExceptionInfo';

          if (isset($e->detail->InvalidCredentialsExceptionInfo->XWebException)
            && is_object($e->detail->InvalidCredentialsExceptionInfo->XWebException)
            && isset($e->detail->InvalidCredentialsExceptionInfo->XWebException->Message)
          ) {
            $message = $e->detail->InvalidCredentialsExceptionInfo->XWebException->Message;
          }

          throw new Exceptions\InvalidCredentialsException($message, 0, $e);
        }

        // InvalidTokenException
        if (isset($e->detail->InvalidTokenException)
          && is_object($e->detail->InvalidTokenException)
        ) {
          static $authRetries = 0;

          // If our token has expired, we will attempt to re-authenticate and get
          // a new token and resend our original request.
          try {
            if ($this->LogHandler instanceof ILogHandler) {
              $this->LogHandler->writeStringMessage('Received InvalidTokenException,
              trying to login again. Attempts so far: ' . $authRetries . '.', LogHandler::INFORMATIONAL);
            }

            if ($authRetries >= $this->AuthorizationMaxRetries) {
              throw new netForumxWebMaxAuthRetriesException('Too many attempts to
            re-authenticate and submit the same request. Is the username and password correct?');
            }

            $authRetries++;

            $this->internalAuthenticate();
          } catch (Exceptions\netForumException $e) {
            throw new netForumxWebBadLoginException('The supplied xWeb Username
              and Password appear to be incorrect. Unable to re-authenticate to
              get new Token.', 0, $e);
          }

          return $this->__soapCall($function_name, $arguments, $options, $input_headers, $output_headers);
        }

        // LockedCredentialsExceptionInfo
        if (isset($e->detail->LockedCredentialsExceptionInfo)
          && is_object($e->detail->LockedCredentialsExceptionInfo)
        ) {
          $message = 'LockedCredentialsExceptionInfo';

          if (isset($e->detail->LockedCredentialsExceptionInfo->XWebException)
            && is_object($e->detail->LockedCredentialsExceptionInfo->XWebException)
            && isset($e->detail->LockedCredentialsExceptionInfo->XWebException->Message)
          ) {
            $message = $e->detail->LockedCredentialsExceptionInfo->XWebException->Message;
          }

          throw new Exceptions\LockedCredentialsException($message, 0, $e);
        }

        // SystemExceptionInfo
        if (isset($e->detail->SystemExceptionInfo)
          && is_object($e->detail->SystemExceptionInfo)
        ) {
          $message = 'SystemExceptionInfo';

          if (isset($e->detail->SystemExceptionInfo->XWebException)
            && is_object($e->detail->SystemExceptionInfo->XWebException)
            && isset($e->detail->SystemExceptionInfo->XWebException->Message)
          ) {
            $message = $e->detail->SystemExceptionInfo->XWebException->Message;
          }

          throw new Exceptions\SystemException($message, 0, $e);
        }

        // None of the above matched
        throw new Exceptions\netForumException('Unknown Exception thrown.', 0, $e);
        //endregion
      }
    }

    if (!empty($output_headers) && array_key_exists('AuthorizationToken', $output_headers)
      && is_object($output_headers['AuthorizationToken'])
    ) {

      $this->AuthorizationToken = $output_headers['AuthorizationToken']->getToken();

      // We replace our saved token with one from the response in case xWeb is
      // using sliding expiration.
      $this->__setSoapHeaders(NULL);
      $this->__setSoapHeaders(
        new \SoapHeader($this->getXMLNamespace(), 'AuthorizationToken', array(
          'Token' => $this->AuthorizationToken,
        ), TRUE));
    }

    return $response;
  }

  /**
   * @param null $soapHeaders
   *
   * @return bool
   */
  public function __setSoapHeaders($soapHeaders = NULL) {
    $this->SoapHeaders = $soapHeaders;

    return parent::__setSoapHeaders($soapHeaders);
  }

  protected function getXMLNamespace() {
    return $this->XmlNamespace;
  }

  protected function setXMLNamespace($xmlNamespace) {
    $this->XmlNamespace = $xmlNamespace;
  }

  /**
   * @return array
   */
  public function getDoNotUseHeaders() {
    return $this->DoNotUseHeaders;
  }

  /**
   * @param array $webMethods
   */
  public function setDoNotUseHeaders($webMethods) {
    $this->DoNotUseHeaders = $webMethods;
  }

  /**
   * @return bool
   */
  public function isDoNotUseAnyHeaders() {
    return $this->DoNotUseAnyHeaders;
  }

  /**
   * @param bool $DoNotUseAnyHeaders
   */
  public function setDoNotUseAnyHeaders($DoNotUseAnyHeaders) {
    $this->DoNotUseAnyHeaders = $DoNotUseAnyHeaders;
  }

  /**
   * Get the current Authenticate Token being used.
   *
   * @return string|null
   */
  public function getAuthorizationToken() {
    return $this->AuthorizationToken;
  }

  /**
   * Set a new Authenticate Token to be used for authentication with xWeb.
   *
   * @param $token string
   */
  public function setAuthorizationToken($token) {
    $this->AuthorizationToken = $token;
  }

  /**
   * @param Callable $AuthenticationHandler
   */
  public function setAuthenticationHandler(Callable $AuthenticationHandler) {
    $this->AuthenticationHandler = $AuthenticationHandler;
  }

  private function internalAuthenticate() {
    if ($this->AuthenticationHandler != NULL) {
      call_user_func($this->AuthenticationHandler);
    }
  }

  /**
   * @param string $request
   * @param string $location
   * @param string $action
   * @param int $version
   * @param int $one_way
   *
   * @return string
   */
  public function __doRequest($request, $location, $action, $version, $one_way = 0) {
    if ($this->Debugging && $this->LogHandler instanceof ILogHandler) {
      $this->LogHandler->writeXmlStringMessage($request, LogHandler::DEBUG);
    }

    $previousSocketTimeout = ini_get('default_socket_timeout');
    ini_set('default_socket_timeout', $this->netForumxWebApiConfig->getRequestTimeout());

    $response = parent::__doRequest($request, $location, $action, $version, $one_way);

    ini_set('default_socket_timeout', $previousSocketTimeout);

    if ($this->Debugging && $this->LogHandler instanceof ILogHandler) {
      $this->LogHandler->writeXmlStringMessage($response, LogHandler::DEBUG);
    }

    return $response;
  }
}
