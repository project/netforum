<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBChaptersGetChapterListResponse
{

    /**
     * @var WEBChaptersGetChapterListResult $WEBChaptersGetChapterListResult
     */
    protected $WEBChaptersGetChapterListResult = null;

    /**
     * @param WEBChaptersGetChapterListResult $WEBChaptersGetChapterListResult
     */
    public function __construct($WEBChaptersGetChapterListResult)
    {
      $this->WEBChaptersGetChapterListResult = $WEBChaptersGetChapterListResult;
    }

    /**
     * @return WEBChaptersGetChapterListResult
     */
    public function getWEBChaptersGetChapterListResult()
    {
      return $this->WEBChaptersGetChapterListResult;
    }

    /**
     * @param WEBChaptersGetChapterListResult $WEBChaptersGetChapterListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBChaptersGetChapterListResponse
     */
    public function setWEBChaptersGetChapterListResult($WEBChaptersGetChapterListResult)
    {
      $this->WEBChaptersGetChapterListResult = $WEBChaptersGetChapterListResult;
      return $this;
    }

}
