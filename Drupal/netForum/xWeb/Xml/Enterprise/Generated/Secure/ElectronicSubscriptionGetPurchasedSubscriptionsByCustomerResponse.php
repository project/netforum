<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResponse
{

    /**
     * @var ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResult $ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResult
     */
    protected $ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResult = null;

    /**
     * @param ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResult $ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResult
     */
    public function __construct($ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResult)
    {
      $this->ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResult = $ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResult;
    }

    /**
     * @return ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResult
     */
    public function getElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResult()
    {
      return $this->ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResult;
    }

    /**
     * @param ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResult $ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResponse
     */
    public function setElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResult($ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResult)
    {
      $this->ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResult = $ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResult;
      return $this;
    }

}
