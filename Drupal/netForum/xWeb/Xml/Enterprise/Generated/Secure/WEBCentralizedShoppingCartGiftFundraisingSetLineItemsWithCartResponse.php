<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCartResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCartResult
     */
    protected $WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCartResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCartResult
     */
    public function __construct($WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCartResult)
    {
      $this->WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCartResult = $WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCartResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCartResult()
    {
      return $this->WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCartResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCartResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCartResponse
     */
    public function setWEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCartResult($WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCartResult)
    {
      $this->WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCartResult = $WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCartResult;
      return $this;
    }

}
