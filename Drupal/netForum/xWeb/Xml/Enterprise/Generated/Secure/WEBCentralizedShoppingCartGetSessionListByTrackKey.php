<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetSessionListByTrackKey
{

    /**
     * @var guid $TrackKey
     */
    protected $TrackKey = null;

    /**
     * @param guid $TrackKey
     */
    public function __construct($TrackKey)
    {
      $this->TrackKey = $TrackKey;
    }

    /**
     * @return guid
     */
    public function getTrackKey()
    {
      return $this->TrackKey;
    }

    /**
     * @param guid $TrackKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetSessionListByTrackKey
     */
    public function setTrackKey($TrackKey)
    {
      $this->TrackKey = $TrackKey;
      return $this;
    }

}
