<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class VersionClass
{

    /**
     * @var string $MajorVersion
     */
    protected $MajorVersion = null;

    /**
     * @var string $MinorVersion
     */
    protected $MinorVersion = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getMajorVersion()
    {
      return $this->MajorVersion;
    }

    /**
     * @param string $MajorVersion
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\VersionClass
     */
    public function setMajorVersion($MajorVersion)
    {
      $this->MajorVersion = $MajorVersion;
      return $this;
    }

    /**
     * @return string
     */
    public function getMinorVersion()
    {
      return $this->MinorVersion;
    }

    /**
     * @param string $MinorVersion
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\VersionClass
     */
    public function setMinorVersion($MinorVersion)
    {
      $this->MinorVersion = $MinorVersion;
      return $this;
    }

}
