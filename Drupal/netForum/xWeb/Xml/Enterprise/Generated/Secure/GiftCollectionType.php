<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GiftCollectionType
{

    /**
     * @var FundraisingGiftType[] $FundraisingGift
     */
    protected $FundraisingGift = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return FundraisingGiftType[]
     */
    public function getFundraisingGift()
    {
      return $this->FundraisingGift;
    }

    /**
     * @param FundraisingGiftType[] $FundraisingGift
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GiftCollectionType
     */
    public function setFundraisingGift(array $FundraisingGift = null)
    {
      $this->FundraisingGift = $FundraisingGift;
      return $this;
    }

}
