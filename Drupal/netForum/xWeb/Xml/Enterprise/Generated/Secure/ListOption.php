<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class ListOption
{

    /**
     * @var string $Value
     */
    protected $Value = null;

    /**
     * @var string $Text
     */
    protected $Text = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getValue()
    {
      return $this->Value;
    }

    /**
     * @param string $Value
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ListOption
     */
    public function setValue($Value)
    {
      $this->Value = $Value;
      return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
      return $this->Text;
    }

    /**
     * @param string $Text
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ListOption
     */
    public function setText($Text)
    {
      $this->Text = $Text;
      return $this;
    }

}
