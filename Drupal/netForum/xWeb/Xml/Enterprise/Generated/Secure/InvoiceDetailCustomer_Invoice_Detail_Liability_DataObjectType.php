<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
{

    /**
     * @var av_key_Type $idc_key
     */
    protected $idc_key = null;

    /**
     * @var stringLength500_Type $idc_cst_sort_name_dn
     */
    protected $idc_cst_sort_name_dn = null;

    /**
     * @var av_key_Type $idc_cst_key
     */
    protected $idc_cst_key = null;

    /**
     * @var av_key_Type $idc_ixo_key
     */
    protected $idc_ixo_key = null;

    /**
     * @var av_key_Type $idc_ixt_key
     */
    protected $idc_ixt_key = null;

    /**
     * @var av_key_Type $idc_ivd_key
     */
    protected $idc_ivd_key = null;

    /**
     * @var av_key_Type $idc_ivd_inv_key
     */
    protected $idc_ivd_inv_key = null;

    /**
     * @var av_date_Type $idc_date
     */
    protected $idc_date = null;

    /**
     * @var stringLength50_Type $idc_code
     */
    protected $idc_code = null;

    /**
     * @var av_currency_Type $idc_price
     */
    protected $idc_price = null;

    /**
     * @var av_integer_Type $idc_qty
     */
    protected $idc_qty = null;

    /**
     * @var av_key_Type $idc_ods_key
     */
    protected $idc_ods_key = null;

    /**
     * @var av_user_Type $idc_add_user
     */
    protected $idc_add_user = null;

    /**
     * @var av_date_Type $idc_add_date
     */
    protected $idc_add_date = null;

    /**
     * @var av_user_Type $idc_change_user
     */
    protected $idc_change_user = null;

    /**
     * @var av_date_Type $idc_change_date
     */
    protected $idc_change_date = null;

    /**
     * @var av_delete_flag_Type $idc_delete_flag
     */
    protected $idc_delete_flag = null;

    /**
     * @var av_key_Type $idc_entity_key
     */
    protected $idc_entity_key = null;

    /**
     * @var av_currency_Type $idc_total
     */
    protected $idc_total = null;

    /**
     * @var decimal_Type $idc_return_qty
     */
    protected $idc_return_qty = null;

    /**
     * @var decimal_Type $idc_cancel_qty
     */
    protected $idc_cancel_qty = null;

    /**
     * @var av_currency_Type $idc_credit_to_apply
     */
    protected $idc_credit_to_apply = null;

    /**
     * @var av_currency_Type $idc_payment_to_apply
     */
    protected $idc_payment_to_apply = null;

    /**
     * @var av_currency_Type $idc_payment_applied
     */
    protected $idc_payment_applied = null;

    /**
     * @var av_currency_Type $idc_credit_applied
     */
    protected $idc_credit_applied = null;

    /**
     * @var av_currency_Type $idc_writeoff_to_apply
     */
    protected $idc_writeoff_to_apply = null;

    /**
     * @var decimal_Type $idc_credit_dollar_amount
     */
    protected $idc_credit_dollar_amount = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getIdc_key()
    {
      return $this->idc_key;
    }

    /**
     * @param av_key_Type $idc_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_key($idc_key)
    {
      $this->idc_key = $idc_key;
      return $this;
    }

    /**
     * @return stringLength500_Type
     */
    public function getIdc_cst_sort_name_dn()
    {
      return $this->idc_cst_sort_name_dn;
    }

    /**
     * @param stringLength500_Type $idc_cst_sort_name_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_cst_sort_name_dn($idc_cst_sort_name_dn)
    {
      $this->idc_cst_sort_name_dn = $idc_cst_sort_name_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getIdc_cst_key()
    {
      return $this->idc_cst_key;
    }

    /**
     * @param av_key_Type $idc_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_cst_key($idc_cst_key)
    {
      $this->idc_cst_key = $idc_cst_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getIdc_ixo_key()
    {
      return $this->idc_ixo_key;
    }

    /**
     * @param av_key_Type $idc_ixo_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_ixo_key($idc_ixo_key)
    {
      $this->idc_ixo_key = $idc_ixo_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getIdc_ixt_key()
    {
      return $this->idc_ixt_key;
    }

    /**
     * @param av_key_Type $idc_ixt_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_ixt_key($idc_ixt_key)
    {
      $this->idc_ixt_key = $idc_ixt_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getIdc_ivd_key()
    {
      return $this->idc_ivd_key;
    }

    /**
     * @param av_key_Type $idc_ivd_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_ivd_key($idc_ivd_key)
    {
      $this->idc_ivd_key = $idc_ivd_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getIdc_ivd_inv_key()
    {
      return $this->idc_ivd_inv_key;
    }

    /**
     * @param av_key_Type $idc_ivd_inv_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_ivd_inv_key($idc_ivd_inv_key)
    {
      $this->idc_ivd_inv_key = $idc_ivd_inv_key;
      return $this;
    }

    /**
     * @return av_date_Type
     */
    public function getIdc_date()
    {
      return $this->idc_date;
    }

    /**
     * @param av_date_Type $idc_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_date($idc_date)
    {
      $this->idc_date = $idc_date;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getIdc_code()
    {
      return $this->idc_code;
    }

    /**
     * @param stringLength50_Type $idc_code
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_code($idc_code)
    {
      $this->idc_code = $idc_code;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIdc_price()
    {
      return $this->idc_price;
    }

    /**
     * @param av_currency_Type $idc_price
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_price($idc_price)
    {
      $this->idc_price = $idc_price;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getIdc_qty()
    {
      return $this->idc_qty;
    }

    /**
     * @param av_integer_Type $idc_qty
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_qty($idc_qty)
    {
      $this->idc_qty = $idc_qty;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getIdc_ods_key()
    {
      return $this->idc_ods_key;
    }

    /**
     * @param av_key_Type $idc_ods_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_ods_key($idc_ods_key)
    {
      $this->idc_ods_key = $idc_ods_key;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getIdc_add_user()
    {
      return $this->idc_add_user;
    }

    /**
     * @param av_user_Type $idc_add_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_add_user($idc_add_user)
    {
      $this->idc_add_user = $idc_add_user;
      return $this;
    }

    /**
     * @return av_date_Type
     */
    public function getIdc_add_date()
    {
      return $this->idc_add_date;
    }

    /**
     * @param av_date_Type $idc_add_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_add_date($idc_add_date)
    {
      $this->idc_add_date = $idc_add_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getIdc_change_user()
    {
      return $this->idc_change_user;
    }

    /**
     * @param av_user_Type $idc_change_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_change_user($idc_change_user)
    {
      $this->idc_change_user = $idc_change_user;
      return $this;
    }

    /**
     * @return av_date_Type
     */
    public function getIdc_change_date()
    {
      return $this->idc_change_date;
    }

    /**
     * @param av_date_Type $idc_change_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_change_date($idc_change_date)
    {
      $this->idc_change_date = $idc_change_date;
      return $this;
    }

    /**
     * @return av_delete_flag_Type
     */
    public function getIdc_delete_flag()
    {
      return $this->idc_delete_flag;
    }

    /**
     * @param av_delete_flag_Type $idc_delete_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_delete_flag($idc_delete_flag)
    {
      $this->idc_delete_flag = $idc_delete_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getIdc_entity_key()
    {
      return $this->idc_entity_key;
    }

    /**
     * @param av_key_Type $idc_entity_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_entity_key($idc_entity_key)
    {
      $this->idc_entity_key = $idc_entity_key;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIdc_total()
    {
      return $this->idc_total;
    }

    /**
     * @param av_currency_Type $idc_total
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_total($idc_total)
    {
      $this->idc_total = $idc_total;
      return $this;
    }

    /**
     * @return decimal_Type
     */
    public function getIdc_return_qty()
    {
      return $this->idc_return_qty;
    }

    /**
     * @param decimal_Type $idc_return_qty
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_return_qty($idc_return_qty)
    {
      $this->idc_return_qty = $idc_return_qty;
      return $this;
    }

    /**
     * @return decimal_Type
     */
    public function getIdc_cancel_qty()
    {
      return $this->idc_cancel_qty;
    }

    /**
     * @param decimal_Type $idc_cancel_qty
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_cancel_qty($idc_cancel_qty)
    {
      $this->idc_cancel_qty = $idc_cancel_qty;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIdc_credit_to_apply()
    {
      return $this->idc_credit_to_apply;
    }

    /**
     * @param av_currency_Type $idc_credit_to_apply
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_credit_to_apply($idc_credit_to_apply)
    {
      $this->idc_credit_to_apply = $idc_credit_to_apply;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIdc_payment_to_apply()
    {
      return $this->idc_payment_to_apply;
    }

    /**
     * @param av_currency_Type $idc_payment_to_apply
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_payment_to_apply($idc_payment_to_apply)
    {
      $this->idc_payment_to_apply = $idc_payment_to_apply;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIdc_payment_applied()
    {
      return $this->idc_payment_applied;
    }

    /**
     * @param av_currency_Type $idc_payment_applied
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_payment_applied($idc_payment_applied)
    {
      $this->idc_payment_applied = $idc_payment_applied;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIdc_credit_applied()
    {
      return $this->idc_credit_applied;
    }

    /**
     * @param av_currency_Type $idc_credit_applied
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_credit_applied($idc_credit_applied)
    {
      $this->idc_credit_applied = $idc_credit_applied;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIdc_writeoff_to_apply()
    {
      return $this->idc_writeoff_to_apply;
    }

    /**
     * @param av_currency_Type $idc_writeoff_to_apply
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_writeoff_to_apply($idc_writeoff_to_apply)
    {
      $this->idc_writeoff_to_apply = $idc_writeoff_to_apply;
      return $this;
    }

    /**
     * @return decimal_Type
     */
    public function getIdc_credit_dollar_amount()
    {
      return $this->idc_credit_dollar_amount;
    }

    /**
     * @param decimal_Type $idc_credit_dollar_amount
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function setIdc_credit_dollar_amount($idc_credit_dollar_amount)
    {
      $this->idc_credit_dollar_amount = $idc_credit_dollar_amount;
      return $this;
    }

}
