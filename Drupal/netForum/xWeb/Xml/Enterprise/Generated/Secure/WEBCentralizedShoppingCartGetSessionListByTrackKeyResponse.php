<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetSessionListByTrackKeyResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetSessionListByTrackKeyResult $WEBCentralizedShoppingCartGetSessionListByTrackKeyResult
     */
    protected $WEBCentralizedShoppingCartGetSessionListByTrackKeyResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetSessionListByTrackKeyResult $WEBCentralizedShoppingCartGetSessionListByTrackKeyResult
     */
    public function __construct($WEBCentralizedShoppingCartGetSessionListByTrackKeyResult)
    {
      $this->WEBCentralizedShoppingCartGetSessionListByTrackKeyResult = $WEBCentralizedShoppingCartGetSessionListByTrackKeyResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetSessionListByTrackKeyResult
     */
    public function getWEBCentralizedShoppingCartGetSessionListByTrackKeyResult()
    {
      return $this->WEBCentralizedShoppingCartGetSessionListByTrackKeyResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetSessionListByTrackKeyResult $WEBCentralizedShoppingCartGetSessionListByTrackKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetSessionListByTrackKeyResponse
     */
    public function setWEBCentralizedShoppingCartGetSessionListByTrackKeyResult($WEBCentralizedShoppingCartGetSessionListByTrackKeyResult)
    {
      $this->WEBCentralizedShoppingCartGetSessionListByTrackKeyResult = $WEBCentralizedShoppingCartGetSessionListByTrackKeyResult;
      return $this;
    }

}
