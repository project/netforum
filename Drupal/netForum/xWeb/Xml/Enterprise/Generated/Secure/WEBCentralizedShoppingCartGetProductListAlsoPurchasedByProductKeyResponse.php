<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResult $WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResult
     */
    protected $WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResult $WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResult
     */
    public function __construct($WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResult)
    {
      $this->WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResult = $WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResult
     */
    public function getWEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResult()
    {
      return $this->WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResult $WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResponse
     */
    public function setWEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResult($WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResult)
    {
      $this->WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResult = $WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResult;
      return $this;
    }

}
