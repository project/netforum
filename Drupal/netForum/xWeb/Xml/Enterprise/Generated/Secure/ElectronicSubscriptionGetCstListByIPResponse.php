<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class ElectronicSubscriptionGetCstListByIPResponse
{

    /**
     * @var ElectronicSubscriptionGetCstListByIPResult $ElectronicSubscriptionGetCstListByIPResult
     */
    protected $ElectronicSubscriptionGetCstListByIPResult = null;

    /**
     * @param ElectronicSubscriptionGetCstListByIPResult $ElectronicSubscriptionGetCstListByIPResult
     */
    public function __construct($ElectronicSubscriptionGetCstListByIPResult)
    {
      $this->ElectronicSubscriptionGetCstListByIPResult = $ElectronicSubscriptionGetCstListByIPResult;
    }

    /**
     * @return ElectronicSubscriptionGetCstListByIPResult
     */
    public function getElectronicSubscriptionGetCstListByIPResult()
    {
      return $this->ElectronicSubscriptionGetCstListByIPResult;
    }

    /**
     * @param ElectronicSubscriptionGetCstListByIPResult $ElectronicSubscriptionGetCstListByIPResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ElectronicSubscriptionGetCstListByIPResponse
     */
    public function setElectronicSubscriptionGetCstListByIPResult($ElectronicSubscriptionGetCstListByIPResult)
    {
      $this->ElectronicSubscriptionGetCstListByIPResult = $ElectronicSubscriptionGetCstListByIPResult;
      return $this;
    }

}
