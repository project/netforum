<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResult $WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResult
     */
    protected $WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResult $WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResult
     */
    public function __construct($WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResult)
    {
      $this->WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResult = $WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResult
     */
    public function getWEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResult()
    {
      return $this->WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResult $WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResponse
     */
    public function setWEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResult($WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResult)
    {
      $this->WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResult = $WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResult;
      return $this;
    }

}
