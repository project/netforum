<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartRefreshResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartRefreshResult
     */
    protected $WEBCentralizedShoppingCartRefreshResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartRefreshResult
     */
    public function __construct($WEBCentralizedShoppingCartRefreshResult)
    {
      $this->WEBCentralizedShoppingCartRefreshResult = $WEBCentralizedShoppingCartRefreshResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartRefreshResult()
    {
      return $this->WEBCentralizedShoppingCartRefreshResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartRefreshResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartRefreshResponse
     */
    public function setWEBCentralizedShoppingCartRefreshResult($WEBCentralizedShoppingCartRefreshResult)
    {
      $this->WEBCentralizedShoppingCartRefreshResult = $WEBCentralizedShoppingCartRefreshResult;
      return $this;
    }

}
