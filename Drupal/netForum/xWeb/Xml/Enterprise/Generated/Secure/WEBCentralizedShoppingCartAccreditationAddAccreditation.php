<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartAccreditationAddAccreditation
{

    /**
     * @var CentralizedOrderEntryType $oCentralizedOrderEntry
     */
    protected $oCentralizedOrderEntry = null;

    /**
     * @var AccreditationAreaType $oAccreditation
     */
    protected $oAccreditation = null;

    /**
     * @param CentralizedOrderEntryType $oCentralizedOrderEntry
     * @param AccreditationAreaType $oAccreditation
     */
    public function __construct($oCentralizedOrderEntry, $oAccreditation)
    {
      $this->oCentralizedOrderEntry = $oCentralizedOrderEntry;
      $this->oAccreditation = $oAccreditation;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getOCentralizedOrderEntry()
    {
      return $this->oCentralizedOrderEntry;
    }

    /**
     * @param CentralizedOrderEntryType $oCentralizedOrderEntry
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartAccreditationAddAccreditation
     */
    public function setOCentralizedOrderEntry($oCentralizedOrderEntry)
    {
      $this->oCentralizedOrderEntry = $oCentralizedOrderEntry;
      return $this;
    }

    /**
     * @return AccreditationAreaType
     */
    public function getOAccreditation()
    {
      return $this->oAccreditation;
    }

    /**
     * @param AccreditationAreaType $oAccreditation
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartAccreditationAddAccreditation
     */
    public function setOAccreditation($oAccreditation)
    {
      $this->oAccreditation = $oAccreditation;
      return $this;
    }

}
