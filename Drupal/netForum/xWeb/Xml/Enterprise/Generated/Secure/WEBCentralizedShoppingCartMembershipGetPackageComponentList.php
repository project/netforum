<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartMembershipGetPackageComponentList
{

    /**
     * @var guid $PackageKey
     */
    protected $PackageKey = null;

    /**
     * @param guid $PackageKey
     */
    public function __construct($PackageKey)
    {
      $this->PackageKey = $PackageKey;
    }

    /**
     * @return guid
     */
    public function getPackageKey()
    {
      return $this->PackageKey;
    }

    /**
     * @param guid $PackageKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartMembershipGetPackageComponentList
     */
    public function setPackageKey($PackageKey)
    {
      $this->PackageKey = $PackageKey;
      return $this;
    }

}
