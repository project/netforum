<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCommitteeGetDocumentsResponse
{

    /**
     * @var WEBCommitteeGetDocumentsResult $WEBCommitteeGetDocumentsResult
     */
    protected $WEBCommitteeGetDocumentsResult = null;

    /**
     * @param WEBCommitteeGetDocumentsResult $WEBCommitteeGetDocumentsResult
     */
    public function __construct($WEBCommitteeGetDocumentsResult)
    {
      $this->WEBCommitteeGetDocumentsResult = $WEBCommitteeGetDocumentsResult;
    }

    /**
     * @return WEBCommitteeGetDocumentsResult
     */
    public function getWEBCommitteeGetDocumentsResult()
    {
      return $this->WEBCommitteeGetDocumentsResult;
    }

    /**
     * @param WEBCommitteeGetDocumentsResult $WEBCommitteeGetDocumentsResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCommitteeGetDocumentsResponse
     */
    public function setWEBCommitteeGetDocumentsResult($WEBCommitteeGetDocumentsResult)
    {
      $this->WEBCommitteeGetDocumentsResult = $WEBCommitteeGetDocumentsResult;
      return $this;
    }

}
