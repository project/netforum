<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetSubscriptionMailingTypesResponse
{

    /**
     * @var ArrayOfMailingList $GetSubscriptionMailingTypesResult
     */
    protected $GetSubscriptionMailingTypesResult = null;

    /**
     * @param ArrayOfMailingList $GetSubscriptionMailingTypesResult
     */
    public function __construct($GetSubscriptionMailingTypesResult)
    {
      $this->GetSubscriptionMailingTypesResult = $GetSubscriptionMailingTypesResult;
    }

    /**
     * @return ArrayOfMailingList
     */
    public function getGetSubscriptionMailingTypesResult()
    {
      return $this->GetSubscriptionMailingTypesResult;
    }

    /**
     * @param ArrayOfMailingList $GetSubscriptionMailingTypesResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetSubscriptionMailingTypesResponse
     */
    public function setGetSubscriptionMailingTypesResult($GetSubscriptionMailingTypesResult)
    {
      $this->GetSubscriptionMailingTypesResult = $GetSubscriptionMailingTypesResult;
      return $this;
    }

}
