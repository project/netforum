<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class Invoice_Detail_LiabilityCollectionType
{

    /**
     * @var InvoiceDetailCustomerType[] $InvoiceDetailCustomer
     */
    protected $InvoiceDetailCustomer = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return InvoiceDetailCustomerType[]
     */
    public function getInvoiceDetailCustomer()
    {
      return $this->InvoiceDetailCustomer;
    }

    /**
     * @param InvoiceDetailCustomerType[] $InvoiceDetailCustomer
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\Invoice_Detail_LiabilityCollectionType
     */
    public function setInvoiceDetailCustomer(array $InvoiceDetailCustomer = null)
    {
      $this->InvoiceDetailCustomer = $InvoiceDetailCustomer;
      return $this;
    }

}
