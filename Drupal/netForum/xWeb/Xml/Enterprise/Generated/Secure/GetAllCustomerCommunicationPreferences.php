<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetAllCustomerCommunicationPreferences
{

    /**
     * @var string $customerKey
     */
    protected $customerKey = null;

    /**
     * @param string $customerKey
     */
    public function __construct($customerKey)
    {
      $this->customerKey = $customerKey;
    }

    /**
     * @return string
     */
    public function getCustomerKey()
    {
      return $this->customerKey;
    }

    /**
     * @param string $customerKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetAllCustomerCommunicationPreferences
     */
    public function setCustomerKey($customerKey)
    {
      $this->customerKey = $customerKey;
      return $this;
    }

}
