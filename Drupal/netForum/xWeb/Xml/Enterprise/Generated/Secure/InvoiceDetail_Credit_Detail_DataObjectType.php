<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class InvoiceDetail_Credit_Detail_DataObjectType
{

    /**
     * @var av_key_Type $cdd_key
     */
    protected $cdd_key = null;

    /**
     * @var av_key_Type $cdd_cdt_key
     */
    protected $cdd_cdt_key = null;

    /**
     * @var stringLength20_Type $cdd_type
     */
    protected $cdd_type = null;

    /**
     * @var av_currency_Type $cdd_amount
     */
    protected $cdd_amount = null;

    /**
     * @var av_key_Type $cdd_gla_dr_key
     */
    protected $cdd_gla_dr_key = null;

    /**
     * @var av_key_Type $cdd_gla_cr_key
     */
    protected $cdd_gla_cr_key = null;

    /**
     * @var av_key_Type $cdd_ret_key
     */
    protected $cdd_ret_key = null;

    /**
     * @var av_key_Type $cdd_ret_ivd_prc_prd_key
     */
    protected $cdd_ret_ivd_prc_prd_key = null;

    /**
     * @var av_key_Type $cdd_ajd_key
     */
    protected $cdd_ajd_key = null;

    /**
     * @var av_flag_Type $cdd_closed_flag
     */
    protected $cdd_closed_flag = null;

    /**
     * @var av_date_Type $cdd_void_date
     */
    protected $cdd_void_date = null;

    /**
     * @var av_flag_Type $cdd_void_flag
     */
    protected $cdd_void_flag = null;

    /**
     * @var av_user_Type $cdd_void_user
     */
    protected $cdd_void_user = null;

    /**
     * @var av_user_Type $cdd_add_user
     */
    protected $cdd_add_user = null;

    /**
     * @var av_date_Type $cdd_add_date
     */
    protected $cdd_add_date = null;

    /**
     * @var av_user_Type $cdd_change_user
     */
    protected $cdd_change_user = null;

    /**
     * @var av_date_Type $cdd_change_date
     */
    protected $cdd_change_date = null;

    /**
     * @var av_delete_flag_Type $cdd_delete_flag
     */
    protected $cdd_delete_flag = null;

    /**
     * @var av_date_small_Type $cdd_from_date
     */
    protected $cdd_from_date = null;

    /**
     * @var av_key_Type $cdd_ptp_key
     */
    protected $cdd_ptp_key = null;

    /**
     * @var av_date_small_Type $cdd_through_date
     */
    protected $cdd_through_date = null;

    /**
     * @var av_key_Type $cdd_pin_key
     */
    protected $cdd_pin_key = null;

    /**
     * @var av_flag_Type $cdd_required_usage_flag
     */
    protected $cdd_required_usage_flag = null;

    /**
     * @var av_key_Type $cdd_ret_ivd_key
     */
    protected $cdd_ret_ivd_key = null;

    /**
     * @var av_key_Type $cdd_entity_key
     */
    protected $cdd_entity_key = null;

    /**
     * @var av_flag_Type $cdd_auto_apply
     */
    protected $cdd_auto_apply = null;

    /**
     * @var av_key_Type $cdd_prg_key
     */
    protected $cdd_prg_key = null;

    /**
     * @var av_key_Type $cdd_mct_key
     */
    protected $cdd_mct_key = null;

    /**
     * @var av_flag_Type $cdd_distributed_liability_flag
     */
    protected $cdd_distributed_liability_flag = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getCdd_key()
    {
      return $this->cdd_key;
    }

    /**
     * @param av_key_Type $cdd_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_key($cdd_key)
    {
      $this->cdd_key = $cdd_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCdd_cdt_key()
    {
      return $this->cdd_cdt_key;
    }

    /**
     * @param av_key_Type $cdd_cdt_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_cdt_key($cdd_cdt_key)
    {
      $this->cdd_cdt_key = $cdd_cdt_key;
      return $this;
    }

    /**
     * @return stringLength20_Type
     */
    public function getCdd_type()
    {
      return $this->cdd_type;
    }

    /**
     * @param stringLength20_Type $cdd_type
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_type($cdd_type)
    {
      $this->cdd_type = $cdd_type;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getCdd_amount()
    {
      return $this->cdd_amount;
    }

    /**
     * @param av_currency_Type $cdd_amount
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_amount($cdd_amount)
    {
      $this->cdd_amount = $cdd_amount;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCdd_gla_dr_key()
    {
      return $this->cdd_gla_dr_key;
    }

    /**
     * @param av_key_Type $cdd_gla_dr_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_gla_dr_key($cdd_gla_dr_key)
    {
      $this->cdd_gla_dr_key = $cdd_gla_dr_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCdd_gla_cr_key()
    {
      return $this->cdd_gla_cr_key;
    }

    /**
     * @param av_key_Type $cdd_gla_cr_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_gla_cr_key($cdd_gla_cr_key)
    {
      $this->cdd_gla_cr_key = $cdd_gla_cr_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCdd_ret_key()
    {
      return $this->cdd_ret_key;
    }

    /**
     * @param av_key_Type $cdd_ret_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_ret_key($cdd_ret_key)
    {
      $this->cdd_ret_key = $cdd_ret_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCdd_ret_ivd_prc_prd_key()
    {
      return $this->cdd_ret_ivd_prc_prd_key;
    }

    /**
     * @param av_key_Type $cdd_ret_ivd_prc_prd_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_ret_ivd_prc_prd_key($cdd_ret_ivd_prc_prd_key)
    {
      $this->cdd_ret_ivd_prc_prd_key = $cdd_ret_ivd_prc_prd_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCdd_ajd_key()
    {
      return $this->cdd_ajd_key;
    }

    /**
     * @param av_key_Type $cdd_ajd_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_ajd_key($cdd_ajd_key)
    {
      $this->cdd_ajd_key = $cdd_ajd_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCdd_closed_flag()
    {
      return $this->cdd_closed_flag;
    }

    /**
     * @param av_flag_Type $cdd_closed_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_closed_flag($cdd_closed_flag)
    {
      $this->cdd_closed_flag = $cdd_closed_flag;
      return $this;
    }

    /**
     * @return av_date_Type
     */
    public function getCdd_void_date()
    {
      return $this->cdd_void_date;
    }

    /**
     * @param av_date_Type $cdd_void_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_void_date($cdd_void_date)
    {
      $this->cdd_void_date = $cdd_void_date;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCdd_void_flag()
    {
      return $this->cdd_void_flag;
    }

    /**
     * @param av_flag_Type $cdd_void_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_void_flag($cdd_void_flag)
    {
      $this->cdd_void_flag = $cdd_void_flag;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getCdd_void_user()
    {
      return $this->cdd_void_user;
    }

    /**
     * @param av_user_Type $cdd_void_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_void_user($cdd_void_user)
    {
      $this->cdd_void_user = $cdd_void_user;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getCdd_add_user()
    {
      return $this->cdd_add_user;
    }

    /**
     * @param av_user_Type $cdd_add_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_add_user($cdd_add_user)
    {
      $this->cdd_add_user = $cdd_add_user;
      return $this;
    }

    /**
     * @return av_date_Type
     */
    public function getCdd_add_date()
    {
      return $this->cdd_add_date;
    }

    /**
     * @param av_date_Type $cdd_add_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_add_date($cdd_add_date)
    {
      $this->cdd_add_date = $cdd_add_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getCdd_change_user()
    {
      return $this->cdd_change_user;
    }

    /**
     * @param av_user_Type $cdd_change_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_change_user($cdd_change_user)
    {
      $this->cdd_change_user = $cdd_change_user;
      return $this;
    }

    /**
     * @return av_date_Type
     */
    public function getCdd_change_date()
    {
      return $this->cdd_change_date;
    }

    /**
     * @param av_date_Type $cdd_change_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_change_date($cdd_change_date)
    {
      $this->cdd_change_date = $cdd_change_date;
      return $this;
    }

    /**
     * @return av_delete_flag_Type
     */
    public function getCdd_delete_flag()
    {
      return $this->cdd_delete_flag;
    }

    /**
     * @param av_delete_flag_Type $cdd_delete_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_delete_flag($cdd_delete_flag)
    {
      $this->cdd_delete_flag = $cdd_delete_flag;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getCdd_from_date()
    {
      return $this->cdd_from_date;
    }

    /**
     * @param av_date_small_Type $cdd_from_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_from_date($cdd_from_date)
    {
      $this->cdd_from_date = $cdd_from_date;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCdd_ptp_key()
    {
      return $this->cdd_ptp_key;
    }

    /**
     * @param av_key_Type $cdd_ptp_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_ptp_key($cdd_ptp_key)
    {
      $this->cdd_ptp_key = $cdd_ptp_key;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getCdd_through_date()
    {
      return $this->cdd_through_date;
    }

    /**
     * @param av_date_small_Type $cdd_through_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_through_date($cdd_through_date)
    {
      $this->cdd_through_date = $cdd_through_date;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCdd_pin_key()
    {
      return $this->cdd_pin_key;
    }

    /**
     * @param av_key_Type $cdd_pin_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_pin_key($cdd_pin_key)
    {
      $this->cdd_pin_key = $cdd_pin_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCdd_required_usage_flag()
    {
      return $this->cdd_required_usage_flag;
    }

    /**
     * @param av_flag_Type $cdd_required_usage_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_required_usage_flag($cdd_required_usage_flag)
    {
      $this->cdd_required_usage_flag = $cdd_required_usage_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCdd_ret_ivd_key()
    {
      return $this->cdd_ret_ivd_key;
    }

    /**
     * @param av_key_Type $cdd_ret_ivd_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_ret_ivd_key($cdd_ret_ivd_key)
    {
      $this->cdd_ret_ivd_key = $cdd_ret_ivd_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCdd_entity_key()
    {
      return $this->cdd_entity_key;
    }

    /**
     * @param av_key_Type $cdd_entity_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_entity_key($cdd_entity_key)
    {
      $this->cdd_entity_key = $cdd_entity_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCdd_auto_apply()
    {
      return $this->cdd_auto_apply;
    }

    /**
     * @param av_flag_Type $cdd_auto_apply
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_auto_apply($cdd_auto_apply)
    {
      $this->cdd_auto_apply = $cdd_auto_apply;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCdd_prg_key()
    {
      return $this->cdd_prg_key;
    }

    /**
     * @param av_key_Type $cdd_prg_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_prg_key($cdd_prg_key)
    {
      $this->cdd_prg_key = $cdd_prg_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCdd_mct_key()
    {
      return $this->cdd_mct_key;
    }

    /**
     * @param av_key_Type $cdd_mct_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_mct_key($cdd_mct_key)
    {
      $this->cdd_mct_key = $cdd_mct_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCdd_distributed_liability_flag()
    {
      return $this->cdd_distributed_liability_flag;
    }

    /**
     * @param av_flag_Type $cdd_distributed_liability_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Credit_Detail_DataObjectType
     */
    public function setCdd_distributed_liability_flag($cdd_distributed_liability_flag)
    {
      $this->cdd_distributed_liability_flag = $cdd_distributed_liability_flag;
      return $this;
    }

}
