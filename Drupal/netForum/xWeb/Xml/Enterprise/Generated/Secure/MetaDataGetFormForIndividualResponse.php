<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class MetaDataGetFormForIndividualResponse
{

    /**
     * @var AVForm $MetaDataGetFormForIndividualResult
     */
    protected $MetaDataGetFormForIndividualResult = null;

    /**
     * @param AVForm $MetaDataGetFormForIndividualResult
     */
    public function __construct($MetaDataGetFormForIndividualResult)
    {
      $this->MetaDataGetFormForIndividualResult = $MetaDataGetFormForIndividualResult;
    }

    /**
     * @return AVForm
     */
    public function getMetaDataGetFormForIndividualResult()
    {
      return $this->MetaDataGetFormForIndividualResult;
    }

    /**
     * @param AVForm $MetaDataGetFormForIndividualResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\MetaDataGetFormForIndividualResponse
     */
    public function setMetaDataGetFormForIndividualResult($MetaDataGetFormForIndividualResult)
    {
      $this->MetaDataGetFormForIndividualResult = $MetaDataGetFormForIndividualResult;
      return $this;
    }

}
