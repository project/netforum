<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class ExhibitorCollectionType
{

    /**
     * @var ExhibitorNewType[] $ExhibitorNew
     */
    protected $ExhibitorNew = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ExhibitorNewType[]
     */
    public function getExhibitorNew()
    {
      return $this->ExhibitorNew;
    }

    /**
     * @param ExhibitorNewType[] $ExhibitorNew
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorCollectionType
     */
    public function setExhibitorNew(array $ExhibitorNew = null)
    {
      $this->ExhibitorNew = $ExhibitorNew;
      return $this;
    }

}
