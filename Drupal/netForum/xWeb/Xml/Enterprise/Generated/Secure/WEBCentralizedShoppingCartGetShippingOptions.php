<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetShippingOptions
{

    /**
     * @var CentralizedOrderEntryType $oCentalizedOrderEntry
     */
    protected $oCentalizedOrderEntry = null;

    /**
     * @param CentralizedOrderEntryType $oCentalizedOrderEntry
     */
    public function __construct($oCentalizedOrderEntry)
    {
      $this->oCentalizedOrderEntry = $oCentalizedOrderEntry;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getOCentalizedOrderEntry()
    {
      return $this->oCentalizedOrderEntry;
    }

    /**
     * @param CentralizedOrderEntryType $oCentalizedOrderEntry
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetShippingOptions
     */
    public function setOCentalizedOrderEntry($oCentalizedOrderEntry)
    {
      $this->oCentalizedOrderEntry = $oCentalizedOrderEntry;
      return $this;
    }

}
