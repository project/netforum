<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetDynamicQueryDefinitionResponse
{

    /**
     * @var QueryDefinition $GetDynamicQueryDefinitionResult
     */
    protected $GetDynamicQueryDefinitionResult = null;

    /**
     * @param QueryDefinition $GetDynamicQueryDefinitionResult
     */
    public function __construct($GetDynamicQueryDefinitionResult)
    {
      $this->GetDynamicQueryDefinitionResult = $GetDynamicQueryDefinitionResult;
    }

    /**
     * @return QueryDefinition
     */
    public function getGetDynamicQueryDefinitionResult()
    {
      return $this->GetDynamicQueryDefinitionResult;
    }

    /**
     * @param QueryDefinition $GetDynamicQueryDefinitionResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetDynamicQueryDefinitionResponse
     */
    public function setGetDynamicQueryDefinitionResult($GetDynamicQueryDefinitionResult)
    {
      $this->GetDynamicQueryDefinitionResult = $GetDynamicQueryDefinitionResult;
      return $this;
    }

}
