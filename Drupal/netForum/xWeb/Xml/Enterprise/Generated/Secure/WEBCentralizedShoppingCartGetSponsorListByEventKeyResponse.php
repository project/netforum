<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetSponsorListByEventKeyResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetSponsorListByEventKeyResult $WEBCentralizedShoppingCartGetSponsorListByEventKeyResult
     */
    protected $WEBCentralizedShoppingCartGetSponsorListByEventKeyResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetSponsorListByEventKeyResult $WEBCentralizedShoppingCartGetSponsorListByEventKeyResult
     */
    public function __construct($WEBCentralizedShoppingCartGetSponsorListByEventKeyResult)
    {
      $this->WEBCentralizedShoppingCartGetSponsorListByEventKeyResult = $WEBCentralizedShoppingCartGetSponsorListByEventKeyResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetSponsorListByEventKeyResult
     */
    public function getWEBCentralizedShoppingCartGetSponsorListByEventKeyResult()
    {
      return $this->WEBCentralizedShoppingCartGetSponsorListByEventKeyResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetSponsorListByEventKeyResult $WEBCentralizedShoppingCartGetSponsorListByEventKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetSponsorListByEventKeyResponse
     */
    public function setWEBCentralizedShoppingCartGetSponsorListByEventKeyResult($WEBCentralizedShoppingCartGetSponsorListByEventKeyResult)
    {
      $this->WEBCentralizedShoppingCartGetSponsorListByEventKeyResult = $WEBCentralizedShoppingCartGetSponsorListByEventKeyResult;
      return $this;
    }

}
