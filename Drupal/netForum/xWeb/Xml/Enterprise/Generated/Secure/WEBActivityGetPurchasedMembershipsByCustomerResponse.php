<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBActivityGetPurchasedMembershipsByCustomerResponse
{

    /**
     * @var WEBActivityGetPurchasedMembershipsByCustomerResult $WEBActivityGetPurchasedMembershipsByCustomerResult
     */
    protected $WEBActivityGetPurchasedMembershipsByCustomerResult = null;

    /**
     * @param WEBActivityGetPurchasedMembershipsByCustomerResult $WEBActivityGetPurchasedMembershipsByCustomerResult
     */
    public function __construct($WEBActivityGetPurchasedMembershipsByCustomerResult)
    {
      $this->WEBActivityGetPurchasedMembershipsByCustomerResult = $WEBActivityGetPurchasedMembershipsByCustomerResult;
    }

    /**
     * @return WEBActivityGetPurchasedMembershipsByCustomerResult
     */
    public function getWEBActivityGetPurchasedMembershipsByCustomerResult()
    {
      return $this->WEBActivityGetPurchasedMembershipsByCustomerResult;
    }

    /**
     * @param WEBActivityGetPurchasedMembershipsByCustomerResult $WEBActivityGetPurchasedMembershipsByCustomerResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBActivityGetPurchasedMembershipsByCustomerResponse
     */
    public function setWEBActivityGetPurchasedMembershipsByCustomerResult($WEBActivityGetPurchasedMembershipsByCustomerResult)
    {
      $this->WEBActivityGetPurchasedMembershipsByCustomerResult = $WEBActivityGetPurchasedMembershipsByCustomerResult;
      return $this;
    }

}
