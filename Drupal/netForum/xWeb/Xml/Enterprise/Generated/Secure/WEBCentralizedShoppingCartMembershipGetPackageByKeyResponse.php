<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartMembershipGetPackageByKeyResponse
{

    /**
     * @var WEBCentralizedShoppingCartMembershipGetPackageByKeyResult $WEBCentralizedShoppingCartMembershipGetPackageByKeyResult
     */
    protected $WEBCentralizedShoppingCartMembershipGetPackageByKeyResult = null;

    /**
     * @param WEBCentralizedShoppingCartMembershipGetPackageByKeyResult $WEBCentralizedShoppingCartMembershipGetPackageByKeyResult
     */
    public function __construct($WEBCentralizedShoppingCartMembershipGetPackageByKeyResult)
    {
      $this->WEBCentralizedShoppingCartMembershipGetPackageByKeyResult = $WEBCentralizedShoppingCartMembershipGetPackageByKeyResult;
    }

    /**
     * @return WEBCentralizedShoppingCartMembershipGetPackageByKeyResult
     */
    public function getWEBCentralizedShoppingCartMembershipGetPackageByKeyResult()
    {
      return $this->WEBCentralizedShoppingCartMembershipGetPackageByKeyResult;
    }

    /**
     * @param WEBCentralizedShoppingCartMembershipGetPackageByKeyResult $WEBCentralizedShoppingCartMembershipGetPackageByKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartMembershipGetPackageByKeyResponse
     */
    public function setWEBCentralizedShoppingCartMembershipGetPackageByKeyResult($WEBCentralizedShoppingCartMembershipGetPackageByKeyResult)
    {
      $this->WEBCentralizedShoppingCartMembershipGetPackageByKeyResult = $WEBCentralizedShoppingCartMembershipGetPackageByKeyResult;
      return $this;
    }

}
