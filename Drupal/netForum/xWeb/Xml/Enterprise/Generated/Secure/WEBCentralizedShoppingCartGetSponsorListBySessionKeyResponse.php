<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetSponsorListBySessionKeyResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetSponsorListBySessionKeyResult $WEBCentralizedShoppingCartGetSponsorListBySessionKeyResult
     */
    protected $WEBCentralizedShoppingCartGetSponsorListBySessionKeyResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetSponsorListBySessionKeyResult $WEBCentralizedShoppingCartGetSponsorListBySessionKeyResult
     */
    public function __construct($WEBCentralizedShoppingCartGetSponsorListBySessionKeyResult)
    {
      $this->WEBCentralizedShoppingCartGetSponsorListBySessionKeyResult = $WEBCentralizedShoppingCartGetSponsorListBySessionKeyResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetSponsorListBySessionKeyResult
     */
    public function getWEBCentralizedShoppingCartGetSponsorListBySessionKeyResult()
    {
      return $this->WEBCentralizedShoppingCartGetSponsorListBySessionKeyResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetSponsorListBySessionKeyResult $WEBCentralizedShoppingCartGetSponsorListBySessionKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetSponsorListBySessionKeyResponse
     */
    public function setWEBCentralizedShoppingCartGetSponsorListBySessionKeyResult($WEBCentralizedShoppingCartGetSponsorListBySessionKeyResult)
    {
      $this->WEBCentralizedShoppingCartGetSponsorListBySessionKeyResult = $WEBCentralizedShoppingCartGetSponsorListBySessionKeyResult;
      return $this;
    }

}
