<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGiftFundraisingGetNewResponse
{

    /**
     * @var FundraisingGiftType $WEBCentralizedShoppingCartGiftFundraisingGetNewResult
     */
    protected $WEBCentralizedShoppingCartGiftFundraisingGetNewResult = null;

    /**
     * @param FundraisingGiftType $WEBCentralizedShoppingCartGiftFundraisingGetNewResult
     */
    public function __construct($WEBCentralizedShoppingCartGiftFundraisingGetNewResult)
    {
      $this->WEBCentralizedShoppingCartGiftFundraisingGetNewResult = $WEBCentralizedShoppingCartGiftFundraisingGetNewResult;
    }

    /**
     * @return FundraisingGiftType
     */
    public function getWEBCentralizedShoppingCartGiftFundraisingGetNewResult()
    {
      return $this->WEBCentralizedShoppingCartGiftFundraisingGetNewResult;
    }

    /**
     * @param FundraisingGiftType $WEBCentralizedShoppingCartGiftFundraisingGetNewResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGiftFundraisingGetNewResponse
     */
    public function setWEBCentralizedShoppingCartGiftFundraisingGetNewResult($WEBCentralizedShoppingCartGiftFundraisingGetNewResult)
    {
      $this->WEBCentralizedShoppingCartGiftFundraisingGetNewResult = $WEBCentralizedShoppingCartGiftFundraisingGetNewResult;
      return $this;
    }

}
