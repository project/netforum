<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartRemoveAllLineItemsResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartRemoveAllLineItemsResult
     */
    protected $WEBCentralizedShoppingCartRemoveAllLineItemsResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartRemoveAllLineItemsResult
     */
    public function __construct($WEBCentralizedShoppingCartRemoveAllLineItemsResult)
    {
      $this->WEBCentralizedShoppingCartRemoveAllLineItemsResult = $WEBCentralizedShoppingCartRemoveAllLineItemsResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartRemoveAllLineItemsResult()
    {
      return $this->WEBCentralizedShoppingCartRemoveAllLineItemsResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartRemoveAllLineItemsResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartRemoveAllLineItemsResponse
     */
    public function setWEBCentralizedShoppingCartRemoveAllLineItemsResult($WEBCentralizedShoppingCartRemoveAllLineItemsResult)
    {
      $this->WEBCentralizedShoppingCartRemoveAllLineItemsResult = $WEBCentralizedShoppingCartRemoveAllLineItemsResult;
      return $this;
    }

}
