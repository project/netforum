<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBFindUsersInRole
{

    /**
     * @var string $roleName
     */
    protected $roleName = null;

    /**
     * @var string $usernameToMatch
     */
    protected $usernameToMatch = null;

    /**
     * @param string $roleName
     * @param string $usernameToMatch
     */
    public function __construct($roleName, $usernameToMatch)
    {
      $this->roleName = $roleName;
      $this->usernameToMatch = $usernameToMatch;
    }

    /**
     * @return string
     */
    public function getRoleName()
    {
      return $this->roleName;
    }

    /**
     * @param string $roleName
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBFindUsersInRole
     */
    public function setRoleName($roleName)
    {
      $this->roleName = $roleName;
      return $this;
    }

    /**
     * @return string
     */
    public function getUsernameToMatch()
    {
      return $this->usernameToMatch;
    }

    /**
     * @param string $usernameToMatch
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBFindUsersInRole
     */
    public function setUsernameToMatch($usernameToMatch)
    {
      $this->usernameToMatch = $usernameToMatch;
      return $this;
    }

}
