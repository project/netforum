<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartMembershipGetResponse
{

    /**
     * @var mb_membershipType $WEBCentralizedShoppingCartMembershipGetResult
     */
    protected $WEBCentralizedShoppingCartMembershipGetResult = null;

    /**
     * @param mb_membershipType $WEBCentralizedShoppingCartMembershipGetResult
     */
    public function __construct($WEBCentralizedShoppingCartMembershipGetResult)
    {
      $this->WEBCentralizedShoppingCartMembershipGetResult = $WEBCentralizedShoppingCartMembershipGetResult;
    }

    /**
     * @return mb_membershipType
     */
    public function getWEBCentralizedShoppingCartMembershipGetResult()
    {
      return $this->WEBCentralizedShoppingCartMembershipGetResult;
    }

    /**
     * @param mb_membershipType $WEBCentralizedShoppingCartMembershipGetResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartMembershipGetResponse
     */
    public function setWEBCentralizedShoppingCartMembershipGetResult($WEBCentralizedShoppingCartMembershipGetResult)
    {
      $this->WEBCentralizedShoppingCartMembershipGetResult = $WEBCentralizedShoppingCartMembershipGetResult;
      return $this;
    }

}
