<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBIndividualGetPrefixesResponse
{

    /**
     * @var WEBIndividualGetPrefixesResult $WEBIndividualGetPrefixesResult
     */
    protected $WEBIndividualGetPrefixesResult = null;

    /**
     * @param WEBIndividualGetPrefixesResult $WEBIndividualGetPrefixesResult
     */
    public function __construct($WEBIndividualGetPrefixesResult)
    {
      $this->WEBIndividualGetPrefixesResult = $WEBIndividualGetPrefixesResult;
    }

    /**
     * @return WEBIndividualGetPrefixesResult
     */
    public function getWEBIndividualGetPrefixesResult()
    {
      return $this->WEBIndividualGetPrefixesResult;
    }

    /**
     * @param WEBIndividualGetPrefixesResult $WEBIndividualGetPrefixesResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBIndividualGetPrefixesResponse
     */
    public function setWEBIndividualGetPrefixesResult($WEBIndividualGetPrefixesResult)
    {
      $this->WEBIndividualGetPrefixesResult = $WEBIndividualGetPrefixesResult;
      return $this;
    }

}
