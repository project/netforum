<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetInstallmentTermsOptionsResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetInstallmentTermsOptionsResult $WEBCentralizedShoppingCartGetInstallmentTermsOptionsResult
     */
    protected $WEBCentralizedShoppingCartGetInstallmentTermsOptionsResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetInstallmentTermsOptionsResult $WEBCentralizedShoppingCartGetInstallmentTermsOptionsResult
     */
    public function __construct($WEBCentralizedShoppingCartGetInstallmentTermsOptionsResult)
    {
      $this->WEBCentralizedShoppingCartGetInstallmentTermsOptionsResult = $WEBCentralizedShoppingCartGetInstallmentTermsOptionsResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetInstallmentTermsOptionsResult
     */
    public function getWEBCentralizedShoppingCartGetInstallmentTermsOptionsResult()
    {
      return $this->WEBCentralizedShoppingCartGetInstallmentTermsOptionsResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetInstallmentTermsOptionsResult $WEBCentralizedShoppingCartGetInstallmentTermsOptionsResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetInstallmentTermsOptionsResponse
     */
    public function setWEBCentralizedShoppingCartGetInstallmentTermsOptionsResult($WEBCentralizedShoppingCartGetInstallmentTermsOptionsResult)
    {
      $this->WEBCentralizedShoppingCartGetInstallmentTermsOptionsResult = $WEBCentralizedShoppingCartGetInstallmentTermsOptionsResult;
      return $this;
    }

}
