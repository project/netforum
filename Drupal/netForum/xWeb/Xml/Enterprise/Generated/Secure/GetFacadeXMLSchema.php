<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetFacadeXMLSchema
{

    /**
     * @var string $szObjectName
     */
    protected $szObjectName = null;

    /**
     * @param string $szObjectName
     */
    public function __construct($szObjectName)
    {
      $this->szObjectName = $szObjectName;
    }

    /**
     * @return string
     */
    public function getSzObjectName()
    {
      return $this->szObjectName;
    }

    /**
     * @param string $szObjectName
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetFacadeXMLSchema
     */
    public function setSzObjectName($szObjectName)
    {
      $this->szObjectName = $szObjectName;
      return $this;
    }

}
