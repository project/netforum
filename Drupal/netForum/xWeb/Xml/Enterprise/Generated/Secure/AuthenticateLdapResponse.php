<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class AuthenticateLdapResponse
{

    /**
     * @var string $AuthenticateLdapResult
     */
    protected $AuthenticateLdapResult = null;

    /**
     * @param string $AuthenticateLdapResult
     */
    public function __construct($AuthenticateLdapResult)
    {
      $this->AuthenticateLdapResult = $AuthenticateLdapResult;
    }

    /**
     * @return string
     */
    public function getAuthenticateLdapResult()
    {
      return $this->AuthenticateLdapResult;
    }

    /**
     * @param string $AuthenticateLdapResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AuthenticateLdapResponse
     */
    public function setAuthenticateLdapResult($AuthenticateLdapResult)
    {
      $this->AuthenticateLdapResult = $AuthenticateLdapResult;
      return $this;
    }

}
