<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserFindUsersByUserNameFirstNameLastNameResponse
{

    /**
     * @var WEBWebUserFindUsersByUserNameFirstNameLastNameResult $WEBWebUserFindUsersByUserNameFirstNameLastNameResult
     */
    protected $WEBWebUserFindUsersByUserNameFirstNameLastNameResult = null;

    /**
     * @param WEBWebUserFindUsersByUserNameFirstNameLastNameResult $WEBWebUserFindUsersByUserNameFirstNameLastNameResult
     */
    public function __construct($WEBWebUserFindUsersByUserNameFirstNameLastNameResult)
    {
      $this->WEBWebUserFindUsersByUserNameFirstNameLastNameResult = $WEBWebUserFindUsersByUserNameFirstNameLastNameResult;
    }

    /**
     * @return WEBWebUserFindUsersByUserNameFirstNameLastNameResult
     */
    public function getWEBWebUserFindUsersByUserNameFirstNameLastNameResult()
    {
      return $this->WEBWebUserFindUsersByUserNameFirstNameLastNameResult;
    }

    /**
     * @param WEBWebUserFindUsersByUserNameFirstNameLastNameResult $WEBWebUserFindUsersByUserNameFirstNameLastNameResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserFindUsersByUserNameFirstNameLastNameResponse
     */
    public function setWEBWebUserFindUsersByUserNameFirstNameLastNameResult($WEBWebUserFindUsersByUserNameFirstNameLastNameResult)
    {
      $this->WEBWebUserFindUsersByUserNameFirstNameLastNameResult = $WEBWebUserFindUsersByUserNameFirstNameLastNameResult;
      return $this;
    }

}
