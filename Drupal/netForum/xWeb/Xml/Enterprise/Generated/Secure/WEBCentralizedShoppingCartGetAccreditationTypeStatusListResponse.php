<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetAccreditationTypeStatusListResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetAccreditationTypeStatusListResult $WEBCentralizedShoppingCartGetAccreditationTypeStatusListResult
     */
    protected $WEBCentralizedShoppingCartGetAccreditationTypeStatusListResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetAccreditationTypeStatusListResult $WEBCentralizedShoppingCartGetAccreditationTypeStatusListResult
     */
    public function __construct($WEBCentralizedShoppingCartGetAccreditationTypeStatusListResult)
    {
      $this->WEBCentralizedShoppingCartGetAccreditationTypeStatusListResult = $WEBCentralizedShoppingCartGetAccreditationTypeStatusListResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetAccreditationTypeStatusListResult
     */
    public function getWEBCentralizedShoppingCartGetAccreditationTypeStatusListResult()
    {
      return $this->WEBCentralizedShoppingCartGetAccreditationTypeStatusListResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetAccreditationTypeStatusListResult $WEBCentralizedShoppingCartGetAccreditationTypeStatusListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetAccreditationTypeStatusListResponse
     */
    public function setWEBCentralizedShoppingCartGetAccreditationTypeStatusListResult($WEBCentralizedShoppingCartGetAccreditationTypeStatusListResult)
    {
      $this->WEBCentralizedShoppingCartGetAccreditationTypeStatusListResult = $WEBCentralizedShoppingCartGetAccreditationTypeStatusListResult;
      return $this;
    }

}
