<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBActivityAlreadyRegisteredForEventResponse
{

    /**
     * @var boolean $WEBActivityAlreadyRegisteredForEventResult
     */
    protected $WEBActivityAlreadyRegisteredForEventResult = null;

    /**
     * @param boolean $WEBActivityAlreadyRegisteredForEventResult
     */
    public function __construct($WEBActivityAlreadyRegisteredForEventResult)
    {
      $this->WEBActivityAlreadyRegisteredForEventResult = $WEBActivityAlreadyRegisteredForEventResult;
    }

    /**
     * @return boolean
     */
    public function getWEBActivityAlreadyRegisteredForEventResult()
    {
      return $this->WEBActivityAlreadyRegisteredForEventResult;
    }

    /**
     * @param boolean $WEBActivityAlreadyRegisteredForEventResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBActivityAlreadyRegisteredForEventResponse
     */
    public function setWEBActivityAlreadyRegisteredForEventResult($WEBActivityAlreadyRegisteredForEventResult)
    {
      $this->WEBActivityAlreadyRegisteredForEventResult = $WEBActivityAlreadyRegisteredForEventResult;
      return $this;
    }

}
