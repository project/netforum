<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartApplySourceCodeResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartApplySourceCodeResult
     */
    protected $WEBCentralizedShoppingCartApplySourceCodeResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartApplySourceCodeResult
     */
    public function __construct($WEBCentralizedShoppingCartApplySourceCodeResult)
    {
      $this->WEBCentralizedShoppingCartApplySourceCodeResult = $WEBCentralizedShoppingCartApplySourceCodeResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartApplySourceCodeResult()
    {
      return $this->WEBCentralizedShoppingCartApplySourceCodeResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartApplySourceCodeResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartApplySourceCodeResponse
     */
    public function setWEBCentralizedShoppingCartApplySourceCodeResult($WEBCentralizedShoppingCartApplySourceCodeResult)
    {
      $this->WEBCentralizedShoppingCartApplySourceCodeResult = $WEBCentralizedShoppingCartApplySourceCodeResult;
      return $this;
    }

}
