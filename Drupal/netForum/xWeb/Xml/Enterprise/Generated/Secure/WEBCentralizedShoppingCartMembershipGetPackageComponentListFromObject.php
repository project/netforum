<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObject
{

    /**
     * @var mb_membershipType $oMembership
     */
    protected $oMembership = null;

    /**
     * @param mb_membershipType $oMembership
     */
    public function __construct($oMembership)
    {
      $this->oMembership = $oMembership;
    }

    /**
     * @return mb_membershipType
     */
    public function getOMembership()
    {
      return $this->oMembership;
    }

    /**
     * @param mb_membershipType $oMembership
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObject
     */
    public function setOMembership($oMembership)
    {
      $this->oMembership = $oMembership;
      return $this;
    }

}
