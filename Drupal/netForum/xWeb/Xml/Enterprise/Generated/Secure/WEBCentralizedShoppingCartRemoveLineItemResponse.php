<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartRemoveLineItemResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartRemoveLineItemResult
     */
    protected $WEBCentralizedShoppingCartRemoveLineItemResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartRemoveLineItemResult
     */
    public function __construct($WEBCentralizedShoppingCartRemoveLineItemResult)
    {
      $this->WEBCentralizedShoppingCartRemoveLineItemResult = $WEBCentralizedShoppingCartRemoveLineItemResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartRemoveLineItemResult()
    {
      return $this->WEBCentralizedShoppingCartRemoveLineItemResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartRemoveLineItemResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartRemoveLineItemResponse
     */
    public function setWEBCentralizedShoppingCartRemoveLineItemResult($WEBCentralizedShoppingCartRemoveLineItemResult)
    {
      $this->WEBCentralizedShoppingCartRemoveLineItemResult = $WEBCentralizedShoppingCartRemoveLineItemResult;
      return $this;
    }

}
