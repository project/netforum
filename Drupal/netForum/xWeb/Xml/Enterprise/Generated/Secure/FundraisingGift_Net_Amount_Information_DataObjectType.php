<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class FundraisingGift_Net_Amount_Information_DataObjectType
{

    /**
     * @var av_key_Type $vid_ivd_key
     */
    protected $vid_ivd_key = null;

    /**
     * @var av_currency_Type $vid_net_total
     */
    protected $vid_net_total = null;

    /**
     * @var av_key_Type $vid_inv_key
     */
    protected $vid_inv_key = null;

    /**
     * @var av_key_Type $vid_ivd_entity_key
     */
    protected $vid_ivd_entity_key = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getVid_ivd_key()
    {
      return $this->vid_ivd_key;
    }

    /**
     * @param av_key_Type $vid_ivd_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\FundraisingGift_Net_Amount_Information_DataObjectType
     */
    public function setVid_ivd_key($vid_ivd_key)
    {
      $this->vid_ivd_key = $vid_ivd_key;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getVid_net_total()
    {
      return $this->vid_net_total;
    }

    /**
     * @param av_currency_Type $vid_net_total
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\FundraisingGift_Net_Amount_Information_DataObjectType
     */
    public function setVid_net_total($vid_net_total)
    {
      $this->vid_net_total = $vid_net_total;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getVid_inv_key()
    {
      return $this->vid_inv_key;
    }

    /**
     * @param av_key_Type $vid_inv_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\FundraisingGift_Net_Amount_Information_DataObjectType
     */
    public function setVid_inv_key($vid_inv_key)
    {
      $this->vid_inv_key = $vid_inv_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getVid_ivd_entity_key()
    {
      return $this->vid_ivd_entity_key;
    }

    /**
     * @param av_key_Type $vid_ivd_entity_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\FundraisingGift_Net_Amount_Information_DataObjectType
     */
    public function setVid_ivd_entity_key($vid_ivd_entity_key)
    {
      $this->vid_ivd_entity_key = $vid_ivd_entity_key;
      return $this;
    }

}
