<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartExhibitorGet
{

    /**
     * @var guid $ExhibitorKey
     */
    protected $ExhibitorKey = null;

    /**
     * @param guid $ExhibitorKey
     */
    public function __construct($ExhibitorKey)
    {
      $this->ExhibitorKey = $ExhibitorKey;
    }

    /**
     * @return guid
     */
    public function getExhibitorKey()
    {
      return $this->ExhibitorKey;
    }

    /**
     * @param guid $ExhibitorKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartExhibitorGet
     */
    public function setExhibitorKey($ExhibitorKey)
    {
      $this->ExhibitorKey = $ExhibitorKey;
      return $this;
    }

}
