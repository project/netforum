<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserGetByRecno_CustomResponse
{

    /**
     * @var WebUserType $WEBWebUserGetByRecno_CustomResult
     */
    protected $WEBWebUserGetByRecno_CustomResult = null;

    /**
     * @param WebUserType $WEBWebUserGetByRecno_CustomResult
     */
    public function __construct($WEBWebUserGetByRecno_CustomResult)
    {
      $this->WEBWebUserGetByRecno_CustomResult = $WEBWebUserGetByRecno_CustomResult;
    }

    /**
     * @return WebUserType
     */
    public function getWEBWebUserGetByRecno_CustomResult()
    {
      return $this->WEBWebUserGetByRecno_CustomResult;
    }

    /**
     * @param WebUserType $WEBWebUserGetByRecno_CustomResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserGetByRecno_CustomResponse
     */
    public function setWEBWebUserGetByRecno_CustomResult($WEBWebUserGetByRecno_CustomResult)
    {
      $this->WEBWebUserGetByRecno_CustomResult = $WEBWebUserGetByRecno_CustomResult;
      return $this;
    }

}
