<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetAccreditationFeeList
{

    /**
     * @var guid $OrganizationCustomerKey
     */
    protected $OrganizationCustomerKey = null;

    /**
     * @var guid $AccreditationProgramKey
     */
    protected $AccreditationProgramKey = null;

    /**
     * @param guid $OrganizationCustomerKey
     * @param guid $AccreditationProgramKey
     */
    public function __construct($OrganizationCustomerKey, $AccreditationProgramKey)
    {
      $this->OrganizationCustomerKey = $OrganizationCustomerKey;
      $this->AccreditationProgramKey = $AccreditationProgramKey;
    }

    /**
     * @return guid
     */
    public function getOrganizationCustomerKey()
    {
      return $this->OrganizationCustomerKey;
    }

    /**
     * @param guid $OrganizationCustomerKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetAccreditationFeeList
     */
    public function setOrganizationCustomerKey($OrganizationCustomerKey)
    {
      $this->OrganizationCustomerKey = $OrganizationCustomerKey;
      return $this;
    }

    /**
     * @return guid
     */
    public function getAccreditationProgramKey()
    {
      return $this->AccreditationProgramKey;
    }

    /**
     * @param guid $AccreditationProgramKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetAccreditationFeeList
     */
    public function setAccreditationProgramKey($AccreditationProgramKey)
    {
      $this->AccreditationProgramKey = $AccreditationProgramKey;
      return $this;
    }

}
