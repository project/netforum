<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class CreatePaymentResponse
{

    /**
     * @var CreatePaymentResult $CreatePaymentResult
     */
    protected $CreatePaymentResult = null;

    /**
     * @param CreatePaymentResult $CreatePaymentResult
     */
    public function __construct($CreatePaymentResult)
    {
      $this->CreatePaymentResult = $CreatePaymentResult;
    }

    /**
     * @return CreatePaymentResult
     */
    public function getCreatePaymentResult()
    {
      return $this->CreatePaymentResult;
    }

    /**
     * @param CreatePaymentResult $CreatePaymentResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\CreatePaymentResponse
     */
    public function setCreatePaymentResult($CreatePaymentResult)
    {
      $this->CreatePaymentResult = $CreatePaymentResult;
      return $this;
    }

}
