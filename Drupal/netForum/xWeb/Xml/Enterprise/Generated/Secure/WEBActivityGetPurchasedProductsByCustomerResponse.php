<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBActivityGetPurchasedProductsByCustomerResponse
{

    /**
     * @var WEBActivityGetPurchasedProductsByCustomerResult $WEBActivityGetPurchasedProductsByCustomerResult
     */
    protected $WEBActivityGetPurchasedProductsByCustomerResult = null;

    /**
     * @param WEBActivityGetPurchasedProductsByCustomerResult $WEBActivityGetPurchasedProductsByCustomerResult
     */
    public function __construct($WEBActivityGetPurchasedProductsByCustomerResult)
    {
      $this->WEBActivityGetPurchasedProductsByCustomerResult = $WEBActivityGetPurchasedProductsByCustomerResult;
    }

    /**
     * @return WEBActivityGetPurchasedProductsByCustomerResult
     */
    public function getWEBActivityGetPurchasedProductsByCustomerResult()
    {
      return $this->WEBActivityGetPurchasedProductsByCustomerResult;
    }

    /**
     * @param WEBActivityGetPurchasedProductsByCustomerResult $WEBActivityGetPurchasedProductsByCustomerResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBActivityGetPurchasedProductsByCustomerResponse
     */
    public function setWEBActivityGetPurchasedProductsByCustomerResult($WEBActivityGetPurchasedProductsByCustomerResult)
    {
      $this->WEBActivityGetPurchasedProductsByCustomerResult = $WEBActivityGetPurchasedProductsByCustomerResult;
      return $this;
    }

}
