<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetPendingOrderResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartGetPendingOrderResult
     */
    protected $WEBCentralizedShoppingCartGetPendingOrderResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartGetPendingOrderResult
     */
    public function __construct($WEBCentralizedShoppingCartGetPendingOrderResult)
    {
      $this->WEBCentralizedShoppingCartGetPendingOrderResult = $WEBCentralizedShoppingCartGetPendingOrderResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartGetPendingOrderResult()
    {
      return $this->WEBCentralizedShoppingCartGetPendingOrderResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartGetPendingOrderResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetPendingOrderResponse
     */
    public function setWEBCentralizedShoppingCartGetPendingOrderResult($WEBCentralizedShoppingCartGetPendingOrderResult)
    {
      $this->WEBCentralizedShoppingCartGetPendingOrderResult = $WEBCentralizedShoppingCartGetPendingOrderResult;
      return $this;
    }

}
