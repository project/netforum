<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistrationResponse
{

    /**
     * @var EventsRegistrantGroupType $WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistrationResult
     */
    protected $WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistrationResult = null;

    /**
     * @param EventsRegistrantGroupType $WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistrationResult
     */
    public function __construct($WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistrationResult)
    {
      $this->WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistrationResult = $WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistrationResult;
    }

    /**
     * @return EventsRegistrantGroupType
     */
    public function getWEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistrationResult()
    {
      return $this->WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistrationResult;
    }

    /**
     * @param EventsRegistrantGroupType $WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistrationResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistrationResponse
     */
    public function setWEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistrationResult($WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistrationResult)
    {
      $this->WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistrationResult = $WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistrationResult;
      return $this;
    }

}
