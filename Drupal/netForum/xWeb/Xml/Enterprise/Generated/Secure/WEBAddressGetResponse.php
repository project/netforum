<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBAddressGetResponse
{

    /**
     * @var CustomerAddressType $WEBAddressGetResult
     */
    protected $WEBAddressGetResult = null;

    /**
     * @param CustomerAddressType $WEBAddressGetResult
     */
    public function __construct($WEBAddressGetResult)
    {
      $this->WEBAddressGetResult = $WEBAddressGetResult;
    }

    /**
     * @return CustomerAddressType
     */
    public function getWEBAddressGetResult()
    {
      return $this->WEBAddressGetResult;
    }

    /**
     * @param CustomerAddressType $WEBAddressGetResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBAddressGetResponse
     */
    public function setWEBAddressGetResult($WEBAddressGetResult)
    {
      $this->WEBAddressGetResult = $WEBAddressGetResult;
      return $this;
    }

}
