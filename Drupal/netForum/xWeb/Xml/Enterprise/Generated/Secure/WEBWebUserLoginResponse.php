<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserLoginResponse
{

    /**
     * @var WebUserType $WEBWebUserLoginResult
     */
    protected $WEBWebUserLoginResult = null;

    /**
     * @param WebUserType $WEBWebUserLoginResult
     */
    public function __construct($WEBWebUserLoginResult)
    {
      $this->WEBWebUserLoginResult = $WEBWebUserLoginResult;
    }

    /**
     * @return WebUserType
     */
    public function getWEBWebUserLoginResult()
    {
      return $this->WEBWebUserLoginResult;
    }

    /**
     * @param WebUserType $WEBWebUserLoginResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserLoginResponse
     */
    public function setWEBWebUserLoginResult($WEBWebUserLoginResult)
    {
      $this->WEBWebUserLoginResult = $WEBWebUserLoginResult;
      return $this;
    }

}
