<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartLoadLineItem
{

    /**
     * @var InvoiceDetailType $oLineItem
     */
    protected $oLineItem = null;

    /**
     * @param InvoiceDetailType $oLineItem
     */
    public function __construct($oLineItem)
    {
      $this->oLineItem = $oLineItem;
    }

    /**
     * @return InvoiceDetailType
     */
    public function getOLineItem()
    {
      return $this->oLineItem;
    }

    /**
     * @param InvoiceDetailType $oLineItem
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartLoadLineItem
     */
    public function setOLineItem($oLineItem)
    {
      $this->oLineItem = $oLineItem;
      return $this;
    }

}
