<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBUpdateSystemOption
{

    /**
     * @var string $szOptionName
     */
    protected $szOptionName = null;

    /**
     * @var string $szOptionValue
     */
    protected $szOptionValue = null;

    /**
     * @param string $szOptionName
     * @param string $szOptionValue
     */
    public function __construct($szOptionName, $szOptionValue)
    {
      $this->szOptionName = $szOptionName;
      $this->szOptionValue = $szOptionValue;
    }

    /**
     * @return string
     */
    public function getSzOptionName()
    {
      return $this->szOptionName;
    }

    /**
     * @param string $szOptionName
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBUpdateSystemOption
     */
    public function setSzOptionName($szOptionName)
    {
      $this->szOptionName = $szOptionName;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzOptionValue()
    {
      return $this->szOptionValue;
    }

    /**
     * @param string $szOptionValue
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBUpdateSystemOption
     */
    public function setSzOptionValue($szOptionValue)
    {
      $this->szOptionValue = $szOptionValue;
      return $this;
    }

}
