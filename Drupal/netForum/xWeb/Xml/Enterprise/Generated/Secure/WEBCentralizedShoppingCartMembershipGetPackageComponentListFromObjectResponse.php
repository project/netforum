<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResponse
{

    /**
     * @var WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResult $WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResult
     */
    protected $WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResult = null;

    /**
     * @param WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResult $WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResult
     */
    public function __construct($WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResult)
    {
      $this->WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResult = $WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResult;
    }

    /**
     * @return WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResult
     */
    public function getWEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResult()
    {
      return $this->WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResult;
    }

    /**
     * @param WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResult $WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResponse
     */
    public function setWEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResult($WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResult)
    {
      $this->WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResult = $WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResult;
      return $this;
    }

}
