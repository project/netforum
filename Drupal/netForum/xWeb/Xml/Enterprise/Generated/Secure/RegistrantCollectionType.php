<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class RegistrantCollectionType
{

    /**
     * @var EventsRegistrantType[] $EventsRegistrant
     */
    protected $EventsRegistrant = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return EventsRegistrantType[]
     */
    public function getEventsRegistrant()
    {
      return $this->EventsRegistrant;
    }

    /**
     * @param EventsRegistrantType[] $EventsRegistrant
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\RegistrantCollectionType
     */
    public function setEventsRegistrant(array $EventsRegistrant = null)
    {
      $this->EventsRegistrant = $EventsRegistrant;
      return $this;
    }

}
