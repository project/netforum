<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartGetResult
     */
    protected $WEBCentralizedShoppingCartGetResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartGetResult
     */
    public function __construct($WEBCentralizedShoppingCartGetResult)
    {
      $this->WEBCentralizedShoppingCartGetResult = $WEBCentralizedShoppingCartGetResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartGetResult()
    {
      return $this->WEBCentralizedShoppingCartGetResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartGetResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetResponse
     */
    public function setWEBCentralizedShoppingCartGetResult($WEBCentralizedShoppingCartGetResult)
    {
      $this->WEBCentralizedShoppingCartGetResult = $WEBCentralizedShoppingCartGetResult;
      return $this;
    }

}
