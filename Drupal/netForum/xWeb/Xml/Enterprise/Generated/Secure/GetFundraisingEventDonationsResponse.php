<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetFundraisingEventDonationsResponse
{

    /**
     * @var ArrayOfDonation $GetFundraisingEventDonationsResult
     */
    protected $GetFundraisingEventDonationsResult = null;

    /**
     * @param ArrayOfDonation $GetFundraisingEventDonationsResult
     */
    public function __construct($GetFundraisingEventDonationsResult)
    {
      $this->GetFundraisingEventDonationsResult = $GetFundraisingEventDonationsResult;
    }

    /**
     * @return ArrayOfDonation
     */
    public function getGetFundraisingEventDonationsResult()
    {
      return $this->GetFundraisingEventDonationsResult;
    }

    /**
     * @param ArrayOfDonation $GetFundraisingEventDonationsResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetFundraisingEventDonationsResponse
     */
    public function setGetFundraisingEventDonationsResult($GetFundraisingEventDonationsResult)
    {
      $this->GetFundraisingEventDonationsResult = $GetFundraisingEventDonationsResult;
      return $this;
    }

}
