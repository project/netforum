<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBOrganizationGet
{

    /**
     * @var guid $key
     */
    protected $key = null;

    /**
     * @param guid $key
     */
    public function __construct($key)
    {
      $this->key = $key;
    }

    /**
     * @return guid
     */
    public function getKey()
    {
      return $this->key;
    }

    /**
     * @param guid $key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBOrganizationGet
     */
    public function setKey($key)
    {
      $this->key = $key;
      return $this;
    }

}
