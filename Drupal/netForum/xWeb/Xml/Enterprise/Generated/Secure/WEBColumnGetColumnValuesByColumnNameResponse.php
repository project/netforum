<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBColumnGetColumnValuesByColumnNameResponse
{

    /**
     * @var ArrayOfAvailableValue $WEBColumnGetColumnValuesByColumnNameResult
     */
    protected $WEBColumnGetColumnValuesByColumnNameResult = null;

    /**
     * @param ArrayOfAvailableValue $WEBColumnGetColumnValuesByColumnNameResult
     */
    public function __construct($WEBColumnGetColumnValuesByColumnNameResult)
    {
      $this->WEBColumnGetColumnValuesByColumnNameResult = $WEBColumnGetColumnValuesByColumnNameResult;
    }

    /**
     * @return ArrayOfAvailableValue
     */
    public function getWEBColumnGetColumnValuesByColumnNameResult()
    {
      return $this->WEBColumnGetColumnValuesByColumnNameResult;
    }

    /**
     * @param ArrayOfAvailableValue $WEBColumnGetColumnValuesByColumnNameResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBColumnGetColumnValuesByColumnNameResponse
     */
    public function setWEBColumnGetColumnValuesByColumnNameResult($WEBColumnGetColumnValuesByColumnNameResult)
    {
      $this->WEBColumnGetColumnValuesByColumnNameResult = $WEBColumnGetColumnValuesByColumnNameResult;
      return $this;
    }

}
