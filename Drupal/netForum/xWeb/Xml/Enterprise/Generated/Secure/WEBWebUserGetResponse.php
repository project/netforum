<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserGetResponse
{

    /**
     * @var WebUserType $WEBWebUserGetResult
     */
    protected $WEBWebUserGetResult = null;

    /**
     * @param WebUserType $WEBWebUserGetResult
     */
    public function __construct($WEBWebUserGetResult)
    {
      $this->WEBWebUserGetResult = $WEBWebUserGetResult;
    }

    /**
     * @return WebUserType
     */
    public function getWEBWebUserGetResult()
    {
      return $this->WEBWebUserGetResult;
    }

    /**
     * @param WebUserType $WEBWebUserGetResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserGetResponse
     */
    public function setWEBWebUserGetResult($WEBWebUserGetResult)
    {
      $this->WEBWebUserGetResult = $WEBWebUserGetResult;
      return $this;
    }

}
