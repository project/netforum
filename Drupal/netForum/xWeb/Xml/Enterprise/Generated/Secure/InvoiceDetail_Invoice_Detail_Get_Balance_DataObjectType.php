<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
{

    /**
     * @var av_key_Type $igb_ivd_key
     */
    protected $igb_ivd_key = null;

    /**
     * @var av_key_Type $igb_inv_key
     */
    protected $igb_inv_key = null;

    /**
     * @var av_decimal4_Type $igb_ivd_qty
     */
    protected $igb_ivd_qty = null;

    /**
     * @var av_decimal4_Type $igb_ship_qty
     */
    protected $igb_ship_qty = null;

    /**
     * @var av_currency_Type $igb_ivd_price
     */
    protected $igb_ivd_price = null;

    /**
     * @var decimal_Type $igb_unit_price_with_discount
     */
    protected $igb_unit_price_with_discount = null;

    /**
     * @var decimal_Type $igb_lineamount
     */
    protected $igb_lineamount = null;

    /**
     * @var decimal_Type $igb_lineamount_no_discount
     */
    protected $igb_lineamount_no_discount = null;

    /**
     * @var decimal_Type $igb_originallineamount
     */
    protected $igb_originallineamount = null;

    /**
     * @var av_currency_Type $igb_payamount
     */
    protected $igb_payamount = null;

    /**
     * @var av_currency_Type $igb_payamount_noreturn
     */
    protected $igb_payamount_noreturn = null;

    /**
     * @var decimal_Type $igb_linebalance
     */
    protected $igb_linebalance = null;

    /**
     * @var decimal_Type $igb_returnquantity
     */
    protected $igb_returnquantity = null;

    /**
     * @var decimal_Type $igb_cancelquantity
     */
    protected $igb_cancelquantity = null;

    /**
     * @var decimal_Type $igb_minimumquantity
     */
    protected $igb_minimumquantity = null;

    /**
     * @var decimal_Type $igb_available_for_return
     */
    protected $igb_available_for_return = null;

    /**
     * @var decimal_Type $igb_available_for_cancel
     */
    protected $igb_available_for_cancel = null;

    /**
     * @var decimal_Type $igb_balance_quantity
     */
    protected $igb_balance_quantity = null;

    /**
     * @var decimal_Type $igb_CreditDollarAmount
     */
    protected $igb_CreditDollarAmount = null;

    /**
     * @var decimal_Type $igb_TotalRecognized
     */
    protected $igb_TotalRecognized = null;

    /**
     * @var decimal_Type $igb_DeferralBalance
     */
    protected $igb_DeferralBalance = null;

    /**
     * @var av_currency_Type $igb_Deferral_Price
     */
    protected $igb_Deferral_Price = null;

    /**
     * @var av_integer_Type $igb_Deferral_Number_Periods
     */
    protected $igb_Deferral_Number_Periods = null;

    /**
     * @var av_integer_Type $igb_Deferral_Number_Periods_Recognized
     */
    protected $igb_Deferral_Number_Periods_Recognized = null;

    /**
     * @var av_currency_Type $igb_Tax
     */
    protected $igb_Tax = null;

    /**
     * @var av_currency_Type $igb_Shipping
     */
    protected $igb_Shipping = null;

    /**
     * @var av_currency_Type $igb_discountamount
     */
    protected $igb_discountamount = null;

    /**
     * @var av_key_Type $igb_TransactionCurrencyKey
     */
    protected $igb_TransactionCurrencyKey = null;

    /**
     * @var stringLength10_Type $igb_HomeCurrencyCode
     */
    protected $igb_HomeCurrencyCode = null;

    /**
     * @var av_key_Type $igb_MulticurrencyKey
     */
    protected $igb_MulticurrencyKey = null;

    /**
     * @var av_currency_Type $igb_writeoffamount
     */
    protected $igb_writeoffamount = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getIgb_ivd_key()
    {
      return $this->igb_ivd_key;
    }

    /**
     * @param av_key_Type $igb_ivd_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_ivd_key($igb_ivd_key)
    {
      $this->igb_ivd_key = $igb_ivd_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getIgb_inv_key()
    {
      return $this->igb_inv_key;
    }

    /**
     * @param av_key_Type $igb_inv_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_inv_key($igb_inv_key)
    {
      $this->igb_inv_key = $igb_inv_key;
      return $this;
    }

    /**
     * @return av_decimal4_Type
     */
    public function getIgb_ivd_qty()
    {
      return $this->igb_ivd_qty;
    }

    /**
     * @param av_decimal4_Type $igb_ivd_qty
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_ivd_qty($igb_ivd_qty)
    {
      $this->igb_ivd_qty = $igb_ivd_qty;
      return $this;
    }

    /**
     * @return av_decimal4_Type
     */
    public function getIgb_ship_qty()
    {
      return $this->igb_ship_qty;
    }

    /**
     * @param av_decimal4_Type $igb_ship_qty
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_ship_qty($igb_ship_qty)
    {
      $this->igb_ship_qty = $igb_ship_qty;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIgb_ivd_price()
    {
      return $this->igb_ivd_price;
    }

    /**
     * @param av_currency_Type $igb_ivd_price
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_ivd_price($igb_ivd_price)
    {
      $this->igb_ivd_price = $igb_ivd_price;
      return $this;
    }

    /**
     * @return decimal_Type
     */
    public function getIgb_unit_price_with_discount()
    {
      return $this->igb_unit_price_with_discount;
    }

    /**
     * @param decimal_Type $igb_unit_price_with_discount
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_unit_price_with_discount($igb_unit_price_with_discount)
    {
      $this->igb_unit_price_with_discount = $igb_unit_price_with_discount;
      return $this;
    }

    /**
     * @return decimal_Type
     */
    public function getIgb_lineamount()
    {
      return $this->igb_lineamount;
    }

    /**
     * @param decimal_Type $igb_lineamount
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_lineamount($igb_lineamount)
    {
      $this->igb_lineamount = $igb_lineamount;
      return $this;
    }

    /**
     * @return decimal_Type
     */
    public function getIgb_lineamount_no_discount()
    {
      return $this->igb_lineamount_no_discount;
    }

    /**
     * @param decimal_Type $igb_lineamount_no_discount
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_lineamount_no_discount($igb_lineamount_no_discount)
    {
      $this->igb_lineamount_no_discount = $igb_lineamount_no_discount;
      return $this;
    }

    /**
     * @return decimal_Type
     */
    public function getIgb_originallineamount()
    {
      return $this->igb_originallineamount;
    }

    /**
     * @param decimal_Type $igb_originallineamount
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_originallineamount($igb_originallineamount)
    {
      $this->igb_originallineamount = $igb_originallineamount;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIgb_payamount()
    {
      return $this->igb_payamount;
    }

    /**
     * @param av_currency_Type $igb_payamount
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_payamount($igb_payamount)
    {
      $this->igb_payamount = $igb_payamount;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIgb_payamount_noreturn()
    {
      return $this->igb_payamount_noreturn;
    }

    /**
     * @param av_currency_Type $igb_payamount_noreturn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_payamount_noreturn($igb_payamount_noreturn)
    {
      $this->igb_payamount_noreturn = $igb_payamount_noreturn;
      return $this;
    }

    /**
     * @return decimal_Type
     */
    public function getIgb_linebalance()
    {
      return $this->igb_linebalance;
    }

    /**
     * @param decimal_Type $igb_linebalance
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_linebalance($igb_linebalance)
    {
      $this->igb_linebalance = $igb_linebalance;
      return $this;
    }

    /**
     * @return decimal_Type
     */
    public function getIgb_returnquantity()
    {
      return $this->igb_returnquantity;
    }

    /**
     * @param decimal_Type $igb_returnquantity
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_returnquantity($igb_returnquantity)
    {
      $this->igb_returnquantity = $igb_returnquantity;
      return $this;
    }

    /**
     * @return decimal_Type
     */
    public function getIgb_cancelquantity()
    {
      return $this->igb_cancelquantity;
    }

    /**
     * @param decimal_Type $igb_cancelquantity
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_cancelquantity($igb_cancelquantity)
    {
      $this->igb_cancelquantity = $igb_cancelquantity;
      return $this;
    }

    /**
     * @return decimal_Type
     */
    public function getIgb_minimumquantity()
    {
      return $this->igb_minimumquantity;
    }

    /**
     * @param decimal_Type $igb_minimumquantity
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_minimumquantity($igb_minimumquantity)
    {
      $this->igb_minimumquantity = $igb_minimumquantity;
      return $this;
    }

    /**
     * @return decimal_Type
     */
    public function getIgb_available_for_return()
    {
      return $this->igb_available_for_return;
    }

    /**
     * @param decimal_Type $igb_available_for_return
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_available_for_return($igb_available_for_return)
    {
      $this->igb_available_for_return = $igb_available_for_return;
      return $this;
    }

    /**
     * @return decimal_Type
     */
    public function getIgb_available_for_cancel()
    {
      return $this->igb_available_for_cancel;
    }

    /**
     * @param decimal_Type $igb_available_for_cancel
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_available_for_cancel($igb_available_for_cancel)
    {
      $this->igb_available_for_cancel = $igb_available_for_cancel;
      return $this;
    }

    /**
     * @return decimal_Type
     */
    public function getIgb_balance_quantity()
    {
      return $this->igb_balance_quantity;
    }

    /**
     * @param decimal_Type $igb_balance_quantity
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_balance_quantity($igb_balance_quantity)
    {
      $this->igb_balance_quantity = $igb_balance_quantity;
      return $this;
    }

    /**
     * @return decimal_Type
     */
    public function getIgb_CreditDollarAmount()
    {
      return $this->igb_CreditDollarAmount;
    }

    /**
     * @param decimal_Type $igb_CreditDollarAmount
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_CreditDollarAmount($igb_CreditDollarAmount)
    {
      $this->igb_CreditDollarAmount = $igb_CreditDollarAmount;
      return $this;
    }

    /**
     * @return decimal_Type
     */
    public function getIgb_TotalRecognized()
    {
      return $this->igb_TotalRecognized;
    }

    /**
     * @param decimal_Type $igb_TotalRecognized
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_TotalRecognized($igb_TotalRecognized)
    {
      $this->igb_TotalRecognized = $igb_TotalRecognized;
      return $this;
    }

    /**
     * @return decimal_Type
     */
    public function getIgb_DeferralBalance()
    {
      return $this->igb_DeferralBalance;
    }

    /**
     * @param decimal_Type $igb_DeferralBalance
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_DeferralBalance($igb_DeferralBalance)
    {
      $this->igb_DeferralBalance = $igb_DeferralBalance;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIgb_Deferral_Price()
    {
      return $this->igb_Deferral_Price;
    }

    /**
     * @param av_currency_Type $igb_Deferral_Price
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_Deferral_Price($igb_Deferral_Price)
    {
      $this->igb_Deferral_Price = $igb_Deferral_Price;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getIgb_Deferral_Number_Periods()
    {
      return $this->igb_Deferral_Number_Periods;
    }

    /**
     * @param av_integer_Type $igb_Deferral_Number_Periods
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_Deferral_Number_Periods($igb_Deferral_Number_Periods)
    {
      $this->igb_Deferral_Number_Periods = $igb_Deferral_Number_Periods;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getIgb_Deferral_Number_Periods_Recognized()
    {
      return $this->igb_Deferral_Number_Periods_Recognized;
    }

    /**
     * @param av_integer_Type $igb_Deferral_Number_Periods_Recognized
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_Deferral_Number_Periods_Recognized($igb_Deferral_Number_Periods_Recognized)
    {
      $this->igb_Deferral_Number_Periods_Recognized = $igb_Deferral_Number_Periods_Recognized;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIgb_Tax()
    {
      return $this->igb_Tax;
    }

    /**
     * @param av_currency_Type $igb_Tax
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_Tax($igb_Tax)
    {
      $this->igb_Tax = $igb_Tax;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIgb_Shipping()
    {
      return $this->igb_Shipping;
    }

    /**
     * @param av_currency_Type $igb_Shipping
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_Shipping($igb_Shipping)
    {
      $this->igb_Shipping = $igb_Shipping;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIgb_discountamount()
    {
      return $this->igb_discountamount;
    }

    /**
     * @param av_currency_Type $igb_discountamount
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_discountamount($igb_discountamount)
    {
      $this->igb_discountamount = $igb_discountamount;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getIgb_TransactionCurrencyKey()
    {
      return $this->igb_TransactionCurrencyKey;
    }

    /**
     * @param av_key_Type $igb_TransactionCurrencyKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_TransactionCurrencyKey($igb_TransactionCurrencyKey)
    {
      $this->igb_TransactionCurrencyKey = $igb_TransactionCurrencyKey;
      return $this;
    }

    /**
     * @return stringLength10_Type
     */
    public function getIgb_HomeCurrencyCode()
    {
      return $this->igb_HomeCurrencyCode;
    }

    /**
     * @param stringLength10_Type $igb_HomeCurrencyCode
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_HomeCurrencyCode($igb_HomeCurrencyCode)
    {
      $this->igb_HomeCurrencyCode = $igb_HomeCurrencyCode;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getIgb_MulticurrencyKey()
    {
      return $this->igb_MulticurrencyKey;
    }

    /**
     * @param av_key_Type $igb_MulticurrencyKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_MulticurrencyKey($igb_MulticurrencyKey)
    {
      $this->igb_MulticurrencyKey = $igb_MulticurrencyKey;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIgb_writeoffamount()
    {
      return $this->igb_writeoffamount;
    }

    /**
     * @param av_currency_Type $igb_writeoffamount
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType
     */
    public function setIgb_writeoffamount($igb_writeoffamount)
    {
      $this->igb_writeoffamount = $igb_writeoffamount;
      return $this;
    }

}
