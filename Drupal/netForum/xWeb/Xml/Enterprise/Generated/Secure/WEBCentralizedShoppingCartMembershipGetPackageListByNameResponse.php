<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartMembershipGetPackageListByNameResponse
{

    /**
     * @var WEBCentralizedShoppingCartMembershipGetPackageListByNameResult $WEBCentralizedShoppingCartMembershipGetPackageListByNameResult
     */
    protected $WEBCentralizedShoppingCartMembershipGetPackageListByNameResult = null;

    /**
     * @param WEBCentralizedShoppingCartMembershipGetPackageListByNameResult $WEBCentralizedShoppingCartMembershipGetPackageListByNameResult
     */
    public function __construct($WEBCentralizedShoppingCartMembershipGetPackageListByNameResult)
    {
      $this->WEBCentralizedShoppingCartMembershipGetPackageListByNameResult = $WEBCentralizedShoppingCartMembershipGetPackageListByNameResult;
    }

    /**
     * @return WEBCentralizedShoppingCartMembershipGetPackageListByNameResult
     */
    public function getWEBCentralizedShoppingCartMembershipGetPackageListByNameResult()
    {
      return $this->WEBCentralizedShoppingCartMembershipGetPackageListByNameResult;
    }

    /**
     * @param WEBCentralizedShoppingCartMembershipGetPackageListByNameResult $WEBCentralizedShoppingCartMembershipGetPackageListByNameResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartMembershipGetPackageListByNameResponse
     */
    public function setWEBCentralizedShoppingCartMembershipGetPackageListByNameResult($WEBCentralizedShoppingCartMembershipGetPackageListByNameResult)
    {
      $this->WEBCentralizedShoppingCartMembershipGetPackageListByNameResult = $WEBCentralizedShoppingCartMembershipGetPackageListByNameResult;
      return $this;
    }

}
