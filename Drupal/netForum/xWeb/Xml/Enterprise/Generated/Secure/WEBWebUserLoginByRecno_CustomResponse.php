<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserLoginByRecno_CustomResponse
{

    /**
     * @var string $WEBWebUserLoginByRecno_CustomResult
     */
    protected $WEBWebUserLoginByRecno_CustomResult = null;

    /**
     * @param string $WEBWebUserLoginByRecno_CustomResult
     */
    public function __construct($WEBWebUserLoginByRecno_CustomResult)
    {
      $this->WEBWebUserLoginByRecno_CustomResult = $WEBWebUserLoginByRecno_CustomResult;
    }

    /**
     * @return string
     */
    public function getWEBWebUserLoginByRecno_CustomResult()
    {
      return $this->WEBWebUserLoginByRecno_CustomResult;
    }

    /**
     * @param string $WEBWebUserLoginByRecno_CustomResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserLoginByRecno_CustomResponse
     */
    public function setWEBWebUserLoginByRecno_CustomResult($WEBWebUserLoginByRecno_CustomResult)
    {
      $this->WEBWebUserLoginByRecno_CustomResult = $WEBWebUserLoginByRecno_CustomResult;
      return $this;
    }

}
