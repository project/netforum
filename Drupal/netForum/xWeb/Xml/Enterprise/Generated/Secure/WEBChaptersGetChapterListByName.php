<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBChaptersGetChapterListByName
{

    /**
     * @var string $ChapterName
     */
    protected $ChapterName = null;

    /**
     * @param string $ChapterName
     */
    public function __construct($ChapterName)
    {
      $this->ChapterName = $ChapterName;
    }

    /**
     * @return string
     */
    public function getChapterName()
    {
      return $this->ChapterName;
    }

    /**
     * @param string $ChapterName
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBChaptersGetChapterListByName
     */
    public function setChapterName($ChapterName)
    {
      $this->ChapterName = $ChapterName;
      return $this;
    }

}
