<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartOpenInvoiceGetListResponse
{

    /**
     * @var WEBCentralizedShoppingCartOpenInvoiceGetListResult $WEBCentralizedShoppingCartOpenInvoiceGetListResult
     */
    protected $WEBCentralizedShoppingCartOpenInvoiceGetListResult = null;

    /**
     * @param WEBCentralizedShoppingCartOpenInvoiceGetListResult $WEBCentralizedShoppingCartOpenInvoiceGetListResult
     */
    public function __construct($WEBCentralizedShoppingCartOpenInvoiceGetListResult)
    {
      $this->WEBCentralizedShoppingCartOpenInvoiceGetListResult = $WEBCentralizedShoppingCartOpenInvoiceGetListResult;
    }

    /**
     * @return WEBCentralizedShoppingCartOpenInvoiceGetListResult
     */
    public function getWEBCentralizedShoppingCartOpenInvoiceGetListResult()
    {
      return $this->WEBCentralizedShoppingCartOpenInvoiceGetListResult;
    }

    /**
     * @param WEBCentralizedShoppingCartOpenInvoiceGetListResult $WEBCentralizedShoppingCartOpenInvoiceGetListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartOpenInvoiceGetListResponse
     */
    public function setWEBCentralizedShoppingCartOpenInvoiceGetListResult($WEBCentralizedShoppingCartOpenInvoiceGetListResult)
    {
      $this->WEBCentralizedShoppingCartOpenInvoiceGetListResult = $WEBCentralizedShoppingCartOpenInvoiceGetListResult;
      return $this;
    }

}
