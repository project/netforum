<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBActivityGetPurchasedChapterMembershipsByCustomerResponse
{

    /**
     * @var WEBActivityGetPurchasedChapterMembershipsByCustomerResult $WEBActivityGetPurchasedChapterMembershipsByCustomerResult
     */
    protected $WEBActivityGetPurchasedChapterMembershipsByCustomerResult = null;

    /**
     * @param WEBActivityGetPurchasedChapterMembershipsByCustomerResult $WEBActivityGetPurchasedChapterMembershipsByCustomerResult
     */
    public function __construct($WEBActivityGetPurchasedChapterMembershipsByCustomerResult)
    {
      $this->WEBActivityGetPurchasedChapterMembershipsByCustomerResult = $WEBActivityGetPurchasedChapterMembershipsByCustomerResult;
    }

    /**
     * @return WEBActivityGetPurchasedChapterMembershipsByCustomerResult
     */
    public function getWEBActivityGetPurchasedChapterMembershipsByCustomerResult()
    {
      return $this->WEBActivityGetPurchasedChapterMembershipsByCustomerResult;
    }

    /**
     * @param WEBActivityGetPurchasedChapterMembershipsByCustomerResult $WEBActivityGetPurchasedChapterMembershipsByCustomerResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBActivityGetPurchasedChapterMembershipsByCustomerResponse
     */
    public function setWEBActivityGetPurchasedChapterMembershipsByCustomerResult($WEBActivityGetPurchasedChapterMembershipsByCustomerResult)
    {
      $this->WEBActivityGetPurchasedChapterMembershipsByCustomerResult = $WEBActivityGetPurchasedChapterMembershipsByCustomerResult;
      return $this;
    }

}
