<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBFaxGetTypesResponse
{

    /**
     * @var WEBFaxGetTypesResult $WEBFaxGetTypesResult
     */
    protected $WEBFaxGetTypesResult = null;

    /**
     * @param WEBFaxGetTypesResult $WEBFaxGetTypesResult
     */
    public function __construct($WEBFaxGetTypesResult)
    {
      $this->WEBFaxGetTypesResult = $WEBFaxGetTypesResult;
    }

    /**
     * @return WEBFaxGetTypesResult
     */
    public function getWEBFaxGetTypesResult()
    {
      return $this->WEBFaxGetTypesResult;
    }

    /**
     * @param WEBFaxGetTypesResult $WEBFaxGetTypesResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBFaxGetTypesResponse
     */
    public function setWEBFaxGetTypesResult($WEBFaxGetTypesResult)
    {
      $this->WEBFaxGetTypesResult = $WEBFaxGetTypesResult;
      return $this;
    }

}
