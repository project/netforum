<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBEmailInsertResponse
{

    /**
     * @var CustomerEmailType $WEBEmailInsertResult
     */
    protected $WEBEmailInsertResult = null;

    /**
     * @param CustomerEmailType $WEBEmailInsertResult
     */
    public function __construct($WEBEmailInsertResult)
    {
      $this->WEBEmailInsertResult = $WEBEmailInsertResult;
    }

    /**
     * @return CustomerEmailType
     */
    public function getWEBEmailInsertResult()
    {
      return $this->WEBEmailInsertResult;
    }

    /**
     * @param CustomerEmailType $WEBEmailInsertResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBEmailInsertResponse
     */
    public function setWEBEmailInsertResult($WEBEmailInsertResult)
    {
      $this->WEBEmailInsertResult = $WEBEmailInsertResult;
      return $this;
    }

}
