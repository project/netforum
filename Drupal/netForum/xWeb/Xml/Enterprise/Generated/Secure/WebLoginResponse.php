<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WebLoginResponse
{

    /**
     * @var string $WebLoginResult
     */
    protected $WebLoginResult = null;

    /**
     * @param string $WebLoginResult
     */
    public function __construct($WebLoginResult)
    {
      $this->WebLoginResult = $WebLoginResult;
    }

    /**
     * @return string
     */
    public function getWebLoginResult()
    {
      return $this->WebLoginResult;
    }

    /**
     * @param string $WebLoginResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WebLoginResponse
     */
    public function setWebLoginResult($WebLoginResult)
    {
      $this->WebLoginResult = $WebLoginResult;
      return $this;
    }

}
