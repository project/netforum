<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBActivityGetPurchasedDownoadableProductsByCustomerResponse
{

    /**
     * @var WEBActivityGetPurchasedDownoadableProductsByCustomerResult $WEBActivityGetPurchasedDownoadableProductsByCustomerResult
     */
    protected $WEBActivityGetPurchasedDownoadableProductsByCustomerResult = null;

    /**
     * @param WEBActivityGetPurchasedDownoadableProductsByCustomerResult $WEBActivityGetPurchasedDownoadableProductsByCustomerResult
     */
    public function __construct($WEBActivityGetPurchasedDownoadableProductsByCustomerResult)
    {
      $this->WEBActivityGetPurchasedDownoadableProductsByCustomerResult = $WEBActivityGetPurchasedDownoadableProductsByCustomerResult;
    }

    /**
     * @return WEBActivityGetPurchasedDownoadableProductsByCustomerResult
     */
    public function getWEBActivityGetPurchasedDownoadableProductsByCustomerResult()
    {
      return $this->WEBActivityGetPurchasedDownoadableProductsByCustomerResult;
    }

    /**
     * @param WEBActivityGetPurchasedDownoadableProductsByCustomerResult $WEBActivityGetPurchasedDownoadableProductsByCustomerResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBActivityGetPurchasedDownoadableProductsByCustomerResponse
     */
    public function setWEBActivityGetPurchasedDownoadableProductsByCustomerResult($WEBActivityGetPurchasedDownoadableProductsByCustomerResult)
    {
      $this->WEBActivityGetPurchasedDownoadableProductsByCustomerResult = $WEBActivityGetPurchasedDownoadableProductsByCustomerResult;
      return $this;
    }

}
