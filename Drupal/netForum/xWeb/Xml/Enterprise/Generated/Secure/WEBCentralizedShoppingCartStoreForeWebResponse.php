<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartStoreForeWebResponse
{

    /**
     * @var string $WEBCentralizedShoppingCartStoreForeWebResult
     */
    protected $WEBCentralizedShoppingCartStoreForeWebResult = null;

    /**
     * @param string $WEBCentralizedShoppingCartStoreForeWebResult
     */
    public function __construct($WEBCentralizedShoppingCartStoreForeWebResult)
    {
      $this->WEBCentralizedShoppingCartStoreForeWebResult = $WEBCentralizedShoppingCartStoreForeWebResult;
    }

    /**
     * @return string
     */
    public function getWEBCentralizedShoppingCartStoreForeWebResult()
    {
      return $this->WEBCentralizedShoppingCartStoreForeWebResult;
    }

    /**
     * @param string $WEBCentralizedShoppingCartStoreForeWebResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartStoreForeWebResponse
     */
    public function setWEBCentralizedShoppingCartStoreForeWebResult($WEBCentralizedShoppingCartStoreForeWebResult)
    {
      $this->WEBCentralizedShoppingCartStoreForeWebResult = $WEBCentralizedShoppingCartStoreForeWebResult;
      return $this;
    }

}
