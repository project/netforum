<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartAccreditationAddAccreditationResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartAccreditationAddAccreditationResult
     */
    protected $WEBCentralizedShoppingCartAccreditationAddAccreditationResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartAccreditationAddAccreditationResult
     */
    public function __construct($WEBCentralizedShoppingCartAccreditationAddAccreditationResult)
    {
      $this->WEBCentralizedShoppingCartAccreditationAddAccreditationResult = $WEBCentralizedShoppingCartAccreditationAddAccreditationResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartAccreditationAddAccreditationResult()
    {
      return $this->WEBCentralizedShoppingCartAccreditationAddAccreditationResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartAccreditationAddAccreditationResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartAccreditationAddAccreditationResponse
     */
    public function setWEBCentralizedShoppingCartAccreditationAddAccreditationResult($WEBCentralizedShoppingCartAccreditationAddAccreditationResult)
    {
      $this->WEBCentralizedShoppingCartAccreditationAddAccreditationResult = $WEBCentralizedShoppingCartAccreditationAddAccreditationResult;
      return $this;
    }

}
