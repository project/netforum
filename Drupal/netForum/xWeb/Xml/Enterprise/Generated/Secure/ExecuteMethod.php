<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class ExecuteMethod
{

    /**
     * @var string $serviceName
     */
    protected $serviceName = null;

    /**
     * @var string $methodName
     */
    protected $methodName = null;

    /**
     * @var ArrayOfParameter $parameters
     */
    protected $parameters = null;

    /**
     * @param string $serviceName
     * @param string $methodName
     * @param ArrayOfParameter $parameters
     */
    public function __construct($serviceName, $methodName, $parameters)
    {
      $this->serviceName = $serviceName;
      $this->methodName = $methodName;
      $this->parameters = $parameters;
    }

    /**
     * @return string
     */
    public function getServiceName()
    {
      return $this->serviceName;
    }

    /**
     * @param string $serviceName
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExecuteMethod
     */
    public function setServiceName($serviceName)
    {
      $this->serviceName = $serviceName;
      return $this;
    }

    /**
     * @return string
     */
    public function getMethodName()
    {
      return $this->methodName;
    }

    /**
     * @param string $methodName
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExecuteMethod
     */
    public function setMethodName($methodName)
    {
      $this->methodName = $methodName;
      return $this;
    }

    /**
     * @return ArrayOfParameter
     */
    public function getParameters()
    {
      return $this->parameters;
    }

    /**
     * @param ArrayOfParameter $parameters
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExecuteMethod
     */
    public function setParameters($parameters)
    {
      $this->parameters = $parameters;
      return $this;
    }

}
