<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType
{

    /**
     * @var av_key_Type $mxo__ixo_key
     */
    protected $mxo__ixo_key = null;

    /**
     * @var stringLength80_Type $mxo__ixo_rlt_code
     */
    protected $mxo__ixo_rlt_code = null;

    /**
     * @var stringLength150_Type $mxo__ixo_title
     */
    protected $mxo__ixo_title = null;

    /**
     * @var av_date_small_Type $mxo__ixo_start_date
     */
    protected $mxo__ixo_start_date = null;

    /**
     * @var av_date_small_Type $mxo__ixo_end_date
     */
    protected $mxo__ixo_end_date = null;

    /**
     * @var av_key_Type $mxo__ixo_ind_cst_key
     */
    protected $mxo__ixo_ind_cst_key = null;

    /**
     * @var av_key_Type $mxo__ixo_org_cst_key
     */
    protected $mxo__ixo_org_cst_key = null;

    /**
     * @var av_date_small_Type $mxo__ixo_add_date
     */
    protected $mxo__ixo_add_date = null;

    /**
     * @var av_user_Type $mxo__ixo_add_user
     */
    protected $mxo__ixo_add_user = null;

    /**
     * @var av_date_small_Type $mxo__ixo_change_date
     */
    protected $mxo__ixo_change_date = null;

    /**
     * @var av_user_Type $mxo__ixo_change_user
     */
    protected $mxo__ixo_change_user = null;

    /**
     * @var av_delete_flag_Type $mxo__ixo_delete_flag
     */
    protected $mxo__ixo_delete_flag = null;

    /**
     * @var av_key_Type $mxo__ixo_key_ext
     */
    protected $mxo__ixo_key_ext = null;

    /**
     * @var av_key_Type $mxo__ixo_cst_key_owner
     */
    protected $mxo__ixo_cst_key_owner = null;

    /**
     * @var av_key_Type $mxo__ixo_entity_key
     */
    protected $mxo__ixo_entity_key = null;

    /**
     * @var av_flag_Type $mxo__ixo_void_flag
     */
    protected $mxo__ixo_void_flag = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getMxo__ixo_key()
    {
      return $this->mxo__ixo_key;
    }

    /**
     * @param av_key_Type $mxo__ixo_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType
     */
    public function setMxo__ixo_key($mxo__ixo_key)
    {
      $this->mxo__ixo_key = $mxo__ixo_key;
      return $this;
    }

    /**
     * @return stringLength80_Type
     */
    public function getMxo__ixo_rlt_code()
    {
      return $this->mxo__ixo_rlt_code;
    }

    /**
     * @param stringLength80_Type $mxo__ixo_rlt_code
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType
     */
    public function setMxo__ixo_rlt_code($mxo__ixo_rlt_code)
    {
      $this->mxo__ixo_rlt_code = $mxo__ixo_rlt_code;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getMxo__ixo_title()
    {
      return $this->mxo__ixo_title;
    }

    /**
     * @param stringLength150_Type $mxo__ixo_title
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType
     */
    public function setMxo__ixo_title($mxo__ixo_title)
    {
      $this->mxo__ixo_title = $mxo__ixo_title;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getMxo__ixo_start_date()
    {
      return $this->mxo__ixo_start_date;
    }

    /**
     * @param av_date_small_Type $mxo__ixo_start_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType
     */
    public function setMxo__ixo_start_date($mxo__ixo_start_date)
    {
      $this->mxo__ixo_start_date = $mxo__ixo_start_date;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getMxo__ixo_end_date()
    {
      return $this->mxo__ixo_end_date;
    }

    /**
     * @param av_date_small_Type $mxo__ixo_end_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType
     */
    public function setMxo__ixo_end_date($mxo__ixo_end_date)
    {
      $this->mxo__ixo_end_date = $mxo__ixo_end_date;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getMxo__ixo_ind_cst_key()
    {
      return $this->mxo__ixo_ind_cst_key;
    }

    /**
     * @param av_key_Type $mxo__ixo_ind_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType
     */
    public function setMxo__ixo_ind_cst_key($mxo__ixo_ind_cst_key)
    {
      $this->mxo__ixo_ind_cst_key = $mxo__ixo_ind_cst_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getMxo__ixo_org_cst_key()
    {
      return $this->mxo__ixo_org_cst_key;
    }

    /**
     * @param av_key_Type $mxo__ixo_org_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType
     */
    public function setMxo__ixo_org_cst_key($mxo__ixo_org_cst_key)
    {
      $this->mxo__ixo_org_cst_key = $mxo__ixo_org_cst_key;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getMxo__ixo_add_date()
    {
      return $this->mxo__ixo_add_date;
    }

    /**
     * @param av_date_small_Type $mxo__ixo_add_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType
     */
    public function setMxo__ixo_add_date($mxo__ixo_add_date)
    {
      $this->mxo__ixo_add_date = $mxo__ixo_add_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getMxo__ixo_add_user()
    {
      return $this->mxo__ixo_add_user;
    }

    /**
     * @param av_user_Type $mxo__ixo_add_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType
     */
    public function setMxo__ixo_add_user($mxo__ixo_add_user)
    {
      $this->mxo__ixo_add_user = $mxo__ixo_add_user;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getMxo__ixo_change_date()
    {
      return $this->mxo__ixo_change_date;
    }

    /**
     * @param av_date_small_Type $mxo__ixo_change_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType
     */
    public function setMxo__ixo_change_date($mxo__ixo_change_date)
    {
      $this->mxo__ixo_change_date = $mxo__ixo_change_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getMxo__ixo_change_user()
    {
      return $this->mxo__ixo_change_user;
    }

    /**
     * @param av_user_Type $mxo__ixo_change_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType
     */
    public function setMxo__ixo_change_user($mxo__ixo_change_user)
    {
      $this->mxo__ixo_change_user = $mxo__ixo_change_user;
      return $this;
    }

    /**
     * @return av_delete_flag_Type
     */
    public function getMxo__ixo_delete_flag()
    {
      return $this->mxo__ixo_delete_flag;
    }

    /**
     * @param av_delete_flag_Type $mxo__ixo_delete_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType
     */
    public function setMxo__ixo_delete_flag($mxo__ixo_delete_flag)
    {
      $this->mxo__ixo_delete_flag = $mxo__ixo_delete_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getMxo__ixo_key_ext()
    {
      return $this->mxo__ixo_key_ext;
    }

    /**
     * @param av_key_Type $mxo__ixo_key_ext
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType
     */
    public function setMxo__ixo_key_ext($mxo__ixo_key_ext)
    {
      $this->mxo__ixo_key_ext = $mxo__ixo_key_ext;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getMxo__ixo_cst_key_owner()
    {
      return $this->mxo__ixo_cst_key_owner;
    }

    /**
     * @param av_key_Type $mxo__ixo_cst_key_owner
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType
     */
    public function setMxo__ixo_cst_key_owner($mxo__ixo_cst_key_owner)
    {
      $this->mxo__ixo_cst_key_owner = $mxo__ixo_cst_key_owner;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getMxo__ixo_entity_key()
    {
      return $this->mxo__ixo_entity_key;
    }

    /**
     * @param av_key_Type $mxo__ixo_entity_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType
     */
    public function setMxo__ixo_entity_key($mxo__ixo_entity_key)
    {
      $this->mxo__ixo_entity_key = $mxo__ixo_entity_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getMxo__ixo_void_flag()
    {
      return $this->mxo__ixo_void_flag;
    }

    /**
     * @param av_flag_Type $mxo__ixo_void_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType
     */
    public function setMxo__ixo_void_flag($mxo__ixo_void_flag)
    {
      $this->mxo__ixo_void_flag = $mxo__ixo_void_flag;
      return $this;
    }

}
