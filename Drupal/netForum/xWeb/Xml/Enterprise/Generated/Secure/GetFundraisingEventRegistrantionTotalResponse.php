<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetFundraisingEventRegistrantionTotalResponse
{

    /**
     * @var EventUserFundraisingDetail $GetFundraisingEventRegistrantionTotalResult
     */
    protected $GetFundraisingEventRegistrantionTotalResult = null;

    /**
     * @param EventUserFundraisingDetail $GetFundraisingEventRegistrantionTotalResult
     */
    public function __construct($GetFundraisingEventRegistrantionTotalResult)
    {
      $this->GetFundraisingEventRegistrantionTotalResult = $GetFundraisingEventRegistrantionTotalResult;
    }

    /**
     * @return EventUserFundraisingDetail
     */
    public function getGetFundraisingEventRegistrantionTotalResult()
    {
      return $this->GetFundraisingEventRegistrantionTotalResult;
    }

    /**
     * @param EventUserFundraisingDetail $GetFundraisingEventRegistrantionTotalResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetFundraisingEventRegistrantionTotalResponse
     */
    public function setGetFundraisingEventRegistrantionTotalResult($GetFundraisingEventRegistrantionTotalResult)
    {
      $this->GetFundraisingEventRegistrantionTotalResult = $GetFundraisingEventRegistrantionTotalResult;
      return $this;
    }

}
