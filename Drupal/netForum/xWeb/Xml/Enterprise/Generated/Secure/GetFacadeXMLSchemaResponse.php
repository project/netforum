<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetFacadeXMLSchemaResponse
{

    /**
     * @var GetFacadeXMLSchemaResult $GetFacadeXMLSchemaResult
     */
    protected $GetFacadeXMLSchemaResult = null;

    /**
     * @param GetFacadeXMLSchemaResult $GetFacadeXMLSchemaResult
     */
    public function __construct($GetFacadeXMLSchemaResult)
    {
      $this->GetFacadeXMLSchemaResult = $GetFacadeXMLSchemaResult;
    }

    /**
     * @return GetFacadeXMLSchemaResult
     */
    public function getGetFacadeXMLSchemaResult()
    {
      return $this->GetFacadeXMLSchemaResult;
    }

    /**
     * @param GetFacadeXMLSchemaResult $GetFacadeXMLSchemaResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetFacadeXMLSchemaResponse
     */
    public function setGetFacadeXMLSchemaResult($GetFacadeXMLSchemaResult)
    {
      $this->GetFacadeXMLSchemaResult = $GetFacadeXMLSchemaResult;
      return $this;
    }

}
