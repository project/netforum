<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBIsUserInRoleResponse
{

    /**
     * @var boolean $WEBIsUserInRoleResult
     */
    protected $WEBIsUserInRoleResult = null;

    /**
     * @param boolean $WEBIsUserInRoleResult
     */
    public function __construct($WEBIsUserInRoleResult)
    {
      $this->WEBIsUserInRoleResult = $WEBIsUserInRoleResult;
    }

    /**
     * @return boolean
     */
    public function getWEBIsUserInRoleResult()
    {
      return $this->WEBIsUserInRoleResult;
    }

    /**
     * @param boolean $WEBIsUserInRoleResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBIsUserInRoleResponse
     */
    public function setWEBIsUserInRoleResult($WEBIsUserInRoleResult)
    {
      $this->WEBIsUserInRoleResult = $WEBIsUserInRoleResult;
      return $this;
    }

}
