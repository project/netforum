<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartMembershipAddMembershipResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartMembershipAddMembershipResult
     */
    protected $WEBCentralizedShoppingCartMembershipAddMembershipResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartMembershipAddMembershipResult
     */
    public function __construct($WEBCentralizedShoppingCartMembershipAddMembershipResult)
    {
      $this->WEBCentralizedShoppingCartMembershipAddMembershipResult = $WEBCentralizedShoppingCartMembershipAddMembershipResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartMembershipAddMembershipResult()
    {
      return $this->WEBCentralizedShoppingCartMembershipAddMembershipResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartMembershipAddMembershipResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartMembershipAddMembershipResponse
     */
    public function setWEBCentralizedShoppingCartMembershipAddMembershipResult($WEBCentralizedShoppingCartMembershipAddMembershipResult)
    {
      $this->WEBCentralizedShoppingCartMembershipAddMembershipResult = $WEBCentralizedShoppingCartMembershipAddMembershipResult;
      return $this;
    }

}
