<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserLoginAndRememberMeResponse
{

    /**
     * @var WebUserType $WEBWebUserLoginAndRememberMeResult
     */
    protected $WEBWebUserLoginAndRememberMeResult = null;

    /**
     * @param WebUserType $WEBWebUserLoginAndRememberMeResult
     */
    public function __construct($WEBWebUserLoginAndRememberMeResult)
    {
      $this->WEBWebUserLoginAndRememberMeResult = $WEBWebUserLoginAndRememberMeResult;
    }

    /**
     * @return WebUserType
     */
    public function getWEBWebUserLoginAndRememberMeResult()
    {
      return $this->WEBWebUserLoginAndRememberMeResult;
    }

    /**
     * @param WebUserType $WEBWebUserLoginAndRememberMeResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserLoginAndRememberMeResponse
     */
    public function setWEBWebUserLoginAndRememberMeResult($WEBWebUserLoginAndRememberMeResult)
    {
      $this->WEBWebUserLoginAndRememberMeResult = $WEBWebUserLoginAndRememberMeResult;
      return $this;
    }

}
