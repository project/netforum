<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetProductLineItem
{

    /**
     * @var guid $ProductKey
     */
    protected $ProductKey = null;

    /**
     * @var guid $CustomerKey
     */
    protected $CustomerKey = null;

    /**
     * @var guid $Customer_X_Address_Key
     */
    protected $Customer_X_Address_Key = null;

    /**
     * @param guid $ProductKey
     * @param guid $CustomerKey
     * @param guid $Customer_X_Address_Key
     */
    public function __construct($ProductKey, $CustomerKey, $Customer_X_Address_Key)
    {
      $this->ProductKey = $ProductKey;
      $this->CustomerKey = $CustomerKey;
      $this->Customer_X_Address_Key = $Customer_X_Address_Key;
    }

    /**
     * @return guid
     */
    public function getProductKey()
    {
      return $this->ProductKey;
    }

    /**
     * @param guid $ProductKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetProductLineItem
     */
    public function setProductKey($ProductKey)
    {
      $this->ProductKey = $ProductKey;
      return $this;
    }

    /**
     * @return guid
     */
    public function getCustomerKey()
    {
      return $this->CustomerKey;
    }

    /**
     * @param guid $CustomerKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetProductLineItem
     */
    public function setCustomerKey($CustomerKey)
    {
      $this->CustomerKey = $CustomerKey;
      return $this;
    }

    /**
     * @return guid
     */
    public function getCustomer_X_Address_Key()
    {
      return $this->Customer_X_Address_Key;
    }

    /**
     * @param guid $Customer_X_Address_Key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetProductLineItem
     */
    public function setCustomer_X_Address_Key($Customer_X_Address_Key)
    {
      $this->Customer_X_Address_Key = $Customer_X_Address_Key;
      return $this;
    }

}
