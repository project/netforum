<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartEventRegistrantGetResponse
{

    /**
     * @var EventsRegistrantType $WEBCentralizedShoppingCartEventRegistrantGetResult
     */
    protected $WEBCentralizedShoppingCartEventRegistrantGetResult = null;

    /**
     * @param EventsRegistrantType $WEBCentralizedShoppingCartEventRegistrantGetResult
     */
    public function __construct($WEBCentralizedShoppingCartEventRegistrantGetResult)
    {
      $this->WEBCentralizedShoppingCartEventRegistrantGetResult = $WEBCentralizedShoppingCartEventRegistrantGetResult;
    }

    /**
     * @return EventsRegistrantType
     */
    public function getWEBCentralizedShoppingCartEventRegistrantGetResult()
    {
      return $this->WEBCentralizedShoppingCartEventRegistrantGetResult;
    }

    /**
     * @param EventsRegistrantType $WEBCentralizedShoppingCartEventRegistrantGetResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartEventRegistrantGetResponse
     */
    public function setWEBCentralizedShoppingCartEventRegistrantGetResult($WEBCentralizedShoppingCartEventRegistrantGetResult)
    {
      $this->WEBCentralizedShoppingCartEventRegistrantGetResult = $WEBCentralizedShoppingCartEventRegistrantGetResult;
      return $this;
    }

}
