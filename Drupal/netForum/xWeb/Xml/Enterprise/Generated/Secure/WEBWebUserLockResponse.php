<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserLockResponse
{

    /**
     * @var boolean $WEBWebUserLockResult
     */
    protected $WEBWebUserLockResult = null;

    /**
     * @param boolean $WEBWebUserLockResult
     */
    public function __construct($WEBWebUserLockResult)
    {
      $this->WEBWebUserLockResult = $WEBWebUserLockResult;
    }

    /**
     * @return boolean
     */
    public function getWEBWebUserLockResult()
    {
      return $this->WEBWebUserLockResult;
    }

    /**
     * @param boolean $WEBWebUserLockResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserLockResponse
     */
    public function setWEBWebUserLockResult($WEBWebUserLockResult)
    {
      $this->WEBWebUserLockResult = $WEBWebUserLockResult;
      return $this;
    }

}
