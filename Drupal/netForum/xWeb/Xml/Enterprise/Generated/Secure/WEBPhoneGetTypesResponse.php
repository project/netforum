<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBPhoneGetTypesResponse
{

    /**
     * @var WEBPhoneGetTypesResult $WEBPhoneGetTypesResult
     */
    protected $WEBPhoneGetTypesResult = null;

    /**
     * @param WEBPhoneGetTypesResult $WEBPhoneGetTypesResult
     */
    public function __construct($WEBPhoneGetTypesResult)
    {
      $this->WEBPhoneGetTypesResult = $WEBPhoneGetTypesResult;
    }

    /**
     * @return WEBPhoneGetTypesResult
     */
    public function getWEBPhoneGetTypesResult()
    {
      return $this->WEBPhoneGetTypesResult;
    }

    /**
     * @param WEBPhoneGetTypesResult $WEBPhoneGetTypesResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBPhoneGetTypesResponse
     */
    public function setWEBPhoneGetTypesResult($WEBPhoneGetTypesResult)
    {
      $this->WEBPhoneGetTypesResult = $WEBPhoneGetTypesResult;
      return $this;
    }

}
