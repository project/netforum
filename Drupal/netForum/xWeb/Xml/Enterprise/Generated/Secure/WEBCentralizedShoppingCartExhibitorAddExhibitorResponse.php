<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartExhibitorAddExhibitorResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartExhibitorAddExhibitorResult
     */
    protected $WEBCentralizedShoppingCartExhibitorAddExhibitorResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartExhibitorAddExhibitorResult
     */
    public function __construct($WEBCentralizedShoppingCartExhibitorAddExhibitorResult)
    {
      $this->WEBCentralizedShoppingCartExhibitorAddExhibitorResult = $WEBCentralizedShoppingCartExhibitorAddExhibitorResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartExhibitorAddExhibitorResult()
    {
      return $this->WEBCentralizedShoppingCartExhibitorAddExhibitorResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartExhibitorAddExhibitorResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartExhibitorAddExhibitorResponse
     */
    public function setWEBCentralizedShoppingCartExhibitorAddExhibitorResult($WEBCentralizedShoppingCartExhibitorAddExhibitorResult)
    {
      $this->WEBCentralizedShoppingCartExhibitorAddExhibitorResult = $WEBCentralizedShoppingCartExhibitorAddExhibitorResult;
      return $this;
    }

}
