<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetAccreditationFeeListResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetAccreditationFeeListResult $WEBCentralizedShoppingCartGetAccreditationFeeListResult
     */
    protected $WEBCentralizedShoppingCartGetAccreditationFeeListResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetAccreditationFeeListResult $WEBCentralizedShoppingCartGetAccreditationFeeListResult
     */
    public function __construct($WEBCentralizedShoppingCartGetAccreditationFeeListResult)
    {
      $this->WEBCentralizedShoppingCartGetAccreditationFeeListResult = $WEBCentralizedShoppingCartGetAccreditationFeeListResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetAccreditationFeeListResult
     */
    public function getWEBCentralizedShoppingCartGetAccreditationFeeListResult()
    {
      return $this->WEBCentralizedShoppingCartGetAccreditationFeeListResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetAccreditationFeeListResult $WEBCentralizedShoppingCartGetAccreditationFeeListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetAccreditationFeeListResponse
     */
    public function setWEBCentralizedShoppingCartGetAccreditationFeeListResult($WEBCentralizedShoppingCartGetAccreditationFeeListResult)
    {
      $this->WEBCentralizedShoppingCartGetAccreditationFeeListResult = $WEBCentralizedShoppingCartGetAccreditationFeeListResult;
      return $this;
    }

}
