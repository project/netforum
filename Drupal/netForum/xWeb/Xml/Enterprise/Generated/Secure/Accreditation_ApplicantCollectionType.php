<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class Accreditation_ApplicantCollectionType
{

    /**
     * @var AccreditationAreaType[] $AccreditationArea
     */
    protected $AccreditationArea = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AccreditationAreaType[]
     */
    public function getAccreditationArea()
    {
      return $this->AccreditationArea;
    }

    /**
     * @param AccreditationAreaType[] $AccreditationArea
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\Accreditation_ApplicantCollectionType
     */
    public function setAccreditationArea(array $AccreditationArea = null)
    {
      $this->AccreditationArea = $AccreditationArea;
      return $this;
    }

}
