<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBMemberDirectorySearchResponse
{

    /**
     * @var WEBMemberDirectorySearchResult $WEBMemberDirectorySearchResult
     */
    protected $WEBMemberDirectorySearchResult = null;

    /**
     * @param WEBMemberDirectorySearchResult $WEBMemberDirectorySearchResult
     */
    public function __construct($WEBMemberDirectorySearchResult)
    {
      $this->WEBMemberDirectorySearchResult = $WEBMemberDirectorySearchResult;
    }

    /**
     * @return WEBMemberDirectorySearchResult
     */
    public function getWEBMemberDirectorySearchResult()
    {
      return $this->WEBMemberDirectorySearchResult;
    }

    /**
     * @param WEBMemberDirectorySearchResult $WEBMemberDirectorySearchResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBMemberDirectorySearchResponse
     */
    public function setWEBMemberDirectorySearchResult($WEBMemberDirectorySearchResult)
    {
      $this->WEBMemberDirectorySearchResult = $WEBMemberDirectorySearchResult;
      return $this;
    }

}
