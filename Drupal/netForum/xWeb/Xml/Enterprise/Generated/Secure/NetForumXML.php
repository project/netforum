<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

use Drupal\netForum\xWeb\Xml\netForumSoapClient;


/**
 * Avectra Xml Web Services with Authentication Token release: http://www.avectra.com/2005/ -   For additional information or for questions concerning xWeb please navigate to netForum xWeb Online help at http://wiki.avectra.com/index.php?title=XWeb.  Please note: all xWeb message exchanges are recommended to occur over SSL.
 */
class NetForumXML extends netForumSoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'WEBCentralizedShoppingCartMebmershipRemoveMembership' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMebmershipRemoveMembership',
      'WEBCentralizedShoppingCartMebmershipRemoveMembershipResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMebmershipRemoveMembershipResponse',
      'AuthorizationToken' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AuthorizationToken',
      'WEBCentralizedShoppingCartMembershipOpenInvoiceGetList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipOpenInvoiceGetList',
      'WEBCentralizedShoppingCartMembershipOpenInvoiceGetListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipOpenInvoiceGetListResponse',
      'WEBCentralizedShoppingCartMembershipOpenInvoiceGetListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipOpenInvoiceGetListResult',
      'WEBCentralizedShoppingCartMembershipOpenInvoiceGet' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipOpenInvoiceGet',
      'WEBCentralizedShoppingCartMembershipOpenInvoiceGetResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipOpenInvoiceGetResponse',
      'WEBCentralizedShoppingCartMembershipOpenInvoiceAdd' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipOpenInvoiceAdd',
      'WEBCentralizedShoppingCartMembershipOpenInvoiceAddResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipOpenInvoiceAddResponse',
      'WEBCentralizedShoppingCartExhibitorGetExhibitList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartExhibitorGetExhibitList',
      'WEBCentralizedShoppingCartExhibitorGetExhibitListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartExhibitorGetExhibitListResponse',
      'WEBCentralizedShoppingCartExhibitorGetExhibitListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartExhibitorGetExhibitListResult',
      'WEBCentralizedShoppingCartExhibitorGetNew' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartExhibitorGetNew',
      'WEBCentralizedShoppingCartExhibitorGetNewResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartExhibitorGetNewResponse',
      'WEBCentralizedShoppingCartExhibitorGet' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartExhibitorGet',
      'WEBCentralizedShoppingCartExhibitorGetResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartExhibitorGetResponse',
      'WEBCentralizedShoppingCartExhibitorGetBoothTypeList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartExhibitorGetBoothTypeList',
      'WEBCentralizedShoppingCartExhibitorGetBoothTypeListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartExhibitorGetBoothTypeListResponse',
      'WEBCentralizedShoppingCartExhibitorGetBoothTypeListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartExhibitorGetBoothTypeListResult',
      'WEBCentralizedShoppingCartExhibitorGetBoothCategoryList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartExhibitorGetBoothCategoryList',
      'WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResponse',
      'WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResult',
      'WEBCentralizedShoppingCartGetBoothList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetBoothList',
      'WEBCentralizedShoppingCartGetBoothListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetBoothListResponse',
      'WEBCentralizedShoppingCartGetBoothListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetBoothListResult',
      'WEBCentralizedShoppingCartExhibitorSetLineItems' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartExhibitorSetLineItems',
      'Fee' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Fee',
      'WEBCentralizedShoppingCartExhibitorSetLineItemsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartExhibitorSetLineItemsResponse',
      'WEBCentralizedShoppingCartExhibitorSetLineItemsWithCart' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartExhibitorSetLineItemsWithCart',
      'WEBCentralizedShoppingCartExhibitorSetLineItemsWithCartResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartExhibitorSetLineItemsWithCartResponse',
      'WEBCentralizedShoppingCartExhibitorAddExhibitor' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartExhibitorAddExhibitor',
      'WEBCentralizedShoppingCartExhibitorAddExhibitorResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartExhibitorAddExhibitorResponse',
      'WEBCentralizedShoppingCartGetShippingOptions' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetShippingOptions',
      'WEBCentralizedShoppingCartGetShippingOptionsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetShippingOptionsResponse',
      'WEBCentralizedShoppingCartGetShippingOptionsResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetShippingOptionsResult',
      'WEBCentralizedShoppingCartAddShippingItem' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartAddShippingItem',
      'WEBCentralizedShoppingCartAddShippingItemResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartAddShippingItemResponse',
      'WEBCentralizedShoppingCartGetPaymentOptions' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetPaymentOptions',
      'WEBCentralizedShoppingCartGetPaymentOptionsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetPaymentOptionsResponse',
      'WEBCentralizedShoppingCartGetPaymentOptionsResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetPaymentOptionsResult',
      'WEBCentralizedShoppingCartGetInstallmentFrequencyOptions' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetInstallmentFrequencyOptions',
      'WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResponse',
      'WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResult',
      'WEBCentralizedShoppingCartGetInstallmentTermsOptions' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetInstallmentTermsOptions',
      'WEBCentralizedShoppingCartGetInstallmentTermsOptionsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetInstallmentTermsOptionsResponse',
      'WEBCentralizedShoppingCartGetInstallmentTermsOptionsResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetInstallmentTermsOptionsResult',
      'WEBCentralizedShoppingCartGetProductLineItem' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductLineItem',
      'WEBCentralizedShoppingCartGetProductLineItemResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductLineItemResponse',
      'WEBCentralizedShoppingCartGetProductLineItemWithCart' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductLineItemWithCart',
      'WEBCentralizedShoppingCartGetProductLineItemWithCartResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductLineItemWithCartResponse',
      'WEBCentralizedShoppingCartLoadLineItem' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartLoadLineItem',
      'WEBCentralizedShoppingCartLoadLineItemResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartLoadLineItemResponse',
      'WEBCentralizedShoppingCartAddLineItem' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartAddLineItem',
      'WEBCentralizedShoppingCartAddLineItemResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartAddLineItemResponse',
      'WEBCentralizedShoppingCartRemoveLineItem' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartRemoveLineItem',
      'WEBCentralizedShoppingCartRemoveLineItemResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartRemoveLineItemResponse',
      'WEBCentralizedShoppingCartRemoveAllLineItems' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartRemoveAllLineItems',
      'WEBCentralizedShoppingCartRemoveAllLineItemsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartRemoveAllLineItemsResponse',
      'WEBCentralizedShoppingCartGetAccreditationList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetAccreditationList',
      'WEBCentralizedShoppingCartGetAccreditationListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetAccreditationListResponse',
      'WEBCentralizedShoppingCartGetAccreditationListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetAccreditationListResult',
      'WEBCentralizedShoppingCartGetAccreditationListByCustomer' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetAccreditationListByCustomer',
      'WEBCentralizedShoppingCartGetAccreditationListByCustomerResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetAccreditationListByCustomerResponse',
      'WEBCentralizedShoppingCartGetAccreditationListByCustomerResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetAccreditationListByCustomerResult',
      'WEBCentralizedShoppingCartGetAccreditationFeeList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetAccreditationFeeList',
      'WEBCentralizedShoppingCartGetAccreditationFeeListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetAccreditationFeeListResponse',
      'WEBCentralizedShoppingCartGetAccreditationFeeListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetAccreditationFeeListResult',
      'WEBCentralizedShoppingCartGetAccreditationTypeList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetAccreditationTypeList',
      'WEBCentralizedShoppingCartGetAccreditationTypeListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetAccreditationTypeListResponse',
      'WEBCentralizedShoppingCartGetAccreditationTypeListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetAccreditationTypeListResult',
      'WEBCentralizedShoppingCartGetAccreditationTypeStatusList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetAccreditationTypeStatusList',
      'WEBCentralizedShoppingCartGetAccreditationTypeStatusListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetAccreditationTypeStatusListResponse',
      'WEBCentralizedShoppingCartGetAccreditationTypeStatusListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetAccreditationTypeStatusListResult',
      'WEBCentralizedShoppingCartAccreditationGet' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartAccreditationGet',
      'WEBCentralizedShoppingCartAccreditationGetResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartAccreditationGetResponse',
      'WEBCentralizedShoppingCartAccreditationGetNew' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartAccreditationGetNew',
      'WEBCentralizedShoppingCartAccreditationGetNewResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartAccreditationGetNewResponse',
      'WEBCentralizedShoppingCartAccreditationSetLineItems' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartAccreditationSetLineItems',
      'Fees' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Fees',
      'WEBCentralizedShoppingCartAccreditationSetLineItemsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartAccreditationSetLineItemsResponse',
      'WEBCentralizedShoppingCartAccreditationAddAccreditation' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartAccreditationAddAccreditation',
      'WEBCentralizedShoppingCartAccreditationAddAccreditationResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartAccreditationAddAccreditationResponse',
      'GetDateTime' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetDateTime',
      'GetDateTimeResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetDateTimeResponse',
      'MetaDataGetForm' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\MetaDataGetForm',
      'MetaDataGetFormResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\MetaDataGetFormResponse',
      'AVForm' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AVForm',
      'ArrayOfAVFormControl' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ArrayOfAVFormControl',
      'AVFormControl' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AVFormControl',
      'DesignedFormControl' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\DesignedFormControl',
      'ArrayOfAvailableValue' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ArrayOfAvailableValue',
      'AvailableValue' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AvailableValue',
      'ArrayOfString' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ArrayOfString',
      'ArrayOfAVFormAction' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ArrayOfAVFormAction',
      'AVFormAction' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AVFormAction',
      'AVFormDataInterface' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AVFormDataInterface',
      'MetaDataGetFormForFacadeObject' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\MetaDataGetFormForFacadeObject',
      'oNode' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\oNode',
      'MetaDataGetFormForFacadeObjectResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\MetaDataGetFormForFacadeObjectResponse',
      'MetaDataGetFormControlForFacadeObject' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\MetaDataGetFormControlForFacadeObject',
      'MetaDataGetFormControlForFacadeObjectResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\MetaDataGetFormControlForFacadeObjectResponse',
      'MetaDataGetFormForIndividual' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\MetaDataGetFormForIndividual',
      'MetaDataGetFormForIndividualResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\MetaDataGetFormForIndividualResponse',
      'MetaDataGetWizard' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\MetaDataGetWizard',
      'MetaDataGetWizardResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\MetaDataGetWizardResponse',
      'AVWizard' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AVWizard',
      'ArrayOfAVWizardForm' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ArrayOfAVWizardForm',
      'AVWizardForm' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AVWizardForm',
      'MetaDataGetWizardForm' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\MetaDataGetWizardForm',
      'MetaDataGetWizardFormResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\MetaDataGetWizardFormResponse',
      'WEBColumnGetColumnValuesByColumnName' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBColumnGetColumnValuesByColumnName',
      'WEBColumnGetColumnValuesByColumnNameResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBColumnGetColumnValuesByColumnNameResponse',
      'GetPromotionalMailingTypes' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetPromotionalMailingTypes',
      'GetPromotionalMailingTypesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetPromotionalMailingTypesResponse',
      'ArrayOfMailingList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ArrayOfMailingList',
      'MailingList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\MailingList',
      'GetSubscriptionMailingTypes' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetSubscriptionMailingTypes',
      'GetSubscriptionMailingTypesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetSubscriptionMailingTypesResponse',
      'GetCustomerCommunicationPreference' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetCustomerCommunicationPreference',
      'GetCustomerCommunicationPreferenceResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetCustomerCommunicationPreferenceResponse',
      'MailingListSetting' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\MailingListSetting',
      'GetAllCustomerCommunicationPreferences' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetAllCustomerCommunicationPreferences',
      'GetAllCustomerCommunicationPreferencesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetAllCustomerCommunicationPreferencesResponse',
      'ArrayOfMailingListSetting' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ArrayOfMailingListSetting',
      'SetCustomerCommunicationPreferences' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\SetCustomerCommunicationPreferences',
      'SetCustomerCommunicationPreferencesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\SetCustomerCommunicationPreferencesResponse',
      'GetFundraisingEventRegistrantionTotal' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetFundraisingEventRegistrantionTotal',
      'GetFundraisingEventRegistrantionTotalResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetFundraisingEventRegistrantionTotalResponse',
      'EventUserFundraisingDetail' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventUserFundraisingDetail',
      'GetFundraisingEventDonations' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetFundraisingEventDonations',
      'GetFundraisingEventDonationsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetFundraisingEventDonationsResponse',
      'ArrayOfDonation' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ArrayOfDonation',
      'Donation' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Donation',
      'GetFundraisingEventDonationUrl' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetFundraisingEventDonationUrl',
      'GetFundraisingEventDonationUrlResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetFundraisingEventDonationUrlResponse',
      'WEBCommitteeGetCommitteeListByName' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetCommitteeListByName',
      'WEBCommitteeGetCommitteeListByNameResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetCommitteeListByNameResponse',
      'WEBCommitteeGetCommitteeListByNameResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetCommitteeListByNameResult',
      'WEBCommitteeGetCommitteesByCustomer' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetCommitteesByCustomer',
      'WEBCommitteeGetCommitteesByCustomerResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetCommitteesByCustomerResponse',
      'WEBCommitteeGetCommitteesByCustomerResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetCommitteesByCustomerResult',
      'WEBCommitteeGetSubCommitteeListByCommittee' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetSubCommitteeListByCommittee',
      'WEBCommitteeGetSubCommitteeListByCommitteeResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetSubCommitteeListByCommitteeResponse',
      'WEBCommitteeGetSubCommitteeListByCommitteeResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetSubCommitteeListByCommitteeResult',
      'WEBCommitteeGetMembers' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetMembers',
      'WEBCommitteeGetMembersResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetMembersResponse',
      'WEBCommitteeGetMembersResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetMembersResult',
      'WEBCommitteeGetDocuments' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetDocuments',
      'WEBCommitteeGetDocumentsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetDocumentsResponse',
      'WEBCommitteeGetDocumentsResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetDocumentsResult',
      'WEBCommitteeGetCommitteeByKey' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetCommitteeByKey',
      'WEBCommitteeGetCommitteeByKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetCommitteeByKeyResponse',
      'WEBCommitteeGetCommitteeByKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetCommitteeByKeyResult',
      'WEBCommitteeGet' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGet',
      'WEBCommitteeGetResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetResponse',
      'WEBCommitteeNominationInsert' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeNominationInsert',
      'WEBCommitteeNominationInsertResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeNominationInsertResponse',
      'WEBCommitteeGetPositionList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetPositionList',
      'WEBCommitteeGetPositionListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetPositionListResponse',
      'WEBCommitteeGetPositionListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetPositionListResult',
      'WEBMemberDirectorySearch' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBMemberDirectorySearch',
      'WEBMemberDirectorySearchResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBMemberDirectorySearchResponse',
      'WEBMemberDirectorySearchResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBMemberDirectorySearchResult',
      'WEBMemberDirectoryOrganizationSearch' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBMemberDirectoryOrganizationSearch',
      'WEBMemberDirectoryOrganizationSearchResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBMemberDirectoryOrganizationSearchResponse',
      'WEBMemberDirectoryOrganizationSearchResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBMemberDirectoryOrganizationSearchResult',
      'WEBChaptersGetChapterList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBChaptersGetChapterList',
      'WEBChaptersGetChapterListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBChaptersGetChapterListResponse',
      'WEBChaptersGetChapterListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBChaptersGetChapterListResult',
      'WEBChaptersGetChapterListByName' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBChaptersGetChapterListByName',
      'WEBChaptersGetChapterListByNameResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBChaptersGetChapterListByNameResponse',
      'WEBChaptersGetChapterListByNameResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBChaptersGetChapterListByNameResult',
      'WEBChaptersGetChapterMembershipRoster' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBChaptersGetChapterMembershipRoster',
      'WEBChaptersGetChapterMembershipRosterResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBChaptersGetChapterMembershipRosterResponse',
      'WEBChaptersGetChapterMembershipRosterResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBChaptersGetChapterMembershipRosterResult',
      'WEBChaptersGetChapterOfficers' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBChaptersGetChapterOfficers',
      'WEBChaptersGetChapterOfficersResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBChaptersGetChapterOfficersResponse',
      'WEBChaptersGetChapterOfficersResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBChaptersGetChapterOfficersResult',
      'WEBChaptersGetChapterByKey' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBChaptersGetChapterByKey',
      'WEBChaptersGetChapterByKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBChaptersGetChapterByKeyResponse',
      'WEBChaptersGetChapterByKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBChaptersGetChapterByKeyResult',
      'WEBChapterGet' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBChapterGet',
      'WEBChapterGetResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBChapterGetResponse',
      'WEBCentralizedShoppingCartStoreForeWeb' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartStoreForeWeb',
      'WEBCentralizedShoppingCartStoreForeWebResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartStoreForeWebResponse',
      'WEBCentralizedShoppingCartGetPendingOrder' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetPendingOrder',
      'WEBCentralizedShoppingCartGetPendingOrderResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetPendingOrderResponse',
      'WEBCentralizedShoppingCartGetNew' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetNew',
      'WEBCentralizedShoppingCartGetNewResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetNewResponse',
      'WEBCentralizedShoppingCartGet' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGet',
      'WEBCentralizedShoppingCartGetResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetResponse',
      'WEBCentralizedShoppingCartGetWithLineItem' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetWithLineItem',
      'WEBCentralizedShoppingCartGetWithLineItemResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetWithLineItemResponse',
      'WEBCentralizedShoppingCartRefresh' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartRefresh',
      'WEBCentralizedShoppingCartRefreshResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartRefreshResponse',
      'WEBCentralizedShoppingCartInsert' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartInsert',
      'WEBCentralizedShoppingCartInsertResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartInsertResponse',
      'WEBCentralizedShoppingCartApplySourceCode' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartApplySourceCode',
      'WEBCentralizedShoppingCartApplySourceCodeResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartApplySourceCodeResponse',
      'WEBCentralizedShoppingCartClearSourceCode' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartClearSourceCode',
      'WEBCentralizedShoppingCartClearSourceCodeResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartClearSourceCodeResponse',
      'WEBCentralizedShoppingCartApplyDiscountCode' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartApplyDiscountCode',
      'WEBCentralizedShoppingCartApplyDiscountCodeResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartApplyDiscountCodeResponse',
      'WEBCentralizedShoppingCartGetProductTypeList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductTypeList',
      'WEBCentralizedShoppingCartGetProductTypeListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductTypeListResponse',
      'WEBCentralizedShoppingCartGetProductTypeListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductTypeListResult',
      'WEBCentralizedShoppingCartGetProductCategoryList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductCategoryList',
      'WEBCentralizedShoppingCartGetProductCategoryListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductCategoryListResponse',
      'WEBCentralizedShoppingCartGetProductCategoryListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductCategoryListResult',
      'WEBCentralizedShoppingCartGetProductList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductList',
      'WEBCentralizedShoppingCartGetProductListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductListResponse',
      'WEBCentralizedShoppingCartGetProductListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductListResult',
      'WEBCentralizedShoppingCartGetProductListByName' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductListByName',
      'WEBCentralizedShoppingCartGetProductListByNameResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductListByNameResponse',
      'WEBCentralizedShoppingCartGetProductListByNameResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductListByNameResult',
      'WEBCentralizedShoppingCartGetProductByKey' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductByKey',
      'WEBCentralizedShoppingCartGetProductByKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductByKeyResponse',
      'WEBCentralizedShoppingCartGetProductByKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductByKeyResult',
      'WEBCentralizedShoppingCartGetProductListKeys' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductListKeys',
      'WEBCentralizedShoppingCartGetProductListKeysResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductListKeysResponse',
      'WEBCentralizedShoppingCartGetProductListKeysResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductListKeysResult',
      'WEBCentralizedShoppingCartGetProductComplements' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductComplements',
      'WEBCentralizedShoppingCartGetProductComplementsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductComplementsResponse',
      'WEBCentralizedShoppingCartGetProductComplementsResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductComplementsResult',
      'WEBCentralizedShoppingCartGetProductComplementListByProductKey' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductComplementListByProductKey',
      'WEBCentralizedShoppingCartGetProductComplementListByProductKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductComplementListByProductKeyResponse',
      'WEBCentralizedShoppingCartGetProductComplementListByProductKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductComplementListByProductKeyResult',
      'WEBCentralizedShoppingCartGetProductSubstitutes' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductSubstitutes',
      'WEBCentralizedShoppingCartGetProductSubstitutesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductSubstitutesResponse',
      'WEBCentralizedShoppingCartGetProductSubstitutesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductSubstitutesResult',
      'WEBCentralizedShoppingCartGetProductSubstituteListByProductKey' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductSubstituteListByProductKey',
      'WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResponse',
      'WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResult',
      'WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKey' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKey',
      'WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResponse',
      'WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResult',
      'WEBCentralizedShoppingCartGetMerchandiseList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetMerchandiseList',
      'WEBCentralizedShoppingCartGetMerchandiseListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetMerchandiseListResponse',
      'WEBCentralizedShoppingCartGetMerchandiseListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetMerchandiseListResult',
      'WEBCentralizedShoppingCartGetMerchandiseList_Ignore_PC' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetMerchandiseList_Ignore_PC',
      'WEBCentralizedShoppingCartGetMerchandiseList_Ignore_PCResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetMerchandiseList_Ignore_PCResponse',
      'WEBCentralizedShoppingCartGetMerchandiseList_Ignore_PCResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetMerchandiseList_Ignore_PCResult',
      'WEBCentralizedShoppingCartGetPublicationList_Ignore_PC' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetPublicationList_Ignore_PC',
      'WEBCentralizedShoppingCartGetPublicationList_Ignore_PCResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetPublicationList_Ignore_PCResponse',
      'WEBCentralizedShoppingCartGetPublicationList_Ignore_PCResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetPublicationList_Ignore_PCResult',
      'WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PC' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PC',
      'WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResponse',
      'WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResult',
      'WEBCentralizedShoppingCartGetEventList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventList',
      'WEBCentralizedShoppingCartGetEventListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventListResponse',
      'WEBCentralizedShoppingCartGetEventListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventListResult',
      'WEBCentralizedShoppingCartGetEventListByName' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventListByName',
      'WEBCentralizedShoppingCartGetEventListByNameResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventListByNameResponse',
      'WEBCentralizedShoppingCartGetEventListByNameResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventListByNameResult',
      'WEBCentralizedShoppingCartGetEventByKey' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventByKey',
      'WEBCentralizedShoppingCartGetEventByKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventByKeyResponse',
      'WEBCentralizedShoppingCartGetEventByKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventByKeyResult',
      'WEBCentralizedShoppingCartGetEventListKeys' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventListKeys',
      'WEBCentralizedShoppingCartGetEventListKeysResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventListKeysResponse',
      'WEBCentralizedShoppingCartGetEventListKeysResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventListKeysResult',
      'WEBCentralizedShoppingCartGetTrackListByEventKey' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetTrackListByEventKey',
      'WEBCentralizedShoppingCartGetTrackListByEventKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetTrackListByEventKeyResponse',
      'WEBCentralizedShoppingCartGetTrackListByEventKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetTrackListByEventKeyResult',
      'WEBCentralizedShoppingCartGetSessionListByEventKey' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetSessionListByEventKey',
      'WEBCentralizedShoppingCartGetSessionListByEventKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetSessionListByEventKeyResponse',
      'WEBCentralizedShoppingCartGetSessionListByEventKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetSessionListByEventKeyResult',
      'WEBCentralizedShoppingCartGetFacultyListByEventKey' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetFacultyListByEventKey',
      'WEBCentralizedShoppingCartGetFacultyListByEventKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetFacultyListByEventKeyResponse',
      'WEBCentralizedShoppingCartGetFacultyListByEventKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetFacultyListByEventKeyResult',
      'WEBCentralizedShoppingCartGetFacultyListBySessionKey' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetFacultyListBySessionKey',
      'WEBCentralizedShoppingCartGetFacultyListBySessionKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetFacultyListBySessionKeyResponse',
      'WEBCentralizedShoppingCartGetFacultyListBySessionKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetFacultyListBySessionKeyResult',
      'WEBCentralizedShoppingCartGetSponsorListByEventKey' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetSponsorListByEventKey',
      'WEBCentralizedShoppingCartGetSponsorListByEventKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetSponsorListByEventKeyResponse',
      'WEBCentralizedShoppingCartGetSponsorListByEventKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetSponsorListByEventKeyResult',
      'WEBCentralizedShoppingCartGetSponsorListBySessionKey' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetSponsorListBySessionKey',
      'WEBCentralizedShoppingCartGetSponsorListBySessionKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetSponsorListBySessionKeyResponse',
      'WEBCentralizedShoppingCartGetSponsorListBySessionKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetSponsorListBySessionKeyResult',
      'WEBCentralizedShoppingCartGetSessionListByTrackKey' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetSessionListByTrackKey',
      'WEBCentralizedShoppingCartGetSessionListByTrackKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetSessionListByTrackKeyResponse',
      'WEBCentralizedShoppingCartGetSessionListByTrackKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetSessionListByTrackKeyResult',
      'WEBCentralizedShoppingCartGetEventRegistrantTypeList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventRegistrantTypeList',
      'WEBCentralizedShoppingCartGetEventRegistrantTypeListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventRegistrantTypeListResponse',
      'WEBCentralizedShoppingCartGetEventRegistrantTypeListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventRegistrantTypeListResult',
      'WEBCentralizedShoppingCartGetEventRegistrantRoomTypeList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventRegistrantRoomTypeList',
      'WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResponse',
      'WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResult',
      'WEBCentralizedShoppingCartGetEventRegistrantTypeListByEvent' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventRegistrantTypeListByEvent',
      'WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResponse',
      'WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResult',
      'WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEvent' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEvent',
      'WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResponse',
      'WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResult',
      'WEBCentralizedShoppingCartGetEventRegistrantSourceCodeList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventRegistrantSourceCodeList',
      'WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResponse',
      'WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResult',
      'WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualList',
      'WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResponse',
      'WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResult',
      'WEBCentralizedShoppingCartEventRegistrantGet' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartEventRegistrantGet',
      'WEBCentralizedShoppingCartEventRegistrantGetResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartEventRegistrantGetResponse',
      'WEBCentralizedShoppingCartEventRegistrantGetNew' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartEventRegistrantGetNew',
      'WEBCentralizedShoppingCartEventRegistrantGetNewResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartEventRegistrantGetNewResponse',
      'WEBCentralizedShoppingCartEventRegistrantGroupGet' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartEventRegistrantGroupGet',
      'WEBCentralizedShoppingCartEventRegistrantGroupGetResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartEventRegistrantGroupGetResponse',
      'WEBCentralizedShoppingCartEventRegistrantGroupGetNew' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartEventRegistrantGroupGetNew',
      'WEBCentralizedShoppingCartEventRegistrantGroupGetNewResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartEventRegistrantGroupGetNewResponse',
      'WEBCentralizedShoppingCartGetEventFees' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventFees',
      'WEBCentralizedShoppingCartGetEventFeesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventFeesResponse',
      'WEBCentralizedShoppingCartGetEventFeesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventFeesResult',
      'WEBCentralizedShoppingCartGetEventTrackFees' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventTrackFees',
      'WEBCentralizedShoppingCartGetEventTrackFeesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventTrackFeesResponse',
      'WEBCentralizedShoppingCartGetEventTrackFeesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventTrackFeesResult',
      'WEBCentralizedShoppingCartGetEventSessionFees' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventSessionFees',
      'WEBCentralizedShoppingCartGetEventSessionFeesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventSessionFeesResponse',
      'WEBCentralizedShoppingCartGetEventSessionFeesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGetEventSessionFeesResult',
      'WEBCentralizedShoppingCartEventRegistrantSetLineItems' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartEventRegistrantSetLineItems',
      'WEBCentralizedShoppingCartEventRegistrantSetLineItemsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartEventRegistrantSetLineItemsResponse',
      'WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCart' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCart',
      'WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCartResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCartResponse',
      'WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistration' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistration',
      'WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistrationResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistrationResponse',
      'WEBCentralizedShoppingCartEventRegistrantRefresh' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartEventRegistrantRefresh',
      'WEBCentralizedShoppingCartEventRegistrantRefreshResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartEventRegistrantRefreshResponse',
      'WEBCentralizedShoppingCartAddEventRegistrant' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartAddEventRegistrant',
      'WEBCentralizedShoppingCartAddEventRegistrantResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartAddEventRegistrantResponse',
      'WEBCentralizedShoppingCartRemoveEventRegistrant' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartRemoveEventRegistrant',
      'WEBCentralizedShoppingCartRemoveEventRegistrantResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartRemoveEventRegistrantResponse',
      'WEBCentralizedShoppingCartEventRegistrantGroupRefresh' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartEventRegistrantGroupRefresh',
      'WEBCentralizedShoppingCartEventRegistrantGroupRefreshResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartEventRegistrantGroupRefreshResponse',
      'WEBCentralizedShoppingCartAddEventRegistrantToGroup' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartAddEventRegistrantToGroup',
      'WEBCentralizedShoppingCartAddEventRegistrantToGroupResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartAddEventRegistrantToGroupResponse',
      'WEBCentralizedShoppingCartRemoveEventRegistrantFromGroup' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartRemoveEventRegistrantFromGroup',
      'WEBCentralizedShoppingCartRemoveEventRegistrantFromGroupResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartRemoveEventRegistrantFromGroupResponse',
      'WEBCentralizedShoppingCartAddEventRegistrantGroup' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartAddEventRegistrantGroup',
      'WEBCentralizedShoppingCartAddEventRegistrantGroupResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartAddEventRegistrantGroupResponse',
      'WEBCentralizedShoppingCartRemoveEventRegistrantGroup' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartRemoveEventRegistrantGroup',
      'WEBCentralizedShoppingCartRemoveEventRegistrantGroupResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartRemoveEventRegistrantGroupResponse',
      'WEBCentralizedShoppingCartGiftGetGiftProductList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftGetGiftProductList',
      'WEBCentralizedShoppingCartGiftGetGiftProductListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftGetGiftProductListResponse',
      'WEBCentralizedShoppingCartGiftGetGiftProductListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftGetGiftProductListResult',
      'WEBCentralizedShoppingCartGiftGetGiftProductListByName' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftGetGiftProductListByName',
      'WEBCentralizedShoppingCartGiftGetGiftProductListByNameResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftGetGiftProductListByNameResponse',
      'WEBCentralizedShoppingCartGiftGetGiftProductListByNameResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftGetGiftProductListByNameResult',
      'WEBCentralizedShoppingCartGiftGetGiftProductByKey' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftGetGiftProductByKey',
      'WEBCentralizedShoppingCartGiftGetGiftProductByKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftGetGiftProductByKeyResponse',
      'WEBCentralizedShoppingCartGiftGetGiftProductByKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftGetGiftProductByKeyResult',
      'WEBCentralizedShoppingCartGiftGetGiftProductListKeys' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftGetGiftProductListKeys',
      'WEBCentralizedShoppingCartGiftGetGiftProductListKeysResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftGetGiftProductListKeysResponse',
      'WEBCentralizedShoppingCartGiftGetGiftProductListKeysResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftGetGiftProductListKeysResult',
      'WEBCentralizedShoppingCartGiftGetGiftTypeList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftGetGiftTypeList',
      'WEBCentralizedShoppingCartGiftGetGiftTypeListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftGetGiftTypeListResponse',
      'WEBCentralizedShoppingCartGiftGetGiftTypeListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftGetGiftTypeListResult',
      'WEBCentralizedShoppingCartGiftGetPremiumProductsListByGift' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftGetPremiumProductsListByGift',
      'WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResponse',
      'WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResult',
      'WEBCentralizedShoppingCartGiftFundraisingGet' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftFundraisingGet',
      'WEBCentralizedShoppingCartGiftFundraisingGetResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftFundraisingGetResponse',
      'WEBCentralizedShoppingCartGiftFundraisingGetNew' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftFundraisingGetNew',
      'WEBCentralizedShoppingCartGiftFundraisingGetNewResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftFundraisingGetNewResponse',
      'WEBCentralizedShoppingCartGiftFundraisingSetLineItems' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftFundraisingSetLineItems',
      'WEBCentralizedShoppingCartGiftFundraisingSetLineItemsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftFundraisingSetLineItemsResponse',
      'WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCart' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCart',
      'WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCartResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCartResponse',
      'WEBCentralizedShoppingCartGiftFundraisingRefresh' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftFundraisingRefresh',
      'WEBCentralizedShoppingCartGiftFundraisingRefreshResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftFundraisingRefreshResponse',
      'WEBCentralizedShoppingCartGiftAddGiftFundraising' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftAddGiftFundraising',
      'WEBCentralizedShoppingCartGiftAddGiftFundraisingResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftAddGiftFundraisingResponse',
      'WEBCentralizedShoppingCartGiftRemoveFundraisingGift' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftRemoveFundraisingGift',
      'WEBCentralizedShoppingCartGiftRemoveFundraisingGiftResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartGiftRemoveFundraisingGiftResponse',
      'WEBCentralizedShoppingCartMembershipGetPackageList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageList',
      'WEBCentralizedShoppingCartMembershipGetPackageListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageListResponse',
      'WEBCentralizedShoppingCartMembershipGetPackageListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageListResult',
      'WEBCentralizedShoppingCartMembershipGetRenewalPackageList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetRenewalPackageList',
      'WEBCentralizedShoppingCartMembershipGetRenewalPackageListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetRenewalPackageListResponse',
      'WEBCentralizedShoppingCartMembershipGetRenewalPackageListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetRenewalPackageListResult',
      'WEBCentralizedShoppingCartMembershipGetMembershipTypeList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetMembershipTypeList',
      'WEBCentralizedShoppingCartMembershipGetMembershipTypeListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetMembershipTypeListResponse',
      'WEBCentralizedShoppingCartMembershipGetMembershipTypeListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetMembershipTypeListResult',
      'WEBCentralizedShoppingCartMembershipGetPackageListByName' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageListByName',
      'WEBCentralizedShoppingCartMembershipGetPackageListByNameResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageListByNameResponse',
      'WEBCentralizedShoppingCartMembershipGetPackageListByNameResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageListByNameResult',
      'WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKey' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKey',
      'WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResponse',
      'WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResult',
      'WEBCentralizedShoppingCartMembershipGetPackageByKey' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageByKey',
      'WEBCentralizedShoppingCartMembershipGetPackageByKeyResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageByKeyResponse',
      'WEBCentralizedShoppingCartMembershipGetPackageByKeyResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageByKeyResult',
      'WEBCentralizedShoppingCartMembershipGetPackageListKeys' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageListKeys',
      'WEBCentralizedShoppingCartMembershipGetPackageListKeysResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageListKeysResponse',
      'WEBCentralizedShoppingCartMembershipGetPackageListKeysResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageListKeysResult',
      'WEBCentralizedShoppingCartMembershipGetPackageComponentList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageComponentList',
      'WEBCentralizedShoppingCartMembershipGetPackageComponentListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageComponentListResponse',
      'WEBCentralizedShoppingCartMembershipGetPackageComponentListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageComponentListResult',
      'WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObject' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObject',
      'WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResponse',
      'WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResult',
      'WEBCentralizedShoppingCartMembershipGet' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGet',
      'WEBCentralizedShoppingCartMembershipGetResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetResponse',
      'WEBCentralizedShoppingCartMembershipGetNew' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetNew',
      'WEBCentralizedShoppingCartMembershipGetNewResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipGetNewResponse',
      'WEBCentralizedShoppingCartMembesrhipSetLineItems' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembesrhipSetLineItems',
      'WEBCentralizedShoppingCartMembesrhipSetLineItemsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembesrhipSetLineItemsResponse',
      'WEBCentralizedShoppingCartMembershipSetLineItemsWithCart' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipSetLineItemsWithCart',
      'WEBCentralizedShoppingCartMembershipSetLineItemsWithCartResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipSetLineItemsWithCartResponse',
      'WEBCentralizedShoppingCartOpenInvoiceAdd' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartOpenInvoiceAdd',
      'WEBCentralizedShoppingCartOpenInvoiceAddResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartOpenInvoiceAddResponse',
      'WEBCentralizedShoppingCartOpenInvoiceGet' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartOpenInvoiceGet',
      'WEBCentralizedShoppingCartOpenInvoiceGetResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartOpenInvoiceGetResponse',
      'WEBCentralizedShoppingCartOpenInvoiceGetList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartOpenInvoiceGetList',
      'WEBCentralizedShoppingCartOpenInvoiceGetListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartOpenInvoiceGetListResponse',
      'WEBCentralizedShoppingCartOpenInvoiceGetListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartOpenInvoiceGetListResult',
      'WEBCentralizedShoppingCartMembershipRefresh' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipRefresh',
      'WEBCentralizedShoppingCartMembershipRefreshResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipRefreshResponse',
      'WEBCentralizedShoppingCartMembershipAddMembership' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipAddMembership',
      'WEBCentralizedShoppingCartMembershipAddMembershipResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCentralizedShoppingCartMembershipAddMembershipResponse',
      'Authenticate' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Authenticate',
      'AuthenticateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AuthenticateResponse',
      'AuthenticateLdap' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AuthenticateLdap',
      'AuthenticateLdapResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AuthenticateLdapResponse',
      'WebLogin' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WebLogin',
      'WebLoginResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WebLoginResponse',
      'WebValidate' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WebValidate',
      'WebValidateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WebValidateResponse',
      'WebLogout' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WebLogout',
      'WebLogoutResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WebLogoutResponse',
      'GetQuery' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetQuery',
      'GetQueryResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetQueryResponse',
      'GetDynamicQuery' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetDynamicQuery',
      'ArrayOfParameter' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ArrayOfParameter',
      'Parameter' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Parameter',
      'GetDynamicQueryResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetDynamicQueryResponse',
      'GetDynamicQueryResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetDynamicQueryResult',
      'GetDynamicQueryDefinition' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetDynamicQueryDefinition',
      'GetDynamicQueryDefinitionResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetDynamicQueryDefinitionResponse',
      'QueryDefinition' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\QueryDefinition',
      'ArrayOfQueryParameter' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ArrayOfQueryParameter',
      'QueryParameter' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\QueryParameter',
      'ArrayOfListOption' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ArrayOfListOption',
      'ListOption' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ListOption',
      'ArrayOfQueryColumn' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ArrayOfQueryColumn',
      'QueryColumn' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\QueryColumn',
      'GetAudienceList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetAudienceList',
      'GetAudienceListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetAudienceListResponse',
      'GetAudienceListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetAudienceListResult',
      'GetAudience' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetAudience',
      'GetAudienceResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetAudienceResponse',
      'GetAudienceResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetAudienceResult',
      'GetAudienceDefinition' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetAudienceDefinition',
      'GetAudienceDefinitionResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetAudienceDefinitionResponse',
      'AudienceDefinition' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AudienceDefinition',
      'GetQueryDefinition' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetQueryDefinition',
      'GetQueryDefinitionResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetQueryDefinitionResponse',
      'Object' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Object',
      'ListTable' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ListTable',
      'ArrayOfListFromTable' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ArrayOfListFromTable',
      'ListFromTable' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ListFromTable',
      'ArrayOfColumn' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ArrayOfColumn',
      'Column' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Column',
      'GetFacadeXMLSchema' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetFacadeXMLSchema',
      'GetFacadeXMLSchemaResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetFacadeXMLSchemaResponse',
      'GetFacadeXMLSchemaResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetFacadeXMLSchemaResult',
      'GetFacadeObjectList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetFacadeObjectList',
      'GetFacadeObjectListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetFacadeObjectListResponse',
      'GetFacadeObjectListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetFacadeObjectListResult',
      'GetFacadeObject' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetFacadeObject',
      'GetFacadeObjectResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetFacadeObjectResponse',
      'GetFacadeObjectResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetFacadeObjectResult',
      'UpdateFacadeObject' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\UpdateFacadeObject',
      'UpdateFacadeObjectResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\UpdateFacadeObjectResponse',
      'UpdateFacadeObjectResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\UpdateFacadeObjectResult',
      'InsertFacadeObject' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InsertFacadeObject',
      'InsertFacadeObjectResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InsertFacadeObjectResponse',
      'InsertFacadeObjectResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InsertFacadeObjectResult',
      'GetIndividualInformation' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetIndividualInformation',
      'GetIndividualInformationResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetIndividualInformationResponse',
      'GetIndividualInformationResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetIndividualInformationResult',
      'SetIndividualInformation' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\SetIndividualInformation',
      'oUpdateNode' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\oUpdateNode',
      'SetIndividualInformationResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\SetIndividualInformationResponse',
      'SetIndividualInformationResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\SetIndividualInformationResult',
      'NewIndividualInformation' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\NewIndividualInformation',
      'NewIndividualInformationResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\NewIndividualInformationResponse',
      'NewIndividualInformationResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\NewIndividualInformationResult',
      'GetOrganizationInformation' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetOrganizationInformation',
      'GetOrganizationInformationResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetOrganizationInformationResponse',
      'GetOrganizationInformationResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetOrganizationInformationResult',
      'SetOrganizationInformation' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\SetOrganizationInformation',
      'SetOrganizationInformationResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\SetOrganizationInformationResponse',
      'SetOrganizationInformationResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\SetOrganizationInformationResult',
      'NewOrganizationInformation' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\NewOrganizationInformation',
      'NewOrganizationInformationResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\NewOrganizationInformationResponse',
      'NewOrganizationInformationResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\NewOrganizationInformationResult',
      'CreateInvoice' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CreateInvoice',
      'InvoiceNode' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceNode',
      'CreateInvoiceResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CreateInvoiceResponse',
      'CreateInvoiceResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CreateInvoiceResult',
      'CreatePayment' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CreatePayment',
      'CreatePaymentResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CreatePaymentResponse',
      'CreatePaymentResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CreatePaymentResult',
      'CreateAdvocacyData' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CreateAdvocacyData',
      'CreateAdvocacyDataResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CreateAdvocacyDataResponse',
      'GetActionTypeList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetActionTypeList',
      'GetActionTypeListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetActionTypeListResponse',
      'GetActionTypeListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetActionTypeListResult',
      'GetActionSubTypeList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetActionSubTypeList',
      'GetActionSubTypeListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetActionSubTypeListResponse',
      'GetActionSubTypeListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetActionSubTypeListResult',
      'InsertCustomerAction' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InsertCustomerAction',
      'ArrayOfGuid' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ArrayOfGuid',
      'InsertCustomerActionResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InsertCustomerActionResponse',
      'InsertActionSubType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InsertActionSubType',
      'InsertActionSubTypeResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InsertActionSubTypeResponse',
      'ElectronicSubscriptionGetCstListByIP' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ElectronicSubscriptionGetCstListByIP',
      'ElectronicSubscriptionGetCstListByIPResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ElectronicSubscriptionGetCstListByIPResponse',
      'ElectronicSubscriptionGetCstListByIPResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ElectronicSubscriptionGetCstListByIPResult',
      'ElectronicSubscriptionGetPurchasedSubscriptionsByCustomer' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ElectronicSubscriptionGetPurchasedSubscriptionsByCustomer',
      'ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResponse',
      'ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResult',
      'ExecuteMethod' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExecuteMethod',
      'ExecuteMethodResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExecuteMethodResponse',
      'ExecuteMethodResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExecuteMethodResult',
      'GetVersion' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetVersion',
      'GetVersionResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetVersionResponse',
      'xWebVersion' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\xWebVersion',
      'VersionClass' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\VersionClass',
      'TestConnection' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\TestConnection',
      'TestConnectionResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\TestConnectionResponse',
      'GetTimeZones' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetTimeZones',
      'GetTimeZonesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetTimeZonesResponse',
      'GetTimeZonesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetTimeZonesResult',
      'MergeCustomers' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\MergeCustomers',
      'ArrayOfMergeCustomer' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ArrayOfMergeCustomer',
      'MergeCustomer' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\MergeCustomer',
      'ArrayOfString1' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ArrayOfString1',
      'MergeCustomersResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\MergeCustomersResponse',
      'MergeCustomerResults' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\MergeCustomerResults',
      'ArrayOfMergeCustomerResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ArrayOfMergeCustomerResult',
      'MergeCustomerResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\MergeCustomerResult',
      'ArrayOfModuleResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ArrayOfModuleResult',
      'ModuleResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ModuleResult',
      'GetMergeCustomerModules' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetMergeCustomerModules',
      'GetMergeCustomerModulesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetMergeCustomerModulesResponse',
      'GetMergeCustomerModulesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GetMergeCustomerModulesResult',
      'WEBGetSystemOptions' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBGetSystemOptions',
      'WEBGetSystemOptionsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBGetSystemOptionsResponse',
      'WEBGetSystemOptionsResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBGetSystemOptionsResult',
      'WEBUpdateSystemOption' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBUpdateSystemOption',
      'WEBUpdateSystemOptionResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBUpdateSystemOptionResponse',
      'WEBGetAllRoles' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBGetAllRoles',
      'WEBGetAllRolesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBGetAllRolesResponse',
      'WEBGetAllRolesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBGetAllRolesResult',
      'WEBFindUsersInRole' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBFindUsersInRole',
      'WEBFindUsersInRoleResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBFindUsersInRoleResponse',
      'WEBFindUsersInRoleResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBFindUsersInRoleResult',
      'WEBGetRolesForUser' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBGetRolesForUser',
      'WEBGetRolesForUserResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBGetRolesForUserResponse',
      'WEBGetRolesForUserResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBGetRolesForUserResult',
      'WEBGetUsersInRole' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBGetUsersInRole',
      'WEBGetUsersInRoleResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBGetUsersInRoleResponse',
      'WEBGetUsersInRoleResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBGetUsersInRoleResult',
      'WEBIsUserInRole' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBIsUserInRole',
      'WEBIsUserInRoleResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBIsUserInRoleResponse',
      'WEBRoleExists' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBRoleExists',
      'WEBRoleExistsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBRoleExistsResponse',
      'WEBWebUserChangePassword' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserChangePassword',
      'WEBWebUserChangePasswordResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserChangePasswordResponse',
      'WEBWebUserCreate' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserCreate',
      'WEBWebUserCreateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserCreateResponse',
      'WEBWebUserUpdate' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserUpdate',
      'WEBWebUserUpdateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserUpdateResponse',
      'WEBWebUserFindUsersByEmail' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserFindUsersByEmail',
      'WEBWebUserFindUsersByEmailResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserFindUsersByEmailResponse',
      'WEBWebUserFindUsersByEmailResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserFindUsersByEmailResult',
      'WEBWebUserFindUsersByDomain' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserFindUsersByDomain',
      'WEBWebUserFindUsersByDomainResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserFindUsersByDomainResponse',
      'WEBWebUserFindUsersByDomainResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserFindUsersByDomainResult',
      'WEBWebUserFindOrganizationsByDomain' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserFindOrganizationsByDomain',
      'WEBWebUserFindOrganizationsByDomainResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserFindOrganizationsByDomainResponse',
      'WEBWebUserFindOrganizationsByDomainResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserFindOrganizationsByDomainResult',
      'WEBWebUserFindUsersByName' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserFindUsersByName',
      'WEBWebUserFindUsersByNameResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserFindUsersByNameResponse',
      'WEBWebUserFindUsersByNameResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserFindUsersByNameResult',
      'WEBWebUserFindUsersByUserNameFirstNameLastName' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserFindUsersByUserNameFirstNameLastName',
      'WEBWebUserFindUsersByUserNameFirstNameLastNameResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserFindUsersByUserNameFirstNameLastNameResponse',
      'WEBWebUserFindUsersByUserNameFirstNameLastNameResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserFindUsersByUserNameFirstNameLastNameResult',
      'WEBWebUserGet' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserGet',
      'WEBWebUserGetResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserGetResponse',
      'WEBWebUserGetByRecno_Custom' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserGetByRecno_Custom',
      'WEBWebUserGetByRecno_CustomResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserGetByRecno_CustomResponse',
      'WEBWebUserUnlock' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserUnlock',
      'WEBWebUserUnlockResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserUnlockResponse',
      'WEBWebUserLock' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserLock',
      'WEBWebUserLockResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserLockResponse',
      'WEBWebUserLogin' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserLogin',
      'WEBWebUserLoginResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserLoginResponse',
      'WEBWebUserLoginAndRememberMe' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserLoginAndRememberMe',
      'WEBWebUserLoginAndRememberMeResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserLoginAndRememberMeResponse',
      'WEBWebUserLoginByRememberMe' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserLoginByRememberMe',
      'WEBWebUserLoginByRememberMeResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserLoginByRememberMeResponse',
      'WEBWebUserValidateLogin' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserValidateLogin',
      'WEBWebUserValidateLoginResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserValidateLoginResponse',
      'WEBWebUserValidateToken' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserValidateToken',
      'WEBWebUserValidateTokenResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserValidateTokenResponse',
      'WEBWebUserLogin_Custom' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserLogin_Custom',
      'WEBWebUserLogin_CustomResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserLogin_CustomResponse',
      'WEBWebUserLoginByRecno_Custom' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserLoginByRecno_Custom',
      'WEBWebUserLoginByRecno_CustomResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserLoginByRecno_CustomResponse',
      'WEBWebUserValidateLogin_Custom' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserValidateLogin_Custom',
      'WEBWebUserValidateLogin_CustomResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserValidateLogin_CustomResponse',
      'WEBWebUserValidateToken_Custom' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserValidateToken_Custom',
      'WEBWebUserValidateToken_CustomResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBWebUserValidateToken_CustomResponse',
      'WEBIndividualGet' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBIndividualGet',
      'WEBIndividualGetResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBIndividualGetResponse',
      'WEBIndividualUpdate' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBIndividualUpdate',
      'WEBIndividualUpdateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBIndividualUpdateResponse',
      'WEBIndividualInsert' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBIndividualInsert',
      'WEBIndividualInsertResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBIndividualInsertResponse',
      'WEBIndividualGetPrefixes' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBIndividualGetPrefixes',
      'WEBIndividualGetPrefixesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBIndividualGetPrefixesResponse',
      'WEBIndividualGetPrefixesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBIndividualGetPrefixesResult',
      'WEBIndividualGetSuffixes' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBIndividualGetSuffixes',
      'WEBIndividualGetSuffixesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBIndividualGetSuffixesResponse',
      'WEBIndividualGetSuffixesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBIndividualGetSuffixesResult',
      'WEBAddressGetTypes' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBAddressGetTypes',
      'WEBAddressGetTypesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBAddressGetTypesResponse',
      'WEBAddressGetTypesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBAddressGetTypesResult',
      'WEBAddressGetCountries' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBAddressGetCountries',
      'WEBAddressGetCountriesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBAddressGetCountriesResponse',
      'WEBAddressGetCountriesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBAddressGetCountriesResult',
      'WEBAddressGetStates' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBAddressGetStates',
      'WEBAddressGetStatesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBAddressGetStatesResponse',
      'WEBAddressGetStatesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBAddressGetStatesResult',
      'WEBAddressGet' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBAddressGet',
      'WEBAddressGetResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBAddressGetResponse',
      'WEBAddressUpdate' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBAddressUpdate',
      'WEBAddressUpdateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBAddressUpdateResponse',
      'WEBAddressInsert' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBAddressInsert',
      'WEBAddressInsertResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBAddressInsertResponse',
      'WEBAddressGetAddressesByCustomer' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBAddressGetAddressesByCustomer',
      'WEBAddressGetAddressesByCustomerResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBAddressGetAddressesByCustomerResponse',
      'WEBAddressGetAddressesByCustomerResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBAddressGetAddressesByCustomerResult',
      'WEBPhoneGetTypes' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBPhoneGetTypes',
      'WEBPhoneGetTypesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBPhoneGetTypesResponse',
      'WEBPhoneGetTypesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBPhoneGetTypesResult',
      'WEBPhoneGet' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBPhoneGet',
      'WEBPhoneGetResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBPhoneGetResponse',
      'WEBPhoneUpdate' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBPhoneUpdate',
      'WEBPhoneUpdateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBPhoneUpdateResponse',
      'WEBPhoneInsert' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBPhoneInsert',
      'WEBPhoneInsertResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBPhoneInsertResponse',
      'WEBPhoneGetPhonesByCustomer' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBPhoneGetPhonesByCustomer',
      'WEBPhoneGetPhonesByCustomerResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBPhoneGetPhonesByCustomerResponse',
      'WEBPhoneGetPhonesByCustomerResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBPhoneGetPhonesByCustomerResult',
      'WEBFaxGetTypes' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBFaxGetTypes',
      'WEBFaxGetTypesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBFaxGetTypesResponse',
      'WEBFaxGetTypesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBFaxGetTypesResult',
      'WEBFaxGet' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBFaxGet',
      'WEBFaxGetResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBFaxGetResponse',
      'WEBFaxUpdate' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBFaxUpdate',
      'WEBFaxUpdateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBFaxUpdateResponse',
      'WEBFaxInsert' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBFaxInsert',
      'WEBFaxInsertResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBFaxInsertResponse',
      'WEBFaxGetFaxesByCustomer' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBFaxGetFaxesByCustomer',
      'WEBFaxGetFaxesByCustomerResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBFaxGetFaxesByCustomerResponse',
      'WEBFaxGetFaxesByCustomerResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBFaxGetFaxesByCustomerResult',
      'WEBEmailGet' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBEmailGet',
      'WEBEmailGetResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBEmailGetResponse',
      'WEBEmailUpdate' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBEmailUpdate',
      'WEBEmailUpdateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBEmailUpdateResponse',
      'WEBEmailInsert' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBEmailInsert',
      'WEBEmailInsertResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBEmailInsertResponse',
      'WEBEmailGetEmailsByCustomer' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBEmailGetEmailsByCustomer',
      'WEBEmailGetEmailsByCustomerResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBEmailGetEmailsByCustomerResponse',
      'WEBEmailGetEmailsByCustomerResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBEmailGetEmailsByCustomerResult',
      'WEBOrganizationGetTypes' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBOrganizationGetTypes',
      'WEBOrganizationGetTypesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBOrganizationGetTypesResponse',
      'WEBOrganizationGetTypesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBOrganizationGetTypesResult',
      'WEBOrganizationGet' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBOrganizationGet',
      'WEBOrganizationGetResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBOrganizationGetResponse',
      'WEBOrganizationUpdate' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBOrganizationUpdate',
      'WEBOrganizationUpdateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBOrganizationUpdateResponse',
      'WEBOrganizationInsert' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBOrganizationInsert',
      'WEBOrganizationInsertResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBOrganizationInsertResponse',
      'WEBContactRequestInsert' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBContactRequestInsert',
      'WEBContactRequestInsertResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBContactRequestInsertResponse',
      'WEBContactRequestGetOriginations' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBContactRequestGetOriginations',
      'WEBContactRequestGetOriginationsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBContactRequestGetOriginationsResponse',
      'WEBContactRequestGetOriginationsResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBContactRequestGetOriginationsResult',
      'WEBContactRequestGetPriorities' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBContactRequestGetPriorities',
      'WEBContactRequestGetPrioritiesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBContactRequestGetPrioritiesResponse',
      'WEBContactRequestGetPrioritiesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBContactRequestGetPrioritiesResult',
      'WEBContactRequestGetRequestTypes' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBContactRequestGetRequestTypes',
      'WEBContactRequestGetRequestTypesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBContactRequestGetRequestTypesResponse',
      'WEBContactRequestGetRequestTypesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBContactRequestGetRequestTypesResult',
      'WEBContactRequestGetRequestTypeReasons' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBContactRequestGetRequestTypeReasons',
      'WEBContactRequestGetRequestTypeReasonsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBContactRequestGetRequestTypeReasonsResponse',
      'WEBContactRequestGetRequestTypeReasonsResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBContactRequestGetRequestTypeReasonsResult',
      'WEBContactRequestGetStatuses' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBContactRequestGetStatuses',
      'WEBContactRequestGetStatusesResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBContactRequestGetStatusesResponse',
      'WEBContactRequestGetStatusesResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBContactRequestGetStatusesResult',
      'WEBActivityGetPurchasedProductsByCustomer' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetPurchasedProductsByCustomer',
      'WEBActivityGetPurchasedProductsByCustomerResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetPurchasedProductsByCustomerResponse',
      'WEBActivityGetPurchasedProductsByCustomerResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetPurchasedProductsByCustomerResult',
      'WEBActivityGetPurchasedEventsByCustomer' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetPurchasedEventsByCustomer',
      'WEBActivityGetPurchasedEventsByCustomerResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetPurchasedEventsByCustomerResponse',
      'WEBActivityGetPurchasedEventsByCustomerResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetPurchasedEventsByCustomerResult',
      'WEBActivityAlreadyRegisteredForEvent' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityAlreadyRegisteredForEvent',
      'WEBActivityAlreadyRegisteredForEventResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityAlreadyRegisteredForEventResponse',
      'WEBActivityNumberOfRegisteredGuests' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityNumberOfRegisteredGuests',
      'WEBActivityNumberOfRegisteredGuestsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityNumberOfRegisteredGuestsResponse',
      'WEBActivityGetPurchasedMembershipsByCustomer' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetPurchasedMembershipsByCustomer',
      'WEBActivityGetPurchasedMembershipsByCustomerResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetPurchasedMembershipsByCustomerResponse',
      'WEBActivityGetPurchasedMembershipsByCustomerResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetPurchasedMembershipsByCustomerResult',
      'WEBActivityGetPurchasedChapterMembershipsByCustomer' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetPurchasedChapterMembershipsByCustomer',
      'WEBActivityGetPurchasedChapterMembershipsByCustomerResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetPurchasedChapterMembershipsByCustomerResponse',
      'WEBActivityGetPurchasedChapterMembershipsByCustomerResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetPurchasedChapterMembershipsByCustomerResult',
      'WEBActivityGetPurchasedDownoadableProductsByCustomer' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetPurchasedDownoadableProductsByCustomer',
      'WEBActivityGetPurchasedDownoadableProductsByCustomerResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetPurchasedDownoadableProductsByCustomerResponse',
      'WEBActivityGetPurchasedDownoadableProductsByCustomerResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetPurchasedDownoadableProductsByCustomerResult',
      'WEBActivityGetRegistrantEvents' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetRegistrantEvents',
      'WEBActivityGetRegistrantEventsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetRegistrantEventsResponse',
      'WEBActivityGetRegistrantEventsResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetRegistrantEventsResult',
      'WEBActivityGetRegistrantSessions' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetRegistrantSessions',
      'WEBActivityGetRegistrantSessionsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetRegistrantSessionsResponse',
      'WEBActivityGetRegistrantSessionsResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetRegistrantSessionsResult',
      'WEBActivityGetRegistrantTracks' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetRegistrantTracks',
      'WEBActivityGetRegistrantTracksResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetRegistrantTracksResponse',
      'WEBActivityGetRegistrantTracksResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetRegistrantTracksResult',
      'WEBActivityGetRegistrantGuests' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetRegistrantGuests',
      'WEBActivityGetRegistrantGuestsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetRegistrantGuestsResponse',
      'WEBActivityGetRegistrantGuestsResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBActivityGetRegistrantGuestsResult',
      'WEBCommitteeGetCommitteeList' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetCommitteeList',
      'WEBCommitteeGetCommitteeListResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetCommitteeListResponse',
      'WEBCommitteeGetCommitteeListResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WEBCommitteeGetCommitteeListResult',
      'CustomerEmailType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CustomerEmailType',
      'IndividualEmail_Email_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualEmail_Email_DataObjectType',
      'IndividualEmail_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualEmail_Customer_DataObjectType',
      'CustomerAddressType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CustomerAddressType',
      'IndividualAddress_Address_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualAddress_Address_XRef_DataObjectType',
      'IndividualAddress_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualAddress_Customer_DataObjectType',
      'IndividualAddress_Individual_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualAddress_Individual_DataObjectType',
      'IndividualAddress_Organization_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualAddress_Organization_DataObjectType',
      'IndividualAddress_Chapter_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualAddress_Chapter_DataObjectType',
      'IndividualAddress_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualAddress_Address_DataObjectType',
      'IndividualAddress_Address_Change_Log_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualAddress_Address_Change_Log_DataObjectType',
      'IndividualAddress_Country_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualAddress_Country_DataObjectType',
      'IndividualAddress_Primary_Affiliation_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualAddress_Primary_Affiliation_DataObjectType',
      'IndividualAddress_StateTerritory_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualAddress_StateTerritory_DataObjectType',
      'InvoiceDetailCustomerType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetailCustomerType',
      'InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType',
      'InvoiceDetailCustomer_Invoice_Detail_Liability_View_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetailCustomer_Invoice_Detail_Liability_View_DataObjectType',
      'InvoiceDetailCustomer_Invoice_Detail_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetailCustomer_Invoice_Detail_DataObjectType',
      'InvoiceDetailCustomer_Price_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetailCustomer_Price_DataObjectType',
      'InvoiceDetailAdditionalType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetailAdditionalType',
      'InvoiceDetailAdditional_Invoice_Detail_Additional_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetailAdditional_Invoice_Detail_Additional_DataObjectType',
      'InvoiceDetailAdditional_Price_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetailAdditional_Price_DataObjectType',
      'InvoiceDetailType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetailType',
      'InvoiceDetail_Invoice_Detail_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Invoice_Detail_DataObjectType',
      'InvoiceDetail_Invoice_Detail_Term_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Invoice_Detail_Term_DataObjectType',
      'InvoiceDetail_Invoice_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Invoice_DataObjectType',
      'InvoiceDetail_Price_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Price_DataObjectType',
      'InvoiceDetail_Product_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Product_DataObjectType',
      'InvoiceDetail_ShipToCustomer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_ShipToCustomer_DataObjectType',
      'InvoiceDetail_SoldToCustomer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_SoldToCustomer_DataObjectType',
      'InvoiceDetail_Shipping_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Shipping_Address_DataObjectType',
      'InvoiceDetail_Shipping_Address_Type_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Shipping_Address_Type_DataObjectType',
      'InvoiceDetail_Invoice_Detail_Additional_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Invoice_Detail_Additional_DataObjectType',
      'InvoiceDetail_Package_Component_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Package_Component_DataObjectType',
      'InvoiceDetail_Bundle_Component_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Bundle_Component_DataObjectType',
      'InvoiceDetail_Product_Type_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Product_Type_DataObjectType',
      'InvoiceDetail_Discount_Product_X_Product_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Discount_Product_X_Product_DataObjectType',
      'InvoiceDetail_Billing_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Billing_Address_DataObjectType',
      'InvoiceDetail_Currency_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Currency_DataObjectType',
      'InvoiceDetail_Order_Detail_Schedule_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Order_Detail_Schedule_DataObjectType',
      'InvoiceDetail_Invoice_Detail_IP_Range_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Invoice_Detail_IP_Range_DataObjectType',
      'InvoiceDetail_Product_Type_Social_Messages_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Product_Type_Social_Messages_DataObjectType',
      'InvoiceDetail_Invoice_Detail_Liability_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Invoice_Detail_Liability_DataObjectType',
      'InvoiceDetail_Credit_Detail_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Credit_Detail_DataObjectType',
      'InvoiceDetail_Invoice_Detail_Amount_Due_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Invoice_Detail_Amount_Due_DataObjectType',
      'InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceDetail_Invoice_Detail_Get_Balance_DataObjectType',
      'Invoice_Detail_AdditionalCollectionType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_Detail_AdditionalCollectionType',
      'Invoice_Detail_LiabilityCollectionType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_Detail_LiabilityCollectionType',
      'FundraisingGiftType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGiftType',
      'FundraisingGift_Gift_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Gift_DataObjectType',
      'FundraisingGift_Constituent_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Constituent_DataObjectType',
      'FundraisingGift_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Customer_DataObjectType',
      'FundraisingGift_Individual_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Individual_DataObjectType',
      'FundraisingGift_Ind_Business_Address_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Ind_Business_Address_XRef_DataObjectType',
      'FundraisingGift_Ind_Business_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Ind_Business_Address_DataObjectType',
      'FundraisingGift_Ind_Business_Address_Country_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Ind_Business_Address_Country_DataObjectType',
      'FundraisingGift_Ind_Business_Phone_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Ind_Business_Phone_XRef_DataObjectType',
      'FundraisingGift_Ind_Business_Phone_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Ind_Business_Phone_DataObjectType',
      'FundraisingGift_Ind_Business_Phone_country_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Ind_Business_Phone_country_DataObjectType',
      'FundraisingGift_Ind_Business_Fax_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Ind_Business_Fax_XRef_DataObjectType',
      'FundraisingGift_Ind_Business_Fax_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Ind_Business_Fax_DataObjectType',
      'FundraisingGift_Ind_Business_Fax_Country_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Ind_Business_Fax_Country_DataObjectType',
      'FundraisingGift_Ind_eMail_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Ind_eMail_DataObjectType',
      'FundraisingGift_Ind_URL_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Ind_URL_DataObjectType',
      'FundraisingGift_Ind_Messaging_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Ind_Messaging_DataObjectType',
      'FundraisingGift_Organization_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Organization_DataObjectType',
      'FundraisingGift_Org_Business_Address_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Org_Business_Address_XRef_DataObjectType',
      'FundraisingGift_Org_Business_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Org_Business_Address_DataObjectType',
      'FundraisingGift_Org_Business_Address_Country_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Org_Business_Address_Country_DataObjectType',
      'FundraisingGift_Org_Business_Phone_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Org_Business_Phone_XRef_DataObjectType',
      'FundraisingGift_Org_Business_Phone_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Org_Business_Phone_DataObjectType',
      'FundraisingGift_Org_Business_Phone_country_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Org_Business_Phone_country_DataObjectType',
      'FundraisingGift_Org_Business_Fax_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Org_Business_Fax_XRef_DataObjectType',
      'FundraisingGift_Org_Business_Fax_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Org_Business_Fax_DataObjectType',
      'FundraisingGift_Org_Business_Fax_Country_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Org_Business_Fax_Country_DataObjectType',
      'FundraisingGift_Org_eMail_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Org_eMail_DataObjectType',
      'FundraisingGift_Org_URL_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Org_URL_DataObjectType',
      'FundraisingGift_Org_Messaging_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Org_Messaging_DataObjectType',
      'FundraisingGift_Pledge_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Pledge_DataObjectType',
      'FundraisingGift_Premium_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Premium_DataObjectType',
      'FundraisingGift_Product_Code_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Product_Code_DataObjectType',
      'FundraisingGift_Product_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Product_DataObjectType',
      'FundraisingGift_Campaign_Code_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Campaign_Code_DataObjectType',
      'FundraisingGift_Fund_Code_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Fund_Code_DataObjectType',
      'FundraisingGift_Appeal_Code_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Appeal_Code_DataObjectType',
      'FundraisingGift_Package_Code_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Package_Code_DataObjectType',
      'FundraisingGift_Order_Detail_Schedule_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Order_Detail_Schedule_DataObjectType',
      'FundraisingGift_Recognition_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Recognition_Customer_DataObjectType',
      'FundraisingGift_Tribute_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Tribute_DataObjectType',
      'FundraisingGift_Tribute_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Tribute_Customer_DataObjectType',
      'FundraisingGift_Gift_Type_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Gift_Type_DataObjectType',
      'FundraisingGift_Recogntion_Type_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Recogntion_Type_DataObjectType',
      'FundraisingGift_Contributor_Gift_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Contributor_Gift_DataObjectType',
      'FundraisingGift_Solicitor_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Solicitor_Customer_DataObjectType',
      'FundraisingGift_Source_Code_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Source_Code_DataObjectType',
      'FundraisingGift_ComplementaryGifts_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_ComplementaryGifts_DataObjectType',
      'FundraisingGift_Invoice_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Invoice_DataObjectType',
      'FundraisingGift_Payment_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Payment_DataObjectType',
      'FundraisingGift_Payment_Info_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Payment_Info_DataObjectType',
      'FundraisingGift_Payment_Method_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Payment_Method_DataObjectType',
      'FundraisingGift_Installment_Billing_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Installment_Billing_DataObjectType',
      'FundraisingGift_Gift_Price_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Gift_Price_DataObjectType',
      'FundraisingGift_Registrant_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Registrant_DataObjectType',
      'FundraisingGift_Registrant_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Registrant_Customer_DataObjectType',
      'FundraisingGift_Event_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Event_DataObjectType',
      'FundraisingGift_Net_Amount_Information_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_Net_Amount_Information_DataObjectType',
      'FundraisingGift_rgs_reg_key_Query_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\FundraisingGift_rgs_reg_key_Query_DataObjectType',
      'Invoice_DetailCollectionType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_DetailCollectionType',
      'Additional_Invoice_DetailCollectionType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Additional_Invoice_DetailCollectionType',
      'mb_membershipType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membershipType',
      'mb_membership_Membership_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Membership_DataObjectType',
      'mb_membership_Chapter_Membership_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Chapter_Membership_DataObjectType',
      'mb_membership_Change_Membership_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Change_Membership_DataObjectType',
      'mb_membership_Invoice_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Invoice_DataObjectType',
      'mb_membership_Batch_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Batch_DataObjectType',
      'mb_membership_Inovice_Terms_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Inovice_Terms_DataObjectType',
      'mb_membership_Individual_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Individual_DataObjectType',
      'mb_membership_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Customer_DataObjectType',
      'mb_membership_Customer_Lookup_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Customer_Lookup_DataObjectType',
      'mb_membership_Referring_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Referring_Customer_DataObjectType',
      'mb_membership_Organization_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Organization_DataObjectType',
      'mb_membership_Membership_Invoices_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Membership_Invoices_DataObjectType',
      'mb_membership_Chapter_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Chapter_DataObjectType',
      'mb_membership_Member_Type_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Member_Type_DataObjectType',
      'mb_membership_Membership_Audit_Trail_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Membership_Audit_Trail_DataObjectType',
      'mb_membership_Member_Dues_History_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Member_Dues_History_DataObjectType',
      'mb_membership_Dues_Variables_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Dues_Variables_DataObjectType',
      'mb_membership_Payment_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Payment_DataObjectType',
      'mb_membership_Payment_Info_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Payment_Info_DataObjectType',
      'mb_membership_Payment_Method_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Payment_Method_DataObjectType',
      'mb_membership_Association_Package_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Association_Package_DataObjectType',
      'mb_membership_Association_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Association_DataObjectType',
      'mb_membership_Bundle_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Bundle_DataObjectType',
      'mb_membership_Installment_Billing_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Installment_Billing_DataObjectType',
      'mb_membership_Package_Component_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Package_Component_DataObjectType',
      'mb_membership_Package_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Package_DataObjectType',
      'mb_membership_Product_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Product_DataObjectType',
      'mb_membership_Chapter_Product_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_Chapter_Product_DataObjectType',
      'mb_membership_CustomerAddress_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_CustomerAddress_DataObjectType',
      'mb_membership_source_code_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\mb_membership_source_code_DataObjectType',
      'IndividualType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualType',
      'Individual_Individual_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Individual_DataObjectType',
      'Individual_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Customer_DataObjectType',
      'Individual_Organization_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Organization_XRef_DataObjectType',
      'Individual_Organization_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Organization_DataObjectType',
      'Individual_Email_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Email_DataObjectType',
      'Individual_Website_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Website_DataObjectType',
      'Individual_Messaging_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Messaging_DataObjectType',
      'Individual_Business_Address_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Business_Address_XRef_DataObjectType',
      'Individual_Business_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Business_Address_DataObjectType',
      'Individual_Business_Address_State_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Business_Address_State_DataObjectType',
      'Individual_Business_Address_Country_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Business_Address_Country_DataObjectType',
      'Individual_Business_Phone_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Business_Phone_XRef_DataObjectType',
      'Individual_Business_Phone_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Business_Phone_DataObjectType',
      'Individual_Business_Phone_Country_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Business_Phone_Country_DataObjectType',
      'Individual_Business_Fax_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Business_Fax_XRef_DataObjectType',
      'Individual_Business_Fax_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Business_Fax_DataObjectType',
      'Individual_Business_Fax_Country_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Business_Fax_Country_DataObjectType',
      'Individual_Home_Address_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Home_Address_XRef_DataObjectType',
      'Individual_Home_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Home_Address_DataObjectType',
      'Individual_Home_Address_State_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Home_Address_State_DataObjectType',
      'Individual_Home_Address_Country_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Home_Address_Country_DataObjectType',
      'Individual_Home_Phone_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Home_Phone_DataObjectType',
      'Individual_Home_Phone_Country_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Home_Phone_Country_DataObjectType',
      'Individual_Home_Phone_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Home_Phone_XRef_DataObjectType',
      'Individual_Home_Fax_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Home_Fax_DataObjectType',
      'Individual_Home_Fax_Country_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Home_Fax_Country_DataObjectType',
      'Individual_Home_Fax_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Home_Fax_XRef_DataObjectType',
      'Individual_Address_Change_Log_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Address_Change_Log_DataObjectType',
      'Individual_TransferToCustomer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_TransferToCustomer_DataObjectType',
      'Individual_Individual_Custom_Demographics_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Individual_Custom_Demographics_DataObjectType',
      'Individual_Social_Links_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_Social_Links_DataObjectType',
      'Individual_source_code_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Individual_source_code_DataObjectType',
      'EventsRegistrantSessionType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantSessionType',
      'EventsRegistrantSession_ev_registration_session_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantSession_ev_registration_session_DataObjectType',
      'EventsRegistrantSession_ev_registration_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantSession_ev_registration_DataObjectType',
      'EventsRegistrantSession_ev_session_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantSession_ev_session_DataObjectType',
      'EventsRegistrantSession_ev_track_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantSession_ev_track_DataObjectType',
      'EventsRegistrantSession_co_customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantSession_co_customer_DataObjectType',
      'EventsRegistrantSession_ev_event_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantSession_ev_event_DataObjectType',
      'EventsRegistrantType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantType',
      'EventsRegistrant_Registrant_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Registrant_DataObjectType',
      'EventsRegistrant_Individual_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Individual_DataObjectType',
      'EventsRegistrant_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Customer_DataObjectType',
      'EventsRegistrant_Individual_X_Organization_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Individual_X_Organization_DataObjectType',
      'EventsRegistrant_Organization_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Organization_DataObjectType',
      'EventsRegistrant_Address_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Address_XRef_DataObjectType',
      'EventsRegistrant_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Address_DataObjectType',
      'EventsRegistrant_Email_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Email_DataObjectType',
      'EventsRegistrant_Website_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Website_DataObjectType',
      'EventsRegistrant_Messaging_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Messaging_DataObjectType',
      'EventsRegistrant_Phone_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Phone_XRef_DataObjectType',
      'EventsRegistrant_Phone_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Phone_DataObjectType',
      'EventsRegistrant_Fax_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Fax_XRef_DataObjectType',
      'EventsRegistrant_Fax_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Fax_DataObjectType',
      'EventsRegistrant_Event_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Event_DataObjectType',
      'EventsRegistrant_Event_Location_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Event_Location_DataObjectType',
      'EventsRegistrant_Location_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Location_DataObjectType',
      'EventsRegistrant_Location_Customer_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Location_Customer_Address_DataObjectType',
      'EventsRegistrant_Location_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Location_Address_DataObjectType',
      'EventsRegistrant_Location_Phone_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Location_Phone_XRef_DataObjectType',
      'EventsRegistrant_Location_Website_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Location_Website_DataObjectType',
      'EventsRegistrant_Event_Location_Room_Type_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Event_Location_Room_Type_DataObjectType',
      'EventsRegistrant_Room_Request_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Room_Request_DataObjectType',
      'EventsRegistrant_Registrant_Housing_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Registrant_Housing_DataObjectType',
      'EventsRegistrant_RegistrantGuest_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_RegistrantGuest_DataObjectType',
      'EventsRegistrant_Invoice_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Invoice_DataObjectType',
      'EventsRegistrant_Payment_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Payment_DataObjectType',
      'EventsRegistrant_Payment_Info_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Payment_Info_DataObjectType',
      'EventsRegistrant_Payment_Method_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Payment_Method_DataObjectType',
      'EventsRegistrant_Product_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Product_DataObjectType',
      'EventsRegistrant_Price_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Price_DataObjectType',
      'EventsRegistrant_Registrant_Type_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Registrant_Type_DataObjectType',
      'EventsRegistrant_Registrant_Custom_Demographics_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Registrant_Custom_Demographics_DataObjectType',
      'EventsRegistrant_Gift_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Gift_DataObjectType',
      'EventsRegistrant_source_code_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_source_code_DataObjectType',
      'EventsRegistrant_Registrant_Session_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrant_Registrant_Session_DataObjectType',
      'Registrant_SessionCollectionType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Registrant_SessionCollectionType',
      'EventsRegistrantGroupType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroupType',
      'EventsRegistrantGroup_Group_Registration_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Group_Registration_DataObjectType',
      'EventsRegistrantGroup_Organization_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Organization_DataObjectType',
      'EventsRegistrantGroup_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Customer_DataObjectType',
      'EventsRegistrantGroup_Individual_X_Organization_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Individual_X_Organization_DataObjectType',
      'EventsRegistrantGroup_Individual_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Individual_DataObjectType',
      'EventsRegistrantGroup_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Address_DataObjectType',
      'EventsRegistrantGroup_Address_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Address_XRef_DataObjectType',
      'EventsRegistrantGroup_Email_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Email_DataObjectType',
      'EventsRegistrantGroup_Website_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Website_DataObjectType',
      'EventsRegistrantGroup_Messaging_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Messaging_DataObjectType',
      'EventsRegistrantGroup_Phone_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Phone_DataObjectType',
      'EventsRegistrantGroup_Phone_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Phone_XRef_DataObjectType',
      'EventsRegistrantGroup_Fax_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Fax_DataObjectType',
      'EventsRegistrantGroup_Fax_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Fax_XRef_DataObjectType',
      'EventsRegistrantGroup_Event_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Event_DataObjectType',
      'EventsRegistrantGroup_Event_Location_Room_Type_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Event_Location_Room_Type_DataObjectType',
      'EventsRegistrantGroup_Room_Request_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Room_Request_DataObjectType',
      'EventsRegistrantGroup_Invoice_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Invoice_DataObjectType',
      'EventsRegistrantGroup_Payment_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Payment_DataObjectType',
      'EventsRegistrantGroup_Payment_Info_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Payment_Info_DataObjectType',
      'EventsRegistrantGroup_Payment_Method_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Payment_Method_DataObjectType',
      'EventsRegistrantGroup_source_code_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_source_code_DataObjectType',
      'EventsRegistrantGroup_Registrant_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\EventsRegistrantGroup_Registrant_DataObjectType',
      'InvoiceType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceType',
      'Invoice_Invoice_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_Invoice_DataObjectType',
      'Invoice_Invoice_Detail_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_Invoice_Detail_DataObjectType',
      'Invoice_Invoice_Detail_Term_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_Invoice_Detail_Term_DataObjectType',
      'Invoice_Claim_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_Claim_DataObjectType',
      'Invoice_Default_Shipping_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_Default_Shipping_Address_DataObjectType',
      'Invoice_Payment_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_Payment_DataObjectType',
      'Invoice_Payment_Info_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_Payment_Info_DataObjectType',
      'Invoice_Payment_Method_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_Payment_Method_DataObjectType',
      'Invoice_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_Customer_DataObjectType',
      'Invoice_Billing_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_Billing_Address_DataObjectType',
      'Invoice_Billing_Adr_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_Billing_Adr_DataObjectType',
      'Invoice_Price_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_Price_DataObjectType',
      'Invoice_Batch_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_Batch_DataObjectType',
      'Invoice_BillToCustomer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_BillToCustomer_DataObjectType',
      'Invoice_BillToIndividual_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_BillToIndividual_DataObjectType',
      'Invoice_InstallmentBilling_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_InstallmentBilling_DataObjectType',
      'Invoice_Company-Business_Unit_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_CompanyBusiness_Unit_DataObjectType',
      'Invoice_Credit_Available_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_Credit_Available_DataObjectType',
      'Invoice_source_code_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_source_code_DataObjectType',
      'Invoice_Sales_Opportunity_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_Sales_Opportunity_DataObjectType',
      'Invoice_currency_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Invoice_currency_DataObjectType',
      'ExhibitorBoothNewType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorBoothNewType',
      'ExhibitorBoothNew_Exhibitor_Booth_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorBoothNew_Exhibitor_Booth_DataObjectType',
      'ExhibitorBoothNew_ExhibitorCustomer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorBoothNew_ExhibitorCustomer_DataObjectType',
      'ExhibitorBoothNew_Exhibitor_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorBoothNew_Exhibitor_DataObjectType',
      'ExhibitorBoothNew_Priority_Point_Detail_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorBoothNew_Priority_Point_Detail_DataObjectType',
      'ExhibitorBoothNew_Booth_Complement_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorBoothNew_Booth_Complement_DataObjectType',
      'ExhibitorBoothNew_Exhibit_Show_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorBoothNew_Exhibit_Show_DataObjectType',
      'ExhibitorBoothNew_Booth_Fee_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorBoothNew_Booth_Fee_DataObjectType',
      'ExhibitorBoothNew_Exhibit_Booth_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorBoothNew_Exhibit_Booth_DataObjectType',
      'ExhibitorBoothNew_Invoice_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorBoothNew_Invoice_DataObjectType',
      'ExhibitorBoothNew_Payment_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorBoothNew_Payment_DataObjectType',
      'ExhibitorBoothNew_Payment_Info_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorBoothNew_Payment_Info_DataObjectType',
      'ExhibitorBoothNew_Payment_Method_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorBoothNew_Payment_Method_DataObjectType',
      'ExhibitorBoothNew_Product_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorBoothNew_Product_DataObjectType',
      'ExhibitorBoothNew_Booth_Number_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorBoothNew_Booth_Number_DataObjectType',
      'ExhibitorBoothNew_Booth_Type_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorBoothNew_Booth_Type_DataObjectType',
      'ExhibitorBoothNew_Booth_Category_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorBoothNew_Booth_Category_DataObjectType',
      'ExhibitorNewType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNewType',
      'ExhibitorNew_Exhibitor_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Exhibitor_DataObjectType',
      'ExhibitorNew_Exhibit_Show_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Exhibit_Show_DataObjectType',
      'ExhibitorNew_Booth_Person_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Booth_Person_DataObjectType',
      'ExhibitorNew_Exhibitor_Booth_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Exhibitor_Booth_DataObjectType',
      'ExhibitorNew_booth_complement_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_booth_complement_DataObjectType',
      'ExhibitorNew_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Customer_DataObjectType',
      'ExhibitorNew_Contact_Person_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Contact_Person_DataObjectType',
      'ExhibitorNew_priority_points_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_priority_points_DataObjectType',
      'ExhibitorNew_Invoice_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Invoice_DataObjectType',
      'ExhibitorNew_Payment_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Payment_DataObjectType',
      'ExhibitorNew_Payment_Info_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Payment_Info_DataObjectType',
      'ExhibitorNew_Payment_Method_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Payment_Method_DataObjectType',
      'ExhibitorNew_Billing_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Billing_Address_DataObjectType',
      'ExhibitorNew_BillToCustomer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_BillToCustomer_DataObjectType',
      'ExhibitorNew_Installment_Billing_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Installment_Billing_DataObjectType',
      'ExhibitorNew_Exhibitor_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Exhibitor_Address_DataObjectType',
      'ExhibitorNew_Primary_Contact_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Primary_Contact_Address_DataObjectType',
      'ExhibitorNew_Exhibitor_X_Contact_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Exhibitor_X_Contact_DataObjectType',
      'ExhibitorNew_Shared_Booth_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Shared_Booth_DataObjectType',
      'ExhibitorNew_Sponsor_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Sponsor_DataObjectType',
      'ExhibitorNew_Primary_Contact_Email_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Primary_Contact_Email_DataObjectType',
      'ExhibitorNew_Primary_Contact_Website_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Primary_Contact_Website_DataObjectType',
      'ExhibitorNew_Exhibitor_Email_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Exhibitor_Email_DataObjectType',
      'ExhibitorNew_Exhibitor_Website_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorNew_Exhibitor_Website_DataObjectType',
      'Exhibitor_BoothCollectionType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Exhibitor_BoothCollectionType',
      'AccreditationAreaType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationAreaType',
      'AccreditationArea_Accreditation_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Accreditation_Customer_DataObjectType',
      'AccreditationArea_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Customer_DataObjectType',
      'AccreditationArea_Customer_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Customer_Address_DataObjectType',
      'AccreditationArea_Customer_Primary_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Customer_Primary_Address_DataObjectType',
      'AccreditationArea_Organization_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Organization_DataObjectType',
      'AccreditationArea_Accreditation_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Accreditation_DataObjectType',
      'AccreditationArea_Accreditation_Status_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Accreditation_Status_DataObjectType',
      'AccreditationArea_Accreditation_Type_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Accreditation_Type_DataObjectType',
      'AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType',
      'AccreditationArea_Affiliated_CEO_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Affiliated_CEO_DataObjectType',
      'AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType',
      'AccreditationArea_Affiliated_CAO_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Affiliated_CAO_DataObjectType',
      'AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType',
      'AccreditationArea_Affiliated_CMO_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Affiliated_CMO_DataObjectType',
      'AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType',
      'AccreditationArea_Affiliated_Survey_Contact_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Affiliated_Survey_Contact_DataObjectType',
      'AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType',
      'AccreditationArea_Decision_Letter_Recipient_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Decision_Letter_Recipient_DataObjectType',
      'AccreditationArea_Invoice_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Invoice_DataObjectType',
      'AccreditationArea_Payment_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Payment_DataObjectType',
      'AccreditationArea_Payment_Info_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Payment_Info_DataObjectType',
      'AccreditationArea_Payment_Method_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\AccreditationArea_Payment_Method_DataObjectType',
      'CentralizedOrderEntryType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntryType',
      'CentralizedOrderEntry_Invoice_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_Invoice_DataObjectType',
      'CentralizedOrderEntry_Invoice_Detail_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_Invoice_Detail_DataObjectType',
      'CentralizedOrderEntry_Invoice_Detail_Term_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_Invoice_Detail_Term_DataObjectType',
      'CentralizedOrderEntry_Claim_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_Claim_DataObjectType',
      'CentralizedOrderEntry_Default_Shipping_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_Default_Shipping_Address_DataObjectType',
      'CentralizedOrderEntry_Payment_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_Payment_DataObjectType',
      'CentralizedOrderEntry_Payment_Info_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_Payment_Info_DataObjectType',
      'CentralizedOrderEntry_Payment_Method_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_Payment_Method_DataObjectType',
      'CentralizedOrderEntry_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_Customer_DataObjectType',
      'CentralizedOrderEntry_Billing_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_Billing_Address_DataObjectType',
      'CentralizedOrderEntry_Billing_Adr_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_Billing_Adr_DataObjectType',
      'CentralizedOrderEntry_Price_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_Price_DataObjectType',
      'CentralizedOrderEntry_Batch_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_Batch_DataObjectType',
      'CentralizedOrderEntry_BillToCustomer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_BillToCustomer_DataObjectType',
      'CentralizedOrderEntry_BillToIndividual_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_BillToIndividual_DataObjectType',
      'CentralizedOrderEntry_InstallmentBilling_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_InstallmentBilling_DataObjectType',
      'CentralizedOrderEntry_Company-Business_Unit_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_CompanyBusiness_Unit_DataObjectType',
      'CentralizedOrderEntry_Credit_Available_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_Credit_Available_DataObjectType',
      'CentralizedOrderEntry_source_code_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_source_code_DataObjectType',
      'CentralizedOrderEntry_Sales_Opportunity_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_Sales_Opportunity_DataObjectType',
      'CentralizedOrderEntry_currency_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CentralizedOrderEntry_currency_DataObjectType',
      'MembershipCollectionType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\MembershipCollectionType',
      'RegistrantCollectionType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\RegistrantCollectionType',
      'Group_RegistrantionCollectionType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Group_RegistrantionCollectionType',
      'GiftCollectionType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\GiftCollectionType',
      'InvoiceToPayCollectionType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\InvoiceToPayCollectionType',
      'ExhibitorCollectionType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ExhibitorCollectionType',
      'Accreditation_ApplicantCollectionType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Accreditation_ApplicantCollectionType',
      'OrganizationType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\OrganizationType',
      'Organization_Organization_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Organization_DataObjectType',
      'Organization_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Customer_DataObjectType',
      'Organization_Parent_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Parent_Customer_DataObjectType',
      'Organization_Email_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Email_DataObjectType',
      'Organization_Website_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Website_DataObjectType',
      'Organization_Messaging_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Messaging_DataObjectType',
      'Organization_Phone_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Phone_XRef_DataObjectType',
      'Organization_Phone_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Phone_DataObjectType',
      'Organization_Phone_Country_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Phone_Country_DataObjectType',
      'Organization_Fax_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Fax_XRef_DataObjectType',
      'Organization_Fax_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Fax_DataObjectType',
      'Organization_Fax_Country_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Fax_Country_DataObjectType',
      'Organization_Address_XRef_1_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Address_XRef_1_DataObjectType',
      'Organization_Address_1_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Address_1_DataObjectType',
      'Organization_Firm_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Firm_DataObjectType',
      'Organization_Business_Address_State_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Business_Address_State_DataObjectType',
      'Organization_Address_1_Country_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Address_1_Country_DataObjectType',
      'Organization_Billing_Address_1_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Billing_Address_1_DataObjectType',
      'Organization_Home_Address_State_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Home_Address_State_DataObjectType',
      'Organization_Billing_Address_1_Country_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Billing_Address_1_Country_DataObjectType',
      'Organization_Billing_Address_XRef_1_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Billing_Address_XRef_1_DataObjectType',
      'Organization_Customer_X_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Customer_X_Customer_DataObjectType',
      'Organization_Primary_Contact_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Primary_Contact_DataObjectType',
      'Organization_Address_Change_Log_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Address_Change_Log_DataObjectType',
      'Organization_Organization_Custom_Demographics_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Organization_Custom_Demographics_DataObjectType',
      'Organization_Social_Links_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_Social_Links_DataObjectType',
      'Organization_source_code_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Organization_source_code_DataObjectType',
      'CommitteeNominationsType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CommitteeNominationsType',
      'CommitteeNominations_Nominations_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CommitteeNominations_Nominations_DataObjectType',
      'CommitteeNominations_Nominee_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CommitteeNominations_Nominee_DataObjectType',
      'CommitteeNominations_Nominated_By_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CommitteeNominations_Nominated_By_DataObjectType',
      'CommitteeNominations_Committee_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CommitteeNominations_Committee_DataObjectType',
      'CommitteeNominations_Committee_Nomination_Status_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CommitteeNominations_Committee_Nomination_Status_DataObjectType',
      'CommitteeNominations_Committee_Position_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CommitteeNominations_Committee_Position_DataObjectType',
      'CommitteeNominations_Committee_Position_Codes_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CommitteeNominations_Committee_Position_Codes_DataObjectType',
      'CommitteeNominations_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CommitteeNominations_Address_DataObjectType',
      'CommitteeNominations_Individual_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CommitteeNominations_Individual_DataObjectType',
      'CustomerFaxType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CustomerFaxType',
      'IndividualFax_Fax_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualFax_Fax_XRef_DataObjectType',
      'IndividualFax_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualFax_Customer_DataObjectType',
      'IndividualFax_Fax_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualFax_Fax_DataObjectType',
      'IndividualFax_Country_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualFax_Country_DataObjectType',
      'WildcardComplexType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WildcardComplexType',
      'ContactRequestType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ContactRequestType',
      'ContactRequest_Contact_Request_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ContactRequest_Contact_Request_DataObjectType',
      'ContactRequest_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ContactRequest_Customer_DataObjectType',
      'ContactRequest_Individual_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ContactRequest_Individual_DataObjectType',
      'ContactRequest_Comments_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ContactRequest_Comments_DataObjectType',
      'ContactRequest_Document_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ContactRequest_Document_DataObjectType',
      'ContactRequest_Primary_Address_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ContactRequest_Primary_Address_XRef_DataObjectType',
      'ContactRequest_Primary_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ContactRequest_Primary_Address_DataObjectType',
      'ContactRequest_Assignment_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ContactRequest_Assignment_Customer_DataObjectType',
      'ContactRequest_Customer_Alias_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ContactRequest_Customer_Alias_DataObjectType',
      'ContactRequest_Customer_Activity_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ContactRequest_Customer_Activity_DataObjectType',
      'ContactRequest_Assignment_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ContactRequest_Assignment_DataObjectType',
      'ContactRequest_Org_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ContactRequest_Org_Customer_DataObjectType',
      'ContactRequest_Org_Request_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ContactRequest_Org_Request_DataObjectType',
      'CustomerPhoneType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CustomerPhoneType',
      'IndividualPhone_Phone_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualPhone_Phone_XRef_DataObjectType',
      'IndividualPhone_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualPhone_Customer_DataObjectType',
      'IndividualPhone_Phone_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualPhone_Phone_DataObjectType',
      'IndividualPhone_Country_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\IndividualPhone_Country_DataObjectType',
      'CommitteeType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\CommitteeType',
      'Committee_Committee_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Committee_Committee_DataObjectType',
      'Committee_Parent_Committee_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Committee_Parent_Committee_DataObjectType',
      'Committee_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Committee_Customer_DataObjectType',
      'Committee_Committee_Participant_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Committee_Committee_Participant_DataObjectType',
      'WebUserType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WebUserType',
      'WebUser_Individual_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WebUser_Individual_DataObjectType',
      'WebUser_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WebUser_Customer_DataObjectType',
      'WebUser_Email_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WebUser_Email_DataObjectType',
      'WebUser_Business_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WebUser_Business_Address_DataObjectType',
      'WebUser_Business_Phone_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WebUser_Business_Phone_DataObjectType',
      'WebUser_Business_Phone_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WebUser_Business_Phone_XRef_DataObjectType',
      'WebUser_Business_Fax_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WebUser_Business_Fax_DataObjectType',
      'WebUser_Business_Fax_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\WebUser_Business_Fax_XRef_DataObjectType',
      'ChapterType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\ChapterType',
      'Chapter_Chapter_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Chapter_Chapter_DataObjectType',
      'Chapter_Customer_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Chapter_Customer_DataObjectType',
      'Chapter_Association_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Chapter_Association_DataObjectType',
      'Chapter_Email_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Chapter_Email_DataObjectType',
      'Chapter_Website_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Chapter_Website_DataObjectType',
      'Chapter_Messaging_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Chapter_Messaging_DataObjectType',
      'Chapter_Phone_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Chapter_Phone_DataObjectType',
      'Chapter_Phone_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Chapter_Phone_XRef_DataObjectType',
      'Chapter_Fax_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Chapter_Fax_DataObjectType',
      'Chapter_Fax_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Chapter_Fax_XRef_DataObjectType',
      'Chapter_Address_1_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Chapter_Address_1_DataObjectType',
      'Chapter_Address_XRef_1_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Chapter_Address_XRef_1_DataObjectType',
      'Chapter_Address_Country_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Chapter_Address_Country_DataObjectType',
      'Chapter_Billing_Address_1_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Chapter_Billing_Address_1_DataObjectType',
      'Chapter_Billing_Address_XRef_1_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Chapter_Billing_Address_XRef_1_DataObjectType',
      'Chapter_Primary_Contact_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Chapter_Primary_Contact_DataObjectType',
      'Chapter_Primary_Contact_XRef_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Chapter_Primary_Contact_XRef_DataObjectType',
      'Chapter_Prim._Contact_Adr_Xref_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Chapter_Prim_Contact_Adr_Xref_DataObjectType',
      'Chapter_Prim._Contact_Address_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Chapter_Prim_Contact_Address_DataObjectType',
      'Chapter_Address_Change_Log_DataObjectType' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Secure\\Chapter_Address_Change_Log_DataObjectType',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'http://nfupgrade.netforument.com/nfUpgrade2017Demo/xWeb/Secure/netForumXML.asmx?WSDL';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * Remove a membership from the shopping cart.
     *
     * @param WEBCentralizedShoppingCartMebmershipRemoveMembership $parameters
     * @return WEBCentralizedShoppingCartMebmershipRemoveMembershipResponse
     */
    public function WEBCentralizedShoppingCartMebmershipRemoveMembership(WEBCentralizedShoppingCartMebmershipRemoveMembership $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartMebmershipRemoveMembership', array($parameters));
    }

    /**
     * Retrieves List of open invoices - a record with a membership type, indicates a
     *
     * @param WEBCentralizedShoppingCartMembershipOpenInvoiceGetList $parameters
     * @return WEBCentralizedShoppingCartMembershipOpenInvoiceGetListResponse
     */
    public function WEBCentralizedShoppingCartMembershipOpenInvoiceGetList(WEBCentralizedShoppingCartMembershipOpenInvoiceGetList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartMembershipOpenInvoiceGetList', array($parameters));
    }

    /**
     * Returns an existing open invoice object
     *
     * @param WEBCentralizedShoppingCartMembershipOpenInvoiceGet $parameters
     * @return WEBCentralizedShoppingCartMembershipOpenInvoiceGetResponse
     */
    public function WEBCentralizedShoppingCartMembershipOpenInvoiceGet(WEBCentralizedShoppingCartMembershipOpenInvoiceGet $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartMembershipOpenInvoiceGet', array($parameters));
    }

    /**
     * Adds the open invoice to the shopping cart
     *
     * @param WEBCentralizedShoppingCartMembershipOpenInvoiceAdd $parameters
     * @return WEBCentralizedShoppingCartMembershipOpenInvoiceAddResponse
     */
    public function WEBCentralizedShoppingCartMembershipOpenInvoiceAdd(WEBCentralizedShoppingCartMembershipOpenInvoiceAdd $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartMembershipOpenInvoiceAdd', array($parameters));
    }

    /**
     * Returns a list of exhibits
     *
     * @param WEBCentralizedShoppingCartExhibitorGetExhibitList $parameters
     * @return WEBCentralizedShoppingCartExhibitorGetExhibitListResponse
     */
    public function WEBCentralizedShoppingCartExhibitorGetExhibitList(WEBCentralizedShoppingCartExhibitorGetExhibitList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartExhibitorGetExhibitList', array($parameters));
    }

    /**
     * Returns a new exhibitor object
     *
     * @param WEBCentralizedShoppingCartExhibitorGetNew $parameters
     * @return WEBCentralizedShoppingCartExhibitorGetNewResponse
     */
    public function WEBCentralizedShoppingCartExhibitorGetNew(WEBCentralizedShoppingCartExhibitorGetNew $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartExhibitorGetNew', array($parameters));
    }

    /**
     * Returns an existing exhibitor object
     *
     * @param WEBCentralizedShoppingCartExhibitorGet $parameters
     * @return WEBCentralizedShoppingCartExhibitorGetResponse
     */
    public function WEBCentralizedShoppingCartExhibitorGet(WEBCentralizedShoppingCartExhibitorGet $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartExhibitorGet', array($parameters));
    }

    /**
     * Returns a list of available booth types by exhibit key
     *
     * @param WEBCentralizedShoppingCartExhibitorGetBoothTypeList $parameters
     * @return WEBCentralizedShoppingCartExhibitorGetBoothTypeListResponse
     */
    public function WEBCentralizedShoppingCartExhibitorGetBoothTypeList(WEBCentralizedShoppingCartExhibitorGetBoothTypeList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartExhibitorGetBoothTypeList', array($parameters));
    }

    /**
     * Returns a list of available booth categories by exhibit key
     *
     * @param WEBCentralizedShoppingCartExhibitorGetBoothCategoryList $parameters
     * @return WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResponse
     */
    public function WEBCentralizedShoppingCartExhibitorGetBoothCategoryList(WEBCentralizedShoppingCartExhibitorGetBoothCategoryList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartExhibitorGetBoothCategoryList', array($parameters));
    }

    /**
     * @param WEBCentralizedShoppingCartGetBoothList $parameters
     * @return WEBCentralizedShoppingCartGetBoothListResponse
     */
    public function WEBCentralizedShoppingCartExhibitorGetBoothList(WEBCentralizedShoppingCartGetBoothList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartExhibitorGetBoothList', array($parameters));
    }

    /**
     * @param WEBCentralizedShoppingCartExhibitorSetLineItems $parameters
     * @return WEBCentralizedShoppingCartExhibitorSetLineItemsResponse
     */
    public function WEBCentralizedShoppingCartExhibitorSetLineItems(WEBCentralizedShoppingCartExhibitorSetLineItems $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartExhibitorSetLineItems', array($parameters));
    }

    /**
     * @param WEBCentralizedShoppingCartExhibitorSetLineItemsWithCart $parameters
     * @return WEBCentralizedShoppingCartExhibitorSetLineItemsWithCartResponse
     */
    public function WEBCentralizedShoppingCartExhibitorSetLineItemsWithCart(WEBCentralizedShoppingCartExhibitorSetLineItemsWithCart $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartExhibitorSetLineItemsWithCart', array($parameters));
    }

    /**
     * @param WEBCentralizedShoppingCartExhibitorAddExhibitor $parameters
     * @return WEBCentralizedShoppingCartExhibitorAddExhibitorResponse
     */
    public function WEBCentralizedShoppingCartExhibitorAddExhibitor(WEBCentralizedShoppingCartExhibitorAddExhibitor $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartExhibitorAddExhibitor', array($parameters));
    }

    /**
     * Retrieves Shipping Product List.
     *
     * @param WEBCentralizedShoppingCartGetShippingOptions $parameters
     * @return WEBCentralizedShoppingCartGetShippingOptionsResponse
     */
    public function WEBCentralizedShoppingCartGetShippingOptions(WEBCentralizedShoppingCartGetShippingOptions $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetShippingOptions', array($parameters));
    }

    /**
     * Add a shipping item to Centralized Shopping Cart
     *
     * @param WEBCentralizedShoppingCartAddShippingItem $parameters
     * @return WEBCentralizedShoppingCartAddShippingItemResponse
     */
    public function WEBCentralizedShoppingCartAddShippingItem(WEBCentralizedShoppingCartAddShippingItem $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartAddShippingItem', array($parameters));
    }

    /**
     * Retrieves Available Payment Methods.
     *
     * @param WEBCentralizedShoppingCartGetPaymentOptions $parameters
     * @return WEBCentralizedShoppingCartGetPaymentOptionsResponse
     */
    public function WEBCentralizedShoppingCartGetPaymentOptions(WEBCentralizedShoppingCartGetPaymentOptions $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetPaymentOptions', array($parameters));
    }

    /**
     * Retrieves Installment Frequency Options. Assign ord_frequency to desired value. To generate Installments, set values for the following fields: ord_num_of_installments, ord_frequency, ord_ait_key
     *
     * @param WEBCentralizedShoppingCartGetInstallmentFrequencyOptions $parameters
     * @return WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResponse
     */
    public function WEBCentralizedShoppingCartGetInstallmentFrequencyOptions(WEBCentralizedShoppingCartGetInstallmentFrequencyOptions $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetInstallmentFrequencyOptions', array($parameters));
    }

    /**
     * Retrieves Installment Terms Options. Assign ord_ait_key to desired value. To generate Installments, set values for the following fields: ord_num_of_installments, ord_frequency, ord_ait_key
     *
     * @param WEBCentralizedShoppingCartGetInstallmentTermsOptions $parameters
     * @return WEBCentralizedShoppingCartGetInstallmentTermsOptionsResponse
     */
    public function WEBCentralizedShoppingCartGetInstallmentTermsOptions(WEBCentralizedShoppingCartGetInstallmentTermsOptions $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetInstallmentTermsOptions', array($parameters));
    }

    /**
     * Returns Product Detail / Line Item.
     *
     * @param WEBCentralizedShoppingCartGetProductLineItem $parameters
     * @return WEBCentralizedShoppingCartGetProductLineItemResponse
     */
    public function WEBCentralizedShoppingCartGetProductLineItem(WEBCentralizedShoppingCartGetProductLineItem $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetProductLineItem', array($parameters));
    }

    /**
     * Returns Product Detail / Line Item.
     *
     * @param WEBCentralizedShoppingCartGetProductLineItemWithCart $parameters
     * @return WEBCentralizedShoppingCartGetProductLineItemWithCartResponse
     */
    public function WEBCentralizedShoppingCartGetProductLineItemWithCart(WEBCentralizedShoppingCartGetProductLineItemWithCart $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetProductLineItemWithCart', array($parameters));
    }

    /**
     * Loads a line item.
     *
     * @param WEBCentralizedShoppingCartLoadLineItem $parameters
     * @return WEBCentralizedShoppingCartLoadLineItemResponse
     */
    public function WEBCentralizedShoppingCartLoadLineItem(WEBCentralizedShoppingCartLoadLineItem $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartLoadLineItem', array($parameters));
    }

    /**
     * Adds a line item to Centralized Shopping Cart
     *
     * @param WEBCentralizedShoppingCartAddLineItem $parameters
     * @return WEBCentralizedShoppingCartAddLineItemResponse
     */
    public function WEBCentralizedShoppingCartAddLineItem(WEBCentralizedShoppingCartAddLineItem $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartAddLineItem', array($parameters));
    }

    /**
     * Removes a line item from Centralized Shopping Cart
     *
     * @param WEBCentralizedShoppingCartRemoveLineItem $parameters
     * @return WEBCentralizedShoppingCartRemoveLineItemResponse
     */
    public function WEBCentralizedShoppingCartRemoveLineItem(WEBCentralizedShoppingCartRemoveLineItem $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartRemoveLineItem', array($parameters));
    }

    /**
     * Removes all line items from Centralized Shopping Cart
     *
     * @param WEBCentralizedShoppingCartRemoveAllLineItems $parameters
     * @return WEBCentralizedShoppingCartRemoveAllLineItemsResponse
     */
    public function WEBCentralizedShoppingCartRemoveAllLineItems(WEBCentralizedShoppingCartRemoveAllLineItems $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartRemoveAllLineItems', array($parameters));
    }

    /**
     * Retrieves Accreditation List
     *
     * @param WEBCentralizedShoppingCartGetAccreditationList $parameters
     * @return WEBCentralizedShoppingCartGetAccreditationListResponse
     */
    public function WEBCentralizedShoppingCartGetAccreditationList(WEBCentralizedShoppingCartGetAccreditationList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetAccreditationList', array($parameters));
    }

    /**
     * Retrieves Accreditation List by Customer
     *
     * @param WEBCentralizedShoppingCartGetAccreditationListByCustomer $parameters
     * @return WEBCentralizedShoppingCartGetAccreditationListByCustomerResponse
     */
    public function WEBCentralizedShoppingCartGetAccreditationListByCustomer(WEBCentralizedShoppingCartGetAccreditationListByCustomer $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetAccreditationListByCustomer', array($parameters));
    }

    /**
     * Retrieves Accreditation Type Fee List
     *
     * @param WEBCentralizedShoppingCartGetAccreditationFeeList $parameters
     * @return WEBCentralizedShoppingCartGetAccreditationFeeListResponse
     */
    public function WEBCentralizedShoppingCartGetAccreditationFeeList(WEBCentralizedShoppingCartGetAccreditationFeeList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetAccreditationFeeList', array($parameters));
    }

    /**
     * Retrieves Accreditation Type List
     *
     * @param WEBCentralizedShoppingCartGetAccreditationTypeList $parameters
     * @return WEBCentralizedShoppingCartGetAccreditationTypeListResponse
     */
    public function WEBCentralizedShoppingCartGetAccreditationTypeList(WEBCentralizedShoppingCartGetAccreditationTypeList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetAccreditationTypeList', array($parameters));
    }

    /**
     * Retrieves AccreditationType Status List
     *
     * @param WEBCentralizedShoppingCartGetAccreditationTypeStatusList $parameters
     * @return WEBCentralizedShoppingCartGetAccreditationTypeStatusListResponse
     */
    public function WEBCentralizedShoppingCartGetAccreditationTypeStatusList(WEBCentralizedShoppingCartGetAccreditationTypeStatusList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetAccreditationTypeStatusList', array($parameters));
    }

    /**
     * Returns an existing Accreditation
     *
     * @param WEBCentralizedShoppingCartAccreditationGet $parameters
     * @return WEBCentralizedShoppingCartAccreditationGetResponse
     */
    public function WEBCentralizedShoppingCartAccreditationGet(WEBCentralizedShoppingCartAccreditationGet $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartAccreditationGet', array($parameters));
    }

    /**
     * Returns a new Accreditation object
     *
     * @param WEBCentralizedShoppingCartAccreditationGetNew $parameters
     * @return WEBCentralizedShoppingCartAccreditationGetNewResponse
     */
    public function WEBCentralizedShoppingCartAccreditationGetNew(WEBCentralizedShoppingCartAccreditationGetNew $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartAccreditationGetNew', array($parameters));
    }

    /**
     * Adds/Removes Accreditationn Fees. Use this method before the Accreditation object is added to Centralized Order Entry
     *
     * @param WEBCentralizedShoppingCartAccreditationSetLineItems $parameters
     * @return WEBCentralizedShoppingCartAccreditationSetLineItemsResponse
     */
    public function WEBCentralizedShoppingCartAccreditationSetLineItems(WEBCentralizedShoppingCartAccreditationSetLineItems $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartAccreditationSetLineItems', array($parameters));
    }

    /**
     * Add a accreditation to the shopping cart.
     *
     * @param WEBCentralizedShoppingCartAccreditationAddAccreditation $parameters
     * @return WEBCentralizedShoppingCartAccreditationAddAccreditationResponse
     */
    public function WEBCentralizedShoppingCartAccreditationAddAccreditation(WEBCentralizedShoppingCartAccreditationAddAccreditation $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartAccreditationAddAccreditation', array($parameters));
    }

    /**
     * @param GetDateTime $parameters
     * @return GetDateTimeResponse
     */
    public function GetDateTime(GetDateTime $parameters)
    {
      return $this->__soapCall('GetDateTime', array($parameters));
    }

    /**
     * Retrieves Form Controls By Form Key
     *
     * @param MetaDataGetForm $parameters
     * @return MetaDataGetFormResponse
     */
    public function MetaDataGetForm(MetaDataGetForm $parameters)
    {
      return $this->__soapCall('MetaDataGetForm', array($parameters));
    }

    /**
     * Retrieves Form Controls By Form Key
     *
     * @param MetaDataGetFormForFacadeObject $parameters
     * @return MetaDataGetFormForFacadeObjectResponse
     */
    public function MetaDataGetFormForFacadeObject(MetaDataGetFormForFacadeObject $parameters)
    {
      return $this->__soapCall('MetaDataGetFormForFacadeObject', array($parameters));
    }

    /**
     * Retrieves Form Control details By Form Key and control name
     *
     * @param MetaDataGetFormControlForFacadeObject $parameters
     * @return MetaDataGetFormControlForFacadeObjectResponse
     */
    public function MetaDataGetFormControlForFacadeObject(MetaDataGetFormControlForFacadeObject $parameters)
    {
      return $this->__soapCall('MetaDataGetFormControlForFacadeObject', array($parameters));
    }

    /**
     * Retrieves Form Controls By Form Key
     *
     * @param MetaDataGetFormForIndividual $parameters
     * @return MetaDataGetFormForIndividualResponse
     */
    public function MetaDataGetFormForIndividual(MetaDataGetFormForIndividual $parameters)
    {
      return $this->__soapCall('MetaDataGetFormForIndividual', array($parameters));
    }

    /**
     * Retrieves Wizard detaild by key
     *
     * @param MetaDataGetWizard $parameters
     * @return MetaDataGetWizardResponse
     */
    public function MetaDataGetWizard(MetaDataGetWizard $parameters)
    {
      return $this->__soapCall('MetaDataGetWizard', array($parameters));
    }

    /**
     * Retrieves Wizard form detaild by key
     *
     * @param MetaDataGetWizardForm $parameters
     * @return MetaDataGetWizardFormResponse
     */
    public function MetaDataGetWizardForm(MetaDataGetWizardForm $parameters)
    {
      return $this->__soapCall('MetaDataGetWizardForm', array($parameters));
    }

    /**
     * Retrieves the column values for DropDownLists and ListBoxes.
     *
     * @param WEBColumnGetColumnValuesByColumnName $parameters
     * @return WEBColumnGetColumnValuesByColumnNameResponse
     */
    public function WEBColumnGetColumnValuesByColumnName(WEBColumnGetColumnValuesByColumnName $parameters)
    {
      return $this->__soapCall('WEBColumnGetColumnValuesByColumnName', array($parameters));
    }

    /**
     * Get available promotional mailing types available for customers
     *
     * @param GetPromotionalMailingTypes $parameters
     * @return GetPromotionalMailingTypesResponse
     */
    public function GetPromotionalMailingTypes(GetPromotionalMailingTypes $parameters)
    {
      return $this->__soapCall('GetPromotionalMailingTypes', array($parameters));
    }

    /**
     * Get available subscription mailing types available for customers
     *
     * @param GetSubscriptionMailingTypes $parameters
     * @return GetSubscriptionMailingTypesResponse
     */
    public function GetSubscriptionMailingTypes(GetSubscriptionMailingTypes $parameters)
    {
      return $this->__soapCall('GetSubscriptionMailingTypes', array($parameters));
    }

    /**
     * Retrieve an individual's contact preference for a single mailing type.
     *
     * @param GetCustomerCommunicationPreference $parameters
     * @return GetCustomerCommunicationPreferenceResponse
     */
    public function GetCustomerCommunicationPreference(GetCustomerCommunicationPreference $parameters)
    {
      return $this->__soapCall('GetCustomerCommunicationPreference', array($parameters));
    }

    /**
     * Retrieve an individual's contact preferences.
     *
     * @param GetAllCustomerCommunicationPreferences $parameters
     * @return GetAllCustomerCommunicationPreferencesResponse
     */
    public function GetAllCustomerCommunicationPreferences(GetAllCustomerCommunicationPreferences $parameters)
    {
      return $this->__soapCall('GetAllCustomerCommunicationPreferences', array($parameters));
    }

    /**
     * Set an individual's contact preferences.
     *
     * @param SetCustomerCommunicationPreferences $parameters
     * @return SetCustomerCommunicationPreferencesResponse
     */
    public function SetCustomerCommunicationPreferences(SetCustomerCommunicationPreferences $parameters)
    {
      return $this->__soapCall('SetCustomerCommunicationPreferences', array($parameters));
    }

    /**
     * Get the fundraising goal and achieved amount for a fundraising event participant.
     *
     * @param GetFundraisingEventRegistrantionTotal $parameters
     * @return GetFundraisingEventRegistrantionTotalResponse
     */
    public function GetFundraisingEventRegistrationTotal(GetFundraisingEventRegistrantionTotal $parameters)
    {
      return $this->__soapCall('GetFundraisingEventRegistrationTotal', array($parameters));
    }

    /**
     * Get a list of donations provided to a fundraising event partifipant.
     *
     * @param GetFundraisingEventDonations $parameters
     * @return GetFundraisingEventDonationsResponse
     */
    public function GetFundraisingEventDonations(GetFundraisingEventDonations $parameters)
    {
      return $this->__soapCall('GetFundraisingEventDonations', array($parameters));
    }

    /**
     * Get a link to a web page that be used to donate on behalf of a fundraising event participant.
     *
     * @param GetFundraisingEventDonationUrl $parameters
     * @return GetFundraisingEventDonationUrlResponse
     */
    public function GetFundraisingEventDonationUrl(GetFundraisingEventDonationUrl $parameters)
    {
      return $this->__soapCall('GetFundraisingEventDonationUrl', array($parameters));
    }

    /**
     * Retrieves List of Active Committees by name.
     *
     * @param WEBCommitteeGetCommitteeListByName $parameters
     * @return WEBCommitteeGetCommitteeListByNameResponse
     */
    public function WEBCommitteeGetCommitteeListByName(WEBCommitteeGetCommitteeListByName $parameters)
    {
      return $this->__soapCall('WEBCommitteeGetCommitteeListByName', array($parameters));
    }

    /**
     * Retrieves Committee List for logged in web user.
     *
     * @param WEBCommitteeGetCommitteesByCustomer $parameters
     * @return WEBCommitteeGetCommitteesByCustomerResponse
     */
    public function WEBCommitteeGetCommitteesByCustomer(WEBCommitteeGetCommitteesByCustomer $parameters)
    {
      return $this->__soapCall('WEBCommitteeGetCommitteesByCustomer', array($parameters));
    }

    /**
     * Retrieves List of Active Sub-Committees.
     *
     * @param WEBCommitteeGetSubCommitteeListByCommittee $parameters
     * @return WEBCommitteeGetSubCommitteeListByCommitteeResponse
     */
    public function WEBCommitteeGetSubCommitteeListByCommittee(WEBCommitteeGetSubCommitteeListByCommittee $parameters)
    {
      return $this->__soapCall('WEBCommitteeGetSubCommitteeListByCommittee', array($parameters));
    }

    /**
     * Retrieves Committee Members for specified committee.
     *
     * @param WEBCommitteeGetMembers $parameters
     * @return WEBCommitteeGetMembersResponse
     */
    public function WEBCommitteeGetMembers(WEBCommitteeGetMembers $parameters)
    {
      return $this->__soapCall('WEBCommitteeGetMembers', array($parameters));
    }

    /**
     * Retrieves Committee Documents for specified committee.
     *
     * @param WEBCommitteeGetDocuments $parameters
     * @return WEBCommitteeGetDocumentsResponse
     */
    public function WEBCommitteeGetDocuments(WEBCommitteeGetDocuments $parameters)
    {
      return $this->__soapCall('WEBCommitteeGetDocuments', array($parameters));
    }

    /**
     * Returns committee information.
     *
     * @param WEBCommitteeGetCommitteeByKey $parameters
     * @return WEBCommitteeGetCommitteeByKeyResponse
     */
    public function WEBCommitteeGetCommitteeByKey(WEBCommitteeGetCommitteeByKey $parameters)
    {
      return $this->__soapCall('WEBCommitteeGetCommitteeByKey', array($parameters));
    }

    /**
     * Returns committee information.
     *
     * @param WEBCommitteeGet $parameters
     * @return WEBCommitteeGetResponse
     */
    public function WEBCommitteeGet(WEBCommitteeGet $parameters)
    {
      return $this->__soapCall('WEBCommitteeGet', array($parameters));
    }

    /**
     * Adds a committee nomination.
     *
     * @param WEBCommitteeNominationInsert $parameters
     * @return WEBCommitteeNominationInsertResponse
     */
    public function WEBCommitteeNominationInsert(WEBCommitteeNominationInsert $parameters)
    {
      return $this->__soapCall('WEBCommitteeNominationInsert', array($parameters));
    }

    /**
     * Retrieves position list by committee.
     *
     * @param WEBCommitteeGetPositionList $parameters
     * @return WEBCommitteeGetPositionListResponse
     */
    public function WEBCommitteeGetPositionList(WEBCommitteeGetPositionList $parameters)
    {
      return $this->__soapCall('WEBCommitteeGetPositionList', array($parameters));
    }

    /**
     * Searches the membership directory.
     *
     * @param WEBMemberDirectorySearch $parameters
     * @return WEBMemberDirectorySearchResponse
     */
    public function WEBMemberDirectorySearch(WEBMemberDirectorySearch $parameters)
    {
      return $this->__soapCall('WEBMemberDirectorySearch', array($parameters));
    }

    /**
     * Searches the membership directory.
     *
     * @param WEBMemberDirectoryOrganizationSearch $parameters
     * @return WEBMemberDirectoryOrganizationSearchResponse
     */
    public function WEBMemberDirectoryOrganizationSearch(WEBMemberDirectoryOrganizationSearch $parameters)
    {
      return $this->__soapCall('WEBMemberDirectoryOrganizationSearch', array($parameters));
    }

    /**
     * Retrieves list of chapters.
     *
     * @param WEBChaptersGetChapterList $parameters
     * @return WEBChaptersGetChapterListResponse
     */
    public function WEBChaptersGetChapterList(WEBChaptersGetChapterList $parameters)
    {
      return $this->__soapCall('WEBChaptersGetChapterList', array($parameters));
    }

    /**
     * Retrieves list of chapters by name.
     *
     * @param WEBChaptersGetChapterListByName $parameters
     * @return WEBChaptersGetChapterListByNameResponse
     */
    public function WEBChaptersGetChapterListByName(WEBChaptersGetChapterListByName $parameters)
    {
      return $this->__soapCall('WEBChaptersGetChapterListByName', array($parameters));
    }

    /**
     * Retrieves a chapter's roster.
     *
     * @param WEBChaptersGetChapterMembershipRoster $parameters
     * @return WEBChaptersGetChapterMembershipRosterResponse
     */
    public function WEBChaptersGetChapterMembershipRoster(WEBChaptersGetChapterMembershipRoster $parameters)
    {
      return $this->__soapCall('WEBChaptersGetChapterMembershipRoster', array($parameters));
    }

    /**
     * Retrieves a chapter's officers.
     *
     * @param WEBChaptersGetChapterOfficers $parameters
     * @return WEBChaptersGetChapterOfficersResponse
     */
    public function WEBChaptersGetChapterOfficers(WEBChaptersGetChapterOfficers $parameters)
    {
      return $this->__soapCall('WEBChaptersGetChapterOfficers', array($parameters));
    }

    /**
     * Returns chapter information.
     *
     * @param WEBChaptersGetChapterByKey $parameters
     * @return WEBChaptersGetChapterByKeyResponse
     */
    public function WEBChaptersGetChapterByKey(WEBChaptersGetChapterByKey $parameters)
    {
      return $this->__soapCall('WEBChaptersGetChapterByKey', array($parameters));
    }

    /**
     * Returns chapter information.
     *
     * @param WEBChapterGet $parameters
     * @return WEBChapterGetResponse
     */
    public function WEBChapterGet(WEBChapterGet $parameters)
    {
      return $this->__soapCall('WEBChapterGet', array($parameters));
    }

    /**
     * Saves a shopping cart order for an existing customer in mox table
     *
     * @param WEBCentralizedShoppingCartStoreForeWeb $parameters
     * @return WEBCentralizedShoppingCartStoreForeWebResponse
     */
    public function WEBCentralizedShoppingCartStoreForeWeb(WEBCentralizedShoppingCartStoreForeWeb $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartStoreForeWeb', array($parameters));
    }

    /**
     * Retrieves the most recent shopping cart order for a specific customer which was saved using WEBCentralizedShoppingCartStoreForeWeb method
     *
     * @param WEBCentralizedShoppingCartGetPendingOrder $parameters
     * @return WEBCentralizedShoppingCartGetPendingOrderResponse
     */
    public function WEBCentralizedShoppingCartGetPendingOrder(WEBCentralizedShoppingCartGetPendingOrder $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetPendingOrder', array($parameters));
    }

    /**
     * Returns a new shopping cart
     *
     * @param WEBCentralizedShoppingCartGetNew $parameters
     * @return WEBCentralizedShoppingCartGetNewResponse
     */
    public function WEBCentralizedShoppingCartGetNew(WEBCentralizedShoppingCartGetNew $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetNew', array($parameters));
    }

    /**
     * Returns a new shopping cart
     *
     * @param WEBCentralizedShoppingCartGet $parameters
     * @return WEBCentralizedShoppingCartGetResponse
     */
    public function WEBCentralizedShoppingCartGet(WEBCentralizedShoppingCartGet $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGet', array($parameters));
    }

    /**
     * Returns a new shopping cart, with a line item added
     *
     * @param WEBCentralizedShoppingCartGetWithLineItem $parameters
     * @return WEBCentralizedShoppingCartGetWithLineItemResponse
     */
    public function WEBCentralizedShoppingCartGetWithLineItem(WEBCentralizedShoppingCartGetWithLineItem $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetWithLineItem', array($parameters));
    }

    /**
     * Refresh Shopping cart
     *
     * @param WEBCentralizedShoppingCartRefresh $parameters
     * @return WEBCentralizedShoppingCartRefreshResponse
     */
    public function WEBCentralizedShoppingCartRefresh(WEBCentralizedShoppingCartRefresh $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartRefresh', array($parameters));
    }

    /**
     * Create Order and/or Invoice. To process a Paypal Express Checkout payment, be sure to set the payment method (pin_apm_key) accordingly and provide the token and payer id (pin_paypaltoken and pin_paypalpayerid respectively
     *
     * @param WEBCentralizedShoppingCartInsert $parameters
     * @return WEBCentralizedShoppingCartInsertResponse
     */
    public function WEBCentralizedShoppingCartInsert(WEBCentralizedShoppingCartInsert $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartInsert', array($parameters));
    }

    /**
     * Apply a source code to Centralized Shopping Cart
     *
     * @param WEBCentralizedShoppingCartApplySourceCode $parameters
     * @return WEBCentralizedShoppingCartApplySourceCodeResponse
     */
    public function WEBCentralizedShoppingCartApplySourceCode(WEBCentralizedShoppingCartApplySourceCode $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartApplySourceCode', array($parameters));
    }

    /**
     * Clears the source code that is applied to Centralized Shopping Cart
     *
     * @param WEBCentralizedShoppingCartClearSourceCode $parameters
     * @return WEBCentralizedShoppingCartClearSourceCodeResponse
     */
    public function WEBCentralizedShoppingCartClearSourceCode(WEBCentralizedShoppingCartClearSourceCode $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartClearSourceCode', array($parameters));
    }

    /**
     * Add a discount code to Centralized Shopping Cart
     *
     * @param WEBCentralizedShoppingCartApplyDiscountCode $parameters
     * @return WEBCentralizedShoppingCartApplyDiscountCodeResponse
     */
    public function WEBCentralizedShoppingCartApplyDiscountCode(WEBCentralizedShoppingCartApplyDiscountCode $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartApplyDiscountCode', array($parameters));
    }

    /**
     * Retrieves Product Type List
     *
     * @param WEBCentralizedShoppingCartGetProductTypeList $parameters
     * @return WEBCentralizedShoppingCartGetProductTypeListResponse
     */
    public function WEBCentralizedShoppingCartGetProductTypeList(WEBCentralizedShoppingCartGetProductTypeList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetProductTypeList', array($parameters));
    }

    /**
     * Retrieves Product Category List
     *
     * @param WEBCentralizedShoppingCartGetProductCategoryList $parameters
     * @return WEBCentralizedShoppingCartGetProductCategoryListResponse
     */
    public function WEBCentralizedShoppingCartGetProductCategoryList(WEBCentralizedShoppingCartGetProductCategoryList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetProductCategoryList', array($parameters));
    }

    /**
     * Retrieves Product List; includes price range
     *
     * @param WEBCentralizedShoppingCartGetProductList $parameters
     * @return WEBCentralizedShoppingCartGetProductListResponse
     */
    public function WEBCentralizedShoppingCartGetProductList(WEBCentralizedShoppingCartGetProductList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetProductList', array($parameters));
    }

    /**
     * Retrieves Product List by name; includes price range
     *
     * @param WEBCentralizedShoppingCartGetProductListByName $parameters
     * @return WEBCentralizedShoppingCartGetProductListByNameResponse
     */
    public function WEBCentralizedShoppingCartGetProductListByName(WEBCentralizedShoppingCartGetProductListByName $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetProductListByName', array($parameters));
    }

    /**
     * Retrieves Product Info; includes price range
     *
     * @param WEBCentralizedShoppingCartGetProductByKey $parameters
     * @return WEBCentralizedShoppingCartGetProductByKeyResponse
     */
    public function WEBCentralizedShoppingCartGetProductByKey(WEBCentralizedShoppingCartGetProductByKey $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetProductByKey', array($parameters));
    }

    /**
     * Retrieves List of product keys
     *
     * @param WEBCentralizedShoppingCartGetProductListKeys $parameters
     * @return WEBCentralizedShoppingCartGetProductListKeysResponse
     */
    public function WEBCentralizedShoppingCartGetProductListKeys(WEBCentralizedShoppingCartGetProductListKeys $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetProductListKeys', array($parameters));
    }

    /**
     * Retrieves list of all product complements
     *
     * @param WEBCentralizedShoppingCartGetProductComplements $parameters
     * @return WEBCentralizedShoppingCartGetProductComplementsResponse
     */
    public function WEBCentralizedShoppingCartGetProductComplements(WEBCentralizedShoppingCartGetProductComplements $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetProductComplements', array($parameters));
    }

    /**
     * Retrieves list of product complements for specified product
     *
     * @param WEBCentralizedShoppingCartGetProductComplementListByProductKey $parameters
     * @return WEBCentralizedShoppingCartGetProductComplementListByProductKeyResponse
     */
    public function WEBCentralizedShoppingCartGetProductComplementListByProductKey(WEBCentralizedShoppingCartGetProductComplementListByProductKey $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetProductComplementListByProductKey', array($parameters));
    }

    /**
     * Retrieves list of all product substitutes
     *
     * @param WEBCentralizedShoppingCartGetProductSubstitutes $parameters
     * @return WEBCentralizedShoppingCartGetProductSubstitutesResponse
     */
    public function WEBCentralizedShoppingCartGetProductSubstitutes(WEBCentralizedShoppingCartGetProductSubstitutes $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetProductSubstitutes', array($parameters));
    }

    /**
     * Retrieves list of product substitutes for specified product
     *
     * @param WEBCentralizedShoppingCartGetProductSubstituteListByProductKey $parameters
     * @return WEBCentralizedShoppingCartGetProductSubstituteListByProductKeyResponse
     */
    public function WEBCentralizedShoppingCartGetProductSubstituteListByProductKey(WEBCentralizedShoppingCartGetProductSubstituteListByProductKey $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetProductSubstituteListByProductKey', array($parameters));
    }

    /**
     * Retrieves list of all products also purchased
     *
     * @param WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKey $parameters
     * @return WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKeyResponse
     */
    public function WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKey(WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKey $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKey', array($parameters));
    }

    /**
     * Retrieves Merchandise List.
     *
     * @param WEBCentralizedShoppingCartGetMerchandiseList $parameters
     * @return WEBCentralizedShoppingCartGetMerchandiseListResponse
     */
    public function WEBCentralizedShoppingCartGetMerchandiseList(WEBCentralizedShoppingCartGetMerchandiseList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetMerchandiseList', array($parameters));
    }

    /**
     * Retrieves Merchandise List without Pricing Control Check.
     *
     * @param WEBCentralizedShoppingCartGetMerchandiseList_Ignore_PC $parameters
     * @return WEBCentralizedShoppingCartGetMerchandiseList_Ignore_PCResponse
     */
    public function WEBCentralizedShoppingCartGetMerchandiseList_Ignore_PC(WEBCentralizedShoppingCartGetMerchandiseList_Ignore_PC $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetMerchandiseList_Ignore_PC', array($parameters));
    }

    /**
     * Retrieves Publication List without Pricing Control Check.
     *
     * @param WEBCentralizedShoppingCartGetPublicationList_Ignore_PC $parameters
     * @return WEBCentralizedShoppingCartGetPublicationList_Ignore_PCResponse
     */
    public function WEBCentralizedShoppingCartGetPublicationList_Ignore_PC(WEBCentralizedShoppingCartGetPublicationList_Ignore_PC $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetPublicationList_Ignore_PC', array($parameters));
    }

    /**
     * Retrieves Subscription List without Pricing Control Check.
     *
     * @param WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PC $parameters
     * @return WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResponse
     */
    public function WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PC(WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PC $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PC', array($parameters));
    }

    /**
     * Retrieves Event List
     *
     * @param WEBCentralizedShoppingCartGetEventList $parameters
     * @return WEBCentralizedShoppingCartGetEventListResponse
     */
    public function WEBCentralizedShoppingCartGetEventList(WEBCentralizedShoppingCartGetEventList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetEventList', array($parameters));
    }

    /**
     * Retrieves Event List By Name
     *
     * @param WEBCentralizedShoppingCartGetEventListByName $parameters
     * @return WEBCentralizedShoppingCartGetEventListByNameResponse
     */
    public function WEBCentralizedShoppingCartGetEventListByName(WEBCentralizedShoppingCartGetEventListByName $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetEventListByName', array($parameters));
    }

    /**
     * Retrieves Event Info
     *
     * @param WEBCentralizedShoppingCartGetEventByKey $parameters
     * @return WEBCentralizedShoppingCartGetEventByKeyResponse
     */
    public function WEBCentralizedShoppingCartGetEventByKey(WEBCentralizedShoppingCartGetEventByKey $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetEventByKey', array($parameters));
    }

    /**
     * Retrieves List of event keys
     *
     * @param WEBCentralizedShoppingCartGetEventListKeys $parameters
     * @return WEBCentralizedShoppingCartGetEventListKeysResponse
     */
    public function WEBCentralizedShoppingCartGetEventListKeys(WEBCentralizedShoppingCartGetEventListKeys $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetEventListKeys', array($parameters));
    }

    /**
     * Retrieves Track list by event
     *
     * @param WEBCentralizedShoppingCartGetTrackListByEventKey $parameters
     * @return WEBCentralizedShoppingCartGetTrackListByEventKeyResponse
     */
    public function WEBCentralizedShoppingCartGetTrackListByEventKey(WEBCentralizedShoppingCartGetTrackListByEventKey $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetTrackListByEventKey', array($parameters));
    }

    /**
     * Retrieves Session list by event
     *
     * @param WEBCentralizedShoppingCartGetSessionListByEventKey $parameters
     * @return WEBCentralizedShoppingCartGetSessionListByEventKeyResponse
     */
    public function WEBCentralizedShoppingCartGetSessionListByEventKey(WEBCentralizedShoppingCartGetSessionListByEventKey $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetSessionListByEventKey', array($parameters));
    }

    /**
     * Retrieves faculty list by event
     *
     * @param WEBCentralizedShoppingCartGetFacultyListByEventKey $parameters
     * @return WEBCentralizedShoppingCartGetFacultyListByEventKeyResponse
     */
    public function WEBCentralizedShoppingCartGetFacultyListByEventKey(WEBCentralizedShoppingCartGetFacultyListByEventKey $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetFacultyListByEventKey', array($parameters));
    }

    /**
     * Retrieves faculty list by session
     *
     * @param WEBCentralizedShoppingCartGetFacultyListBySessionKey $parameters
     * @return WEBCentralizedShoppingCartGetFacultyListBySessionKeyResponse
     */
    public function WEBCentralizedShoppingCartGetFacultyListBySessionKey(WEBCentralizedShoppingCartGetFacultyListBySessionKey $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetFacultyListBySessionKey', array($parameters));
    }

    /**
     * Retrieves sponsor list by event
     *
     * @param WEBCentralizedShoppingCartGetSponsorListByEventKey $parameters
     * @return WEBCentralizedShoppingCartGetSponsorListByEventKeyResponse
     */
    public function WEBCentralizedShoppingCartGetSponsorListByEventKey(WEBCentralizedShoppingCartGetSponsorListByEventKey $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetSponsorListByEventKey', array($parameters));
    }

    /**
     * Retrieves sponsor list by session
     *
     * @param WEBCentralizedShoppingCartGetSponsorListBySessionKey $parameters
     * @return WEBCentralizedShoppingCartGetSponsorListBySessionKeyResponse
     */
    public function WEBCentralizedShoppingCartGetSponsorListBySessionKey(WEBCentralizedShoppingCartGetSponsorListBySessionKey $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetSponsorListBySessionKey', array($parameters));
    }

    /**
     * Retrieves Session Info by track
     *
     * @param WEBCentralizedShoppingCartGetSessionListByTrackKey $parameters
     * @return WEBCentralizedShoppingCartGetSessionListByTrackKeyResponse
     */
    public function WEBCentralizedShoppingCartGetSessionListByTrackKey(WEBCentralizedShoppingCartGetSessionListByTrackKey $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetSessionListByTrackKey', array($parameters));
    }

    /**
     * Retrieves All Event Registrant Type List
     *
     * @param WEBCentralizedShoppingCartGetEventRegistrantTypeList $parameters
     * @return WEBCentralizedShoppingCartGetEventRegistrantTypeListResponse
     */
    public function WEBCentralizedShoppingCartGetEventRegistrantTypeList(WEBCentralizedShoppingCartGetEventRegistrantTypeList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetEventRegistrantTypeList', array($parameters));
    }

    /**
     * Retrieves Housing Room Type List
     *
     * @param WEBCentralizedShoppingCartGetEventRegistrantRoomTypeList $parameters
     * @return WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResponse
     */
    public function WEBCentralizedShoppingCartGetEventRegistrantRoomTypeList(WEBCentralizedShoppingCartGetEventRegistrantRoomTypeList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetEventRegistrantRoomTypeList', array($parameters));
    }

    /**
     * Retrieves Event Registrant Type List by event
     *
     * @param WEBCentralizedShoppingCartGetEventRegistrantTypeListByEvent $parameters
     * @return WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResponse
     */
    public function WEBCentralizedShoppingCartGetEventRegistrantTypeListByEvent(WEBCentralizedShoppingCartGetEventRegistrantTypeListByEvent $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetEventRegistrantTypeListByEvent', array($parameters));
    }

    /**
     * Retrieves Event Guest Registrant Type List by event
     *
     * @param WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEvent $parameters
     * @return WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResponse
     */
    public function WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEvent(WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEvent $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEvent', array($parameters));
    }

    /**
     * Retrieves Event Registrant Type List
     *
     * @param WEBCentralizedShoppingCartGetEventRegistrantSourceCodeList $parameters
     * @return WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResponse
     */
    public function WEBCentralizedShoppingCartGetEventRegistrantSourceCodeList(WEBCentralizedShoppingCartGetEventRegistrantSourceCodeList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetEventRegistrantSourceCodeList', array($parameters));
    }

    /**
     * Retrieves Event Registrant Type List by event
     *
     * @param WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualList $parameters
     * @return WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResponse
     */
    public function WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualList(WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualList', array($parameters));
    }

    /**
     * Returns an existing event registration
     *
     * @param WEBCentralizedShoppingCartEventRegistrantGet $parameters
     * @return WEBCentralizedShoppingCartEventRegistrantGetResponse
     */
    public function WEBCentralizedShoppingCartEventRegistrantGet(WEBCentralizedShoppingCartEventRegistrantGet $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartEventRegistrantGet', array($parameters));
    }

    /**
     * Returns a new event registration object
     *
     * @param WEBCentralizedShoppingCartEventRegistrantGetNew $parameters
     * @return WEBCentralizedShoppingCartEventRegistrantGetNewResponse
     */
    public function WEBCentralizedShoppingCartEventRegistrantGetNew(WEBCentralizedShoppingCartEventRegistrantGetNew $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartEventRegistrantGetNew', array($parameters));
    }

    /**
     * Returns an existing group event registration
     *
     * @param WEBCentralizedShoppingCartEventRegistrantGroupGet $parameters
     * @return WEBCentralizedShoppingCartEventRegistrantGroupGetResponse
     */
    public function WEBCentralizedShoppingCartEventRegistrantGroupGet(WEBCentralizedShoppingCartEventRegistrantGroupGet $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartEventRegistrantGroupGet', array($parameters));
    }

    /**
     * Returns a new group event registration object
     *
     * @param WEBCentralizedShoppingCartEventRegistrantGroupGetNew $parameters
     * @return WEBCentralizedShoppingCartEventRegistrantGroupGetNewResponse
     */
    public function WEBCentralizedShoppingCartEventRegistrantGroupGetNew(WEBCentralizedShoppingCartEventRegistrantGroupGetNew $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartEventRegistrantGroupGetNew', array($parameters));
    }

    /**
     * Retrieves Event Registration Fees.
     *
     * @param WEBCentralizedShoppingCartGetEventFees $parameters
     * @return WEBCentralizedShoppingCartGetEventFeesResponse
     */
    public function WEBCentralizedShoppingCartGetEventFees(WEBCentralizedShoppingCartGetEventFees $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetEventFees', array($parameters));
    }

    /**
     * Retrieves Event Track Registration Fees.
     *
     * @param WEBCentralizedShoppingCartGetEventTrackFees $parameters
     * @return WEBCentralizedShoppingCartGetEventTrackFeesResponse
     */
    public function WEBCentralizedShoppingCartGetEventTrackFees(WEBCentralizedShoppingCartGetEventTrackFees $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetEventTrackFees', array($parameters));
    }

    /**
     * Retrieves Event Session Registration Fees.
     *
     * @param WEBCentralizedShoppingCartGetEventSessionFees $parameters
     * @return WEBCentralizedShoppingCartGetEventSessionFeesResponse
     */
    public function WEBCentralizedShoppingCartGetEventSessionFees(WEBCentralizedShoppingCartGetEventSessionFees $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGetEventSessionFees', array($parameters));
    }

    /**
     * Adds/Removes Event Registration Fees. Use this method before the registration object is added to Centralized Order Entry
     *
     * @param WEBCentralizedShoppingCartEventRegistrantSetLineItems $parameters
     * @return WEBCentralizedShoppingCartEventRegistrantSetLineItemsResponse
     */
    public function WEBCentralizedShoppingCartEventRegistrantSetLineItems(WEBCentralizedShoppingCartEventRegistrantSetLineItems $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartEventRegistrantSetLineItems', array($parameters));
    }

    /**
     * Adds/Removes Event Registration Fees. Use this method after the registration object is added to Centralized Order Entry
     *
     * @param WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCart $parameters
     * @return WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCartResponse
     */
    public function WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCart(WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCart $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCart', array($parameters));
    }

    /**
     * Adds/Removes Event Registration Fees. Use this method after the registration object is added to Centralized Order Entry
     *
     * @param WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistration $parameters
     * @return WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistrationResponse
     */
    public function WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistration(WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistration $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithGroupRegistration', array($parameters));
    }

    /**
     * Refreshes the registration object
     *
     * @param WEBCentralizedShoppingCartEventRegistrantRefresh $parameters
     * @return WEBCentralizedShoppingCartEventRegistrantRefreshResponse
     */
    public function WEBCentralizedShoppingCartEventRegistrantRefresh(WEBCentralizedShoppingCartEventRegistrantRefresh $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartEventRegistrantRefresh', array($parameters));
    }

    /**
     * Add a registrant to the shopping cart.
     *
     * @param WEBCentralizedShoppingCartAddEventRegistrant $parameters
     * @return WEBCentralizedShoppingCartAddEventRegistrantResponse
     */
    public function WEBCentralizedShoppingCartAddEventRegistrant(WEBCentralizedShoppingCartAddEventRegistrant $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartAddEventRegistrant', array($parameters));
    }

    /**
     * Remove a registrant from the shopping cart.
     *
     * @param WEBCentralizedShoppingCartRemoveEventRegistrant $parameters
     * @return WEBCentralizedShoppingCartRemoveEventRegistrantResponse
     */
    public function WEBCentralizedShoppingCartRemoveEventRegistrant(WEBCentralizedShoppingCartRemoveEventRegistrant $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartRemoveEventRegistrant', array($parameters));
    }

    /**
     * Refreshes the group registration object
     *
     * @param WEBCentralizedShoppingCartEventRegistrantGroupRefresh $parameters
     * @return WEBCentralizedShoppingCartEventRegistrantGroupRefreshResponse
     */
    public function WEBCentralizedShoppingCartEventRegistrantGroupRefresh(WEBCentralizedShoppingCartEventRegistrantGroupRefresh $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartEventRegistrantGroupRefresh', array($parameters));
    }

    /**
     * Add an individual registration to a group registration.
     *
     * @param WEBCentralizedShoppingCartAddEventRegistrantToGroup $parameters
     * @return WEBCentralizedShoppingCartAddEventRegistrantToGroupResponse
     */
    public function WEBCentralizedShoppingCartAddEventRegistrantToGroup(WEBCentralizedShoppingCartAddEventRegistrantToGroup $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartAddEventRegistrantToGroup', array($parameters));
    }

    /**
     * Remove a registration from a group registration.
     *
     * @param WEBCentralizedShoppingCartRemoveEventRegistrantFromGroup $parameters
     * @return WEBCentralizedShoppingCartRemoveEventRegistrantFromGroupResponse
     */
    public function WEBCentralizedShoppingCartRemoveEventRegistrantFromGroup(WEBCentralizedShoppingCartRemoveEventRegistrantFromGroup $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartRemoveEventRegistrantFromGroup', array($parameters));
    }

    /**
     * Add a group registration to the shopping cart.
     *
     * @param WEBCentralizedShoppingCartAddEventRegistrantGroup $parameters
     * @return WEBCentralizedShoppingCartAddEventRegistrantGroupResponse
     */
    public function WEBCentralizedShoppingCartAddEventRegistrantGroup(WEBCentralizedShoppingCartAddEventRegistrantGroup $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartAddEventRegistrantGroup', array($parameters));
    }

    /**
     * Remove a group registration from the shopping cart.
     *
     * @param WEBCentralizedShoppingCartRemoveEventRegistrantGroup $parameters
     * @return WEBCentralizedShoppingCartRemoveEventRegistrantGroupResponse
     */
    public function WEBCentralizedShoppingCartRemoveEventRegistrantGroup(WEBCentralizedShoppingCartRemoveEventRegistrantGroup $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartRemoveEventRegistrantGroup', array($parameters));
    }

    /**
     * Retrieves Gift List
     *
     * @param WEBCentralizedShoppingCartGiftGetGiftProductList $parameters
     * @return WEBCentralizedShoppingCartGiftGetGiftProductListResponse
     */
    public function WEBCentralizedShoppingCartGiftGetGiftProductList(WEBCentralizedShoppingCartGiftGetGiftProductList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGiftGetGiftProductList', array($parameters));
    }

    /**
     * Retrieves Gift List By Name
     *
     * @param WEBCentralizedShoppingCartGiftGetGiftProductListByName $parameters
     * @return WEBCentralizedShoppingCartGiftGetGiftProductListByNameResponse
     */
    public function WEBCentralizedShoppingCartGiftGetGiftProductListByName(WEBCentralizedShoppingCartGiftGetGiftProductListByName $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGiftGetGiftProductListByName', array($parameters));
    }

    /**
     * Retrieves Gift Info (use prc_key for GiftKey)
     *
     * @param WEBCentralizedShoppingCartGiftGetGiftProductByKey $parameters
     * @return WEBCentralizedShoppingCartGiftGetGiftProductByKeyResponse
     */
    public function WEBCentralizedShoppingCartGiftGetGiftProductByKey(WEBCentralizedShoppingCartGiftGetGiftProductByKey $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGiftGetGiftProductByKey', array($parameters));
    }

    /**
     * Retrieves List of gift keys
     *
     * @param WEBCentralizedShoppingCartGiftGetGiftProductListKeys $parameters
     * @return WEBCentralizedShoppingCartGiftGetGiftProductListKeysResponse
     */
    public function WEBCentralizedShoppingCartGiftGetGiftProductListKeys(WEBCentralizedShoppingCartGiftGetGiftProductListKeys $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGiftGetGiftProductListKeys', array($parameters));
    }

    /**
     * Retrieves All Gift Type List (selected value should be set to gft_gty_key)
     *
     * @param WEBCentralizedShoppingCartGiftGetGiftTypeList $parameters
     * @return WEBCentralizedShoppingCartGiftGetGiftTypeListResponse
     */
    public function WEBCentralizedShoppingCartGiftGetGiftTypeList(WEBCentralizedShoppingCartGiftGetGiftTypeList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGiftGetGiftTypeList', array($parameters));
    }

    /**
     * Retrieves Gift Premium Products (should set selected value to gft_prm_prc_key)
     *
     * @param WEBCentralizedShoppingCartGiftGetPremiumProductsListByGift $parameters
     * @return WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResponse
     */
    public function WEBCentralizedShoppingCartGiftGetPremiumProductsListByGift(WEBCentralizedShoppingCartGiftGetPremiumProductsListByGift $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGiftGetPremiumProductsListByGift', array($parameters));
    }

    /**
     * Returns an existing fundraising gift
     *
     * @param WEBCentralizedShoppingCartGiftFundraisingGet $parameters
     * @return WEBCentralizedShoppingCartGiftFundraisingGetResponse
     */
    public function WEBCentralizedShoppingCartGiftFundraisingGet(WEBCentralizedShoppingCartGiftFundraisingGet $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGiftFundraisingGet', array($parameters));
    }

    /**
     * Returns a new fundraising gift object
     *
     * @param WEBCentralizedShoppingCartGiftFundraisingGetNew $parameters
     * @return WEBCentralizedShoppingCartGiftFundraisingGetNewResponse
     */
    public function WEBCentralizedShoppingCartGiftFundraisingGetNew(WEBCentralizedShoppingCartGiftFundraisingGetNew $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGiftFundraisingGetNew', array($parameters));
    }

    /**
     * Adds/Removes Gift line items. Use this method before the Fundraising Gift object is added to Centralized Order Entry
     *
     * @param WEBCentralizedShoppingCartGiftFundraisingSetLineItems $parameters
     * @return WEBCentralizedShoppingCartGiftFundraisingSetLineItemsResponse
     */
    public function WEBCentralizedShoppingCartGiftFundraisingSetLineItems(WEBCentralizedShoppingCartGiftFundraisingSetLineItems $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGiftFundraisingSetLineItems', array($parameters));
    }

    /**
     * Adds/Removes Gift line items. Use this method after the fundraising gift is added to Centralized Order Entry
     *
     * @param WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCart $parameters
     * @return WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCartResponse
     */
    public function WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCart(WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCart $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGiftFundraisingSetLineItemsWithCart', array($parameters));
    }

    /**
     * Refreshes the fundraising gift object
     *
     * @param WEBCentralizedShoppingCartGiftFundraisingRefresh $parameters
     * @return WEBCentralizedShoppingCartGiftFundraisingRefreshResponse
     */
    public function WEBCentralizedShoppingCartGiftFundraisingRefresh(WEBCentralizedShoppingCartGiftFundraisingRefresh $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGiftFundraisingRefresh', array($parameters));
    }

    /**
     * Add a fundraising gift to the shopping cart.
     *
     * @param WEBCentralizedShoppingCartGiftAddGiftFundraising $parameters
     * @return WEBCentralizedShoppingCartGiftAddGiftFundraisingResponse
     */
    public function WEBCentralizedShoppingCartGiftAddGiftFundraising(WEBCentralizedShoppingCartGiftAddGiftFundraising $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGiftAddGiftFundraising', array($parameters));
    }

    /**
     * Remove a fundraising gift from the shopping cart.
     *
     * @param WEBCentralizedShoppingCartGiftRemoveFundraisingGift $parameters
     * @return WEBCentralizedShoppingCartGiftRemoveFundraisingGiftResponse
     */
    public function WEBCentralizedShoppingCartGiftRemoveFundraisingGift(WEBCentralizedShoppingCartGiftRemoveFundraisingGift $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartGiftRemoveFundraisingGift', array($parameters));
    }

    /**
     * Retrieves Memembership Package List; the package product key mbr_pak_prd_key can be set in the membership object with the value of asp_pak_prd_key returned in this list.
     *
     * @param WEBCentralizedShoppingCartMembershipGetPackageList $parameters
     * @return WEBCentralizedShoppingCartMembershipGetPackageListResponse
     */
    public function WEBCentralizedShoppingCartMembershipGetPackageList(WEBCentralizedShoppingCartMembershipGetPackageList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartMembershipGetPackageList', array($parameters));
    }

    /**
     * Retrieves Memembership Package List; the package product key mbr_pak_prd_key can be set in the membership object with the value of asp_pak_prd_key returned in this list.
     *
     * @param WEBCentralizedShoppingCartMembershipGetRenewalPackageList $parameters
     * @return WEBCentralizedShoppingCartMembershipGetRenewalPackageListResponse
     */
    public function WEBCentralizedShoppingCartMembershipGetRenewalPackageList(WEBCentralizedShoppingCartMembershipGetRenewalPackageList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartMembershipGetRenewalPackageList', array($parameters));
    }

    /**
     * Retrieves Memembership Type List.
     *
     * @param WEBCentralizedShoppingCartMembershipGetMembershipTypeList $parameters
     * @return WEBCentralizedShoppingCartMembershipGetMembershipTypeListResponse
     */
    public function WEBCentralizedShoppingCartMembershipGetMembershipTypeList(WEBCentralizedShoppingCartMembershipGetMembershipTypeList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartMembershipGetMembershipTypeList', array($parameters));
    }

    /**
     * Retrieves Membership Package List By Name
     *
     * @param WEBCentralizedShoppingCartMembershipGetPackageListByName $parameters
     * @return WEBCentralizedShoppingCartMembershipGetPackageListByNameResponse
     */
    public function WEBCentralizedShoppingCartMembershipGetPackageListByName(WEBCentralizedShoppingCartMembershipGetPackageListByName $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartMembershipGetPackageListByName', array($parameters));
    }

    /**
     * Retrieves Membership Package List By Membership Type
     *
     * @param WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKey $parameters
     * @return WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResponse
     */
    public function WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKey(WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKey $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKey', array($parameters));
    }

    /**
     * Retrieves Package Info (use value of asp_pak_prd_key for PackageKey)
     *
     * @param WEBCentralizedShoppingCartMembershipGetPackageByKey $parameters
     * @return WEBCentralizedShoppingCartMembershipGetPackageByKeyResponse
     */
    public function WEBCentralizedShoppingCartMembershipGetPackageByKey(WEBCentralizedShoppingCartMembershipGetPackageByKey $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartMembershipGetPackageByKey', array($parameters));
    }

    /**
     * Retrieves List of package keys
     *
     * @param WEBCentralizedShoppingCartMembershipGetPackageListKeys $parameters
     * @return WEBCentralizedShoppingCartMembershipGetPackageListKeysResponse
     */
    public function WEBCentralizedShoppingCartMembershipGetPackageListKeys(WEBCentralizedShoppingCartMembershipGetPackageListKeys $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartMembershipGetPackageListKeys', array($parameters));
    }

    /**
     * Retrieves List of package components
     *
     * @param WEBCentralizedShoppingCartMembershipGetPackageComponentList $parameters
     * @return WEBCentralizedShoppingCartMembershipGetPackageComponentListResponse
     */
    public function WEBCentralizedShoppingCartMembershipGetPackageComponentList(WEBCentralizedShoppingCartMembershipGetPackageComponentList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartMembershipGetPackageComponentList', array($parameters));
    }

    /**
     * Retrieves List of package components with custom prices from the membership object; mbr_pak_prd_key and mbr_cst_key must both be set.
     *
     * @param WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObject $parameters
     * @return WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObjectResponse
     */
    public function WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObject(WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObject $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartMembershipGetPackageComponentListFromObject', array($parameters));
    }

    /**
     * Returns an existing membership object
     *
     * @param WEBCentralizedShoppingCartMembershipGet $parameters
     * @return WEBCentralizedShoppingCartMembershipGetResponse
     */
    public function WEBCentralizedShoppingCartMembershipGet(WEBCentralizedShoppingCartMembershipGet $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartMembershipGet', array($parameters));
    }

    /**
     * Returns a new membership object
     *
     * @param WEBCentralizedShoppingCartMembershipGetNew $parameters
     * @return WEBCentralizedShoppingCartMembershipGetNewResponse
     */
    public function WEBCentralizedShoppingCartMembershipGetNew(WEBCentralizedShoppingCartMembershipGetNew $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartMembershipGetNew', array($parameters));
    }

    /**
     * Adds/Removes membership package line items. Use this method before the membership object is added to Centralized Order Entry
     *
     * @param WEBCentralizedShoppingCartMembesrhipSetLineItems $parameters
     * @return WEBCentralizedShoppingCartMembesrhipSetLineItemsResponse
     */
    public function WEBCentralizedShoppingCartMembesrhipSetLineItems(WEBCentralizedShoppingCartMembesrhipSetLineItems $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartMembesrhipSetLineItems', array($parameters));
    }

    /**
     * Adds/Removes membership pacakge line items. Use this method after the membership object is added to Centralized Order Entry
     *
     * @param WEBCentralizedShoppingCartMembershipSetLineItemsWithCart $parameters
     * @return WEBCentralizedShoppingCartMembershipSetLineItemsWithCartResponse
     */
    public function WEBCentralizedShoppingCartMembershipSetLineItemsWithCart(WEBCentralizedShoppingCartMembershipSetLineItemsWithCart $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartMembershipSetLineItemsWithCart', array($parameters));
    }

    /**
     * Adds an open invoice and returns the object
     *
     * @param WEBCentralizedShoppingCartOpenInvoiceAdd $parameters
     * @return WEBCentralizedShoppingCartOpenInvoiceAddResponse
     */
    public function WEBCentralizedShoppingCartOpenInvoiceAdd(WEBCentralizedShoppingCartOpenInvoiceAdd $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartOpenInvoiceAdd', array($parameters));
    }

    /**
     * Returns an existing open invoice
     *
     * @param WEBCentralizedShoppingCartOpenInvoiceGet $parameters
     * @return WEBCentralizedShoppingCartOpenInvoiceGetResponse
     */
    public function WEBCentralizedShoppingCartOpenInvoiceGet(WEBCentralizedShoppingCartOpenInvoiceGet $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartOpenInvoiceGet', array($parameters));
    }

    /**
     * Retrieves List of open invoices -  invoice date, invoice ID, total, amt paid, balance, amt due
     *
     * @param WEBCentralizedShoppingCartOpenInvoiceGetList $parameters
     * @return WEBCentralizedShoppingCartOpenInvoiceGetListResponse
     */
    public function WEBCentralizedShoppingCartOpenInvoiceGetList(WEBCentralizedShoppingCartOpenInvoiceGetList $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartOpenInvoiceGetList', array($parameters));
    }

    /**
     * Refreshes the membership object
     *
     * @param WEBCentralizedShoppingCartMembershipRefresh $parameters
     * @return WEBCentralizedShoppingCartMembershipRefreshResponse
     */
    public function WEBCentralizedShoppingCartMembershipRefresh(WEBCentralizedShoppingCartMembershipRefresh $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartMembershipRefresh', array($parameters));
    }

    /**
     * Add a membership to the shopping cart.
     *
     * @param WEBCentralizedShoppingCartMembershipAddMembership $parameters
     * @return WEBCentralizedShoppingCartMembershipAddMembershipResponse
     */
    public function WEBCentralizedShoppingCartMembershipAddMembership(WEBCentralizedShoppingCartMembershipAddMembership $parameters)
    {
      return $this->__soapCall('WEBCentralizedShoppingCartMembershipAddMembership', array($parameters));
    }

    /**
     * Passing correct credentials to this method will return an authentication token - without an authentication token, the rest of the xWeb web methods will be inoperable.  The authentication token is governed by the group privileges assigned to the account invoking the xWeb web methods.  Please consult with the administrator of the netForum database to ensure your level of authorization.  This method MUST be the first method invoked at xWeb, as the authentication token that is returned is required for every xWeb method.
     *
     * @param Authenticate $parameters
     * @return AuthenticateResponse
     */
    public function Authenticate(Authenticate $parameters)
    {
      return $this->__soapCall('Authenticate', array($parameters));
    }

    /**
     * Passing correct credentials to this method will return an authentication token - without an authentication token, the rest of the xWeb web methods will be inoperable.  The authentication token is governed by the group privileges assigned to the account invoking the xWeb web methods.  Please consult with the administrator of the netForum database to ensure your level of authorization.  This method MUST be the first method invoked at xWeb, as the authentication token that is returned is required for every xWeb method.
     *
     * @param AuthenticateLdap $parameters
     * @return AuthenticateLdapResponse
     */
    public function AuthenticateLdap(AuthenticateLdap $parameters)
    {
      return $this->__soapCall('AuthenticateLdap', array($parameters));
    }

    /**
     * Provides an authentication token for a valid customer login and password.  The value for the login field depends on a netForum Systems Option ("useEmailForAuthorization"), so please check with the netforum administrator for the setting.  If there are more than one record with the exact same login and password combination, then the service will return the first match.  The keyOverride parameter is an implementation specific parameter (web.config change) to force xWeb to return the customer key (cst_key) for a valid credential.
     *
     * @param WebLogin $parameters
     * @return WebLoginResponse
     */
    public function WebLogin(WebLogin $parameters)
    {
      return $this->__soapCall('WebLogin', array($parameters));
    }

    /**
     * Provides the customer key (cst_key) for the authenticated token that is passed as a parameter - this method can be invoked after a successful WebLogin() method returns an authentication token.  The authentication token is the only parameter for this method.
     *
     * @param WebValidate $parameters
     * @return WebValidateResponse
     */
    public function WebValidate(WebValidate $parameters)
    {
      return $this->__soapCall('WebValidate', array($parameters));
    }

    /**
     * Forces the authentication token to be destroyed - this method is the companion method to the WebLogin() method or the WebValidate() method.  xWeb doesn't return an Xml Node for success.
     *
     * @param WebLogout $parameters
     * @return WebLogoutResponse
     */
    public function WebLogout(WebLogout $parameters)
    {
      return $this->__soapCall('WebLogout', array($parameters));
    }

    /**
     * Provides a means of retrieving single or multiple data records and returns either the specific columns (pruning the number of elements is governed by how the netforum facade object was constructed, for example, the one-to-many relationships cannot be rendered in a flat-view - in these cases, no value will be returned for the impacted elements) or the complete Facade object record (returning all the fields associated with a netForum Facade object can be done by passing a * as the sole value for the szColumnList value).
     *
     * @param GetQuery $parameters
     * @return GetQueryResponse
     */
    public function GetQuery(GetQuery $parameters)
    {
      return $this->__soapCall('GetQuery', array($parameters));
    }

    /**
     * Retrieve data from a query that was built in the netForum iWeb Run Query form.
     *
     * @param GetDynamicQuery $parameters
     * @return GetDynamicQueryResponse
     */
    public function GetDynamicQuery(GetDynamicQuery $parameters)
    {
      return $this->__soapCall('GetDynamicQuery', array($parameters));
    }

    /**
     * Retrieve definition of an iWeb Dynamic Query
     *
     * @param GetDynamicQueryDefinition $parameters
     * @return GetDynamicQueryDefinitionResponse
     */
    public function GetDynamicQueryDefinition(GetDynamicQueryDefinition $parameters)
    {
      return $this->__soapCall('GetDynamicQueryDefinition', array($parameters));
    }

    /**
     * Retrieves list of all audiences that a user can run.
     *
     * @param GetAudienceList $parameters
     * @return GetAudienceListResponse
     */
    public function GetAudienceList(GetAudienceList $parameters)
    {
      return $this->__soapCall('GetAudienceList', array($parameters));
    }

    /**
     * Retrieve data from an audience that was built in the netForum iWeb Audience form.
     *
     * @param GetAudience $parameters
     * @return GetAudienceResponse
     */
    public function GetAudience(GetAudience $parameters)
    {
      return $this->__soapCall('GetAudience', array($parameters));
    }

    /**
     * Retrieve definition of an iWeb Audience.
     *
     * @param GetAudienceDefinition $parameters
     * @return GetAudienceDefinitionResponse
     */
    public function GetAudienceDefinition(GetAudienceDefinition $parameters)
    {
      return $this->__soapCall('GetAudienceDefinition', array($parameters));
    }

    /**
     * Returns XmlNode with List Table definition for a given netFORUM Object.  This information is provided for documentation for using the GetQuery web method.
     *
     * @param GetQueryDefinition $parameters
     * @return GetQueryDefinitionResponse
     */
    public function GetQueryDefinition(GetQueryDefinition $parameters)
    {
      return $this->__soapCall('GetQueryDefinition', array($parameters));
    }

    /**
     * Provides a W3C XML Schema for any netFORUM Facade object.  The only parameter for this method is a valid ObjectName - the textual display name of the object for which the Xml Schema document is being requested (individual, address, etc.).  The Xml Schema document that is returned contains the data relationships, the datatypes, and the documentation for each element and group.  In short, this method returns a complete snapshot of the netForum Facade object.
     *
     * @param GetFacadeXMLSchema $parameters
     * @return GetFacadeXMLSchemaResponse
     */
    public function GetFacadeXMLSchema(GetFacadeXMLSchema $parameters)
    {
      return $this->__soapCall('GetFacadeXMLSchema', array($parameters));
    }

    /**
     * Retrieves the complete list of netForum Facade Objects that can be invoked.
     *
     * @param GetFacadeObjectList $parameters
     * @return GetFacadeObjectListResponse
     */
    public function GetFacadeObjectList(GetFacadeObjectList $parameters)
    {
      return $this->__soapCall('GetFacadeObjectList', array($parameters));
    }

    /**
     * Returns all the fields and related data for the requested object.  This method is marked for removal - the GetQuery() method returns same functionality.
     *
     * @param GetFacadeObject $parameters
     * @return GetFacadeObjectResponse
     */
    public function GetFacadeObject(GetFacadeObject $parameters)
    {
      return $this->__soapCall('GetFacadeObject', array($parameters));
    }

    /**
     * Updates a netForum Facade object and returns the modified netForum Facade object record. Parameters: szObjectName, szObjectKey, oNode.  szObjectName - the textual display name of the object for which the schema information is being Requested (individual, address, etc.).  szObjectKey - the GUID representing an object's unique identity (primary key).  oNode - an XML node containing elements that correspond to the Facade object's fields used to update the netForum data store.
     *
     * @param UpdateFacadeObject $parameters
     * @return UpdateFacadeObjectResponse
     */
    public function UpdateFacadeObject(UpdateFacadeObject $parameters)
    {
      return $this->__soapCall('UpdateFacadeObject', array($parameters));
    }

    /**
     * Inserts a netForum Facade object and if successful returns the primary key of the Facade object record that was created.
     *
     * @param InsertFacadeObject $parameters
     * @return InsertFacadeObjectResponse
     */
    public function InsertFacadeObject(InsertFacadeObject $parameters)
    {
      return $this->__soapCall('InsertFacadeObject', array($parameters));
    }

    /**
     * Returns basic contact information for the individual.
     *
     * @param GetIndividualInformation $parameters
     * @return GetIndividualInformationResponse
     */
    public function GetIndividualInformation(GetIndividualInformation $parameters)
    {
      return $this->__soapCall('GetIndividualInformation', array($parameters));
    }

    /**
     * Updates basic contact information for the individual.
     *
     * @param SetIndividualInformation $parameters
     * @return SetIndividualInformationResponse
     */
    public function SetIndividualInformation(SetIndividualInformation $parameters)
    {
      return $this->__soapCall('SetIndividualInformation', array($parameters));
    }

    /**
     * Creates basic contact information for the individual.
     *
     * @param NewIndividualInformation $parameters
     * @return NewIndividualInformationResponse
     */
    public function NewIndividualInformation(NewIndividualInformation $parameters)
    {
      return $this->__soapCall('NewIndividualInformation', array($parameters));
    }

    /**
     * Returns basic contact information for the organization.
     *
     * @param GetOrganizationInformation $parameters
     * @return GetOrganizationInformationResponse
     */
    public function GetOrganizationInformation(GetOrganizationInformation $parameters)
    {
      return $this->__soapCall('GetOrganizationInformation', array($parameters));
    }

    /**
     * Updates basic contact information for the organization.
     *
     * @param SetOrganizationInformation $parameters
     * @return SetOrganizationInformationResponse
     */
    public function SetOrganizationInformation(SetOrganizationInformation $parameters)
    {
      return $this->__soapCall('SetOrganizationInformation', array($parameters));
    }

    /**
     * Creates basic contact information for the organization.
     *
     * @param NewOrganizationInformation $parameters
     * @return NewOrganizationInformationResponse
     */
    public function NewOrganizationInformation(NewOrganizationInformation $parameters)
    {
      return $this->__soapCall('NewOrganizationInformation', array($parameters));
    }

    /**
     * Returns the inserted key(s) for the invoice.  Parameters: XmlNode or 3DES encrypted string.
     *
     * @param CreateInvoice $parameters
     * @return CreateInvoiceResponse
     */
    public function CreateInvoice(CreateInvoice $parameters)
    {
      return $this->__soapCall('CreateInvoice', array($parameters));
    }

    /**
     * Returns the inserted key(s) for the payment.  Parameters: XmlNode or 3DES encrypted string.
     *
     * @param CreatePayment $parameters
     * @return CreatePaymentResponse
     */
    public function CreatePayment(CreatePayment $parameters)
    {
      return $this->__soapCall('CreatePayment', array($parameters));
    }

    /**
     * Inserts advocacy data for US Representatives and US Senators.  This method can only create content and cannot update content.
     *
     * @param CreateAdvocacyData $parameters
     * @return CreateAdvocacyDataResponse
     */
    public function CreateAdvocacyData(CreateAdvocacyData $parameters)
    {
      return $this->__soapCall('CreateAdvocacyData', array($parameters));
    }

    /**
     * Return a list of available action types.
     *
     * @param GetActionTypeList $parameters
     * @return GetActionTypeListResponse
     */
    public function GetActionTypeList(GetActionTypeList $parameters)
    {
      return $this->__soapCall('GetActionTypeList', array($parameters));
    }

    /**
     * Return a list of action sub types for a specified action type.
     *
     * @param GetActionSubTypeList $parameters
     * @return GetActionSubTypeListResponse
     */
    public function GetActionSubTypeList(GetActionSubTypeList $parameters)
    {
      return $this->__soapCall('GetActionSubTypeList', array($parameters));
    }

    /**
     * Insert an action record for a customer.
     *
     * @param InsertCustomerAction $parameters
     * @return InsertCustomerActionResponse
     */
    public function InsertCustomerAction(InsertCustomerAction $parameters)
    {
      return $this->__soapCall('InsertCustomerAction', array($parameters));
    }

    /**
     * Create a new action sub type for a specified action type.
     *
     * @param InsertActionSubType $parameters
     * @return InsertActionSubTypeResponse
     */
    public function InsertActionSubType(InsertActionSubType $parameters)
    {
      return $this->__soapCall('InsertActionSubType', array($parameters));
    }

    /**
     * Return a list of cst of the IP.
     *
     * @param ElectronicSubscriptionGetCstListByIP $parameters
     * @return ElectronicSubscriptionGetCstListByIPResponse
     */
    public function ElectronicSubscriptionGetCustomerListByIP(ElectronicSubscriptionGetCstListByIP $parameters)
    {
      return $this->__soapCall('ElectronicSubscriptionGetCustomerListByIP', array($parameters));
    }

    /**
     * Return a list of Subscriptions of Customer.
     *
     * @param ElectronicSubscriptionGetPurchasedSubscriptionsByCustomer $parameters
     * @return ElectronicSubscriptionGetPurchasedSubscriptionsByCustomerResponse
     */
    public function ElectronicSubscriptionGetPurchasedSubscriptionsByCustomer(ElectronicSubscriptionGetPurchasedSubscriptionsByCustomer $parameters)
    {
      return $this->__soapCall('ElectronicSubscriptionGetPurchasedSubscriptionsByCustomer', array($parameters));
    }

    /**
     * @param ExecuteMethod $parameters
     * @return ExecuteMethodResponse
     */
    public function ExecuteMethod(ExecuteMethod $parameters)
    {
      return $this->__soapCall('ExecuteMethod', array($parameters));
    }

    /**
     * Provides version  information about the xWeb application and Database Server and Name.
     *
     * @param GetVersion $parameters
     * @return GetVersionResponse
     */
    public function GetVersion(GetVersion $parameters)
    {
      return $this->__soapCall('GetVersion', array($parameters));
    }

    /**
     * Tests the applications connection to the database.
     *
     * @param TestConnection $parameters
     * @return TestConnectionResponse
     */
    public function TestConnection(TestConnection $parameters)
    {
      return $this->__soapCall('TestConnection', array($parameters));
    }

    /**
     * Get a list of all the time zones defined in netFORUM.
     *
     * @param GetTimeZones $parameters
     * @return GetTimeZonesResponse
     */
    public function GetTimeZones(GetTimeZones $parameters)
    {
      return $this->__soapCall('GetTimeZones', array($parameters));
    }

    /**
     * Merges one or more customers in the customersToMerge list with the customer designated by the cst_key_keep parameter. The method returns a MergeCustomersResult containing detailed information about the results of the merger of each customer and module.
     *
     * @param MergeCustomers $parameters
     * @return MergeCustomersResponse
     */
    public function MergeCustomers(MergeCustomers $parameters)
    {
      return $this->__soapCall('MergeCustomers', array($parameters));
    }

    /**
     * Returns a list of Module groups for the merging of customers.
     *
     * @param GetMergeCustomerModules $parameters
     * @return GetMergeCustomerModulesResponse
     */
    public function GetMergeCustomerModules(GetMergeCustomerModules $parameters)
    {
      return $this->__soapCall('GetMergeCustomerModules', array($parameters));
    }

    /**
     * Retrieves WEB integration relevent system options.
     *
     * @param WEBGetSystemOptions $parameters
     * @return WEBGetSystemOptionsResponse
     */
    public function WEBGetSystemOptions(WEBGetSystemOptions $parameters)
    {
      return $this->__soapCall('WEBGetSystemOptions', array($parameters));
    }

    /**
     * Updates a netForum System Option and returns the modified netForum Facade object record. Parameters: szOptionName, szOptionValue. Returns true if successful, false otherwise.
     *
     * @param WEBUpdateSystemOption $parameters
     * @return WEBUpdateSystemOptionResponse
     */
    public function WEBUpdateSystemOption(WEBUpdateSystemOption $parameters)
    {
      return $this->__soapCall('WEBUpdateSystemOption', array($parameters));
    }

    /**
     * Retrieves netForum roles.
     *
     * @param WEBGetAllRoles $parameters
     * @return WEBGetAllRolesResponse
     */
    public function WEBGetAllRoles(WEBGetAllRoles $parameters)
    {
      return $this->__soapCall('WEBGetAllRoles', array($parameters));
    }

    /**
     * Matches username to specified role and returns matching usernames.
     *
     * @param WEBFindUsersInRole $parameters
     * @return WEBFindUsersInRoleResponse
     */
    public function WEBFindUsersInRole(WEBFindUsersInRole $parameters)
    {
      return $this->__soapCall('WEBFindUsersInRole', array($parameters));
    }

    /**
     * Retrieves all roles for specified username.
     *
     * @param WEBGetRolesForUser $parameters
     * @return WEBGetRolesForUserResponse
     */
    public function WEBGetRolesForUser(WEBGetRolesForUser $parameters)
    {
      return $this->__soapCall('WEBGetRolesForUser', array($parameters));
    }

    /**
     * Retrieves all users in a specified role.
     *
     * @param WEBGetUsersInRole $parameters
     * @return WEBGetUsersInRoleResponse
     */
    public function WEBGetUsersInRole(WEBGetUsersInRole $parameters)
    {
      return $this->__soapCall('WEBGetUsersInRole', array($parameters));
    }

    /**
     * Returns a boolean indicating if the user is in the specified role.
     *
     * @param WEBIsUserInRole $parameters
     * @return WEBIsUserInRoleResponse
     */
    public function WEBIsUserInRole(WEBIsUserInRole $parameters)
    {
      return $this->__soapCall('WEBIsUserInRole', array($parameters));
    }

    /**
     * Returns a boolean indicating if the specified role exists.
     *
     * @param WEBRoleExists $parameters
     * @return WEBRoleExistsResponse
     */
    public function WEBRoleExists(WEBRoleExists $parameters)
    {
      return $this->__soapCall('WEBRoleExists', array($parameters));
    }

    /**
     * Changes the user password. Returns true if successful, false otherwise.
     *
     * @param WEBWebUserChangePassword $parameters
     * @return WEBWebUserChangePasswordResponse
     */
    public function WEBWebUserChangePassword(WEBWebUserChangePassword $parameters)
    {
      return $this->__soapCall('WEBWebUserChangePassword', array($parameters));
    }

    /**
     * Creates a web user. if susccessul, returns cst_recno used subsequently as username. if email is used as login id, leave username empty.
     *
     * @param WEBWebUserCreate $parameters
     * @return WEBWebUserCreateResponse
     */
    public function WEBWebUserCreate(WEBWebUserCreate $parameters)
    {
      return $this->__soapCall('WEBWebUserCreate', array($parameters));
    }

    /**
     * Creates a web user. if susccessul, returns cst_recno used subsequently as username. if email is used as login id, leave username empty.
     *
     * @param WEBWebUserUpdate $parameters
     * @return WEBWebUserUpdateResponse
     */
    public function WEBWebUserUpdate(WEBWebUserUpdate $parameters)
    {
      return $this->__soapCall('WEBWebUserUpdate', array($parameters));
    }

    /**
     * Matches username to specified email and returns matching users.
     *
     * @param WEBWebUserFindUsersByEmail $parameters
     * @return WEBWebUserFindUsersByEmailResponse
     */
    public function WEBWebUserFindUsersByEmail(WEBWebUserFindUsersByEmail $parameters)
    {
      return $this->__soapCall('WEBWebUserFindUsersByEmail', array($parameters));
    }

    /**
     * Matches username to domain and returns matching users.
     *
     * @param WEBWebUserFindUsersByDomain $parameters
     * @return WEBWebUserFindUsersByDomainResponse
     */
    public function WEBWebUserFindUsersByDomain(WEBWebUserFindUsersByDomain $parameters)
    {
      return $this->__soapCall('WEBWebUserFindUsersByDomain', array($parameters));
    }

    /**
     * Matches organizations to domain and returns matching organizations.
     *
     * @param WEBWebUserFindOrganizationsByDomain $parameters
     * @return WEBWebUserFindOrganizationsByDomainResponse
     */
    public function WEBWebUserFindOrganizationsByDomain(WEBWebUserFindOrganizationsByDomain $parameters)
    {
      return $this->__soapCall('WEBWebUserFindOrganizationsByDomain', array($parameters));
    }

    /**
     * returns web users with a matching userName, firstName or lastName.
     *
     * @param WEBWebUserFindUsersByName $parameters
     * @return WEBWebUserFindUsersByNameResponse
     */
    public function WEBWebUserFindUsersByName(WEBWebUserFindUsersByName $parameters)
    {
      return $this->__soapCall('WEBWebUserFindUsersByName', array($parameters));
    }

    /**
     * returns web users with a matching userName, firstName and lastName.
     *
     * @param WEBWebUserFindUsersByUserNameFirstNameLastName $parameters
     * @return WEBWebUserFindUsersByUserNameFirstNameLastNameResponse
     */
    public function WEBWebUserFindUsersByUserNameFirstNameLastName(WEBWebUserFindUsersByUserNameFirstNameLastName $parameters)
    {
      return $this->__soapCall('WEBWebUserFindUsersByUserNameFirstNameLastName', array($parameters));
    }

    /**
     * returns web user info.
     *
     * @param WEBWebUserGet $parameters
     * @return WEBWebUserGetResponse
     */
    public function WEBWebUserGet(WEBWebUserGet $parameters)
    {
      return $this->__soapCall('WEBWebUserGet', array($parameters));
    }

    /**
     * returns web user info.
     *
     * @param WEBWebUserGetByRecno_Custom $parameters
     * @return WEBWebUserGetByRecno_CustomResponse
     */
    public function WEBWebUserGetByRecno_Custom(WEBWebUserGetByRecno_Custom $parameters)
    {
      return $this->__soapCall('WEBWebUserGetByRecno_Custom', array($parameters));
    }

    /**
     * unlock the web user. Returns true if successful, false otherwise.
     *
     * @param WEBWebUserUnlock $parameters
     * @return WEBWebUserUnlockResponse
     */
    public function WEBWebUserUnlock(WEBWebUserUnlock $parameters)
    {
      return $this->__soapCall('WEBWebUserUnlock', array($parameters));
    }

    /**
     * lock-out the web user. Returns true if successful, false otherwise.
     *
     * @param WEBWebUserLock $parameters
     * @return WEBWebUserLockResponse
     */
    public function WEBWebUserLock(WEBWebUserLock $parameters)
    {
      return $this->__soapCall('WEBWebUserLock', array($parameters));
    }

    /**
     * pass login id/email and password to validate the web user. Returns Web User Info if successful.
     *
     * @param WEBWebUserLogin $parameters
     * @return WEBWebUserLoginResponse
     */
    public function WEBWebUserLogin(WEBWebUserLogin $parameters)
    {
      return $this->__soapCall('WEBWebUserLogin', array($parameters));
    }

    /**
     * pass login id/email and password to validate the web user and return a remember-me token. Returns Web User Info if successful.
     *
     * @param WEBWebUserLoginAndRememberMe $parameters
     * @return WEBWebUserLoginAndRememberMeResponse
     */
    public function WEBWebUserLoginAndRememberMe(WEBWebUserLoginAndRememberMe $parameters)
    {
      return $this->__soapCall('WEBWebUserLoginAndRememberMe', array($parameters));
    }

    /**
     * Provides the Web User info for the authenticated token that is passed as a parameter - this method can be invoked after a successful Login that returns an authentication token.  The authentication token is the only parameter for this method.
     *
     * @param WEBWebUserLoginByRememberMe $parameters
     * @return WEBWebUserLoginByRememberMeResponse
     */
    public function WEBWebUserLoginByRememberMe(WEBWebUserLoginByRememberMe $parameters)
    {
      return $this->__soapCall('WEBWebUserLoginByRememberMe', array($parameters));
    }

    /**
     * pass login id/email and password to validate the web user. Returns Web User info if validated.
     *
     * @param WEBWebUserValidateLogin $parameters
     * @return WEBWebUserValidateLoginResponse
     */
    public function WEBWebUserValidateLogin(WEBWebUserValidateLogin $parameters)
    {
      return $this->__soapCall('WEBWebUserValidateLogin', array($parameters));
    }

    /**
     * Provides the Web User info for the authenticated token that is passed as a parameter - this method can be invoked after a successful Login that returns an authentication token.  The authentication token is the only parameter for this method.
     *
     * @param WEBWebUserValidateToken $parameters
     * @return WEBWebUserValidateTokenResponse
     */
    public function WEBWebUserValidateToken(WEBWebUserValidateToken $parameters)
    {
      return $this->__soapCall('WEBWebUserValidateToken', array($parameters));
    }

    /**
     * pass login id/email and password to validate the web user. Returns cst_recno/username if successful.
     *
     * @param WEBWebUserLogin_Custom $parameters
     * @return WEBWebUserLogin_CustomResponse
     */
    public function WEBWebUserLogin_Custom(WEBWebUserLogin_Custom $parameters)
    {
      return $this->__soapCall('WEBWebUserLogin_Custom', array($parameters));
    }

    /**
     * pass login id/email and password to validate the web user. Returns cst_recno/username if successful.
     *
     * @param WEBWebUserLoginByRecno_Custom $parameters
     * @return WEBWebUserLoginByRecno_CustomResponse
     */
    public function WEBWebUserLoginByRecno_Custom(WEBWebUserLoginByRecno_Custom $parameters)
    {
      return $this->__soapCall('WEBWebUserLoginByRecno_Custom', array($parameters));
    }

    /**
     * pass login id/email and password to validate the web user. Returns cst_recno/username if validated.
     *
     * @param WEBWebUserValidateLogin_Custom $parameters
     * @return WEBWebUserValidateLogin_CustomResponse
     */
    public function WEBWebUserValidateLogin_Custom(WEBWebUserValidateLogin_Custom $parameters)
    {
      return $this->__soapCall('WEBWebUserValidateLogin_Custom', array($parameters));
    }

    /**
     * Provides the cst_recno for the authenticated token that is passed as a parameter - this method can be invoked after a successful Login that returns an authentication token.  The authentication token is the only parameter for this method.
     *
     * @param WEBWebUserValidateToken_Custom $parameters
     * @return WEBWebUserValidateToken_CustomResponse
     */
    public function WEBWebUserValidateToken_Custom(WEBWebUserValidateToken_Custom $parameters)
    {
      return $this->__soapCall('WEBWebUserValidateToken_Custom', array($parameters));
    }

    /**
     * Gets an individual's Info.
     *
     * @param WEBIndividualGet $parameters
     * @return WEBIndividualGetResponse
     */
    public function WEBIndividualGet(WEBIndividualGet $parameters)
    {
      return $this->__soapCall('WEBIndividualGet', array($parameters));
    }

    /**
     * Updates an individual's infor.
     *
     * @param WEBIndividualUpdate $parameters
     * @return WEBIndividualUpdateResponse
     */
    public function WEBIndividualUpdate(WEBIndividualUpdate $parameters)
    {
      return $this->__soapCall('WEBIndividualUpdate', array($parameters));
    }

    /**
     * Inserts a new individual.
     *
     * @param WEBIndividualInsert $parameters
     * @return WEBIndividualInsertResponse
     */
    public function WEBIndividualInsert(WEBIndividualInsert $parameters)
    {
      return $this->__soapCall('WEBIndividualInsert', array($parameters));
    }

    /**
     * Retrieves a list of individual prefixes. A prefix code can be assigned to ind_prf_code when inserting/updating an individual.
     *
     * @param WEBIndividualGetPrefixes $parameters
     * @return WEBIndividualGetPrefixesResponse
     */
    public function WEBIndividualGetPrefixes(WEBIndividualGetPrefixes $parameters)
    {
      return $this->__soapCall('WEBIndividualGetPrefixes', array($parameters));
    }

    /**
     * Retrieves a list of individual suffixes. A suffix code can be assigned to ind_sfx_code when inserting/updating an individual.
     *
     * @param WEBIndividualGetSuffixes $parameters
     * @return WEBIndividualGetSuffixesResponse
     */
    public function WEBIndividualGetSuffixes(WEBIndividualGetSuffixes $parameters)
    {
      return $this->__soapCall('WEBIndividualGetSuffixes', array($parameters));
    }

    /**
     * Retrieves a list of address types. An address type key can be assigned to cxa_adt_key when inserting/updating an address.
     *
     * @param WEBAddressGetTypes $parameters
     * @return WEBAddressGetTypesResponse
     */
    public function WEBAddressGetTypes(WEBAddressGetTypes $parameters)
    {
      return $this->__soapCall('WEBAddressGetTypes', array($parameters));
    }

    /**
     * Retrieves a list of countries. A country code can be assigned to adr_country when inserting/updating an address.
     *
     * @param WEBAddressGetCountries $parameters
     * @return WEBAddressGetCountriesResponse
     */
    public function WEBAddressGetCountries(WEBAddressGetCountries $parameters)
    {
      return $this->__soapCall('WEBAddressGetCountries', array($parameters));
    }

    /**
     * Retrieves a list of states. A state code can be assigned to adr_state when inserting/updating an address.
     *
     * @param WEBAddressGetStates $parameters
     * @return WEBAddressGetStatesResponse
     */
    public function WEBAddressGetStates(WEBAddressGetStates $parameters)
    {
      return $this->__soapCall('WEBAddressGetStates', array($parameters));
    }

    /**
     * Gets a customer's address info.
     *
     * @param WEBAddressGet $parameters
     * @return WEBAddressGetResponse
     */
    public function WEBAddressGet(WEBAddressGet $parameters)
    {
      return $this->__soapCall('WEBAddressGet', array($parameters));
    }

    /**
     * Updates a customer's address info.
     *
     * @param WEBAddressUpdate $parameters
     * @return WEBAddressUpdateResponse
     */
    public function WEBAddressUpdate(WEBAddressUpdate $parameters)
    {
      return $this->__soapCall('WEBAddressUpdate', array($parameters));
    }

    /**
     * Inserts a new customer address.
     *
     * @param WEBAddressInsert $parameters
     * @return WEBAddressInsertResponse
     */
    public function WEBAddressInsert(WEBAddressInsert $parameters)
    {
      return $this->__soapCall('WEBAddressInsert', array($parameters));
    }

    /**
     * Retrieves list of addresses for specified customer.
     *
     * @param WEBAddressGetAddressesByCustomer $parameters
     * @return WEBAddressGetAddressesByCustomerResponse
     */
    public function WEBAddressGetAddressesByCustomer(WEBAddressGetAddressesByCustomer $parameters)
    {
      return $this->__soapCall('WEBAddressGetAddressesByCustomer', array($parameters));
    }

    /**
     * Retrieves a list of phone types. A phone type key can be assigned to cph_pht_key when inserting/updating a phone number.
     *
     * @param WEBPhoneGetTypes $parameters
     * @return WEBPhoneGetTypesResponse
     */
    public function WEBPhoneGetTypes(WEBPhoneGetTypes $parameters)
    {
      return $this->__soapCall('WEBPhoneGetTypes', array($parameters));
    }

    /**
     * Gets a customer's phone info.
     *
     * @param WEBPhoneGet $parameters
     * @return WEBPhoneGetResponse
     */
    public function WEBPhoneGet(WEBPhoneGet $parameters)
    {
      return $this->__soapCall('WEBPhoneGet', array($parameters));
    }

    /**
     * Updates a customer's fax info.
     *
     * @param WEBPhoneUpdate $parameters
     * @return WEBPhoneUpdateResponse
     */
    public function WEBPhoneUpdate(WEBPhoneUpdate $parameters)
    {
      return $this->__soapCall('WEBPhoneUpdate', array($parameters));
    }

    /**
     * Inserts a new customer phone.
     *
     * @param WEBPhoneInsert $parameters
     * @return WEBPhoneInsertResponse
     */
    public function WEBPhoneInsert(WEBPhoneInsert $parameters)
    {
      return $this->__soapCall('WEBPhoneInsert', array($parameters));
    }

    /**
     * Retrieves list of phone numbers for specified customer.
     *
     * @param WEBPhoneGetPhonesByCustomer $parameters
     * @return WEBPhoneGetPhonesByCustomerResponse
     */
    public function WEBPhoneGetPhonesByCustomer(WEBPhoneGetPhonesByCustomer $parameters)
    {
      return $this->__soapCall('WEBPhoneGetPhonesByCustomer', array($parameters));
    }

    /**
     * Retrieves a list of fax types. A fax type key can be assigned to cfx_fxt_key when inserting/updating a fax number.
     *
     * @param WEBFaxGetTypes $parameters
     * @return WEBFaxGetTypesResponse
     */
    public function WEBFaxGetTypes(WEBFaxGetTypes $parameters)
    {
      return $this->__soapCall('WEBFaxGetTypes', array($parameters));
    }

    /**
     * Gets a customer's fax info.
     *
     * @param WEBFaxGet $parameters
     * @return WEBFaxGetResponse
     */
    public function WEBFaxGet(WEBFaxGet $parameters)
    {
      return $this->__soapCall('WEBFaxGet', array($parameters));
    }

    /**
     * Updates a customer's fax info.
     *
     * @param WEBFaxUpdate $parameters
     * @return WEBFaxUpdateResponse
     */
    public function WEBFaxUpdate(WEBFaxUpdate $parameters)
    {
      return $this->__soapCall('WEBFaxUpdate', array($parameters));
    }

    /**
     * Inserts a new customer fax.
     *
     * @param WEBFaxInsert $parameters
     * @return WEBFaxInsertResponse
     */
    public function WEBFaxInsert(WEBFaxInsert $parameters)
    {
      return $this->__soapCall('WEBFaxInsert', array($parameters));
    }

    /**
     * Retrieves list of faxes for specified customer.
     *
     * @param WEBFaxGetFaxesByCustomer $parameters
     * @return WEBFaxGetFaxesByCustomerResponse
     */
    public function WEBFaxGetFaxesByCustomer(WEBFaxGetFaxesByCustomer $parameters)
    {
      return $this->__soapCall('WEBFaxGetFaxesByCustomer', array($parameters));
    }

    /**
     * Gets a customer's email info.
     *
     * @param WEBEmailGet $parameters
     * @return WEBEmailGetResponse
     */
    public function WEBEmailGet(WEBEmailGet $parameters)
    {
      return $this->__soapCall('WEBEmailGet', array($parameters));
    }

    /**
     * Updates a customer's email info.
     *
     * @param WEBEmailUpdate $parameters
     * @return WEBEmailUpdateResponse
     */
    public function WEBEmailUpdate(WEBEmailUpdate $parameters)
    {
      return $this->__soapCall('WEBEmailUpdate', array($parameters));
    }

    /**
     * Inserts a new customer email.
     *
     * @param WEBEmailInsert $parameters
     * @return WEBEmailInsertResponse
     */
    public function WEBEmailInsert(WEBEmailInsert $parameters)
    {
      return $this->__soapCall('WEBEmailInsert', array($parameters));
    }

    /**
     * Retrieves list of emails for specified customer.
     *
     * @param WEBEmailGetEmailsByCustomer $parameters
     * @return WEBEmailGetEmailsByCustomerResponse
     */
    public function WEBEmailGetEmailsByCustomer(WEBEmailGetEmailsByCustomer $parameters)
    {
      return $this->__soapCall('WEBEmailGetEmailsByCustomer', array($parameters));
    }

    /**
     * Retrieves a list of organization types. The organization type code can be assigned to org_ogt_code when inserting/updating an organization.
     *
     * @param WEBOrganizationGetTypes $parameters
     * @return WEBOrganizationGetTypesResponse
     */
    public function WEBOrganizationGetTypes(WEBOrganizationGetTypes $parameters)
    {
      return $this->__soapCall('WEBOrganizationGetTypes', array($parameters));
    }

    /**
     * Gets an individual's Organization Info.
     *
     * @param WEBOrganizationGet $parameters
     * @return WEBOrganizationGetResponse
     */
    public function WEBOrganizationGet(WEBOrganizationGet $parameters)
    {
      return $this->__soapCall('WEBOrganizationGet', array($parameters));
    }

    /**
     * Updates an individual's Organization info.
     *
     * @param WEBOrganizationUpdate $parameters
     * @return WEBOrganizationUpdateResponse
     */
    public function WEBOrganizationUpdate(WEBOrganizationUpdate $parameters)
    {
      return $this->__soapCall('WEBOrganizationUpdate', array($parameters));
    }

    /**
     * Inserts a new organization.
     *
     * @param WEBOrganizationInsert $parameters
     * @return WEBOrganizationInsertResponse
     */
    public function WEBOrganizationInsert(WEBOrganizationInsert $parameters)
    {
      return $this->__soapCall('WEBOrganizationInsert', array($parameters));
    }

    /**
     * Creates a new contact request.
     *
     * @param WEBContactRequestInsert $parameters
     * @return WEBContactRequestInsertResponse
     */
    public function WEBContactRequestInsert(WEBContactRequestInsert $parameters)
    {
      return $this->__soapCall('WEBContactRequestInsert', array($parameters));
    }

    /**
     * Retrieves a list of contact request originations.
     *
     * @param WEBContactRequestGetOriginations $parameters
     * @return WEBContactRequestGetOriginationsResponse
     */
    public function WEBContactRequestGetOriginations(WEBContactRequestGetOriginations $parameters)
    {
      return $this->__soapCall('WEBContactRequestGetOriginations', array($parameters));
    }

    /**
     * Retrieves a list of contact request priorities.
     *
     * @param WEBContactRequestGetPriorities $parameters
     * @return WEBContactRequestGetPrioritiesResponse
     */
    public function WEBContactRequestGetPriorities(WEBContactRequestGetPriorities $parameters)
    {
      return $this->__soapCall('WEBContactRequestGetPriorities', array($parameters));
    }

    /**
     * Retrieves a list of contact request types.
     *
     * @param WEBContactRequestGetRequestTypes $parameters
     * @return WEBContactRequestGetRequestTypesResponse
     */
    public function WEBContactRequestGetRequestTypes(WEBContactRequestGetRequestTypes $parameters)
    {
      return $this->__soapCall('WEBContactRequestGetRequestTypes', array($parameters));
    }

    /**
     * Retrieves a list of contact request reasons for all types.
     *
     * @param WEBContactRequestGetRequestTypeReasons $parameters
     * @return WEBContactRequestGetRequestTypeReasonsResponse
     */
    public function WEBContactRequestGetRequestTypeReasons(WEBContactRequestGetRequestTypeReasons $parameters)
    {
      return $this->__soapCall('WEBContactRequestGetRequestTypeReasons', array($parameters));
    }

    /**
     * Retrieves a list of contact request statuses.
     *
     * @param WEBContactRequestGetStatuses $parameters
     * @return WEBContactRequestGetStatusesResponse
     */
    public function WEBContactRequestGetStatuses(WEBContactRequestGetStatuses $parameters)
    {
      return $this->__soapCall('WEBContactRequestGetStatuses', array($parameters));
    }

    /**
     * Retrieves list of purchased products by customer.
     *
     * @param WEBActivityGetPurchasedProductsByCustomer $parameters
     * @return WEBActivityGetPurchasedProductsByCustomerResponse
     */
    public function WEBActivityGetPurchasedProductsByCustomer(WEBActivityGetPurchasedProductsByCustomer $parameters)
    {
      return $this->__soapCall('WEBActivityGetPurchasedProductsByCustomer', array($parameters));
    }

    /**
     * Retrieves list of purchased events by customer.
     *
     * @param WEBActivityGetPurchasedEventsByCustomer $parameters
     * @return WEBActivityGetPurchasedEventsByCustomerResponse
     */
    public function WEBActivityGetPurchasedEventsByCustomer(WEBActivityGetPurchasedEventsByCustomer $parameters)
    {
      return $this->__soapCall('WEBActivityGetPurchasedEventsByCustomer', array($parameters));
    }

    /**
     * Returns TRUE if the customer is already registered for the event..
     *
     * @param WEBActivityAlreadyRegisteredForEvent $parameters
     * @return WEBActivityAlreadyRegisteredForEventResponse
     */
    public function WEBActivityAlreadyRegisteredForEvent(WEBActivityAlreadyRegisteredForEvent $parameters)
    {
      return $this->__soapCall('WEBActivityAlreadyRegisteredForEvent', array($parameters));
    }

    /**
     * Returns the number of guests attached to a particular registration.
     *
     * @param WEBActivityNumberOfRegisteredGuests $parameters
     * @return WEBActivityNumberOfRegisteredGuestsResponse
     */
    public function WEBActivityNumberOfRegisteredGuests(WEBActivityNumberOfRegisteredGuests $parameters)
    {
      return $this->__soapCall('WEBActivityNumberOfRegisteredGuests', array($parameters));
    }

    /**
     * Retrieves list of purchased memberships by customer.
     *
     * @param WEBActivityGetPurchasedMembershipsByCustomer $parameters
     * @return WEBActivityGetPurchasedMembershipsByCustomerResponse
     */
    public function WEBActivityGetPurchasedMembershipsByCustomer(WEBActivityGetPurchasedMembershipsByCustomer $parameters)
    {
      return $this->__soapCall('WEBActivityGetPurchasedMembershipsByCustomer', array($parameters));
    }

    /**
     * Retrieves list of purchased chapter memberships by customer.
     *
     * @param WEBActivityGetPurchasedChapterMembershipsByCustomer $parameters
     * @return WEBActivityGetPurchasedChapterMembershipsByCustomerResponse
     */
    public function WEBActivityGetPurchasedChapterMembershipsByCustomer(WEBActivityGetPurchasedChapterMembershipsByCustomer $parameters)
    {
      return $this->__soapCall('WEBActivityGetPurchasedChapterMembershipsByCustomer', array($parameters));
    }

    /**
     * Retrieves list of downloadable purchased products by customer.
     *
     * @param WEBActivityGetPurchasedDownoadableProductsByCustomer $parameters
     * @return WEBActivityGetPurchasedDownoadableProductsByCustomerResponse
     */
    public function WEBActivityGetPurchasedDownoadableProductsByCustomer(WEBActivityGetPurchasedDownoadableProductsByCustomer $parameters)
    {
      return $this->__soapCall('WEBActivityGetPurchasedDownoadableProductsByCustomer', array($parameters));
    }

    /**
     * Retrieves Registrant Events.
     *
     * @param WEBActivityGetRegistrantEvents $parameters
     * @return WEBActivityGetRegistrantEventsResponse
     */
    public function WEBActivityGetRegistrantEvents(WEBActivityGetRegistrantEvents $parameters)
    {
      return $this->__soapCall('WEBActivityGetRegistrantEvents', array($parameters));
    }

    /**
     * Retrieves Registrant Sessions
     *
     * @param WEBActivityGetRegistrantSessions $parameters
     * @return WEBActivityGetRegistrantSessionsResponse
     */
    public function WEBActivityGetRegistrantSessions(WEBActivityGetRegistrantSessions $parameters)
    {
      return $this->__soapCall('WEBActivityGetRegistrantSessions', array($parameters));
    }

    /**
     * Retrieves Registrant Tracks
     *
     * @param WEBActivityGetRegistrantTracks $parameters
     * @return WEBActivityGetRegistrantTracksResponse
     */
    public function WEBActivityGetRegistrantTracks(WEBActivityGetRegistrantTracks $parameters)
    {
      return $this->__soapCall('WEBActivityGetRegistrantTracks', array($parameters));
    }

    /**
     * Retrieves Registrant Guests
     *
     * @param WEBActivityGetRegistrantGuests $parameters
     * @return WEBActivityGetRegistrantGuestsResponse
     */
    public function WEBActivityGetRegistrantGuests(WEBActivityGetRegistrantGuests $parameters)
    {
      return $this->__soapCall('WEBActivityGetRegistrantGuests', array($parameters));
    }

    /**
     * Retrieves List of Active Committees.
     *
     * @param WEBCommitteeGetCommitteeList $parameters
     * @return WEBCommitteeGetCommitteeListResponse
     */
    public function WEBCommitteeGetCommitteeList(WEBCommitteeGetCommitteeList $parameters)
    {
      return $this->__soapCall('WEBCommitteeGetCommitteeList', array($parameters));
    }

}
