<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class InvoiceDetailCustomer_Invoice_Detail_Liability_View_DataObjectType
{

    /**
     * @var av_key_Type $idx_idc_key
     */
    protected $idx_idc_key = null;

    /**
     * @var av_currency_Type $idx_idc_total
     */
    protected $idx_idc_total = null;

    /**
     * @var av_currency_Type $idx_idc_original_payamount
     */
    protected $idx_idc_original_payamount = null;

    /**
     * @var av_currency_Type $idx_idc_balance
     */
    protected $idx_idc_balance = null;

    /**
     * @var av_currency_Type $idx_idc_original_writeoff_amount
     */
    protected $idx_idc_original_writeoff_amount = null;

    /**
     * @var decimal_Type $idx_idc_original_cancel_qty
     */
    protected $idx_idc_original_cancel_qty = null;

    /**
     * @var decimal_Type $idx_idc_available_cancel_qty
     */
    protected $idx_idc_available_cancel_qty = null;

    /**
     * @var decimal_Type $idx_idc_original_return_qty
     */
    protected $idx_idc_original_return_qty = null;

    /**
     * @var av_integer_Type $idx_idc_available_return_qty
     */
    protected $idx_idc_available_return_qty = null;

    /**
     * @var av_currency_Type $idx_idc_available_credit_dollar_amount
     */
    protected $idx_idc_available_credit_dollar_amount = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getIdx_idc_key()
    {
      return $this->idx_idc_key;
    }

    /**
     * @param av_key_Type $idx_idc_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_View_DataObjectType
     */
    public function setIdx_idc_key($idx_idc_key)
    {
      $this->idx_idc_key = $idx_idc_key;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIdx_idc_total()
    {
      return $this->idx_idc_total;
    }

    /**
     * @param av_currency_Type $idx_idc_total
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_View_DataObjectType
     */
    public function setIdx_idc_total($idx_idc_total)
    {
      $this->idx_idc_total = $idx_idc_total;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIdx_idc_original_payamount()
    {
      return $this->idx_idc_original_payamount;
    }

    /**
     * @param av_currency_Type $idx_idc_original_payamount
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_View_DataObjectType
     */
    public function setIdx_idc_original_payamount($idx_idc_original_payamount)
    {
      $this->idx_idc_original_payamount = $idx_idc_original_payamount;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIdx_idc_balance()
    {
      return $this->idx_idc_balance;
    }

    /**
     * @param av_currency_Type $idx_idc_balance
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_View_DataObjectType
     */
    public function setIdx_idc_balance($idx_idc_balance)
    {
      $this->idx_idc_balance = $idx_idc_balance;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIdx_idc_original_writeoff_amount()
    {
      return $this->idx_idc_original_writeoff_amount;
    }

    /**
     * @param av_currency_Type $idx_idc_original_writeoff_amount
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_View_DataObjectType
     */
    public function setIdx_idc_original_writeoff_amount($idx_idc_original_writeoff_amount)
    {
      $this->idx_idc_original_writeoff_amount = $idx_idc_original_writeoff_amount;
      return $this;
    }

    /**
     * @return decimal_Type
     */
    public function getIdx_idc_original_cancel_qty()
    {
      return $this->idx_idc_original_cancel_qty;
    }

    /**
     * @param decimal_Type $idx_idc_original_cancel_qty
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_View_DataObjectType
     */
    public function setIdx_idc_original_cancel_qty($idx_idc_original_cancel_qty)
    {
      $this->idx_idc_original_cancel_qty = $idx_idc_original_cancel_qty;
      return $this;
    }

    /**
     * @return decimal_Type
     */
    public function getIdx_idc_available_cancel_qty()
    {
      return $this->idx_idc_available_cancel_qty;
    }

    /**
     * @param decimal_Type $idx_idc_available_cancel_qty
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_View_DataObjectType
     */
    public function setIdx_idc_available_cancel_qty($idx_idc_available_cancel_qty)
    {
      $this->idx_idc_available_cancel_qty = $idx_idc_available_cancel_qty;
      return $this;
    }

    /**
     * @return decimal_Type
     */
    public function getIdx_idc_original_return_qty()
    {
      return $this->idx_idc_original_return_qty;
    }

    /**
     * @param decimal_Type $idx_idc_original_return_qty
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_View_DataObjectType
     */
    public function setIdx_idc_original_return_qty($idx_idc_original_return_qty)
    {
      $this->idx_idc_original_return_qty = $idx_idc_original_return_qty;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getIdx_idc_available_return_qty()
    {
      return $this->idx_idc_available_return_qty;
    }

    /**
     * @param av_integer_Type $idx_idc_available_return_qty
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_View_DataObjectType
     */
    public function setIdx_idc_available_return_qty($idx_idc_available_return_qty)
    {
      $this->idx_idc_available_return_qty = $idx_idc_available_return_qty;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIdx_idc_available_credit_dollar_amount()
    {
      return $this->idx_idc_available_credit_dollar_amount;
    }

    /**
     * @param av_currency_Type $idx_idc_available_credit_dollar_amount
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomer_Invoice_Detail_Liability_View_DataObjectType
     */
    public function setIdx_idc_available_credit_dollar_amount($idx_idc_available_credit_dollar_amount)
    {
      $this->idx_idc_available_credit_dollar_amount = $idx_idc_available_credit_dollar_amount;
      return $this;
    }

}
