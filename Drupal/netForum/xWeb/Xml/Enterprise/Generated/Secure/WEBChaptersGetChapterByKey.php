<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBChaptersGetChapterByKey
{

    /**
     * @var guid $ChapterKey
     */
    protected $ChapterKey = null;

    /**
     * @param guid $ChapterKey
     */
    public function __construct($ChapterKey)
    {
      $this->ChapterKey = $ChapterKey;
    }

    /**
     * @return guid
     */
    public function getChapterKey()
    {
      return $this->ChapterKey;
    }

    /**
     * @param guid $ChapterKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBChaptersGetChapterByKey
     */
    public function setChapterKey($ChapterKey)
    {
      $this->ChapterKey = $ChapterKey;
      return $this;
    }

}
