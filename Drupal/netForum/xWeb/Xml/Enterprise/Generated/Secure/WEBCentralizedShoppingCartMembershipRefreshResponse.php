<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartMembershipRefreshResponse
{

    /**
     * @var mb_membershipType $WEBCentralizedShoppingCartMembershipRefreshResult
     */
    protected $WEBCentralizedShoppingCartMembershipRefreshResult = null;

    /**
     * @param mb_membershipType $WEBCentralizedShoppingCartMembershipRefreshResult
     */
    public function __construct($WEBCentralizedShoppingCartMembershipRefreshResult)
    {
      $this->WEBCentralizedShoppingCartMembershipRefreshResult = $WEBCentralizedShoppingCartMembershipRefreshResult;
    }

    /**
     * @return mb_membershipType
     */
    public function getWEBCentralizedShoppingCartMembershipRefreshResult()
    {
      return $this->WEBCentralizedShoppingCartMembershipRefreshResult;
    }

    /**
     * @param mb_membershipType $WEBCentralizedShoppingCartMembershipRefreshResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartMembershipRefreshResponse
     */
    public function setWEBCentralizedShoppingCartMembershipRefreshResult($WEBCentralizedShoppingCartMembershipRefreshResult)
    {
      $this->WEBCentralizedShoppingCartMembershipRefreshResult = $WEBCentralizedShoppingCartMembershipRefreshResult;
      return $this;
    }

}
