<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class AccreditationArea_Accreditation_Type_DataObjectType
{

    /**
     * @var av_key_Type $amt_key
     */
    protected $amt_key = null;

    /**
     * @var av_user_Type $amt_add_user
     */
    protected $amt_add_user = null;

    /**
     * @var av_date_Type $amt_add_date
     */
    protected $amt_add_date = null;

    /**
     * @var av_user_Type $amt_change_user
     */
    protected $amt_change_user = null;

    /**
     * @var av_date_Type $amt_change_date
     */
    protected $amt_change_date = null;

    /**
     * @var av_delete_flag_Type $amt_delete_flag
     */
    protected $amt_delete_flag = null;

    /**
     * @var av_key_Type $amt_entity_key
     */
    protected $amt_entity_key = null;

    /**
     * @var av_key_Type $amt_key_ext
     */
    protected $amt_key_ext = null;

    /**
     * @var stringLength200_Type $amt_desc
     */
    protected $amt_desc = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getAmt_key()
    {
      return $this->amt_key;
    }

    /**
     * @param av_key_Type $amt_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Type_DataObjectType
     */
    public function setAmt_key($amt_key)
    {
      $this->amt_key = $amt_key;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getAmt_add_user()
    {
      return $this->amt_add_user;
    }

    /**
     * @param av_user_Type $amt_add_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Type_DataObjectType
     */
    public function setAmt_add_user($amt_add_user)
    {
      $this->amt_add_user = $amt_add_user;
      return $this;
    }

    /**
     * @return av_date_Type
     */
    public function getAmt_add_date()
    {
      return $this->amt_add_date;
    }

    /**
     * @param av_date_Type $amt_add_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Type_DataObjectType
     */
    public function setAmt_add_date($amt_add_date)
    {
      $this->amt_add_date = $amt_add_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getAmt_change_user()
    {
      return $this->amt_change_user;
    }

    /**
     * @param av_user_Type $amt_change_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Type_DataObjectType
     */
    public function setAmt_change_user($amt_change_user)
    {
      $this->amt_change_user = $amt_change_user;
      return $this;
    }

    /**
     * @return av_date_Type
     */
    public function getAmt_change_date()
    {
      return $this->amt_change_date;
    }

    /**
     * @param av_date_Type $amt_change_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Type_DataObjectType
     */
    public function setAmt_change_date($amt_change_date)
    {
      $this->amt_change_date = $amt_change_date;
      return $this;
    }

    /**
     * @return av_delete_flag_Type
     */
    public function getAmt_delete_flag()
    {
      return $this->amt_delete_flag;
    }

    /**
     * @param av_delete_flag_Type $amt_delete_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Type_DataObjectType
     */
    public function setAmt_delete_flag($amt_delete_flag)
    {
      $this->amt_delete_flag = $amt_delete_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAmt_entity_key()
    {
      return $this->amt_entity_key;
    }

    /**
     * @param av_key_Type $amt_entity_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Type_DataObjectType
     */
    public function setAmt_entity_key($amt_entity_key)
    {
      $this->amt_entity_key = $amt_entity_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAmt_key_ext()
    {
      return $this->amt_key_ext;
    }

    /**
     * @param av_key_Type $amt_key_ext
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Type_DataObjectType
     */
    public function setAmt_key_ext($amt_key_ext)
    {
      $this->amt_key_ext = $amt_key_ext;
      return $this;
    }

    /**
     * @return stringLength200_Type
     */
    public function getAmt_desc()
    {
      return $this->amt_desc;
    }

    /**
     * @param stringLength200_Type $amt_desc
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Type_DataObjectType
     */
    public function setAmt_desc($amt_desc)
    {
      $this->amt_desc = $amt_desc;
      return $this;
    }

}
