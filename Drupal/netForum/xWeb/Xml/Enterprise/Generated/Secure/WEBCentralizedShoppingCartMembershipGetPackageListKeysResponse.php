<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartMembershipGetPackageListKeysResponse
{

    /**
     * @var WEBCentralizedShoppingCartMembershipGetPackageListKeysResult $WEBCentralizedShoppingCartMembershipGetPackageListKeysResult
     */
    protected $WEBCentralizedShoppingCartMembershipGetPackageListKeysResult = null;

    /**
     * @param WEBCentralizedShoppingCartMembershipGetPackageListKeysResult $WEBCentralizedShoppingCartMembershipGetPackageListKeysResult
     */
    public function __construct($WEBCentralizedShoppingCartMembershipGetPackageListKeysResult)
    {
      $this->WEBCentralizedShoppingCartMembershipGetPackageListKeysResult = $WEBCentralizedShoppingCartMembershipGetPackageListKeysResult;
    }

    /**
     * @return WEBCentralizedShoppingCartMembershipGetPackageListKeysResult
     */
    public function getWEBCentralizedShoppingCartMembershipGetPackageListKeysResult()
    {
      return $this->WEBCentralizedShoppingCartMembershipGetPackageListKeysResult;
    }

    /**
     * @param WEBCentralizedShoppingCartMembershipGetPackageListKeysResult $WEBCentralizedShoppingCartMembershipGetPackageListKeysResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartMembershipGetPackageListKeysResponse
     */
    public function setWEBCentralizedShoppingCartMembershipGetPackageListKeysResult($WEBCentralizedShoppingCartMembershipGetPackageListKeysResult)
    {
      $this->WEBCentralizedShoppingCartMembershipGetPackageListKeysResult = $WEBCentralizedShoppingCartMembershipGetPackageListKeysResult;
      return $this;
    }

}
