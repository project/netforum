<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartMembershipGetPackageListByName
{

    /**
     * @var string $PackageName
     */
    protected $PackageName = null;

    /**
     * @param string $PackageName
     */
    public function __construct($PackageName)
    {
      $this->PackageName = $PackageName;
    }

    /**
     * @return string
     */
    public function getPackageName()
    {
      return $this->PackageName;
    }

    /**
     * @param string $PackageName
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartMembershipGetPackageListByName
     */
    public function setPackageName($PackageName)
    {
      $this->PackageName = $PackageName;
      return $this;
    }

}
