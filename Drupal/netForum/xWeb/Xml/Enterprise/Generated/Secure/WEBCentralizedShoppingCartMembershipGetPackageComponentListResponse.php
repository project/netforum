<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartMembershipGetPackageComponentListResponse
{

    /**
     * @var WEBCentralizedShoppingCartMembershipGetPackageComponentListResult $WEBCentralizedShoppingCartMembershipGetPackageComponentListResult
     */
    protected $WEBCentralizedShoppingCartMembershipGetPackageComponentListResult = null;

    /**
     * @param WEBCentralizedShoppingCartMembershipGetPackageComponentListResult $WEBCentralizedShoppingCartMembershipGetPackageComponentListResult
     */
    public function __construct($WEBCentralizedShoppingCartMembershipGetPackageComponentListResult)
    {
      $this->WEBCentralizedShoppingCartMembershipGetPackageComponentListResult = $WEBCentralizedShoppingCartMembershipGetPackageComponentListResult;
    }

    /**
     * @return WEBCentralizedShoppingCartMembershipGetPackageComponentListResult
     */
    public function getWEBCentralizedShoppingCartMembershipGetPackageComponentListResult()
    {
      return $this->WEBCentralizedShoppingCartMembershipGetPackageComponentListResult;
    }

    /**
     * @param WEBCentralizedShoppingCartMembershipGetPackageComponentListResult $WEBCentralizedShoppingCartMembershipGetPackageComponentListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartMembershipGetPackageComponentListResponse
     */
    public function setWEBCentralizedShoppingCartMembershipGetPackageComponentListResult($WEBCentralizedShoppingCartMembershipGetPackageComponentListResult)
    {
      $this->WEBCentralizedShoppingCartMembershipGetPackageComponentListResult = $WEBCentralizedShoppingCartMembershipGetPackageComponentListResult;
      return $this;
    }

}
