<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetOrganizationInformation
{

    /**
     * @var string $OrganizationKey
     */
    protected $OrganizationKey = null;

    /**
     * @param string $OrganizationKey
     */
    public function __construct($OrganizationKey)
    {
      $this->OrganizationKey = $OrganizationKey;
    }

    /**
     * @return string
     */
    public function getOrganizationKey()
    {
      return $this->OrganizationKey;
    }

    /**
     * @param string $OrganizationKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetOrganizationInformation
     */
    public function setOrganizationKey($OrganizationKey)
    {
      $this->OrganizationKey = $OrganizationKey;
      return $this;
    }

}
