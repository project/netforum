<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetAccreditationTypeStatusList
{

    /**
     * @var guid $AccreditationTypeKey
     */
    protected $AccreditationTypeKey = null;

    /**
     * @param guid $AccreditationTypeKey
     */
    public function __construct($AccreditationTypeKey)
    {
      $this->AccreditationTypeKey = $AccreditationTypeKey;
    }

    /**
     * @return guid
     */
    public function getAccreditationTypeKey()
    {
      return $this->AccreditationTypeKey;
    }

    /**
     * @param guid $AccreditationTypeKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetAccreditationTypeStatusList
     */
    public function setAccreditationTypeKey($AccreditationTypeKey)
    {
      $this->AccreditationTypeKey = $AccreditationTypeKey;
      return $this;
    }

}
