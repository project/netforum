<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class AccreditationArea_Decision_Letter_Recipient_DataObjectType
{

    /**
     * @var av_key_Type $dlr__cst_key
     */
    protected $dlr__cst_key = null;

    /**
     * @var stringLength20_Type $dlr__cst_type
     */
    protected $dlr__cst_type = null;

    /**
     * @var stringLength150_Type $dlr__cst_name_cp
     */
    protected $dlr__cst_name_cp = null;

    /**
     * @var stringLength150_Type $dlr__cst_sort_name_dn
     */
    protected $dlr__cst_sort_name_dn = null;

    /**
     * @var stringLength150_Type $dlr__cst_ind_full_name_dn
     */
    protected $dlr__cst_ind_full_name_dn = null;

    /**
     * @var stringLength150_Type $dlr__cst_org_name_dn
     */
    protected $dlr__cst_org_name_dn = null;

    /**
     * @var stringLength150_Type $dlr__cst_ixo_title_dn
     */
    protected $dlr__cst_ixo_title_dn = null;

    /**
     * @var stringLength10_Type $dlr__cst_pref_comm_meth
     */
    protected $dlr__cst_pref_comm_meth = null;

    /**
     * @var av_text_Type $dlr__cst_bio
     */
    protected $dlr__cst_bio = null;

    /**
     * @var av_date_small_Type $dlr__cst_add_date
     */
    protected $dlr__cst_add_date = null;

    /**
     * @var av_user_Type $dlr__cst_add_user
     */
    protected $dlr__cst_add_user = null;

    /**
     * @var av_date_small_Type $dlr__cst_change_date
     */
    protected $dlr__cst_change_date = null;

    /**
     * @var av_user_Type $dlr__cst_change_user
     */
    protected $dlr__cst_change_user = null;

    /**
     * @var av_delete_flag_Type $dlr__cst_delete_flag
     */
    protected $dlr__cst_delete_flag = null;

    /**
     * @var av_recno_Type $dlr__cst_recno
     */
    protected $dlr__cst_recno = null;

    /**
     * @var stringLength10_Type $dlr__cst_id
     */
    protected $dlr__cst_id = null;

    /**
     * @var av_key_Type $dlr__cst_key_ext
     */
    protected $dlr__cst_key_ext = null;

    /**
     * @var av_flag_Type $dlr__cst_email_text_only
     */
    protected $dlr__cst_email_text_only = null;

    /**
     * @var av_currency_Type $dlr__cst_credit_limit
     */
    protected $dlr__cst_credit_limit = null;

    /**
     * @var av_key_Type $dlr__cst_src_key
     */
    protected $dlr__cst_src_key = null;

    /**
     * @var stringLength50_Type $dlr__cst_src_code
     */
    protected $dlr__cst_src_code = null;

    /**
     * @var av_flag_Type $dlr__cst_tax_exempt_flag
     */
    protected $dlr__cst_tax_exempt_flag = null;

    /**
     * @var stringLength30_Type $dlr__cst_tax_id
     */
    protected $dlr__cst_tax_id = null;

    /**
     * @var av_key_Type $dlr__cst_cxa_key
     */
    protected $dlr__cst_cxa_key = null;

    /**
     * @var av_flag_Type $dlr__cst_no_email_flag
     */
    protected $dlr__cst_no_email_flag = null;

    /**
     * @var av_key_Type $dlr__cst_cxa_billing_key
     */
    protected $dlr__cst_cxa_billing_key = null;

    /**
     * @var av_email_Type $dlr__cst_eml_address_dn
     */
    protected $dlr__cst_eml_address_dn = null;

    /**
     * @var av_key_Type $dlr__cst_eml_key
     */
    protected $dlr__cst_eml_key = null;

    /**
     * @var av_flag_Type $dlr__cst_no_phone_flag
     */
    protected $dlr__cst_no_phone_flag = null;

    /**
     * @var stringLength55_Type $dlr__cst_phn_number_complete_dn
     */
    protected $dlr__cst_phn_number_complete_dn = null;

    /**
     * @var av_key_Type $dlr__cst_cph_key
     */
    protected $dlr__cst_cph_key = null;

    /**
     * @var av_flag_Type $dlr__cst_no_fax_flag
     */
    protected $dlr__cst_no_fax_flag = null;

    /**
     * @var stringLength55_Type $dlr__cst_fax_number_complete_dn
     */
    protected $dlr__cst_fax_number_complete_dn = null;

    /**
     * @var av_key_Type $dlr__cst_cfx_key
     */
    protected $dlr__cst_cfx_key = null;

    /**
     * @var av_key_Type $dlr__cst_ixo_key
     */
    protected $dlr__cst_ixo_key = null;

    /**
     * @var av_flag_Type $dlr__cst_no_web_flag
     */
    protected $dlr__cst_no_web_flag = null;

    /**
     * @var stringLength15_Type $dlr__cst_oldid
     */
    protected $dlr__cst_oldid = null;

    /**
     * @var av_flag_Type $dlr__cst_member_flag
     */
    protected $dlr__cst_member_flag = null;

    /**
     * @var av_url_Type $dlr__cst_url_code_dn
     */
    protected $dlr__cst_url_code_dn = null;

    /**
     * @var av_key_Type $dlr__cst_parent_cst_key
     */
    protected $dlr__cst_parent_cst_key = null;

    /**
     * @var av_key_Type $dlr__cst_url_key
     */
    protected $dlr__cst_url_key = null;

    /**
     * @var av_flag_Type $dlr__cst_no_msg_flag
     */
    protected $dlr__cst_no_msg_flag = null;

    /**
     * @var av_messaging_name_Type $dlr__cst_msg_handle_dn
     */
    protected $dlr__cst_msg_handle_dn = null;

    /**
     * @var stringLength80_Type $dlr__cst_web_login
     */
    protected $dlr__cst_web_login = null;

    /**
     * @var stringLength50_Type $dlr__cst_web_password
     */
    protected $dlr__cst_web_password = null;

    /**
     * @var av_key_Type $dlr__cst_entity_key
     */
    protected $dlr__cst_entity_key = null;

    /**
     * @var av_key_Type $dlr__cst_msg_key
     */
    protected $dlr__cst_msg_key = null;

    /**
     * @var av_flag_Type $dlr__cst_no_mail_flag
     */
    protected $dlr__cst_no_mail_flag = null;

    /**
     * @var av_date_small_Type $dlr__cst_web_start_date
     */
    protected $dlr__cst_web_start_date = null;

    /**
     * @var av_date_small_Type $dlr__cst_web_end_date
     */
    protected $dlr__cst_web_end_date = null;

    /**
     * @var av_flag_Type $dlr__cst_web_force_password_change
     */
    protected $dlr__cst_web_force_password_change = null;

    /**
     * @var av_flag_Type $dlr__cst_web_login_disabled_flag
     */
    protected $dlr__cst_web_login_disabled_flag = null;

    /**
     * @var av_text_Type $dlr__cst_comment
     */
    protected $dlr__cst_comment = null;

    /**
     * @var av_flag_Type $dlr__cst_credit_hold_flag
     */
    protected $dlr__cst_credit_hold_flag = null;

    /**
     * @var stringLength50_Type $dlr__cst_credit_hold_reason
     */
    protected $dlr__cst_credit_hold_reason = null;

    /**
     * @var av_flag_Type $dlr__cst_web_forgot_password_status
     */
    protected $dlr__cst_web_forgot_password_status = null;

    /**
     * @var av_key_Type $dlr__cst_old_cxa_key
     */
    protected $dlr__cst_old_cxa_key = null;

    /**
     * @var av_date_small_Type $dlr__cst_last_email_date
     */
    protected $dlr__cst_last_email_date = null;

    /**
     * @var av_flag_Type $dlr__cst_no_publish_flag
     */
    protected $dlr__cst_no_publish_flag = null;

    /**
     * @var av_key_Type $dlr__cst_sin_key
     */
    protected $dlr__cst_sin_key = null;

    /**
     * @var av_key_Type $dlr__cst_ttl_key
     */
    protected $dlr__cst_ttl_key = null;

    /**
     * @var av_key_Type $dlr__cst_jfn_key
     */
    protected $dlr__cst_jfn_key = null;

    /**
     * @var av_key_Type $dlr__cst_cur_key
     */
    protected $dlr__cst_cur_key = null;

    /**
     * @var stringLength510_Type $dlr__cst_attribute_1
     */
    protected $dlr__cst_attribute_1 = null;

    /**
     * @var stringLength510_Type $dlr__cst_attribute_2
     */
    protected $dlr__cst_attribute_2 = null;

    /**
     * @var stringLength100_Type $dlr__cst_salutation_1
     */
    protected $dlr__cst_salutation_1 = null;

    /**
     * @var stringLength100_Type $dlr__cst_salutation_2
     */
    protected $dlr__cst_salutation_2 = null;

    /**
     * @var av_key_Type $dlr__cst_merge_cst_key
     */
    protected $dlr__cst_merge_cst_key = null;

    /**
     * @var stringLength100_Type $dlr__cst_salutation_3
     */
    protected $dlr__cst_salutation_3 = null;

    /**
     * @var stringLength100_Type $dlr__cst_salutation_4
     */
    protected $dlr__cst_salutation_4 = null;

    /**
     * @var stringLength200_Type $dlr__cst_default_recognize_as
     */
    protected $dlr__cst_default_recognize_as = null;

    /**
     * @var av_decimal4_Type $dlr__cst_score
     */
    protected $dlr__cst_score = null;

    /**
     * @var av_integer_Type $dlr__cst_score_normalized
     */
    protected $dlr__cst_score_normalized = null;

    /**
     * @var av_integer_Type $dlr__cst_score_trend
     */
    protected $dlr__cst_score_trend = null;

    /**
     * @var stringLength25_Type $dlr__cst_vault_account
     */
    protected $dlr__cst_vault_account = null;

    /**
     * @var av_flag_Type $dlr__cst_exclude_from_social_flag
     */
    protected $dlr__cst_exclude_from_social_flag = null;

    /**
     * @var av_integer_Type $dlr__cst_social_score
     */
    protected $dlr__cst_social_score = null;

    /**
     * @var stringLength9_Type $dlr__cst_ptin
     */
    protected $dlr__cst_ptin = null;

    /**
     * @var av_recno_Type $dlr__cst_aicpa_member_id
     */
    protected $dlr__cst_aicpa_member_id = null;

    /**
     * @var stringLength50_Type $dlr__cst_vendor_code
     */
    protected $dlr__cst_vendor_code = null;

    /**
     * @var stringLength1_Type $dlr__cst_salt
     */
    protected $dlr__cst_salt = null;

    /**
     * @var av_key_Type $dlr__cst_sca_key
     */
    protected $dlr__cst_sca_key = null;

    /**
     * @var av_integer_Type $dlr__cst_iterations
     */
    protected $dlr__cst_iterations = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getDlr__cst_key()
    {
      return $this->dlr__cst_key;
    }

    /**
     * @param av_key_Type $dlr__cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_key($dlr__cst_key)
    {
      $this->dlr__cst_key = $dlr__cst_key;
      return $this;
    }

    /**
     * @return stringLength20_Type
     */
    public function getDlr__cst_type()
    {
      return $this->dlr__cst_type;
    }

    /**
     * @param stringLength20_Type $dlr__cst_type
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_type($dlr__cst_type)
    {
      $this->dlr__cst_type = $dlr__cst_type;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getDlr__cst_name_cp()
    {
      return $this->dlr__cst_name_cp;
    }

    /**
     * @param stringLength150_Type $dlr__cst_name_cp
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_name_cp($dlr__cst_name_cp)
    {
      $this->dlr__cst_name_cp = $dlr__cst_name_cp;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getDlr__cst_sort_name_dn()
    {
      return $this->dlr__cst_sort_name_dn;
    }

    /**
     * @param stringLength150_Type $dlr__cst_sort_name_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_sort_name_dn($dlr__cst_sort_name_dn)
    {
      $this->dlr__cst_sort_name_dn = $dlr__cst_sort_name_dn;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getDlr__cst_ind_full_name_dn()
    {
      return $this->dlr__cst_ind_full_name_dn;
    }

    /**
     * @param stringLength150_Type $dlr__cst_ind_full_name_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_ind_full_name_dn($dlr__cst_ind_full_name_dn)
    {
      $this->dlr__cst_ind_full_name_dn = $dlr__cst_ind_full_name_dn;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getDlr__cst_org_name_dn()
    {
      return $this->dlr__cst_org_name_dn;
    }

    /**
     * @param stringLength150_Type $dlr__cst_org_name_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_org_name_dn($dlr__cst_org_name_dn)
    {
      $this->dlr__cst_org_name_dn = $dlr__cst_org_name_dn;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getDlr__cst_ixo_title_dn()
    {
      return $this->dlr__cst_ixo_title_dn;
    }

    /**
     * @param stringLength150_Type $dlr__cst_ixo_title_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_ixo_title_dn($dlr__cst_ixo_title_dn)
    {
      $this->dlr__cst_ixo_title_dn = $dlr__cst_ixo_title_dn;
      return $this;
    }

    /**
     * @return stringLength10_Type
     */
    public function getDlr__cst_pref_comm_meth()
    {
      return $this->dlr__cst_pref_comm_meth;
    }

    /**
     * @param stringLength10_Type $dlr__cst_pref_comm_meth
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_pref_comm_meth($dlr__cst_pref_comm_meth)
    {
      $this->dlr__cst_pref_comm_meth = $dlr__cst_pref_comm_meth;
      return $this;
    }

    /**
     * @return av_text_Type
     */
    public function getDlr__cst_bio()
    {
      return $this->dlr__cst_bio;
    }

    /**
     * @param av_text_Type $dlr__cst_bio
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_bio($dlr__cst_bio)
    {
      $this->dlr__cst_bio = $dlr__cst_bio;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getDlr__cst_add_date()
    {
      return $this->dlr__cst_add_date;
    }

    /**
     * @param av_date_small_Type $dlr__cst_add_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_add_date($dlr__cst_add_date)
    {
      $this->dlr__cst_add_date = $dlr__cst_add_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getDlr__cst_add_user()
    {
      return $this->dlr__cst_add_user;
    }

    /**
     * @param av_user_Type $dlr__cst_add_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_add_user($dlr__cst_add_user)
    {
      $this->dlr__cst_add_user = $dlr__cst_add_user;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getDlr__cst_change_date()
    {
      return $this->dlr__cst_change_date;
    }

    /**
     * @param av_date_small_Type $dlr__cst_change_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_change_date($dlr__cst_change_date)
    {
      $this->dlr__cst_change_date = $dlr__cst_change_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getDlr__cst_change_user()
    {
      return $this->dlr__cst_change_user;
    }

    /**
     * @param av_user_Type $dlr__cst_change_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_change_user($dlr__cst_change_user)
    {
      $this->dlr__cst_change_user = $dlr__cst_change_user;
      return $this;
    }

    /**
     * @return av_delete_flag_Type
     */
    public function getDlr__cst_delete_flag()
    {
      return $this->dlr__cst_delete_flag;
    }

    /**
     * @param av_delete_flag_Type $dlr__cst_delete_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_delete_flag($dlr__cst_delete_flag)
    {
      $this->dlr__cst_delete_flag = $dlr__cst_delete_flag;
      return $this;
    }

    /**
     * @return av_recno_Type
     */
    public function getDlr__cst_recno()
    {
      return $this->dlr__cst_recno;
    }

    /**
     * @param av_recno_Type $dlr__cst_recno
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_recno($dlr__cst_recno)
    {
      $this->dlr__cst_recno = $dlr__cst_recno;
      return $this;
    }

    /**
     * @return stringLength10_Type
     */
    public function getDlr__cst_id()
    {
      return $this->dlr__cst_id;
    }

    /**
     * @param stringLength10_Type $dlr__cst_id
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_id($dlr__cst_id)
    {
      $this->dlr__cst_id = $dlr__cst_id;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getDlr__cst_key_ext()
    {
      return $this->dlr__cst_key_ext;
    }

    /**
     * @param av_key_Type $dlr__cst_key_ext
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_key_ext($dlr__cst_key_ext)
    {
      $this->dlr__cst_key_ext = $dlr__cst_key_ext;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getDlr__cst_email_text_only()
    {
      return $this->dlr__cst_email_text_only;
    }

    /**
     * @param av_flag_Type $dlr__cst_email_text_only
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_email_text_only($dlr__cst_email_text_only)
    {
      $this->dlr__cst_email_text_only = $dlr__cst_email_text_only;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getDlr__cst_credit_limit()
    {
      return $this->dlr__cst_credit_limit;
    }

    /**
     * @param av_currency_Type $dlr__cst_credit_limit
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_credit_limit($dlr__cst_credit_limit)
    {
      $this->dlr__cst_credit_limit = $dlr__cst_credit_limit;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getDlr__cst_src_key()
    {
      return $this->dlr__cst_src_key;
    }

    /**
     * @param av_key_Type $dlr__cst_src_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_src_key($dlr__cst_src_key)
    {
      $this->dlr__cst_src_key = $dlr__cst_src_key;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getDlr__cst_src_code()
    {
      return $this->dlr__cst_src_code;
    }

    /**
     * @param stringLength50_Type $dlr__cst_src_code
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_src_code($dlr__cst_src_code)
    {
      $this->dlr__cst_src_code = $dlr__cst_src_code;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getDlr__cst_tax_exempt_flag()
    {
      return $this->dlr__cst_tax_exempt_flag;
    }

    /**
     * @param av_flag_Type $dlr__cst_tax_exempt_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_tax_exempt_flag($dlr__cst_tax_exempt_flag)
    {
      $this->dlr__cst_tax_exempt_flag = $dlr__cst_tax_exempt_flag;
      return $this;
    }

    /**
     * @return stringLength30_Type
     */
    public function getDlr__cst_tax_id()
    {
      return $this->dlr__cst_tax_id;
    }

    /**
     * @param stringLength30_Type $dlr__cst_tax_id
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_tax_id($dlr__cst_tax_id)
    {
      $this->dlr__cst_tax_id = $dlr__cst_tax_id;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getDlr__cst_cxa_key()
    {
      return $this->dlr__cst_cxa_key;
    }

    /**
     * @param av_key_Type $dlr__cst_cxa_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_cxa_key($dlr__cst_cxa_key)
    {
      $this->dlr__cst_cxa_key = $dlr__cst_cxa_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getDlr__cst_no_email_flag()
    {
      return $this->dlr__cst_no_email_flag;
    }

    /**
     * @param av_flag_Type $dlr__cst_no_email_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_no_email_flag($dlr__cst_no_email_flag)
    {
      $this->dlr__cst_no_email_flag = $dlr__cst_no_email_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getDlr__cst_cxa_billing_key()
    {
      return $this->dlr__cst_cxa_billing_key;
    }

    /**
     * @param av_key_Type $dlr__cst_cxa_billing_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_cxa_billing_key($dlr__cst_cxa_billing_key)
    {
      $this->dlr__cst_cxa_billing_key = $dlr__cst_cxa_billing_key;
      return $this;
    }

    /**
     * @return av_email_Type
     */
    public function getDlr__cst_eml_address_dn()
    {
      return $this->dlr__cst_eml_address_dn;
    }

    /**
     * @param av_email_Type $dlr__cst_eml_address_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_eml_address_dn($dlr__cst_eml_address_dn)
    {
      $this->dlr__cst_eml_address_dn = $dlr__cst_eml_address_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getDlr__cst_eml_key()
    {
      return $this->dlr__cst_eml_key;
    }

    /**
     * @param av_key_Type $dlr__cst_eml_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_eml_key($dlr__cst_eml_key)
    {
      $this->dlr__cst_eml_key = $dlr__cst_eml_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getDlr__cst_no_phone_flag()
    {
      return $this->dlr__cst_no_phone_flag;
    }

    /**
     * @param av_flag_Type $dlr__cst_no_phone_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_no_phone_flag($dlr__cst_no_phone_flag)
    {
      $this->dlr__cst_no_phone_flag = $dlr__cst_no_phone_flag;
      return $this;
    }

    /**
     * @return stringLength55_Type
     */
    public function getDlr__cst_phn_number_complete_dn()
    {
      return $this->dlr__cst_phn_number_complete_dn;
    }

    /**
     * @param stringLength55_Type $dlr__cst_phn_number_complete_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_phn_number_complete_dn($dlr__cst_phn_number_complete_dn)
    {
      $this->dlr__cst_phn_number_complete_dn = $dlr__cst_phn_number_complete_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getDlr__cst_cph_key()
    {
      return $this->dlr__cst_cph_key;
    }

    /**
     * @param av_key_Type $dlr__cst_cph_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_cph_key($dlr__cst_cph_key)
    {
      $this->dlr__cst_cph_key = $dlr__cst_cph_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getDlr__cst_no_fax_flag()
    {
      return $this->dlr__cst_no_fax_flag;
    }

    /**
     * @param av_flag_Type $dlr__cst_no_fax_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_no_fax_flag($dlr__cst_no_fax_flag)
    {
      $this->dlr__cst_no_fax_flag = $dlr__cst_no_fax_flag;
      return $this;
    }

    /**
     * @return stringLength55_Type
     */
    public function getDlr__cst_fax_number_complete_dn()
    {
      return $this->dlr__cst_fax_number_complete_dn;
    }

    /**
     * @param stringLength55_Type $dlr__cst_fax_number_complete_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_fax_number_complete_dn($dlr__cst_fax_number_complete_dn)
    {
      $this->dlr__cst_fax_number_complete_dn = $dlr__cst_fax_number_complete_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getDlr__cst_cfx_key()
    {
      return $this->dlr__cst_cfx_key;
    }

    /**
     * @param av_key_Type $dlr__cst_cfx_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_cfx_key($dlr__cst_cfx_key)
    {
      $this->dlr__cst_cfx_key = $dlr__cst_cfx_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getDlr__cst_ixo_key()
    {
      return $this->dlr__cst_ixo_key;
    }

    /**
     * @param av_key_Type $dlr__cst_ixo_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_ixo_key($dlr__cst_ixo_key)
    {
      $this->dlr__cst_ixo_key = $dlr__cst_ixo_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getDlr__cst_no_web_flag()
    {
      return $this->dlr__cst_no_web_flag;
    }

    /**
     * @param av_flag_Type $dlr__cst_no_web_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_no_web_flag($dlr__cst_no_web_flag)
    {
      $this->dlr__cst_no_web_flag = $dlr__cst_no_web_flag;
      return $this;
    }

    /**
     * @return stringLength15_Type
     */
    public function getDlr__cst_oldid()
    {
      return $this->dlr__cst_oldid;
    }

    /**
     * @param stringLength15_Type $dlr__cst_oldid
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_oldid($dlr__cst_oldid)
    {
      $this->dlr__cst_oldid = $dlr__cst_oldid;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getDlr__cst_member_flag()
    {
      return $this->dlr__cst_member_flag;
    }

    /**
     * @param av_flag_Type $dlr__cst_member_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_member_flag($dlr__cst_member_flag)
    {
      $this->dlr__cst_member_flag = $dlr__cst_member_flag;
      return $this;
    }

    /**
     * @return av_url_Type
     */
    public function getDlr__cst_url_code_dn()
    {
      return $this->dlr__cst_url_code_dn;
    }

    /**
     * @param av_url_Type $dlr__cst_url_code_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_url_code_dn($dlr__cst_url_code_dn)
    {
      $this->dlr__cst_url_code_dn = $dlr__cst_url_code_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getDlr__cst_parent_cst_key()
    {
      return $this->dlr__cst_parent_cst_key;
    }

    /**
     * @param av_key_Type $dlr__cst_parent_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_parent_cst_key($dlr__cst_parent_cst_key)
    {
      $this->dlr__cst_parent_cst_key = $dlr__cst_parent_cst_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getDlr__cst_url_key()
    {
      return $this->dlr__cst_url_key;
    }

    /**
     * @param av_key_Type $dlr__cst_url_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_url_key($dlr__cst_url_key)
    {
      $this->dlr__cst_url_key = $dlr__cst_url_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getDlr__cst_no_msg_flag()
    {
      return $this->dlr__cst_no_msg_flag;
    }

    /**
     * @param av_flag_Type $dlr__cst_no_msg_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_no_msg_flag($dlr__cst_no_msg_flag)
    {
      $this->dlr__cst_no_msg_flag = $dlr__cst_no_msg_flag;
      return $this;
    }

    /**
     * @return av_messaging_name_Type
     */
    public function getDlr__cst_msg_handle_dn()
    {
      return $this->dlr__cst_msg_handle_dn;
    }

    /**
     * @param av_messaging_name_Type $dlr__cst_msg_handle_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_msg_handle_dn($dlr__cst_msg_handle_dn)
    {
      $this->dlr__cst_msg_handle_dn = $dlr__cst_msg_handle_dn;
      return $this;
    }

    /**
     * @return stringLength80_Type
     */
    public function getDlr__cst_web_login()
    {
      return $this->dlr__cst_web_login;
    }

    /**
     * @param stringLength80_Type $dlr__cst_web_login
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_web_login($dlr__cst_web_login)
    {
      $this->dlr__cst_web_login = $dlr__cst_web_login;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getDlr__cst_web_password()
    {
      return $this->dlr__cst_web_password;
    }

    /**
     * @param stringLength50_Type $dlr__cst_web_password
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_web_password($dlr__cst_web_password)
    {
      $this->dlr__cst_web_password = $dlr__cst_web_password;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getDlr__cst_entity_key()
    {
      return $this->dlr__cst_entity_key;
    }

    /**
     * @param av_key_Type $dlr__cst_entity_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_entity_key($dlr__cst_entity_key)
    {
      $this->dlr__cst_entity_key = $dlr__cst_entity_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getDlr__cst_msg_key()
    {
      return $this->dlr__cst_msg_key;
    }

    /**
     * @param av_key_Type $dlr__cst_msg_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_msg_key($dlr__cst_msg_key)
    {
      $this->dlr__cst_msg_key = $dlr__cst_msg_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getDlr__cst_no_mail_flag()
    {
      return $this->dlr__cst_no_mail_flag;
    }

    /**
     * @param av_flag_Type $dlr__cst_no_mail_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_no_mail_flag($dlr__cst_no_mail_flag)
    {
      $this->dlr__cst_no_mail_flag = $dlr__cst_no_mail_flag;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getDlr__cst_web_start_date()
    {
      return $this->dlr__cst_web_start_date;
    }

    /**
     * @param av_date_small_Type $dlr__cst_web_start_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_web_start_date($dlr__cst_web_start_date)
    {
      $this->dlr__cst_web_start_date = $dlr__cst_web_start_date;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getDlr__cst_web_end_date()
    {
      return $this->dlr__cst_web_end_date;
    }

    /**
     * @param av_date_small_Type $dlr__cst_web_end_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_web_end_date($dlr__cst_web_end_date)
    {
      $this->dlr__cst_web_end_date = $dlr__cst_web_end_date;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getDlr__cst_web_force_password_change()
    {
      return $this->dlr__cst_web_force_password_change;
    }

    /**
     * @param av_flag_Type $dlr__cst_web_force_password_change
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_web_force_password_change($dlr__cst_web_force_password_change)
    {
      $this->dlr__cst_web_force_password_change = $dlr__cst_web_force_password_change;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getDlr__cst_web_login_disabled_flag()
    {
      return $this->dlr__cst_web_login_disabled_flag;
    }

    /**
     * @param av_flag_Type $dlr__cst_web_login_disabled_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_web_login_disabled_flag($dlr__cst_web_login_disabled_flag)
    {
      $this->dlr__cst_web_login_disabled_flag = $dlr__cst_web_login_disabled_flag;
      return $this;
    }

    /**
     * @return av_text_Type
     */
    public function getDlr__cst_comment()
    {
      return $this->dlr__cst_comment;
    }

    /**
     * @param av_text_Type $dlr__cst_comment
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_comment($dlr__cst_comment)
    {
      $this->dlr__cst_comment = $dlr__cst_comment;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getDlr__cst_credit_hold_flag()
    {
      return $this->dlr__cst_credit_hold_flag;
    }

    /**
     * @param av_flag_Type $dlr__cst_credit_hold_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_credit_hold_flag($dlr__cst_credit_hold_flag)
    {
      $this->dlr__cst_credit_hold_flag = $dlr__cst_credit_hold_flag;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getDlr__cst_credit_hold_reason()
    {
      return $this->dlr__cst_credit_hold_reason;
    }

    /**
     * @param stringLength50_Type $dlr__cst_credit_hold_reason
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_credit_hold_reason($dlr__cst_credit_hold_reason)
    {
      $this->dlr__cst_credit_hold_reason = $dlr__cst_credit_hold_reason;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getDlr__cst_web_forgot_password_status()
    {
      return $this->dlr__cst_web_forgot_password_status;
    }

    /**
     * @param av_flag_Type $dlr__cst_web_forgot_password_status
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_web_forgot_password_status($dlr__cst_web_forgot_password_status)
    {
      $this->dlr__cst_web_forgot_password_status = $dlr__cst_web_forgot_password_status;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getDlr__cst_old_cxa_key()
    {
      return $this->dlr__cst_old_cxa_key;
    }

    /**
     * @param av_key_Type $dlr__cst_old_cxa_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_old_cxa_key($dlr__cst_old_cxa_key)
    {
      $this->dlr__cst_old_cxa_key = $dlr__cst_old_cxa_key;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getDlr__cst_last_email_date()
    {
      return $this->dlr__cst_last_email_date;
    }

    /**
     * @param av_date_small_Type $dlr__cst_last_email_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_last_email_date($dlr__cst_last_email_date)
    {
      $this->dlr__cst_last_email_date = $dlr__cst_last_email_date;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getDlr__cst_no_publish_flag()
    {
      return $this->dlr__cst_no_publish_flag;
    }

    /**
     * @param av_flag_Type $dlr__cst_no_publish_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_no_publish_flag($dlr__cst_no_publish_flag)
    {
      $this->dlr__cst_no_publish_flag = $dlr__cst_no_publish_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getDlr__cst_sin_key()
    {
      return $this->dlr__cst_sin_key;
    }

    /**
     * @param av_key_Type $dlr__cst_sin_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_sin_key($dlr__cst_sin_key)
    {
      $this->dlr__cst_sin_key = $dlr__cst_sin_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getDlr__cst_ttl_key()
    {
      return $this->dlr__cst_ttl_key;
    }

    /**
     * @param av_key_Type $dlr__cst_ttl_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_ttl_key($dlr__cst_ttl_key)
    {
      $this->dlr__cst_ttl_key = $dlr__cst_ttl_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getDlr__cst_jfn_key()
    {
      return $this->dlr__cst_jfn_key;
    }

    /**
     * @param av_key_Type $dlr__cst_jfn_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_jfn_key($dlr__cst_jfn_key)
    {
      $this->dlr__cst_jfn_key = $dlr__cst_jfn_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getDlr__cst_cur_key()
    {
      return $this->dlr__cst_cur_key;
    }

    /**
     * @param av_key_Type $dlr__cst_cur_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_cur_key($dlr__cst_cur_key)
    {
      $this->dlr__cst_cur_key = $dlr__cst_cur_key;
      return $this;
    }

    /**
     * @return stringLength510_Type
     */
    public function getDlr__cst_attribute_1()
    {
      return $this->dlr__cst_attribute_1;
    }

    /**
     * @param stringLength510_Type $dlr__cst_attribute_1
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_attribute_1($dlr__cst_attribute_1)
    {
      $this->dlr__cst_attribute_1 = $dlr__cst_attribute_1;
      return $this;
    }

    /**
     * @return stringLength510_Type
     */
    public function getDlr__cst_attribute_2()
    {
      return $this->dlr__cst_attribute_2;
    }

    /**
     * @param stringLength510_Type $dlr__cst_attribute_2
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_attribute_2($dlr__cst_attribute_2)
    {
      $this->dlr__cst_attribute_2 = $dlr__cst_attribute_2;
      return $this;
    }

    /**
     * @return stringLength100_Type
     */
    public function getDlr__cst_salutation_1()
    {
      return $this->dlr__cst_salutation_1;
    }

    /**
     * @param stringLength100_Type $dlr__cst_salutation_1
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_salutation_1($dlr__cst_salutation_1)
    {
      $this->dlr__cst_salutation_1 = $dlr__cst_salutation_1;
      return $this;
    }

    /**
     * @return stringLength100_Type
     */
    public function getDlr__cst_salutation_2()
    {
      return $this->dlr__cst_salutation_2;
    }

    /**
     * @param stringLength100_Type $dlr__cst_salutation_2
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_salutation_2($dlr__cst_salutation_2)
    {
      $this->dlr__cst_salutation_2 = $dlr__cst_salutation_2;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getDlr__cst_merge_cst_key()
    {
      return $this->dlr__cst_merge_cst_key;
    }

    /**
     * @param av_key_Type $dlr__cst_merge_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_merge_cst_key($dlr__cst_merge_cst_key)
    {
      $this->dlr__cst_merge_cst_key = $dlr__cst_merge_cst_key;
      return $this;
    }

    /**
     * @return stringLength100_Type
     */
    public function getDlr__cst_salutation_3()
    {
      return $this->dlr__cst_salutation_3;
    }

    /**
     * @param stringLength100_Type $dlr__cst_salutation_3
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_salutation_3($dlr__cst_salutation_3)
    {
      $this->dlr__cst_salutation_3 = $dlr__cst_salutation_3;
      return $this;
    }

    /**
     * @return stringLength100_Type
     */
    public function getDlr__cst_salutation_4()
    {
      return $this->dlr__cst_salutation_4;
    }

    /**
     * @param stringLength100_Type $dlr__cst_salutation_4
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_salutation_4($dlr__cst_salutation_4)
    {
      $this->dlr__cst_salutation_4 = $dlr__cst_salutation_4;
      return $this;
    }

    /**
     * @return stringLength200_Type
     */
    public function getDlr__cst_default_recognize_as()
    {
      return $this->dlr__cst_default_recognize_as;
    }

    /**
     * @param stringLength200_Type $dlr__cst_default_recognize_as
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_default_recognize_as($dlr__cst_default_recognize_as)
    {
      $this->dlr__cst_default_recognize_as = $dlr__cst_default_recognize_as;
      return $this;
    }

    /**
     * @return av_decimal4_Type
     */
    public function getDlr__cst_score()
    {
      return $this->dlr__cst_score;
    }

    /**
     * @param av_decimal4_Type $dlr__cst_score
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_score($dlr__cst_score)
    {
      $this->dlr__cst_score = $dlr__cst_score;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getDlr__cst_score_normalized()
    {
      return $this->dlr__cst_score_normalized;
    }

    /**
     * @param av_integer_Type $dlr__cst_score_normalized
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_score_normalized($dlr__cst_score_normalized)
    {
      $this->dlr__cst_score_normalized = $dlr__cst_score_normalized;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getDlr__cst_score_trend()
    {
      return $this->dlr__cst_score_trend;
    }

    /**
     * @param av_integer_Type $dlr__cst_score_trend
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_score_trend($dlr__cst_score_trend)
    {
      $this->dlr__cst_score_trend = $dlr__cst_score_trend;
      return $this;
    }

    /**
     * @return stringLength25_Type
     */
    public function getDlr__cst_vault_account()
    {
      return $this->dlr__cst_vault_account;
    }

    /**
     * @param stringLength25_Type $dlr__cst_vault_account
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_vault_account($dlr__cst_vault_account)
    {
      $this->dlr__cst_vault_account = $dlr__cst_vault_account;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getDlr__cst_exclude_from_social_flag()
    {
      return $this->dlr__cst_exclude_from_social_flag;
    }

    /**
     * @param av_flag_Type $dlr__cst_exclude_from_social_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_exclude_from_social_flag($dlr__cst_exclude_from_social_flag)
    {
      $this->dlr__cst_exclude_from_social_flag = $dlr__cst_exclude_from_social_flag;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getDlr__cst_social_score()
    {
      return $this->dlr__cst_social_score;
    }

    /**
     * @param av_integer_Type $dlr__cst_social_score
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_social_score($dlr__cst_social_score)
    {
      $this->dlr__cst_social_score = $dlr__cst_social_score;
      return $this;
    }

    /**
     * @return stringLength9_Type
     */
    public function getDlr__cst_ptin()
    {
      return $this->dlr__cst_ptin;
    }

    /**
     * @param stringLength9_Type $dlr__cst_ptin
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_ptin($dlr__cst_ptin)
    {
      $this->dlr__cst_ptin = $dlr__cst_ptin;
      return $this;
    }

    /**
     * @return av_recno_Type
     */
    public function getDlr__cst_aicpa_member_id()
    {
      return $this->dlr__cst_aicpa_member_id;
    }

    /**
     * @param av_recno_Type $dlr__cst_aicpa_member_id
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_aicpa_member_id($dlr__cst_aicpa_member_id)
    {
      $this->dlr__cst_aicpa_member_id = $dlr__cst_aicpa_member_id;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getDlr__cst_vendor_code()
    {
      return $this->dlr__cst_vendor_code;
    }

    /**
     * @param stringLength50_Type $dlr__cst_vendor_code
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_vendor_code($dlr__cst_vendor_code)
    {
      $this->dlr__cst_vendor_code = $dlr__cst_vendor_code;
      return $this;
    }

    /**
     * @return stringLength1_Type
     */
    public function getDlr__cst_salt()
    {
      return $this->dlr__cst_salt;
    }

    /**
     * @param stringLength1_Type $dlr__cst_salt
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_salt($dlr__cst_salt)
    {
      $this->dlr__cst_salt = $dlr__cst_salt;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getDlr__cst_sca_key()
    {
      return $this->dlr__cst_sca_key;
    }

    /**
     * @param av_key_Type $dlr__cst_sca_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_sca_key($dlr__cst_sca_key)
    {
      $this->dlr__cst_sca_key = $dlr__cst_sca_key;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getDlr__cst_iterations()
    {
      return $this->dlr__cst_iterations;
    }

    /**
     * @param av_integer_Type $dlr__cst_iterations
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function setDlr__cst_iterations($dlr__cst_iterations)
    {
      $this->dlr__cst_iterations = $dlr__cst_iterations;
      return $this;
    }

}
