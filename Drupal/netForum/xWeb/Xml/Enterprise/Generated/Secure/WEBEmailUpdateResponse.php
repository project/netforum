<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBEmailUpdateResponse
{

    /**
     * @var boolean $WEBEmailUpdateResult
     */
    protected $WEBEmailUpdateResult = null;

    /**
     * @param boolean $WEBEmailUpdateResult
     */
    public function __construct($WEBEmailUpdateResult)
    {
      $this->WEBEmailUpdateResult = $WEBEmailUpdateResult;
    }

    /**
     * @return boolean
     */
    public function getWEBEmailUpdateResult()
    {
      return $this->WEBEmailUpdateResult;
    }

    /**
     * @param boolean $WEBEmailUpdateResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBEmailUpdateResponse
     */
    public function setWEBEmailUpdateResult($WEBEmailUpdateResult)
    {
      $this->WEBEmailUpdateResult = $WEBEmailUpdateResult;
      return $this;
    }

}
