<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class UpdateFacadeObjectResponse
{

    /**
     * @var UpdateFacadeObjectResult $UpdateFacadeObjectResult
     */
    protected $UpdateFacadeObjectResult = null;

    /**
     * @param UpdateFacadeObjectResult $UpdateFacadeObjectResult
     */
    public function __construct($UpdateFacadeObjectResult)
    {
      $this->UpdateFacadeObjectResult = $UpdateFacadeObjectResult;
    }

    /**
     * @return UpdateFacadeObjectResult
     */
    public function getUpdateFacadeObjectResult()
    {
      return $this->UpdateFacadeObjectResult;
    }

    /**
     * @param UpdateFacadeObjectResult $UpdateFacadeObjectResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\UpdateFacadeObjectResponse
     */
    public function setUpdateFacadeObjectResult($UpdateFacadeObjectResult)
    {
      $this->UpdateFacadeObjectResult = $UpdateFacadeObjectResult;
      return $this;
    }

}
