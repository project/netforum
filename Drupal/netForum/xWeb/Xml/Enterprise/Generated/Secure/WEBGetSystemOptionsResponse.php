<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBGetSystemOptionsResponse
{

    /**
     * @var WEBGetSystemOptionsResult $WEBGetSystemOptionsResult
     */
    protected $WEBGetSystemOptionsResult = null;

    /**
     * @param WEBGetSystemOptionsResult $WEBGetSystemOptionsResult
     */
    public function __construct($WEBGetSystemOptionsResult)
    {
      $this->WEBGetSystemOptionsResult = $WEBGetSystemOptionsResult;
    }

    /**
     * @return WEBGetSystemOptionsResult
     */
    public function getWEBGetSystemOptionsResult()
    {
      return $this->WEBGetSystemOptionsResult;
    }

    /**
     * @param WEBGetSystemOptionsResult $WEBGetSystemOptionsResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBGetSystemOptionsResponse
     */
    public function setWEBGetSystemOptionsResult($WEBGetSystemOptionsResult)
    {
      $this->WEBGetSystemOptionsResult = $WEBGetSystemOptionsResult;
      return $this;
    }

}
