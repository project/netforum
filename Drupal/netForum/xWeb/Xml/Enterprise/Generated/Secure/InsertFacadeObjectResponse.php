<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class InsertFacadeObjectResponse
{

    /**
     * @var InsertFacadeObjectResult $InsertFacadeObjectResult
     */
    protected $InsertFacadeObjectResult = null;

    /**
     * @param InsertFacadeObjectResult $InsertFacadeObjectResult
     */
    public function __construct($InsertFacadeObjectResult)
    {
      $this->InsertFacadeObjectResult = $InsertFacadeObjectResult;
    }

    /**
     * @return InsertFacadeObjectResult
     */
    public function getInsertFacadeObjectResult()
    {
      return $this->InsertFacadeObjectResult;
    }

    /**
     * @param InsertFacadeObjectResult $InsertFacadeObjectResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InsertFacadeObjectResponse
     */
    public function setInsertFacadeObjectResult($InsertFacadeObjectResult)
    {
      $this->InsertFacadeObjectResult = $InsertFacadeObjectResult;
      return $this;
    }

}
