<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBOrganizationGetResponse
{

    /**
     * @var OrganizationType $WEBOrganizationGetResult
     */
    protected $WEBOrganizationGetResult = null;

    /**
     * @param OrganizationType $WEBOrganizationGetResult
     */
    public function __construct($WEBOrganizationGetResult)
    {
      $this->WEBOrganizationGetResult = $WEBOrganizationGetResult;
    }

    /**
     * @return OrganizationType
     */
    public function getWEBOrganizationGetResult()
    {
      return $this->WEBOrganizationGetResult;
    }

    /**
     * @param OrganizationType $WEBOrganizationGetResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBOrganizationGetResponse
     */
    public function setWEBOrganizationGetResult($WEBOrganizationGetResult)
    {
      $this->WEBOrganizationGetResult = $WEBOrganizationGetResult;
      return $this;
    }

}
