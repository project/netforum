<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartAccreditationGet
{

    /**
     * @var guid $AccreditationKey
     */
    protected $AccreditationKey = null;

    /**
     * @param guid $AccreditationKey
     */
    public function __construct($AccreditationKey)
    {
      $this->AccreditationKey = $AccreditationKey;
    }

    /**
     * @return guid
     */
    public function getAccreditationKey()
    {
      return $this->AccreditationKey;
    }

    /**
     * @param guid $AccreditationKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartAccreditationGet
     */
    public function setAccreditationKey($AccreditationKey)
    {
      $this->AccreditationKey = $AccreditationKey;
      return $this;
    }

}
