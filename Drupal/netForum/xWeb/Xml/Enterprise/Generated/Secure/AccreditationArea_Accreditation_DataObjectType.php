<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class AccreditationArea_Accreditation_DataObjectType
{

    /**
     * @var av_key_Type $ama_key
     */
    protected $ama_key = null;

    /**
     * @var stringLength50_Type $ama_code
     */
    protected $ama_code = null;

    /**
     * @var stringLength200_Type $ama_desc
     */
    protected $ama_desc = null;

    /**
     * @var av_user_Type $ama_add_user
     */
    protected $ama_add_user = null;

    /**
     * @var av_date_Type $ama_add_date
     */
    protected $ama_add_date = null;

    /**
     * @var av_user_Type $ama_change_user
     */
    protected $ama_change_user = null;

    /**
     * @var av_date_Type $ama_change_date
     */
    protected $ama_change_date = null;

    /**
     * @var av_delete_flag_Type $ama_delete_flag
     */
    protected $ama_delete_flag = null;

    /**
     * @var av_key_Type $ama_key_ext
     */
    protected $ama_key_ext = null;

    /**
     * @var av_key_Type $ama_entity_key
     */
    protected $ama_entity_key = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getAma_key()
    {
      return $this->ama_key;
    }

    /**
     * @param av_key_Type $ama_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_DataObjectType
     */
    public function setAma_key($ama_key)
    {
      $this->ama_key = $ama_key;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getAma_code()
    {
      return $this->ama_code;
    }

    /**
     * @param stringLength50_Type $ama_code
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_DataObjectType
     */
    public function setAma_code($ama_code)
    {
      $this->ama_code = $ama_code;
      return $this;
    }

    /**
     * @return stringLength200_Type
     */
    public function getAma_desc()
    {
      return $this->ama_desc;
    }

    /**
     * @param stringLength200_Type $ama_desc
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_DataObjectType
     */
    public function setAma_desc($ama_desc)
    {
      $this->ama_desc = $ama_desc;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getAma_add_user()
    {
      return $this->ama_add_user;
    }

    /**
     * @param av_user_Type $ama_add_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_DataObjectType
     */
    public function setAma_add_user($ama_add_user)
    {
      $this->ama_add_user = $ama_add_user;
      return $this;
    }

    /**
     * @return av_date_Type
     */
    public function getAma_add_date()
    {
      return $this->ama_add_date;
    }

    /**
     * @param av_date_Type $ama_add_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_DataObjectType
     */
    public function setAma_add_date($ama_add_date)
    {
      $this->ama_add_date = $ama_add_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getAma_change_user()
    {
      return $this->ama_change_user;
    }

    /**
     * @param av_user_Type $ama_change_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_DataObjectType
     */
    public function setAma_change_user($ama_change_user)
    {
      $this->ama_change_user = $ama_change_user;
      return $this;
    }

    /**
     * @return av_date_Type
     */
    public function getAma_change_date()
    {
      return $this->ama_change_date;
    }

    /**
     * @param av_date_Type $ama_change_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_DataObjectType
     */
    public function setAma_change_date($ama_change_date)
    {
      $this->ama_change_date = $ama_change_date;
      return $this;
    }

    /**
     * @return av_delete_flag_Type
     */
    public function getAma_delete_flag()
    {
      return $this->ama_delete_flag;
    }

    /**
     * @param av_delete_flag_Type $ama_delete_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_DataObjectType
     */
    public function setAma_delete_flag($ama_delete_flag)
    {
      $this->ama_delete_flag = $ama_delete_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAma_key_ext()
    {
      return $this->ama_key_ext;
    }

    /**
     * @param av_key_Type $ama_key_ext
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_DataObjectType
     */
    public function setAma_key_ext($ama_key_ext)
    {
      $this->ama_key_ext = $ama_key_ext;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAma_entity_key()
    {
      return $this->ama_entity_key;
    }

    /**
     * @param av_key_Type $ama_entity_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_DataObjectType
     */
    public function setAma_entity_key($ama_entity_key)
    {
      $this->ama_entity_key = $ama_entity_key;
      return $this;
    }

}
