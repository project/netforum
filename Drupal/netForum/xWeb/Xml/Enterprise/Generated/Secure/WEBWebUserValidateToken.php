<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserValidateToken
{

    /**
     * @var string $authenticationToken
     */
    protected $authenticationToken = null;

    /**
     * @param string $authenticationToken
     */
    public function __construct($authenticationToken)
    {
      $this->authenticationToken = $authenticationToken;
    }

    /**
     * @return string
     */
    public function getAuthenticationToken()
    {
      return $this->authenticationToken;
    }

    /**
     * @param string $authenticationToken
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserValidateToken
     */
    public function setAuthenticationToken($authenticationToken)
    {
      $this->authenticationToken = $authenticationToken;
      return $this;
    }

}
