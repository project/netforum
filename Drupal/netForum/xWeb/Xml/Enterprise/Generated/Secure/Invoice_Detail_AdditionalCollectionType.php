<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class Invoice_Detail_AdditionalCollectionType
{

    /**
     * @var InvoiceDetailAdditionalType[] $InvoiceDetailAdditional
     */
    protected $InvoiceDetailAdditional = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return InvoiceDetailAdditionalType[]
     */
    public function getInvoiceDetailAdditional()
    {
      return $this->InvoiceDetailAdditional;
    }

    /**
     * @param InvoiceDetailAdditionalType[] $InvoiceDetailAdditional
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\Invoice_Detail_AdditionalCollectionType
     */
    public function setInvoiceDetailAdditional(array $InvoiceDetailAdditional = null)
    {
      $this->InvoiceDetailAdditional = $InvoiceDetailAdditional;
      return $this;
    }

}
