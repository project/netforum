<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBChaptersGetChapterOfficersResponse
{

    /**
     * @var WEBChaptersGetChapterOfficersResult $WEBChaptersGetChapterOfficersResult
     */
    protected $WEBChaptersGetChapterOfficersResult = null;

    /**
     * @param WEBChaptersGetChapterOfficersResult $WEBChaptersGetChapterOfficersResult
     */
    public function __construct($WEBChaptersGetChapterOfficersResult)
    {
      $this->WEBChaptersGetChapterOfficersResult = $WEBChaptersGetChapterOfficersResult;
    }

    /**
     * @return WEBChaptersGetChapterOfficersResult
     */
    public function getWEBChaptersGetChapterOfficersResult()
    {
      return $this->WEBChaptersGetChapterOfficersResult;
    }

    /**
     * @param WEBChaptersGetChapterOfficersResult $WEBChaptersGetChapterOfficersResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBChaptersGetChapterOfficersResponse
     */
    public function setWEBChaptersGetChapterOfficersResult($WEBChaptersGetChapterOfficersResult)
    {
      $this->WEBChaptersGetChapterOfficersResult = $WEBChaptersGetChapterOfficersResult;
      return $this;
    }

}
