<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBRoleExists
{

    /**
     * @var string $roleName
     */
    protected $roleName = null;

    /**
     * @param string $roleName
     */
    public function __construct($roleName)
    {
      $this->roleName = $roleName;
    }

    /**
     * @return string
     */
    public function getRoleName()
    {
      return $this->roleName;
    }

    /**
     * @param string $roleName
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBRoleExists
     */
    public function setRoleName($roleName)
    {
      $this->roleName = $roleName;
      return $this;
    }

}
