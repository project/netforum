<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCartResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCartResult
     */
    protected $WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCartResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCartResult
     */
    public function __construct($WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCartResult)
    {
      $this->WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCartResult = $WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCartResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCartResult()
    {
      return $this->WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCartResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCartResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCartResponse
     */
    public function setWEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCartResult($WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCartResult)
    {
      $this->WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCartResult = $WEBCentralizedShoppingCartEventRegistrantSetLineItemsWithCartResult;
      return $this;
    }

}
