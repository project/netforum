<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserFindUsersByEmailResponse
{

    /**
     * @var WEBWebUserFindUsersByEmailResult $WEBWebUserFindUsersByEmailResult
     */
    protected $WEBWebUserFindUsersByEmailResult = null;

    /**
     * @param WEBWebUserFindUsersByEmailResult $WEBWebUserFindUsersByEmailResult
     */
    public function __construct($WEBWebUserFindUsersByEmailResult)
    {
      $this->WEBWebUserFindUsersByEmailResult = $WEBWebUserFindUsersByEmailResult;
    }

    /**
     * @return WEBWebUserFindUsersByEmailResult
     */
    public function getWEBWebUserFindUsersByEmailResult()
    {
      return $this->WEBWebUserFindUsersByEmailResult;
    }

    /**
     * @param WEBWebUserFindUsersByEmailResult $WEBWebUserFindUsersByEmailResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserFindUsersByEmailResponse
     */
    public function setWEBWebUserFindUsersByEmailResult($WEBWebUserFindUsersByEmailResult)
    {
      $this->WEBWebUserFindUsersByEmailResult = $WEBWebUserFindUsersByEmailResult;
      return $this;
    }

}
