<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class InvoiceToPayCollectionType
{

    /**
     * @var InvoiceType[] $Invoice
     */
    protected $Invoice = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return InvoiceType[]
     */
    public function getInvoice()
    {
      return $this->Invoice;
    }

    /**
     * @param InvoiceType[] $Invoice
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceToPayCollectionType
     */
    public function setInvoice(array $Invoice = null)
    {
      $this->Invoice = $Invoice;
      return $this;
    }

}
