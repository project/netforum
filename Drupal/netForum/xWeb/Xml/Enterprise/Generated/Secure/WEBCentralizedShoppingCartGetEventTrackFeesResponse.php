<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetEventTrackFeesResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetEventTrackFeesResult $WEBCentralizedShoppingCartGetEventTrackFeesResult
     */
    protected $WEBCentralizedShoppingCartGetEventTrackFeesResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetEventTrackFeesResult $WEBCentralizedShoppingCartGetEventTrackFeesResult
     */
    public function __construct($WEBCentralizedShoppingCartGetEventTrackFeesResult)
    {
      $this->WEBCentralizedShoppingCartGetEventTrackFeesResult = $WEBCentralizedShoppingCartGetEventTrackFeesResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetEventTrackFeesResult
     */
    public function getWEBCentralizedShoppingCartGetEventTrackFeesResult()
    {
      return $this->WEBCentralizedShoppingCartGetEventTrackFeesResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetEventTrackFeesResult $WEBCentralizedShoppingCartGetEventTrackFeesResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetEventTrackFeesResponse
     */
    public function setWEBCentralizedShoppingCartGetEventTrackFeesResult($WEBCentralizedShoppingCartGetEventTrackFeesResult)
    {
      $this->WEBCentralizedShoppingCartGetEventTrackFeesResult = $WEBCentralizedShoppingCartGetEventTrackFeesResult;
      return $this;
    }

}
