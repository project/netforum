<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartMembershipGetPackageListResponse
{

    /**
     * @var WEBCentralizedShoppingCartMembershipGetPackageListResult $WEBCentralizedShoppingCartMembershipGetPackageListResult
     */
    protected $WEBCentralizedShoppingCartMembershipGetPackageListResult = null;

    /**
     * @param WEBCentralizedShoppingCartMembershipGetPackageListResult $WEBCentralizedShoppingCartMembershipGetPackageListResult
     */
    public function __construct($WEBCentralizedShoppingCartMembershipGetPackageListResult)
    {
      $this->WEBCentralizedShoppingCartMembershipGetPackageListResult = $WEBCentralizedShoppingCartMembershipGetPackageListResult;
    }

    /**
     * @return WEBCentralizedShoppingCartMembershipGetPackageListResult
     */
    public function getWEBCentralizedShoppingCartMembershipGetPackageListResult()
    {
      return $this->WEBCentralizedShoppingCartMembershipGetPackageListResult;
    }

    /**
     * @param WEBCentralizedShoppingCartMembershipGetPackageListResult $WEBCentralizedShoppingCartMembershipGetPackageListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartMembershipGetPackageListResponse
     */
    public function setWEBCentralizedShoppingCartMembershipGetPackageListResult($WEBCentralizedShoppingCartMembershipGetPackageListResult)
    {
      $this->WEBCentralizedShoppingCartMembershipGetPackageListResult = $WEBCentralizedShoppingCartMembershipGetPackageListResult;
      return $this;
    }

}
