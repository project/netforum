<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResult $WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResult
     */
    protected $WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResult $WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResult
     */
    public function __construct($WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResult)
    {
      $this->WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResult = $WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResult
     */
    public function getWEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResult()
    {
      return $this->WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResult $WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResponse
     */
    public function setWEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResult($WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResult)
    {
      $this->WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResult = $WEBCentralizedShoppingCartGetEventRegistrantSourceCodeListResult;
      return $this;
    }

}
