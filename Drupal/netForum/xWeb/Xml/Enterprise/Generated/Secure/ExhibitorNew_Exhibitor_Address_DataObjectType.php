<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class ExhibitorNew_Exhibitor_Address_DataObjectType
{

    /**
     * @var av_key_Type $ead__cxa_cst_key
     */
    protected $ead__cxa_cst_key = null;

    /**
     * @var av_key_Type $ead__cxa_adr_key
     */
    protected $ead__cxa_adr_key = null;

    /**
     * @var av_date_small_Type $ead__cxa_add_date
     */
    protected $ead__cxa_add_date = null;

    /**
     * @var av_user_Type $ead__cxa_add_user
     */
    protected $ead__cxa_add_user = null;

    /**
     * @var av_date_small_Type $ead__cxa_change_date
     */
    protected $ead__cxa_change_date = null;

    /**
     * @var av_user_Type $ead__cxa_change_user
     */
    protected $ead__cxa_change_user = null;

    /**
     * @var av_delete_flag_Type $ead__cxa_delete_flag
     */
    protected $ead__cxa_delete_flag = null;

    /**
     * @var av_key_Type $ead__cxa_key
     */
    protected $ead__cxa_key = null;

    /**
     * @var av_key_Type $ead__cxa_key_ext
     */
    protected $ead__cxa_key_ext = null;

    /**
     * @var av_flag_Type $ead__cxa_on_hold_flag
     */
    protected $ead__cxa_on_hold_flag = null;

    /**
     * @var av_key_Type $ead__cxa_adh_key
     */
    protected $ead__cxa_adh_key = null;

    /**
     * @var av_key_Type $ead__cxa_adt_key
     */
    protected $ead__cxa_adt_key = null;

    /**
     * @var av_date_small_Type $ead__cxa_seasonal_from_date
     */
    protected $ead__cxa_seasonal_from_date = null;

    /**
     * @var stringLength450_Type $ead__cxa_mailing_label
     */
    protected $ead__cxa_mailing_label = null;

    /**
     * @var av_date_small_Type $ead__cxa_seasonal_through_date
     */
    protected $ead__cxa_seasonal_through_date = null;

    /**
     * @var av_key_Type $ead__cxa_entity_key
     */
    protected $ead__cxa_entity_key = null;

    /**
     * @var stringLength50_Type $ead__cxa_mail_stop
     */
    protected $ead__cxa_mail_stop = null;

    /**
     * @var stringLength450_Type $ead__cxa_mailing_label_html
     */
    protected $ead__cxa_mailing_label_html = null;

    /**
     * @var av_date_small_Type $ead__cxa_on_hold_from
     */
    protected $ead__cxa_on_hold_from = null;

    /**
     * @var av_date_small_Type $ead__cxa_on_hold_through
     */
    protected $ead__cxa_on_hold_through = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getEad__cxa_cst_key()
    {
      return $this->ead__cxa_cst_key;
    }

    /**
     * @param av_key_Type $ead__cxa_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorNew_Exhibitor_Address_DataObjectType
     */
    public function setEad__cxa_cst_key($ead__cxa_cst_key)
    {
      $this->ead__cxa_cst_key = $ead__cxa_cst_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getEad__cxa_adr_key()
    {
      return $this->ead__cxa_adr_key;
    }

    /**
     * @param av_key_Type $ead__cxa_adr_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorNew_Exhibitor_Address_DataObjectType
     */
    public function setEad__cxa_adr_key($ead__cxa_adr_key)
    {
      $this->ead__cxa_adr_key = $ead__cxa_adr_key;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getEad__cxa_add_date()
    {
      return $this->ead__cxa_add_date;
    }

    /**
     * @param av_date_small_Type $ead__cxa_add_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorNew_Exhibitor_Address_DataObjectType
     */
    public function setEad__cxa_add_date($ead__cxa_add_date)
    {
      $this->ead__cxa_add_date = $ead__cxa_add_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getEad__cxa_add_user()
    {
      return $this->ead__cxa_add_user;
    }

    /**
     * @param av_user_Type $ead__cxa_add_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorNew_Exhibitor_Address_DataObjectType
     */
    public function setEad__cxa_add_user($ead__cxa_add_user)
    {
      $this->ead__cxa_add_user = $ead__cxa_add_user;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getEad__cxa_change_date()
    {
      return $this->ead__cxa_change_date;
    }

    /**
     * @param av_date_small_Type $ead__cxa_change_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorNew_Exhibitor_Address_DataObjectType
     */
    public function setEad__cxa_change_date($ead__cxa_change_date)
    {
      $this->ead__cxa_change_date = $ead__cxa_change_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getEad__cxa_change_user()
    {
      return $this->ead__cxa_change_user;
    }

    /**
     * @param av_user_Type $ead__cxa_change_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorNew_Exhibitor_Address_DataObjectType
     */
    public function setEad__cxa_change_user($ead__cxa_change_user)
    {
      $this->ead__cxa_change_user = $ead__cxa_change_user;
      return $this;
    }

    /**
     * @return av_delete_flag_Type
     */
    public function getEad__cxa_delete_flag()
    {
      return $this->ead__cxa_delete_flag;
    }

    /**
     * @param av_delete_flag_Type $ead__cxa_delete_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorNew_Exhibitor_Address_DataObjectType
     */
    public function setEad__cxa_delete_flag($ead__cxa_delete_flag)
    {
      $this->ead__cxa_delete_flag = $ead__cxa_delete_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getEad__cxa_key()
    {
      return $this->ead__cxa_key;
    }

    /**
     * @param av_key_Type $ead__cxa_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorNew_Exhibitor_Address_DataObjectType
     */
    public function setEad__cxa_key($ead__cxa_key)
    {
      $this->ead__cxa_key = $ead__cxa_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getEad__cxa_key_ext()
    {
      return $this->ead__cxa_key_ext;
    }

    /**
     * @param av_key_Type $ead__cxa_key_ext
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorNew_Exhibitor_Address_DataObjectType
     */
    public function setEad__cxa_key_ext($ead__cxa_key_ext)
    {
      $this->ead__cxa_key_ext = $ead__cxa_key_ext;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getEad__cxa_on_hold_flag()
    {
      return $this->ead__cxa_on_hold_flag;
    }

    /**
     * @param av_flag_Type $ead__cxa_on_hold_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorNew_Exhibitor_Address_DataObjectType
     */
    public function setEad__cxa_on_hold_flag($ead__cxa_on_hold_flag)
    {
      $this->ead__cxa_on_hold_flag = $ead__cxa_on_hold_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getEad__cxa_adh_key()
    {
      return $this->ead__cxa_adh_key;
    }

    /**
     * @param av_key_Type $ead__cxa_adh_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorNew_Exhibitor_Address_DataObjectType
     */
    public function setEad__cxa_adh_key($ead__cxa_adh_key)
    {
      $this->ead__cxa_adh_key = $ead__cxa_adh_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getEad__cxa_adt_key()
    {
      return $this->ead__cxa_adt_key;
    }

    /**
     * @param av_key_Type $ead__cxa_adt_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorNew_Exhibitor_Address_DataObjectType
     */
    public function setEad__cxa_adt_key($ead__cxa_adt_key)
    {
      $this->ead__cxa_adt_key = $ead__cxa_adt_key;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getEad__cxa_seasonal_from_date()
    {
      return $this->ead__cxa_seasonal_from_date;
    }

    /**
     * @param av_date_small_Type $ead__cxa_seasonal_from_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorNew_Exhibitor_Address_DataObjectType
     */
    public function setEad__cxa_seasonal_from_date($ead__cxa_seasonal_from_date)
    {
      $this->ead__cxa_seasonal_from_date = $ead__cxa_seasonal_from_date;
      return $this;
    }

    /**
     * @return stringLength450_Type
     */
    public function getEad__cxa_mailing_label()
    {
      return $this->ead__cxa_mailing_label;
    }

    /**
     * @param stringLength450_Type $ead__cxa_mailing_label
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorNew_Exhibitor_Address_DataObjectType
     */
    public function setEad__cxa_mailing_label($ead__cxa_mailing_label)
    {
      $this->ead__cxa_mailing_label = $ead__cxa_mailing_label;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getEad__cxa_seasonal_through_date()
    {
      return $this->ead__cxa_seasonal_through_date;
    }

    /**
     * @param av_date_small_Type $ead__cxa_seasonal_through_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorNew_Exhibitor_Address_DataObjectType
     */
    public function setEad__cxa_seasonal_through_date($ead__cxa_seasonal_through_date)
    {
      $this->ead__cxa_seasonal_through_date = $ead__cxa_seasonal_through_date;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getEad__cxa_entity_key()
    {
      return $this->ead__cxa_entity_key;
    }

    /**
     * @param av_key_Type $ead__cxa_entity_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorNew_Exhibitor_Address_DataObjectType
     */
    public function setEad__cxa_entity_key($ead__cxa_entity_key)
    {
      $this->ead__cxa_entity_key = $ead__cxa_entity_key;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getEad__cxa_mail_stop()
    {
      return $this->ead__cxa_mail_stop;
    }

    /**
     * @param stringLength50_Type $ead__cxa_mail_stop
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorNew_Exhibitor_Address_DataObjectType
     */
    public function setEad__cxa_mail_stop($ead__cxa_mail_stop)
    {
      $this->ead__cxa_mail_stop = $ead__cxa_mail_stop;
      return $this;
    }

    /**
     * @return stringLength450_Type
     */
    public function getEad__cxa_mailing_label_html()
    {
      return $this->ead__cxa_mailing_label_html;
    }

    /**
     * @param stringLength450_Type $ead__cxa_mailing_label_html
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorNew_Exhibitor_Address_DataObjectType
     */
    public function setEad__cxa_mailing_label_html($ead__cxa_mailing_label_html)
    {
      $this->ead__cxa_mailing_label_html = $ead__cxa_mailing_label_html;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getEad__cxa_on_hold_from()
    {
      return $this->ead__cxa_on_hold_from;
    }

    /**
     * @param av_date_small_Type $ead__cxa_on_hold_from
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorNew_Exhibitor_Address_DataObjectType
     */
    public function setEad__cxa_on_hold_from($ead__cxa_on_hold_from)
    {
      $this->ead__cxa_on_hold_from = $ead__cxa_on_hold_from;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getEad__cxa_on_hold_through()
    {
      return $this->ead__cxa_on_hold_through;
    }

    /**
     * @param av_date_small_Type $ead__cxa_on_hold_through
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ExhibitorNew_Exhibitor_Address_DataObjectType
     */
    public function setEad__cxa_on_hold_through($ead__cxa_on_hold_through)
    {
      $this->ead__cxa_on_hold_through = $ead__cxa_on_hold_through;
      return $this;
    }

}
