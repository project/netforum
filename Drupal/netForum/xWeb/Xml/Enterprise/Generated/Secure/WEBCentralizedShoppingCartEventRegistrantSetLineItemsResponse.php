<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartEventRegistrantSetLineItemsResponse
{

    /**
     * @var EventsRegistrantType $WEBCentralizedShoppingCartEventRegistrantSetLineItemsResult
     */
    protected $WEBCentralizedShoppingCartEventRegistrantSetLineItemsResult = null;

    /**
     * @param EventsRegistrantType $WEBCentralizedShoppingCartEventRegistrantSetLineItemsResult
     */
    public function __construct($WEBCentralizedShoppingCartEventRegistrantSetLineItemsResult)
    {
      $this->WEBCentralizedShoppingCartEventRegistrantSetLineItemsResult = $WEBCentralizedShoppingCartEventRegistrantSetLineItemsResult;
    }

    /**
     * @return EventsRegistrantType
     */
    public function getWEBCentralizedShoppingCartEventRegistrantSetLineItemsResult()
    {
      return $this->WEBCentralizedShoppingCartEventRegistrantSetLineItemsResult;
    }

    /**
     * @param EventsRegistrantType $WEBCentralizedShoppingCartEventRegistrantSetLineItemsResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartEventRegistrantSetLineItemsResponse
     */
    public function setWEBCentralizedShoppingCartEventRegistrantSetLineItemsResult($WEBCentralizedShoppingCartEventRegistrantSetLineItemsResult)
    {
      $this->WEBCentralizedShoppingCartEventRegistrantSetLineItemsResult = $WEBCentralizedShoppingCartEventRegistrantSetLineItemsResult;
      return $this;
    }

}
