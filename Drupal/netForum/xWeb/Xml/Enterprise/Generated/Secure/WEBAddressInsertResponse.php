<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBAddressInsertResponse
{

    /**
     * @var CustomerAddressType $WEBAddressInsertResult
     */
    protected $WEBAddressInsertResult = null;

    /**
     * @param CustomerAddressType $WEBAddressInsertResult
     */
    public function __construct($WEBAddressInsertResult)
    {
      $this->WEBAddressInsertResult = $WEBAddressInsertResult;
    }

    /**
     * @return CustomerAddressType
     */
    public function getWEBAddressInsertResult()
    {
      return $this->WEBAddressInsertResult;
    }

    /**
     * @param CustomerAddressType $WEBAddressInsertResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBAddressInsertResponse
     */
    public function setWEBAddressInsertResult($WEBAddressInsertResult)
    {
      $this->WEBAddressInsertResult = $WEBAddressInsertResult;
      return $this;
    }

}
