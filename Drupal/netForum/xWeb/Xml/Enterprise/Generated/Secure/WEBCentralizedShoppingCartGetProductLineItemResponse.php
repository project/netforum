<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetProductLineItemResponse
{

    /**
     * @var InvoiceDetailType $WEBCentralizedShoppingCartGetProductLineItemResult
     */
    protected $WEBCentralizedShoppingCartGetProductLineItemResult = null;

    /**
     * @param InvoiceDetailType $WEBCentralizedShoppingCartGetProductLineItemResult
     */
    public function __construct($WEBCentralizedShoppingCartGetProductLineItemResult)
    {
      $this->WEBCentralizedShoppingCartGetProductLineItemResult = $WEBCentralizedShoppingCartGetProductLineItemResult;
    }

    /**
     * @return InvoiceDetailType
     */
    public function getWEBCentralizedShoppingCartGetProductLineItemResult()
    {
      return $this->WEBCentralizedShoppingCartGetProductLineItemResult;
    }

    /**
     * @param InvoiceDetailType $WEBCentralizedShoppingCartGetProductLineItemResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetProductLineItemResponse
     */
    public function setWEBCentralizedShoppingCartGetProductLineItemResult($WEBCentralizedShoppingCartGetProductLineItemResult)
    {
      $this->WEBCentralizedShoppingCartGetProductLineItemResult = $WEBCentralizedShoppingCartGetProductLineItemResult;
      return $this;
    }

}
