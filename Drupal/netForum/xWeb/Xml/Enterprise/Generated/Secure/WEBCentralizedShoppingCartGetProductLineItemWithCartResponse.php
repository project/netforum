<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetProductLineItemWithCartResponse
{

    /**
     * @var InvoiceDetailType $WEBCentralizedShoppingCartGetProductLineItemWithCartResult
     */
    protected $WEBCentralizedShoppingCartGetProductLineItemWithCartResult = null;

    /**
     * @param InvoiceDetailType $WEBCentralizedShoppingCartGetProductLineItemWithCartResult
     */
    public function __construct($WEBCentralizedShoppingCartGetProductLineItemWithCartResult)
    {
      $this->WEBCentralizedShoppingCartGetProductLineItemWithCartResult = $WEBCentralizedShoppingCartGetProductLineItemWithCartResult;
    }

    /**
     * @return InvoiceDetailType
     */
    public function getWEBCentralizedShoppingCartGetProductLineItemWithCartResult()
    {
      return $this->WEBCentralizedShoppingCartGetProductLineItemWithCartResult;
    }

    /**
     * @param InvoiceDetailType $WEBCentralizedShoppingCartGetProductLineItemWithCartResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetProductLineItemWithCartResponse
     */
    public function setWEBCentralizedShoppingCartGetProductLineItemWithCartResult($WEBCentralizedShoppingCartGetProductLineItemWithCartResult)
    {
      $this->WEBCentralizedShoppingCartGetProductLineItemWithCartResult = $WEBCentralizedShoppingCartGetProductLineItemWithCartResult;
      return $this;
    }

}
