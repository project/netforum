<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResult $WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResult
     */
    protected $WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResult $WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResult
     */
    public function __construct($WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResult)
    {
      $this->WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResult = $WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResult
     */
    public function getWEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResult()
    {
      return $this->WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResult $WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResponse
     */
    public function setWEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResult($WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResult)
    {
      $this->WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResult = $WEBCentralizedShoppingCartGetSubscriptionList_Ignore_PCResult;
      return $this;
    }

}
