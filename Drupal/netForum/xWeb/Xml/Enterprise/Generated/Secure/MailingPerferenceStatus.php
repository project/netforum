<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class MailingPerferenceStatus
{
    const __default = 'OptIn';
    const OptIn = 'OptIn';
    const OptOut = 'OptOut';
    const Undefined = 'Undefined';


}
