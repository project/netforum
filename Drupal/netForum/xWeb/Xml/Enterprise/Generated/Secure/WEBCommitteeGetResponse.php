<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCommitteeGetResponse
{

    /**
     * @var CommitteeType $WEBCommitteeGetResult
     */
    protected $WEBCommitteeGetResult = null;

    /**
     * @param CommitteeType $WEBCommitteeGetResult
     */
    public function __construct($WEBCommitteeGetResult)
    {
      $this->WEBCommitteeGetResult = $WEBCommitteeGetResult;
    }

    /**
     * @return CommitteeType
     */
    public function getWEBCommitteeGetResult()
    {
      return $this->WEBCommitteeGetResult;
    }

    /**
     * @param CommitteeType $WEBCommitteeGetResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCommitteeGetResponse
     */
    public function setWEBCommitteeGetResult($WEBCommitteeGetResult)
    {
      $this->WEBCommitteeGetResult = $WEBCommitteeGetResult;
      return $this;
    }

}
