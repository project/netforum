<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class AccreditationAreaType
{

    /**
     * @var av_key_Type $CurrentKey
     */
    protected $CurrentKey = null;

    /**
     * @var AccreditationArea_Accreditation_Customer_DataObjectType $Accreditation_Customer
     */
    protected $Accreditation_Customer = null;

    /**
     * @var AccreditationArea_Customer_DataObjectType $Customer
     */
    protected $Customer = null;

    /**
     * @var AccreditationArea_Customer_Address_DataObjectType $Customer_Address
     */
    protected $Customer_Address = null;

    /**
     * @var AccreditationArea_Customer_Primary_Address_DataObjectType $Customer_Primary_Address
     */
    protected $Customer_Primary_Address = null;

    /**
     * @var AccreditationArea_Organization_DataObjectType $Organization
     */
    protected $Organization = null;

    /**
     * @var AccreditationArea_Accreditation_DataObjectType $Accreditation
     */
    protected $Accreditation = null;

    /**
     * @var AccreditationArea_Accreditation_Status_DataObjectType $Accreditation_Status
     */
    protected $Accreditation_Status = null;

    /**
     * @var AccreditationArea_Accreditation_Type_DataObjectType $Accreditation_Type
     */
    protected $Accreditation_Type = null;

    /**
     * @var AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType $Individual_Organization_CEO_Affiliation
     */
    protected $Individual_Organization_CEO_Affiliation = null;

    /**
     * @var AccreditationArea_Affiliated_CEO_DataObjectType $Affiliated_CEO
     */
    protected $Affiliated_CEO = null;

    /**
     * @var AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType $Individual_Organization_CAO_Affiliation
     */
    protected $Individual_Organization_CAO_Affiliation = null;

    /**
     * @var AccreditationArea_Affiliated_CAO_DataObjectType $Affiliated_CAO
     */
    protected $Affiliated_CAO = null;

    /**
     * @var AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType $Individual_Organization_CMOA_ffiliation
     */
    protected $Individual_Organization_CMOA_ffiliation = null;

    /**
     * @var AccreditationArea_Affiliated_CMO_DataObjectType $Affiliated_CMO
     */
    protected $Affiliated_CMO = null;

    /**
     * @var AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType $Individual_Organization_Survey_Contact_Affiliation
     */
    protected $Individual_Organization_Survey_Contact_Affiliation = null;

    /**
     * @var AccreditationArea_Affiliated_Survey_Contact_DataObjectType $Affiliated_Survey_Contact
     */
    protected $Affiliated_Survey_Contact = null;

    /**
     * @var AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType $Individual_Decision_Letter_Recipient
     */
    protected $Individual_Decision_Letter_Recipient = null;

    /**
     * @var AccreditationArea_Decision_Letter_Recipient_DataObjectType $Decision_Letter_Recipient
     */
    protected $Decision_Letter_Recipient = null;

    /**
     * @var AccreditationArea_Invoice_DataObjectType $Invoice
     */
    protected $Invoice = null;

    /**
     * @var AccreditationArea_Payment_DataObjectType $Payment
     */
    protected $Payment = null;

    /**
     * @var AccreditationArea_Payment_Info_DataObjectType $Payment_Info
     */
    protected $Payment_Info = null;

    /**
     * @var AccreditationArea_Payment_Method_DataObjectType $Payment_Method
     */
    protected $Payment_Method = null;

    /**
     * @var Invoice_DetailCollectionType $Invoice_DetailCollection
     */
    protected $Invoice_DetailCollection = null;

    /**
     * @param AccreditationArea_Accreditation_Customer_DataObjectType $Accreditation_Customer
     * @param AccreditationArea_Customer_DataObjectType $Customer
     * @param AccreditationArea_Customer_Address_DataObjectType $Customer_Address
     * @param AccreditationArea_Customer_Primary_Address_DataObjectType $Customer_Primary_Address
     * @param AccreditationArea_Organization_DataObjectType $Organization
     * @param AccreditationArea_Accreditation_DataObjectType $Accreditation
     * @param AccreditationArea_Accreditation_Status_DataObjectType $Accreditation_Status
     * @param AccreditationArea_Accreditation_Type_DataObjectType $Accreditation_Type
     * @param AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType $Individual_Organization_CEO_Affiliation
     * @param AccreditationArea_Affiliated_CEO_DataObjectType $Affiliated_CEO
     * @param AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType $Individual_Organization_CAO_Affiliation
     * @param AccreditationArea_Affiliated_CAO_DataObjectType $Affiliated_CAO
     * @param AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType $Individual_Organization_CMOA_ffiliation
     * @param AccreditationArea_Affiliated_CMO_DataObjectType $Affiliated_CMO
     * @param AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType $Individual_Organization_Survey_Contact_Affiliation
     * @param AccreditationArea_Affiliated_Survey_Contact_DataObjectType $Affiliated_Survey_Contact
     * @param AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType $Individual_Decision_Letter_Recipient
     * @param AccreditationArea_Decision_Letter_Recipient_DataObjectType $Decision_Letter_Recipient
     * @param AccreditationArea_Invoice_DataObjectType $Invoice
     * @param AccreditationArea_Payment_DataObjectType $Payment
     * @param AccreditationArea_Payment_Info_DataObjectType $Payment_Info
     * @param AccreditationArea_Payment_Method_DataObjectType $Payment_Method
     * @param Invoice_DetailCollectionType $Invoice_DetailCollection
     */
    public function __construct($Accreditation_Customer, $Customer, $Customer_Address, $Customer_Primary_Address, $Organization, $Accreditation, $Accreditation_Status, $Accreditation_Type, $Individual_Organization_CEO_Affiliation, $Affiliated_CEO, $Individual_Organization_CAO_Affiliation, $Affiliated_CAO, $Individual_Organization_CMOA_ffiliation, $Affiliated_CMO, $Individual_Organization_Survey_Contact_Affiliation, $Affiliated_Survey_Contact, $Individual_Decision_Letter_Recipient, $Decision_Letter_Recipient, $Invoice, $Payment, $Payment_Info, $Payment_Method, $Invoice_DetailCollection)
    {
      $this->Accreditation_Customer = $Accreditation_Customer;
      $this->Customer = $Customer;
      $this->Customer_Address = $Customer_Address;
      $this->Customer_Primary_Address = $Customer_Primary_Address;
      $this->Organization = $Organization;
      $this->Accreditation = $Accreditation;
      $this->Accreditation_Status = $Accreditation_Status;
      $this->Accreditation_Type = $Accreditation_Type;
      $this->Individual_Organization_CEO_Affiliation = $Individual_Organization_CEO_Affiliation;
      $this->Affiliated_CEO = $Affiliated_CEO;
      $this->Individual_Organization_CAO_Affiliation = $Individual_Organization_CAO_Affiliation;
      $this->Affiliated_CAO = $Affiliated_CAO;
      $this->Individual_Organization_CMOA_ffiliation = $Individual_Organization_CMOA_ffiliation;
      $this->Affiliated_CMO = $Affiliated_CMO;
      $this->Individual_Organization_Survey_Contact_Affiliation = $Individual_Organization_Survey_Contact_Affiliation;
      $this->Affiliated_Survey_Contact = $Affiliated_Survey_Contact;
      $this->Individual_Decision_Letter_Recipient = $Individual_Decision_Letter_Recipient;
      $this->Decision_Letter_Recipient = $Decision_Letter_Recipient;
      $this->Invoice = $Invoice;
      $this->Payment = $Payment;
      $this->Payment_Info = $Payment_Info;
      $this->Payment_Method = $Payment_Method;
      $this->Invoice_DetailCollection = $Invoice_DetailCollection;
    }

    /**
     * @return av_key_Type
     */
    public function getCurrentKey()
    {
      return $this->CurrentKey;
    }

    /**
     * @param av_key_Type $CurrentKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setCurrentKey($CurrentKey)
    {
      $this->CurrentKey = $CurrentKey;
      return $this;
    }

    /**
     * @return AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function getAccreditation_Customer()
    {
      return $this->Accreditation_Customer;
    }

    /**
     * @param AccreditationArea_Accreditation_Customer_DataObjectType $Accreditation_Customer
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setAccreditation_Customer($Accreditation_Customer)
    {
      $this->Accreditation_Customer = $Accreditation_Customer;
      return $this;
    }

    /**
     * @return AccreditationArea_Customer_DataObjectType
     */
    public function getCustomer()
    {
      return $this->Customer;
    }

    /**
     * @param AccreditationArea_Customer_DataObjectType $Customer
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setCustomer($Customer)
    {
      $this->Customer = $Customer;
      return $this;
    }

    /**
     * @return AccreditationArea_Customer_Address_DataObjectType
     */
    public function getCustomer_Address()
    {
      return $this->Customer_Address;
    }

    /**
     * @param AccreditationArea_Customer_Address_DataObjectType $Customer_Address
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setCustomer_Address($Customer_Address)
    {
      $this->Customer_Address = $Customer_Address;
      return $this;
    }

    /**
     * @return AccreditationArea_Customer_Primary_Address_DataObjectType
     */
    public function getCustomer_Primary_Address()
    {
      return $this->Customer_Primary_Address;
    }

    /**
     * @param AccreditationArea_Customer_Primary_Address_DataObjectType $Customer_Primary_Address
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setCustomer_Primary_Address($Customer_Primary_Address)
    {
      $this->Customer_Primary_Address = $Customer_Primary_Address;
      return $this;
    }

    /**
     * @return AccreditationArea_Organization_DataObjectType
     */
    public function getOrganization()
    {
      return $this->Organization;
    }

    /**
     * @param AccreditationArea_Organization_DataObjectType $Organization
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setOrganization($Organization)
    {
      $this->Organization = $Organization;
      return $this;
    }

    /**
     * @return AccreditationArea_Accreditation_DataObjectType
     */
    public function getAccreditation()
    {
      return $this->Accreditation;
    }

    /**
     * @param AccreditationArea_Accreditation_DataObjectType $Accreditation
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setAccreditation($Accreditation)
    {
      $this->Accreditation = $Accreditation;
      return $this;
    }

    /**
     * @return AccreditationArea_Accreditation_Status_DataObjectType
     */
    public function getAccreditation_Status()
    {
      return $this->Accreditation_Status;
    }

    /**
     * @param AccreditationArea_Accreditation_Status_DataObjectType $Accreditation_Status
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setAccreditation_Status($Accreditation_Status)
    {
      $this->Accreditation_Status = $Accreditation_Status;
      return $this;
    }

    /**
     * @return AccreditationArea_Accreditation_Type_DataObjectType
     */
    public function getAccreditation_Type()
    {
      return $this->Accreditation_Type;
    }

    /**
     * @param AccreditationArea_Accreditation_Type_DataObjectType $Accreditation_Type
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setAccreditation_Type($Accreditation_Type)
    {
      $this->Accreditation_Type = $Accreditation_Type;
      return $this;
    }

    /**
     * @return AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType
     */
    public function getIndividual_Organization_CEO_Affiliation()
    {
      return $this->Individual_Organization_CEO_Affiliation;
    }

    /**
     * @param AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType $Individual_Organization_CEO_Affiliation
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setIndividual_Organization_CEO_Affiliation($Individual_Organization_CEO_Affiliation)
    {
      $this->Individual_Organization_CEO_Affiliation = $Individual_Organization_CEO_Affiliation;
      return $this;
    }

    /**
     * @return AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function getAffiliated_CEO()
    {
      return $this->Affiliated_CEO;
    }

    /**
     * @param AccreditationArea_Affiliated_CEO_DataObjectType $Affiliated_CEO
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setAffiliated_CEO($Affiliated_CEO)
    {
      $this->Affiliated_CEO = $Affiliated_CEO;
      return $this;
    }

    /**
     * @return AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType
     */
    public function getIndividual_Organization_CAO_Affiliation()
    {
      return $this->Individual_Organization_CAO_Affiliation;
    }

    /**
     * @param AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType $Individual_Organization_CAO_Affiliation
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setIndividual_Organization_CAO_Affiliation($Individual_Organization_CAO_Affiliation)
    {
      $this->Individual_Organization_CAO_Affiliation = $Individual_Organization_CAO_Affiliation;
      return $this;
    }

    /**
     * @return AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function getAffiliated_CAO()
    {
      return $this->Affiliated_CAO;
    }

    /**
     * @param AccreditationArea_Affiliated_CAO_DataObjectType $Affiliated_CAO
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setAffiliated_CAO($Affiliated_CAO)
    {
      $this->Affiliated_CAO = $Affiliated_CAO;
      return $this;
    }

    /**
     * @return AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType
     */
    public function getIndividual_Organization_CMOA_ffiliation()
    {
      return $this->Individual_Organization_CMOA_ffiliation;
    }

    /**
     * @param AccreditationArea_Individual_Organization_CMOA_ffiliation_DataObjectType $Individual_Organization_CMOA_ffiliation
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setIndividual_Organization_CMOA_ffiliation($Individual_Organization_CMOA_ffiliation)
    {
      $this->Individual_Organization_CMOA_ffiliation = $Individual_Organization_CMOA_ffiliation;
      return $this;
    }

    /**
     * @return AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function getAffiliated_CMO()
    {
      return $this->Affiliated_CMO;
    }

    /**
     * @param AccreditationArea_Affiliated_CMO_DataObjectType $Affiliated_CMO
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setAffiliated_CMO($Affiliated_CMO)
    {
      $this->Affiliated_CMO = $Affiliated_CMO;
      return $this;
    }

    /**
     * @return AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType
     */
    public function getIndividual_Organization_Survey_Contact_Affiliation()
    {
      return $this->Individual_Organization_Survey_Contact_Affiliation;
    }

    /**
     * @param AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType $Individual_Organization_Survey_Contact_Affiliation
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setIndividual_Organization_Survey_Contact_Affiliation($Individual_Organization_Survey_Contact_Affiliation)
    {
      $this->Individual_Organization_Survey_Contact_Affiliation = $Individual_Organization_Survey_Contact_Affiliation;
      return $this;
    }

    /**
     * @return AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function getAffiliated_Survey_Contact()
    {
      return $this->Affiliated_Survey_Contact;
    }

    /**
     * @param AccreditationArea_Affiliated_Survey_Contact_DataObjectType $Affiliated_Survey_Contact
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setAffiliated_Survey_Contact($Affiliated_Survey_Contact)
    {
      $this->Affiliated_Survey_Contact = $Affiliated_Survey_Contact;
      return $this;
    }

    /**
     * @return AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType
     */
    public function getIndividual_Decision_Letter_Recipient()
    {
      return $this->Individual_Decision_Letter_Recipient;
    }

    /**
     * @param AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType $Individual_Decision_Letter_Recipient
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setIndividual_Decision_Letter_Recipient($Individual_Decision_Letter_Recipient)
    {
      $this->Individual_Decision_Letter_Recipient = $Individual_Decision_Letter_Recipient;
      return $this;
    }

    /**
     * @return AccreditationArea_Decision_Letter_Recipient_DataObjectType
     */
    public function getDecision_Letter_Recipient()
    {
      return $this->Decision_Letter_Recipient;
    }

    /**
     * @param AccreditationArea_Decision_Letter_Recipient_DataObjectType $Decision_Letter_Recipient
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setDecision_Letter_Recipient($Decision_Letter_Recipient)
    {
      $this->Decision_Letter_Recipient = $Decision_Letter_Recipient;
      return $this;
    }

    /**
     * @return AccreditationArea_Invoice_DataObjectType
     */
    public function getInvoice()
    {
      return $this->Invoice;
    }

    /**
     * @param AccreditationArea_Invoice_DataObjectType $Invoice
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setInvoice($Invoice)
    {
      $this->Invoice = $Invoice;
      return $this;
    }

    /**
     * @return AccreditationArea_Payment_DataObjectType
     */
    public function getPayment()
    {
      return $this->Payment;
    }

    /**
     * @param AccreditationArea_Payment_DataObjectType $Payment
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setPayment($Payment)
    {
      $this->Payment = $Payment;
      return $this;
    }

    /**
     * @return AccreditationArea_Payment_Info_DataObjectType
     */
    public function getPayment_Info()
    {
      return $this->Payment_Info;
    }

    /**
     * @param AccreditationArea_Payment_Info_DataObjectType $Payment_Info
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setPayment_Info($Payment_Info)
    {
      $this->Payment_Info = $Payment_Info;
      return $this;
    }

    /**
     * @return AccreditationArea_Payment_Method_DataObjectType
     */
    public function getPayment_Method()
    {
      return $this->Payment_Method;
    }

    /**
     * @param AccreditationArea_Payment_Method_DataObjectType $Payment_Method
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setPayment_Method($Payment_Method)
    {
      $this->Payment_Method = $Payment_Method;
      return $this;
    }

    /**
     * @return Invoice_DetailCollectionType
     */
    public function getInvoice_DetailCollection()
    {
      return $this->Invoice_DetailCollection;
    }

    /**
     * @param Invoice_DetailCollectionType $Invoice_DetailCollection
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationAreaType
     */
    public function setInvoice_DetailCollection($Invoice_DetailCollection)
    {
      $this->Invoice_DetailCollection = $Invoice_DetailCollection;
      return $this;
    }

}
