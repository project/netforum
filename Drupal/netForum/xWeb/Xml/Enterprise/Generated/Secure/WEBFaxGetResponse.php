<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBFaxGetResponse
{

    /**
     * @var CustomerFaxType $WEBFaxGetResult
     */
    protected $WEBFaxGetResult = null;

    /**
     * @param CustomerFaxType $WEBFaxGetResult
     */
    public function __construct($WEBFaxGetResult)
    {
      $this->WEBFaxGetResult = $WEBFaxGetResult;
    }

    /**
     * @return CustomerFaxType
     */
    public function getWEBFaxGetResult()
    {
      return $this->WEBFaxGetResult;
    }

    /**
     * @param CustomerFaxType $WEBFaxGetResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBFaxGetResponse
     */
    public function setWEBFaxGetResult($WEBFaxGetResult)
    {
      $this->WEBFaxGetResult = $WEBFaxGetResult;
      return $this;
    }

}
