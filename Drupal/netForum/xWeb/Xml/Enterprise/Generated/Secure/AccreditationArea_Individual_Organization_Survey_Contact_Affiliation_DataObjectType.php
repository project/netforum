<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType
{

    /**
     * @var av_key_Type $sxo__ixo_key
     */
    protected $sxo__ixo_key = null;

    /**
     * @var stringLength80_Type $sxo__ixo_rlt_code
     */
    protected $sxo__ixo_rlt_code = null;

    /**
     * @var stringLength150_Type $sxo__ixo_title
     */
    protected $sxo__ixo_title = null;

    /**
     * @var av_date_small_Type $sxo__ixo_start_date
     */
    protected $sxo__ixo_start_date = null;

    /**
     * @var av_date_small_Type $sxo__ixo_end_date
     */
    protected $sxo__ixo_end_date = null;

    /**
     * @var av_key_Type $sxo__ixo_ind_cst_key
     */
    protected $sxo__ixo_ind_cst_key = null;

    /**
     * @var av_key_Type $sxo__ixo_org_cst_key
     */
    protected $sxo__ixo_org_cst_key = null;

    /**
     * @var av_date_small_Type $sxo__ixo_add_date
     */
    protected $sxo__ixo_add_date = null;

    /**
     * @var av_user_Type $sxo__ixo_add_user
     */
    protected $sxo__ixo_add_user = null;

    /**
     * @var av_date_small_Type $sxo__ixo_change_date
     */
    protected $sxo__ixo_change_date = null;

    /**
     * @var av_user_Type $sxo__ixo_change_user
     */
    protected $sxo__ixo_change_user = null;

    /**
     * @var av_delete_flag_Type $sxo__ixo_delete_flag
     */
    protected $sxo__ixo_delete_flag = null;

    /**
     * @var av_key_Type $sxo__ixo_key_ext
     */
    protected $sxo__ixo_key_ext = null;

    /**
     * @var av_key_Type $sxo__ixo_cst_key_owner
     */
    protected $sxo__ixo_cst_key_owner = null;

    /**
     * @var av_key_Type $sxo__ixo_entity_key
     */
    protected $sxo__ixo_entity_key = null;

    /**
     * @var av_flag_Type $sxo__ixo_void_flag
     */
    protected $sxo__ixo_void_flag = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getSxo__ixo_key()
    {
      return $this->sxo__ixo_key;
    }

    /**
     * @param av_key_Type $sxo__ixo_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType
     */
    public function setSxo__ixo_key($sxo__ixo_key)
    {
      $this->sxo__ixo_key = $sxo__ixo_key;
      return $this;
    }

    /**
     * @return stringLength80_Type
     */
    public function getSxo__ixo_rlt_code()
    {
      return $this->sxo__ixo_rlt_code;
    }

    /**
     * @param stringLength80_Type $sxo__ixo_rlt_code
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType
     */
    public function setSxo__ixo_rlt_code($sxo__ixo_rlt_code)
    {
      $this->sxo__ixo_rlt_code = $sxo__ixo_rlt_code;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getSxo__ixo_title()
    {
      return $this->sxo__ixo_title;
    }

    /**
     * @param stringLength150_Type $sxo__ixo_title
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType
     */
    public function setSxo__ixo_title($sxo__ixo_title)
    {
      $this->sxo__ixo_title = $sxo__ixo_title;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getSxo__ixo_start_date()
    {
      return $this->sxo__ixo_start_date;
    }

    /**
     * @param av_date_small_Type $sxo__ixo_start_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType
     */
    public function setSxo__ixo_start_date($sxo__ixo_start_date)
    {
      $this->sxo__ixo_start_date = $sxo__ixo_start_date;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getSxo__ixo_end_date()
    {
      return $this->sxo__ixo_end_date;
    }

    /**
     * @param av_date_small_Type $sxo__ixo_end_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType
     */
    public function setSxo__ixo_end_date($sxo__ixo_end_date)
    {
      $this->sxo__ixo_end_date = $sxo__ixo_end_date;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSxo__ixo_ind_cst_key()
    {
      return $this->sxo__ixo_ind_cst_key;
    }

    /**
     * @param av_key_Type $sxo__ixo_ind_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType
     */
    public function setSxo__ixo_ind_cst_key($sxo__ixo_ind_cst_key)
    {
      $this->sxo__ixo_ind_cst_key = $sxo__ixo_ind_cst_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSxo__ixo_org_cst_key()
    {
      return $this->sxo__ixo_org_cst_key;
    }

    /**
     * @param av_key_Type $sxo__ixo_org_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType
     */
    public function setSxo__ixo_org_cst_key($sxo__ixo_org_cst_key)
    {
      $this->sxo__ixo_org_cst_key = $sxo__ixo_org_cst_key;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getSxo__ixo_add_date()
    {
      return $this->sxo__ixo_add_date;
    }

    /**
     * @param av_date_small_Type $sxo__ixo_add_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType
     */
    public function setSxo__ixo_add_date($sxo__ixo_add_date)
    {
      $this->sxo__ixo_add_date = $sxo__ixo_add_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getSxo__ixo_add_user()
    {
      return $this->sxo__ixo_add_user;
    }

    /**
     * @param av_user_Type $sxo__ixo_add_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType
     */
    public function setSxo__ixo_add_user($sxo__ixo_add_user)
    {
      $this->sxo__ixo_add_user = $sxo__ixo_add_user;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getSxo__ixo_change_date()
    {
      return $this->sxo__ixo_change_date;
    }

    /**
     * @param av_date_small_Type $sxo__ixo_change_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType
     */
    public function setSxo__ixo_change_date($sxo__ixo_change_date)
    {
      $this->sxo__ixo_change_date = $sxo__ixo_change_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getSxo__ixo_change_user()
    {
      return $this->sxo__ixo_change_user;
    }

    /**
     * @param av_user_Type $sxo__ixo_change_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType
     */
    public function setSxo__ixo_change_user($sxo__ixo_change_user)
    {
      $this->sxo__ixo_change_user = $sxo__ixo_change_user;
      return $this;
    }

    /**
     * @return av_delete_flag_Type
     */
    public function getSxo__ixo_delete_flag()
    {
      return $this->sxo__ixo_delete_flag;
    }

    /**
     * @param av_delete_flag_Type $sxo__ixo_delete_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType
     */
    public function setSxo__ixo_delete_flag($sxo__ixo_delete_flag)
    {
      $this->sxo__ixo_delete_flag = $sxo__ixo_delete_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSxo__ixo_key_ext()
    {
      return $this->sxo__ixo_key_ext;
    }

    /**
     * @param av_key_Type $sxo__ixo_key_ext
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType
     */
    public function setSxo__ixo_key_ext($sxo__ixo_key_ext)
    {
      $this->sxo__ixo_key_ext = $sxo__ixo_key_ext;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSxo__ixo_cst_key_owner()
    {
      return $this->sxo__ixo_cst_key_owner;
    }

    /**
     * @param av_key_Type $sxo__ixo_cst_key_owner
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType
     */
    public function setSxo__ixo_cst_key_owner($sxo__ixo_cst_key_owner)
    {
      $this->sxo__ixo_cst_key_owner = $sxo__ixo_cst_key_owner;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSxo__ixo_entity_key()
    {
      return $this->sxo__ixo_entity_key;
    }

    /**
     * @param av_key_Type $sxo__ixo_entity_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType
     */
    public function setSxo__ixo_entity_key($sxo__ixo_entity_key)
    {
      $this->sxo__ixo_entity_key = $sxo__ixo_entity_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getSxo__ixo_void_flag()
    {
      return $this->sxo__ixo_void_flag;
    }

    /**
     * @param av_flag_Type $sxo__ixo_void_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_Survey_Contact_Affiliation_DataObjectType
     */
    public function setSxo__ixo_void_flag($sxo__ixo_void_flag)
    {
      $this->sxo__ixo_void_flag = $sxo__ixo_void_flag;
      return $this;
    }

}
