<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResult $WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResult
     */
    protected $WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResult $WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResult
     */
    public function __construct($WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResult)
    {
      $this->WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResult = $WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResult
     */
    public function getWEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResult()
    {
      return $this->WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResult $WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResponse
     */
    public function setWEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResult($WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResult)
    {
      $this->WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResult = $WEBCentralizedShoppingCartGetEventGuestRegistrantTypeListByEventResult;
      return $this;
    }

}
