<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetActionTypeListResponse
{

    /**
     * @var GetActionTypeListResult $GetActionTypeListResult
     */
    protected $GetActionTypeListResult = null;

    /**
     * @param GetActionTypeListResult $GetActionTypeListResult
     */
    public function __construct($GetActionTypeListResult)
    {
      $this->GetActionTypeListResult = $GetActionTypeListResult;
    }

    /**
     * @return GetActionTypeListResult
     */
    public function getGetActionTypeListResult()
    {
      return $this->GetActionTypeListResult;
    }

    /**
     * @param GetActionTypeListResult $GetActionTypeListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetActionTypeListResponse
     */
    public function setGetActionTypeListResult($GetActionTypeListResult)
    {
      $this->GetActionTypeListResult = $GetActionTypeListResult;
      return $this;
    }

}
