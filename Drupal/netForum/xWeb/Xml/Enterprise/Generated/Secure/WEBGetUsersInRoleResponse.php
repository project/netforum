<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBGetUsersInRoleResponse
{

    /**
     * @var WEBGetUsersInRoleResult $WEBGetUsersInRoleResult
     */
    protected $WEBGetUsersInRoleResult = null;

    /**
     * @param WEBGetUsersInRoleResult $WEBGetUsersInRoleResult
     */
    public function __construct($WEBGetUsersInRoleResult)
    {
      $this->WEBGetUsersInRoleResult = $WEBGetUsersInRoleResult;
    }

    /**
     * @return WEBGetUsersInRoleResult
     */
    public function getWEBGetUsersInRoleResult()
    {
      return $this->WEBGetUsersInRoleResult;
    }

    /**
     * @param WEBGetUsersInRoleResult $WEBGetUsersInRoleResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBGetUsersInRoleResponse
     */
    public function setWEBGetUsersInRoleResult($WEBGetUsersInRoleResult)
    {
      $this->WEBGetUsersInRoleResult = $WEBGetUsersInRoleResult;
      return $this;
    }

}
