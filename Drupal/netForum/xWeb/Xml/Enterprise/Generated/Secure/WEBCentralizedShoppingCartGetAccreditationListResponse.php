<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetAccreditationListResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetAccreditationListResult $WEBCentralizedShoppingCartGetAccreditationListResult
     */
    protected $WEBCentralizedShoppingCartGetAccreditationListResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetAccreditationListResult $WEBCentralizedShoppingCartGetAccreditationListResult
     */
    public function __construct($WEBCentralizedShoppingCartGetAccreditationListResult)
    {
      $this->WEBCentralizedShoppingCartGetAccreditationListResult = $WEBCentralizedShoppingCartGetAccreditationListResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetAccreditationListResult
     */
    public function getWEBCentralizedShoppingCartGetAccreditationListResult()
    {
      return $this->WEBCentralizedShoppingCartGetAccreditationListResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetAccreditationListResult $WEBCentralizedShoppingCartGetAccreditationListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetAccreditationListResponse
     */
    public function setWEBCentralizedShoppingCartGetAccreditationListResult($WEBCentralizedShoppingCartGetAccreditationListResult)
    {
      $this->WEBCentralizedShoppingCartGetAccreditationListResult = $WEBCentralizedShoppingCartGetAccreditationListResult;
      return $this;
    }

}
