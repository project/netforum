<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class EventUserFundraisingDetail
{

    /**
     * @var string $reg_fundraising_goal
     */
    protected $reg_fundraising_goal = null;

    /**
     * @var string $reg_fundraising_achieved
     */
    protected $reg_fundraising_achieved = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getReg_fundraising_goal()
    {
      return $this->reg_fundraising_goal;
    }

    /**
     * @param string $reg_fundraising_goal
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\EventUserFundraisingDetail
     */
    public function setReg_fundraising_goal($reg_fundraising_goal)
    {
      $this->reg_fundraising_goal = $reg_fundraising_goal;
      return $this;
    }

    /**
     * @return string
     */
    public function getReg_fundraising_achieved()
    {
      return $this->reg_fundraising_achieved;
    }

    /**
     * @param string $reg_fundraising_achieved
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\EventUserFundraisingDetail
     */
    public function setReg_fundraising_achieved($reg_fundraising_achieved)
    {
      $this->reg_fundraising_achieved = $reg_fundraising_achieved;
      return $this;
    }

}
