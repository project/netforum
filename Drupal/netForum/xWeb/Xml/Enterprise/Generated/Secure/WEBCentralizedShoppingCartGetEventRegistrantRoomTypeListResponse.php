<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResult $WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResult
     */
    protected $WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResult $WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResult
     */
    public function __construct($WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResult)
    {
      $this->WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResult = $WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResult
     */
    public function getWEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResult()
    {
      return $this->WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResult $WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResponse
     */
    public function setWEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResult($WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResult)
    {
      $this->WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResult = $WEBCentralizedShoppingCartGetEventRegistrantRoomTypeListResult;
      return $this;
    }

}
