<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class Invoice_DetailCollectionType
{

    /**
     * @var InvoiceDetailType[] $InvoiceDetail
     */
    protected $InvoiceDetail = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return InvoiceDetailType[]
     */
    public function getInvoiceDetail()
    {
      return $this->InvoiceDetail;
    }

    /**
     * @param InvoiceDetailType[] $InvoiceDetail
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\Invoice_DetailCollectionType
     */
    public function setInvoiceDetail(array $InvoiceDetail = null)
    {
      $this->InvoiceDetail = $InvoiceDetail;
      return $this;
    }

}
