<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetMergeCustomerModulesResponse
{

    /**
     * @var GetMergeCustomerModulesResult $GetMergeCustomerModulesResult
     */
    protected $GetMergeCustomerModulesResult = null;

    /**
     * @param GetMergeCustomerModulesResult $GetMergeCustomerModulesResult
     */
    public function __construct($GetMergeCustomerModulesResult)
    {
      $this->GetMergeCustomerModulesResult = $GetMergeCustomerModulesResult;
    }

    /**
     * @return GetMergeCustomerModulesResult
     */
    public function getGetMergeCustomerModulesResult()
    {
      return $this->GetMergeCustomerModulesResult;
    }

    /**
     * @param GetMergeCustomerModulesResult $GetMergeCustomerModulesResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetMergeCustomerModulesResponse
     */
    public function setGetMergeCustomerModulesResult($GetMergeCustomerModulesResult)
    {
      $this->GetMergeCustomerModulesResult = $GetMergeCustomerModulesResult;
      return $this;
    }

}
