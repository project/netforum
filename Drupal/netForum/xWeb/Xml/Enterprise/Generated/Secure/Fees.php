<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class Fees
{

    /**
     * @var Fee[] $Fee
     */
    protected $Fee = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return Fee[]
     */
    public function getFee()
    {
      return $this->Fee;
    }

    /**
     * @param Fee[] $Fee
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\Fees
     */
    public function setFee(array $Fee = null)
    {
      $this->Fee = $Fee;
      return $this;
    }

}
