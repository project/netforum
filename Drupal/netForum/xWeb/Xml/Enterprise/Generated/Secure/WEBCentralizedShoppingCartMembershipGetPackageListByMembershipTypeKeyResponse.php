<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResponse
{

    /**
     * @var WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResult $WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResult
     */
    protected $WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResult = null;

    /**
     * @param WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResult $WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResult
     */
    public function __construct($WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResult)
    {
      $this->WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResult = $WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResult;
    }

    /**
     * @return WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResult
     */
    public function getWEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResult()
    {
      return $this->WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResult;
    }

    /**
     * @param WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResult $WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResponse
     */
    public function setWEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResult($WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResult)
    {
      $this->WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResult = $WEBCentralizedShoppingCartMembershipGetPackageListByMembershipTypeKeyResult;
      return $this;
    }

}
