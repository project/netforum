<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGiftGetGiftProductListResponse
{

    /**
     * @var WEBCentralizedShoppingCartGiftGetGiftProductListResult $WEBCentralizedShoppingCartGiftGetGiftProductListResult
     */
    protected $WEBCentralizedShoppingCartGiftGetGiftProductListResult = null;

    /**
     * @param WEBCentralizedShoppingCartGiftGetGiftProductListResult $WEBCentralizedShoppingCartGiftGetGiftProductListResult
     */
    public function __construct($WEBCentralizedShoppingCartGiftGetGiftProductListResult)
    {
      $this->WEBCentralizedShoppingCartGiftGetGiftProductListResult = $WEBCentralizedShoppingCartGiftGetGiftProductListResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGiftGetGiftProductListResult
     */
    public function getWEBCentralizedShoppingCartGiftGetGiftProductListResult()
    {
      return $this->WEBCentralizedShoppingCartGiftGetGiftProductListResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGiftGetGiftProductListResult $WEBCentralizedShoppingCartGiftGetGiftProductListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGiftGetGiftProductListResponse
     */
    public function setWEBCentralizedShoppingCartGiftGetGiftProductListResult($WEBCentralizedShoppingCartGiftGetGiftProductListResult)
    {
      $this->WEBCentralizedShoppingCartGiftGetGiftProductListResult = $WEBCentralizedShoppingCartGiftGetGiftProductListResult;
      return $this;
    }

}
