<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class InvoiceDetailCustomerType
{

    /**
     * @var av_key_Type $CurrentKey
     */
    protected $CurrentKey = null;

    /**
     * @var InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType $Invoice_Detail_Liability
     */
    protected $Invoice_Detail_Liability = null;

    /**
     * @var InvoiceDetailCustomer_Invoice_Detail_Liability_View_DataObjectType $Invoice_Detail_Liability_View
     */
    protected $Invoice_Detail_Liability_View = null;

    /**
     * @var InvoiceDetailCustomer_Invoice_Detail_DataObjectType $Invoice_Detail
     */
    protected $Invoice_Detail = null;

    /**
     * @var InvoiceDetailCustomer_Price_DataObjectType $Price
     */
    protected $Price = null;

    /**
     * @param InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType $Invoice_Detail_Liability
     * @param InvoiceDetailCustomer_Invoice_Detail_Liability_View_DataObjectType $Invoice_Detail_Liability_View
     * @param InvoiceDetailCustomer_Invoice_Detail_DataObjectType $Invoice_Detail
     * @param InvoiceDetailCustomer_Price_DataObjectType $Price
     */
    public function __construct($Invoice_Detail_Liability, $Invoice_Detail_Liability_View, $Invoice_Detail, $Price)
    {
      $this->Invoice_Detail_Liability = $Invoice_Detail_Liability;
      $this->Invoice_Detail_Liability_View = $Invoice_Detail_Liability_View;
      $this->Invoice_Detail = $Invoice_Detail;
      $this->Price = $Price;
    }

    /**
     * @return av_key_Type
     */
    public function getCurrentKey()
    {
      return $this->CurrentKey;
    }

    /**
     * @param av_key_Type $CurrentKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomerType
     */
    public function setCurrentKey($CurrentKey)
    {
      $this->CurrentKey = $CurrentKey;
      return $this;
    }

    /**
     * @return InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType
     */
    public function getInvoice_Detail_Liability()
    {
      return $this->Invoice_Detail_Liability;
    }

    /**
     * @param InvoiceDetailCustomer_Invoice_Detail_Liability_DataObjectType $Invoice_Detail_Liability
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomerType
     */
    public function setInvoice_Detail_Liability($Invoice_Detail_Liability)
    {
      $this->Invoice_Detail_Liability = $Invoice_Detail_Liability;
      return $this;
    }

    /**
     * @return InvoiceDetailCustomer_Invoice_Detail_Liability_View_DataObjectType
     */
    public function getInvoice_Detail_Liability_View()
    {
      return $this->Invoice_Detail_Liability_View;
    }

    /**
     * @param InvoiceDetailCustomer_Invoice_Detail_Liability_View_DataObjectType $Invoice_Detail_Liability_View
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomerType
     */
    public function setInvoice_Detail_Liability_View($Invoice_Detail_Liability_View)
    {
      $this->Invoice_Detail_Liability_View = $Invoice_Detail_Liability_View;
      return $this;
    }

    /**
     * @return InvoiceDetailCustomer_Invoice_Detail_DataObjectType
     */
    public function getInvoice_Detail()
    {
      return $this->Invoice_Detail;
    }

    /**
     * @param InvoiceDetailCustomer_Invoice_Detail_DataObjectType $Invoice_Detail
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomerType
     */
    public function setInvoice_Detail($Invoice_Detail)
    {
      $this->Invoice_Detail = $Invoice_Detail;
      return $this;
    }

    /**
     * @return InvoiceDetailCustomer_Price_DataObjectType
     */
    public function getPrice()
    {
      return $this->Price;
    }

    /**
     * @param InvoiceDetailCustomer_Price_DataObjectType $Price
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetailCustomerType
     */
    public function setPrice($Price)
    {
      $this->Price = $Price;
      return $this;
    }

}
