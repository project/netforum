<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResult $WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResult
     */
    protected $WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResult $WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResult
     */
    public function __construct($WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResult)
    {
      $this->WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResult = $WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResult
     */
    public function getWEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResult()
    {
      return $this->WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResult $WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResponse
     */
    public function setWEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResult($WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResult)
    {
      $this->WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResult = $WEBCentralizedShoppingCartGetEventRegistrantGroupIndividualListResult;
      return $this;
    }

}
