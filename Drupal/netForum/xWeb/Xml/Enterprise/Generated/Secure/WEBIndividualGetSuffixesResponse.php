<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBIndividualGetSuffixesResponse
{

    /**
     * @var WEBIndividualGetSuffixesResult $WEBIndividualGetSuffixesResult
     */
    protected $WEBIndividualGetSuffixesResult = null;

    /**
     * @param WEBIndividualGetSuffixesResult $WEBIndividualGetSuffixesResult
     */
    public function __construct($WEBIndividualGetSuffixesResult)
    {
      $this->WEBIndividualGetSuffixesResult = $WEBIndividualGetSuffixesResult;
    }

    /**
     * @return WEBIndividualGetSuffixesResult
     */
    public function getWEBIndividualGetSuffixesResult()
    {
      return $this->WEBIndividualGetSuffixesResult;
    }

    /**
     * @param WEBIndividualGetSuffixesResult $WEBIndividualGetSuffixesResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBIndividualGetSuffixesResponse
     */
    public function setWEBIndividualGetSuffixesResult($WEBIndividualGetSuffixesResult)
    {
      $this->WEBIndividualGetSuffixesResult = $WEBIndividualGetSuffixesResult;
      return $this;
    }

}
