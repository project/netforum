<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBGetAllRolesResponse
{

    /**
     * @var WEBGetAllRolesResult $WEBGetAllRolesResult
     */
    protected $WEBGetAllRolesResult = null;

    /**
     * @param WEBGetAllRolesResult $WEBGetAllRolesResult
     */
    public function __construct($WEBGetAllRolesResult)
    {
      $this->WEBGetAllRolesResult = $WEBGetAllRolesResult;
    }

    /**
     * @return WEBGetAllRolesResult
     */
    public function getWEBGetAllRolesResult()
    {
      return $this->WEBGetAllRolesResult;
    }

    /**
     * @param WEBGetAllRolesResult $WEBGetAllRolesResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBGetAllRolesResponse
     */
    public function setWEBGetAllRolesResult($WEBGetAllRolesResult)
    {
      $this->WEBGetAllRolesResult = $WEBGetAllRolesResult;
      return $this;
    }

}
