<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetTrackListByEventKeyResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetTrackListByEventKeyResult $WEBCentralizedShoppingCartGetTrackListByEventKeyResult
     */
    protected $WEBCentralizedShoppingCartGetTrackListByEventKeyResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetTrackListByEventKeyResult $WEBCentralizedShoppingCartGetTrackListByEventKeyResult
     */
    public function __construct($WEBCentralizedShoppingCartGetTrackListByEventKeyResult)
    {
      $this->WEBCentralizedShoppingCartGetTrackListByEventKeyResult = $WEBCentralizedShoppingCartGetTrackListByEventKeyResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetTrackListByEventKeyResult
     */
    public function getWEBCentralizedShoppingCartGetTrackListByEventKeyResult()
    {
      return $this->WEBCentralizedShoppingCartGetTrackListByEventKeyResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetTrackListByEventKeyResult $WEBCentralizedShoppingCartGetTrackListByEventKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetTrackListByEventKeyResponse
     */
    public function setWEBCentralizedShoppingCartGetTrackListByEventKeyResult($WEBCentralizedShoppingCartGetTrackListByEventKeyResult)
    {
      $this->WEBCentralizedShoppingCartGetTrackListByEventKeyResult = $WEBCentralizedShoppingCartGetTrackListByEventKeyResult;
      return $this;
    }

}
