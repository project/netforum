<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class InvoiceDetail_Invoice_Detail_Amount_Due_DataObjectType
{

    /**
     * @var av_key_Type $idd_ivd_key
     */
    protected $idd_ivd_key = null;

    /**
     * @var av_currency_Type $idd_ivd_amount_cp
     */
    protected $idd_ivd_amount_cp = null;

    /**
     * @var av_currency_Type $idd_ivd_amount_due
     */
    protected $idd_ivd_amount_due = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getIdd_ivd_key()
    {
      return $this->idd_ivd_key;
    }

    /**
     * @param av_key_Type $idd_ivd_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Amount_Due_DataObjectType
     */
    public function setIdd_ivd_key($idd_ivd_key)
    {
      $this->idd_ivd_key = $idd_ivd_key;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIdd_ivd_amount_cp()
    {
      return $this->idd_ivd_amount_cp;
    }

    /**
     * @param av_currency_Type $idd_ivd_amount_cp
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Amount_Due_DataObjectType
     */
    public function setIdd_ivd_amount_cp($idd_ivd_amount_cp)
    {
      $this->idd_ivd_amount_cp = $idd_ivd_amount_cp;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getIdd_ivd_amount_due()
    {
      return $this->idd_ivd_amount_due;
    }

    /**
     * @param av_currency_Type $idd_ivd_amount_due
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InvoiceDetail_Invoice_Detail_Amount_Due_DataObjectType
     */
    public function setIdd_ivd_amount_due($idd_ivd_amount_due)
    {
      $this->idd_ivd_amount_due = $idd_ivd_amount_due;
      return $this;
    }

}
