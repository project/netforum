<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGiftGetGiftProductListKeysResponse
{

    /**
     * @var WEBCentralizedShoppingCartGiftGetGiftProductListKeysResult $WEBCentralizedShoppingCartGiftGetGiftProductListKeysResult
     */
    protected $WEBCentralizedShoppingCartGiftGetGiftProductListKeysResult = null;

    /**
     * @param WEBCentralizedShoppingCartGiftGetGiftProductListKeysResult $WEBCentralizedShoppingCartGiftGetGiftProductListKeysResult
     */
    public function __construct($WEBCentralizedShoppingCartGiftGetGiftProductListKeysResult)
    {
      $this->WEBCentralizedShoppingCartGiftGetGiftProductListKeysResult = $WEBCentralizedShoppingCartGiftGetGiftProductListKeysResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGiftGetGiftProductListKeysResult
     */
    public function getWEBCentralizedShoppingCartGiftGetGiftProductListKeysResult()
    {
      return $this->WEBCentralizedShoppingCartGiftGetGiftProductListKeysResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGiftGetGiftProductListKeysResult $WEBCentralizedShoppingCartGiftGetGiftProductListKeysResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGiftGetGiftProductListKeysResponse
     */
    public function setWEBCentralizedShoppingCartGiftGetGiftProductListKeysResult($WEBCentralizedShoppingCartGiftGetGiftProductListKeysResult)
    {
      $this->WEBCentralizedShoppingCartGiftGetGiftProductListKeysResult = $WEBCentralizedShoppingCartGiftGetGiftProductListKeysResult;
      return $this;
    }

}
