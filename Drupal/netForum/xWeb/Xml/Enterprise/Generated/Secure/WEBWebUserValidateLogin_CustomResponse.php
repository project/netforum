<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserValidateLogin_CustomResponse
{

    /**
     * @var string $WEBWebUserValidateLogin_CustomResult
     */
    protected $WEBWebUserValidateLogin_CustomResult = null;

    /**
     * @param string $WEBWebUserValidateLogin_CustomResult
     */
    public function __construct($WEBWebUserValidateLogin_CustomResult)
    {
      $this->WEBWebUserValidateLogin_CustomResult = $WEBWebUserValidateLogin_CustomResult;
    }

    /**
     * @return string
     */
    public function getWEBWebUserValidateLogin_CustomResult()
    {
      return $this->WEBWebUserValidateLogin_CustomResult;
    }

    /**
     * @param string $WEBWebUserValidateLogin_CustomResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserValidateLogin_CustomResponse
     */
    public function setWEBWebUserValidateLogin_CustomResult($WEBWebUserValidateLogin_CustomResult)
    {
      $this->WEBWebUserValidateLogin_CustomResult = $WEBWebUserValidateLogin_CustomResult;
      return $this;
    }

}
