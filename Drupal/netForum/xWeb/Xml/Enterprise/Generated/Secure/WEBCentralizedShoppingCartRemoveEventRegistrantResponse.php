<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartRemoveEventRegistrantResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartRemoveEventRegistrantResult
     */
    protected $WEBCentralizedShoppingCartRemoveEventRegistrantResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartRemoveEventRegistrantResult
     */
    public function __construct($WEBCentralizedShoppingCartRemoveEventRegistrantResult)
    {
      $this->WEBCentralizedShoppingCartRemoveEventRegistrantResult = $WEBCentralizedShoppingCartRemoveEventRegistrantResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartRemoveEventRegistrantResult()
    {
      return $this->WEBCentralizedShoppingCartRemoveEventRegistrantResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartRemoveEventRegistrantResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartRemoveEventRegistrantResponse
     */
    public function setWEBCentralizedShoppingCartRemoveEventRegistrantResult($WEBCentralizedShoppingCartRemoveEventRegistrantResult)
    {
      $this->WEBCentralizedShoppingCartRemoveEventRegistrantResult = $WEBCentralizedShoppingCartRemoveEventRegistrantResult;
      return $this;
    }

}
