<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCommitteeGetPositionListResponse
{

    /**
     * @var WEBCommitteeGetPositionListResult $WEBCommitteeGetPositionListResult
     */
    protected $WEBCommitteeGetPositionListResult = null;

    /**
     * @param WEBCommitteeGetPositionListResult $WEBCommitteeGetPositionListResult
     */
    public function __construct($WEBCommitteeGetPositionListResult)
    {
      $this->WEBCommitteeGetPositionListResult = $WEBCommitteeGetPositionListResult;
    }

    /**
     * @return WEBCommitteeGetPositionListResult
     */
    public function getWEBCommitteeGetPositionListResult()
    {
      return $this->WEBCommitteeGetPositionListResult;
    }

    /**
     * @param WEBCommitteeGetPositionListResult $WEBCommitteeGetPositionListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCommitteeGetPositionListResponse
     */
    public function setWEBCommitteeGetPositionListResult($WEBCommitteeGetPositionListResult)
    {
      $this->WEBCommitteeGetPositionListResult = $WEBCommitteeGetPositionListResult;
      return $this;
    }

}
