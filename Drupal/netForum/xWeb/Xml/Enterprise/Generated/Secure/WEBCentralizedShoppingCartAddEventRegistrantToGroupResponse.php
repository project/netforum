<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartAddEventRegistrantToGroupResponse
{

    /**
     * @var EventsRegistrantGroupType $WEBCentralizedShoppingCartAddEventRegistrantToGroupResult
     */
    protected $WEBCentralizedShoppingCartAddEventRegistrantToGroupResult = null;

    /**
     * @param EventsRegistrantGroupType $WEBCentralizedShoppingCartAddEventRegistrantToGroupResult
     */
    public function __construct($WEBCentralizedShoppingCartAddEventRegistrantToGroupResult)
    {
      $this->WEBCentralizedShoppingCartAddEventRegistrantToGroupResult = $WEBCentralizedShoppingCartAddEventRegistrantToGroupResult;
    }

    /**
     * @return EventsRegistrantGroupType
     */
    public function getWEBCentralizedShoppingCartAddEventRegistrantToGroupResult()
    {
      return $this->WEBCentralizedShoppingCartAddEventRegistrantToGroupResult;
    }

    /**
     * @param EventsRegistrantGroupType $WEBCentralizedShoppingCartAddEventRegistrantToGroupResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartAddEventRegistrantToGroupResponse
     */
    public function setWEBCentralizedShoppingCartAddEventRegistrantToGroupResult($WEBCentralizedShoppingCartAddEventRegistrantToGroupResult)
    {
      $this->WEBCentralizedShoppingCartAddEventRegistrantToGroupResult = $WEBCentralizedShoppingCartAddEventRegistrantToGroupResult;
      return $this;
    }

}
