<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetDynamicQuery
{

    /**
     * @var string $szObjectName
     */
    protected $szObjectName = null;

    /**
     * @var string $szQueryName
     */
    protected $szQueryName = null;

    /**
     * @var string $WithDescriptions
     */
    protected $WithDescriptions = null;

    /**
     * @var ArrayOfParameter $Parameters
     */
    protected $Parameters = null;

    /**
     * @var int $currentPage
     */
    protected $currentPage = null;

    /**
     * @param string $szObjectName
     * @param string $szQueryName
     * @param string $WithDescriptions
     * @param ArrayOfParameter $Parameters
     * @param int $currentPage
     */
    public function __construct($szObjectName, $szQueryName, $WithDescriptions, $Parameters, $currentPage)
    {
      $this->szObjectName = $szObjectName;
      $this->szQueryName = $szQueryName;
      $this->WithDescriptions = $WithDescriptions;
      $this->Parameters = $Parameters;
      $this->currentPage = $currentPage;
    }

    /**
     * @return string
     */
    public function getSzObjectName()
    {
      return $this->szObjectName;
    }

    /**
     * @param string $szObjectName
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetDynamicQuery
     */
    public function setSzObjectName($szObjectName)
    {
      $this->szObjectName = $szObjectName;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzQueryName()
    {
      return $this->szQueryName;
    }

    /**
     * @param string $szQueryName
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetDynamicQuery
     */
    public function setSzQueryName($szQueryName)
    {
      $this->szQueryName = $szQueryName;
      return $this;
    }

    /**
     * @return string
     */
    public function getWithDescriptions()
    {
      return $this->WithDescriptions;
    }

    /**
     * @param string $WithDescriptions
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetDynamicQuery
     */
    public function setWithDescriptions($WithDescriptions)
    {
      $this->WithDescriptions = $WithDescriptions;
      return $this;
    }

    /**
     * @return ArrayOfParameter
     */
    public function getParameters()
    {
      return $this->Parameters;
    }

    /**
     * @param ArrayOfParameter $Parameters
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetDynamicQuery
     */
    public function setParameters($Parameters)
    {
      $this->Parameters = $Parameters;
      return $this;
    }

    /**
     * @return int
     */
    public function getCurrentPage()
    {
      return $this->currentPage;
    }

    /**
     * @param int $currentPage
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetDynamicQuery
     */
    public function setCurrentPage($currentPage)
    {
      $this->currentPage = $currentPage;
      return $this;
    }

}
