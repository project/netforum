<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGiftGetGiftTypeListResponse
{

    /**
     * @var WEBCentralizedShoppingCartGiftGetGiftTypeListResult $WEBCentralizedShoppingCartGiftGetGiftTypeListResult
     */
    protected $WEBCentralizedShoppingCartGiftGetGiftTypeListResult = null;

    /**
     * @param WEBCentralizedShoppingCartGiftGetGiftTypeListResult $WEBCentralizedShoppingCartGiftGetGiftTypeListResult
     */
    public function __construct($WEBCentralizedShoppingCartGiftGetGiftTypeListResult)
    {
      $this->WEBCentralizedShoppingCartGiftGetGiftTypeListResult = $WEBCentralizedShoppingCartGiftGetGiftTypeListResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGiftGetGiftTypeListResult
     */
    public function getWEBCentralizedShoppingCartGiftGetGiftTypeListResult()
    {
      return $this->WEBCentralizedShoppingCartGiftGetGiftTypeListResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGiftGetGiftTypeListResult $WEBCentralizedShoppingCartGiftGetGiftTypeListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGiftGetGiftTypeListResponse
     */
    public function setWEBCentralizedShoppingCartGiftGetGiftTypeListResult($WEBCentralizedShoppingCartGiftGetGiftTypeListResult)
    {
      $this->WEBCentralizedShoppingCartGiftGetGiftTypeListResult = $WEBCentralizedShoppingCartGiftGetGiftTypeListResult;
      return $this;
    }

}
