<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBFindUsersInRoleResponse
{

    /**
     * @var WEBFindUsersInRoleResult $WEBFindUsersInRoleResult
     */
    protected $WEBFindUsersInRoleResult = null;

    /**
     * @param WEBFindUsersInRoleResult $WEBFindUsersInRoleResult
     */
    public function __construct($WEBFindUsersInRoleResult)
    {
      $this->WEBFindUsersInRoleResult = $WEBFindUsersInRoleResult;
    }

    /**
     * @return WEBFindUsersInRoleResult
     */
    public function getWEBFindUsersInRoleResult()
    {
      return $this->WEBFindUsersInRoleResult;
    }

    /**
     * @param WEBFindUsersInRoleResult $WEBFindUsersInRoleResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBFindUsersInRoleResponse
     */
    public function setWEBFindUsersInRoleResult($WEBFindUsersInRoleResult)
    {
      $this->WEBFindUsersInRoleResult = $WEBFindUsersInRoleResult;
      return $this;
    }

}
