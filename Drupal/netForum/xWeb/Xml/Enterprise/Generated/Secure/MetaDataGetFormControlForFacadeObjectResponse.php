<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class MetaDataGetFormControlForFacadeObjectResponse
{

    /**
     * @var ArrayOfAVFormControl $MetaDataGetFormControlForFacadeObjectResult
     */
    protected $MetaDataGetFormControlForFacadeObjectResult = null;

    /**
     * @param ArrayOfAVFormControl $MetaDataGetFormControlForFacadeObjectResult
     */
    public function __construct($MetaDataGetFormControlForFacadeObjectResult)
    {
      $this->MetaDataGetFormControlForFacadeObjectResult = $MetaDataGetFormControlForFacadeObjectResult;
    }

    /**
     * @return ArrayOfAVFormControl
     */
    public function getMetaDataGetFormControlForFacadeObjectResult()
    {
      return $this->MetaDataGetFormControlForFacadeObjectResult;
    }

    /**
     * @param ArrayOfAVFormControl $MetaDataGetFormControlForFacadeObjectResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\MetaDataGetFormControlForFacadeObjectResponse
     */
    public function setMetaDataGetFormControlForFacadeObjectResult($MetaDataGetFormControlForFacadeObjectResult)
    {
      $this->MetaDataGetFormControlForFacadeObjectResult = $MetaDataGetFormControlForFacadeObjectResult;
      return $this;
    }

}
