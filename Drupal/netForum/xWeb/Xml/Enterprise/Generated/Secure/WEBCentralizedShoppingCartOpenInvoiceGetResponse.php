<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartOpenInvoiceGetResponse
{

    /**
     * @var InvoiceType $WEBCentralizedShoppingCartOpenInvoiceGetResult
     */
    protected $WEBCentralizedShoppingCartOpenInvoiceGetResult = null;

    /**
     * @param InvoiceType $WEBCentralizedShoppingCartOpenInvoiceGetResult
     */
    public function __construct($WEBCentralizedShoppingCartOpenInvoiceGetResult)
    {
      $this->WEBCentralizedShoppingCartOpenInvoiceGetResult = $WEBCentralizedShoppingCartOpenInvoiceGetResult;
    }

    /**
     * @return InvoiceType
     */
    public function getWEBCentralizedShoppingCartOpenInvoiceGetResult()
    {
      return $this->WEBCentralizedShoppingCartOpenInvoiceGetResult;
    }

    /**
     * @param InvoiceType $WEBCentralizedShoppingCartOpenInvoiceGetResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartOpenInvoiceGetResponse
     */
    public function setWEBCentralizedShoppingCartOpenInvoiceGetResult($WEBCentralizedShoppingCartOpenInvoiceGetResult)
    {
      $this->WEBCentralizedShoppingCartOpenInvoiceGetResult = $WEBCentralizedShoppingCartOpenInvoiceGetResult;
      return $this;
    }

}
