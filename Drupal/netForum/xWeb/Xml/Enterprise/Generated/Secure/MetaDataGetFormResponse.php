<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class MetaDataGetFormResponse
{

    /**
     * @var AVForm $MetaDataGetFormResult
     */
    protected $MetaDataGetFormResult = null;

    /**
     * @param AVForm $MetaDataGetFormResult
     */
    public function __construct($MetaDataGetFormResult)
    {
      $this->MetaDataGetFormResult = $MetaDataGetFormResult;
    }

    /**
     * @return AVForm
     */
    public function getMetaDataGetFormResult()
    {
      return $this->MetaDataGetFormResult;
    }

    /**
     * @param AVForm $MetaDataGetFormResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\MetaDataGetFormResponse
     */
    public function setMetaDataGetFormResult($MetaDataGetFormResult)
    {
      $this->MetaDataGetFormResult = $MetaDataGetFormResult;
      return $this;
    }

}
