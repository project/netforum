<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCommitteeGetMembersResponse
{

    /**
     * @var WEBCommitteeGetMembersResult $WEBCommitteeGetMembersResult
     */
    protected $WEBCommitteeGetMembersResult = null;

    /**
     * @param WEBCommitteeGetMembersResult $WEBCommitteeGetMembersResult
     */
    public function __construct($WEBCommitteeGetMembersResult)
    {
      $this->WEBCommitteeGetMembersResult = $WEBCommitteeGetMembersResult;
    }

    /**
     * @return WEBCommitteeGetMembersResult
     */
    public function getWEBCommitteeGetMembersResult()
    {
      return $this->WEBCommitteeGetMembersResult;
    }

    /**
     * @param WEBCommitteeGetMembersResult $WEBCommitteeGetMembersResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCommitteeGetMembersResponse
     */
    public function setWEBCommitteeGetMembersResult($WEBCommitteeGetMembersResult)
    {
      $this->WEBCommitteeGetMembersResult = $WEBCommitteeGetMembersResult;
      return $this;
    }

}
