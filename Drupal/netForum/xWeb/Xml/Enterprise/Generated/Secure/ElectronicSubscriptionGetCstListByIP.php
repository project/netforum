<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class ElectronicSubscriptionGetCstListByIP
{

    /**
     * @var string $oIPAddress
     */
    protected $oIPAddress = null;

    /**
     * @param string $oIPAddress
     */
    public function __construct($oIPAddress)
    {
      $this->oIPAddress = $oIPAddress;
    }

    /**
     * @return string
     */
    public function getOIPAddress()
    {
      return $this->oIPAddress;
    }

    /**
     * @param string $oIPAddress
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ElectronicSubscriptionGetCstListByIP
     */
    public function setOIPAddress($oIPAddress)
    {
      $this->oIPAddress = $oIPAddress;
      return $this;
    }

}
