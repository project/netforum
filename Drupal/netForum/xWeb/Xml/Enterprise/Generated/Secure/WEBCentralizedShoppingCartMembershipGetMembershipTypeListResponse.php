<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartMembershipGetMembershipTypeListResponse
{

    /**
     * @var WEBCentralizedShoppingCartMembershipGetMembershipTypeListResult $WEBCentralizedShoppingCartMembershipGetMembershipTypeListResult
     */
    protected $WEBCentralizedShoppingCartMembershipGetMembershipTypeListResult = null;

    /**
     * @param WEBCentralizedShoppingCartMembershipGetMembershipTypeListResult $WEBCentralizedShoppingCartMembershipGetMembershipTypeListResult
     */
    public function __construct($WEBCentralizedShoppingCartMembershipGetMembershipTypeListResult)
    {
      $this->WEBCentralizedShoppingCartMembershipGetMembershipTypeListResult = $WEBCentralizedShoppingCartMembershipGetMembershipTypeListResult;
    }

    /**
     * @return WEBCentralizedShoppingCartMembershipGetMembershipTypeListResult
     */
    public function getWEBCentralizedShoppingCartMembershipGetMembershipTypeListResult()
    {
      return $this->WEBCentralizedShoppingCartMembershipGetMembershipTypeListResult;
    }

    /**
     * @param WEBCentralizedShoppingCartMembershipGetMembershipTypeListResult $WEBCentralizedShoppingCartMembershipGetMembershipTypeListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartMembershipGetMembershipTypeListResponse
     */
    public function setWEBCentralizedShoppingCartMembershipGetMembershipTypeListResult($WEBCentralizedShoppingCartMembershipGetMembershipTypeListResult)
    {
      $this->WEBCentralizedShoppingCartMembershipGetMembershipTypeListResult = $WEBCentralizedShoppingCartMembershipGetMembershipTypeListResult;
      return $this;
    }

}
