<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBActivityGetRegistrantTracksResponse
{

    /**
     * @var WEBActivityGetRegistrantTracksResult $WEBActivityGetRegistrantTracksResult
     */
    protected $WEBActivityGetRegistrantTracksResult = null;

    /**
     * @param WEBActivityGetRegistrantTracksResult $WEBActivityGetRegistrantTracksResult
     */
    public function __construct($WEBActivityGetRegistrantTracksResult)
    {
      $this->WEBActivityGetRegistrantTracksResult = $WEBActivityGetRegistrantTracksResult;
    }

    /**
     * @return WEBActivityGetRegistrantTracksResult
     */
    public function getWEBActivityGetRegistrantTracksResult()
    {
      return $this->WEBActivityGetRegistrantTracksResult;
    }

    /**
     * @param WEBActivityGetRegistrantTracksResult $WEBActivityGetRegistrantTracksResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBActivityGetRegistrantTracksResponse
     */
    public function setWEBActivityGetRegistrantTracksResult($WEBActivityGetRegistrantTracksResult)
    {
      $this->WEBActivityGetRegistrantTracksResult = $WEBActivityGetRegistrantTracksResult;
      return $this;
    }

}
