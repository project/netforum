<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartExhibitorSetLineItemsResponse
{

    /**
     * @var ExhibitorNewType $WEBCentralizedShoppingCartExhibitorSetLineItemsResult
     */
    protected $WEBCentralizedShoppingCartExhibitorSetLineItemsResult = null;

    /**
     * @param ExhibitorNewType $WEBCentralizedShoppingCartExhibitorSetLineItemsResult
     */
    public function __construct($WEBCentralizedShoppingCartExhibitorSetLineItemsResult)
    {
      $this->WEBCentralizedShoppingCartExhibitorSetLineItemsResult = $WEBCentralizedShoppingCartExhibitorSetLineItemsResult;
    }

    /**
     * @return ExhibitorNewType
     */
    public function getWEBCentralizedShoppingCartExhibitorSetLineItemsResult()
    {
      return $this->WEBCentralizedShoppingCartExhibitorSetLineItemsResult;
    }

    /**
     * @param ExhibitorNewType $WEBCentralizedShoppingCartExhibitorSetLineItemsResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartExhibitorSetLineItemsResponse
     */
    public function setWEBCentralizedShoppingCartExhibitorSetLineItemsResult($WEBCentralizedShoppingCartExhibitorSetLineItemsResult)
    {
      $this->WEBCentralizedShoppingCartExhibitorSetLineItemsResult = $WEBCentralizedShoppingCartExhibitorSetLineItemsResult;
      return $this;
    }

}
