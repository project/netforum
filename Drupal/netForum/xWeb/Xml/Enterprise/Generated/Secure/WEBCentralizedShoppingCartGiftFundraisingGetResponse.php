<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGiftFundraisingGetResponse
{

    /**
     * @var FundraisingGiftType $WEBCentralizedShoppingCartGiftFundraisingGetResult
     */
    protected $WEBCentralizedShoppingCartGiftFundraisingGetResult = null;

    /**
     * @param FundraisingGiftType $WEBCentralizedShoppingCartGiftFundraisingGetResult
     */
    public function __construct($WEBCentralizedShoppingCartGiftFundraisingGetResult)
    {
      $this->WEBCentralizedShoppingCartGiftFundraisingGetResult = $WEBCentralizedShoppingCartGiftFundraisingGetResult;
    }

    /**
     * @return FundraisingGiftType
     */
    public function getWEBCentralizedShoppingCartGiftFundraisingGetResult()
    {
      return $this->WEBCentralizedShoppingCartGiftFundraisingGetResult;
    }

    /**
     * @param FundraisingGiftType $WEBCentralizedShoppingCartGiftFundraisingGetResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGiftFundraisingGetResponse
     */
    public function setWEBCentralizedShoppingCartGiftFundraisingGetResult($WEBCentralizedShoppingCartGiftFundraisingGetResult)
    {
      $this->WEBCentralizedShoppingCartGiftFundraisingGetResult = $WEBCentralizedShoppingCartGiftFundraisingGetResult;
      return $this;
    }

}
