<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetQueryResponse
{

    /**
     * @var WildcardComplexType $GetQueryResult
     */
    protected $GetQueryResult = null;

    /**
     * @param WildcardComplexType $GetQueryResult
     */
    public function __construct($GetQueryResult)
    {
      $this->GetQueryResult = $GetQueryResult;
    }

    /**
     * @return WildcardComplexType
     */
    public function getGetQueryResult()
    {
      return $this->GetQueryResult;
    }

    /**
     * @param WildcardComplexType $GetQueryResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetQueryResponse
     */
    public function setGetQueryResult($GetQueryResult)
    {
      $this->GetQueryResult = $GetQueryResult;
      return $this;
    }

}
