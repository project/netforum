<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetPromotionalMailingTypesResponse
{

    /**
     * @var ArrayOfMailingList $GetPromotionalMailingTypesResult
     */
    protected $GetPromotionalMailingTypesResult = null;

    /**
     * @param ArrayOfMailingList $GetPromotionalMailingTypesResult
     */
    public function __construct($GetPromotionalMailingTypesResult)
    {
      $this->GetPromotionalMailingTypesResult = $GetPromotionalMailingTypesResult;
    }

    /**
     * @return ArrayOfMailingList
     */
    public function getGetPromotionalMailingTypesResult()
    {
      return $this->GetPromotionalMailingTypesResult;
    }

    /**
     * @param ArrayOfMailingList $GetPromotionalMailingTypesResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetPromotionalMailingTypesResponse
     */
    public function setGetPromotionalMailingTypesResult($GetPromotionalMailingTypesResult)
    {
      $this->GetPromotionalMailingTypesResult = $GetPromotionalMailingTypesResult;
      return $this;
    }

}
