<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetFacultyListBySessionKey
{

    /**
     * @var guid $SessionKey
     */
    protected $SessionKey = null;

    /**
     * @param guid $SessionKey
     */
    public function __construct($SessionKey)
    {
      $this->SessionKey = $SessionKey;
    }

    /**
     * @return guid
     */
    public function getSessionKey()
    {
      return $this->SessionKey;
    }

    /**
     * @param guid $SessionKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetFacultyListBySessionKey
     */
    public function setSessionKey($SessionKey)
    {
      $this->SessionKey = $SessionKey;
      return $this;
    }

}
