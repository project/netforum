<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserFindOrganizationsByDomain
{

    /**
     * @var string $domainToMatch
     */
    protected $domainToMatch = null;

    /**
     * @param string $domainToMatch
     */
    public function __construct($domainToMatch)
    {
      $this->domainToMatch = $domainToMatch;
    }

    /**
     * @return string
     */
    public function getDomainToMatch()
    {
      return $this->domainToMatch;
    }

    /**
     * @param string $domainToMatch
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserFindOrganizationsByDomain
     */
    public function setDomainToMatch($domainToMatch)
    {
      $this->domainToMatch = $domainToMatch;
      return $this;
    }

}
