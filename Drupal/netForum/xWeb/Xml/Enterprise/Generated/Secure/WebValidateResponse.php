<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WebValidateResponse
{

    /**
     * @var string $WebValidateResult
     */
    protected $WebValidateResult = null;

    /**
     * @param string $WebValidateResult
     */
    public function __construct($WebValidateResult)
    {
      $this->WebValidateResult = $WebValidateResult;
    }

    /**
     * @return string
     */
    public function getWebValidateResult()
    {
      return $this->WebValidateResult;
    }

    /**
     * @param string $WebValidateResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WebValidateResponse
     */
    public function setWebValidateResult($WebValidateResult)
    {
      $this->WebValidateResult = $WebValidateResult;
      return $this;
    }

}
