<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResult $WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResult
     */
    protected $WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResult $WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResult
     */
    public function __construct($WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResult)
    {
      $this->WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResult = $WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResult
     */
    public function getWEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResult()
    {
      return $this->WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResult $WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResponse
     */
    public function setWEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResult($WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResult)
    {
      $this->WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResult = $WEBCentralizedShoppingCartGetInstallmentFrequencyOptionsResult;
      return $this;
    }

}
