<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartInsertResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartInsertResult
     */
    protected $WEBCentralizedShoppingCartInsertResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartInsertResult
     */
    public function __construct($WEBCentralizedShoppingCartInsertResult)
    {
      $this->WEBCentralizedShoppingCartInsertResult = $WEBCentralizedShoppingCartInsertResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartInsertResult()
    {
      return $this->WEBCentralizedShoppingCartInsertResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartInsertResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartInsertResponse
     */
    public function setWEBCentralizedShoppingCartInsertResult($WEBCentralizedShoppingCartInsertResult)
    {
      $this->WEBCentralizedShoppingCartInsertResult = $WEBCentralizedShoppingCartInsertResult;
      return $this;
    }

}
