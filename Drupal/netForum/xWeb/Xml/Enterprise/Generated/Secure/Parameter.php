<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class Parameter
{

    /**
     * @var string $Name
     */
    protected $Name = null;

    /**
     * @var string $Value
     */
    protected $Value = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->Name;
    }

    /**
     * @param string $Name
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\Parameter
     */
    public function setName($Name)
    {
      $this->Name = $Name;
      return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
      return $this->Value;
    }

    /**
     * @param string $Value
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\Parameter
     */
    public function setValue($Value)
    {
      $this->Value = $Value;
      return $this;
    }

}
