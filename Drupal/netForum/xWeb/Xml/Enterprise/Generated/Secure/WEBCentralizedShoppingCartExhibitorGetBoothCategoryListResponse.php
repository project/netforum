<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResponse
{

    /**
     * @var WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResult $WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResult
     */
    protected $WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResult = null;

    /**
     * @param WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResult $WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResult
     */
    public function __construct($WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResult)
    {
      $this->WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResult = $WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResult;
    }

    /**
     * @return WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResult
     */
    public function getWEBCentralizedShoppingCartExhibitorGetBoothCategoryListResult()
    {
      return $this->WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResult;
    }

    /**
     * @param WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResult $WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResponse
     */
    public function setWEBCentralizedShoppingCartExhibitorGetBoothCategoryListResult($WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResult)
    {
      $this->WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResult = $WEBCentralizedShoppingCartExhibitorGetBoothCategoryListResult;
      return $this;
    }

}
