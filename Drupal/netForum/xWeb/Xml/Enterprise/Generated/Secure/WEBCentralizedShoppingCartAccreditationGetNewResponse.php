<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartAccreditationGetNewResponse
{

    /**
     * @var AccreditationAreaType $WEBCentralizedShoppingCartAccreditationGetNewResult
     */
    protected $WEBCentralizedShoppingCartAccreditationGetNewResult = null;

    /**
     * @param AccreditationAreaType $WEBCentralizedShoppingCartAccreditationGetNewResult
     */
    public function __construct($WEBCentralizedShoppingCartAccreditationGetNewResult)
    {
      $this->WEBCentralizedShoppingCartAccreditationGetNewResult = $WEBCentralizedShoppingCartAccreditationGetNewResult;
    }

    /**
     * @return AccreditationAreaType
     */
    public function getWEBCentralizedShoppingCartAccreditationGetNewResult()
    {
      return $this->WEBCentralizedShoppingCartAccreditationGetNewResult;
    }

    /**
     * @param AccreditationAreaType $WEBCentralizedShoppingCartAccreditationGetNewResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartAccreditationGetNewResponse
     */
    public function setWEBCentralizedShoppingCartAccreditationGetNewResult($WEBCentralizedShoppingCartAccreditationGetNewResult)
    {
      $this->WEBCentralizedShoppingCartAccreditationGetNewResult = $WEBCentralizedShoppingCartAccreditationGetNewResult;
      return $this;
    }

}
