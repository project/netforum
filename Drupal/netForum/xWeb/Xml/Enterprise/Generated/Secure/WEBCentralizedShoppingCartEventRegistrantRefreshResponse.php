<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartEventRegistrantRefreshResponse
{

    /**
     * @var EventsRegistrantType $WEBCentralizedShoppingCartEventRegistrantRefreshResult
     */
    protected $WEBCentralizedShoppingCartEventRegistrantRefreshResult = null;

    /**
     * @param EventsRegistrantType $WEBCentralizedShoppingCartEventRegistrantRefreshResult
     */
    public function __construct($WEBCentralizedShoppingCartEventRegistrantRefreshResult)
    {
      $this->WEBCentralizedShoppingCartEventRegistrantRefreshResult = $WEBCentralizedShoppingCartEventRegistrantRefreshResult;
    }

    /**
     * @return EventsRegistrantType
     */
    public function getWEBCentralizedShoppingCartEventRegistrantRefreshResult()
    {
      return $this->WEBCentralizedShoppingCartEventRegistrantRefreshResult;
    }

    /**
     * @param EventsRegistrantType $WEBCentralizedShoppingCartEventRegistrantRefreshResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartEventRegistrantRefreshResponse
     */
    public function setWEBCentralizedShoppingCartEventRegistrantRefreshResult($WEBCentralizedShoppingCartEventRegistrantRefreshResult)
    {
      $this->WEBCentralizedShoppingCartEventRegistrantRefreshResult = $WEBCentralizedShoppingCartEventRegistrantRefreshResult;
      return $this;
    }

}
