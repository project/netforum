<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetOrganizationInformationResponse
{

    /**
     * @var GetOrganizationInformationResult $GetOrganizationInformationResult
     */
    protected $GetOrganizationInformationResult = null;

    /**
     * @param GetOrganizationInformationResult $GetOrganizationInformationResult
     */
    public function __construct($GetOrganizationInformationResult)
    {
      $this->GetOrganizationInformationResult = $GetOrganizationInformationResult;
    }

    /**
     * @return GetOrganizationInformationResult
     */
    public function getGetOrganizationInformationResult()
    {
      return $this->GetOrganizationInformationResult;
    }

    /**
     * @param GetOrganizationInformationResult $GetOrganizationInformationResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetOrganizationInformationResponse
     */
    public function setGetOrganizationInformationResult($GetOrganizationInformationResult)
    {
      $this->GetOrganizationInformationResult = $GetOrganizationInformationResult;
      return $this;
    }

}
