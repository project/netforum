<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserGetByRecno_Custom
{

    /**
     * @var string $cst_recno
     */
    protected $cst_recno = null;

    /**
     * @param string $cst_recno
     */
    public function __construct($cst_recno)
    {
      $this->cst_recno = $cst_recno;
    }

    /**
     * @return string
     */
    public function getCst_recno()
    {
      return $this->cst_recno;
    }

    /**
     * @param string $cst_recno
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserGetByRecno_Custom
     */
    public function setCst_recno($cst_recno)
    {
      $this->cst_recno = $cst_recno;
      return $this;
    }

}
