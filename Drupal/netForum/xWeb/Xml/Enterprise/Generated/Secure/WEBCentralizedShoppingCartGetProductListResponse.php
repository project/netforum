<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetProductListResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetProductListResult $WEBCentralizedShoppingCartGetProductListResult
     */
    protected $WEBCentralizedShoppingCartGetProductListResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetProductListResult $WEBCentralizedShoppingCartGetProductListResult
     */
    public function __construct($WEBCentralizedShoppingCartGetProductListResult)
    {
      $this->WEBCentralizedShoppingCartGetProductListResult = $WEBCentralizedShoppingCartGetProductListResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetProductListResult
     */
    public function getWEBCentralizedShoppingCartGetProductListResult()
    {
      return $this->WEBCentralizedShoppingCartGetProductListResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetProductListResult $WEBCentralizedShoppingCartGetProductListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetProductListResponse
     */
    public function setWEBCentralizedShoppingCartGetProductListResult($WEBCentralizedShoppingCartGetProductListResult)
    {
      $this->WEBCentralizedShoppingCartGetProductListResult = $WEBCentralizedShoppingCartGetProductListResult;
      return $this;
    }

}
