<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBOrganizationUpdateResponse
{

    /**
     * @var boolean $WEBOrganizationUpdateResult
     */
    protected $WEBOrganizationUpdateResult = null;

    /**
     * @param boolean $WEBOrganizationUpdateResult
     */
    public function __construct($WEBOrganizationUpdateResult)
    {
      $this->WEBOrganizationUpdateResult = $WEBOrganizationUpdateResult;
    }

    /**
     * @return boolean
     */
    public function getWEBOrganizationUpdateResult()
    {
      return $this->WEBOrganizationUpdateResult;
    }

    /**
     * @param boolean $WEBOrganizationUpdateResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBOrganizationUpdateResponse
     */
    public function setWEBOrganizationUpdateResult($WEBOrganizationUpdateResult)
    {
      $this->WEBOrganizationUpdateResult = $WEBOrganizationUpdateResult;
      return $this;
    }

}
