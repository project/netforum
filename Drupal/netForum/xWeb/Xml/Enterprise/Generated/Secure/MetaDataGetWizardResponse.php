<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class MetaDataGetWizardResponse
{

    /**
     * @var AVWizard $MetaDataGetWizardResult
     */
    protected $MetaDataGetWizardResult = null;

    /**
     * @param AVWizard $MetaDataGetWizardResult
     */
    public function __construct($MetaDataGetWizardResult)
    {
      $this->MetaDataGetWizardResult = $MetaDataGetWizardResult;
    }

    /**
     * @return AVWizard
     */
    public function getMetaDataGetWizardResult()
    {
      return $this->MetaDataGetWizardResult;
    }

    /**
     * @param AVWizard $MetaDataGetWizardResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\MetaDataGetWizardResponse
     */
    public function setMetaDataGetWizardResult($MetaDataGetWizardResult)
    {
      $this->MetaDataGetWizardResult = $MetaDataGetWizardResult;
      return $this;
    }

}
