<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType
{

    /**
     * @var av_key_Type $lxo__ixo_key
     */
    protected $lxo__ixo_key = null;

    /**
     * @var stringLength80_Type $lxo__ixo_rlt_code
     */
    protected $lxo__ixo_rlt_code = null;

    /**
     * @var stringLength150_Type $lxo__ixo_title
     */
    protected $lxo__ixo_title = null;

    /**
     * @var av_date_small_Type $lxo__ixo_start_date
     */
    protected $lxo__ixo_start_date = null;

    /**
     * @var av_date_small_Type $lxo__ixo_end_date
     */
    protected $lxo__ixo_end_date = null;

    /**
     * @var av_key_Type $lxo__ixo_ind_cst_key
     */
    protected $lxo__ixo_ind_cst_key = null;

    /**
     * @var av_key_Type $lxo__ixo_org_cst_key
     */
    protected $lxo__ixo_org_cst_key = null;

    /**
     * @var av_date_small_Type $lxo__ixo_add_date
     */
    protected $lxo__ixo_add_date = null;

    /**
     * @var av_user_Type $lxo__ixo_add_user
     */
    protected $lxo__ixo_add_user = null;

    /**
     * @var av_date_small_Type $lxo__ixo_change_date
     */
    protected $lxo__ixo_change_date = null;

    /**
     * @var av_user_Type $lxo__ixo_change_user
     */
    protected $lxo__ixo_change_user = null;

    /**
     * @var av_delete_flag_Type $lxo__ixo_delete_flag
     */
    protected $lxo__ixo_delete_flag = null;

    /**
     * @var av_key_Type $lxo__ixo_key_ext
     */
    protected $lxo__ixo_key_ext = null;

    /**
     * @var av_key_Type $lxo__ixo_cst_key_owner
     */
    protected $lxo__ixo_cst_key_owner = null;

    /**
     * @var av_key_Type $lxo__ixo_entity_key
     */
    protected $lxo__ixo_entity_key = null;

    /**
     * @var av_flag_Type $lxo__ixo_void_flag
     */
    protected $lxo__ixo_void_flag = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getLxo__ixo_key()
    {
      return $this->lxo__ixo_key;
    }

    /**
     * @param av_key_Type $lxo__ixo_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType
     */
    public function setLxo__ixo_key($lxo__ixo_key)
    {
      $this->lxo__ixo_key = $lxo__ixo_key;
      return $this;
    }

    /**
     * @return stringLength80_Type
     */
    public function getLxo__ixo_rlt_code()
    {
      return $this->lxo__ixo_rlt_code;
    }

    /**
     * @param stringLength80_Type $lxo__ixo_rlt_code
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType
     */
    public function setLxo__ixo_rlt_code($lxo__ixo_rlt_code)
    {
      $this->lxo__ixo_rlt_code = $lxo__ixo_rlt_code;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getLxo__ixo_title()
    {
      return $this->lxo__ixo_title;
    }

    /**
     * @param stringLength150_Type $lxo__ixo_title
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType
     */
    public function setLxo__ixo_title($lxo__ixo_title)
    {
      $this->lxo__ixo_title = $lxo__ixo_title;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getLxo__ixo_start_date()
    {
      return $this->lxo__ixo_start_date;
    }

    /**
     * @param av_date_small_Type $lxo__ixo_start_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType
     */
    public function setLxo__ixo_start_date($lxo__ixo_start_date)
    {
      $this->lxo__ixo_start_date = $lxo__ixo_start_date;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getLxo__ixo_end_date()
    {
      return $this->lxo__ixo_end_date;
    }

    /**
     * @param av_date_small_Type $lxo__ixo_end_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType
     */
    public function setLxo__ixo_end_date($lxo__ixo_end_date)
    {
      $this->lxo__ixo_end_date = $lxo__ixo_end_date;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getLxo__ixo_ind_cst_key()
    {
      return $this->lxo__ixo_ind_cst_key;
    }

    /**
     * @param av_key_Type $lxo__ixo_ind_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType
     */
    public function setLxo__ixo_ind_cst_key($lxo__ixo_ind_cst_key)
    {
      $this->lxo__ixo_ind_cst_key = $lxo__ixo_ind_cst_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getLxo__ixo_org_cst_key()
    {
      return $this->lxo__ixo_org_cst_key;
    }

    /**
     * @param av_key_Type $lxo__ixo_org_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType
     */
    public function setLxo__ixo_org_cst_key($lxo__ixo_org_cst_key)
    {
      $this->lxo__ixo_org_cst_key = $lxo__ixo_org_cst_key;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getLxo__ixo_add_date()
    {
      return $this->lxo__ixo_add_date;
    }

    /**
     * @param av_date_small_Type $lxo__ixo_add_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType
     */
    public function setLxo__ixo_add_date($lxo__ixo_add_date)
    {
      $this->lxo__ixo_add_date = $lxo__ixo_add_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getLxo__ixo_add_user()
    {
      return $this->lxo__ixo_add_user;
    }

    /**
     * @param av_user_Type $lxo__ixo_add_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType
     */
    public function setLxo__ixo_add_user($lxo__ixo_add_user)
    {
      $this->lxo__ixo_add_user = $lxo__ixo_add_user;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getLxo__ixo_change_date()
    {
      return $this->lxo__ixo_change_date;
    }

    /**
     * @param av_date_small_Type $lxo__ixo_change_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType
     */
    public function setLxo__ixo_change_date($lxo__ixo_change_date)
    {
      $this->lxo__ixo_change_date = $lxo__ixo_change_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getLxo__ixo_change_user()
    {
      return $this->lxo__ixo_change_user;
    }

    /**
     * @param av_user_Type $lxo__ixo_change_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType
     */
    public function setLxo__ixo_change_user($lxo__ixo_change_user)
    {
      $this->lxo__ixo_change_user = $lxo__ixo_change_user;
      return $this;
    }

    /**
     * @return av_delete_flag_Type
     */
    public function getLxo__ixo_delete_flag()
    {
      return $this->lxo__ixo_delete_flag;
    }

    /**
     * @param av_delete_flag_Type $lxo__ixo_delete_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType
     */
    public function setLxo__ixo_delete_flag($lxo__ixo_delete_flag)
    {
      $this->lxo__ixo_delete_flag = $lxo__ixo_delete_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getLxo__ixo_key_ext()
    {
      return $this->lxo__ixo_key_ext;
    }

    /**
     * @param av_key_Type $lxo__ixo_key_ext
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType
     */
    public function setLxo__ixo_key_ext($lxo__ixo_key_ext)
    {
      $this->lxo__ixo_key_ext = $lxo__ixo_key_ext;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getLxo__ixo_cst_key_owner()
    {
      return $this->lxo__ixo_cst_key_owner;
    }

    /**
     * @param av_key_Type $lxo__ixo_cst_key_owner
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType
     */
    public function setLxo__ixo_cst_key_owner($lxo__ixo_cst_key_owner)
    {
      $this->lxo__ixo_cst_key_owner = $lxo__ixo_cst_key_owner;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getLxo__ixo_entity_key()
    {
      return $this->lxo__ixo_entity_key;
    }

    /**
     * @param av_key_Type $lxo__ixo_entity_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType
     */
    public function setLxo__ixo_entity_key($lxo__ixo_entity_key)
    {
      $this->lxo__ixo_entity_key = $lxo__ixo_entity_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getLxo__ixo_void_flag()
    {
      return $this->lxo__ixo_void_flag;
    }

    /**
     * @param av_flag_Type $lxo__ixo_void_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Decision_Letter_Recipient_DataObjectType
     */
    public function setLxo__ixo_void_flag($lxo__ixo_void_flag)
    {
      $this->lxo__ixo_void_flag = $lxo__ixo_void_flag;
      return $this;
    }

}
