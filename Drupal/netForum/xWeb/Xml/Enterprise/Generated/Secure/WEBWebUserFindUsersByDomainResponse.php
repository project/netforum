<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserFindUsersByDomainResponse
{

    /**
     * @var WEBWebUserFindUsersByDomainResult $WEBWebUserFindUsersByDomainResult
     */
    protected $WEBWebUserFindUsersByDomainResult = null;

    /**
     * @param WEBWebUserFindUsersByDomainResult $WEBWebUserFindUsersByDomainResult
     */
    public function __construct($WEBWebUserFindUsersByDomainResult)
    {
      $this->WEBWebUserFindUsersByDomainResult = $WEBWebUserFindUsersByDomainResult;
    }

    /**
     * @return WEBWebUserFindUsersByDomainResult
     */
    public function getWEBWebUserFindUsersByDomainResult()
    {
      return $this->WEBWebUserFindUsersByDomainResult;
    }

    /**
     * @param WEBWebUserFindUsersByDomainResult $WEBWebUserFindUsersByDomainResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserFindUsersByDomainResponse
     */
    public function setWEBWebUserFindUsersByDomainResult($WEBWebUserFindUsersByDomainResult)
    {
      $this->WEBWebUserFindUsersByDomainResult = $WEBWebUserFindUsersByDomainResult;
      return $this;
    }

}
