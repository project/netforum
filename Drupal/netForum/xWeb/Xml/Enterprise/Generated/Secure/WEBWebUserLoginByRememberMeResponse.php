<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserLoginByRememberMeResponse
{

    /**
     * @var WebUserType $WEBWebUserLoginByRememberMeResult
     */
    protected $WEBWebUserLoginByRememberMeResult = null;

    /**
     * @param WebUserType $WEBWebUserLoginByRememberMeResult
     */
    public function __construct($WEBWebUserLoginByRememberMeResult)
    {
      $this->WEBWebUserLoginByRememberMeResult = $WEBWebUserLoginByRememberMeResult;
    }

    /**
     * @return WebUserType
     */
    public function getWEBWebUserLoginByRememberMeResult()
    {
      return $this->WEBWebUserLoginByRememberMeResult;
    }

    /**
     * @param WebUserType $WEBWebUserLoginByRememberMeResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserLoginByRememberMeResponse
     */
    public function setWEBWebUserLoginByRememberMeResult($WEBWebUserLoginByRememberMeResult)
    {
      $this->WEBWebUserLoginByRememberMeResult = $WEBWebUserLoginByRememberMeResult;
      return $this;
    }

}
