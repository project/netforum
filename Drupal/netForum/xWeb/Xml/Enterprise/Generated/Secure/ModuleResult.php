<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class ModuleResult
{

    /**
     * @var string $Module
     */
    protected $Module = null;

    /**
     * @var string $ErrorMessage
     */
    protected $ErrorMessage = null;

    /**
     * @var MergeResult $Status
     */
    protected $Status = null;

    /**
     * @param MergeResult $Status
     */
    public function __construct($Status)
    {
      $this->Status = $Status;
    }

    /**
     * @return string
     */
    public function getModule()
    {
      return $this->Module;
    }

    /**
     * @param string $Module
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ModuleResult
     */
    public function setModule($Module)
    {
      $this->Module = $Module;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
      return $this->ErrorMessage;
    }

    /**
     * @param string $ErrorMessage
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ModuleResult
     */
    public function setErrorMessage($ErrorMessage)
    {
      $this->ErrorMessage = $ErrorMessage;
      return $this;
    }

    /**
     * @return MergeResult
     */
    public function getStatus()
    {
      return $this->Status;
    }

    /**
     * @param MergeResult $Status
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\ModuleResult
     */
    public function setStatus($Status)
    {
      $this->Status = $Status;
      return $this;
    }

}
