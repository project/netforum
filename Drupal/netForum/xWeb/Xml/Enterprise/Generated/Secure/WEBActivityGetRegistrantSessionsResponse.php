<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBActivityGetRegistrantSessionsResponse
{

    /**
     * @var WEBActivityGetRegistrantSessionsResult $WEBActivityGetRegistrantSessionsResult
     */
    protected $WEBActivityGetRegistrantSessionsResult = null;

    /**
     * @param WEBActivityGetRegistrantSessionsResult $WEBActivityGetRegistrantSessionsResult
     */
    public function __construct($WEBActivityGetRegistrantSessionsResult)
    {
      $this->WEBActivityGetRegistrantSessionsResult = $WEBActivityGetRegistrantSessionsResult;
    }

    /**
     * @return WEBActivityGetRegistrantSessionsResult
     */
    public function getWEBActivityGetRegistrantSessionsResult()
    {
      return $this->WEBActivityGetRegistrantSessionsResult;
    }

    /**
     * @param WEBActivityGetRegistrantSessionsResult $WEBActivityGetRegistrantSessionsResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBActivityGetRegistrantSessionsResponse
     */
    public function setWEBActivityGetRegistrantSessionsResult($WEBActivityGetRegistrantSessionsResult)
    {
      $this->WEBActivityGetRegistrantSessionsResult = $WEBActivityGetRegistrantSessionsResult;
      return $this;
    }

}
