<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserCreate
{

    /**
     * @var WebUserType $oWebUser
     */
    protected $oWebUser = null;

    /**
     * @param WebUserType $oWebUser
     */
    public function __construct($oWebUser)
    {
      $this->oWebUser = $oWebUser;
    }

    /**
     * @return WebUserType
     */
    public function getOWebUser()
    {
      return $this->oWebUser;
    }

    /**
     * @param WebUserType $oWebUser
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserCreate
     */
    public function setOWebUser($oWebUser)
    {
      $this->oWebUser = $oWebUser;
      return $this;
    }

}
