<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class AccreditationArea_Affiliated_Survey_Contact_DataObjectType
{

    /**
     * @var av_key_Type $sur__cst_key
     */
    protected $sur__cst_key = null;

    /**
     * @var stringLength20_Type $sur__cst_type
     */
    protected $sur__cst_type = null;

    /**
     * @var stringLength150_Type $sur__cst_name_cp
     */
    protected $sur__cst_name_cp = null;

    /**
     * @var stringLength150_Type $sur__cst_sort_name_dn
     */
    protected $sur__cst_sort_name_dn = null;

    /**
     * @var stringLength150_Type $sur__cst_ind_full_name_dn
     */
    protected $sur__cst_ind_full_name_dn = null;

    /**
     * @var stringLength150_Type $sur__cst_org_name_dn
     */
    protected $sur__cst_org_name_dn = null;

    /**
     * @var stringLength150_Type $sur__cst_ixo_title_dn
     */
    protected $sur__cst_ixo_title_dn = null;

    /**
     * @var stringLength10_Type $sur__cst_pref_comm_meth
     */
    protected $sur__cst_pref_comm_meth = null;

    /**
     * @var av_text_Type $sur__cst_bio
     */
    protected $sur__cst_bio = null;

    /**
     * @var av_date_small_Type $sur__cst_add_date
     */
    protected $sur__cst_add_date = null;

    /**
     * @var av_user_Type $sur__cst_add_user
     */
    protected $sur__cst_add_user = null;

    /**
     * @var av_date_small_Type $sur__cst_change_date
     */
    protected $sur__cst_change_date = null;

    /**
     * @var av_user_Type $sur__cst_change_user
     */
    protected $sur__cst_change_user = null;

    /**
     * @var av_delete_flag_Type $sur__cst_delete_flag
     */
    protected $sur__cst_delete_flag = null;

    /**
     * @var av_recno_Type $sur__cst_recno
     */
    protected $sur__cst_recno = null;

    /**
     * @var stringLength10_Type $sur__cst_id
     */
    protected $sur__cst_id = null;

    /**
     * @var av_key_Type $sur__cst_key_ext
     */
    protected $sur__cst_key_ext = null;

    /**
     * @var av_flag_Type $sur__cst_email_text_only
     */
    protected $sur__cst_email_text_only = null;

    /**
     * @var av_currency_Type $sur__cst_credit_limit
     */
    protected $sur__cst_credit_limit = null;

    /**
     * @var av_key_Type $sur__cst_src_key
     */
    protected $sur__cst_src_key = null;

    /**
     * @var stringLength50_Type $sur__cst_src_code
     */
    protected $sur__cst_src_code = null;

    /**
     * @var av_flag_Type $sur__cst_tax_exempt_flag
     */
    protected $sur__cst_tax_exempt_flag = null;

    /**
     * @var stringLength30_Type $sur__cst_tax_id
     */
    protected $sur__cst_tax_id = null;

    /**
     * @var av_key_Type $sur__cst_cxa_key
     */
    protected $sur__cst_cxa_key = null;

    /**
     * @var av_flag_Type $sur__cst_no_email_flag
     */
    protected $sur__cst_no_email_flag = null;

    /**
     * @var av_key_Type $sur__cst_cxa_billing_key
     */
    protected $sur__cst_cxa_billing_key = null;

    /**
     * @var av_email_Type $sur__cst_eml_address_dn
     */
    protected $sur__cst_eml_address_dn = null;

    /**
     * @var av_key_Type $sur__cst_eml_key
     */
    protected $sur__cst_eml_key = null;

    /**
     * @var av_flag_Type $sur__cst_no_phone_flag
     */
    protected $sur__cst_no_phone_flag = null;

    /**
     * @var stringLength55_Type $sur__cst_phn_number_complete_dn
     */
    protected $sur__cst_phn_number_complete_dn = null;

    /**
     * @var av_key_Type $sur__cst_cph_key
     */
    protected $sur__cst_cph_key = null;

    /**
     * @var av_flag_Type $sur__cst_no_fax_flag
     */
    protected $sur__cst_no_fax_flag = null;

    /**
     * @var stringLength55_Type $sur__cst_fax_number_complete_dn
     */
    protected $sur__cst_fax_number_complete_dn = null;

    /**
     * @var av_key_Type $sur__cst_cfx_key
     */
    protected $sur__cst_cfx_key = null;

    /**
     * @var av_key_Type $sur__cst_ixo_key
     */
    protected $sur__cst_ixo_key = null;

    /**
     * @var av_flag_Type $sur__cst_no_web_flag
     */
    protected $sur__cst_no_web_flag = null;

    /**
     * @var stringLength15_Type $sur__cst_oldid
     */
    protected $sur__cst_oldid = null;

    /**
     * @var av_flag_Type $sur__cst_member_flag
     */
    protected $sur__cst_member_flag = null;

    /**
     * @var av_url_Type $sur__cst_url_code_dn
     */
    protected $sur__cst_url_code_dn = null;

    /**
     * @var av_key_Type $sur__cst_parent_cst_key
     */
    protected $sur__cst_parent_cst_key = null;

    /**
     * @var av_key_Type $sur__cst_url_key
     */
    protected $sur__cst_url_key = null;

    /**
     * @var av_flag_Type $sur__cst_no_msg_flag
     */
    protected $sur__cst_no_msg_flag = null;

    /**
     * @var av_messaging_name_Type $sur__cst_msg_handle_dn
     */
    protected $sur__cst_msg_handle_dn = null;

    /**
     * @var stringLength80_Type $sur__cst_web_login
     */
    protected $sur__cst_web_login = null;

    /**
     * @var stringLength50_Type $sur__cst_web_password
     */
    protected $sur__cst_web_password = null;

    /**
     * @var av_key_Type $sur__cst_entity_key
     */
    protected $sur__cst_entity_key = null;

    /**
     * @var av_key_Type $sur__cst_msg_key
     */
    protected $sur__cst_msg_key = null;

    /**
     * @var av_flag_Type $sur__cst_no_mail_flag
     */
    protected $sur__cst_no_mail_flag = null;

    /**
     * @var av_date_small_Type $sur__cst_web_start_date
     */
    protected $sur__cst_web_start_date = null;

    /**
     * @var av_date_small_Type $sur__cst_web_end_date
     */
    protected $sur__cst_web_end_date = null;

    /**
     * @var av_flag_Type $sur__cst_web_force_password_change
     */
    protected $sur__cst_web_force_password_change = null;

    /**
     * @var av_flag_Type $sur__cst_web_login_disabled_flag
     */
    protected $sur__cst_web_login_disabled_flag = null;

    /**
     * @var av_text_Type $sur__cst_comment
     */
    protected $sur__cst_comment = null;

    /**
     * @var av_flag_Type $sur__cst_credit_hold_flag
     */
    protected $sur__cst_credit_hold_flag = null;

    /**
     * @var stringLength50_Type $sur__cst_credit_hold_reason
     */
    protected $sur__cst_credit_hold_reason = null;

    /**
     * @var av_flag_Type $sur__cst_web_forgot_password_status
     */
    protected $sur__cst_web_forgot_password_status = null;

    /**
     * @var av_key_Type $sur__cst_old_cxa_key
     */
    protected $sur__cst_old_cxa_key = null;

    /**
     * @var av_date_small_Type $sur__cst_last_email_date
     */
    protected $sur__cst_last_email_date = null;

    /**
     * @var av_flag_Type $sur__cst_no_publish_flag
     */
    protected $sur__cst_no_publish_flag = null;

    /**
     * @var av_key_Type $sur__cst_sin_key
     */
    protected $sur__cst_sin_key = null;

    /**
     * @var av_key_Type $sur__cst_ttl_key
     */
    protected $sur__cst_ttl_key = null;

    /**
     * @var av_key_Type $sur__cst_jfn_key
     */
    protected $sur__cst_jfn_key = null;

    /**
     * @var av_key_Type $sur__cst_cur_key
     */
    protected $sur__cst_cur_key = null;

    /**
     * @var stringLength510_Type $sur__cst_attribute_1
     */
    protected $sur__cst_attribute_1 = null;

    /**
     * @var stringLength510_Type $sur__cst_attribute_2
     */
    protected $sur__cst_attribute_2 = null;

    /**
     * @var stringLength100_Type $sur__cst_salutation_1
     */
    protected $sur__cst_salutation_1 = null;

    /**
     * @var stringLength100_Type $sur__cst_salutation_2
     */
    protected $sur__cst_salutation_2 = null;

    /**
     * @var av_key_Type $sur__cst_merge_cst_key
     */
    protected $sur__cst_merge_cst_key = null;

    /**
     * @var stringLength100_Type $sur__cst_salutation_3
     */
    protected $sur__cst_salutation_3 = null;

    /**
     * @var stringLength100_Type $sur__cst_salutation_4
     */
    protected $sur__cst_salutation_4 = null;

    /**
     * @var stringLength200_Type $sur__cst_default_recognize_as
     */
    protected $sur__cst_default_recognize_as = null;

    /**
     * @var av_decimal4_Type $sur__cst_score
     */
    protected $sur__cst_score = null;

    /**
     * @var av_integer_Type $sur__cst_score_normalized
     */
    protected $sur__cst_score_normalized = null;

    /**
     * @var av_integer_Type $sur__cst_score_trend
     */
    protected $sur__cst_score_trend = null;

    /**
     * @var stringLength25_Type $sur__cst_vault_account
     */
    protected $sur__cst_vault_account = null;

    /**
     * @var av_flag_Type $sur__cst_exclude_from_social_flag
     */
    protected $sur__cst_exclude_from_social_flag = null;

    /**
     * @var av_integer_Type $sur__cst_social_score
     */
    protected $sur__cst_social_score = null;

    /**
     * @var stringLength9_Type $sur__cst_ptin
     */
    protected $sur__cst_ptin = null;

    /**
     * @var av_recno_Type $sur__cst_aicpa_member_id
     */
    protected $sur__cst_aicpa_member_id = null;

    /**
     * @var stringLength50_Type $sur__cst_vendor_code
     */
    protected $sur__cst_vendor_code = null;

    /**
     * @var stringLength1_Type $sur__cst_salt
     */
    protected $sur__cst_salt = null;

    /**
     * @var av_key_Type $sur__cst_sca_key
     */
    protected $sur__cst_sca_key = null;

    /**
     * @var av_integer_Type $sur__cst_iterations
     */
    protected $sur__cst_iterations = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getSur__cst_key()
    {
      return $this->sur__cst_key;
    }

    /**
     * @param av_key_Type $sur__cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_key($sur__cst_key)
    {
      $this->sur__cst_key = $sur__cst_key;
      return $this;
    }

    /**
     * @return stringLength20_Type
     */
    public function getSur__cst_type()
    {
      return $this->sur__cst_type;
    }

    /**
     * @param stringLength20_Type $sur__cst_type
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_type($sur__cst_type)
    {
      $this->sur__cst_type = $sur__cst_type;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getSur__cst_name_cp()
    {
      return $this->sur__cst_name_cp;
    }

    /**
     * @param stringLength150_Type $sur__cst_name_cp
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_name_cp($sur__cst_name_cp)
    {
      $this->sur__cst_name_cp = $sur__cst_name_cp;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getSur__cst_sort_name_dn()
    {
      return $this->sur__cst_sort_name_dn;
    }

    /**
     * @param stringLength150_Type $sur__cst_sort_name_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_sort_name_dn($sur__cst_sort_name_dn)
    {
      $this->sur__cst_sort_name_dn = $sur__cst_sort_name_dn;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getSur__cst_ind_full_name_dn()
    {
      return $this->sur__cst_ind_full_name_dn;
    }

    /**
     * @param stringLength150_Type $sur__cst_ind_full_name_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_ind_full_name_dn($sur__cst_ind_full_name_dn)
    {
      $this->sur__cst_ind_full_name_dn = $sur__cst_ind_full_name_dn;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getSur__cst_org_name_dn()
    {
      return $this->sur__cst_org_name_dn;
    }

    /**
     * @param stringLength150_Type $sur__cst_org_name_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_org_name_dn($sur__cst_org_name_dn)
    {
      $this->sur__cst_org_name_dn = $sur__cst_org_name_dn;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getSur__cst_ixo_title_dn()
    {
      return $this->sur__cst_ixo_title_dn;
    }

    /**
     * @param stringLength150_Type $sur__cst_ixo_title_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_ixo_title_dn($sur__cst_ixo_title_dn)
    {
      $this->sur__cst_ixo_title_dn = $sur__cst_ixo_title_dn;
      return $this;
    }

    /**
     * @return stringLength10_Type
     */
    public function getSur__cst_pref_comm_meth()
    {
      return $this->sur__cst_pref_comm_meth;
    }

    /**
     * @param stringLength10_Type $sur__cst_pref_comm_meth
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_pref_comm_meth($sur__cst_pref_comm_meth)
    {
      $this->sur__cst_pref_comm_meth = $sur__cst_pref_comm_meth;
      return $this;
    }

    /**
     * @return av_text_Type
     */
    public function getSur__cst_bio()
    {
      return $this->sur__cst_bio;
    }

    /**
     * @param av_text_Type $sur__cst_bio
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_bio($sur__cst_bio)
    {
      $this->sur__cst_bio = $sur__cst_bio;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getSur__cst_add_date()
    {
      return $this->sur__cst_add_date;
    }

    /**
     * @param av_date_small_Type $sur__cst_add_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_add_date($sur__cst_add_date)
    {
      $this->sur__cst_add_date = $sur__cst_add_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getSur__cst_add_user()
    {
      return $this->sur__cst_add_user;
    }

    /**
     * @param av_user_Type $sur__cst_add_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_add_user($sur__cst_add_user)
    {
      $this->sur__cst_add_user = $sur__cst_add_user;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getSur__cst_change_date()
    {
      return $this->sur__cst_change_date;
    }

    /**
     * @param av_date_small_Type $sur__cst_change_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_change_date($sur__cst_change_date)
    {
      $this->sur__cst_change_date = $sur__cst_change_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getSur__cst_change_user()
    {
      return $this->sur__cst_change_user;
    }

    /**
     * @param av_user_Type $sur__cst_change_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_change_user($sur__cst_change_user)
    {
      $this->sur__cst_change_user = $sur__cst_change_user;
      return $this;
    }

    /**
     * @return av_delete_flag_Type
     */
    public function getSur__cst_delete_flag()
    {
      return $this->sur__cst_delete_flag;
    }

    /**
     * @param av_delete_flag_Type $sur__cst_delete_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_delete_flag($sur__cst_delete_flag)
    {
      $this->sur__cst_delete_flag = $sur__cst_delete_flag;
      return $this;
    }

    /**
     * @return av_recno_Type
     */
    public function getSur__cst_recno()
    {
      return $this->sur__cst_recno;
    }

    /**
     * @param av_recno_Type $sur__cst_recno
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_recno($sur__cst_recno)
    {
      $this->sur__cst_recno = $sur__cst_recno;
      return $this;
    }

    /**
     * @return stringLength10_Type
     */
    public function getSur__cst_id()
    {
      return $this->sur__cst_id;
    }

    /**
     * @param stringLength10_Type $sur__cst_id
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_id($sur__cst_id)
    {
      $this->sur__cst_id = $sur__cst_id;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSur__cst_key_ext()
    {
      return $this->sur__cst_key_ext;
    }

    /**
     * @param av_key_Type $sur__cst_key_ext
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_key_ext($sur__cst_key_ext)
    {
      $this->sur__cst_key_ext = $sur__cst_key_ext;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getSur__cst_email_text_only()
    {
      return $this->sur__cst_email_text_only;
    }

    /**
     * @param av_flag_Type $sur__cst_email_text_only
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_email_text_only($sur__cst_email_text_only)
    {
      $this->sur__cst_email_text_only = $sur__cst_email_text_only;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getSur__cst_credit_limit()
    {
      return $this->sur__cst_credit_limit;
    }

    /**
     * @param av_currency_Type $sur__cst_credit_limit
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_credit_limit($sur__cst_credit_limit)
    {
      $this->sur__cst_credit_limit = $sur__cst_credit_limit;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSur__cst_src_key()
    {
      return $this->sur__cst_src_key;
    }

    /**
     * @param av_key_Type $sur__cst_src_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_src_key($sur__cst_src_key)
    {
      $this->sur__cst_src_key = $sur__cst_src_key;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getSur__cst_src_code()
    {
      return $this->sur__cst_src_code;
    }

    /**
     * @param stringLength50_Type $sur__cst_src_code
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_src_code($sur__cst_src_code)
    {
      $this->sur__cst_src_code = $sur__cst_src_code;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getSur__cst_tax_exempt_flag()
    {
      return $this->sur__cst_tax_exempt_flag;
    }

    /**
     * @param av_flag_Type $sur__cst_tax_exempt_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_tax_exempt_flag($sur__cst_tax_exempt_flag)
    {
      $this->sur__cst_tax_exempt_flag = $sur__cst_tax_exempt_flag;
      return $this;
    }

    /**
     * @return stringLength30_Type
     */
    public function getSur__cst_tax_id()
    {
      return $this->sur__cst_tax_id;
    }

    /**
     * @param stringLength30_Type $sur__cst_tax_id
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_tax_id($sur__cst_tax_id)
    {
      $this->sur__cst_tax_id = $sur__cst_tax_id;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSur__cst_cxa_key()
    {
      return $this->sur__cst_cxa_key;
    }

    /**
     * @param av_key_Type $sur__cst_cxa_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_cxa_key($sur__cst_cxa_key)
    {
      $this->sur__cst_cxa_key = $sur__cst_cxa_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getSur__cst_no_email_flag()
    {
      return $this->sur__cst_no_email_flag;
    }

    /**
     * @param av_flag_Type $sur__cst_no_email_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_no_email_flag($sur__cst_no_email_flag)
    {
      $this->sur__cst_no_email_flag = $sur__cst_no_email_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSur__cst_cxa_billing_key()
    {
      return $this->sur__cst_cxa_billing_key;
    }

    /**
     * @param av_key_Type $sur__cst_cxa_billing_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_cxa_billing_key($sur__cst_cxa_billing_key)
    {
      $this->sur__cst_cxa_billing_key = $sur__cst_cxa_billing_key;
      return $this;
    }

    /**
     * @return av_email_Type
     */
    public function getSur__cst_eml_address_dn()
    {
      return $this->sur__cst_eml_address_dn;
    }

    /**
     * @param av_email_Type $sur__cst_eml_address_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_eml_address_dn($sur__cst_eml_address_dn)
    {
      $this->sur__cst_eml_address_dn = $sur__cst_eml_address_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSur__cst_eml_key()
    {
      return $this->sur__cst_eml_key;
    }

    /**
     * @param av_key_Type $sur__cst_eml_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_eml_key($sur__cst_eml_key)
    {
      $this->sur__cst_eml_key = $sur__cst_eml_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getSur__cst_no_phone_flag()
    {
      return $this->sur__cst_no_phone_flag;
    }

    /**
     * @param av_flag_Type $sur__cst_no_phone_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_no_phone_flag($sur__cst_no_phone_flag)
    {
      $this->sur__cst_no_phone_flag = $sur__cst_no_phone_flag;
      return $this;
    }

    /**
     * @return stringLength55_Type
     */
    public function getSur__cst_phn_number_complete_dn()
    {
      return $this->sur__cst_phn_number_complete_dn;
    }

    /**
     * @param stringLength55_Type $sur__cst_phn_number_complete_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_phn_number_complete_dn($sur__cst_phn_number_complete_dn)
    {
      $this->sur__cst_phn_number_complete_dn = $sur__cst_phn_number_complete_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSur__cst_cph_key()
    {
      return $this->sur__cst_cph_key;
    }

    /**
     * @param av_key_Type $sur__cst_cph_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_cph_key($sur__cst_cph_key)
    {
      $this->sur__cst_cph_key = $sur__cst_cph_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getSur__cst_no_fax_flag()
    {
      return $this->sur__cst_no_fax_flag;
    }

    /**
     * @param av_flag_Type $sur__cst_no_fax_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_no_fax_flag($sur__cst_no_fax_flag)
    {
      $this->sur__cst_no_fax_flag = $sur__cst_no_fax_flag;
      return $this;
    }

    /**
     * @return stringLength55_Type
     */
    public function getSur__cst_fax_number_complete_dn()
    {
      return $this->sur__cst_fax_number_complete_dn;
    }

    /**
     * @param stringLength55_Type $sur__cst_fax_number_complete_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_fax_number_complete_dn($sur__cst_fax_number_complete_dn)
    {
      $this->sur__cst_fax_number_complete_dn = $sur__cst_fax_number_complete_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSur__cst_cfx_key()
    {
      return $this->sur__cst_cfx_key;
    }

    /**
     * @param av_key_Type $sur__cst_cfx_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_cfx_key($sur__cst_cfx_key)
    {
      $this->sur__cst_cfx_key = $sur__cst_cfx_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSur__cst_ixo_key()
    {
      return $this->sur__cst_ixo_key;
    }

    /**
     * @param av_key_Type $sur__cst_ixo_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_ixo_key($sur__cst_ixo_key)
    {
      $this->sur__cst_ixo_key = $sur__cst_ixo_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getSur__cst_no_web_flag()
    {
      return $this->sur__cst_no_web_flag;
    }

    /**
     * @param av_flag_Type $sur__cst_no_web_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_no_web_flag($sur__cst_no_web_flag)
    {
      $this->sur__cst_no_web_flag = $sur__cst_no_web_flag;
      return $this;
    }

    /**
     * @return stringLength15_Type
     */
    public function getSur__cst_oldid()
    {
      return $this->sur__cst_oldid;
    }

    /**
     * @param stringLength15_Type $sur__cst_oldid
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_oldid($sur__cst_oldid)
    {
      $this->sur__cst_oldid = $sur__cst_oldid;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getSur__cst_member_flag()
    {
      return $this->sur__cst_member_flag;
    }

    /**
     * @param av_flag_Type $sur__cst_member_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_member_flag($sur__cst_member_flag)
    {
      $this->sur__cst_member_flag = $sur__cst_member_flag;
      return $this;
    }

    /**
     * @return av_url_Type
     */
    public function getSur__cst_url_code_dn()
    {
      return $this->sur__cst_url_code_dn;
    }

    /**
     * @param av_url_Type $sur__cst_url_code_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_url_code_dn($sur__cst_url_code_dn)
    {
      $this->sur__cst_url_code_dn = $sur__cst_url_code_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSur__cst_parent_cst_key()
    {
      return $this->sur__cst_parent_cst_key;
    }

    /**
     * @param av_key_Type $sur__cst_parent_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_parent_cst_key($sur__cst_parent_cst_key)
    {
      $this->sur__cst_parent_cst_key = $sur__cst_parent_cst_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSur__cst_url_key()
    {
      return $this->sur__cst_url_key;
    }

    /**
     * @param av_key_Type $sur__cst_url_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_url_key($sur__cst_url_key)
    {
      $this->sur__cst_url_key = $sur__cst_url_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getSur__cst_no_msg_flag()
    {
      return $this->sur__cst_no_msg_flag;
    }

    /**
     * @param av_flag_Type $sur__cst_no_msg_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_no_msg_flag($sur__cst_no_msg_flag)
    {
      $this->sur__cst_no_msg_flag = $sur__cst_no_msg_flag;
      return $this;
    }

    /**
     * @return av_messaging_name_Type
     */
    public function getSur__cst_msg_handle_dn()
    {
      return $this->sur__cst_msg_handle_dn;
    }

    /**
     * @param av_messaging_name_Type $sur__cst_msg_handle_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_msg_handle_dn($sur__cst_msg_handle_dn)
    {
      $this->sur__cst_msg_handle_dn = $sur__cst_msg_handle_dn;
      return $this;
    }

    /**
     * @return stringLength80_Type
     */
    public function getSur__cst_web_login()
    {
      return $this->sur__cst_web_login;
    }

    /**
     * @param stringLength80_Type $sur__cst_web_login
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_web_login($sur__cst_web_login)
    {
      $this->sur__cst_web_login = $sur__cst_web_login;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getSur__cst_web_password()
    {
      return $this->sur__cst_web_password;
    }

    /**
     * @param stringLength50_Type $sur__cst_web_password
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_web_password($sur__cst_web_password)
    {
      $this->sur__cst_web_password = $sur__cst_web_password;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSur__cst_entity_key()
    {
      return $this->sur__cst_entity_key;
    }

    /**
     * @param av_key_Type $sur__cst_entity_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_entity_key($sur__cst_entity_key)
    {
      $this->sur__cst_entity_key = $sur__cst_entity_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSur__cst_msg_key()
    {
      return $this->sur__cst_msg_key;
    }

    /**
     * @param av_key_Type $sur__cst_msg_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_msg_key($sur__cst_msg_key)
    {
      $this->sur__cst_msg_key = $sur__cst_msg_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getSur__cst_no_mail_flag()
    {
      return $this->sur__cst_no_mail_flag;
    }

    /**
     * @param av_flag_Type $sur__cst_no_mail_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_no_mail_flag($sur__cst_no_mail_flag)
    {
      $this->sur__cst_no_mail_flag = $sur__cst_no_mail_flag;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getSur__cst_web_start_date()
    {
      return $this->sur__cst_web_start_date;
    }

    /**
     * @param av_date_small_Type $sur__cst_web_start_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_web_start_date($sur__cst_web_start_date)
    {
      $this->sur__cst_web_start_date = $sur__cst_web_start_date;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getSur__cst_web_end_date()
    {
      return $this->sur__cst_web_end_date;
    }

    /**
     * @param av_date_small_Type $sur__cst_web_end_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_web_end_date($sur__cst_web_end_date)
    {
      $this->sur__cst_web_end_date = $sur__cst_web_end_date;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getSur__cst_web_force_password_change()
    {
      return $this->sur__cst_web_force_password_change;
    }

    /**
     * @param av_flag_Type $sur__cst_web_force_password_change
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_web_force_password_change($sur__cst_web_force_password_change)
    {
      $this->sur__cst_web_force_password_change = $sur__cst_web_force_password_change;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getSur__cst_web_login_disabled_flag()
    {
      return $this->sur__cst_web_login_disabled_flag;
    }

    /**
     * @param av_flag_Type $sur__cst_web_login_disabled_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_web_login_disabled_flag($sur__cst_web_login_disabled_flag)
    {
      $this->sur__cst_web_login_disabled_flag = $sur__cst_web_login_disabled_flag;
      return $this;
    }

    /**
     * @return av_text_Type
     */
    public function getSur__cst_comment()
    {
      return $this->sur__cst_comment;
    }

    /**
     * @param av_text_Type $sur__cst_comment
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_comment($sur__cst_comment)
    {
      $this->sur__cst_comment = $sur__cst_comment;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getSur__cst_credit_hold_flag()
    {
      return $this->sur__cst_credit_hold_flag;
    }

    /**
     * @param av_flag_Type $sur__cst_credit_hold_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_credit_hold_flag($sur__cst_credit_hold_flag)
    {
      $this->sur__cst_credit_hold_flag = $sur__cst_credit_hold_flag;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getSur__cst_credit_hold_reason()
    {
      return $this->sur__cst_credit_hold_reason;
    }

    /**
     * @param stringLength50_Type $sur__cst_credit_hold_reason
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_credit_hold_reason($sur__cst_credit_hold_reason)
    {
      $this->sur__cst_credit_hold_reason = $sur__cst_credit_hold_reason;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getSur__cst_web_forgot_password_status()
    {
      return $this->sur__cst_web_forgot_password_status;
    }

    /**
     * @param av_flag_Type $sur__cst_web_forgot_password_status
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_web_forgot_password_status($sur__cst_web_forgot_password_status)
    {
      $this->sur__cst_web_forgot_password_status = $sur__cst_web_forgot_password_status;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSur__cst_old_cxa_key()
    {
      return $this->sur__cst_old_cxa_key;
    }

    /**
     * @param av_key_Type $sur__cst_old_cxa_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_old_cxa_key($sur__cst_old_cxa_key)
    {
      $this->sur__cst_old_cxa_key = $sur__cst_old_cxa_key;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getSur__cst_last_email_date()
    {
      return $this->sur__cst_last_email_date;
    }

    /**
     * @param av_date_small_Type $sur__cst_last_email_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_last_email_date($sur__cst_last_email_date)
    {
      $this->sur__cst_last_email_date = $sur__cst_last_email_date;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getSur__cst_no_publish_flag()
    {
      return $this->sur__cst_no_publish_flag;
    }

    /**
     * @param av_flag_Type $sur__cst_no_publish_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_no_publish_flag($sur__cst_no_publish_flag)
    {
      $this->sur__cst_no_publish_flag = $sur__cst_no_publish_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSur__cst_sin_key()
    {
      return $this->sur__cst_sin_key;
    }

    /**
     * @param av_key_Type $sur__cst_sin_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_sin_key($sur__cst_sin_key)
    {
      $this->sur__cst_sin_key = $sur__cst_sin_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSur__cst_ttl_key()
    {
      return $this->sur__cst_ttl_key;
    }

    /**
     * @param av_key_Type $sur__cst_ttl_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_ttl_key($sur__cst_ttl_key)
    {
      $this->sur__cst_ttl_key = $sur__cst_ttl_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSur__cst_jfn_key()
    {
      return $this->sur__cst_jfn_key;
    }

    /**
     * @param av_key_Type $sur__cst_jfn_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_jfn_key($sur__cst_jfn_key)
    {
      $this->sur__cst_jfn_key = $sur__cst_jfn_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSur__cst_cur_key()
    {
      return $this->sur__cst_cur_key;
    }

    /**
     * @param av_key_Type $sur__cst_cur_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_cur_key($sur__cst_cur_key)
    {
      $this->sur__cst_cur_key = $sur__cst_cur_key;
      return $this;
    }

    /**
     * @return stringLength510_Type
     */
    public function getSur__cst_attribute_1()
    {
      return $this->sur__cst_attribute_1;
    }

    /**
     * @param stringLength510_Type $sur__cst_attribute_1
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_attribute_1($sur__cst_attribute_1)
    {
      $this->sur__cst_attribute_1 = $sur__cst_attribute_1;
      return $this;
    }

    /**
     * @return stringLength510_Type
     */
    public function getSur__cst_attribute_2()
    {
      return $this->sur__cst_attribute_2;
    }

    /**
     * @param stringLength510_Type $sur__cst_attribute_2
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_attribute_2($sur__cst_attribute_2)
    {
      $this->sur__cst_attribute_2 = $sur__cst_attribute_2;
      return $this;
    }

    /**
     * @return stringLength100_Type
     */
    public function getSur__cst_salutation_1()
    {
      return $this->sur__cst_salutation_1;
    }

    /**
     * @param stringLength100_Type $sur__cst_salutation_1
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_salutation_1($sur__cst_salutation_1)
    {
      $this->sur__cst_salutation_1 = $sur__cst_salutation_1;
      return $this;
    }

    /**
     * @return stringLength100_Type
     */
    public function getSur__cst_salutation_2()
    {
      return $this->sur__cst_salutation_2;
    }

    /**
     * @param stringLength100_Type $sur__cst_salutation_2
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_salutation_2($sur__cst_salutation_2)
    {
      $this->sur__cst_salutation_2 = $sur__cst_salutation_2;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSur__cst_merge_cst_key()
    {
      return $this->sur__cst_merge_cst_key;
    }

    /**
     * @param av_key_Type $sur__cst_merge_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_merge_cst_key($sur__cst_merge_cst_key)
    {
      $this->sur__cst_merge_cst_key = $sur__cst_merge_cst_key;
      return $this;
    }

    /**
     * @return stringLength100_Type
     */
    public function getSur__cst_salutation_3()
    {
      return $this->sur__cst_salutation_3;
    }

    /**
     * @param stringLength100_Type $sur__cst_salutation_3
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_salutation_3($sur__cst_salutation_3)
    {
      $this->sur__cst_salutation_3 = $sur__cst_salutation_3;
      return $this;
    }

    /**
     * @return stringLength100_Type
     */
    public function getSur__cst_salutation_4()
    {
      return $this->sur__cst_salutation_4;
    }

    /**
     * @param stringLength100_Type $sur__cst_salutation_4
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_salutation_4($sur__cst_salutation_4)
    {
      $this->sur__cst_salutation_4 = $sur__cst_salutation_4;
      return $this;
    }

    /**
     * @return stringLength200_Type
     */
    public function getSur__cst_default_recognize_as()
    {
      return $this->sur__cst_default_recognize_as;
    }

    /**
     * @param stringLength200_Type $sur__cst_default_recognize_as
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_default_recognize_as($sur__cst_default_recognize_as)
    {
      $this->sur__cst_default_recognize_as = $sur__cst_default_recognize_as;
      return $this;
    }

    /**
     * @return av_decimal4_Type
     */
    public function getSur__cst_score()
    {
      return $this->sur__cst_score;
    }

    /**
     * @param av_decimal4_Type $sur__cst_score
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_score($sur__cst_score)
    {
      $this->sur__cst_score = $sur__cst_score;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getSur__cst_score_normalized()
    {
      return $this->sur__cst_score_normalized;
    }

    /**
     * @param av_integer_Type $sur__cst_score_normalized
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_score_normalized($sur__cst_score_normalized)
    {
      $this->sur__cst_score_normalized = $sur__cst_score_normalized;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getSur__cst_score_trend()
    {
      return $this->sur__cst_score_trend;
    }

    /**
     * @param av_integer_Type $sur__cst_score_trend
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_score_trend($sur__cst_score_trend)
    {
      $this->sur__cst_score_trend = $sur__cst_score_trend;
      return $this;
    }

    /**
     * @return stringLength25_Type
     */
    public function getSur__cst_vault_account()
    {
      return $this->sur__cst_vault_account;
    }

    /**
     * @param stringLength25_Type $sur__cst_vault_account
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_vault_account($sur__cst_vault_account)
    {
      $this->sur__cst_vault_account = $sur__cst_vault_account;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getSur__cst_exclude_from_social_flag()
    {
      return $this->sur__cst_exclude_from_social_flag;
    }

    /**
     * @param av_flag_Type $sur__cst_exclude_from_social_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_exclude_from_social_flag($sur__cst_exclude_from_social_flag)
    {
      $this->sur__cst_exclude_from_social_flag = $sur__cst_exclude_from_social_flag;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getSur__cst_social_score()
    {
      return $this->sur__cst_social_score;
    }

    /**
     * @param av_integer_Type $sur__cst_social_score
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_social_score($sur__cst_social_score)
    {
      $this->sur__cst_social_score = $sur__cst_social_score;
      return $this;
    }

    /**
     * @return stringLength9_Type
     */
    public function getSur__cst_ptin()
    {
      return $this->sur__cst_ptin;
    }

    /**
     * @param stringLength9_Type $sur__cst_ptin
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_ptin($sur__cst_ptin)
    {
      $this->sur__cst_ptin = $sur__cst_ptin;
      return $this;
    }

    /**
     * @return av_recno_Type
     */
    public function getSur__cst_aicpa_member_id()
    {
      return $this->sur__cst_aicpa_member_id;
    }

    /**
     * @param av_recno_Type $sur__cst_aicpa_member_id
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_aicpa_member_id($sur__cst_aicpa_member_id)
    {
      $this->sur__cst_aicpa_member_id = $sur__cst_aicpa_member_id;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getSur__cst_vendor_code()
    {
      return $this->sur__cst_vendor_code;
    }

    /**
     * @param stringLength50_Type $sur__cst_vendor_code
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_vendor_code($sur__cst_vendor_code)
    {
      $this->sur__cst_vendor_code = $sur__cst_vendor_code;
      return $this;
    }

    /**
     * @return stringLength1_Type
     */
    public function getSur__cst_salt()
    {
      return $this->sur__cst_salt;
    }

    /**
     * @param stringLength1_Type $sur__cst_salt
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_salt($sur__cst_salt)
    {
      $this->sur__cst_salt = $sur__cst_salt;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getSur__cst_sca_key()
    {
      return $this->sur__cst_sca_key;
    }

    /**
     * @param av_key_Type $sur__cst_sca_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_sca_key($sur__cst_sca_key)
    {
      $this->sur__cst_sca_key = $sur__cst_sca_key;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getSur__cst_iterations()
    {
      return $this->sur__cst_iterations;
    }

    /**
     * @param av_integer_Type $sur__cst_iterations
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_Survey_Contact_DataObjectType
     */
    public function setSur__cst_iterations($sur__cst_iterations)
    {
      $this->sur__cst_iterations = $sur__cst_iterations;
      return $this;
    }

}
