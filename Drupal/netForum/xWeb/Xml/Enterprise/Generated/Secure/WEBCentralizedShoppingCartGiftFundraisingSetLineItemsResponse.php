<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGiftFundraisingSetLineItemsResponse
{

    /**
     * @var FundraisingGiftType $WEBCentralizedShoppingCartGiftFundraisingSetLineItemsResult
     */
    protected $WEBCentralizedShoppingCartGiftFundraisingSetLineItemsResult = null;

    /**
     * @param FundraisingGiftType $WEBCentralizedShoppingCartGiftFundraisingSetLineItemsResult
     */
    public function __construct($WEBCentralizedShoppingCartGiftFundraisingSetLineItemsResult)
    {
      $this->WEBCentralizedShoppingCartGiftFundraisingSetLineItemsResult = $WEBCentralizedShoppingCartGiftFundraisingSetLineItemsResult;
    }

    /**
     * @return FundraisingGiftType
     */
    public function getWEBCentralizedShoppingCartGiftFundraisingSetLineItemsResult()
    {
      return $this->WEBCentralizedShoppingCartGiftFundraisingSetLineItemsResult;
    }

    /**
     * @param FundraisingGiftType $WEBCentralizedShoppingCartGiftFundraisingSetLineItemsResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGiftFundraisingSetLineItemsResponse
     */
    public function setWEBCentralizedShoppingCartGiftFundraisingSetLineItemsResult($WEBCentralizedShoppingCartGiftFundraisingSetLineItemsResult)
    {
      $this->WEBCentralizedShoppingCartGiftFundraisingSetLineItemsResult = $WEBCentralizedShoppingCartGiftFundraisingSetLineItemsResult;
      return $this;
    }

}
