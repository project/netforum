<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserLoginAndRememberMe
{

    /**
     * @var string $LoginOrEmail
     */
    protected $LoginOrEmail = null;

    /**
     * @var string $password
     */
    protected $password = null;

    /**
     * @param string $LoginOrEmail
     * @param string $password
     */
    public function __construct($LoginOrEmail, $password)
    {
      $this->LoginOrEmail = $LoginOrEmail;
      $this->password = $password;
    }

    /**
     * @return string
     */
    public function getLoginOrEmail()
    {
      return $this->LoginOrEmail;
    }

    /**
     * @param string $LoginOrEmail
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserLoginAndRememberMe
     */
    public function setLoginOrEmail($LoginOrEmail)
    {
      $this->LoginOrEmail = $LoginOrEmail;
      return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
      return $this->password;
    }

    /**
     * @param string $password
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserLoginAndRememberMe
     */
    public function setPassword($password)
    {
      $this->password = $password;
      return $this;
    }

}
