<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetBoothListResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetBoothListResult $WEBCentralizedShoppingCartGetBoothListResult
     */
    protected $WEBCentralizedShoppingCartGetBoothListResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetBoothListResult $WEBCentralizedShoppingCartGetBoothListResult
     */
    public function __construct($WEBCentralizedShoppingCartGetBoothListResult)
    {
      $this->WEBCentralizedShoppingCartGetBoothListResult = $WEBCentralizedShoppingCartGetBoothListResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetBoothListResult
     */
    public function getWEBCentralizedShoppingCartGetBoothListResult()
    {
      return $this->WEBCentralizedShoppingCartGetBoothListResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetBoothListResult $WEBCentralizedShoppingCartGetBoothListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetBoothListResponse
     */
    public function setWEBCentralizedShoppingCartGetBoothListResult($WEBCentralizedShoppingCartGetBoothListResult)
    {
      $this->WEBCentralizedShoppingCartGetBoothListResult = $WEBCentralizedShoppingCartGetBoothListResult;
      return $this;
    }

}
