<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartMembershipOpenInvoiceGetResponse
{

    /**
     * @var InvoiceType $WEBCentralizedShoppingCartMembershipOpenInvoiceGetResult
     */
    protected $WEBCentralizedShoppingCartMembershipOpenInvoiceGetResult = null;

    /**
     * @param InvoiceType $WEBCentralizedShoppingCartMembershipOpenInvoiceGetResult
     */
    public function __construct($WEBCentralizedShoppingCartMembershipOpenInvoiceGetResult)
    {
      $this->WEBCentralizedShoppingCartMembershipOpenInvoiceGetResult = $WEBCentralizedShoppingCartMembershipOpenInvoiceGetResult;
    }

    /**
     * @return InvoiceType
     */
    public function getWEBCentralizedShoppingCartMembershipOpenInvoiceGetResult()
    {
      return $this->WEBCentralizedShoppingCartMembershipOpenInvoiceGetResult;
    }

    /**
     * @param InvoiceType $WEBCentralizedShoppingCartMembershipOpenInvoiceGetResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartMembershipOpenInvoiceGetResponse
     */
    public function setWEBCentralizedShoppingCartMembershipOpenInvoiceGetResult($WEBCentralizedShoppingCartMembershipOpenInvoiceGetResult)
    {
      $this->WEBCentralizedShoppingCartMembershipOpenInvoiceGetResult = $WEBCentralizedShoppingCartMembershipOpenInvoiceGetResult;
      return $this;
    }

}
