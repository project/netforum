<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetPaymentOptionsResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetPaymentOptionsResult $WEBCentralizedShoppingCartGetPaymentOptionsResult
     */
    protected $WEBCentralizedShoppingCartGetPaymentOptionsResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetPaymentOptionsResult $WEBCentralizedShoppingCartGetPaymentOptionsResult
     */
    public function __construct($WEBCentralizedShoppingCartGetPaymentOptionsResult)
    {
      $this->WEBCentralizedShoppingCartGetPaymentOptionsResult = $WEBCentralizedShoppingCartGetPaymentOptionsResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetPaymentOptionsResult
     */
    public function getWEBCentralizedShoppingCartGetPaymentOptionsResult()
    {
      return $this->WEBCentralizedShoppingCartGetPaymentOptionsResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetPaymentOptionsResult $WEBCentralizedShoppingCartGetPaymentOptionsResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetPaymentOptionsResponse
     */
    public function setWEBCentralizedShoppingCartGetPaymentOptionsResult($WEBCentralizedShoppingCartGetPaymentOptionsResult)
    {
      $this->WEBCentralizedShoppingCartGetPaymentOptionsResult = $WEBCentralizedShoppingCartGetPaymentOptionsResult;
      return $this;
    }

}
