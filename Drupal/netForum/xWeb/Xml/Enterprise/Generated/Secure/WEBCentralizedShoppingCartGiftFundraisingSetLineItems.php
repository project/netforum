<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGiftFundraisingSetLineItems
{

    /**
     * @var FundraisingGiftType $oFundraisingGift
     */
    protected $oFundraisingGift = null;

    /**
     * @var Fees $oFeeCollection
     */
    protected $oFeeCollection = null;

    /**
     * @param FundraisingGiftType $oFundraisingGift
     * @param Fees $oFeeCollection
     */
    public function __construct($oFundraisingGift, $oFeeCollection)
    {
      $this->oFundraisingGift = $oFundraisingGift;
      $this->oFeeCollection = $oFeeCollection;
    }

    /**
     * @return FundraisingGiftType
     */
    public function getOFundraisingGift()
    {
      return $this->oFundraisingGift;
    }

    /**
     * @param FundraisingGiftType $oFundraisingGift
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGiftFundraisingSetLineItems
     */
    public function setOFundraisingGift($oFundraisingGift)
    {
      $this->oFundraisingGift = $oFundraisingGift;
      return $this;
    }

    /**
     * @return Fees
     */
    public function getOFeeCollection()
    {
      return $this->oFeeCollection;
    }

    /**
     * @param Fees $oFeeCollection
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGiftFundraisingSetLineItems
     */
    public function setOFeeCollection($oFeeCollection)
    {
      $this->oFeeCollection = $oFeeCollection;
      return $this;
    }

}
