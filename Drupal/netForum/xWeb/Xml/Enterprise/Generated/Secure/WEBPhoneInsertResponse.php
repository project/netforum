<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBPhoneInsertResponse
{

    /**
     * @var CustomerPhoneType $WEBPhoneInsertResult
     */
    protected $WEBPhoneInsertResult = null;

    /**
     * @param CustomerPhoneType $WEBPhoneInsertResult
     */
    public function __construct($WEBPhoneInsertResult)
    {
      $this->WEBPhoneInsertResult = $WEBPhoneInsertResult;
    }

    /**
     * @return CustomerPhoneType
     */
    public function getWEBPhoneInsertResult()
    {
      return $this->WEBPhoneInsertResult;
    }

    /**
     * @param CustomerPhoneType $WEBPhoneInsertResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBPhoneInsertResponse
     */
    public function setWEBPhoneInsertResult($WEBPhoneInsertResult)
    {
      $this->WEBPhoneInsertResult = $WEBPhoneInsertResult;
      return $this;
    }

}
