<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartMembershipGetRenewalPackageListResponse
{

    /**
     * @var WEBCentralizedShoppingCartMembershipGetRenewalPackageListResult $WEBCentralizedShoppingCartMembershipGetRenewalPackageListResult
     */
    protected $WEBCentralizedShoppingCartMembershipGetRenewalPackageListResult = null;

    /**
     * @param WEBCentralizedShoppingCartMembershipGetRenewalPackageListResult $WEBCentralizedShoppingCartMembershipGetRenewalPackageListResult
     */
    public function __construct($WEBCentralizedShoppingCartMembershipGetRenewalPackageListResult)
    {
      $this->WEBCentralizedShoppingCartMembershipGetRenewalPackageListResult = $WEBCentralizedShoppingCartMembershipGetRenewalPackageListResult;
    }

    /**
     * @return WEBCentralizedShoppingCartMembershipGetRenewalPackageListResult
     */
    public function getWEBCentralizedShoppingCartMembershipGetRenewalPackageListResult()
    {
      return $this->WEBCentralizedShoppingCartMembershipGetRenewalPackageListResult;
    }

    /**
     * @param WEBCentralizedShoppingCartMembershipGetRenewalPackageListResult $WEBCentralizedShoppingCartMembershipGetRenewalPackageListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartMembershipGetRenewalPackageListResponse
     */
    public function setWEBCentralizedShoppingCartMembershipGetRenewalPackageListResult($WEBCentralizedShoppingCartMembershipGetRenewalPackageListResult)
    {
      $this->WEBCentralizedShoppingCartMembershipGetRenewalPackageListResult = $WEBCentralizedShoppingCartMembershipGetRenewalPackageListResult;
      return $this;
    }

}
