<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class InsertCustomerActionResponse
{

    /**
     * @var guid $InsertCustomerActionResult
     */
    protected $InsertCustomerActionResult = null;

    /**
     * @param guid $InsertCustomerActionResult
     */
    public function __construct($InsertCustomerActionResult)
    {
      $this->InsertCustomerActionResult = $InsertCustomerActionResult;
    }

    /**
     * @return guid
     */
    public function getInsertCustomerActionResult()
    {
      return $this->InsertCustomerActionResult;
    }

    /**
     * @param guid $InsertCustomerActionResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InsertCustomerActionResponse
     */
    public function setInsertCustomerActionResult($InsertCustomerActionResult)
    {
      $this->InsertCustomerActionResult = $InsertCustomerActionResult;
      return $this;
    }

}
