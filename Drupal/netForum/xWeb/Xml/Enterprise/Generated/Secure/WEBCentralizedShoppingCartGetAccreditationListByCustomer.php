<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetAccreditationListByCustomer
{

    /**
     * @var guid $OrganizationCustomerKey
     */
    protected $OrganizationCustomerKey = null;

    /**
     * @param guid $OrganizationCustomerKey
     */
    public function __construct($OrganizationCustomerKey)
    {
      $this->OrganizationCustomerKey = $OrganizationCustomerKey;
    }

    /**
     * @return guid
     */
    public function getOrganizationCustomerKey()
    {
      return $this->OrganizationCustomerKey;
    }

    /**
     * @param guid $OrganizationCustomerKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetAccreditationListByCustomer
     */
    public function setOrganizationCustomerKey($OrganizationCustomerKey)
    {
      $this->OrganizationCustomerKey = $OrganizationCustomerKey;
      return $this;
    }

}
