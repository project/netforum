<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGiftAddGiftFundraisingResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartGiftAddGiftFundraisingResult
     */
    protected $WEBCentralizedShoppingCartGiftAddGiftFundraisingResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartGiftAddGiftFundraisingResult
     */
    public function __construct($WEBCentralizedShoppingCartGiftAddGiftFundraisingResult)
    {
      $this->WEBCentralizedShoppingCartGiftAddGiftFundraisingResult = $WEBCentralizedShoppingCartGiftAddGiftFundraisingResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartGiftAddGiftFundraisingResult()
    {
      return $this->WEBCentralizedShoppingCartGiftAddGiftFundraisingResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartGiftAddGiftFundraisingResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGiftAddGiftFundraisingResponse
     */
    public function setWEBCentralizedShoppingCartGiftAddGiftFundraisingResult($WEBCentralizedShoppingCartGiftAddGiftFundraisingResult)
    {
      $this->WEBCentralizedShoppingCartGiftAddGiftFundraisingResult = $WEBCentralizedShoppingCartGiftAddGiftFundraisingResult;
      return $this;
    }

}
