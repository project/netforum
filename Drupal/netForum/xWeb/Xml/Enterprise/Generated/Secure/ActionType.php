<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class ActionType
{
    const __default = 'SAVE';
    const SAVE = 'SAVE';
    const DELETE = 'DELETE';
    const NONE = 'NONE';


}
