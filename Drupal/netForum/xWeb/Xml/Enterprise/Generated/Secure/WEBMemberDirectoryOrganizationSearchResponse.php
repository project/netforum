<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBMemberDirectoryOrganizationSearchResponse
{

    /**
     * @var WEBMemberDirectoryOrganizationSearchResult $WEBMemberDirectoryOrganizationSearchResult
     */
    protected $WEBMemberDirectoryOrganizationSearchResult = null;

    /**
     * @param WEBMemberDirectoryOrganizationSearchResult $WEBMemberDirectoryOrganizationSearchResult
     */
    public function __construct($WEBMemberDirectoryOrganizationSearchResult)
    {
      $this->WEBMemberDirectoryOrganizationSearchResult = $WEBMemberDirectoryOrganizationSearchResult;
    }

    /**
     * @return WEBMemberDirectoryOrganizationSearchResult
     */
    public function getWEBMemberDirectoryOrganizationSearchResult()
    {
      return $this->WEBMemberDirectoryOrganizationSearchResult;
    }

    /**
     * @param WEBMemberDirectoryOrganizationSearchResult $WEBMemberDirectoryOrganizationSearchResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBMemberDirectoryOrganizationSearchResponse
     */
    public function setWEBMemberDirectoryOrganizationSearchResult($WEBMemberDirectoryOrganizationSearchResult)
    {
      $this->WEBMemberDirectoryOrganizationSearchResult = $WEBMemberDirectoryOrganizationSearchResult;
      return $this;
    }

}
