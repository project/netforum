<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class FundraisingGift_rgs_reg_key_Query_DataObjectType
{

    /**
     * @var av_key_Type $eab_rgs_key
     */
    protected $eab_rgs_key = null;

    /**
     * @var av_key_Type $eab_rgs_reg_key
     */
    protected $eab_rgs_reg_key = null;

    /**
     * @var av_key_Type $eab_rgs_gft_key
     */
    protected $eab_rgs_gft_key = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getEab_rgs_key()
    {
      return $this->eab_rgs_key;
    }

    /**
     * @param av_key_Type $eab_rgs_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\FundraisingGift_rgs_reg_key_Query_DataObjectType
     */
    public function setEab_rgs_key($eab_rgs_key)
    {
      $this->eab_rgs_key = $eab_rgs_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getEab_rgs_reg_key()
    {
      return $this->eab_rgs_reg_key;
    }

    /**
     * @param av_key_Type $eab_rgs_reg_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\FundraisingGift_rgs_reg_key_Query_DataObjectType
     */
    public function setEab_rgs_reg_key($eab_rgs_reg_key)
    {
      $this->eab_rgs_reg_key = $eab_rgs_reg_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getEab_rgs_gft_key()
    {
      return $this->eab_rgs_gft_key;
    }

    /**
     * @param av_key_Type $eab_rgs_gft_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\FundraisingGift_rgs_reg_key_Query_DataObjectType
     */
    public function setEab_rgs_gft_key($eab_rgs_gft_key)
    {
      $this->eab_rgs_gft_key = $eab_rgs_gft_key;
      return $this;
    }

}
