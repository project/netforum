<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBIndividualUpdate
{

    /**
     * @var IndividualType $oFacadeObject
     */
    protected $oFacadeObject = null;

    /**
     * @var string $bRefresh
     */
    protected $bRefresh = null;

    /**
     * @param IndividualType $oFacadeObject
     * @param string $bRefresh
     */
    public function __construct($oFacadeObject, $bRefresh)
    {
      $this->oFacadeObject = $oFacadeObject;
      $this->bRefresh = $bRefresh;
    }

    /**
     * @return IndividualType
     */
    public function getOFacadeObject()
    {
      return $this->oFacadeObject;
    }

    /**
     * @param IndividualType $oFacadeObject
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBIndividualUpdate
     */
    public function setOFacadeObject($oFacadeObject)
    {
      $this->oFacadeObject = $oFacadeObject;
      return $this;
    }

    /**
     * @return string
     */
    public function getBRefresh()
    {
      return $this->bRefresh;
    }

    /**
     * @param string $bRefresh
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBIndividualUpdate
     */
    public function setBRefresh($bRefresh)
    {
      $this->bRefresh = $bRefresh;
      return $this;
    }

}
