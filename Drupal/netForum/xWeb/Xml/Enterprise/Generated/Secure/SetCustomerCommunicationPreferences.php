<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class SetCustomerCommunicationPreferences
{

    /**
     * @var string $customerKey
     */
    protected $customerKey = null;

    /**
     * @var ArrayOfMailingListSetting $preferences
     */
    protected $preferences = null;

    /**
     * @param string $customerKey
     * @param ArrayOfMailingListSetting $preferences
     */
    public function __construct($customerKey, $preferences)
    {
      $this->customerKey = $customerKey;
      $this->preferences = $preferences;
    }

    /**
     * @return string
     */
    public function getCustomerKey()
    {
      return $this->customerKey;
    }

    /**
     * @param string $customerKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\SetCustomerCommunicationPreferences
     */
    public function setCustomerKey($customerKey)
    {
      $this->customerKey = $customerKey;
      return $this;
    }

    /**
     * @return ArrayOfMailingListSetting
     */
    public function getPreferences()
    {
      return $this->preferences;
    }

    /**
     * @param ArrayOfMailingListSetting $preferences
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\SetCustomerCommunicationPreferences
     */
    public function setPreferences($preferences)
    {
      $this->preferences = $preferences;
      return $this;
    }

}
