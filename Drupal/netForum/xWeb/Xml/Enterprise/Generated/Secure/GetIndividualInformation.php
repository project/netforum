<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetIndividualInformation
{

    /**
     * @var string $IndividualKey
     */
    protected $IndividualKey = null;

    /**
     * @param string $IndividualKey
     */
    public function __construct($IndividualKey)
    {
      $this->IndividualKey = $IndividualKey;
    }

    /**
     * @return string
     */
    public function getIndividualKey()
    {
      return $this->IndividualKey;
    }

    /**
     * @param string $IndividualKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetIndividualInformation
     */
    public function setIndividualKey($IndividualKey)
    {
      $this->IndividualKey = $IndividualKey;
      return $this;
    }

}
