<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBActivityGetRegistrantGuestsResponse
{

    /**
     * @var WEBActivityGetRegistrantGuestsResult $WEBActivityGetRegistrantGuestsResult
     */
    protected $WEBActivityGetRegistrantGuestsResult = null;

    /**
     * @param WEBActivityGetRegistrantGuestsResult $WEBActivityGetRegistrantGuestsResult
     */
    public function __construct($WEBActivityGetRegistrantGuestsResult)
    {
      $this->WEBActivityGetRegistrantGuestsResult = $WEBActivityGetRegistrantGuestsResult;
    }

    /**
     * @return WEBActivityGetRegistrantGuestsResult
     */
    public function getWEBActivityGetRegistrantGuestsResult()
    {
      return $this->WEBActivityGetRegistrantGuestsResult;
    }

    /**
     * @param WEBActivityGetRegistrantGuestsResult $WEBActivityGetRegistrantGuestsResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBActivityGetRegistrantGuestsResponse
     */
    public function setWEBActivityGetRegistrantGuestsResult($WEBActivityGetRegistrantGuestsResult)
    {
      $this->WEBActivityGetRegistrantGuestsResult = $WEBActivityGetRegistrantGuestsResult;
      return $this;
    }

}
