<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCommitteeNominationInsertResponse
{

    /**
     * @var CommitteeNominationsType $WEBCommitteeNominationInsertResult
     */
    protected $WEBCommitteeNominationInsertResult = null;

    /**
     * @param CommitteeNominationsType $WEBCommitteeNominationInsertResult
     */
    public function __construct($WEBCommitteeNominationInsertResult)
    {
      $this->WEBCommitteeNominationInsertResult = $WEBCommitteeNominationInsertResult;
    }

    /**
     * @return CommitteeNominationsType
     */
    public function getWEBCommitteeNominationInsertResult()
    {
      return $this->WEBCommitteeNominationInsertResult;
    }

    /**
     * @param CommitteeNominationsType $WEBCommitteeNominationInsertResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCommitteeNominationInsertResponse
     */
    public function setWEBCommitteeNominationInsertResult($WEBCommitteeNominationInsertResult)
    {
      $this->WEBCommitteeNominationInsertResult = $WEBCommitteeNominationInsertResult;
      return $this;
    }

}
