<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetFundraisingEventDonationUrlResponse
{

    /**
     * @var string $GetFundraisingEventDonationUrlResult
     */
    protected $GetFundraisingEventDonationUrlResult = null;

    /**
     * @param string $GetFundraisingEventDonationUrlResult
     */
    public function __construct($GetFundraisingEventDonationUrlResult)
    {
      $this->GetFundraisingEventDonationUrlResult = $GetFundraisingEventDonationUrlResult;
    }

    /**
     * @return string
     */
    public function getGetFundraisingEventDonationUrlResult()
    {
      return $this->GetFundraisingEventDonationUrlResult;
    }

    /**
     * @param string $GetFundraisingEventDonationUrlResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetFundraisingEventDonationUrlResponse
     */
    public function setGetFundraisingEventDonationUrlResult($GetFundraisingEventDonationUrlResult)
    {
      $this->GetFundraisingEventDonationUrlResult = $GetFundraisingEventDonationUrlResult;
      return $this;
    }

}
