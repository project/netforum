<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartAddEventRegistrantGroupResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartAddEventRegistrantGroupResult
     */
    protected $WEBCentralizedShoppingCartAddEventRegistrantGroupResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartAddEventRegistrantGroupResult
     */
    public function __construct($WEBCentralizedShoppingCartAddEventRegistrantGroupResult)
    {
      $this->WEBCentralizedShoppingCartAddEventRegistrantGroupResult = $WEBCentralizedShoppingCartAddEventRegistrantGroupResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartAddEventRegistrantGroupResult()
    {
      return $this->WEBCentralizedShoppingCartAddEventRegistrantGroupResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartAddEventRegistrantGroupResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartAddEventRegistrantGroupResponse
     */
    public function setWEBCentralizedShoppingCartAddEventRegistrantGroupResult($WEBCentralizedShoppingCartAddEventRegistrantGroupResult)
    {
      $this->WEBCentralizedShoppingCartAddEventRegistrantGroupResult = $WEBCentralizedShoppingCartAddEventRegistrantGroupResult;
      return $this;
    }

}
