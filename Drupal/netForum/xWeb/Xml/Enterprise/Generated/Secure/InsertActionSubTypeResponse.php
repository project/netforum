<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class InsertActionSubTypeResponse
{

    /**
     * @var guid $InsertActionSubTypeResult
     */
    protected $InsertActionSubTypeResult = null;

    /**
     * @param guid $InsertActionSubTypeResult
     */
    public function __construct($InsertActionSubTypeResult)
    {
      $this->InsertActionSubTypeResult = $InsertActionSubTypeResult;
    }

    /**
     * @return guid
     */
    public function getInsertActionSubTypeResult()
    {
      return $this->InsertActionSubTypeResult;
    }

    /**
     * @param guid $InsertActionSubTypeResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\InsertActionSubTypeResponse
     */
    public function setInsertActionSubTypeResult($InsertActionSubTypeResult)
    {
      $this->InsertActionSubTypeResult = $InsertActionSubTypeResult;
      return $this;
    }

}
