<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGiftGetGiftProductListByNameResponse
{

    /**
     * @var WEBCentralizedShoppingCartGiftGetGiftProductListByNameResult $WEBCentralizedShoppingCartGiftGetGiftProductListByNameResult
     */
    protected $WEBCentralizedShoppingCartGiftGetGiftProductListByNameResult = null;

    /**
     * @param WEBCentralizedShoppingCartGiftGetGiftProductListByNameResult $WEBCentralizedShoppingCartGiftGetGiftProductListByNameResult
     */
    public function __construct($WEBCentralizedShoppingCartGiftGetGiftProductListByNameResult)
    {
      $this->WEBCentralizedShoppingCartGiftGetGiftProductListByNameResult = $WEBCentralizedShoppingCartGiftGetGiftProductListByNameResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGiftGetGiftProductListByNameResult
     */
    public function getWEBCentralizedShoppingCartGiftGetGiftProductListByNameResult()
    {
      return $this->WEBCentralizedShoppingCartGiftGetGiftProductListByNameResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGiftGetGiftProductListByNameResult $WEBCentralizedShoppingCartGiftGetGiftProductListByNameResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGiftGetGiftProductListByNameResponse
     */
    public function setWEBCentralizedShoppingCartGiftGetGiftProductListByNameResult($WEBCentralizedShoppingCartGiftGetGiftProductListByNameResult)
    {
      $this->WEBCentralizedShoppingCartGiftGetGiftProductListByNameResult = $WEBCentralizedShoppingCartGiftGetGiftProductListByNameResult;
      return $this;
    }

}
