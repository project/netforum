<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartLoadLineItemResponse
{

    /**
     * @var InvoiceDetailType $WEBCentralizedShoppingCartLoadLineItemResult
     */
    protected $WEBCentralizedShoppingCartLoadLineItemResult = null;

    /**
     * @param InvoiceDetailType $WEBCentralizedShoppingCartLoadLineItemResult
     */
    public function __construct($WEBCentralizedShoppingCartLoadLineItemResult)
    {
      $this->WEBCentralizedShoppingCartLoadLineItemResult = $WEBCentralizedShoppingCartLoadLineItemResult;
    }

    /**
     * @return InvoiceDetailType
     */
    public function getWEBCentralizedShoppingCartLoadLineItemResult()
    {
      return $this->WEBCentralizedShoppingCartLoadLineItemResult;
    }

    /**
     * @param InvoiceDetailType $WEBCentralizedShoppingCartLoadLineItemResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartLoadLineItemResponse
     */
    public function setWEBCentralizedShoppingCartLoadLineItemResult($WEBCentralizedShoppingCartLoadLineItemResult)
    {
      $this->WEBCentralizedShoppingCartLoadLineItemResult = $WEBCentralizedShoppingCartLoadLineItemResult;
      return $this;
    }

}
