<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResult $WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResult
     */
    protected $WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResult $WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResult
     */
    public function __construct($WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResult)
    {
      $this->WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResult = $WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResult
     */
    public function getWEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResult()
    {
      return $this->WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResult $WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResponse
     */
    public function setWEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResult($WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResult)
    {
      $this->WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResult = $WEBCentralizedShoppingCartGetEventRegistrantTypeListByEventResult;
      return $this;
    }

}
