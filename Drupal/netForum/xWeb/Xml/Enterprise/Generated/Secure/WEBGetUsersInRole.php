<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBGetUsersInRole
{

    /**
     * @var string $roleName
     */
    protected $roleName = null;

    /**
     * @param string $roleName
     */
    public function __construct($roleName)
    {
      $this->roleName = $roleName;
    }

    /**
     * @return string
     */
    public function getRoleName()
    {
      return $this->roleName;
    }

    /**
     * @param string $roleName
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBGetUsersInRole
     */
    public function setRoleName($roleName)
    {
      $this->roleName = $roleName;
      return $this;
    }

}
