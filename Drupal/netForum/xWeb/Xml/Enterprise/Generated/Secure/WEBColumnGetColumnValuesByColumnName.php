<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBColumnGetColumnValuesByColumnName
{

    /**
     * @var string $ColumnName
     */
    protected $ColumnName = null;

    /**
     * @param string $ColumnName
     */
    public function __construct($ColumnName)
    {
      $this->ColumnName = $ColumnName;
    }

    /**
     * @return string
     */
    public function getColumnName()
    {
      return $this->ColumnName;
    }

    /**
     * @param string $ColumnName
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBColumnGetColumnValuesByColumnName
     */
    public function setColumnName($ColumnName)
    {
      $this->ColumnName = $ColumnName;
      return $this;
    }

}
