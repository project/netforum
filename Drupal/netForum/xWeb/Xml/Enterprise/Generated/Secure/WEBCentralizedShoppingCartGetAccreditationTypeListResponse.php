<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetAccreditationTypeListResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetAccreditationTypeListResult $WEBCentralizedShoppingCartGetAccreditationTypeListResult
     */
    protected $WEBCentralizedShoppingCartGetAccreditationTypeListResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetAccreditationTypeListResult $WEBCentralizedShoppingCartGetAccreditationTypeListResult
     */
    public function __construct($WEBCentralizedShoppingCartGetAccreditationTypeListResult)
    {
      $this->WEBCentralizedShoppingCartGetAccreditationTypeListResult = $WEBCentralizedShoppingCartGetAccreditationTypeListResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetAccreditationTypeListResult
     */
    public function getWEBCentralizedShoppingCartGetAccreditationTypeListResult()
    {
      return $this->WEBCentralizedShoppingCartGetAccreditationTypeListResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetAccreditationTypeListResult $WEBCentralizedShoppingCartGetAccreditationTypeListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetAccreditationTypeListResponse
     */
    public function setWEBCentralizedShoppingCartGetAccreditationTypeListResult($WEBCentralizedShoppingCartGetAccreditationTypeListResult)
    {
      $this->WEBCentralizedShoppingCartGetAccreditationTypeListResult = $WEBCentralizedShoppingCartGetAccreditationTypeListResult;
      return $this;
    }

}
