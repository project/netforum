<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBFaxUpdateResponse
{

    /**
     * @var boolean $WEBFaxUpdateResult
     */
    protected $WEBFaxUpdateResult = null;

    /**
     * @param boolean $WEBFaxUpdateResult
     */
    public function __construct($WEBFaxUpdateResult)
    {
      $this->WEBFaxUpdateResult = $WEBFaxUpdateResult;
    }

    /**
     * @return boolean
     */
    public function getWEBFaxUpdateResult()
    {
      return $this->WEBFaxUpdateResult;
    }

    /**
     * @param boolean $WEBFaxUpdateResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBFaxUpdateResponse
     */
    public function setWEBFaxUpdateResult($WEBFaxUpdateResult)
    {
      $this->WEBFaxUpdateResult = $WEBFaxUpdateResult;
      return $this;
    }

}
