<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetTimeZonesResponse
{

    /**
     * @var GetTimeZonesResult $GetTimeZonesResult
     */
    protected $GetTimeZonesResult = null;

    /**
     * @param GetTimeZonesResult $GetTimeZonesResult
     */
    public function __construct($GetTimeZonesResult)
    {
      $this->GetTimeZonesResult = $GetTimeZonesResult;
    }

    /**
     * @return GetTimeZonesResult
     */
    public function getGetTimeZonesResult()
    {
      return $this->GetTimeZonesResult;
    }

    /**
     * @param GetTimeZonesResult $GetTimeZonesResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetTimeZonesResponse
     */
    public function setGetTimeZonesResult($GetTimeZonesResult)
    {
      $this->GetTimeZonesResult = $GetTimeZonesResult;
      return $this;
    }

}
