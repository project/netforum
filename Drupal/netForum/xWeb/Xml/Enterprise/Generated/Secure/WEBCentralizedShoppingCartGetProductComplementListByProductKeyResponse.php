<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetProductComplementListByProductKeyResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetProductComplementListByProductKeyResult $WEBCentralizedShoppingCartGetProductComplementListByProductKeyResult
     */
    protected $WEBCentralizedShoppingCartGetProductComplementListByProductKeyResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetProductComplementListByProductKeyResult $WEBCentralizedShoppingCartGetProductComplementListByProductKeyResult
     */
    public function __construct($WEBCentralizedShoppingCartGetProductComplementListByProductKeyResult)
    {
      $this->WEBCentralizedShoppingCartGetProductComplementListByProductKeyResult = $WEBCentralizedShoppingCartGetProductComplementListByProductKeyResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetProductComplementListByProductKeyResult
     */
    public function getWEBCentralizedShoppingCartGetProductComplementListByProductKeyResult()
    {
      return $this->WEBCentralizedShoppingCartGetProductComplementListByProductKeyResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetProductComplementListByProductKeyResult $WEBCentralizedShoppingCartGetProductComplementListByProductKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetProductComplementListByProductKeyResponse
     */
    public function setWEBCentralizedShoppingCartGetProductComplementListByProductKeyResult($WEBCentralizedShoppingCartGetProductComplementListByProductKeyResult)
    {
      $this->WEBCentralizedShoppingCartGetProductComplementListByProductKeyResult = $WEBCentralizedShoppingCartGetProductComplementListByProductKeyResult;
      return $this;
    }

}
