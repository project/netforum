<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetDynamicQueryResponse
{

    /**
     * @var GetDynamicQueryResult $GetDynamicQueryResult
     */
    protected $GetDynamicQueryResult = null;

    /**
     * @param GetDynamicQueryResult $GetDynamicQueryResult
     */
    public function __construct($GetDynamicQueryResult)
    {
      $this->GetDynamicQueryResult = $GetDynamicQueryResult;
    }

    /**
     * @return GetDynamicQueryResult
     */
    public function getGetDynamicQueryResult()
    {
      return $this->GetDynamicQueryResult;
    }

    /**
     * @param GetDynamicQueryResult $GetDynamicQueryResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetDynamicQueryResponse
     */
    public function setGetDynamicQueryResult($GetDynamicQueryResult)
    {
      $this->GetDynamicQueryResult = $GetDynamicQueryResult;
      return $this;
    }

}
