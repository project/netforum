<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetEventListByNameResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetEventListByNameResult $WEBCentralizedShoppingCartGetEventListByNameResult
     */
    protected $WEBCentralizedShoppingCartGetEventListByNameResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetEventListByNameResult $WEBCentralizedShoppingCartGetEventListByNameResult
     */
    public function __construct($WEBCentralizedShoppingCartGetEventListByNameResult)
    {
      $this->WEBCentralizedShoppingCartGetEventListByNameResult = $WEBCentralizedShoppingCartGetEventListByNameResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetEventListByNameResult
     */
    public function getWEBCentralizedShoppingCartGetEventListByNameResult()
    {
      return $this->WEBCentralizedShoppingCartGetEventListByNameResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetEventListByNameResult $WEBCentralizedShoppingCartGetEventListByNameResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetEventListByNameResponse
     */
    public function setWEBCentralizedShoppingCartGetEventListByNameResult($WEBCentralizedShoppingCartGetEventListByNameResult)
    {
      $this->WEBCentralizedShoppingCartGetEventListByNameResult = $WEBCentralizedShoppingCartGetEventListByNameResult;
      return $this;
    }

}
