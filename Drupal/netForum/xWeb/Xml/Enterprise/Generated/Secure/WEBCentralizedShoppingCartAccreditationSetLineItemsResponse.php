<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartAccreditationSetLineItemsResponse
{

    /**
     * @var AccreditationAreaType $WEBCentralizedShoppingCartAccreditationSetLineItemsResult
     */
    protected $WEBCentralizedShoppingCartAccreditationSetLineItemsResult = null;

    /**
     * @param AccreditationAreaType $WEBCentralizedShoppingCartAccreditationSetLineItemsResult
     */
    public function __construct($WEBCentralizedShoppingCartAccreditationSetLineItemsResult)
    {
      $this->WEBCentralizedShoppingCartAccreditationSetLineItemsResult = $WEBCentralizedShoppingCartAccreditationSetLineItemsResult;
    }

    /**
     * @return AccreditationAreaType
     */
    public function getWEBCentralizedShoppingCartAccreditationSetLineItemsResult()
    {
      return $this->WEBCentralizedShoppingCartAccreditationSetLineItemsResult;
    }

    /**
     * @param AccreditationAreaType $WEBCentralizedShoppingCartAccreditationSetLineItemsResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartAccreditationSetLineItemsResponse
     */
    public function setWEBCentralizedShoppingCartAccreditationSetLineItemsResult($WEBCentralizedShoppingCartAccreditationSetLineItemsResult)
    {
      $this->WEBCentralizedShoppingCartAccreditationSetLineItemsResult = $WEBCentralizedShoppingCartAccreditationSetLineItemsResult;
      return $this;
    }

}
