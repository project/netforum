<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class AccreditationArea_Accreditation_Customer_DataObjectType
{

    /**
     * @var av_key_Type $amc_key
     */
    protected $amc_key = null;

    /**
     * @var av_key_Type $amc_cst_key
     */
    protected $amc_cst_key = null;

    /**
     * @var av_date_Type $amc_start_date
     */
    protected $amc_start_date = null;

    /**
     * @var av_date_Type $amc_end_date
     */
    protected $amc_end_date = null;

    /**
     * @var av_user_Type $amc_add_user
     */
    protected $amc_add_user = null;

    /**
     * @var av_date_Type $amc_add_date
     */
    protected $amc_add_date = null;

    /**
     * @var av_user_Type $amc_change_user
     */
    protected $amc_change_user = null;

    /**
     * @var av_date_Type $amc_change_date
     */
    protected $amc_change_date = null;

    /**
     * @var av_delete_flag_Type $amc_delete_flag
     */
    protected $amc_delete_flag = null;

    /**
     * @var av_key_Type $amc_key_ext
     */
    protected $amc_key_ext = null;

    /**
     * @var av_key_Type $amc_ixo_cao_key
     */
    protected $amc_ixo_cao_key = null;

    /**
     * @var av_key_Type $amc_ama_key
     */
    protected $amc_ama_key = null;

    /**
     * @var stringLength50_Type $amc_inv_code
     */
    protected $amc_inv_code = null;

    /**
     * @var av_key_Type $amc_ixo_ceo_key
     */
    protected $amc_ixo_ceo_key = null;

    /**
     * @var av_key_Type $amc_ams_key
     */
    protected $amc_ams_key = null;

    /**
     * @var av_key_Type $amc_ixo_cmo_key
     */
    protected $amc_ixo_cmo_key = null;

    /**
     * @var av_key_Type $amc_ixo_adl_key
     */
    protected $amc_ixo_adl_key = null;

    /**
     * @var av_date_Type $amc_renewal_letter_date
     */
    protected $amc_renewal_letter_date = null;

    /**
     * @var av_key_Type $amc_amt_key
     */
    protected $amc_amt_key = null;

    /**
     * @var av_date_Type $amc_decision_date
     */
    protected $amc_decision_date = null;

    /**
     * @var av_integer_Type $amc_renewal_letter_sent
     */
    protected $amc_renewal_letter_sent = null;

    /**
     * @var stringLength40_Type $amc_dupe_check
     */
    protected $amc_dupe_check = null;

    /**
     * @var av_key_Type $amc_entity_key
     */
    protected $amc_entity_key = null;

    /**
     * @var av_key_Type $amc_ivd_key
     */
    protected $amc_ivd_key = null;

    /**
     * @var av_key_Type $amc_ixo_surveycontact_key
     */
    protected $amc_ixo_surveycontact_key = null;

    /**
     * @var av_date_Type $amc_status_date
     */
    protected $amc_status_date = null;

    /**
     * @var stringLength50_Type $amc_ord_code
     */
    protected $amc_ord_code = null;

    /**
     * @var av_key_Type $amc_odd_key
     */
    protected $amc_odd_key = null;

    /**
     * @var av_key_Type $amc_bup_prd_key
     */
    protected $amc_bup_prd_key = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getAmc_key()
    {
      return $this->amc_key;
    }

    /**
     * @param av_key_Type $amc_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_key($amc_key)
    {
      $this->amc_key = $amc_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAmc_cst_key()
    {
      return $this->amc_cst_key;
    }

    /**
     * @param av_key_Type $amc_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_cst_key($amc_cst_key)
    {
      $this->amc_cst_key = $amc_cst_key;
      return $this;
    }

    /**
     * @return av_date_Type
     */
    public function getAmc_start_date()
    {
      return $this->amc_start_date;
    }

    /**
     * @param av_date_Type $amc_start_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_start_date($amc_start_date)
    {
      $this->amc_start_date = $amc_start_date;
      return $this;
    }

    /**
     * @return av_date_Type
     */
    public function getAmc_end_date()
    {
      return $this->amc_end_date;
    }

    /**
     * @param av_date_Type $amc_end_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_end_date($amc_end_date)
    {
      $this->amc_end_date = $amc_end_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getAmc_add_user()
    {
      return $this->amc_add_user;
    }

    /**
     * @param av_user_Type $amc_add_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_add_user($amc_add_user)
    {
      $this->amc_add_user = $amc_add_user;
      return $this;
    }

    /**
     * @return av_date_Type
     */
    public function getAmc_add_date()
    {
      return $this->amc_add_date;
    }

    /**
     * @param av_date_Type $amc_add_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_add_date($amc_add_date)
    {
      $this->amc_add_date = $amc_add_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getAmc_change_user()
    {
      return $this->amc_change_user;
    }

    /**
     * @param av_user_Type $amc_change_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_change_user($amc_change_user)
    {
      $this->amc_change_user = $amc_change_user;
      return $this;
    }

    /**
     * @return av_date_Type
     */
    public function getAmc_change_date()
    {
      return $this->amc_change_date;
    }

    /**
     * @param av_date_Type $amc_change_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_change_date($amc_change_date)
    {
      $this->amc_change_date = $amc_change_date;
      return $this;
    }

    /**
     * @return av_delete_flag_Type
     */
    public function getAmc_delete_flag()
    {
      return $this->amc_delete_flag;
    }

    /**
     * @param av_delete_flag_Type $amc_delete_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_delete_flag($amc_delete_flag)
    {
      $this->amc_delete_flag = $amc_delete_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAmc_key_ext()
    {
      return $this->amc_key_ext;
    }

    /**
     * @param av_key_Type $amc_key_ext
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_key_ext($amc_key_ext)
    {
      $this->amc_key_ext = $amc_key_ext;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAmc_ixo_cao_key()
    {
      return $this->amc_ixo_cao_key;
    }

    /**
     * @param av_key_Type $amc_ixo_cao_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_ixo_cao_key($amc_ixo_cao_key)
    {
      $this->amc_ixo_cao_key = $amc_ixo_cao_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAmc_ama_key()
    {
      return $this->amc_ama_key;
    }

    /**
     * @param av_key_Type $amc_ama_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_ama_key($amc_ama_key)
    {
      $this->amc_ama_key = $amc_ama_key;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getAmc_inv_code()
    {
      return $this->amc_inv_code;
    }

    /**
     * @param stringLength50_Type $amc_inv_code
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_inv_code($amc_inv_code)
    {
      $this->amc_inv_code = $amc_inv_code;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAmc_ixo_ceo_key()
    {
      return $this->amc_ixo_ceo_key;
    }

    /**
     * @param av_key_Type $amc_ixo_ceo_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_ixo_ceo_key($amc_ixo_ceo_key)
    {
      $this->amc_ixo_ceo_key = $amc_ixo_ceo_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAmc_ams_key()
    {
      return $this->amc_ams_key;
    }

    /**
     * @param av_key_Type $amc_ams_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_ams_key($amc_ams_key)
    {
      $this->amc_ams_key = $amc_ams_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAmc_ixo_cmo_key()
    {
      return $this->amc_ixo_cmo_key;
    }

    /**
     * @param av_key_Type $amc_ixo_cmo_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_ixo_cmo_key($amc_ixo_cmo_key)
    {
      $this->amc_ixo_cmo_key = $amc_ixo_cmo_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAmc_ixo_adl_key()
    {
      return $this->amc_ixo_adl_key;
    }

    /**
     * @param av_key_Type $amc_ixo_adl_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_ixo_adl_key($amc_ixo_adl_key)
    {
      $this->amc_ixo_adl_key = $amc_ixo_adl_key;
      return $this;
    }

    /**
     * @return av_date_Type
     */
    public function getAmc_renewal_letter_date()
    {
      return $this->amc_renewal_letter_date;
    }

    /**
     * @param av_date_Type $amc_renewal_letter_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_renewal_letter_date($amc_renewal_letter_date)
    {
      $this->amc_renewal_letter_date = $amc_renewal_letter_date;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAmc_amt_key()
    {
      return $this->amc_amt_key;
    }

    /**
     * @param av_key_Type $amc_amt_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_amt_key($amc_amt_key)
    {
      $this->amc_amt_key = $amc_amt_key;
      return $this;
    }

    /**
     * @return av_date_Type
     */
    public function getAmc_decision_date()
    {
      return $this->amc_decision_date;
    }

    /**
     * @param av_date_Type $amc_decision_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_decision_date($amc_decision_date)
    {
      $this->amc_decision_date = $amc_decision_date;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getAmc_renewal_letter_sent()
    {
      return $this->amc_renewal_letter_sent;
    }

    /**
     * @param av_integer_Type $amc_renewal_letter_sent
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_renewal_letter_sent($amc_renewal_letter_sent)
    {
      $this->amc_renewal_letter_sent = $amc_renewal_letter_sent;
      return $this;
    }

    /**
     * @return stringLength40_Type
     */
    public function getAmc_dupe_check()
    {
      return $this->amc_dupe_check;
    }

    /**
     * @param stringLength40_Type $amc_dupe_check
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_dupe_check($amc_dupe_check)
    {
      $this->amc_dupe_check = $amc_dupe_check;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAmc_entity_key()
    {
      return $this->amc_entity_key;
    }

    /**
     * @param av_key_Type $amc_entity_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_entity_key($amc_entity_key)
    {
      $this->amc_entity_key = $amc_entity_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAmc_ivd_key()
    {
      return $this->amc_ivd_key;
    }

    /**
     * @param av_key_Type $amc_ivd_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_ivd_key($amc_ivd_key)
    {
      $this->amc_ivd_key = $amc_ivd_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAmc_ixo_surveycontact_key()
    {
      return $this->amc_ixo_surveycontact_key;
    }

    /**
     * @param av_key_Type $amc_ixo_surveycontact_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_ixo_surveycontact_key($amc_ixo_surveycontact_key)
    {
      $this->amc_ixo_surveycontact_key = $amc_ixo_surveycontact_key;
      return $this;
    }

    /**
     * @return av_date_Type
     */
    public function getAmc_status_date()
    {
      return $this->amc_status_date;
    }

    /**
     * @param av_date_Type $amc_status_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_status_date($amc_status_date)
    {
      $this->amc_status_date = $amc_status_date;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getAmc_ord_code()
    {
      return $this->amc_ord_code;
    }

    /**
     * @param stringLength50_Type $amc_ord_code
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_ord_code($amc_ord_code)
    {
      $this->amc_ord_code = $amc_ord_code;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAmc_odd_key()
    {
      return $this->amc_odd_key;
    }

    /**
     * @param av_key_Type $amc_odd_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_odd_key($amc_odd_key)
    {
      $this->amc_odd_key = $amc_odd_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAmc_bup_prd_key()
    {
      return $this->amc_bup_prd_key;
    }

    /**
     * @param av_key_Type $amc_bup_prd_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Customer_DataObjectType
     */
    public function setAmc_bup_prd_key($amc_bup_prd_key)
    {
      $this->amc_bup_prd_key = $amc_bup_prd_key;
      return $this;
    }

}
