<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetQueryDefinitionResponse
{

    /**
     * @var Object $GetQueryDefinitionResult
     */
    protected $GetQueryDefinitionResult = null;

    /**
     * @param Object $GetQueryDefinitionResult
     */
    public function __construct($GetQueryDefinitionResult)
    {
      $this->GetQueryDefinitionResult = $GetQueryDefinitionResult;
    }

    /**
     * @return Object
     */
    public function getGetQueryDefinitionResult()
    {
      return $this->GetQueryDefinitionResult;
    }

    /**
     * @param Object $GetQueryDefinitionResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetQueryDefinitionResponse
     */
    public function setGetQueryDefinitionResult($GetQueryDefinitionResult)
    {
      $this->GetQueryDefinitionResult = $GetQueryDefinitionResult;
      return $this;
    }

}
