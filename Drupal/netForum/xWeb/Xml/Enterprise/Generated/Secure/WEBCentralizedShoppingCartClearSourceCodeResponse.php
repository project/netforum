<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartClearSourceCodeResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartClearSourceCodeResult
     */
    protected $WEBCentralizedShoppingCartClearSourceCodeResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartClearSourceCodeResult
     */
    public function __construct($WEBCentralizedShoppingCartClearSourceCodeResult)
    {
      $this->WEBCentralizedShoppingCartClearSourceCodeResult = $WEBCentralizedShoppingCartClearSourceCodeResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartClearSourceCodeResult()
    {
      return $this->WEBCentralizedShoppingCartClearSourceCodeResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartClearSourceCodeResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartClearSourceCodeResponse
     */
    public function setWEBCentralizedShoppingCartClearSourceCodeResult($WEBCentralizedShoppingCartClearSourceCodeResult)
    {
      $this->WEBCentralizedShoppingCartClearSourceCodeResult = $WEBCentralizedShoppingCartClearSourceCodeResult;
      return $this;
    }

}
