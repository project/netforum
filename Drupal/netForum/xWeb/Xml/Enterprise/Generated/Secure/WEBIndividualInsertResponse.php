<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBIndividualInsertResponse
{

    /**
     * @var IndividualType $WEBIndividualInsertResult
     */
    protected $WEBIndividualInsertResult = null;

    /**
     * @param IndividualType $WEBIndividualInsertResult
     */
    public function __construct($WEBIndividualInsertResult)
    {
      $this->WEBIndividualInsertResult = $WEBIndividualInsertResult;
    }

    /**
     * @return IndividualType
     */
    public function getWEBIndividualInsertResult()
    {
      return $this->WEBIndividualInsertResult;
    }

    /**
     * @param IndividualType $WEBIndividualInsertResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBIndividualInsertResponse
     */
    public function setWEBIndividualInsertResult($WEBIndividualInsertResult)
    {
      $this->WEBIndividualInsertResult = $WEBIndividualInsertResult;
      return $this;
    }

}
