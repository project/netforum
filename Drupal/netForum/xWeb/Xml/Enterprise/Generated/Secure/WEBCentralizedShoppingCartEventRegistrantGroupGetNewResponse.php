<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartEventRegistrantGroupGetNewResponse
{

    /**
     * @var EventsRegistrantGroupType $WEBCentralizedShoppingCartEventRegistrantGroupGetNewResult
     */
    protected $WEBCentralizedShoppingCartEventRegistrantGroupGetNewResult = null;

    /**
     * @param EventsRegistrantGroupType $WEBCentralizedShoppingCartEventRegistrantGroupGetNewResult
     */
    public function __construct($WEBCentralizedShoppingCartEventRegistrantGroupGetNewResult)
    {
      $this->WEBCentralizedShoppingCartEventRegistrantGroupGetNewResult = $WEBCentralizedShoppingCartEventRegistrantGroupGetNewResult;
    }

    /**
     * @return EventsRegistrantGroupType
     */
    public function getWEBCentralizedShoppingCartEventRegistrantGroupGetNewResult()
    {
      return $this->WEBCentralizedShoppingCartEventRegistrantGroupGetNewResult;
    }

    /**
     * @param EventsRegistrantGroupType $WEBCentralizedShoppingCartEventRegistrantGroupGetNewResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartEventRegistrantGroupGetNewResponse
     */
    public function setWEBCentralizedShoppingCartEventRegistrantGroupGetNewResult($WEBCentralizedShoppingCartEventRegistrantGroupGetNewResult)
    {
      $this->WEBCentralizedShoppingCartEventRegistrantGroupGetNewResult = $WEBCentralizedShoppingCartEventRegistrantGroupGetNewResult;
      return $this;
    }

}
