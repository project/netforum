<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCommitteeGetDocuments
{

    /**
     * @var guid $CommitteeKey
     */
    protected $CommitteeKey = null;

    /**
     * @param guid $CommitteeKey
     */
    public function __construct($CommitteeKey)
    {
      $this->CommitteeKey = $CommitteeKey;
    }

    /**
     * @return guid
     */
    public function getCommitteeKey()
    {
      return $this->CommitteeKey;
    }

    /**
     * @param guid $CommitteeKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCommitteeGetDocuments
     */
    public function setCommitteeKey($CommitteeKey)
    {
      $this->CommitteeKey = $CommitteeKey;
      return $this;
    }

}
