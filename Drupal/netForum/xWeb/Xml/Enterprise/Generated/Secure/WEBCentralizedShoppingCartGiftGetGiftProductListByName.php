<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGiftGetGiftProductListByName
{

    /**
     * @var string $GiftName
     */
    protected $GiftName = null;

    /**
     * @param string $GiftName
     */
    public function __construct($GiftName)
    {
      $this->GiftName = $GiftName;
    }

    /**
     * @return string
     */
    public function getGiftName()
    {
      return $this->GiftName;
    }

    /**
     * @param string $GiftName
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGiftGetGiftProductListByName
     */
    public function setGiftName($GiftName)
    {
      $this->GiftName = $GiftName;
      return $this;
    }

}
