<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBUpdateSystemOptionResponse
{

    /**
     * @var boolean $WEBUpdateSystemOptionResult
     */
    protected $WEBUpdateSystemOptionResult = null;

    /**
     * @param boolean $WEBUpdateSystemOptionResult
     */
    public function __construct($WEBUpdateSystemOptionResult)
    {
      $this->WEBUpdateSystemOptionResult = $WEBUpdateSystemOptionResult;
    }

    /**
     * @return boolean
     */
    public function getWEBUpdateSystemOptionResult()
    {
      return $this->WEBUpdateSystemOptionResult;
    }

    /**
     * @param boolean $WEBUpdateSystemOptionResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBUpdateSystemOptionResponse
     */
    public function setWEBUpdateSystemOptionResult($WEBUpdateSystemOptionResult)
    {
      $this->WEBUpdateSystemOptionResult = $WEBUpdateSystemOptionResult;
      return $this;
    }

}
