<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetVersionResponse
{

    /**
     * @var xWebVersion $GetVersionResult
     */
    protected $GetVersionResult = null;

    /**
     * @param xWebVersion $GetVersionResult
     */
    public function __construct($GetVersionResult)
    {
      $this->GetVersionResult = $GetVersionResult;
    }

    /**
     * @return xWebVersion
     */
    public function getGetVersionResult()
    {
      return $this->GetVersionResult;
    }

    /**
     * @param xWebVersion $GetVersionResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetVersionResponse
     */
    public function setGetVersionResult($GetVersionResult)
    {
      $this->GetVersionResult = $GetVersionResult;
      return $this;
    }

}
