<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBAddressGetAddressesByCustomerResponse
{

    /**
     * @var WEBAddressGetAddressesByCustomerResult $WEBAddressGetAddressesByCustomerResult
     */
    protected $WEBAddressGetAddressesByCustomerResult = null;

    /**
     * @param WEBAddressGetAddressesByCustomerResult $WEBAddressGetAddressesByCustomerResult
     */
    public function __construct($WEBAddressGetAddressesByCustomerResult)
    {
      $this->WEBAddressGetAddressesByCustomerResult = $WEBAddressGetAddressesByCustomerResult;
    }

    /**
     * @return WEBAddressGetAddressesByCustomerResult
     */
    public function getWEBAddressGetAddressesByCustomerResult()
    {
      return $this->WEBAddressGetAddressesByCustomerResult;
    }

    /**
     * @param WEBAddressGetAddressesByCustomerResult $WEBAddressGetAddressesByCustomerResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBAddressGetAddressesByCustomerResponse
     */
    public function setWEBAddressGetAddressesByCustomerResult($WEBAddressGetAddressesByCustomerResult)
    {
      $this->WEBAddressGetAddressesByCustomerResult = $WEBAddressGetAddressesByCustomerResult;
      return $this;
    }

}
