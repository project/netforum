<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class AccreditationArea_Affiliated_CAO_DataObjectType
{

    /**
     * @var av_key_Type $cao__cst_key
     */
    protected $cao__cst_key = null;

    /**
     * @var stringLength20_Type $cao__cst_type
     */
    protected $cao__cst_type = null;

    /**
     * @var stringLength150_Type $cao__cst_name_cp
     */
    protected $cao__cst_name_cp = null;

    /**
     * @var stringLength150_Type $cao__cst_sort_name_dn
     */
    protected $cao__cst_sort_name_dn = null;

    /**
     * @var stringLength150_Type $cao__cst_ind_full_name_dn
     */
    protected $cao__cst_ind_full_name_dn = null;

    /**
     * @var stringLength150_Type $cao__cst_org_name_dn
     */
    protected $cao__cst_org_name_dn = null;

    /**
     * @var stringLength150_Type $cao__cst_ixo_title_dn
     */
    protected $cao__cst_ixo_title_dn = null;

    /**
     * @var stringLength10_Type $cao__cst_pref_comm_meth
     */
    protected $cao__cst_pref_comm_meth = null;

    /**
     * @var av_text_Type $cao__cst_bio
     */
    protected $cao__cst_bio = null;

    /**
     * @var av_date_small_Type $cao__cst_add_date
     */
    protected $cao__cst_add_date = null;

    /**
     * @var av_user_Type $cao__cst_add_user
     */
    protected $cao__cst_add_user = null;

    /**
     * @var av_date_small_Type $cao__cst_change_date
     */
    protected $cao__cst_change_date = null;

    /**
     * @var av_user_Type $cao__cst_change_user
     */
    protected $cao__cst_change_user = null;

    /**
     * @var av_delete_flag_Type $cao__cst_delete_flag
     */
    protected $cao__cst_delete_flag = null;

    /**
     * @var av_recno_Type $cao__cst_recno
     */
    protected $cao__cst_recno = null;

    /**
     * @var stringLength10_Type $cao__cst_id
     */
    protected $cao__cst_id = null;

    /**
     * @var av_key_Type $cao__cst_key_ext
     */
    protected $cao__cst_key_ext = null;

    /**
     * @var av_flag_Type $cao__cst_email_text_only
     */
    protected $cao__cst_email_text_only = null;

    /**
     * @var av_currency_Type $cao__cst_credit_limit
     */
    protected $cao__cst_credit_limit = null;

    /**
     * @var av_key_Type $cao__cst_src_key
     */
    protected $cao__cst_src_key = null;

    /**
     * @var stringLength50_Type $cao__cst_src_code
     */
    protected $cao__cst_src_code = null;

    /**
     * @var av_flag_Type $cao__cst_tax_exempt_flag
     */
    protected $cao__cst_tax_exempt_flag = null;

    /**
     * @var stringLength30_Type $cao__cst_tax_id
     */
    protected $cao__cst_tax_id = null;

    /**
     * @var av_key_Type $cao__cst_cxa_key
     */
    protected $cao__cst_cxa_key = null;

    /**
     * @var av_flag_Type $cao__cst_no_email_flag
     */
    protected $cao__cst_no_email_flag = null;

    /**
     * @var av_key_Type $cao__cst_cxa_billing_key
     */
    protected $cao__cst_cxa_billing_key = null;

    /**
     * @var av_email_Type $cao__cst_eml_address_dn
     */
    protected $cao__cst_eml_address_dn = null;

    /**
     * @var av_key_Type $cao__cst_eml_key
     */
    protected $cao__cst_eml_key = null;

    /**
     * @var av_flag_Type $cao__cst_no_phone_flag
     */
    protected $cao__cst_no_phone_flag = null;

    /**
     * @var stringLength55_Type $cao__cst_phn_number_complete_dn
     */
    protected $cao__cst_phn_number_complete_dn = null;

    /**
     * @var av_key_Type $cao__cst_cph_key
     */
    protected $cao__cst_cph_key = null;

    /**
     * @var av_flag_Type $cao__cst_no_fax_flag
     */
    protected $cao__cst_no_fax_flag = null;

    /**
     * @var stringLength55_Type $cao__cst_fax_number_complete_dn
     */
    protected $cao__cst_fax_number_complete_dn = null;

    /**
     * @var av_key_Type $cao__cst_cfx_key
     */
    protected $cao__cst_cfx_key = null;

    /**
     * @var av_key_Type $cao__cst_ixo_key
     */
    protected $cao__cst_ixo_key = null;

    /**
     * @var av_flag_Type $cao__cst_no_web_flag
     */
    protected $cao__cst_no_web_flag = null;

    /**
     * @var stringLength15_Type $cao__cst_oldid
     */
    protected $cao__cst_oldid = null;

    /**
     * @var av_flag_Type $cao__cst_member_flag
     */
    protected $cao__cst_member_flag = null;

    /**
     * @var av_url_Type $cao__cst_url_code_dn
     */
    protected $cao__cst_url_code_dn = null;

    /**
     * @var av_key_Type $cao__cst_parent_cst_key
     */
    protected $cao__cst_parent_cst_key = null;

    /**
     * @var av_key_Type $cao__cst_url_key
     */
    protected $cao__cst_url_key = null;

    /**
     * @var av_flag_Type $cao__cst_no_msg_flag
     */
    protected $cao__cst_no_msg_flag = null;

    /**
     * @var av_messaging_name_Type $cao__cst_msg_handle_dn
     */
    protected $cao__cst_msg_handle_dn = null;

    /**
     * @var stringLength80_Type $cao__cst_web_login
     */
    protected $cao__cst_web_login = null;

    /**
     * @var stringLength50_Type $cao__cst_web_password
     */
    protected $cao__cst_web_password = null;

    /**
     * @var av_key_Type $cao__cst_entity_key
     */
    protected $cao__cst_entity_key = null;

    /**
     * @var av_key_Type $cao__cst_msg_key
     */
    protected $cao__cst_msg_key = null;

    /**
     * @var av_flag_Type $cao__cst_no_mail_flag
     */
    protected $cao__cst_no_mail_flag = null;

    /**
     * @var av_date_small_Type $cao__cst_web_start_date
     */
    protected $cao__cst_web_start_date = null;

    /**
     * @var av_date_small_Type $cao__cst_web_end_date
     */
    protected $cao__cst_web_end_date = null;

    /**
     * @var av_flag_Type $cao__cst_web_force_password_change
     */
    protected $cao__cst_web_force_password_change = null;

    /**
     * @var av_flag_Type $cao__cst_web_login_disabled_flag
     */
    protected $cao__cst_web_login_disabled_flag = null;

    /**
     * @var av_text_Type $cao__cst_comment
     */
    protected $cao__cst_comment = null;

    /**
     * @var av_flag_Type $cao__cst_credit_hold_flag
     */
    protected $cao__cst_credit_hold_flag = null;

    /**
     * @var stringLength50_Type $cao__cst_credit_hold_reason
     */
    protected $cao__cst_credit_hold_reason = null;

    /**
     * @var av_flag_Type $cao__cst_web_forgot_password_status
     */
    protected $cao__cst_web_forgot_password_status = null;

    /**
     * @var av_key_Type $cao__cst_old_cxa_key
     */
    protected $cao__cst_old_cxa_key = null;

    /**
     * @var av_date_small_Type $cao__cst_last_email_date
     */
    protected $cao__cst_last_email_date = null;

    /**
     * @var av_flag_Type $cao__cst_no_publish_flag
     */
    protected $cao__cst_no_publish_flag = null;

    /**
     * @var av_key_Type $cao__cst_sin_key
     */
    protected $cao__cst_sin_key = null;

    /**
     * @var av_key_Type $cao__cst_ttl_key
     */
    protected $cao__cst_ttl_key = null;

    /**
     * @var av_key_Type $cao__cst_jfn_key
     */
    protected $cao__cst_jfn_key = null;

    /**
     * @var av_key_Type $cao__cst_cur_key
     */
    protected $cao__cst_cur_key = null;

    /**
     * @var stringLength510_Type $cao__cst_attribute_1
     */
    protected $cao__cst_attribute_1 = null;

    /**
     * @var stringLength510_Type $cao__cst_attribute_2
     */
    protected $cao__cst_attribute_2 = null;

    /**
     * @var stringLength100_Type $cao__cst_salutation_1
     */
    protected $cao__cst_salutation_1 = null;

    /**
     * @var stringLength100_Type $cao__cst_salutation_2
     */
    protected $cao__cst_salutation_2 = null;

    /**
     * @var av_key_Type $cao__cst_merge_cst_key
     */
    protected $cao__cst_merge_cst_key = null;

    /**
     * @var stringLength100_Type $cao__cst_salutation_3
     */
    protected $cao__cst_salutation_3 = null;

    /**
     * @var stringLength100_Type $cao__cst_salutation_4
     */
    protected $cao__cst_salutation_4 = null;

    /**
     * @var stringLength200_Type $cao__cst_default_recognize_as
     */
    protected $cao__cst_default_recognize_as = null;

    /**
     * @var av_decimal4_Type $cao__cst_score
     */
    protected $cao__cst_score = null;

    /**
     * @var av_integer_Type $cao__cst_score_normalized
     */
    protected $cao__cst_score_normalized = null;

    /**
     * @var av_integer_Type $cao__cst_score_trend
     */
    protected $cao__cst_score_trend = null;

    /**
     * @var stringLength25_Type $cao__cst_vault_account
     */
    protected $cao__cst_vault_account = null;

    /**
     * @var av_flag_Type $cao__cst_exclude_from_social_flag
     */
    protected $cao__cst_exclude_from_social_flag = null;

    /**
     * @var av_integer_Type $cao__cst_social_score
     */
    protected $cao__cst_social_score = null;

    /**
     * @var stringLength9_Type $cao__cst_ptin
     */
    protected $cao__cst_ptin = null;

    /**
     * @var av_recno_Type $cao__cst_aicpa_member_id
     */
    protected $cao__cst_aicpa_member_id = null;

    /**
     * @var stringLength50_Type $cao__cst_vendor_code
     */
    protected $cao__cst_vendor_code = null;

    /**
     * @var stringLength1_Type $cao__cst_salt
     */
    protected $cao__cst_salt = null;

    /**
     * @var av_key_Type $cao__cst_sca_key
     */
    protected $cao__cst_sca_key = null;

    /**
     * @var av_integer_Type $cao__cst_iterations
     */
    protected $cao__cst_iterations = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getCao__cst_key()
    {
      return $this->cao__cst_key;
    }

    /**
     * @param av_key_Type $cao__cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_key($cao__cst_key)
    {
      $this->cao__cst_key = $cao__cst_key;
      return $this;
    }

    /**
     * @return stringLength20_Type
     */
    public function getCao__cst_type()
    {
      return $this->cao__cst_type;
    }

    /**
     * @param stringLength20_Type $cao__cst_type
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_type($cao__cst_type)
    {
      $this->cao__cst_type = $cao__cst_type;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getCao__cst_name_cp()
    {
      return $this->cao__cst_name_cp;
    }

    /**
     * @param stringLength150_Type $cao__cst_name_cp
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_name_cp($cao__cst_name_cp)
    {
      $this->cao__cst_name_cp = $cao__cst_name_cp;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getCao__cst_sort_name_dn()
    {
      return $this->cao__cst_sort_name_dn;
    }

    /**
     * @param stringLength150_Type $cao__cst_sort_name_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_sort_name_dn($cao__cst_sort_name_dn)
    {
      $this->cao__cst_sort_name_dn = $cao__cst_sort_name_dn;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getCao__cst_ind_full_name_dn()
    {
      return $this->cao__cst_ind_full_name_dn;
    }

    /**
     * @param stringLength150_Type $cao__cst_ind_full_name_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_ind_full_name_dn($cao__cst_ind_full_name_dn)
    {
      $this->cao__cst_ind_full_name_dn = $cao__cst_ind_full_name_dn;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getCao__cst_org_name_dn()
    {
      return $this->cao__cst_org_name_dn;
    }

    /**
     * @param stringLength150_Type $cao__cst_org_name_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_org_name_dn($cao__cst_org_name_dn)
    {
      $this->cao__cst_org_name_dn = $cao__cst_org_name_dn;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getCao__cst_ixo_title_dn()
    {
      return $this->cao__cst_ixo_title_dn;
    }

    /**
     * @param stringLength150_Type $cao__cst_ixo_title_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_ixo_title_dn($cao__cst_ixo_title_dn)
    {
      $this->cao__cst_ixo_title_dn = $cao__cst_ixo_title_dn;
      return $this;
    }

    /**
     * @return stringLength10_Type
     */
    public function getCao__cst_pref_comm_meth()
    {
      return $this->cao__cst_pref_comm_meth;
    }

    /**
     * @param stringLength10_Type $cao__cst_pref_comm_meth
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_pref_comm_meth($cao__cst_pref_comm_meth)
    {
      $this->cao__cst_pref_comm_meth = $cao__cst_pref_comm_meth;
      return $this;
    }

    /**
     * @return av_text_Type
     */
    public function getCao__cst_bio()
    {
      return $this->cao__cst_bio;
    }

    /**
     * @param av_text_Type $cao__cst_bio
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_bio($cao__cst_bio)
    {
      $this->cao__cst_bio = $cao__cst_bio;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getCao__cst_add_date()
    {
      return $this->cao__cst_add_date;
    }

    /**
     * @param av_date_small_Type $cao__cst_add_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_add_date($cao__cst_add_date)
    {
      $this->cao__cst_add_date = $cao__cst_add_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getCao__cst_add_user()
    {
      return $this->cao__cst_add_user;
    }

    /**
     * @param av_user_Type $cao__cst_add_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_add_user($cao__cst_add_user)
    {
      $this->cao__cst_add_user = $cao__cst_add_user;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getCao__cst_change_date()
    {
      return $this->cao__cst_change_date;
    }

    /**
     * @param av_date_small_Type $cao__cst_change_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_change_date($cao__cst_change_date)
    {
      $this->cao__cst_change_date = $cao__cst_change_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getCao__cst_change_user()
    {
      return $this->cao__cst_change_user;
    }

    /**
     * @param av_user_Type $cao__cst_change_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_change_user($cao__cst_change_user)
    {
      $this->cao__cst_change_user = $cao__cst_change_user;
      return $this;
    }

    /**
     * @return av_delete_flag_Type
     */
    public function getCao__cst_delete_flag()
    {
      return $this->cao__cst_delete_flag;
    }

    /**
     * @param av_delete_flag_Type $cao__cst_delete_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_delete_flag($cao__cst_delete_flag)
    {
      $this->cao__cst_delete_flag = $cao__cst_delete_flag;
      return $this;
    }

    /**
     * @return av_recno_Type
     */
    public function getCao__cst_recno()
    {
      return $this->cao__cst_recno;
    }

    /**
     * @param av_recno_Type $cao__cst_recno
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_recno($cao__cst_recno)
    {
      $this->cao__cst_recno = $cao__cst_recno;
      return $this;
    }

    /**
     * @return stringLength10_Type
     */
    public function getCao__cst_id()
    {
      return $this->cao__cst_id;
    }

    /**
     * @param stringLength10_Type $cao__cst_id
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_id($cao__cst_id)
    {
      $this->cao__cst_id = $cao__cst_id;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCao__cst_key_ext()
    {
      return $this->cao__cst_key_ext;
    }

    /**
     * @param av_key_Type $cao__cst_key_ext
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_key_ext($cao__cst_key_ext)
    {
      $this->cao__cst_key_ext = $cao__cst_key_ext;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCao__cst_email_text_only()
    {
      return $this->cao__cst_email_text_only;
    }

    /**
     * @param av_flag_Type $cao__cst_email_text_only
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_email_text_only($cao__cst_email_text_only)
    {
      $this->cao__cst_email_text_only = $cao__cst_email_text_only;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getCao__cst_credit_limit()
    {
      return $this->cao__cst_credit_limit;
    }

    /**
     * @param av_currency_Type $cao__cst_credit_limit
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_credit_limit($cao__cst_credit_limit)
    {
      $this->cao__cst_credit_limit = $cao__cst_credit_limit;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCao__cst_src_key()
    {
      return $this->cao__cst_src_key;
    }

    /**
     * @param av_key_Type $cao__cst_src_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_src_key($cao__cst_src_key)
    {
      $this->cao__cst_src_key = $cao__cst_src_key;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getCao__cst_src_code()
    {
      return $this->cao__cst_src_code;
    }

    /**
     * @param stringLength50_Type $cao__cst_src_code
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_src_code($cao__cst_src_code)
    {
      $this->cao__cst_src_code = $cao__cst_src_code;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCao__cst_tax_exempt_flag()
    {
      return $this->cao__cst_tax_exempt_flag;
    }

    /**
     * @param av_flag_Type $cao__cst_tax_exempt_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_tax_exempt_flag($cao__cst_tax_exempt_flag)
    {
      $this->cao__cst_tax_exempt_flag = $cao__cst_tax_exempt_flag;
      return $this;
    }

    /**
     * @return stringLength30_Type
     */
    public function getCao__cst_tax_id()
    {
      return $this->cao__cst_tax_id;
    }

    /**
     * @param stringLength30_Type $cao__cst_tax_id
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_tax_id($cao__cst_tax_id)
    {
      $this->cao__cst_tax_id = $cao__cst_tax_id;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCao__cst_cxa_key()
    {
      return $this->cao__cst_cxa_key;
    }

    /**
     * @param av_key_Type $cao__cst_cxa_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_cxa_key($cao__cst_cxa_key)
    {
      $this->cao__cst_cxa_key = $cao__cst_cxa_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCao__cst_no_email_flag()
    {
      return $this->cao__cst_no_email_flag;
    }

    /**
     * @param av_flag_Type $cao__cst_no_email_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_no_email_flag($cao__cst_no_email_flag)
    {
      $this->cao__cst_no_email_flag = $cao__cst_no_email_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCao__cst_cxa_billing_key()
    {
      return $this->cao__cst_cxa_billing_key;
    }

    /**
     * @param av_key_Type $cao__cst_cxa_billing_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_cxa_billing_key($cao__cst_cxa_billing_key)
    {
      $this->cao__cst_cxa_billing_key = $cao__cst_cxa_billing_key;
      return $this;
    }

    /**
     * @return av_email_Type
     */
    public function getCao__cst_eml_address_dn()
    {
      return $this->cao__cst_eml_address_dn;
    }

    /**
     * @param av_email_Type $cao__cst_eml_address_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_eml_address_dn($cao__cst_eml_address_dn)
    {
      $this->cao__cst_eml_address_dn = $cao__cst_eml_address_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCao__cst_eml_key()
    {
      return $this->cao__cst_eml_key;
    }

    /**
     * @param av_key_Type $cao__cst_eml_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_eml_key($cao__cst_eml_key)
    {
      $this->cao__cst_eml_key = $cao__cst_eml_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCao__cst_no_phone_flag()
    {
      return $this->cao__cst_no_phone_flag;
    }

    /**
     * @param av_flag_Type $cao__cst_no_phone_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_no_phone_flag($cao__cst_no_phone_flag)
    {
      $this->cao__cst_no_phone_flag = $cao__cst_no_phone_flag;
      return $this;
    }

    /**
     * @return stringLength55_Type
     */
    public function getCao__cst_phn_number_complete_dn()
    {
      return $this->cao__cst_phn_number_complete_dn;
    }

    /**
     * @param stringLength55_Type $cao__cst_phn_number_complete_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_phn_number_complete_dn($cao__cst_phn_number_complete_dn)
    {
      $this->cao__cst_phn_number_complete_dn = $cao__cst_phn_number_complete_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCao__cst_cph_key()
    {
      return $this->cao__cst_cph_key;
    }

    /**
     * @param av_key_Type $cao__cst_cph_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_cph_key($cao__cst_cph_key)
    {
      $this->cao__cst_cph_key = $cao__cst_cph_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCao__cst_no_fax_flag()
    {
      return $this->cao__cst_no_fax_flag;
    }

    /**
     * @param av_flag_Type $cao__cst_no_fax_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_no_fax_flag($cao__cst_no_fax_flag)
    {
      $this->cao__cst_no_fax_flag = $cao__cst_no_fax_flag;
      return $this;
    }

    /**
     * @return stringLength55_Type
     */
    public function getCao__cst_fax_number_complete_dn()
    {
      return $this->cao__cst_fax_number_complete_dn;
    }

    /**
     * @param stringLength55_Type $cao__cst_fax_number_complete_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_fax_number_complete_dn($cao__cst_fax_number_complete_dn)
    {
      $this->cao__cst_fax_number_complete_dn = $cao__cst_fax_number_complete_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCao__cst_cfx_key()
    {
      return $this->cao__cst_cfx_key;
    }

    /**
     * @param av_key_Type $cao__cst_cfx_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_cfx_key($cao__cst_cfx_key)
    {
      $this->cao__cst_cfx_key = $cao__cst_cfx_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCao__cst_ixo_key()
    {
      return $this->cao__cst_ixo_key;
    }

    /**
     * @param av_key_Type $cao__cst_ixo_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_ixo_key($cao__cst_ixo_key)
    {
      $this->cao__cst_ixo_key = $cao__cst_ixo_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCao__cst_no_web_flag()
    {
      return $this->cao__cst_no_web_flag;
    }

    /**
     * @param av_flag_Type $cao__cst_no_web_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_no_web_flag($cao__cst_no_web_flag)
    {
      $this->cao__cst_no_web_flag = $cao__cst_no_web_flag;
      return $this;
    }

    /**
     * @return stringLength15_Type
     */
    public function getCao__cst_oldid()
    {
      return $this->cao__cst_oldid;
    }

    /**
     * @param stringLength15_Type $cao__cst_oldid
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_oldid($cao__cst_oldid)
    {
      $this->cao__cst_oldid = $cao__cst_oldid;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCao__cst_member_flag()
    {
      return $this->cao__cst_member_flag;
    }

    /**
     * @param av_flag_Type $cao__cst_member_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_member_flag($cao__cst_member_flag)
    {
      $this->cao__cst_member_flag = $cao__cst_member_flag;
      return $this;
    }

    /**
     * @return av_url_Type
     */
    public function getCao__cst_url_code_dn()
    {
      return $this->cao__cst_url_code_dn;
    }

    /**
     * @param av_url_Type $cao__cst_url_code_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_url_code_dn($cao__cst_url_code_dn)
    {
      $this->cao__cst_url_code_dn = $cao__cst_url_code_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCao__cst_parent_cst_key()
    {
      return $this->cao__cst_parent_cst_key;
    }

    /**
     * @param av_key_Type $cao__cst_parent_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_parent_cst_key($cao__cst_parent_cst_key)
    {
      $this->cao__cst_parent_cst_key = $cao__cst_parent_cst_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCao__cst_url_key()
    {
      return $this->cao__cst_url_key;
    }

    /**
     * @param av_key_Type $cao__cst_url_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_url_key($cao__cst_url_key)
    {
      $this->cao__cst_url_key = $cao__cst_url_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCao__cst_no_msg_flag()
    {
      return $this->cao__cst_no_msg_flag;
    }

    /**
     * @param av_flag_Type $cao__cst_no_msg_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_no_msg_flag($cao__cst_no_msg_flag)
    {
      $this->cao__cst_no_msg_flag = $cao__cst_no_msg_flag;
      return $this;
    }

    /**
     * @return av_messaging_name_Type
     */
    public function getCao__cst_msg_handle_dn()
    {
      return $this->cao__cst_msg_handle_dn;
    }

    /**
     * @param av_messaging_name_Type $cao__cst_msg_handle_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_msg_handle_dn($cao__cst_msg_handle_dn)
    {
      $this->cao__cst_msg_handle_dn = $cao__cst_msg_handle_dn;
      return $this;
    }

    /**
     * @return stringLength80_Type
     */
    public function getCao__cst_web_login()
    {
      return $this->cao__cst_web_login;
    }

    /**
     * @param stringLength80_Type $cao__cst_web_login
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_web_login($cao__cst_web_login)
    {
      $this->cao__cst_web_login = $cao__cst_web_login;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getCao__cst_web_password()
    {
      return $this->cao__cst_web_password;
    }

    /**
     * @param stringLength50_Type $cao__cst_web_password
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_web_password($cao__cst_web_password)
    {
      $this->cao__cst_web_password = $cao__cst_web_password;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCao__cst_entity_key()
    {
      return $this->cao__cst_entity_key;
    }

    /**
     * @param av_key_Type $cao__cst_entity_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_entity_key($cao__cst_entity_key)
    {
      $this->cao__cst_entity_key = $cao__cst_entity_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCao__cst_msg_key()
    {
      return $this->cao__cst_msg_key;
    }

    /**
     * @param av_key_Type $cao__cst_msg_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_msg_key($cao__cst_msg_key)
    {
      $this->cao__cst_msg_key = $cao__cst_msg_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCao__cst_no_mail_flag()
    {
      return $this->cao__cst_no_mail_flag;
    }

    /**
     * @param av_flag_Type $cao__cst_no_mail_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_no_mail_flag($cao__cst_no_mail_flag)
    {
      $this->cao__cst_no_mail_flag = $cao__cst_no_mail_flag;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getCao__cst_web_start_date()
    {
      return $this->cao__cst_web_start_date;
    }

    /**
     * @param av_date_small_Type $cao__cst_web_start_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_web_start_date($cao__cst_web_start_date)
    {
      $this->cao__cst_web_start_date = $cao__cst_web_start_date;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getCao__cst_web_end_date()
    {
      return $this->cao__cst_web_end_date;
    }

    /**
     * @param av_date_small_Type $cao__cst_web_end_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_web_end_date($cao__cst_web_end_date)
    {
      $this->cao__cst_web_end_date = $cao__cst_web_end_date;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCao__cst_web_force_password_change()
    {
      return $this->cao__cst_web_force_password_change;
    }

    /**
     * @param av_flag_Type $cao__cst_web_force_password_change
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_web_force_password_change($cao__cst_web_force_password_change)
    {
      $this->cao__cst_web_force_password_change = $cao__cst_web_force_password_change;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCao__cst_web_login_disabled_flag()
    {
      return $this->cao__cst_web_login_disabled_flag;
    }

    /**
     * @param av_flag_Type $cao__cst_web_login_disabled_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_web_login_disabled_flag($cao__cst_web_login_disabled_flag)
    {
      $this->cao__cst_web_login_disabled_flag = $cao__cst_web_login_disabled_flag;
      return $this;
    }

    /**
     * @return av_text_Type
     */
    public function getCao__cst_comment()
    {
      return $this->cao__cst_comment;
    }

    /**
     * @param av_text_Type $cao__cst_comment
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_comment($cao__cst_comment)
    {
      $this->cao__cst_comment = $cao__cst_comment;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCao__cst_credit_hold_flag()
    {
      return $this->cao__cst_credit_hold_flag;
    }

    /**
     * @param av_flag_Type $cao__cst_credit_hold_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_credit_hold_flag($cao__cst_credit_hold_flag)
    {
      $this->cao__cst_credit_hold_flag = $cao__cst_credit_hold_flag;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getCao__cst_credit_hold_reason()
    {
      return $this->cao__cst_credit_hold_reason;
    }

    /**
     * @param stringLength50_Type $cao__cst_credit_hold_reason
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_credit_hold_reason($cao__cst_credit_hold_reason)
    {
      $this->cao__cst_credit_hold_reason = $cao__cst_credit_hold_reason;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCao__cst_web_forgot_password_status()
    {
      return $this->cao__cst_web_forgot_password_status;
    }

    /**
     * @param av_flag_Type $cao__cst_web_forgot_password_status
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_web_forgot_password_status($cao__cst_web_forgot_password_status)
    {
      $this->cao__cst_web_forgot_password_status = $cao__cst_web_forgot_password_status;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCao__cst_old_cxa_key()
    {
      return $this->cao__cst_old_cxa_key;
    }

    /**
     * @param av_key_Type $cao__cst_old_cxa_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_old_cxa_key($cao__cst_old_cxa_key)
    {
      $this->cao__cst_old_cxa_key = $cao__cst_old_cxa_key;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getCao__cst_last_email_date()
    {
      return $this->cao__cst_last_email_date;
    }

    /**
     * @param av_date_small_Type $cao__cst_last_email_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_last_email_date($cao__cst_last_email_date)
    {
      $this->cao__cst_last_email_date = $cao__cst_last_email_date;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCao__cst_no_publish_flag()
    {
      return $this->cao__cst_no_publish_flag;
    }

    /**
     * @param av_flag_Type $cao__cst_no_publish_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_no_publish_flag($cao__cst_no_publish_flag)
    {
      $this->cao__cst_no_publish_flag = $cao__cst_no_publish_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCao__cst_sin_key()
    {
      return $this->cao__cst_sin_key;
    }

    /**
     * @param av_key_Type $cao__cst_sin_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_sin_key($cao__cst_sin_key)
    {
      $this->cao__cst_sin_key = $cao__cst_sin_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCao__cst_ttl_key()
    {
      return $this->cao__cst_ttl_key;
    }

    /**
     * @param av_key_Type $cao__cst_ttl_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_ttl_key($cao__cst_ttl_key)
    {
      $this->cao__cst_ttl_key = $cao__cst_ttl_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCao__cst_jfn_key()
    {
      return $this->cao__cst_jfn_key;
    }

    /**
     * @param av_key_Type $cao__cst_jfn_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_jfn_key($cao__cst_jfn_key)
    {
      $this->cao__cst_jfn_key = $cao__cst_jfn_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCao__cst_cur_key()
    {
      return $this->cao__cst_cur_key;
    }

    /**
     * @param av_key_Type $cao__cst_cur_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_cur_key($cao__cst_cur_key)
    {
      $this->cao__cst_cur_key = $cao__cst_cur_key;
      return $this;
    }

    /**
     * @return stringLength510_Type
     */
    public function getCao__cst_attribute_1()
    {
      return $this->cao__cst_attribute_1;
    }

    /**
     * @param stringLength510_Type $cao__cst_attribute_1
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_attribute_1($cao__cst_attribute_1)
    {
      $this->cao__cst_attribute_1 = $cao__cst_attribute_1;
      return $this;
    }

    /**
     * @return stringLength510_Type
     */
    public function getCao__cst_attribute_2()
    {
      return $this->cao__cst_attribute_2;
    }

    /**
     * @param stringLength510_Type $cao__cst_attribute_2
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_attribute_2($cao__cst_attribute_2)
    {
      $this->cao__cst_attribute_2 = $cao__cst_attribute_2;
      return $this;
    }

    /**
     * @return stringLength100_Type
     */
    public function getCao__cst_salutation_1()
    {
      return $this->cao__cst_salutation_1;
    }

    /**
     * @param stringLength100_Type $cao__cst_salutation_1
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_salutation_1($cao__cst_salutation_1)
    {
      $this->cao__cst_salutation_1 = $cao__cst_salutation_1;
      return $this;
    }

    /**
     * @return stringLength100_Type
     */
    public function getCao__cst_salutation_2()
    {
      return $this->cao__cst_salutation_2;
    }

    /**
     * @param stringLength100_Type $cao__cst_salutation_2
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_salutation_2($cao__cst_salutation_2)
    {
      $this->cao__cst_salutation_2 = $cao__cst_salutation_2;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCao__cst_merge_cst_key()
    {
      return $this->cao__cst_merge_cst_key;
    }

    /**
     * @param av_key_Type $cao__cst_merge_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_merge_cst_key($cao__cst_merge_cst_key)
    {
      $this->cao__cst_merge_cst_key = $cao__cst_merge_cst_key;
      return $this;
    }

    /**
     * @return stringLength100_Type
     */
    public function getCao__cst_salutation_3()
    {
      return $this->cao__cst_salutation_3;
    }

    /**
     * @param stringLength100_Type $cao__cst_salutation_3
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_salutation_3($cao__cst_salutation_3)
    {
      $this->cao__cst_salutation_3 = $cao__cst_salutation_3;
      return $this;
    }

    /**
     * @return stringLength100_Type
     */
    public function getCao__cst_salutation_4()
    {
      return $this->cao__cst_salutation_4;
    }

    /**
     * @param stringLength100_Type $cao__cst_salutation_4
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_salutation_4($cao__cst_salutation_4)
    {
      $this->cao__cst_salutation_4 = $cao__cst_salutation_4;
      return $this;
    }

    /**
     * @return stringLength200_Type
     */
    public function getCao__cst_default_recognize_as()
    {
      return $this->cao__cst_default_recognize_as;
    }

    /**
     * @param stringLength200_Type $cao__cst_default_recognize_as
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_default_recognize_as($cao__cst_default_recognize_as)
    {
      $this->cao__cst_default_recognize_as = $cao__cst_default_recognize_as;
      return $this;
    }

    /**
     * @return av_decimal4_Type
     */
    public function getCao__cst_score()
    {
      return $this->cao__cst_score;
    }

    /**
     * @param av_decimal4_Type $cao__cst_score
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_score($cao__cst_score)
    {
      $this->cao__cst_score = $cao__cst_score;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getCao__cst_score_normalized()
    {
      return $this->cao__cst_score_normalized;
    }

    /**
     * @param av_integer_Type $cao__cst_score_normalized
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_score_normalized($cao__cst_score_normalized)
    {
      $this->cao__cst_score_normalized = $cao__cst_score_normalized;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getCao__cst_score_trend()
    {
      return $this->cao__cst_score_trend;
    }

    /**
     * @param av_integer_Type $cao__cst_score_trend
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_score_trend($cao__cst_score_trend)
    {
      $this->cao__cst_score_trend = $cao__cst_score_trend;
      return $this;
    }

    /**
     * @return stringLength25_Type
     */
    public function getCao__cst_vault_account()
    {
      return $this->cao__cst_vault_account;
    }

    /**
     * @param stringLength25_Type $cao__cst_vault_account
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_vault_account($cao__cst_vault_account)
    {
      $this->cao__cst_vault_account = $cao__cst_vault_account;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCao__cst_exclude_from_social_flag()
    {
      return $this->cao__cst_exclude_from_social_flag;
    }

    /**
     * @param av_flag_Type $cao__cst_exclude_from_social_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_exclude_from_social_flag($cao__cst_exclude_from_social_flag)
    {
      $this->cao__cst_exclude_from_social_flag = $cao__cst_exclude_from_social_flag;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getCao__cst_social_score()
    {
      return $this->cao__cst_social_score;
    }

    /**
     * @param av_integer_Type $cao__cst_social_score
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_social_score($cao__cst_social_score)
    {
      $this->cao__cst_social_score = $cao__cst_social_score;
      return $this;
    }

    /**
     * @return stringLength9_Type
     */
    public function getCao__cst_ptin()
    {
      return $this->cao__cst_ptin;
    }

    /**
     * @param stringLength9_Type $cao__cst_ptin
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_ptin($cao__cst_ptin)
    {
      $this->cao__cst_ptin = $cao__cst_ptin;
      return $this;
    }

    /**
     * @return av_recno_Type
     */
    public function getCao__cst_aicpa_member_id()
    {
      return $this->cao__cst_aicpa_member_id;
    }

    /**
     * @param av_recno_Type $cao__cst_aicpa_member_id
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_aicpa_member_id($cao__cst_aicpa_member_id)
    {
      $this->cao__cst_aicpa_member_id = $cao__cst_aicpa_member_id;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getCao__cst_vendor_code()
    {
      return $this->cao__cst_vendor_code;
    }

    /**
     * @param stringLength50_Type $cao__cst_vendor_code
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_vendor_code($cao__cst_vendor_code)
    {
      $this->cao__cst_vendor_code = $cao__cst_vendor_code;
      return $this;
    }

    /**
     * @return stringLength1_Type
     */
    public function getCao__cst_salt()
    {
      return $this->cao__cst_salt;
    }

    /**
     * @param stringLength1_Type $cao__cst_salt
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_salt($cao__cst_salt)
    {
      $this->cao__cst_salt = $cao__cst_salt;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCao__cst_sca_key()
    {
      return $this->cao__cst_sca_key;
    }

    /**
     * @param av_key_Type $cao__cst_sca_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_sca_key($cao__cst_sca_key)
    {
      $this->cao__cst_sca_key = $cao__cst_sca_key;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getCao__cst_iterations()
    {
      return $this->cao__cst_iterations;
    }

    /**
     * @param av_integer_Type $cao__cst_iterations
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CAO_DataObjectType
     */
    public function setCao__cst_iterations($cao__cst_iterations)
    {
      $this->cao__cst_iterations = $cao__cst_iterations;
      return $this;
    }

}
