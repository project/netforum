<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGiftGetGiftProductByKeyResponse
{

    /**
     * @var WEBCentralizedShoppingCartGiftGetGiftProductByKeyResult $WEBCentralizedShoppingCartGiftGetGiftProductByKeyResult
     */
    protected $WEBCentralizedShoppingCartGiftGetGiftProductByKeyResult = null;

    /**
     * @param WEBCentralizedShoppingCartGiftGetGiftProductByKeyResult $WEBCentralizedShoppingCartGiftGetGiftProductByKeyResult
     */
    public function __construct($WEBCentralizedShoppingCartGiftGetGiftProductByKeyResult)
    {
      $this->WEBCentralizedShoppingCartGiftGetGiftProductByKeyResult = $WEBCentralizedShoppingCartGiftGetGiftProductByKeyResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGiftGetGiftProductByKeyResult
     */
    public function getWEBCentralizedShoppingCartGiftGetGiftProductByKeyResult()
    {
      return $this->WEBCentralizedShoppingCartGiftGetGiftProductByKeyResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGiftGetGiftProductByKeyResult $WEBCentralizedShoppingCartGiftGetGiftProductByKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGiftGetGiftProductByKeyResponse
     */
    public function setWEBCentralizedShoppingCartGiftGetGiftProductByKeyResult($WEBCentralizedShoppingCartGiftGetGiftProductByKeyResult)
    {
      $this->WEBCentralizedShoppingCartGiftGetGiftProductByKeyResult = $WEBCentralizedShoppingCartGiftGetGiftProductByKeyResult;
      return $this;
    }

}
