<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBChaptersGetChapterByKeyResponse
{

    /**
     * @var WEBChaptersGetChapterByKeyResult $WEBChaptersGetChapterByKeyResult
     */
    protected $WEBChaptersGetChapterByKeyResult = null;

    /**
     * @param WEBChaptersGetChapterByKeyResult $WEBChaptersGetChapterByKeyResult
     */
    public function __construct($WEBChaptersGetChapterByKeyResult)
    {
      $this->WEBChaptersGetChapterByKeyResult = $WEBChaptersGetChapterByKeyResult;
    }

    /**
     * @return WEBChaptersGetChapterByKeyResult
     */
    public function getWEBChaptersGetChapterByKeyResult()
    {
      return $this->WEBChaptersGetChapterByKeyResult;
    }

    /**
     * @param WEBChaptersGetChapterByKeyResult $WEBChaptersGetChapterByKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBChaptersGetChapterByKeyResponse
     */
    public function setWEBChaptersGetChapterByKeyResult($WEBChaptersGetChapterByKeyResult)
    {
      $this->WEBChaptersGetChapterByKeyResult = $WEBChaptersGetChapterByKeyResult;
      return $this;
    }

}
