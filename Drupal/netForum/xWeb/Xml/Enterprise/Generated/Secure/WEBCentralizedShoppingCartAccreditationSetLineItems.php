<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartAccreditationSetLineItems
{

    /**
     * @var AccreditationAreaType $oAccreditation
     */
    protected $oAccreditation = null;

    /**
     * @var Fees $oFeeCollection
     */
    protected $oFeeCollection = null;

    /**
     * @param AccreditationAreaType $oAccreditation
     * @param Fees $oFeeCollection
     */
    public function __construct($oAccreditation, $oFeeCollection)
    {
      $this->oAccreditation = $oAccreditation;
      $this->oFeeCollection = $oFeeCollection;
    }

    /**
     * @return AccreditationAreaType
     */
    public function getOAccreditation()
    {
      return $this->oAccreditation;
    }

    /**
     * @param AccreditationAreaType $oAccreditation
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartAccreditationSetLineItems
     */
    public function setOAccreditation($oAccreditation)
    {
      $this->oAccreditation = $oAccreditation;
      return $this;
    }

    /**
     * @return Fees
     */
    public function getOFeeCollection()
    {
      return $this->oFeeCollection;
    }

    /**
     * @param Fees $oFeeCollection
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartAccreditationSetLineItems
     */
    public function setOFeeCollection($oFeeCollection)
    {
      $this->oFeeCollection = $oFeeCollection;
      return $this;
    }

}
