<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class AccreditationArea_Affiliated_CEO_DataObjectType
{

    /**
     * @var av_key_Type $ceo__cst_key
     */
    protected $ceo__cst_key = null;

    /**
     * @var stringLength20_Type $ceo__cst_type
     */
    protected $ceo__cst_type = null;

    /**
     * @var stringLength150_Type $ceo__cst_name_cp
     */
    protected $ceo__cst_name_cp = null;

    /**
     * @var stringLength150_Type $ceo__cst_sort_name_dn
     */
    protected $ceo__cst_sort_name_dn = null;

    /**
     * @var stringLength150_Type $ceo__cst_ind_full_name_dn
     */
    protected $ceo__cst_ind_full_name_dn = null;

    /**
     * @var stringLength150_Type $ceo__cst_org_name_dn
     */
    protected $ceo__cst_org_name_dn = null;

    /**
     * @var stringLength150_Type $ceo__cst_ixo_title_dn
     */
    protected $ceo__cst_ixo_title_dn = null;

    /**
     * @var stringLength10_Type $ceo__cst_pref_comm_meth
     */
    protected $ceo__cst_pref_comm_meth = null;

    /**
     * @var av_text_Type $ceo__cst_bio
     */
    protected $ceo__cst_bio = null;

    /**
     * @var av_date_small_Type $ceo__cst_add_date
     */
    protected $ceo__cst_add_date = null;

    /**
     * @var av_user_Type $ceo__cst_add_user
     */
    protected $ceo__cst_add_user = null;

    /**
     * @var av_date_small_Type $ceo__cst_change_date
     */
    protected $ceo__cst_change_date = null;

    /**
     * @var av_user_Type $ceo__cst_change_user
     */
    protected $ceo__cst_change_user = null;

    /**
     * @var av_delete_flag_Type $ceo__cst_delete_flag
     */
    protected $ceo__cst_delete_flag = null;

    /**
     * @var av_recno_Type $ceo__cst_recno
     */
    protected $ceo__cst_recno = null;

    /**
     * @var stringLength10_Type $ceo__cst_id
     */
    protected $ceo__cst_id = null;

    /**
     * @var av_key_Type $ceo__cst_key_ext
     */
    protected $ceo__cst_key_ext = null;

    /**
     * @var av_flag_Type $ceo__cst_email_text_only
     */
    protected $ceo__cst_email_text_only = null;

    /**
     * @var av_currency_Type $ceo__cst_credit_limit
     */
    protected $ceo__cst_credit_limit = null;

    /**
     * @var av_key_Type $ceo__cst_src_key
     */
    protected $ceo__cst_src_key = null;

    /**
     * @var stringLength50_Type $ceo__cst_src_code
     */
    protected $ceo__cst_src_code = null;

    /**
     * @var av_flag_Type $ceo__cst_tax_exempt_flag
     */
    protected $ceo__cst_tax_exempt_flag = null;

    /**
     * @var stringLength30_Type $ceo__cst_tax_id
     */
    protected $ceo__cst_tax_id = null;

    /**
     * @var av_key_Type $ceo__cst_cxa_key
     */
    protected $ceo__cst_cxa_key = null;

    /**
     * @var av_flag_Type $ceo__cst_no_email_flag
     */
    protected $ceo__cst_no_email_flag = null;

    /**
     * @var av_key_Type $ceo__cst_cxa_billing_key
     */
    protected $ceo__cst_cxa_billing_key = null;

    /**
     * @var av_email_Type $ceo__cst_eml_address_dn
     */
    protected $ceo__cst_eml_address_dn = null;

    /**
     * @var av_key_Type $ceo__cst_eml_key
     */
    protected $ceo__cst_eml_key = null;

    /**
     * @var av_flag_Type $ceo__cst_no_phone_flag
     */
    protected $ceo__cst_no_phone_flag = null;

    /**
     * @var stringLength55_Type $ceo__cst_phn_number_complete_dn
     */
    protected $ceo__cst_phn_number_complete_dn = null;

    /**
     * @var av_key_Type $ceo__cst_cph_key
     */
    protected $ceo__cst_cph_key = null;

    /**
     * @var av_flag_Type $ceo__cst_no_fax_flag
     */
    protected $ceo__cst_no_fax_flag = null;

    /**
     * @var stringLength55_Type $ceo__cst_fax_number_complete_dn
     */
    protected $ceo__cst_fax_number_complete_dn = null;

    /**
     * @var av_key_Type $ceo__cst_cfx_key
     */
    protected $ceo__cst_cfx_key = null;

    /**
     * @var av_key_Type $ceo__cst_ixo_key
     */
    protected $ceo__cst_ixo_key = null;

    /**
     * @var av_flag_Type $ceo__cst_no_web_flag
     */
    protected $ceo__cst_no_web_flag = null;

    /**
     * @var stringLength15_Type $ceo__cst_oldid
     */
    protected $ceo__cst_oldid = null;

    /**
     * @var av_flag_Type $ceo__cst_member_flag
     */
    protected $ceo__cst_member_flag = null;

    /**
     * @var av_url_Type $ceo__cst_url_code_dn
     */
    protected $ceo__cst_url_code_dn = null;

    /**
     * @var av_key_Type $ceo__cst_parent_cst_key
     */
    protected $ceo__cst_parent_cst_key = null;

    /**
     * @var av_key_Type $ceo__cst_url_key
     */
    protected $ceo__cst_url_key = null;

    /**
     * @var av_flag_Type $ceo__cst_no_msg_flag
     */
    protected $ceo__cst_no_msg_flag = null;

    /**
     * @var av_messaging_name_Type $ceo__cst_msg_handle_dn
     */
    protected $ceo__cst_msg_handle_dn = null;

    /**
     * @var stringLength80_Type $ceo__cst_web_login
     */
    protected $ceo__cst_web_login = null;

    /**
     * @var stringLength50_Type $ceo__cst_web_password
     */
    protected $ceo__cst_web_password = null;

    /**
     * @var av_key_Type $ceo__cst_entity_key
     */
    protected $ceo__cst_entity_key = null;

    /**
     * @var av_key_Type $ceo__cst_msg_key
     */
    protected $ceo__cst_msg_key = null;

    /**
     * @var av_flag_Type $ceo__cst_no_mail_flag
     */
    protected $ceo__cst_no_mail_flag = null;

    /**
     * @var av_date_small_Type $ceo__cst_web_start_date
     */
    protected $ceo__cst_web_start_date = null;

    /**
     * @var av_date_small_Type $ceo__cst_web_end_date
     */
    protected $ceo__cst_web_end_date = null;

    /**
     * @var av_flag_Type $ceo__cst_web_force_password_change
     */
    protected $ceo__cst_web_force_password_change = null;

    /**
     * @var av_flag_Type $ceo__cst_web_login_disabled_flag
     */
    protected $ceo__cst_web_login_disabled_flag = null;

    /**
     * @var av_text_Type $ceo__cst_comment
     */
    protected $ceo__cst_comment = null;

    /**
     * @var av_flag_Type $ceo__cst_credit_hold_flag
     */
    protected $ceo__cst_credit_hold_flag = null;

    /**
     * @var stringLength50_Type $ceo__cst_credit_hold_reason
     */
    protected $ceo__cst_credit_hold_reason = null;

    /**
     * @var av_flag_Type $ceo__cst_web_forgot_password_status
     */
    protected $ceo__cst_web_forgot_password_status = null;

    /**
     * @var av_key_Type $ceo__cst_old_cxa_key
     */
    protected $ceo__cst_old_cxa_key = null;

    /**
     * @var av_date_small_Type $ceo__cst_last_email_date
     */
    protected $ceo__cst_last_email_date = null;

    /**
     * @var av_flag_Type $ceo__cst_no_publish_flag
     */
    protected $ceo__cst_no_publish_flag = null;

    /**
     * @var av_key_Type $ceo__cst_sin_key
     */
    protected $ceo__cst_sin_key = null;

    /**
     * @var av_key_Type $ceo__cst_ttl_key
     */
    protected $ceo__cst_ttl_key = null;

    /**
     * @var av_key_Type $ceo__cst_jfn_key
     */
    protected $ceo__cst_jfn_key = null;

    /**
     * @var av_key_Type $ceo__cst_cur_key
     */
    protected $ceo__cst_cur_key = null;

    /**
     * @var stringLength510_Type $ceo__cst_attribute_1
     */
    protected $ceo__cst_attribute_1 = null;

    /**
     * @var stringLength510_Type $ceo__cst_attribute_2
     */
    protected $ceo__cst_attribute_2 = null;

    /**
     * @var stringLength100_Type $ceo__cst_salutation_1
     */
    protected $ceo__cst_salutation_1 = null;

    /**
     * @var stringLength100_Type $ceo__cst_salutation_2
     */
    protected $ceo__cst_salutation_2 = null;

    /**
     * @var av_key_Type $ceo__cst_merge_cst_key
     */
    protected $ceo__cst_merge_cst_key = null;

    /**
     * @var stringLength100_Type $ceo__cst_salutation_3
     */
    protected $ceo__cst_salutation_3 = null;

    /**
     * @var stringLength100_Type $ceo__cst_salutation_4
     */
    protected $ceo__cst_salutation_4 = null;

    /**
     * @var stringLength200_Type $ceo__cst_default_recognize_as
     */
    protected $ceo__cst_default_recognize_as = null;

    /**
     * @var av_decimal4_Type $ceo__cst_score
     */
    protected $ceo__cst_score = null;

    /**
     * @var av_integer_Type $ceo__cst_score_normalized
     */
    protected $ceo__cst_score_normalized = null;

    /**
     * @var av_integer_Type $ceo__cst_score_trend
     */
    protected $ceo__cst_score_trend = null;

    /**
     * @var stringLength25_Type $ceo__cst_vault_account
     */
    protected $ceo__cst_vault_account = null;

    /**
     * @var av_flag_Type $ceo__cst_exclude_from_social_flag
     */
    protected $ceo__cst_exclude_from_social_flag = null;

    /**
     * @var av_integer_Type $ceo__cst_social_score
     */
    protected $ceo__cst_social_score = null;

    /**
     * @var stringLength9_Type $ceo__cst_ptin
     */
    protected $ceo__cst_ptin = null;

    /**
     * @var av_recno_Type $ceo__cst_aicpa_member_id
     */
    protected $ceo__cst_aicpa_member_id = null;

    /**
     * @var stringLength50_Type $ceo__cst_vendor_code
     */
    protected $ceo__cst_vendor_code = null;

    /**
     * @var stringLength1_Type $ceo__cst_salt
     */
    protected $ceo__cst_salt = null;

    /**
     * @var av_key_Type $ceo__cst_sca_key
     */
    protected $ceo__cst_sca_key = null;

    /**
     * @var av_integer_Type $ceo__cst_iterations
     */
    protected $ceo__cst_iterations = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getCeo__cst_key()
    {
      return $this->ceo__cst_key;
    }

    /**
     * @param av_key_Type $ceo__cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_key($ceo__cst_key)
    {
      $this->ceo__cst_key = $ceo__cst_key;
      return $this;
    }

    /**
     * @return stringLength20_Type
     */
    public function getCeo__cst_type()
    {
      return $this->ceo__cst_type;
    }

    /**
     * @param stringLength20_Type $ceo__cst_type
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_type($ceo__cst_type)
    {
      $this->ceo__cst_type = $ceo__cst_type;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getCeo__cst_name_cp()
    {
      return $this->ceo__cst_name_cp;
    }

    /**
     * @param stringLength150_Type $ceo__cst_name_cp
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_name_cp($ceo__cst_name_cp)
    {
      $this->ceo__cst_name_cp = $ceo__cst_name_cp;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getCeo__cst_sort_name_dn()
    {
      return $this->ceo__cst_sort_name_dn;
    }

    /**
     * @param stringLength150_Type $ceo__cst_sort_name_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_sort_name_dn($ceo__cst_sort_name_dn)
    {
      $this->ceo__cst_sort_name_dn = $ceo__cst_sort_name_dn;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getCeo__cst_ind_full_name_dn()
    {
      return $this->ceo__cst_ind_full_name_dn;
    }

    /**
     * @param stringLength150_Type $ceo__cst_ind_full_name_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_ind_full_name_dn($ceo__cst_ind_full_name_dn)
    {
      $this->ceo__cst_ind_full_name_dn = $ceo__cst_ind_full_name_dn;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getCeo__cst_org_name_dn()
    {
      return $this->ceo__cst_org_name_dn;
    }

    /**
     * @param stringLength150_Type $ceo__cst_org_name_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_org_name_dn($ceo__cst_org_name_dn)
    {
      $this->ceo__cst_org_name_dn = $ceo__cst_org_name_dn;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getCeo__cst_ixo_title_dn()
    {
      return $this->ceo__cst_ixo_title_dn;
    }

    /**
     * @param stringLength150_Type $ceo__cst_ixo_title_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_ixo_title_dn($ceo__cst_ixo_title_dn)
    {
      $this->ceo__cst_ixo_title_dn = $ceo__cst_ixo_title_dn;
      return $this;
    }

    /**
     * @return stringLength10_Type
     */
    public function getCeo__cst_pref_comm_meth()
    {
      return $this->ceo__cst_pref_comm_meth;
    }

    /**
     * @param stringLength10_Type $ceo__cst_pref_comm_meth
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_pref_comm_meth($ceo__cst_pref_comm_meth)
    {
      $this->ceo__cst_pref_comm_meth = $ceo__cst_pref_comm_meth;
      return $this;
    }

    /**
     * @return av_text_Type
     */
    public function getCeo__cst_bio()
    {
      return $this->ceo__cst_bio;
    }

    /**
     * @param av_text_Type $ceo__cst_bio
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_bio($ceo__cst_bio)
    {
      $this->ceo__cst_bio = $ceo__cst_bio;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getCeo__cst_add_date()
    {
      return $this->ceo__cst_add_date;
    }

    /**
     * @param av_date_small_Type $ceo__cst_add_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_add_date($ceo__cst_add_date)
    {
      $this->ceo__cst_add_date = $ceo__cst_add_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getCeo__cst_add_user()
    {
      return $this->ceo__cst_add_user;
    }

    /**
     * @param av_user_Type $ceo__cst_add_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_add_user($ceo__cst_add_user)
    {
      $this->ceo__cst_add_user = $ceo__cst_add_user;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getCeo__cst_change_date()
    {
      return $this->ceo__cst_change_date;
    }

    /**
     * @param av_date_small_Type $ceo__cst_change_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_change_date($ceo__cst_change_date)
    {
      $this->ceo__cst_change_date = $ceo__cst_change_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getCeo__cst_change_user()
    {
      return $this->ceo__cst_change_user;
    }

    /**
     * @param av_user_Type $ceo__cst_change_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_change_user($ceo__cst_change_user)
    {
      $this->ceo__cst_change_user = $ceo__cst_change_user;
      return $this;
    }

    /**
     * @return av_delete_flag_Type
     */
    public function getCeo__cst_delete_flag()
    {
      return $this->ceo__cst_delete_flag;
    }

    /**
     * @param av_delete_flag_Type $ceo__cst_delete_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_delete_flag($ceo__cst_delete_flag)
    {
      $this->ceo__cst_delete_flag = $ceo__cst_delete_flag;
      return $this;
    }

    /**
     * @return av_recno_Type
     */
    public function getCeo__cst_recno()
    {
      return $this->ceo__cst_recno;
    }

    /**
     * @param av_recno_Type $ceo__cst_recno
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_recno($ceo__cst_recno)
    {
      $this->ceo__cst_recno = $ceo__cst_recno;
      return $this;
    }

    /**
     * @return stringLength10_Type
     */
    public function getCeo__cst_id()
    {
      return $this->ceo__cst_id;
    }

    /**
     * @param stringLength10_Type $ceo__cst_id
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_id($ceo__cst_id)
    {
      $this->ceo__cst_id = $ceo__cst_id;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCeo__cst_key_ext()
    {
      return $this->ceo__cst_key_ext;
    }

    /**
     * @param av_key_Type $ceo__cst_key_ext
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_key_ext($ceo__cst_key_ext)
    {
      $this->ceo__cst_key_ext = $ceo__cst_key_ext;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCeo__cst_email_text_only()
    {
      return $this->ceo__cst_email_text_only;
    }

    /**
     * @param av_flag_Type $ceo__cst_email_text_only
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_email_text_only($ceo__cst_email_text_only)
    {
      $this->ceo__cst_email_text_only = $ceo__cst_email_text_only;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getCeo__cst_credit_limit()
    {
      return $this->ceo__cst_credit_limit;
    }

    /**
     * @param av_currency_Type $ceo__cst_credit_limit
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_credit_limit($ceo__cst_credit_limit)
    {
      $this->ceo__cst_credit_limit = $ceo__cst_credit_limit;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCeo__cst_src_key()
    {
      return $this->ceo__cst_src_key;
    }

    /**
     * @param av_key_Type $ceo__cst_src_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_src_key($ceo__cst_src_key)
    {
      $this->ceo__cst_src_key = $ceo__cst_src_key;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getCeo__cst_src_code()
    {
      return $this->ceo__cst_src_code;
    }

    /**
     * @param stringLength50_Type $ceo__cst_src_code
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_src_code($ceo__cst_src_code)
    {
      $this->ceo__cst_src_code = $ceo__cst_src_code;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCeo__cst_tax_exempt_flag()
    {
      return $this->ceo__cst_tax_exempt_flag;
    }

    /**
     * @param av_flag_Type $ceo__cst_tax_exempt_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_tax_exempt_flag($ceo__cst_tax_exempt_flag)
    {
      $this->ceo__cst_tax_exempt_flag = $ceo__cst_tax_exempt_flag;
      return $this;
    }

    /**
     * @return stringLength30_Type
     */
    public function getCeo__cst_tax_id()
    {
      return $this->ceo__cst_tax_id;
    }

    /**
     * @param stringLength30_Type $ceo__cst_tax_id
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_tax_id($ceo__cst_tax_id)
    {
      $this->ceo__cst_tax_id = $ceo__cst_tax_id;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCeo__cst_cxa_key()
    {
      return $this->ceo__cst_cxa_key;
    }

    /**
     * @param av_key_Type $ceo__cst_cxa_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_cxa_key($ceo__cst_cxa_key)
    {
      $this->ceo__cst_cxa_key = $ceo__cst_cxa_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCeo__cst_no_email_flag()
    {
      return $this->ceo__cst_no_email_flag;
    }

    /**
     * @param av_flag_Type $ceo__cst_no_email_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_no_email_flag($ceo__cst_no_email_flag)
    {
      $this->ceo__cst_no_email_flag = $ceo__cst_no_email_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCeo__cst_cxa_billing_key()
    {
      return $this->ceo__cst_cxa_billing_key;
    }

    /**
     * @param av_key_Type $ceo__cst_cxa_billing_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_cxa_billing_key($ceo__cst_cxa_billing_key)
    {
      $this->ceo__cst_cxa_billing_key = $ceo__cst_cxa_billing_key;
      return $this;
    }

    /**
     * @return av_email_Type
     */
    public function getCeo__cst_eml_address_dn()
    {
      return $this->ceo__cst_eml_address_dn;
    }

    /**
     * @param av_email_Type $ceo__cst_eml_address_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_eml_address_dn($ceo__cst_eml_address_dn)
    {
      $this->ceo__cst_eml_address_dn = $ceo__cst_eml_address_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCeo__cst_eml_key()
    {
      return $this->ceo__cst_eml_key;
    }

    /**
     * @param av_key_Type $ceo__cst_eml_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_eml_key($ceo__cst_eml_key)
    {
      $this->ceo__cst_eml_key = $ceo__cst_eml_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCeo__cst_no_phone_flag()
    {
      return $this->ceo__cst_no_phone_flag;
    }

    /**
     * @param av_flag_Type $ceo__cst_no_phone_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_no_phone_flag($ceo__cst_no_phone_flag)
    {
      $this->ceo__cst_no_phone_flag = $ceo__cst_no_phone_flag;
      return $this;
    }

    /**
     * @return stringLength55_Type
     */
    public function getCeo__cst_phn_number_complete_dn()
    {
      return $this->ceo__cst_phn_number_complete_dn;
    }

    /**
     * @param stringLength55_Type $ceo__cst_phn_number_complete_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_phn_number_complete_dn($ceo__cst_phn_number_complete_dn)
    {
      $this->ceo__cst_phn_number_complete_dn = $ceo__cst_phn_number_complete_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCeo__cst_cph_key()
    {
      return $this->ceo__cst_cph_key;
    }

    /**
     * @param av_key_Type $ceo__cst_cph_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_cph_key($ceo__cst_cph_key)
    {
      $this->ceo__cst_cph_key = $ceo__cst_cph_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCeo__cst_no_fax_flag()
    {
      return $this->ceo__cst_no_fax_flag;
    }

    /**
     * @param av_flag_Type $ceo__cst_no_fax_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_no_fax_flag($ceo__cst_no_fax_flag)
    {
      $this->ceo__cst_no_fax_flag = $ceo__cst_no_fax_flag;
      return $this;
    }

    /**
     * @return stringLength55_Type
     */
    public function getCeo__cst_fax_number_complete_dn()
    {
      return $this->ceo__cst_fax_number_complete_dn;
    }

    /**
     * @param stringLength55_Type $ceo__cst_fax_number_complete_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_fax_number_complete_dn($ceo__cst_fax_number_complete_dn)
    {
      $this->ceo__cst_fax_number_complete_dn = $ceo__cst_fax_number_complete_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCeo__cst_cfx_key()
    {
      return $this->ceo__cst_cfx_key;
    }

    /**
     * @param av_key_Type $ceo__cst_cfx_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_cfx_key($ceo__cst_cfx_key)
    {
      $this->ceo__cst_cfx_key = $ceo__cst_cfx_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCeo__cst_ixo_key()
    {
      return $this->ceo__cst_ixo_key;
    }

    /**
     * @param av_key_Type $ceo__cst_ixo_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_ixo_key($ceo__cst_ixo_key)
    {
      $this->ceo__cst_ixo_key = $ceo__cst_ixo_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCeo__cst_no_web_flag()
    {
      return $this->ceo__cst_no_web_flag;
    }

    /**
     * @param av_flag_Type $ceo__cst_no_web_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_no_web_flag($ceo__cst_no_web_flag)
    {
      $this->ceo__cst_no_web_flag = $ceo__cst_no_web_flag;
      return $this;
    }

    /**
     * @return stringLength15_Type
     */
    public function getCeo__cst_oldid()
    {
      return $this->ceo__cst_oldid;
    }

    /**
     * @param stringLength15_Type $ceo__cst_oldid
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_oldid($ceo__cst_oldid)
    {
      $this->ceo__cst_oldid = $ceo__cst_oldid;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCeo__cst_member_flag()
    {
      return $this->ceo__cst_member_flag;
    }

    /**
     * @param av_flag_Type $ceo__cst_member_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_member_flag($ceo__cst_member_flag)
    {
      $this->ceo__cst_member_flag = $ceo__cst_member_flag;
      return $this;
    }

    /**
     * @return av_url_Type
     */
    public function getCeo__cst_url_code_dn()
    {
      return $this->ceo__cst_url_code_dn;
    }

    /**
     * @param av_url_Type $ceo__cst_url_code_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_url_code_dn($ceo__cst_url_code_dn)
    {
      $this->ceo__cst_url_code_dn = $ceo__cst_url_code_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCeo__cst_parent_cst_key()
    {
      return $this->ceo__cst_parent_cst_key;
    }

    /**
     * @param av_key_Type $ceo__cst_parent_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_parent_cst_key($ceo__cst_parent_cst_key)
    {
      $this->ceo__cst_parent_cst_key = $ceo__cst_parent_cst_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCeo__cst_url_key()
    {
      return $this->ceo__cst_url_key;
    }

    /**
     * @param av_key_Type $ceo__cst_url_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_url_key($ceo__cst_url_key)
    {
      $this->ceo__cst_url_key = $ceo__cst_url_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCeo__cst_no_msg_flag()
    {
      return $this->ceo__cst_no_msg_flag;
    }

    /**
     * @param av_flag_Type $ceo__cst_no_msg_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_no_msg_flag($ceo__cst_no_msg_flag)
    {
      $this->ceo__cst_no_msg_flag = $ceo__cst_no_msg_flag;
      return $this;
    }

    /**
     * @return av_messaging_name_Type
     */
    public function getCeo__cst_msg_handle_dn()
    {
      return $this->ceo__cst_msg_handle_dn;
    }

    /**
     * @param av_messaging_name_Type $ceo__cst_msg_handle_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_msg_handle_dn($ceo__cst_msg_handle_dn)
    {
      $this->ceo__cst_msg_handle_dn = $ceo__cst_msg_handle_dn;
      return $this;
    }

    /**
     * @return stringLength80_Type
     */
    public function getCeo__cst_web_login()
    {
      return $this->ceo__cst_web_login;
    }

    /**
     * @param stringLength80_Type $ceo__cst_web_login
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_web_login($ceo__cst_web_login)
    {
      $this->ceo__cst_web_login = $ceo__cst_web_login;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getCeo__cst_web_password()
    {
      return $this->ceo__cst_web_password;
    }

    /**
     * @param stringLength50_Type $ceo__cst_web_password
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_web_password($ceo__cst_web_password)
    {
      $this->ceo__cst_web_password = $ceo__cst_web_password;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCeo__cst_entity_key()
    {
      return $this->ceo__cst_entity_key;
    }

    /**
     * @param av_key_Type $ceo__cst_entity_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_entity_key($ceo__cst_entity_key)
    {
      $this->ceo__cst_entity_key = $ceo__cst_entity_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCeo__cst_msg_key()
    {
      return $this->ceo__cst_msg_key;
    }

    /**
     * @param av_key_Type $ceo__cst_msg_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_msg_key($ceo__cst_msg_key)
    {
      $this->ceo__cst_msg_key = $ceo__cst_msg_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCeo__cst_no_mail_flag()
    {
      return $this->ceo__cst_no_mail_flag;
    }

    /**
     * @param av_flag_Type $ceo__cst_no_mail_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_no_mail_flag($ceo__cst_no_mail_flag)
    {
      $this->ceo__cst_no_mail_flag = $ceo__cst_no_mail_flag;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getCeo__cst_web_start_date()
    {
      return $this->ceo__cst_web_start_date;
    }

    /**
     * @param av_date_small_Type $ceo__cst_web_start_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_web_start_date($ceo__cst_web_start_date)
    {
      $this->ceo__cst_web_start_date = $ceo__cst_web_start_date;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getCeo__cst_web_end_date()
    {
      return $this->ceo__cst_web_end_date;
    }

    /**
     * @param av_date_small_Type $ceo__cst_web_end_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_web_end_date($ceo__cst_web_end_date)
    {
      $this->ceo__cst_web_end_date = $ceo__cst_web_end_date;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCeo__cst_web_force_password_change()
    {
      return $this->ceo__cst_web_force_password_change;
    }

    /**
     * @param av_flag_Type $ceo__cst_web_force_password_change
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_web_force_password_change($ceo__cst_web_force_password_change)
    {
      $this->ceo__cst_web_force_password_change = $ceo__cst_web_force_password_change;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCeo__cst_web_login_disabled_flag()
    {
      return $this->ceo__cst_web_login_disabled_flag;
    }

    /**
     * @param av_flag_Type $ceo__cst_web_login_disabled_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_web_login_disabled_flag($ceo__cst_web_login_disabled_flag)
    {
      $this->ceo__cst_web_login_disabled_flag = $ceo__cst_web_login_disabled_flag;
      return $this;
    }

    /**
     * @return av_text_Type
     */
    public function getCeo__cst_comment()
    {
      return $this->ceo__cst_comment;
    }

    /**
     * @param av_text_Type $ceo__cst_comment
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_comment($ceo__cst_comment)
    {
      $this->ceo__cst_comment = $ceo__cst_comment;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCeo__cst_credit_hold_flag()
    {
      return $this->ceo__cst_credit_hold_flag;
    }

    /**
     * @param av_flag_Type $ceo__cst_credit_hold_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_credit_hold_flag($ceo__cst_credit_hold_flag)
    {
      $this->ceo__cst_credit_hold_flag = $ceo__cst_credit_hold_flag;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getCeo__cst_credit_hold_reason()
    {
      return $this->ceo__cst_credit_hold_reason;
    }

    /**
     * @param stringLength50_Type $ceo__cst_credit_hold_reason
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_credit_hold_reason($ceo__cst_credit_hold_reason)
    {
      $this->ceo__cst_credit_hold_reason = $ceo__cst_credit_hold_reason;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCeo__cst_web_forgot_password_status()
    {
      return $this->ceo__cst_web_forgot_password_status;
    }

    /**
     * @param av_flag_Type $ceo__cst_web_forgot_password_status
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_web_forgot_password_status($ceo__cst_web_forgot_password_status)
    {
      $this->ceo__cst_web_forgot_password_status = $ceo__cst_web_forgot_password_status;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCeo__cst_old_cxa_key()
    {
      return $this->ceo__cst_old_cxa_key;
    }

    /**
     * @param av_key_Type $ceo__cst_old_cxa_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_old_cxa_key($ceo__cst_old_cxa_key)
    {
      $this->ceo__cst_old_cxa_key = $ceo__cst_old_cxa_key;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getCeo__cst_last_email_date()
    {
      return $this->ceo__cst_last_email_date;
    }

    /**
     * @param av_date_small_Type $ceo__cst_last_email_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_last_email_date($ceo__cst_last_email_date)
    {
      $this->ceo__cst_last_email_date = $ceo__cst_last_email_date;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCeo__cst_no_publish_flag()
    {
      return $this->ceo__cst_no_publish_flag;
    }

    /**
     * @param av_flag_Type $ceo__cst_no_publish_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_no_publish_flag($ceo__cst_no_publish_flag)
    {
      $this->ceo__cst_no_publish_flag = $ceo__cst_no_publish_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCeo__cst_sin_key()
    {
      return $this->ceo__cst_sin_key;
    }

    /**
     * @param av_key_Type $ceo__cst_sin_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_sin_key($ceo__cst_sin_key)
    {
      $this->ceo__cst_sin_key = $ceo__cst_sin_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCeo__cst_ttl_key()
    {
      return $this->ceo__cst_ttl_key;
    }

    /**
     * @param av_key_Type $ceo__cst_ttl_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_ttl_key($ceo__cst_ttl_key)
    {
      $this->ceo__cst_ttl_key = $ceo__cst_ttl_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCeo__cst_jfn_key()
    {
      return $this->ceo__cst_jfn_key;
    }

    /**
     * @param av_key_Type $ceo__cst_jfn_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_jfn_key($ceo__cst_jfn_key)
    {
      $this->ceo__cst_jfn_key = $ceo__cst_jfn_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCeo__cst_cur_key()
    {
      return $this->ceo__cst_cur_key;
    }

    /**
     * @param av_key_Type $ceo__cst_cur_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_cur_key($ceo__cst_cur_key)
    {
      $this->ceo__cst_cur_key = $ceo__cst_cur_key;
      return $this;
    }

    /**
     * @return stringLength510_Type
     */
    public function getCeo__cst_attribute_1()
    {
      return $this->ceo__cst_attribute_1;
    }

    /**
     * @param stringLength510_Type $ceo__cst_attribute_1
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_attribute_1($ceo__cst_attribute_1)
    {
      $this->ceo__cst_attribute_1 = $ceo__cst_attribute_1;
      return $this;
    }

    /**
     * @return stringLength510_Type
     */
    public function getCeo__cst_attribute_2()
    {
      return $this->ceo__cst_attribute_2;
    }

    /**
     * @param stringLength510_Type $ceo__cst_attribute_2
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_attribute_2($ceo__cst_attribute_2)
    {
      $this->ceo__cst_attribute_2 = $ceo__cst_attribute_2;
      return $this;
    }

    /**
     * @return stringLength100_Type
     */
    public function getCeo__cst_salutation_1()
    {
      return $this->ceo__cst_salutation_1;
    }

    /**
     * @param stringLength100_Type $ceo__cst_salutation_1
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_salutation_1($ceo__cst_salutation_1)
    {
      $this->ceo__cst_salutation_1 = $ceo__cst_salutation_1;
      return $this;
    }

    /**
     * @return stringLength100_Type
     */
    public function getCeo__cst_salutation_2()
    {
      return $this->ceo__cst_salutation_2;
    }

    /**
     * @param stringLength100_Type $ceo__cst_salutation_2
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_salutation_2($ceo__cst_salutation_2)
    {
      $this->ceo__cst_salutation_2 = $ceo__cst_salutation_2;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCeo__cst_merge_cst_key()
    {
      return $this->ceo__cst_merge_cst_key;
    }

    /**
     * @param av_key_Type $ceo__cst_merge_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_merge_cst_key($ceo__cst_merge_cst_key)
    {
      $this->ceo__cst_merge_cst_key = $ceo__cst_merge_cst_key;
      return $this;
    }

    /**
     * @return stringLength100_Type
     */
    public function getCeo__cst_salutation_3()
    {
      return $this->ceo__cst_salutation_3;
    }

    /**
     * @param stringLength100_Type $ceo__cst_salutation_3
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_salutation_3($ceo__cst_salutation_3)
    {
      $this->ceo__cst_salutation_3 = $ceo__cst_salutation_3;
      return $this;
    }

    /**
     * @return stringLength100_Type
     */
    public function getCeo__cst_salutation_4()
    {
      return $this->ceo__cst_salutation_4;
    }

    /**
     * @param stringLength100_Type $ceo__cst_salutation_4
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_salutation_4($ceo__cst_salutation_4)
    {
      $this->ceo__cst_salutation_4 = $ceo__cst_salutation_4;
      return $this;
    }

    /**
     * @return stringLength200_Type
     */
    public function getCeo__cst_default_recognize_as()
    {
      return $this->ceo__cst_default_recognize_as;
    }

    /**
     * @param stringLength200_Type $ceo__cst_default_recognize_as
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_default_recognize_as($ceo__cst_default_recognize_as)
    {
      $this->ceo__cst_default_recognize_as = $ceo__cst_default_recognize_as;
      return $this;
    }

    /**
     * @return av_decimal4_Type
     */
    public function getCeo__cst_score()
    {
      return $this->ceo__cst_score;
    }

    /**
     * @param av_decimal4_Type $ceo__cst_score
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_score($ceo__cst_score)
    {
      $this->ceo__cst_score = $ceo__cst_score;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getCeo__cst_score_normalized()
    {
      return $this->ceo__cst_score_normalized;
    }

    /**
     * @param av_integer_Type $ceo__cst_score_normalized
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_score_normalized($ceo__cst_score_normalized)
    {
      $this->ceo__cst_score_normalized = $ceo__cst_score_normalized;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getCeo__cst_score_trend()
    {
      return $this->ceo__cst_score_trend;
    }

    /**
     * @param av_integer_Type $ceo__cst_score_trend
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_score_trend($ceo__cst_score_trend)
    {
      $this->ceo__cst_score_trend = $ceo__cst_score_trend;
      return $this;
    }

    /**
     * @return stringLength25_Type
     */
    public function getCeo__cst_vault_account()
    {
      return $this->ceo__cst_vault_account;
    }

    /**
     * @param stringLength25_Type $ceo__cst_vault_account
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_vault_account($ceo__cst_vault_account)
    {
      $this->ceo__cst_vault_account = $ceo__cst_vault_account;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCeo__cst_exclude_from_social_flag()
    {
      return $this->ceo__cst_exclude_from_social_flag;
    }

    /**
     * @param av_flag_Type $ceo__cst_exclude_from_social_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_exclude_from_social_flag($ceo__cst_exclude_from_social_flag)
    {
      $this->ceo__cst_exclude_from_social_flag = $ceo__cst_exclude_from_social_flag;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getCeo__cst_social_score()
    {
      return $this->ceo__cst_social_score;
    }

    /**
     * @param av_integer_Type $ceo__cst_social_score
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_social_score($ceo__cst_social_score)
    {
      $this->ceo__cst_social_score = $ceo__cst_social_score;
      return $this;
    }

    /**
     * @return stringLength9_Type
     */
    public function getCeo__cst_ptin()
    {
      return $this->ceo__cst_ptin;
    }

    /**
     * @param stringLength9_Type $ceo__cst_ptin
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_ptin($ceo__cst_ptin)
    {
      $this->ceo__cst_ptin = $ceo__cst_ptin;
      return $this;
    }

    /**
     * @return av_recno_Type
     */
    public function getCeo__cst_aicpa_member_id()
    {
      return $this->ceo__cst_aicpa_member_id;
    }

    /**
     * @param av_recno_Type $ceo__cst_aicpa_member_id
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_aicpa_member_id($ceo__cst_aicpa_member_id)
    {
      $this->ceo__cst_aicpa_member_id = $ceo__cst_aicpa_member_id;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getCeo__cst_vendor_code()
    {
      return $this->ceo__cst_vendor_code;
    }

    /**
     * @param stringLength50_Type $ceo__cst_vendor_code
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_vendor_code($ceo__cst_vendor_code)
    {
      $this->ceo__cst_vendor_code = $ceo__cst_vendor_code;
      return $this;
    }

    /**
     * @return stringLength1_Type
     */
    public function getCeo__cst_salt()
    {
      return $this->ceo__cst_salt;
    }

    /**
     * @param stringLength1_Type $ceo__cst_salt
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_salt($ceo__cst_salt)
    {
      $this->ceo__cst_salt = $ceo__cst_salt;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCeo__cst_sca_key()
    {
      return $this->ceo__cst_sca_key;
    }

    /**
     * @param av_key_Type $ceo__cst_sca_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_sca_key($ceo__cst_sca_key)
    {
      $this->ceo__cst_sca_key = $ceo__cst_sca_key;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getCeo__cst_iterations()
    {
      return $this->ceo__cst_iterations;
    }

    /**
     * @param av_integer_Type $ceo__cst_iterations
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CEO_DataObjectType
     */
    public function setCeo__cst_iterations($ceo__cst_iterations)
    {
      $this->ceo__cst_iterations = $ceo__cst_iterations;
      return $this;
    }

}
