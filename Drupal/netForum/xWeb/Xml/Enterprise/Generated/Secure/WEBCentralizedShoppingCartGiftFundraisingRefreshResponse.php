<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGiftFundraisingRefreshResponse
{

    /**
     * @var FundraisingGiftType $WEBCentralizedShoppingCartGiftFundraisingRefreshResult
     */
    protected $WEBCentralizedShoppingCartGiftFundraisingRefreshResult = null;

    /**
     * @param FundraisingGiftType $WEBCentralizedShoppingCartGiftFundraisingRefreshResult
     */
    public function __construct($WEBCentralizedShoppingCartGiftFundraisingRefreshResult)
    {
      $this->WEBCentralizedShoppingCartGiftFundraisingRefreshResult = $WEBCentralizedShoppingCartGiftFundraisingRefreshResult;
    }

    /**
     * @return FundraisingGiftType
     */
    public function getWEBCentralizedShoppingCartGiftFundraisingRefreshResult()
    {
      return $this->WEBCentralizedShoppingCartGiftFundraisingRefreshResult;
    }

    /**
     * @param FundraisingGiftType $WEBCentralizedShoppingCartGiftFundraisingRefreshResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGiftFundraisingRefreshResponse
     */
    public function setWEBCentralizedShoppingCartGiftFundraisingRefreshResult($WEBCentralizedShoppingCartGiftFundraisingRefreshResult)
    {
      $this->WEBCentralizedShoppingCartGiftFundraisingRefreshResult = $WEBCentralizedShoppingCartGiftFundraisingRefreshResult;
      return $this;
    }

}
