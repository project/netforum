<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetActionSubTypeListResponse
{

    /**
     * @var GetActionSubTypeListResult $GetActionSubTypeListResult
     */
    protected $GetActionSubTypeListResult = null;

    /**
     * @param GetActionSubTypeListResult $GetActionSubTypeListResult
     */
    public function __construct($GetActionSubTypeListResult)
    {
      $this->GetActionSubTypeListResult = $GetActionSubTypeListResult;
    }

    /**
     * @return GetActionSubTypeListResult
     */
    public function getGetActionSubTypeListResult()
    {
      return $this->GetActionSubTypeListResult;
    }

    /**
     * @param GetActionSubTypeListResult $GetActionSubTypeListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetActionSubTypeListResponse
     */
    public function setGetActionSubTypeListResult($GetActionSubTypeListResult)
    {
      $this->GetActionSubTypeListResult = $GetActionSubTypeListResult;
      return $this;
    }

}
