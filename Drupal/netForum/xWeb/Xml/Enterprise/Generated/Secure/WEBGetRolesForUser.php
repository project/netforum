<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBGetRolesForUser
{

    /**
     * @var string $username
     */
    protected $username = null;

    /**
     * @param string $username
     */
    public function __construct($username)
    {
      $this->username = $username;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
      return $this->username;
    }

    /**
     * @param string $username
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBGetRolesForUser
     */
    public function setUsername($username)
    {
      $this->username = $username;
      return $this;
    }

}
