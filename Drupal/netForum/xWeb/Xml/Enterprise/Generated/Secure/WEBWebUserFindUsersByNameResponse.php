<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserFindUsersByNameResponse
{

    /**
     * @var WEBWebUserFindUsersByNameResult $WEBWebUserFindUsersByNameResult
     */
    protected $WEBWebUserFindUsersByNameResult = null;

    /**
     * @param WEBWebUserFindUsersByNameResult $WEBWebUserFindUsersByNameResult
     */
    public function __construct($WEBWebUserFindUsersByNameResult)
    {
      $this->WEBWebUserFindUsersByNameResult = $WEBWebUserFindUsersByNameResult;
    }

    /**
     * @return WEBWebUserFindUsersByNameResult
     */
    public function getWEBWebUserFindUsersByNameResult()
    {
      return $this->WEBWebUserFindUsersByNameResult;
    }

    /**
     * @param WEBWebUserFindUsersByNameResult $WEBWebUserFindUsersByNameResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserFindUsersByNameResponse
     */
    public function setWEBWebUserFindUsersByNameResult($WEBWebUserFindUsersByNameResult)
    {
      $this->WEBWebUserFindUsersByNameResult = $WEBWebUserFindUsersByNameResult;
      return $this;
    }

}
