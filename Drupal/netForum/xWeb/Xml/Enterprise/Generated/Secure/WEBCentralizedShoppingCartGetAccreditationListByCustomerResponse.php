<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetAccreditationListByCustomerResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetAccreditationListByCustomerResult $WEBCentralizedShoppingCartGetAccreditationListByCustomerResult
     */
    protected $WEBCentralizedShoppingCartGetAccreditationListByCustomerResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetAccreditationListByCustomerResult $WEBCentralizedShoppingCartGetAccreditationListByCustomerResult
     */
    public function __construct($WEBCentralizedShoppingCartGetAccreditationListByCustomerResult)
    {
      $this->WEBCentralizedShoppingCartGetAccreditationListByCustomerResult = $WEBCentralizedShoppingCartGetAccreditationListByCustomerResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetAccreditationListByCustomerResult
     */
    public function getWEBCentralizedShoppingCartGetAccreditationListByCustomerResult()
    {
      return $this->WEBCentralizedShoppingCartGetAccreditationListByCustomerResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetAccreditationListByCustomerResult $WEBCentralizedShoppingCartGetAccreditationListByCustomerResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetAccreditationListByCustomerResponse
     */
    public function setWEBCentralizedShoppingCartGetAccreditationListByCustomerResult($WEBCentralizedShoppingCartGetAccreditationListByCustomerResult)
    {
      $this->WEBCentralizedShoppingCartGetAccreditationListByCustomerResult = $WEBCentralizedShoppingCartGetAccreditationListByCustomerResult;
      return $this;
    }

}
