<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType
{

    /**
     * @var av_key_Type $exo__ixo_key
     */
    protected $exo__ixo_key = null;

    /**
     * @var stringLength80_Type $exo__ixo_rlt_code
     */
    protected $exo__ixo_rlt_code = null;

    /**
     * @var stringLength150_Type $exo__ixo_title
     */
    protected $exo__ixo_title = null;

    /**
     * @var av_date_small_Type $exo__ixo_start_date
     */
    protected $exo__ixo_start_date = null;

    /**
     * @var av_date_small_Type $exo__ixo_end_date
     */
    protected $exo__ixo_end_date = null;

    /**
     * @var av_key_Type $exo__ixo_ind_cst_key
     */
    protected $exo__ixo_ind_cst_key = null;

    /**
     * @var av_key_Type $exo__ixo_org_cst_key
     */
    protected $exo__ixo_org_cst_key = null;

    /**
     * @var av_date_small_Type $exo__ixo_add_date
     */
    protected $exo__ixo_add_date = null;

    /**
     * @var av_user_Type $exo__ixo_add_user
     */
    protected $exo__ixo_add_user = null;

    /**
     * @var av_date_small_Type $exo__ixo_change_date
     */
    protected $exo__ixo_change_date = null;

    /**
     * @var av_user_Type $exo__ixo_change_user
     */
    protected $exo__ixo_change_user = null;

    /**
     * @var av_delete_flag_Type $exo__ixo_delete_flag
     */
    protected $exo__ixo_delete_flag = null;

    /**
     * @var av_key_Type $exo__ixo_key_ext
     */
    protected $exo__ixo_key_ext = null;

    /**
     * @var av_key_Type $exo__ixo_cst_key_owner
     */
    protected $exo__ixo_cst_key_owner = null;

    /**
     * @var av_key_Type $exo__ixo_entity_key
     */
    protected $exo__ixo_entity_key = null;

    /**
     * @var av_flag_Type $exo__ixo_void_flag
     */
    protected $exo__ixo_void_flag = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getExo__ixo_key()
    {
      return $this->exo__ixo_key;
    }

    /**
     * @param av_key_Type $exo__ixo_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType
     */
    public function setExo__ixo_key($exo__ixo_key)
    {
      $this->exo__ixo_key = $exo__ixo_key;
      return $this;
    }

    /**
     * @return stringLength80_Type
     */
    public function getExo__ixo_rlt_code()
    {
      return $this->exo__ixo_rlt_code;
    }

    /**
     * @param stringLength80_Type $exo__ixo_rlt_code
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType
     */
    public function setExo__ixo_rlt_code($exo__ixo_rlt_code)
    {
      $this->exo__ixo_rlt_code = $exo__ixo_rlt_code;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getExo__ixo_title()
    {
      return $this->exo__ixo_title;
    }

    /**
     * @param stringLength150_Type $exo__ixo_title
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType
     */
    public function setExo__ixo_title($exo__ixo_title)
    {
      $this->exo__ixo_title = $exo__ixo_title;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getExo__ixo_start_date()
    {
      return $this->exo__ixo_start_date;
    }

    /**
     * @param av_date_small_Type $exo__ixo_start_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType
     */
    public function setExo__ixo_start_date($exo__ixo_start_date)
    {
      $this->exo__ixo_start_date = $exo__ixo_start_date;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getExo__ixo_end_date()
    {
      return $this->exo__ixo_end_date;
    }

    /**
     * @param av_date_small_Type $exo__ixo_end_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType
     */
    public function setExo__ixo_end_date($exo__ixo_end_date)
    {
      $this->exo__ixo_end_date = $exo__ixo_end_date;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getExo__ixo_ind_cst_key()
    {
      return $this->exo__ixo_ind_cst_key;
    }

    /**
     * @param av_key_Type $exo__ixo_ind_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType
     */
    public function setExo__ixo_ind_cst_key($exo__ixo_ind_cst_key)
    {
      $this->exo__ixo_ind_cst_key = $exo__ixo_ind_cst_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getExo__ixo_org_cst_key()
    {
      return $this->exo__ixo_org_cst_key;
    }

    /**
     * @param av_key_Type $exo__ixo_org_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType
     */
    public function setExo__ixo_org_cst_key($exo__ixo_org_cst_key)
    {
      $this->exo__ixo_org_cst_key = $exo__ixo_org_cst_key;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getExo__ixo_add_date()
    {
      return $this->exo__ixo_add_date;
    }

    /**
     * @param av_date_small_Type $exo__ixo_add_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType
     */
    public function setExo__ixo_add_date($exo__ixo_add_date)
    {
      $this->exo__ixo_add_date = $exo__ixo_add_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getExo__ixo_add_user()
    {
      return $this->exo__ixo_add_user;
    }

    /**
     * @param av_user_Type $exo__ixo_add_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType
     */
    public function setExo__ixo_add_user($exo__ixo_add_user)
    {
      $this->exo__ixo_add_user = $exo__ixo_add_user;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getExo__ixo_change_date()
    {
      return $this->exo__ixo_change_date;
    }

    /**
     * @param av_date_small_Type $exo__ixo_change_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType
     */
    public function setExo__ixo_change_date($exo__ixo_change_date)
    {
      $this->exo__ixo_change_date = $exo__ixo_change_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getExo__ixo_change_user()
    {
      return $this->exo__ixo_change_user;
    }

    /**
     * @param av_user_Type $exo__ixo_change_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType
     */
    public function setExo__ixo_change_user($exo__ixo_change_user)
    {
      $this->exo__ixo_change_user = $exo__ixo_change_user;
      return $this;
    }

    /**
     * @return av_delete_flag_Type
     */
    public function getExo__ixo_delete_flag()
    {
      return $this->exo__ixo_delete_flag;
    }

    /**
     * @param av_delete_flag_Type $exo__ixo_delete_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType
     */
    public function setExo__ixo_delete_flag($exo__ixo_delete_flag)
    {
      $this->exo__ixo_delete_flag = $exo__ixo_delete_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getExo__ixo_key_ext()
    {
      return $this->exo__ixo_key_ext;
    }

    /**
     * @param av_key_Type $exo__ixo_key_ext
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType
     */
    public function setExo__ixo_key_ext($exo__ixo_key_ext)
    {
      $this->exo__ixo_key_ext = $exo__ixo_key_ext;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getExo__ixo_cst_key_owner()
    {
      return $this->exo__ixo_cst_key_owner;
    }

    /**
     * @param av_key_Type $exo__ixo_cst_key_owner
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType
     */
    public function setExo__ixo_cst_key_owner($exo__ixo_cst_key_owner)
    {
      $this->exo__ixo_cst_key_owner = $exo__ixo_cst_key_owner;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getExo__ixo_entity_key()
    {
      return $this->exo__ixo_entity_key;
    }

    /**
     * @param av_key_Type $exo__ixo_entity_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType
     */
    public function setExo__ixo_entity_key($exo__ixo_entity_key)
    {
      $this->exo__ixo_entity_key = $exo__ixo_entity_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getExo__ixo_void_flag()
    {
      return $this->exo__ixo_void_flag;
    }

    /**
     * @param av_flag_Type $exo__ixo_void_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CEO_Affiliation_DataObjectType
     */
    public function setExo__ixo_void_flag($exo__ixo_void_flag)
    {
      $this->exo__ixo_void_flag = $exo__ixo_void_flag;
      return $this;
    }

}
