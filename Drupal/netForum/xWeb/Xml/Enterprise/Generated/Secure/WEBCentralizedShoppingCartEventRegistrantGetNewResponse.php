<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartEventRegistrantGetNewResponse
{

    /**
     * @var EventsRegistrantType $WEBCentralizedShoppingCartEventRegistrantGetNewResult
     */
    protected $WEBCentralizedShoppingCartEventRegistrantGetNewResult = null;

    /**
     * @param EventsRegistrantType $WEBCentralizedShoppingCartEventRegistrantGetNewResult
     */
    public function __construct($WEBCentralizedShoppingCartEventRegistrantGetNewResult)
    {
      $this->WEBCentralizedShoppingCartEventRegistrantGetNewResult = $WEBCentralizedShoppingCartEventRegistrantGetNewResult;
    }

    /**
     * @return EventsRegistrantType
     */
    public function getWEBCentralizedShoppingCartEventRegistrantGetNewResult()
    {
      return $this->WEBCentralizedShoppingCartEventRegistrantGetNewResult;
    }

    /**
     * @param EventsRegistrantType $WEBCentralizedShoppingCartEventRegistrantGetNewResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartEventRegistrantGetNewResponse
     */
    public function setWEBCentralizedShoppingCartEventRegistrantGetNewResult($WEBCentralizedShoppingCartEventRegistrantGetNewResult)
    {
      $this->WEBCentralizedShoppingCartEventRegistrantGetNewResult = $WEBCentralizedShoppingCartEventRegistrantGetNewResult;
      return $this;
    }

}
