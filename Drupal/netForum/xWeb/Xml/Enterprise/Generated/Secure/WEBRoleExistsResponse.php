<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBRoleExistsResponse
{

    /**
     * @var boolean $WEBRoleExistsResult
     */
    protected $WEBRoleExistsResult = null;

    /**
     * @param boolean $WEBRoleExistsResult
     */
    public function __construct($WEBRoleExistsResult)
    {
      $this->WEBRoleExistsResult = $WEBRoleExistsResult;
    }

    /**
     * @return boolean
     */
    public function getWEBRoleExistsResult()
    {
      return $this->WEBRoleExistsResult;
    }

    /**
     * @param boolean $WEBRoleExistsResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBRoleExistsResponse
     */
    public function setWEBRoleExistsResult($WEBRoleExistsResult)
    {
      $this->WEBRoleExistsResult = $WEBRoleExistsResult;
      return $this;
    }

}
