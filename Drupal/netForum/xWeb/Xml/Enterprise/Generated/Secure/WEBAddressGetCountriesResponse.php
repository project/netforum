<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBAddressGetCountriesResponse
{

    /**
     * @var WEBAddressGetCountriesResult $WEBAddressGetCountriesResult
     */
    protected $WEBAddressGetCountriesResult = null;

    /**
     * @param WEBAddressGetCountriesResult $WEBAddressGetCountriesResult
     */
    public function __construct($WEBAddressGetCountriesResult)
    {
      $this->WEBAddressGetCountriesResult = $WEBAddressGetCountriesResult;
    }

    /**
     * @return WEBAddressGetCountriesResult
     */
    public function getWEBAddressGetCountriesResult()
    {
      return $this->WEBAddressGetCountriesResult;
    }

    /**
     * @param WEBAddressGetCountriesResult $WEBAddressGetCountriesResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBAddressGetCountriesResponse
     */
    public function setWEBAddressGetCountriesResult($WEBAddressGetCountriesResult)
    {
      $this->WEBAddressGetCountriesResult = $WEBAddressGetCountriesResult;
      return $this;
    }

}
