<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetProductListKeysResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetProductListKeysResult $WEBCentralizedShoppingCartGetProductListKeysResult
     */
    protected $WEBCentralizedShoppingCartGetProductListKeysResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetProductListKeysResult $WEBCentralizedShoppingCartGetProductListKeysResult
     */
    public function __construct($WEBCentralizedShoppingCartGetProductListKeysResult)
    {
      $this->WEBCentralizedShoppingCartGetProductListKeysResult = $WEBCentralizedShoppingCartGetProductListKeysResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetProductListKeysResult
     */
    public function getWEBCentralizedShoppingCartGetProductListKeysResult()
    {
      return $this->WEBCentralizedShoppingCartGetProductListKeysResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetProductListKeysResult $WEBCentralizedShoppingCartGetProductListKeysResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetProductListKeysResponse
     */
    public function setWEBCentralizedShoppingCartGetProductListKeysResult($WEBCentralizedShoppingCartGetProductListKeysResult)
    {
      $this->WEBCentralizedShoppingCartGetProductListKeysResult = $WEBCentralizedShoppingCartGetProductListKeysResult;
      return $this;
    }

}
