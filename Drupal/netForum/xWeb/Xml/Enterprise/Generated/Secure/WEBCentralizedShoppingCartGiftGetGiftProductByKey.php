<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGiftGetGiftProductByKey
{

    /**
     * @var guid $GiftKey
     */
    protected $GiftKey = null;

    /**
     * @param guid $GiftKey
     */
    public function __construct($GiftKey)
    {
      $this->GiftKey = $GiftKey;
    }

    /**
     * @return guid
     */
    public function getGiftKey()
    {
      return $this->GiftKey;
    }

    /**
     * @param guid $GiftKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGiftGetGiftProductByKey
     */
    public function setGiftKey($GiftKey)
    {
      $this->GiftKey = $GiftKey;
      return $this;
    }

}
