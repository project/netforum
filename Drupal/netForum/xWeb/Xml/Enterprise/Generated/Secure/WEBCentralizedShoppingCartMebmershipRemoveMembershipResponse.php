<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartMebmershipRemoveMembershipResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartMebmershipRemoveMembershipResult
     */
    protected $WEBCentralizedShoppingCartMebmershipRemoveMembershipResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartMebmershipRemoveMembershipResult
     */
    public function __construct($WEBCentralizedShoppingCartMebmershipRemoveMembershipResult)
    {
      $this->WEBCentralizedShoppingCartMebmershipRemoveMembershipResult = $WEBCentralizedShoppingCartMebmershipRemoveMembershipResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartMebmershipRemoveMembershipResult()
    {
      return $this->WEBCentralizedShoppingCartMebmershipRemoveMembershipResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartMebmershipRemoveMembershipResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartMebmershipRemoveMembershipResponse
     */
    public function setWEBCentralizedShoppingCartMebmershipRemoveMembershipResult($WEBCentralizedShoppingCartMebmershipRemoveMembershipResult)
    {
      $this->WEBCentralizedShoppingCartMebmershipRemoveMembershipResult = $WEBCentralizedShoppingCartMebmershipRemoveMembershipResult;
      return $this;
    }

}
