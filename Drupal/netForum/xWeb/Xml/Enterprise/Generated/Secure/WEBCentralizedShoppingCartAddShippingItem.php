<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartAddShippingItem
{

    /**
     * @var CentralizedOrderEntryType $oCentralizedOrderEntry
     */
    protected $oCentralizedOrderEntry = null;

    /**
     * @var InvoiceDetailType $oShippingItem
     */
    protected $oShippingItem = null;

    /**
     * @param CentralizedOrderEntryType $oCentralizedOrderEntry
     * @param InvoiceDetailType $oShippingItem
     */
    public function __construct($oCentralizedOrderEntry, $oShippingItem)
    {
      $this->oCentralizedOrderEntry = $oCentralizedOrderEntry;
      $this->oShippingItem = $oShippingItem;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getOCentralizedOrderEntry()
    {
      return $this->oCentralizedOrderEntry;
    }

    /**
     * @param CentralizedOrderEntryType $oCentralizedOrderEntry
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartAddShippingItem
     */
    public function setOCentralizedOrderEntry($oCentralizedOrderEntry)
    {
      $this->oCentralizedOrderEntry = $oCentralizedOrderEntry;
      return $this;
    }

    /**
     * @return InvoiceDetailType
     */
    public function getOShippingItem()
    {
      return $this->oShippingItem;
    }

    /**
     * @param InvoiceDetailType $oShippingItem
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartAddShippingItem
     */
    public function setOShippingItem($oShippingItem)
    {
      $this->oShippingItem = $oShippingItem;
      return $this;
    }

}
