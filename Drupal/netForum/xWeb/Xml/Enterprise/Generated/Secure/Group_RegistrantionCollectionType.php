<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class Group_RegistrantionCollectionType
{

    /**
     * @var EventsRegistrantGroupType[] $EventsRegistrantGroup
     */
    protected $EventsRegistrantGroup = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return EventsRegistrantGroupType[]
     */
    public function getEventsRegistrantGroup()
    {
      return $this->EventsRegistrantGroup;
    }

    /**
     * @param EventsRegistrantGroupType[] $EventsRegistrantGroup
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\Group_RegistrantionCollectionType
     */
    public function setEventsRegistrantGroup(array $EventsRegistrantGroup = null)
    {
      $this->EventsRegistrantGroup = $EventsRegistrantGroup;
      return $this;
    }

}
