<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserUnlockResponse
{

    /**
     * @var boolean $WEBWebUserUnlockResult
     */
    protected $WEBWebUserUnlockResult = null;

    /**
     * @param boolean $WEBWebUserUnlockResult
     */
    public function __construct($WEBWebUserUnlockResult)
    {
      $this->WEBWebUserUnlockResult = $WEBWebUserUnlockResult;
    }

    /**
     * @return boolean
     */
    public function getWEBWebUserUnlockResult()
    {
      return $this->WEBWebUserUnlockResult;
    }

    /**
     * @param boolean $WEBWebUserUnlockResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserUnlockResponse
     */
    public function setWEBWebUserUnlockResult($WEBWebUserUnlockResult)
    {
      $this->WEBWebUserUnlockResult = $WEBWebUserUnlockResult;
      return $this;
    }

}
