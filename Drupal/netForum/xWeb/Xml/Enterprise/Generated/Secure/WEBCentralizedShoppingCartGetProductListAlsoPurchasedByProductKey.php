<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKey
{

    /**
     * @var guid $ProductKey
     */
    protected $ProductKey = null;

    /**
     * @param guid $ProductKey
     */
    public function __construct($ProductKey)
    {
      $this->ProductKey = $ProductKey;
    }

    /**
     * @return guid
     */
    public function getProductKey()
    {
      return $this->ProductKey;
    }

    /**
     * @param guid $ProductKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetProductListAlsoPurchasedByProductKey
     */
    public function setProductKey($ProductKey)
    {
      $this->ProductKey = $ProductKey;
      return $this;
    }

}
