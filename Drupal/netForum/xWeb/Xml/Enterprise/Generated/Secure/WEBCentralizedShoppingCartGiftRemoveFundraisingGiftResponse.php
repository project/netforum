<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGiftRemoveFundraisingGiftResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartGiftRemoveFundraisingGiftResult
     */
    protected $WEBCentralizedShoppingCartGiftRemoveFundraisingGiftResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartGiftRemoveFundraisingGiftResult
     */
    public function __construct($WEBCentralizedShoppingCartGiftRemoveFundraisingGiftResult)
    {
      $this->WEBCentralizedShoppingCartGiftRemoveFundraisingGiftResult = $WEBCentralizedShoppingCartGiftRemoveFundraisingGiftResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartGiftRemoveFundraisingGiftResult()
    {
      return $this->WEBCentralizedShoppingCartGiftRemoveFundraisingGiftResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartGiftRemoveFundraisingGiftResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGiftRemoveFundraisingGiftResponse
     */
    public function setWEBCentralizedShoppingCartGiftRemoveFundraisingGiftResult($WEBCentralizedShoppingCartGiftRemoveFundraisingGiftResult)
    {
      $this->WEBCentralizedShoppingCartGiftRemoveFundraisingGiftResult = $WEBCentralizedShoppingCartGiftRemoveFundraisingGiftResult;
      return $this;
    }

}
