<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class Exhibitor_BoothCollectionType
{

    /**
     * @var ExhibitorBoothNewType[] $ExhibitorBoothNew
     */
    protected $ExhibitorBoothNew = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ExhibitorBoothNewType[]
     */
    public function getExhibitorBoothNew()
    {
      return $this->ExhibitorBoothNew;
    }

    /**
     * @param ExhibitorBoothNewType[] $ExhibitorBoothNew
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\Exhibitor_BoothCollectionType
     */
    public function setExhibitorBoothNew(array $ExhibitorBoothNew = null)
    {
      $this->ExhibitorBoothNew = $ExhibitorBoothNew;
      return $this;
    }

}
