<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBGetRolesForUserResponse
{

    /**
     * @var WEBGetRolesForUserResult $WEBGetRolesForUserResult
     */
    protected $WEBGetRolesForUserResult = null;

    /**
     * @param WEBGetRolesForUserResult $WEBGetRolesForUserResult
     */
    public function __construct($WEBGetRolesForUserResult)
    {
      $this->WEBGetRolesForUserResult = $WEBGetRolesForUserResult;
    }

    /**
     * @return WEBGetRolesForUserResult
     */
    public function getWEBGetRolesForUserResult()
    {
      return $this->WEBGetRolesForUserResult;
    }

    /**
     * @param WEBGetRolesForUserResult $WEBGetRolesForUserResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBGetRolesForUserResponse
     */
    public function setWEBGetRolesForUserResult($WEBGetRolesForUserResult)
    {
      $this->WEBGetRolesForUserResult = $WEBGetRolesForUserResult;
      return $this;
    }

}
