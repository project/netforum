<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserValidateLoginResponse
{

    /**
     * @var WebUserType $WEBWebUserValidateLoginResult
     */
    protected $WEBWebUserValidateLoginResult = null;

    /**
     * @param WebUserType $WEBWebUserValidateLoginResult
     */
    public function __construct($WEBWebUserValidateLoginResult)
    {
      $this->WEBWebUserValidateLoginResult = $WEBWebUserValidateLoginResult;
    }

    /**
     * @return WebUserType
     */
    public function getWEBWebUserValidateLoginResult()
    {
      return $this->WEBWebUserValidateLoginResult;
    }

    /**
     * @param WebUserType $WEBWebUserValidateLoginResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserValidateLoginResponse
     */
    public function setWEBWebUserValidateLoginResult($WEBWebUserValidateLoginResult)
    {
      $this->WEBWebUserValidateLoginResult = $WEBWebUserValidateLoginResult;
      return $this;
    }

}
