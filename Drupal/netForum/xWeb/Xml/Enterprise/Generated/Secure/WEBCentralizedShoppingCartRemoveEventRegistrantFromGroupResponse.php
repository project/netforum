<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartRemoveEventRegistrantFromGroupResponse
{

    /**
     * @var EventsRegistrantGroupType $WEBCentralizedShoppingCartRemoveEventRegistrantFromGroupResult
     */
    protected $WEBCentralizedShoppingCartRemoveEventRegistrantFromGroupResult = null;

    /**
     * @param EventsRegistrantGroupType $WEBCentralizedShoppingCartRemoveEventRegistrantFromGroupResult
     */
    public function __construct($WEBCentralizedShoppingCartRemoveEventRegistrantFromGroupResult)
    {
      $this->WEBCentralizedShoppingCartRemoveEventRegistrantFromGroupResult = $WEBCentralizedShoppingCartRemoveEventRegistrantFromGroupResult;
    }

    /**
     * @return EventsRegistrantGroupType
     */
    public function getWEBCentralizedShoppingCartRemoveEventRegistrantFromGroupResult()
    {
      return $this->WEBCentralizedShoppingCartRemoveEventRegistrantFromGroupResult;
    }

    /**
     * @param EventsRegistrantGroupType $WEBCentralizedShoppingCartRemoveEventRegistrantFromGroupResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartRemoveEventRegistrantFromGroupResponse
     */
    public function setWEBCentralizedShoppingCartRemoveEventRegistrantFromGroupResult($WEBCentralizedShoppingCartRemoveEventRegistrantFromGroupResult)
    {
      $this->WEBCentralizedShoppingCartRemoveEventRegistrantFromGroupResult = $WEBCentralizedShoppingCartRemoveEventRegistrantFromGroupResult;
      return $this;
    }

}
