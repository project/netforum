<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType
{

    /**
     * @var av_key_Type $axo__ixo_key
     */
    protected $axo__ixo_key = null;

    /**
     * @var stringLength80_Type $axo__ixo_rlt_code
     */
    protected $axo__ixo_rlt_code = null;

    /**
     * @var stringLength150_Type $axo__ixo_title
     */
    protected $axo__ixo_title = null;

    /**
     * @var av_date_small_Type $axo__ixo_start_date
     */
    protected $axo__ixo_start_date = null;

    /**
     * @var av_date_small_Type $axo__ixo_end_date
     */
    protected $axo__ixo_end_date = null;

    /**
     * @var av_key_Type $axo__ixo_ind_cst_key
     */
    protected $axo__ixo_ind_cst_key = null;

    /**
     * @var av_key_Type $axo__ixo_org_cst_key
     */
    protected $axo__ixo_org_cst_key = null;

    /**
     * @var av_date_small_Type $axo__ixo_add_date
     */
    protected $axo__ixo_add_date = null;

    /**
     * @var av_user_Type $axo__ixo_add_user
     */
    protected $axo__ixo_add_user = null;

    /**
     * @var av_date_small_Type $axo__ixo_change_date
     */
    protected $axo__ixo_change_date = null;

    /**
     * @var av_user_Type $axo__ixo_change_user
     */
    protected $axo__ixo_change_user = null;

    /**
     * @var av_delete_flag_Type $axo__ixo_delete_flag
     */
    protected $axo__ixo_delete_flag = null;

    /**
     * @var av_key_Type $axo__ixo_key_ext
     */
    protected $axo__ixo_key_ext = null;

    /**
     * @var av_key_Type $axo__ixo_cst_key_owner
     */
    protected $axo__ixo_cst_key_owner = null;

    /**
     * @var av_key_Type $axo__ixo_entity_key
     */
    protected $axo__ixo_entity_key = null;

    /**
     * @var av_flag_Type $axo__ixo_void_flag
     */
    protected $axo__ixo_void_flag = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getAxo__ixo_key()
    {
      return $this->axo__ixo_key;
    }

    /**
     * @param av_key_Type $axo__ixo_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType
     */
    public function setAxo__ixo_key($axo__ixo_key)
    {
      $this->axo__ixo_key = $axo__ixo_key;
      return $this;
    }

    /**
     * @return stringLength80_Type
     */
    public function getAxo__ixo_rlt_code()
    {
      return $this->axo__ixo_rlt_code;
    }

    /**
     * @param stringLength80_Type $axo__ixo_rlt_code
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType
     */
    public function setAxo__ixo_rlt_code($axo__ixo_rlt_code)
    {
      $this->axo__ixo_rlt_code = $axo__ixo_rlt_code;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getAxo__ixo_title()
    {
      return $this->axo__ixo_title;
    }

    /**
     * @param stringLength150_Type $axo__ixo_title
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType
     */
    public function setAxo__ixo_title($axo__ixo_title)
    {
      $this->axo__ixo_title = $axo__ixo_title;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getAxo__ixo_start_date()
    {
      return $this->axo__ixo_start_date;
    }

    /**
     * @param av_date_small_Type $axo__ixo_start_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType
     */
    public function setAxo__ixo_start_date($axo__ixo_start_date)
    {
      $this->axo__ixo_start_date = $axo__ixo_start_date;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getAxo__ixo_end_date()
    {
      return $this->axo__ixo_end_date;
    }

    /**
     * @param av_date_small_Type $axo__ixo_end_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType
     */
    public function setAxo__ixo_end_date($axo__ixo_end_date)
    {
      $this->axo__ixo_end_date = $axo__ixo_end_date;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAxo__ixo_ind_cst_key()
    {
      return $this->axo__ixo_ind_cst_key;
    }

    /**
     * @param av_key_Type $axo__ixo_ind_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType
     */
    public function setAxo__ixo_ind_cst_key($axo__ixo_ind_cst_key)
    {
      $this->axo__ixo_ind_cst_key = $axo__ixo_ind_cst_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAxo__ixo_org_cst_key()
    {
      return $this->axo__ixo_org_cst_key;
    }

    /**
     * @param av_key_Type $axo__ixo_org_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType
     */
    public function setAxo__ixo_org_cst_key($axo__ixo_org_cst_key)
    {
      $this->axo__ixo_org_cst_key = $axo__ixo_org_cst_key;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getAxo__ixo_add_date()
    {
      return $this->axo__ixo_add_date;
    }

    /**
     * @param av_date_small_Type $axo__ixo_add_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType
     */
    public function setAxo__ixo_add_date($axo__ixo_add_date)
    {
      $this->axo__ixo_add_date = $axo__ixo_add_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getAxo__ixo_add_user()
    {
      return $this->axo__ixo_add_user;
    }

    /**
     * @param av_user_Type $axo__ixo_add_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType
     */
    public function setAxo__ixo_add_user($axo__ixo_add_user)
    {
      $this->axo__ixo_add_user = $axo__ixo_add_user;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getAxo__ixo_change_date()
    {
      return $this->axo__ixo_change_date;
    }

    /**
     * @param av_date_small_Type $axo__ixo_change_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType
     */
    public function setAxo__ixo_change_date($axo__ixo_change_date)
    {
      $this->axo__ixo_change_date = $axo__ixo_change_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getAxo__ixo_change_user()
    {
      return $this->axo__ixo_change_user;
    }

    /**
     * @param av_user_Type $axo__ixo_change_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType
     */
    public function setAxo__ixo_change_user($axo__ixo_change_user)
    {
      $this->axo__ixo_change_user = $axo__ixo_change_user;
      return $this;
    }

    /**
     * @return av_delete_flag_Type
     */
    public function getAxo__ixo_delete_flag()
    {
      return $this->axo__ixo_delete_flag;
    }

    /**
     * @param av_delete_flag_Type $axo__ixo_delete_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType
     */
    public function setAxo__ixo_delete_flag($axo__ixo_delete_flag)
    {
      $this->axo__ixo_delete_flag = $axo__ixo_delete_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAxo__ixo_key_ext()
    {
      return $this->axo__ixo_key_ext;
    }

    /**
     * @param av_key_Type $axo__ixo_key_ext
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType
     */
    public function setAxo__ixo_key_ext($axo__ixo_key_ext)
    {
      $this->axo__ixo_key_ext = $axo__ixo_key_ext;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAxo__ixo_cst_key_owner()
    {
      return $this->axo__ixo_cst_key_owner;
    }

    /**
     * @param av_key_Type $axo__ixo_cst_key_owner
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType
     */
    public function setAxo__ixo_cst_key_owner($axo__ixo_cst_key_owner)
    {
      $this->axo__ixo_cst_key_owner = $axo__ixo_cst_key_owner;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAxo__ixo_entity_key()
    {
      return $this->axo__ixo_entity_key;
    }

    /**
     * @param av_key_Type $axo__ixo_entity_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType
     */
    public function setAxo__ixo_entity_key($axo__ixo_entity_key)
    {
      $this->axo__ixo_entity_key = $axo__ixo_entity_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getAxo__ixo_void_flag()
    {
      return $this->axo__ixo_void_flag;
    }

    /**
     * @param av_flag_Type $axo__ixo_void_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Individual_Organization_CAO_Affiliation_DataObjectType
     */
    public function setAxo__ixo_void_flag($axo__ixo_void_flag)
    {
      $this->axo__ixo_void_flag = $axo__ixo_void_flag;
      return $this;
    }

}
