<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetFacultyListByEventKeyResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetFacultyListByEventKeyResult $WEBCentralizedShoppingCartGetFacultyListByEventKeyResult
     */
    protected $WEBCentralizedShoppingCartGetFacultyListByEventKeyResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetFacultyListByEventKeyResult $WEBCentralizedShoppingCartGetFacultyListByEventKeyResult
     */
    public function __construct($WEBCentralizedShoppingCartGetFacultyListByEventKeyResult)
    {
      $this->WEBCentralizedShoppingCartGetFacultyListByEventKeyResult = $WEBCentralizedShoppingCartGetFacultyListByEventKeyResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetFacultyListByEventKeyResult
     */
    public function getWEBCentralizedShoppingCartGetFacultyListByEventKeyResult()
    {
      return $this->WEBCentralizedShoppingCartGetFacultyListByEventKeyResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetFacultyListByEventKeyResult $WEBCentralizedShoppingCartGetFacultyListByEventKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetFacultyListByEventKeyResponse
     */
    public function setWEBCentralizedShoppingCartGetFacultyListByEventKeyResult($WEBCentralizedShoppingCartGetFacultyListByEventKeyResult)
    {
      $this->WEBCentralizedShoppingCartGetFacultyListByEventKeyResult = $WEBCentralizedShoppingCartGetFacultyListByEventKeyResult;
      return $this;
    }

}
