<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBContactRequestGetRequestTypeReasonsResponse
{

    /**
     * @var WEBContactRequestGetRequestTypeReasonsResult $WEBContactRequestGetRequestTypeReasonsResult
     */
    protected $WEBContactRequestGetRequestTypeReasonsResult = null;

    /**
     * @param WEBContactRequestGetRequestTypeReasonsResult $WEBContactRequestGetRequestTypeReasonsResult
     */
    public function __construct($WEBContactRequestGetRequestTypeReasonsResult)
    {
      $this->WEBContactRequestGetRequestTypeReasonsResult = $WEBContactRequestGetRequestTypeReasonsResult;
    }

    /**
     * @return WEBContactRequestGetRequestTypeReasonsResult
     */
    public function getWEBContactRequestGetRequestTypeReasonsResult()
    {
      return $this->WEBContactRequestGetRequestTypeReasonsResult;
    }

    /**
     * @param WEBContactRequestGetRequestTypeReasonsResult $WEBContactRequestGetRequestTypeReasonsResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBContactRequestGetRequestTypeReasonsResponse
     */
    public function setWEBContactRequestGetRequestTypeReasonsResult($WEBContactRequestGetRequestTypeReasonsResult)
    {
      $this->WEBContactRequestGetRequestTypeReasonsResult = $WEBContactRequestGetRequestTypeReasonsResult;
      return $this;
    }

}
