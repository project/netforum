<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBActivityGetRegistrantGuests
{

    /**
     * @var guid $RegKey
     */
    protected $RegKey = null;

    /**
     * @param guid $RegKey
     */
    public function __construct($RegKey)
    {
      $this->RegKey = $RegKey;
    }

    /**
     * @return guid
     */
    public function getRegKey()
    {
      return $this->RegKey;
    }

    /**
     * @param guid $RegKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBActivityGetRegistrantGuests
     */
    public function setRegKey($RegKey)
    {
      $this->RegKey = $RegKey;
      return $this;
    }

}
