<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class NewIndividualInformationResponse
{

    /**
     * @var NewIndividualInformationResult $NewIndividualInformationResult
     */
    protected $NewIndividualInformationResult = null;

    /**
     * @param NewIndividualInformationResult $NewIndividualInformationResult
     */
    public function __construct($NewIndividualInformationResult)
    {
      $this->NewIndividualInformationResult = $NewIndividualInformationResult;
    }

    /**
     * @return NewIndividualInformationResult
     */
    public function getNewIndividualInformationResult()
    {
      return $this->NewIndividualInformationResult;
    }

    /**
     * @param NewIndividualInformationResult $NewIndividualInformationResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\NewIndividualInformationResponse
     */
    public function setNewIndividualInformationResult($NewIndividualInformationResult)
    {
      $this->NewIndividualInformationResult = $NewIndividualInformationResult;
      return $this;
    }

}
