<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartAccreditationGetResponse
{

    /**
     * @var AccreditationAreaType $WEBCentralizedShoppingCartAccreditationGetResult
     */
    protected $WEBCentralizedShoppingCartAccreditationGetResult = null;

    /**
     * @param AccreditationAreaType $WEBCentralizedShoppingCartAccreditationGetResult
     */
    public function __construct($WEBCentralizedShoppingCartAccreditationGetResult)
    {
      $this->WEBCentralizedShoppingCartAccreditationGetResult = $WEBCentralizedShoppingCartAccreditationGetResult;
    }

    /**
     * @return AccreditationAreaType
     */
    public function getWEBCentralizedShoppingCartAccreditationGetResult()
    {
      return $this->WEBCentralizedShoppingCartAccreditationGetResult;
    }

    /**
     * @param AccreditationAreaType $WEBCentralizedShoppingCartAccreditationGetResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartAccreditationGetResponse
     */
    public function setWEBCentralizedShoppingCartAccreditationGetResult($WEBCentralizedShoppingCartAccreditationGetResult)
    {
      $this->WEBCentralizedShoppingCartAccreditationGetResult = $WEBCentralizedShoppingCartAccreditationGetResult;
      return $this;
    }

}
