<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBEmailGetResponse
{

    /**
     * @var CustomerEmailType $WEBEmailGetResult
     */
    protected $WEBEmailGetResult = null;

    /**
     * @param CustomerEmailType $WEBEmailGetResult
     */
    public function __construct($WEBEmailGetResult)
    {
      $this->WEBEmailGetResult = $WEBEmailGetResult;
    }

    /**
     * @return CustomerEmailType
     */
    public function getWEBEmailGetResult()
    {
      return $this->WEBEmailGetResult;
    }

    /**
     * @param CustomerEmailType $WEBEmailGetResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBEmailGetResponse
     */
    public function setWEBEmailGetResult($WEBEmailGetResult)
    {
      $this->WEBEmailGetResult = $WEBEmailGetResult;
      return $this;
    }

}
