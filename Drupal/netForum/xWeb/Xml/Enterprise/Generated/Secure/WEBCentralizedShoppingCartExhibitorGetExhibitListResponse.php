<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartExhibitorGetExhibitListResponse
{

    /**
     * @var WEBCentralizedShoppingCartExhibitorGetExhibitListResult $WEBCentralizedShoppingCartExhibitorGetExhibitListResult
     */
    protected $WEBCentralizedShoppingCartExhibitorGetExhibitListResult = null;

    /**
     * @param WEBCentralizedShoppingCartExhibitorGetExhibitListResult $WEBCentralizedShoppingCartExhibitorGetExhibitListResult
     */
    public function __construct($WEBCentralizedShoppingCartExhibitorGetExhibitListResult)
    {
      $this->WEBCentralizedShoppingCartExhibitorGetExhibitListResult = $WEBCentralizedShoppingCartExhibitorGetExhibitListResult;
    }

    /**
     * @return WEBCentralizedShoppingCartExhibitorGetExhibitListResult
     */
    public function getWEBCentralizedShoppingCartExhibitorGetExhibitListResult()
    {
      return $this->WEBCentralizedShoppingCartExhibitorGetExhibitListResult;
    }

    /**
     * @param WEBCentralizedShoppingCartExhibitorGetExhibitListResult $WEBCentralizedShoppingCartExhibitorGetExhibitListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartExhibitorGetExhibitListResponse
     */
    public function setWEBCentralizedShoppingCartExhibitorGetExhibitListResult($WEBCentralizedShoppingCartExhibitorGetExhibitListResult)
    {
      $this->WEBCentralizedShoppingCartExhibitorGetExhibitListResult = $WEBCentralizedShoppingCartExhibitorGetExhibitListResult;
      return $this;
    }

}
