<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBAddressInsert
{

    /**
     * @var CustomerAddressType $oFacadeObject
     */
    protected $oFacadeObject = null;

    /**
     * @param CustomerAddressType $oFacadeObject
     */
    public function __construct($oFacadeObject)
    {
      $this->oFacadeObject = $oFacadeObject;
    }

    /**
     * @return CustomerAddressType
     */
    public function getOFacadeObject()
    {
      return $this->oFacadeObject;
    }

    /**
     * @param CustomerAddressType $oFacadeObject
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBAddressInsert
     */
    public function setOFacadeObject($oFacadeObject)
    {
      $this->oFacadeObject = $oFacadeObject;
      return $this;
    }

}
