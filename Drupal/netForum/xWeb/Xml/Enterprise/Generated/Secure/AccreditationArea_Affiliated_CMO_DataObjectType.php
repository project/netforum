<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class AccreditationArea_Affiliated_CMO_DataObjectType
{

    /**
     * @var av_key_Type $cmo__cst_key
     */
    protected $cmo__cst_key = null;

    /**
     * @var stringLength20_Type $cmo__cst_type
     */
    protected $cmo__cst_type = null;

    /**
     * @var stringLength150_Type $cmo__cst_name_cp
     */
    protected $cmo__cst_name_cp = null;

    /**
     * @var stringLength150_Type $cmo__cst_sort_name_dn
     */
    protected $cmo__cst_sort_name_dn = null;

    /**
     * @var stringLength150_Type $cmo__cst_ind_full_name_dn
     */
    protected $cmo__cst_ind_full_name_dn = null;

    /**
     * @var stringLength150_Type $cmo__cst_org_name_dn
     */
    protected $cmo__cst_org_name_dn = null;

    /**
     * @var stringLength150_Type $cmo__cst_ixo_title_dn
     */
    protected $cmo__cst_ixo_title_dn = null;

    /**
     * @var stringLength10_Type $cmo__cst_pref_comm_meth
     */
    protected $cmo__cst_pref_comm_meth = null;

    /**
     * @var av_text_Type $cmo__cst_bio
     */
    protected $cmo__cst_bio = null;

    /**
     * @var av_date_small_Type $cmo__cst_add_date
     */
    protected $cmo__cst_add_date = null;

    /**
     * @var av_user_Type $cmo__cst_add_user
     */
    protected $cmo__cst_add_user = null;

    /**
     * @var av_date_small_Type $cmo__cst_change_date
     */
    protected $cmo__cst_change_date = null;

    /**
     * @var av_user_Type $cmo__cst_change_user
     */
    protected $cmo__cst_change_user = null;

    /**
     * @var av_delete_flag_Type $cmo__cst_delete_flag
     */
    protected $cmo__cst_delete_flag = null;

    /**
     * @var av_recno_Type $cmo__cst_recno
     */
    protected $cmo__cst_recno = null;

    /**
     * @var stringLength10_Type $cmo__cst_id
     */
    protected $cmo__cst_id = null;

    /**
     * @var av_key_Type $cmo__cst_key_ext
     */
    protected $cmo__cst_key_ext = null;

    /**
     * @var av_flag_Type $cmo__cst_email_text_only
     */
    protected $cmo__cst_email_text_only = null;

    /**
     * @var av_currency_Type $cmo__cst_credit_limit
     */
    protected $cmo__cst_credit_limit = null;

    /**
     * @var av_key_Type $cmo__cst_src_key
     */
    protected $cmo__cst_src_key = null;

    /**
     * @var stringLength50_Type $cmo__cst_src_code
     */
    protected $cmo__cst_src_code = null;

    /**
     * @var av_flag_Type $cmo__cst_tax_exempt_flag
     */
    protected $cmo__cst_tax_exempt_flag = null;

    /**
     * @var stringLength30_Type $cmo__cst_tax_id
     */
    protected $cmo__cst_tax_id = null;

    /**
     * @var av_key_Type $cmo__cst_cxa_key
     */
    protected $cmo__cst_cxa_key = null;

    /**
     * @var av_flag_Type $cmo__cst_no_email_flag
     */
    protected $cmo__cst_no_email_flag = null;

    /**
     * @var av_key_Type $cmo__cst_cxa_billing_key
     */
    protected $cmo__cst_cxa_billing_key = null;

    /**
     * @var av_email_Type $cmo__cst_eml_address_dn
     */
    protected $cmo__cst_eml_address_dn = null;

    /**
     * @var av_key_Type $cmo__cst_eml_key
     */
    protected $cmo__cst_eml_key = null;

    /**
     * @var av_flag_Type $cmo__cst_no_phone_flag
     */
    protected $cmo__cst_no_phone_flag = null;

    /**
     * @var stringLength55_Type $cmo__cst_phn_number_complete_dn
     */
    protected $cmo__cst_phn_number_complete_dn = null;

    /**
     * @var av_key_Type $cmo__cst_cph_key
     */
    protected $cmo__cst_cph_key = null;

    /**
     * @var av_flag_Type $cmo__cst_no_fax_flag
     */
    protected $cmo__cst_no_fax_flag = null;

    /**
     * @var stringLength55_Type $cmo__cst_fax_number_complete_dn
     */
    protected $cmo__cst_fax_number_complete_dn = null;

    /**
     * @var av_key_Type $cmo__cst_cfx_key
     */
    protected $cmo__cst_cfx_key = null;

    /**
     * @var av_key_Type $cmo__cst_ixo_key
     */
    protected $cmo__cst_ixo_key = null;

    /**
     * @var av_flag_Type $cmo__cst_no_web_flag
     */
    protected $cmo__cst_no_web_flag = null;

    /**
     * @var stringLength15_Type $cmo__cst_oldid
     */
    protected $cmo__cst_oldid = null;

    /**
     * @var av_flag_Type $cmo__cst_member_flag
     */
    protected $cmo__cst_member_flag = null;

    /**
     * @var av_url_Type $cmo__cst_url_code_dn
     */
    protected $cmo__cst_url_code_dn = null;

    /**
     * @var av_key_Type $cmo__cst_parent_cst_key
     */
    protected $cmo__cst_parent_cst_key = null;

    /**
     * @var av_key_Type $cmo__cst_url_key
     */
    protected $cmo__cst_url_key = null;

    /**
     * @var av_flag_Type $cmo__cst_no_msg_flag
     */
    protected $cmo__cst_no_msg_flag = null;

    /**
     * @var av_messaging_name_Type $cmo__cst_msg_handle_dn
     */
    protected $cmo__cst_msg_handle_dn = null;

    /**
     * @var stringLength80_Type $cmo__cst_web_login
     */
    protected $cmo__cst_web_login = null;

    /**
     * @var stringLength50_Type $cmo__cst_web_password
     */
    protected $cmo__cst_web_password = null;

    /**
     * @var av_key_Type $cmo__cst_entity_key
     */
    protected $cmo__cst_entity_key = null;

    /**
     * @var av_key_Type $cmo__cst_msg_key
     */
    protected $cmo__cst_msg_key = null;

    /**
     * @var av_flag_Type $cmo__cst_no_mail_flag
     */
    protected $cmo__cst_no_mail_flag = null;

    /**
     * @var av_date_small_Type $cmo__cst_web_start_date
     */
    protected $cmo__cst_web_start_date = null;

    /**
     * @var av_date_small_Type $cmo__cst_web_end_date
     */
    protected $cmo__cst_web_end_date = null;

    /**
     * @var av_flag_Type $cmo__cst_web_force_password_change
     */
    protected $cmo__cst_web_force_password_change = null;

    /**
     * @var av_flag_Type $cmo__cst_web_login_disabled_flag
     */
    protected $cmo__cst_web_login_disabled_flag = null;

    /**
     * @var av_text_Type $cmo__cst_comment
     */
    protected $cmo__cst_comment = null;

    /**
     * @var av_flag_Type $cmo__cst_credit_hold_flag
     */
    protected $cmo__cst_credit_hold_flag = null;

    /**
     * @var stringLength50_Type $cmo__cst_credit_hold_reason
     */
    protected $cmo__cst_credit_hold_reason = null;

    /**
     * @var av_flag_Type $cmo__cst_web_forgot_password_status
     */
    protected $cmo__cst_web_forgot_password_status = null;

    /**
     * @var av_key_Type $cmo__cst_old_cxa_key
     */
    protected $cmo__cst_old_cxa_key = null;

    /**
     * @var av_date_small_Type $cmo__cst_last_email_date
     */
    protected $cmo__cst_last_email_date = null;

    /**
     * @var av_flag_Type $cmo__cst_no_publish_flag
     */
    protected $cmo__cst_no_publish_flag = null;

    /**
     * @var av_key_Type $cmo__cst_sin_key
     */
    protected $cmo__cst_sin_key = null;

    /**
     * @var av_key_Type $cmo__cst_ttl_key
     */
    protected $cmo__cst_ttl_key = null;

    /**
     * @var av_key_Type $cmo__cst_jfn_key
     */
    protected $cmo__cst_jfn_key = null;

    /**
     * @var av_key_Type $cmo__cst_cur_key
     */
    protected $cmo__cst_cur_key = null;

    /**
     * @var stringLength510_Type $cmo__cst_attribute_1
     */
    protected $cmo__cst_attribute_1 = null;

    /**
     * @var stringLength510_Type $cmo__cst_attribute_2
     */
    protected $cmo__cst_attribute_2 = null;

    /**
     * @var stringLength100_Type $cmo__cst_salutation_1
     */
    protected $cmo__cst_salutation_1 = null;

    /**
     * @var stringLength100_Type $cmo__cst_salutation_2
     */
    protected $cmo__cst_salutation_2 = null;

    /**
     * @var av_key_Type $cmo__cst_merge_cst_key
     */
    protected $cmo__cst_merge_cst_key = null;

    /**
     * @var stringLength100_Type $cmo__cst_salutation_3
     */
    protected $cmo__cst_salutation_3 = null;

    /**
     * @var stringLength100_Type $cmo__cst_salutation_4
     */
    protected $cmo__cst_salutation_4 = null;

    /**
     * @var stringLength200_Type $cmo__cst_default_recognize_as
     */
    protected $cmo__cst_default_recognize_as = null;

    /**
     * @var av_decimal4_Type $cmo__cst_score
     */
    protected $cmo__cst_score = null;

    /**
     * @var av_integer_Type $cmo__cst_score_normalized
     */
    protected $cmo__cst_score_normalized = null;

    /**
     * @var av_integer_Type $cmo__cst_score_trend
     */
    protected $cmo__cst_score_trend = null;

    /**
     * @var stringLength25_Type $cmo__cst_vault_account
     */
    protected $cmo__cst_vault_account = null;

    /**
     * @var av_flag_Type $cmo__cst_exclude_from_social_flag
     */
    protected $cmo__cst_exclude_from_social_flag = null;

    /**
     * @var av_integer_Type $cmo__cst_social_score
     */
    protected $cmo__cst_social_score = null;

    /**
     * @var stringLength9_Type $cmo__cst_ptin
     */
    protected $cmo__cst_ptin = null;

    /**
     * @var av_recno_Type $cmo__cst_aicpa_member_id
     */
    protected $cmo__cst_aicpa_member_id = null;

    /**
     * @var stringLength50_Type $cmo__cst_vendor_code
     */
    protected $cmo__cst_vendor_code = null;

    /**
     * @var stringLength1_Type $cmo__cst_salt
     */
    protected $cmo__cst_salt = null;

    /**
     * @var av_key_Type $cmo__cst_sca_key
     */
    protected $cmo__cst_sca_key = null;

    /**
     * @var av_integer_Type $cmo__cst_iterations
     */
    protected $cmo__cst_iterations = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getCmo__cst_key()
    {
      return $this->cmo__cst_key;
    }

    /**
     * @param av_key_Type $cmo__cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_key($cmo__cst_key)
    {
      $this->cmo__cst_key = $cmo__cst_key;
      return $this;
    }

    /**
     * @return stringLength20_Type
     */
    public function getCmo__cst_type()
    {
      return $this->cmo__cst_type;
    }

    /**
     * @param stringLength20_Type $cmo__cst_type
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_type($cmo__cst_type)
    {
      $this->cmo__cst_type = $cmo__cst_type;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getCmo__cst_name_cp()
    {
      return $this->cmo__cst_name_cp;
    }

    /**
     * @param stringLength150_Type $cmo__cst_name_cp
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_name_cp($cmo__cst_name_cp)
    {
      $this->cmo__cst_name_cp = $cmo__cst_name_cp;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getCmo__cst_sort_name_dn()
    {
      return $this->cmo__cst_sort_name_dn;
    }

    /**
     * @param stringLength150_Type $cmo__cst_sort_name_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_sort_name_dn($cmo__cst_sort_name_dn)
    {
      $this->cmo__cst_sort_name_dn = $cmo__cst_sort_name_dn;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getCmo__cst_ind_full_name_dn()
    {
      return $this->cmo__cst_ind_full_name_dn;
    }

    /**
     * @param stringLength150_Type $cmo__cst_ind_full_name_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_ind_full_name_dn($cmo__cst_ind_full_name_dn)
    {
      $this->cmo__cst_ind_full_name_dn = $cmo__cst_ind_full_name_dn;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getCmo__cst_org_name_dn()
    {
      return $this->cmo__cst_org_name_dn;
    }

    /**
     * @param stringLength150_Type $cmo__cst_org_name_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_org_name_dn($cmo__cst_org_name_dn)
    {
      $this->cmo__cst_org_name_dn = $cmo__cst_org_name_dn;
      return $this;
    }

    /**
     * @return stringLength150_Type
     */
    public function getCmo__cst_ixo_title_dn()
    {
      return $this->cmo__cst_ixo_title_dn;
    }

    /**
     * @param stringLength150_Type $cmo__cst_ixo_title_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_ixo_title_dn($cmo__cst_ixo_title_dn)
    {
      $this->cmo__cst_ixo_title_dn = $cmo__cst_ixo_title_dn;
      return $this;
    }

    /**
     * @return stringLength10_Type
     */
    public function getCmo__cst_pref_comm_meth()
    {
      return $this->cmo__cst_pref_comm_meth;
    }

    /**
     * @param stringLength10_Type $cmo__cst_pref_comm_meth
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_pref_comm_meth($cmo__cst_pref_comm_meth)
    {
      $this->cmo__cst_pref_comm_meth = $cmo__cst_pref_comm_meth;
      return $this;
    }

    /**
     * @return av_text_Type
     */
    public function getCmo__cst_bio()
    {
      return $this->cmo__cst_bio;
    }

    /**
     * @param av_text_Type $cmo__cst_bio
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_bio($cmo__cst_bio)
    {
      $this->cmo__cst_bio = $cmo__cst_bio;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getCmo__cst_add_date()
    {
      return $this->cmo__cst_add_date;
    }

    /**
     * @param av_date_small_Type $cmo__cst_add_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_add_date($cmo__cst_add_date)
    {
      $this->cmo__cst_add_date = $cmo__cst_add_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getCmo__cst_add_user()
    {
      return $this->cmo__cst_add_user;
    }

    /**
     * @param av_user_Type $cmo__cst_add_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_add_user($cmo__cst_add_user)
    {
      $this->cmo__cst_add_user = $cmo__cst_add_user;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getCmo__cst_change_date()
    {
      return $this->cmo__cst_change_date;
    }

    /**
     * @param av_date_small_Type $cmo__cst_change_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_change_date($cmo__cst_change_date)
    {
      $this->cmo__cst_change_date = $cmo__cst_change_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getCmo__cst_change_user()
    {
      return $this->cmo__cst_change_user;
    }

    /**
     * @param av_user_Type $cmo__cst_change_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_change_user($cmo__cst_change_user)
    {
      $this->cmo__cst_change_user = $cmo__cst_change_user;
      return $this;
    }

    /**
     * @return av_delete_flag_Type
     */
    public function getCmo__cst_delete_flag()
    {
      return $this->cmo__cst_delete_flag;
    }

    /**
     * @param av_delete_flag_Type $cmo__cst_delete_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_delete_flag($cmo__cst_delete_flag)
    {
      $this->cmo__cst_delete_flag = $cmo__cst_delete_flag;
      return $this;
    }

    /**
     * @return av_recno_Type
     */
    public function getCmo__cst_recno()
    {
      return $this->cmo__cst_recno;
    }

    /**
     * @param av_recno_Type $cmo__cst_recno
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_recno($cmo__cst_recno)
    {
      $this->cmo__cst_recno = $cmo__cst_recno;
      return $this;
    }

    /**
     * @return stringLength10_Type
     */
    public function getCmo__cst_id()
    {
      return $this->cmo__cst_id;
    }

    /**
     * @param stringLength10_Type $cmo__cst_id
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_id($cmo__cst_id)
    {
      $this->cmo__cst_id = $cmo__cst_id;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCmo__cst_key_ext()
    {
      return $this->cmo__cst_key_ext;
    }

    /**
     * @param av_key_Type $cmo__cst_key_ext
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_key_ext($cmo__cst_key_ext)
    {
      $this->cmo__cst_key_ext = $cmo__cst_key_ext;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCmo__cst_email_text_only()
    {
      return $this->cmo__cst_email_text_only;
    }

    /**
     * @param av_flag_Type $cmo__cst_email_text_only
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_email_text_only($cmo__cst_email_text_only)
    {
      $this->cmo__cst_email_text_only = $cmo__cst_email_text_only;
      return $this;
    }

    /**
     * @return av_currency_Type
     */
    public function getCmo__cst_credit_limit()
    {
      return $this->cmo__cst_credit_limit;
    }

    /**
     * @param av_currency_Type $cmo__cst_credit_limit
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_credit_limit($cmo__cst_credit_limit)
    {
      $this->cmo__cst_credit_limit = $cmo__cst_credit_limit;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCmo__cst_src_key()
    {
      return $this->cmo__cst_src_key;
    }

    /**
     * @param av_key_Type $cmo__cst_src_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_src_key($cmo__cst_src_key)
    {
      $this->cmo__cst_src_key = $cmo__cst_src_key;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getCmo__cst_src_code()
    {
      return $this->cmo__cst_src_code;
    }

    /**
     * @param stringLength50_Type $cmo__cst_src_code
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_src_code($cmo__cst_src_code)
    {
      $this->cmo__cst_src_code = $cmo__cst_src_code;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCmo__cst_tax_exempt_flag()
    {
      return $this->cmo__cst_tax_exempt_flag;
    }

    /**
     * @param av_flag_Type $cmo__cst_tax_exempt_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_tax_exempt_flag($cmo__cst_tax_exempt_flag)
    {
      $this->cmo__cst_tax_exempt_flag = $cmo__cst_tax_exempt_flag;
      return $this;
    }

    /**
     * @return stringLength30_Type
     */
    public function getCmo__cst_tax_id()
    {
      return $this->cmo__cst_tax_id;
    }

    /**
     * @param stringLength30_Type $cmo__cst_tax_id
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_tax_id($cmo__cst_tax_id)
    {
      $this->cmo__cst_tax_id = $cmo__cst_tax_id;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCmo__cst_cxa_key()
    {
      return $this->cmo__cst_cxa_key;
    }

    /**
     * @param av_key_Type $cmo__cst_cxa_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_cxa_key($cmo__cst_cxa_key)
    {
      $this->cmo__cst_cxa_key = $cmo__cst_cxa_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCmo__cst_no_email_flag()
    {
      return $this->cmo__cst_no_email_flag;
    }

    /**
     * @param av_flag_Type $cmo__cst_no_email_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_no_email_flag($cmo__cst_no_email_flag)
    {
      $this->cmo__cst_no_email_flag = $cmo__cst_no_email_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCmo__cst_cxa_billing_key()
    {
      return $this->cmo__cst_cxa_billing_key;
    }

    /**
     * @param av_key_Type $cmo__cst_cxa_billing_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_cxa_billing_key($cmo__cst_cxa_billing_key)
    {
      $this->cmo__cst_cxa_billing_key = $cmo__cst_cxa_billing_key;
      return $this;
    }

    /**
     * @return av_email_Type
     */
    public function getCmo__cst_eml_address_dn()
    {
      return $this->cmo__cst_eml_address_dn;
    }

    /**
     * @param av_email_Type $cmo__cst_eml_address_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_eml_address_dn($cmo__cst_eml_address_dn)
    {
      $this->cmo__cst_eml_address_dn = $cmo__cst_eml_address_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCmo__cst_eml_key()
    {
      return $this->cmo__cst_eml_key;
    }

    /**
     * @param av_key_Type $cmo__cst_eml_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_eml_key($cmo__cst_eml_key)
    {
      $this->cmo__cst_eml_key = $cmo__cst_eml_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCmo__cst_no_phone_flag()
    {
      return $this->cmo__cst_no_phone_flag;
    }

    /**
     * @param av_flag_Type $cmo__cst_no_phone_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_no_phone_flag($cmo__cst_no_phone_flag)
    {
      $this->cmo__cst_no_phone_flag = $cmo__cst_no_phone_flag;
      return $this;
    }

    /**
     * @return stringLength55_Type
     */
    public function getCmo__cst_phn_number_complete_dn()
    {
      return $this->cmo__cst_phn_number_complete_dn;
    }

    /**
     * @param stringLength55_Type $cmo__cst_phn_number_complete_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_phn_number_complete_dn($cmo__cst_phn_number_complete_dn)
    {
      $this->cmo__cst_phn_number_complete_dn = $cmo__cst_phn_number_complete_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCmo__cst_cph_key()
    {
      return $this->cmo__cst_cph_key;
    }

    /**
     * @param av_key_Type $cmo__cst_cph_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_cph_key($cmo__cst_cph_key)
    {
      $this->cmo__cst_cph_key = $cmo__cst_cph_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCmo__cst_no_fax_flag()
    {
      return $this->cmo__cst_no_fax_flag;
    }

    /**
     * @param av_flag_Type $cmo__cst_no_fax_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_no_fax_flag($cmo__cst_no_fax_flag)
    {
      $this->cmo__cst_no_fax_flag = $cmo__cst_no_fax_flag;
      return $this;
    }

    /**
     * @return stringLength55_Type
     */
    public function getCmo__cst_fax_number_complete_dn()
    {
      return $this->cmo__cst_fax_number_complete_dn;
    }

    /**
     * @param stringLength55_Type $cmo__cst_fax_number_complete_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_fax_number_complete_dn($cmo__cst_fax_number_complete_dn)
    {
      $this->cmo__cst_fax_number_complete_dn = $cmo__cst_fax_number_complete_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCmo__cst_cfx_key()
    {
      return $this->cmo__cst_cfx_key;
    }

    /**
     * @param av_key_Type $cmo__cst_cfx_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_cfx_key($cmo__cst_cfx_key)
    {
      $this->cmo__cst_cfx_key = $cmo__cst_cfx_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCmo__cst_ixo_key()
    {
      return $this->cmo__cst_ixo_key;
    }

    /**
     * @param av_key_Type $cmo__cst_ixo_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_ixo_key($cmo__cst_ixo_key)
    {
      $this->cmo__cst_ixo_key = $cmo__cst_ixo_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCmo__cst_no_web_flag()
    {
      return $this->cmo__cst_no_web_flag;
    }

    /**
     * @param av_flag_Type $cmo__cst_no_web_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_no_web_flag($cmo__cst_no_web_flag)
    {
      $this->cmo__cst_no_web_flag = $cmo__cst_no_web_flag;
      return $this;
    }

    /**
     * @return stringLength15_Type
     */
    public function getCmo__cst_oldid()
    {
      return $this->cmo__cst_oldid;
    }

    /**
     * @param stringLength15_Type $cmo__cst_oldid
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_oldid($cmo__cst_oldid)
    {
      $this->cmo__cst_oldid = $cmo__cst_oldid;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCmo__cst_member_flag()
    {
      return $this->cmo__cst_member_flag;
    }

    /**
     * @param av_flag_Type $cmo__cst_member_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_member_flag($cmo__cst_member_flag)
    {
      $this->cmo__cst_member_flag = $cmo__cst_member_flag;
      return $this;
    }

    /**
     * @return av_url_Type
     */
    public function getCmo__cst_url_code_dn()
    {
      return $this->cmo__cst_url_code_dn;
    }

    /**
     * @param av_url_Type $cmo__cst_url_code_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_url_code_dn($cmo__cst_url_code_dn)
    {
      $this->cmo__cst_url_code_dn = $cmo__cst_url_code_dn;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCmo__cst_parent_cst_key()
    {
      return $this->cmo__cst_parent_cst_key;
    }

    /**
     * @param av_key_Type $cmo__cst_parent_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_parent_cst_key($cmo__cst_parent_cst_key)
    {
      $this->cmo__cst_parent_cst_key = $cmo__cst_parent_cst_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCmo__cst_url_key()
    {
      return $this->cmo__cst_url_key;
    }

    /**
     * @param av_key_Type $cmo__cst_url_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_url_key($cmo__cst_url_key)
    {
      $this->cmo__cst_url_key = $cmo__cst_url_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCmo__cst_no_msg_flag()
    {
      return $this->cmo__cst_no_msg_flag;
    }

    /**
     * @param av_flag_Type $cmo__cst_no_msg_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_no_msg_flag($cmo__cst_no_msg_flag)
    {
      $this->cmo__cst_no_msg_flag = $cmo__cst_no_msg_flag;
      return $this;
    }

    /**
     * @return av_messaging_name_Type
     */
    public function getCmo__cst_msg_handle_dn()
    {
      return $this->cmo__cst_msg_handle_dn;
    }

    /**
     * @param av_messaging_name_Type $cmo__cst_msg_handle_dn
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_msg_handle_dn($cmo__cst_msg_handle_dn)
    {
      $this->cmo__cst_msg_handle_dn = $cmo__cst_msg_handle_dn;
      return $this;
    }

    /**
     * @return stringLength80_Type
     */
    public function getCmo__cst_web_login()
    {
      return $this->cmo__cst_web_login;
    }

    /**
     * @param stringLength80_Type $cmo__cst_web_login
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_web_login($cmo__cst_web_login)
    {
      $this->cmo__cst_web_login = $cmo__cst_web_login;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getCmo__cst_web_password()
    {
      return $this->cmo__cst_web_password;
    }

    /**
     * @param stringLength50_Type $cmo__cst_web_password
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_web_password($cmo__cst_web_password)
    {
      $this->cmo__cst_web_password = $cmo__cst_web_password;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCmo__cst_entity_key()
    {
      return $this->cmo__cst_entity_key;
    }

    /**
     * @param av_key_Type $cmo__cst_entity_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_entity_key($cmo__cst_entity_key)
    {
      $this->cmo__cst_entity_key = $cmo__cst_entity_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCmo__cst_msg_key()
    {
      return $this->cmo__cst_msg_key;
    }

    /**
     * @param av_key_Type $cmo__cst_msg_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_msg_key($cmo__cst_msg_key)
    {
      $this->cmo__cst_msg_key = $cmo__cst_msg_key;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCmo__cst_no_mail_flag()
    {
      return $this->cmo__cst_no_mail_flag;
    }

    /**
     * @param av_flag_Type $cmo__cst_no_mail_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_no_mail_flag($cmo__cst_no_mail_flag)
    {
      $this->cmo__cst_no_mail_flag = $cmo__cst_no_mail_flag;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getCmo__cst_web_start_date()
    {
      return $this->cmo__cst_web_start_date;
    }

    /**
     * @param av_date_small_Type $cmo__cst_web_start_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_web_start_date($cmo__cst_web_start_date)
    {
      $this->cmo__cst_web_start_date = $cmo__cst_web_start_date;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getCmo__cst_web_end_date()
    {
      return $this->cmo__cst_web_end_date;
    }

    /**
     * @param av_date_small_Type $cmo__cst_web_end_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_web_end_date($cmo__cst_web_end_date)
    {
      $this->cmo__cst_web_end_date = $cmo__cst_web_end_date;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCmo__cst_web_force_password_change()
    {
      return $this->cmo__cst_web_force_password_change;
    }

    /**
     * @param av_flag_Type $cmo__cst_web_force_password_change
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_web_force_password_change($cmo__cst_web_force_password_change)
    {
      $this->cmo__cst_web_force_password_change = $cmo__cst_web_force_password_change;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCmo__cst_web_login_disabled_flag()
    {
      return $this->cmo__cst_web_login_disabled_flag;
    }

    /**
     * @param av_flag_Type $cmo__cst_web_login_disabled_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_web_login_disabled_flag($cmo__cst_web_login_disabled_flag)
    {
      $this->cmo__cst_web_login_disabled_flag = $cmo__cst_web_login_disabled_flag;
      return $this;
    }

    /**
     * @return av_text_Type
     */
    public function getCmo__cst_comment()
    {
      return $this->cmo__cst_comment;
    }

    /**
     * @param av_text_Type $cmo__cst_comment
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_comment($cmo__cst_comment)
    {
      $this->cmo__cst_comment = $cmo__cst_comment;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCmo__cst_credit_hold_flag()
    {
      return $this->cmo__cst_credit_hold_flag;
    }

    /**
     * @param av_flag_Type $cmo__cst_credit_hold_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_credit_hold_flag($cmo__cst_credit_hold_flag)
    {
      $this->cmo__cst_credit_hold_flag = $cmo__cst_credit_hold_flag;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getCmo__cst_credit_hold_reason()
    {
      return $this->cmo__cst_credit_hold_reason;
    }

    /**
     * @param stringLength50_Type $cmo__cst_credit_hold_reason
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_credit_hold_reason($cmo__cst_credit_hold_reason)
    {
      $this->cmo__cst_credit_hold_reason = $cmo__cst_credit_hold_reason;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCmo__cst_web_forgot_password_status()
    {
      return $this->cmo__cst_web_forgot_password_status;
    }

    /**
     * @param av_flag_Type $cmo__cst_web_forgot_password_status
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_web_forgot_password_status($cmo__cst_web_forgot_password_status)
    {
      $this->cmo__cst_web_forgot_password_status = $cmo__cst_web_forgot_password_status;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCmo__cst_old_cxa_key()
    {
      return $this->cmo__cst_old_cxa_key;
    }

    /**
     * @param av_key_Type $cmo__cst_old_cxa_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_old_cxa_key($cmo__cst_old_cxa_key)
    {
      $this->cmo__cst_old_cxa_key = $cmo__cst_old_cxa_key;
      return $this;
    }

    /**
     * @return av_date_small_Type
     */
    public function getCmo__cst_last_email_date()
    {
      return $this->cmo__cst_last_email_date;
    }

    /**
     * @param av_date_small_Type $cmo__cst_last_email_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_last_email_date($cmo__cst_last_email_date)
    {
      $this->cmo__cst_last_email_date = $cmo__cst_last_email_date;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCmo__cst_no_publish_flag()
    {
      return $this->cmo__cst_no_publish_flag;
    }

    /**
     * @param av_flag_Type $cmo__cst_no_publish_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_no_publish_flag($cmo__cst_no_publish_flag)
    {
      $this->cmo__cst_no_publish_flag = $cmo__cst_no_publish_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCmo__cst_sin_key()
    {
      return $this->cmo__cst_sin_key;
    }

    /**
     * @param av_key_Type $cmo__cst_sin_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_sin_key($cmo__cst_sin_key)
    {
      $this->cmo__cst_sin_key = $cmo__cst_sin_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCmo__cst_ttl_key()
    {
      return $this->cmo__cst_ttl_key;
    }

    /**
     * @param av_key_Type $cmo__cst_ttl_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_ttl_key($cmo__cst_ttl_key)
    {
      $this->cmo__cst_ttl_key = $cmo__cst_ttl_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCmo__cst_jfn_key()
    {
      return $this->cmo__cst_jfn_key;
    }

    /**
     * @param av_key_Type $cmo__cst_jfn_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_jfn_key($cmo__cst_jfn_key)
    {
      $this->cmo__cst_jfn_key = $cmo__cst_jfn_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCmo__cst_cur_key()
    {
      return $this->cmo__cst_cur_key;
    }

    /**
     * @param av_key_Type $cmo__cst_cur_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_cur_key($cmo__cst_cur_key)
    {
      $this->cmo__cst_cur_key = $cmo__cst_cur_key;
      return $this;
    }

    /**
     * @return stringLength510_Type
     */
    public function getCmo__cst_attribute_1()
    {
      return $this->cmo__cst_attribute_1;
    }

    /**
     * @param stringLength510_Type $cmo__cst_attribute_1
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_attribute_1($cmo__cst_attribute_1)
    {
      $this->cmo__cst_attribute_1 = $cmo__cst_attribute_1;
      return $this;
    }

    /**
     * @return stringLength510_Type
     */
    public function getCmo__cst_attribute_2()
    {
      return $this->cmo__cst_attribute_2;
    }

    /**
     * @param stringLength510_Type $cmo__cst_attribute_2
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_attribute_2($cmo__cst_attribute_2)
    {
      $this->cmo__cst_attribute_2 = $cmo__cst_attribute_2;
      return $this;
    }

    /**
     * @return stringLength100_Type
     */
    public function getCmo__cst_salutation_1()
    {
      return $this->cmo__cst_salutation_1;
    }

    /**
     * @param stringLength100_Type $cmo__cst_salutation_1
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_salutation_1($cmo__cst_salutation_1)
    {
      $this->cmo__cst_salutation_1 = $cmo__cst_salutation_1;
      return $this;
    }

    /**
     * @return stringLength100_Type
     */
    public function getCmo__cst_salutation_2()
    {
      return $this->cmo__cst_salutation_2;
    }

    /**
     * @param stringLength100_Type $cmo__cst_salutation_2
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_salutation_2($cmo__cst_salutation_2)
    {
      $this->cmo__cst_salutation_2 = $cmo__cst_salutation_2;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCmo__cst_merge_cst_key()
    {
      return $this->cmo__cst_merge_cst_key;
    }

    /**
     * @param av_key_Type $cmo__cst_merge_cst_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_merge_cst_key($cmo__cst_merge_cst_key)
    {
      $this->cmo__cst_merge_cst_key = $cmo__cst_merge_cst_key;
      return $this;
    }

    /**
     * @return stringLength100_Type
     */
    public function getCmo__cst_salutation_3()
    {
      return $this->cmo__cst_salutation_3;
    }

    /**
     * @param stringLength100_Type $cmo__cst_salutation_3
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_salutation_3($cmo__cst_salutation_3)
    {
      $this->cmo__cst_salutation_3 = $cmo__cst_salutation_3;
      return $this;
    }

    /**
     * @return stringLength100_Type
     */
    public function getCmo__cst_salutation_4()
    {
      return $this->cmo__cst_salutation_4;
    }

    /**
     * @param stringLength100_Type $cmo__cst_salutation_4
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_salutation_4($cmo__cst_salutation_4)
    {
      $this->cmo__cst_salutation_4 = $cmo__cst_salutation_4;
      return $this;
    }

    /**
     * @return stringLength200_Type
     */
    public function getCmo__cst_default_recognize_as()
    {
      return $this->cmo__cst_default_recognize_as;
    }

    /**
     * @param stringLength200_Type $cmo__cst_default_recognize_as
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_default_recognize_as($cmo__cst_default_recognize_as)
    {
      $this->cmo__cst_default_recognize_as = $cmo__cst_default_recognize_as;
      return $this;
    }

    /**
     * @return av_decimal4_Type
     */
    public function getCmo__cst_score()
    {
      return $this->cmo__cst_score;
    }

    /**
     * @param av_decimal4_Type $cmo__cst_score
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_score($cmo__cst_score)
    {
      $this->cmo__cst_score = $cmo__cst_score;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getCmo__cst_score_normalized()
    {
      return $this->cmo__cst_score_normalized;
    }

    /**
     * @param av_integer_Type $cmo__cst_score_normalized
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_score_normalized($cmo__cst_score_normalized)
    {
      $this->cmo__cst_score_normalized = $cmo__cst_score_normalized;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getCmo__cst_score_trend()
    {
      return $this->cmo__cst_score_trend;
    }

    /**
     * @param av_integer_Type $cmo__cst_score_trend
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_score_trend($cmo__cst_score_trend)
    {
      $this->cmo__cst_score_trend = $cmo__cst_score_trend;
      return $this;
    }

    /**
     * @return stringLength25_Type
     */
    public function getCmo__cst_vault_account()
    {
      return $this->cmo__cst_vault_account;
    }

    /**
     * @param stringLength25_Type $cmo__cst_vault_account
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_vault_account($cmo__cst_vault_account)
    {
      $this->cmo__cst_vault_account = $cmo__cst_vault_account;
      return $this;
    }

    /**
     * @return av_flag_Type
     */
    public function getCmo__cst_exclude_from_social_flag()
    {
      return $this->cmo__cst_exclude_from_social_flag;
    }

    /**
     * @param av_flag_Type $cmo__cst_exclude_from_social_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_exclude_from_social_flag($cmo__cst_exclude_from_social_flag)
    {
      $this->cmo__cst_exclude_from_social_flag = $cmo__cst_exclude_from_social_flag;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getCmo__cst_social_score()
    {
      return $this->cmo__cst_social_score;
    }

    /**
     * @param av_integer_Type $cmo__cst_social_score
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_social_score($cmo__cst_social_score)
    {
      $this->cmo__cst_social_score = $cmo__cst_social_score;
      return $this;
    }

    /**
     * @return stringLength9_Type
     */
    public function getCmo__cst_ptin()
    {
      return $this->cmo__cst_ptin;
    }

    /**
     * @param stringLength9_Type $cmo__cst_ptin
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_ptin($cmo__cst_ptin)
    {
      $this->cmo__cst_ptin = $cmo__cst_ptin;
      return $this;
    }

    /**
     * @return av_recno_Type
     */
    public function getCmo__cst_aicpa_member_id()
    {
      return $this->cmo__cst_aicpa_member_id;
    }

    /**
     * @param av_recno_Type $cmo__cst_aicpa_member_id
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_aicpa_member_id($cmo__cst_aicpa_member_id)
    {
      $this->cmo__cst_aicpa_member_id = $cmo__cst_aicpa_member_id;
      return $this;
    }

    /**
     * @return stringLength50_Type
     */
    public function getCmo__cst_vendor_code()
    {
      return $this->cmo__cst_vendor_code;
    }

    /**
     * @param stringLength50_Type $cmo__cst_vendor_code
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_vendor_code($cmo__cst_vendor_code)
    {
      $this->cmo__cst_vendor_code = $cmo__cst_vendor_code;
      return $this;
    }

    /**
     * @return stringLength1_Type
     */
    public function getCmo__cst_salt()
    {
      return $this->cmo__cst_salt;
    }

    /**
     * @param stringLength1_Type $cmo__cst_salt
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_salt($cmo__cst_salt)
    {
      $this->cmo__cst_salt = $cmo__cst_salt;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getCmo__cst_sca_key()
    {
      return $this->cmo__cst_sca_key;
    }

    /**
     * @param av_key_Type $cmo__cst_sca_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_sca_key($cmo__cst_sca_key)
    {
      $this->cmo__cst_sca_key = $cmo__cst_sca_key;
      return $this;
    }

    /**
     * @return av_integer_Type
     */
    public function getCmo__cst_iterations()
    {
      return $this->cmo__cst_iterations;
    }

    /**
     * @param av_integer_Type $cmo__cst_iterations
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Affiliated_CMO_DataObjectType
     */
    public function setCmo__cst_iterations($cmo__cst_iterations)
    {
      $this->cmo__cst_iterations = $cmo__cst_iterations;
      return $this;
    }

}
