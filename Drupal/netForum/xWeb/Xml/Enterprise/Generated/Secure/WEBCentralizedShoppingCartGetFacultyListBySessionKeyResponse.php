<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetFacultyListBySessionKeyResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetFacultyListBySessionKeyResult $WEBCentralizedShoppingCartGetFacultyListBySessionKeyResult
     */
    protected $WEBCentralizedShoppingCartGetFacultyListBySessionKeyResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetFacultyListBySessionKeyResult $WEBCentralizedShoppingCartGetFacultyListBySessionKeyResult
     */
    public function __construct($WEBCentralizedShoppingCartGetFacultyListBySessionKeyResult)
    {
      $this->WEBCentralizedShoppingCartGetFacultyListBySessionKeyResult = $WEBCentralizedShoppingCartGetFacultyListBySessionKeyResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetFacultyListBySessionKeyResult
     */
    public function getWEBCentralizedShoppingCartGetFacultyListBySessionKeyResult()
    {
      return $this->WEBCentralizedShoppingCartGetFacultyListBySessionKeyResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetFacultyListBySessionKeyResult $WEBCentralizedShoppingCartGetFacultyListBySessionKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetFacultyListBySessionKeyResponse
     */
    public function setWEBCentralizedShoppingCartGetFacultyListBySessionKeyResult($WEBCentralizedShoppingCartGetFacultyListBySessionKeyResult)
    {
      $this->WEBCentralizedShoppingCartGetFacultyListBySessionKeyResult = $WEBCentralizedShoppingCartGetFacultyListBySessionKeyResult;
      return $this;
    }

}
