<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserFindOrganizationsByDomainResponse
{

    /**
     * @var WEBWebUserFindOrganizationsByDomainResult $WEBWebUserFindOrganizationsByDomainResult
     */
    protected $WEBWebUserFindOrganizationsByDomainResult = null;

    /**
     * @param WEBWebUserFindOrganizationsByDomainResult $WEBWebUserFindOrganizationsByDomainResult
     */
    public function __construct($WEBWebUserFindOrganizationsByDomainResult)
    {
      $this->WEBWebUserFindOrganizationsByDomainResult = $WEBWebUserFindOrganizationsByDomainResult;
    }

    /**
     * @return WEBWebUserFindOrganizationsByDomainResult
     */
    public function getWEBWebUserFindOrganizationsByDomainResult()
    {
      return $this->WEBWebUserFindOrganizationsByDomainResult;
    }

    /**
     * @param WEBWebUserFindOrganizationsByDomainResult $WEBWebUserFindOrganizationsByDomainResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserFindOrganizationsByDomainResponse
     */
    public function setWEBWebUserFindOrganizationsByDomainResult($WEBWebUserFindOrganizationsByDomainResult)
    {
      $this->WEBWebUserFindOrganizationsByDomainResult = $WEBWebUserFindOrganizationsByDomainResult;
      return $this;
    }

}
