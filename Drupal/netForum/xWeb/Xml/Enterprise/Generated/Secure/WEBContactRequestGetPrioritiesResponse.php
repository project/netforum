<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBContactRequestGetPrioritiesResponse
{

    /**
     * @var WEBContactRequestGetPrioritiesResult $WEBContactRequestGetPrioritiesResult
     */
    protected $WEBContactRequestGetPrioritiesResult = null;

    /**
     * @param WEBContactRequestGetPrioritiesResult $WEBContactRequestGetPrioritiesResult
     */
    public function __construct($WEBContactRequestGetPrioritiesResult)
    {
      $this->WEBContactRequestGetPrioritiesResult = $WEBContactRequestGetPrioritiesResult;
    }

    /**
     * @return WEBContactRequestGetPrioritiesResult
     */
    public function getWEBContactRequestGetPrioritiesResult()
    {
      return $this->WEBContactRequestGetPrioritiesResult;
    }

    /**
     * @param WEBContactRequestGetPrioritiesResult $WEBContactRequestGetPrioritiesResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBContactRequestGetPrioritiesResponse
     */
    public function setWEBContactRequestGetPrioritiesResult($WEBContactRequestGetPrioritiesResult)
    {
      $this->WEBContactRequestGetPrioritiesResult = $WEBContactRequestGetPrioritiesResult;
      return $this;
    }

}
