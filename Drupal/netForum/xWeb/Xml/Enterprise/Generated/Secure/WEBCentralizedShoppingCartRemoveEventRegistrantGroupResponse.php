<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartRemoveEventRegistrantGroupResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartRemoveEventRegistrantGroupResult
     */
    protected $WEBCentralizedShoppingCartRemoveEventRegistrantGroupResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartRemoveEventRegistrantGroupResult
     */
    public function __construct($WEBCentralizedShoppingCartRemoveEventRegistrantGroupResult)
    {
      $this->WEBCentralizedShoppingCartRemoveEventRegistrantGroupResult = $WEBCentralizedShoppingCartRemoveEventRegistrantGroupResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartRemoveEventRegistrantGroupResult()
    {
      return $this->WEBCentralizedShoppingCartRemoveEventRegistrantGroupResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartRemoveEventRegistrantGroupResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartRemoveEventRegistrantGroupResponse
     */
    public function setWEBCentralizedShoppingCartRemoveEventRegistrantGroupResult($WEBCentralizedShoppingCartRemoveEventRegistrantGroupResult)
    {
      $this->WEBCentralizedShoppingCartRemoveEventRegistrantGroupResult = $WEBCentralizedShoppingCartRemoveEventRegistrantGroupResult;
      return $this;
    }

}
