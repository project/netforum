<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBFaxInsert
{

    /**
     * @var CustomerFaxType $oFacadeObject
     */
    protected $oFacadeObject = null;

    /**
     * @param CustomerFaxType $oFacadeObject
     */
    public function __construct($oFacadeObject)
    {
      $this->oFacadeObject = $oFacadeObject;
    }

    /**
     * @return CustomerFaxType
     */
    public function getOFacadeObject()
    {
      return $this->oFacadeObject;
    }

    /**
     * @param CustomerFaxType $oFacadeObject
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBFaxInsert
     */
    public function setOFacadeObject($oFacadeObject)
    {
      $this->oFacadeObject = $oFacadeObject;
      return $this;
    }

}
