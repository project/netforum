<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserFindUsersByName
{

    /**
     * @var string $usernameToMatch
     */
    protected $usernameToMatch = null;

    /**
     * @param string $usernameToMatch
     */
    public function __construct($usernameToMatch)
    {
      $this->usernameToMatch = $usernameToMatch;
    }

    /**
     * @return string
     */
    public function getUsernameToMatch()
    {
      return $this->usernameToMatch;
    }

    /**
     * @param string $usernameToMatch
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserFindUsersByName
     */
    public function setUsernameToMatch($usernameToMatch)
    {
      $this->usernameToMatch = $usernameToMatch;
      return $this;
    }

}
