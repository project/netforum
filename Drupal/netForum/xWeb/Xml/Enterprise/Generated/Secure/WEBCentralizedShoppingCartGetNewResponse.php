<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetNewResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartGetNewResult
     */
    protected $WEBCentralizedShoppingCartGetNewResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartGetNewResult
     */
    public function __construct($WEBCentralizedShoppingCartGetNewResult)
    {
      $this->WEBCentralizedShoppingCartGetNewResult = $WEBCentralizedShoppingCartGetNewResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartGetNewResult()
    {
      return $this->WEBCentralizedShoppingCartGetNewResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartGetNewResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetNewResponse
     */
    public function setWEBCentralizedShoppingCartGetNewResult($WEBCentralizedShoppingCartGetNewResult)
    {
      $this->WEBCentralizedShoppingCartGetNewResult = $WEBCentralizedShoppingCartGetNewResult;
      return $this;
    }

}
