<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartMembershipOpenInvoiceAddResponse
{

    /**
     * @var CentralizedOrderEntryType $WEBCentralizedShoppingCartMembershipOpenInvoiceAddResult
     */
    protected $WEBCentralizedShoppingCartMembershipOpenInvoiceAddResult = null;

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartMembershipOpenInvoiceAddResult
     */
    public function __construct($WEBCentralizedShoppingCartMembershipOpenInvoiceAddResult)
    {
      $this->WEBCentralizedShoppingCartMembershipOpenInvoiceAddResult = $WEBCentralizedShoppingCartMembershipOpenInvoiceAddResult;
    }

    /**
     * @return CentralizedOrderEntryType
     */
    public function getWEBCentralizedShoppingCartMembershipOpenInvoiceAddResult()
    {
      return $this->WEBCentralizedShoppingCartMembershipOpenInvoiceAddResult;
    }

    /**
     * @param CentralizedOrderEntryType $WEBCentralizedShoppingCartMembershipOpenInvoiceAddResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartMembershipOpenInvoiceAddResponse
     */
    public function setWEBCentralizedShoppingCartMembershipOpenInvoiceAddResult($WEBCentralizedShoppingCartMembershipOpenInvoiceAddResult)
    {
      $this->WEBCentralizedShoppingCartMembershipOpenInvoiceAddResult = $WEBCentralizedShoppingCartMembershipOpenInvoiceAddResult;
      return $this;
    }

}
