<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBAddressGetStatesResponse
{

    /**
     * @var WEBAddressGetStatesResult $WEBAddressGetStatesResult
     */
    protected $WEBAddressGetStatesResult = null;

    /**
     * @param WEBAddressGetStatesResult $WEBAddressGetStatesResult
     */
    public function __construct($WEBAddressGetStatesResult)
    {
      $this->WEBAddressGetStatesResult = $WEBAddressGetStatesResult;
    }

    /**
     * @return WEBAddressGetStatesResult
     */
    public function getWEBAddressGetStatesResult()
    {
      return $this->WEBAddressGetStatesResult;
    }

    /**
     * @param WEBAddressGetStatesResult $WEBAddressGetStatesResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBAddressGetStatesResponse
     */
    public function setWEBAddressGetStatesResult($WEBAddressGetStatesResult)
    {
      $this->WEBAddressGetStatesResult = $WEBAddressGetStatesResult;
      return $this;
    }

}
