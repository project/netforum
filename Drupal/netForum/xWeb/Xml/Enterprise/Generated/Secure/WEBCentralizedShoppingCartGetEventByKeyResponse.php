<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetEventByKeyResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetEventByKeyResult $WEBCentralizedShoppingCartGetEventByKeyResult
     */
    protected $WEBCentralizedShoppingCartGetEventByKeyResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetEventByKeyResult $WEBCentralizedShoppingCartGetEventByKeyResult
     */
    public function __construct($WEBCentralizedShoppingCartGetEventByKeyResult)
    {
      $this->WEBCentralizedShoppingCartGetEventByKeyResult = $WEBCentralizedShoppingCartGetEventByKeyResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetEventByKeyResult
     */
    public function getWEBCentralizedShoppingCartGetEventByKeyResult()
    {
      return $this->WEBCentralizedShoppingCartGetEventByKeyResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetEventByKeyResult $WEBCentralizedShoppingCartGetEventByKeyResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetEventByKeyResponse
     */
    public function setWEBCentralizedShoppingCartGetEventByKeyResult($WEBCentralizedShoppingCartGetEventByKeyResult)
    {
      $this->WEBCentralizedShoppingCartGetEventByKeyResult = $WEBCentralizedShoppingCartGetEventByKeyResult;
      return $this;
    }

}
