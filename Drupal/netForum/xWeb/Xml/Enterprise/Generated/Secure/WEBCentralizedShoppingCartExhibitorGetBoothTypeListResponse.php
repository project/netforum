<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartExhibitorGetBoothTypeListResponse
{

    /**
     * @var WEBCentralizedShoppingCartExhibitorGetBoothTypeListResult $WEBCentralizedShoppingCartExhibitorGetBoothTypeListResult
     */
    protected $WEBCentralizedShoppingCartExhibitorGetBoothTypeListResult = null;

    /**
     * @param WEBCentralizedShoppingCartExhibitorGetBoothTypeListResult $WEBCentralizedShoppingCartExhibitorGetBoothTypeListResult
     */
    public function __construct($WEBCentralizedShoppingCartExhibitorGetBoothTypeListResult)
    {
      $this->WEBCentralizedShoppingCartExhibitorGetBoothTypeListResult = $WEBCentralizedShoppingCartExhibitorGetBoothTypeListResult;
    }

    /**
     * @return WEBCentralizedShoppingCartExhibitorGetBoothTypeListResult
     */
    public function getWEBCentralizedShoppingCartExhibitorGetBoothTypeListResult()
    {
      return $this->WEBCentralizedShoppingCartExhibitorGetBoothTypeListResult;
    }

    /**
     * @param WEBCentralizedShoppingCartExhibitorGetBoothTypeListResult $WEBCentralizedShoppingCartExhibitorGetBoothTypeListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartExhibitorGetBoothTypeListResponse
     */
    public function setWEBCentralizedShoppingCartExhibitorGetBoothTypeListResult($WEBCentralizedShoppingCartExhibitorGetBoothTypeListResult)
    {
      $this->WEBCentralizedShoppingCartExhibitorGetBoothTypeListResult = $WEBCentralizedShoppingCartExhibitorGetBoothTypeListResult;
      return $this;
    }

}
