<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResponse
{

    /**
     * @var WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResult $WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResult
     */
    protected $WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResult = null;

    /**
     * @param WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResult $WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResult
     */
    public function __construct($WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResult)
    {
      $this->WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResult = $WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResult
     */
    public function getWEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResult()
    {
      return $this->WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResult $WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResponse
     */
    public function setWEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResult($WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResult)
    {
      $this->WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResult = $WEBCentralizedShoppingCartGiftGetPremiumProductsListByGiftResult;
      return $this;
    }

}
