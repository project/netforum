<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBWebUserFindUsersByEmail
{

    /**
     * @var string $emailToMatch
     */
    protected $emailToMatch = null;

    /**
     * @param string $emailToMatch
     */
    public function __construct($emailToMatch)
    {
      $this->emailToMatch = $emailToMatch;
    }

    /**
     * @return string
     */
    public function getEmailToMatch()
    {
      return $this->emailToMatch;
    }

    /**
     * @param string $emailToMatch
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBWebUserFindUsersByEmail
     */
    public function setEmailToMatch($emailToMatch)
    {
      $this->emailToMatch = $emailToMatch;
      return $this;
    }

}
