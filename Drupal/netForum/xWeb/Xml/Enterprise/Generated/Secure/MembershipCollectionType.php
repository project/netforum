<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class MembershipCollectionType
{

    /**
     * @var mb_membershipType[] $mb_membership
     */
    protected $mb_membership = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return mb_membershipType[]
     */
    public function getMb_membership()
    {
      return $this->mb_membership;
    }

    /**
     * @param mb_membershipType[] $mb_membership
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\MembershipCollectionType
     */
    public function setMb_membership(array $mb_membership = null)
    {
      $this->mb_membership = $mb_membership;
      return $this;
    }

}
