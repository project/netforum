<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartEventRegistrantGroupRefreshResponse
{

    /**
     * @var EventsRegistrantGroupType $WEBCentralizedShoppingCartEventRegistrantGroupRefreshResult
     */
    protected $WEBCentralizedShoppingCartEventRegistrantGroupRefreshResult = null;

    /**
     * @param EventsRegistrantGroupType $WEBCentralizedShoppingCartEventRegistrantGroupRefreshResult
     */
    public function __construct($WEBCentralizedShoppingCartEventRegistrantGroupRefreshResult)
    {
      $this->WEBCentralizedShoppingCartEventRegistrantGroupRefreshResult = $WEBCentralizedShoppingCartEventRegistrantGroupRefreshResult;
    }

    /**
     * @return EventsRegistrantGroupType
     */
    public function getWEBCentralizedShoppingCartEventRegistrantGroupRefreshResult()
    {
      return $this->WEBCentralizedShoppingCartEventRegistrantGroupRefreshResult;
    }

    /**
     * @param EventsRegistrantGroupType $WEBCentralizedShoppingCartEventRegistrantGroupRefreshResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartEventRegistrantGroupRefreshResponse
     */
    public function setWEBCentralizedShoppingCartEventRegistrantGroupRefreshResult($WEBCentralizedShoppingCartEventRegistrantGroupRefreshResult)
    {
      $this->WEBCentralizedShoppingCartEventRegistrantGroupRefreshResult = $WEBCentralizedShoppingCartEventRegistrantGroupRefreshResult;
      return $this;
    }

}
