<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBChapterGetResponse
{

    /**
     * @var ChapterType $WEBChapterGetResult
     */
    protected $WEBChapterGetResult = null;

    /**
     * @param ChapterType $WEBChapterGetResult
     */
    public function __construct($WEBChapterGetResult)
    {
      $this->WEBChapterGetResult = $WEBChapterGetResult;
    }

    /**
     * @return ChapterType
     */
    public function getWEBChapterGetResult()
    {
      return $this->WEBChapterGetResult;
    }

    /**
     * @param ChapterType $WEBChapterGetResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBChapterGetResponse
     */
    public function setWEBChapterGetResult($WEBChapterGetResult)
    {
      $this->WEBChapterGetResult = $WEBChapterGetResult;
      return $this;
    }

}
