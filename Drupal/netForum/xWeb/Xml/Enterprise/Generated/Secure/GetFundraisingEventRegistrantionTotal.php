<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class GetFundraisingEventRegistrantionTotal
{

    /**
     * @var string $eventKey
     */
    protected $eventKey = null;

    /**
     * @var string $customerKey
     */
    protected $customerKey = null;

    /**
     * @param string $eventKey
     * @param string $customerKey
     */
    public function __construct($eventKey, $customerKey)
    {
      $this->eventKey = $eventKey;
      $this->customerKey = $customerKey;
    }

    /**
     * @return string
     */
    public function getEventKey()
    {
      return $this->eventKey;
    }

    /**
     * @param string $eventKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetFundraisingEventRegistrantionTotal
     */
    public function setEventKey($eventKey)
    {
      $this->eventKey = $eventKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getCustomerKey()
    {
      return $this->customerKey;
    }

    /**
     * @param string $customerKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\GetFundraisingEventRegistrantionTotal
     */
    public function setCustomerKey($customerKey)
    {
      $this->customerKey = $customerKey;
      return $this;
    }

}
