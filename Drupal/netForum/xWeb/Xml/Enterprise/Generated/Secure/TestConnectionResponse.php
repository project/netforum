<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class TestConnectionResponse
{

    /**
     * @var string $TestConnectionResult
     */
    protected $TestConnectionResult = null;

    /**
     * @param string $TestConnectionResult
     */
    public function __construct($TestConnectionResult)
    {
      $this->TestConnectionResult = $TestConnectionResult;
    }

    /**
     * @return string
     */
    public function getTestConnectionResult()
    {
      return $this->TestConnectionResult;
    }

    /**
     * @param string $TestConnectionResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\TestConnectionResponse
     */
    public function setTestConnectionResult($TestConnectionResult)
    {
      $this->TestConnectionResult = $TestConnectionResult;
      return $this;
    }

}
