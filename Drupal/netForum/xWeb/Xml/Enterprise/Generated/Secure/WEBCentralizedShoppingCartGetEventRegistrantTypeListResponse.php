<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class WEBCentralizedShoppingCartGetEventRegistrantTypeListResponse
{

    /**
     * @var WEBCentralizedShoppingCartGetEventRegistrantTypeListResult $WEBCentralizedShoppingCartGetEventRegistrantTypeListResult
     */
    protected $WEBCentralizedShoppingCartGetEventRegistrantTypeListResult = null;

    /**
     * @param WEBCentralizedShoppingCartGetEventRegistrantTypeListResult $WEBCentralizedShoppingCartGetEventRegistrantTypeListResult
     */
    public function __construct($WEBCentralizedShoppingCartGetEventRegistrantTypeListResult)
    {
      $this->WEBCentralizedShoppingCartGetEventRegistrantTypeListResult = $WEBCentralizedShoppingCartGetEventRegistrantTypeListResult;
    }

    /**
     * @return WEBCentralizedShoppingCartGetEventRegistrantTypeListResult
     */
    public function getWEBCentralizedShoppingCartGetEventRegistrantTypeListResult()
    {
      return $this->WEBCentralizedShoppingCartGetEventRegistrantTypeListResult;
    }

    /**
     * @param WEBCentralizedShoppingCartGetEventRegistrantTypeListResult $WEBCentralizedShoppingCartGetEventRegistrantTypeListResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\WEBCentralizedShoppingCartGetEventRegistrantTypeListResponse
     */
    public function setWEBCentralizedShoppingCartGetEventRegistrantTypeListResult($WEBCentralizedShoppingCartGetEventRegistrantTypeListResult)
    {
      $this->WEBCentralizedShoppingCartGetEventRegistrantTypeListResult = $WEBCentralizedShoppingCartGetEventRegistrantTypeListResult;
      return $this;
    }

}
