<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure;

class AccreditationArea_Accreditation_Status_DataObjectType
{

    /**
     * @var av_key_Type $ams_key
     */
    protected $ams_key = null;

    /**
     * @var av_user_Type $ams_add_user
     */
    protected $ams_add_user = null;

    /**
     * @var av_date_Type $ams_add_date
     */
    protected $ams_add_date = null;

    /**
     * @var av_user_Type $ams_change_user
     */
    protected $ams_change_user = null;

    /**
     * @var av_date_Type $ams_change_date
     */
    protected $ams_change_date = null;

    /**
     * @var av_delete_flag_Type $ams_delete_flag
     */
    protected $ams_delete_flag = null;

    /**
     * @var av_key_Type $ams_entity_key
     */
    protected $ams_entity_key = null;

    /**
     * @var av_key_Type $ams_key_ext
     */
    protected $ams_key_ext = null;

    /**
     * @var stringLength200_Type $ams_desc
     */
    protected $ams_desc = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return av_key_Type
     */
    public function getAms_key()
    {
      return $this->ams_key;
    }

    /**
     * @param av_key_Type $ams_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Status_DataObjectType
     */
    public function setAms_key($ams_key)
    {
      $this->ams_key = $ams_key;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getAms_add_user()
    {
      return $this->ams_add_user;
    }

    /**
     * @param av_user_Type $ams_add_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Status_DataObjectType
     */
    public function setAms_add_user($ams_add_user)
    {
      $this->ams_add_user = $ams_add_user;
      return $this;
    }

    /**
     * @return av_date_Type
     */
    public function getAms_add_date()
    {
      return $this->ams_add_date;
    }

    /**
     * @param av_date_Type $ams_add_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Status_DataObjectType
     */
    public function setAms_add_date($ams_add_date)
    {
      $this->ams_add_date = $ams_add_date;
      return $this;
    }

    /**
     * @return av_user_Type
     */
    public function getAms_change_user()
    {
      return $this->ams_change_user;
    }

    /**
     * @param av_user_Type $ams_change_user
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Status_DataObjectType
     */
    public function setAms_change_user($ams_change_user)
    {
      $this->ams_change_user = $ams_change_user;
      return $this;
    }

    /**
     * @return av_date_Type
     */
    public function getAms_change_date()
    {
      return $this->ams_change_date;
    }

    /**
     * @param av_date_Type $ams_change_date
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Status_DataObjectType
     */
    public function setAms_change_date($ams_change_date)
    {
      $this->ams_change_date = $ams_change_date;
      return $this;
    }

    /**
     * @return av_delete_flag_Type
     */
    public function getAms_delete_flag()
    {
      return $this->ams_delete_flag;
    }

    /**
     * @param av_delete_flag_Type $ams_delete_flag
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Status_DataObjectType
     */
    public function setAms_delete_flag($ams_delete_flag)
    {
      $this->ams_delete_flag = $ams_delete_flag;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAms_entity_key()
    {
      return $this->ams_entity_key;
    }

    /**
     * @param av_key_Type $ams_entity_key
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Status_DataObjectType
     */
    public function setAms_entity_key($ams_entity_key)
    {
      $this->ams_entity_key = $ams_entity_key;
      return $this;
    }

    /**
     * @return av_key_Type
     */
    public function getAms_key_ext()
    {
      return $this->ams_key_ext;
    }

    /**
     * @param av_key_Type $ams_key_ext
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Status_DataObjectType
     */
    public function setAms_key_ext($ams_key_ext)
    {
      $this->ams_key_ext = $ams_key_ext;
      return $this;
    }

    /**
     * @return stringLength200_Type
     */
    public function getAms_desc()
    {
      return $this->ams_desc;
    }

    /**
     * @param stringLength200_Type $ams_desc
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\AccreditationArea_Accreditation_Status_DataObjectType
     */
    public function setAms_desc($ams_desc)
    {
      $this->ams_desc = $ams_desc;
      return $this;
    }

}
