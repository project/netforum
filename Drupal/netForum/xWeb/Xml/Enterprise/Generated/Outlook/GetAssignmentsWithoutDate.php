<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook;

class GetAssignmentsWithoutDate
{

    /**
     * @var string $mUserName
     */
    protected $mUserName = null;

    /**
     * @param string $mUserName
     */
    public function __construct($mUserName)
    {
      $this->mUserName = $mUserName;
    }

    /**
     * @return string
     */
    public function getMUserName()
    {
      return $this->mUserName;
    }

    /**
     * @param string $mUserName
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetAssignmentsWithoutDate
     */
    public function setMUserName($mUserName)
    {
      $this->mUserName = $mUserName;
      return $this;
    }

}
