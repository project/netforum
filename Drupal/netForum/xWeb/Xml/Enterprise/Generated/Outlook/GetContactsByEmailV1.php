<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook;

class GetContactsByEmailV1
{

    /**
     * @var string $szEmailAddress
     */
    protected $szEmailAddress = null;

    /**
     * @param string $szEmailAddress
     */
    public function __construct($szEmailAddress)
    {
      $this->szEmailAddress = $szEmailAddress;
    }

    /**
     * @return string
     */
    public function getSzEmailAddress()
    {
      return $this->szEmailAddress;
    }

    /**
     * @param string $szEmailAddress
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetContactsByEmailV1
     */
    public function setSzEmailAddress($szEmailAddress)
    {
      $this->szEmailAddress = $szEmailAddress;
      return $this;
    }

}
