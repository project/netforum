<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook;

class GetMailingDetailV1
{

    /**
     * @var string $szListName
     */
    protected $szListName = null;

    /**
     * @var string $szKey
     */
    protected $szKey = null;

    /**
     * @param string $szListName
     * @param string $szKey
     */
    public function __construct($szListName, $szKey)
    {
      $this->szListName = $szListName;
      $this->szKey = $szKey;
    }

    /**
     * @return string
     */
    public function getSzListName()
    {
      return $this->szListName;
    }

    /**
     * @param string $szListName
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetMailingDetailV1
     */
    public function setSzListName($szListName)
    {
      $this->szListName = $szListName;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzKey()
    {
      return $this->szKey;
    }

    /**
     * @param string $szKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetMailingDetailV1
     */
    public function setSzKey($szKey)
    {
      $this->szKey = $szKey;
      return $this;
    }

}
