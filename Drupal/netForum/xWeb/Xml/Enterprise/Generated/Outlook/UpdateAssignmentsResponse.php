<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook;

class UpdateAssignmentsResponse
{

    /**
     * @var boolean $UpdateAssignmentsResult
     */
    protected $UpdateAssignmentsResult = null;

    /**
     * @param boolean $UpdateAssignmentsResult
     */
    public function __construct($UpdateAssignmentsResult)
    {
      $this->UpdateAssignmentsResult = $UpdateAssignmentsResult;
    }

    /**
     * @return boolean
     */
    public function getUpdateAssignmentsResult()
    {
      return $this->UpdateAssignmentsResult;
    }

    /**
     * @param boolean $UpdateAssignmentsResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\UpdateAssignmentsResponse
     */
    public function setUpdateAssignmentsResult($UpdateAssignmentsResult)
    {
      $this->UpdateAssignmentsResult = $UpdateAssignmentsResult;
      return $this;
    }

}
