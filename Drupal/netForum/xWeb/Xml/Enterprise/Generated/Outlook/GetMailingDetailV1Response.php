<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook;

class GetMailingDetailV1Response
{

    /**
     * @var GetMailingDetailV1Result $GetMailingDetailV1Result
     */
    protected $GetMailingDetailV1Result = null;

    /**
     * @param GetMailingDetailV1Result $GetMailingDetailV1Result
     */
    public function __construct($GetMailingDetailV1Result)
    {
      $this->GetMailingDetailV1Result = $GetMailingDetailV1Result;
    }

    /**
     * @return GetMailingDetailV1Result
     */
    public function getGetMailingDetailV1Result()
    {
      return $this->GetMailingDetailV1Result;
    }

    /**
     * @param GetMailingDetailV1Result $GetMailingDetailV1Result
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetMailingDetailV1Response
     */
    public function setGetMailingDetailV1Result($GetMailingDetailV1Result)
    {
      $this->GetMailingDetailV1Result = $GetMailingDetailV1Result;
      return $this;
    }

}
