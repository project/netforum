<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook;

class GetTabsAndLinksResponse
{

    /**
     * @var GetTabsAndLinksResult $GetTabsAndLinksResult
     */
    protected $GetTabsAndLinksResult = null;

    /**
     * @param GetTabsAndLinksResult $GetTabsAndLinksResult
     */
    public function __construct($GetTabsAndLinksResult)
    {
      $this->GetTabsAndLinksResult = $GetTabsAndLinksResult;
    }

    /**
     * @return GetTabsAndLinksResult
     */
    public function getGetTabsAndLinksResult()
    {
      return $this->GetTabsAndLinksResult;
    }

    /**
     * @param GetTabsAndLinksResult $GetTabsAndLinksResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetTabsAndLinksResponse
     */
    public function setGetTabsAndLinksResult($GetTabsAndLinksResult)
    {
      $this->GetTabsAndLinksResult = $GetTabsAndLinksResult;
      return $this;
    }

}
