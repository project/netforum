<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook;

class GetAssignments
{

    /**
     * @var string $mUserName
     */
    protected $mUserName = null;

    /**
     * @var string $mLastSync
     */
    protected $mLastSync = null;

    /**
     * @param string $mUserName
     * @param string $mLastSync
     */
    public function __construct($mUserName, $mLastSync)
    {
      $this->mUserName = $mUserName;
      $this->mLastSync = $mLastSync;
    }

    /**
     * @return string
     */
    public function getMUserName()
    {
      return $this->mUserName;
    }

    /**
     * @param string $mUserName
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetAssignments
     */
    public function setMUserName($mUserName)
    {
      $this->mUserName = $mUserName;
      return $this;
    }

    /**
     * @return string
     */
    public function getMLastSync()
    {
      return $this->mLastSync;
    }

    /**
     * @param string $mLastSync
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetAssignments
     */
    public function setMLastSync($mLastSync)
    {
      $this->mLastSync = $mLastSync;
      return $this;
    }

}
