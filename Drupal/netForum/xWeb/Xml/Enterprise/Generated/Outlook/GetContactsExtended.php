<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook;

class GetContactsExtended
{

    /**
     * @var ArrayOfString $parametersNames
     */
    protected $parametersNames = null;

    /**
     * @var ArrayOfString $parametersValues
     */
    protected $parametersValues = null;

    /**
     * @param ArrayOfString $parametersNames
     * @param ArrayOfString $parametersValues
     */
    public function __construct($parametersNames, $parametersValues)
    {
      $this->parametersNames = $parametersNames;
      $this->parametersValues = $parametersValues;
    }

    /**
     * @return ArrayOfString
     */
    public function getParametersNames()
    {
      return $this->parametersNames;
    }

    /**
     * @param ArrayOfString $parametersNames
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetContactsExtended
     */
    public function setParametersNames($parametersNames)
    {
      $this->parametersNames = $parametersNames;
      return $this;
    }

    /**
     * @return ArrayOfString
     */
    public function getParametersValues()
    {
      return $this->parametersValues;
    }

    /**
     * @param ArrayOfString $parametersValues
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetContactsExtended
     */
    public function setParametersValues($parametersValues)
    {
      $this->parametersValues = $parametersValues;
      return $this;
    }

}
