<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook;

class GetAssignmentsResponse
{

    /**
     * @var GetAssignmentsResult $GetAssignmentsResult
     */
    protected $GetAssignmentsResult = null;

    /**
     * @param GetAssignmentsResult $GetAssignmentsResult
     */
    public function __construct($GetAssignmentsResult)
    {
      $this->GetAssignmentsResult = $GetAssignmentsResult;
    }

    /**
     * @return GetAssignmentsResult
     */
    public function getGetAssignmentsResult()
    {
      return $this->GetAssignmentsResult;
    }

    /**
     * @param GetAssignmentsResult $GetAssignmentsResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetAssignmentsResponse
     */
    public function setGetAssignmentsResult($GetAssignmentsResult)
    {
      $this->GetAssignmentsResult = $GetAssignmentsResult;
      return $this;
    }

}
