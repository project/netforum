<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook;

class GetTasksResponse
{

    /**
     * @var GetTasksResult $GetTasksResult
     */
    protected $GetTasksResult = null;

    /**
     * @param GetTasksResult $GetTasksResult
     */
    public function __construct($GetTasksResult)
    {
      $this->GetTasksResult = $GetTasksResult;
    }

    /**
     * @return GetTasksResult
     */
    public function getGetTasksResult()
    {
      return $this->GetTasksResult;
    }

    /**
     * @param GetTasksResult $GetTasksResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetTasksResponse
     */
    public function setGetTasksResult($GetTasksResult)
    {
      $this->GetTasksResult = $GetTasksResult;
      return $this;
    }

}
