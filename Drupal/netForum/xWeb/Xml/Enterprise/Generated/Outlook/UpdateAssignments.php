<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook;

class UpdateAssignments
{

    /**
     * @var string $szGuid
     */
    protected $szGuid = null;

    /**
     * @var string $szSubject
     */
    protected $szSubject = null;

    /**
     * @var string $szBody
     */
    protected $szBody = null;

    /**
     * @var string $szActivityType
     */
    protected $szActivityType = null;

    /**
     * @var string $szPriority
     */
    protected $szPriority = null;

    /**
     * @var string $szReminderDate
     */
    protected $szReminderDate = null;

    /**
     * @var string $szDueDate
     */
    protected $szDueDate = null;

    /**
     * @var string $szStatus
     */
    protected $szStatus = null;

    /**
     * @param string $szGuid
     * @param string $szSubject
     * @param string $szBody
     * @param string $szActivityType
     * @param string $szPriority
     * @param string $szReminderDate
     * @param string $szDueDate
     * @param string $szStatus
     */
    public function __construct($szGuid, $szSubject, $szBody, $szActivityType, $szPriority, $szReminderDate, $szDueDate, $szStatus)
    {
      $this->szGuid = $szGuid;
      $this->szSubject = $szSubject;
      $this->szBody = $szBody;
      $this->szActivityType = $szActivityType;
      $this->szPriority = $szPriority;
      $this->szReminderDate = $szReminderDate;
      $this->szDueDate = $szDueDate;
      $this->szStatus = $szStatus;
    }

    /**
     * @return string
     */
    public function getSzGuid()
    {
      return $this->szGuid;
    }

    /**
     * @param string $szGuid
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\UpdateAssignments
     */
    public function setSzGuid($szGuid)
    {
      $this->szGuid = $szGuid;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzSubject()
    {
      return $this->szSubject;
    }

    /**
     * @param string $szSubject
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\UpdateAssignments
     */
    public function setSzSubject($szSubject)
    {
      $this->szSubject = $szSubject;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzBody()
    {
      return $this->szBody;
    }

    /**
     * @param string $szBody
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\UpdateAssignments
     */
    public function setSzBody($szBody)
    {
      $this->szBody = $szBody;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzActivityType()
    {
      return $this->szActivityType;
    }

    /**
     * @param string $szActivityType
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\UpdateAssignments
     */
    public function setSzActivityType($szActivityType)
    {
      $this->szActivityType = $szActivityType;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzPriority()
    {
      return $this->szPriority;
    }

    /**
     * @param string $szPriority
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\UpdateAssignments
     */
    public function setSzPriority($szPriority)
    {
      $this->szPriority = $szPriority;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzReminderDate()
    {
      return $this->szReminderDate;
    }

    /**
     * @param string $szReminderDate
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\UpdateAssignments
     */
    public function setSzReminderDate($szReminderDate)
    {
      $this->szReminderDate = $szReminderDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzDueDate()
    {
      return $this->szDueDate;
    }

    /**
     * @param string $szDueDate
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\UpdateAssignments
     */
    public function setSzDueDate($szDueDate)
    {
      $this->szDueDate = $szDueDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzStatus()
    {
      return $this->szStatus;
    }

    /**
     * @param string $szStatus
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\UpdateAssignments
     */
    public function setSzStatus($szStatus)
    {
      $this->szStatus = $szStatus;
      return $this;
    }

}
