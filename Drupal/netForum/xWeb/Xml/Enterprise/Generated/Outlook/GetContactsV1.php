<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook;

class GetContactsV1
{

    /**
     * @var ArrayOfString $Names
     */
    protected $Names = null;

    /**
     * @param ArrayOfString $Names
     */
    public function __construct($Names)
    {
      $this->Names = $Names;
    }

    /**
     * @return ArrayOfString
     */
    public function getNames()
    {
      return $this->Names;
    }

    /**
     * @param ArrayOfString $Names
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetContactsV1
     */
    public function setNames($Names)
    {
      $this->Names = $Names;
      return $this;
    }

}
