<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook;

class GetContactsByEmailV1Response
{

    /**
     * @var GetContactsByEmailV1Result $GetContactsByEmailV1Result
     */
    protected $GetContactsByEmailV1Result = null;

    /**
     * @param GetContactsByEmailV1Result $GetContactsByEmailV1Result
     */
    public function __construct($GetContactsByEmailV1Result)
    {
      $this->GetContactsByEmailV1Result = $GetContactsByEmailV1Result;
    }

    /**
     * @return GetContactsByEmailV1Result
     */
    public function getGetContactsByEmailV1Result()
    {
      return $this->GetContactsByEmailV1Result;
    }

    /**
     * @param GetContactsByEmailV1Result $GetContactsByEmailV1Result
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetContactsByEmailV1Response
     */
    public function setGetContactsByEmailV1Result($GetContactsByEmailV1Result)
    {
      $this->GetContactsByEmailV1Result = $GetContactsByEmailV1Result;
      return $this;
    }

}
