<?php


 function autoload_92dd423a5bf7f0ae045cfb85074fc2b4($class)
{
    $classes = array(
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\NetForumOutlook' => __DIR__ .'/NetForumOutlook.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\Authenticate' => __DIR__ .'/Authenticate.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\AuthenticateResponse' => __DIR__ .'/AuthenticateResponse.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\AuthorizationToken' => __DIR__ .'/AuthorizationToken.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetVersion' => __DIR__ .'/GetVersion.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetVersionResponse' => __DIR__ .'/GetVersionResponse.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetContactsV1' => __DIR__ .'/GetContactsV1.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\ArrayOfString' => __DIR__ .'/ArrayOfString.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetContactsV1Response' => __DIR__ .'/GetContactsV1Response.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetContactsV1Result' => __DIR__ .'/GetContactsV1Result.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetMailingDetailV1' => __DIR__ .'/GetMailingDetailV1.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetMailingDetailV1Response' => __DIR__ .'/GetMailingDetailV1Response.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetMailingDetailV1Result' => __DIR__ .'/GetMailingDetailV1Result.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetContactsByEmailV1' => __DIR__ .'/GetContactsByEmailV1.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetContactsByEmailV1Response' => __DIR__ .'/GetContactsByEmailV1Response.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetContactsByEmailV1Result' => __DIR__ .'/GetContactsByEmailV1Result.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\LogMailV1' => __DIR__ .'/LogMailV1.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\LogMailV1Response' => __DIR__ .'/LogMailV1Response.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetContactsExtended' => __DIR__ .'/GetContactsExtended.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetContactsExtendedResponse' => __DIR__ .'/GetContactsExtendedResponse.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetContactsExtendedResult' => __DIR__ .'/GetContactsExtendedResult.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetTabsAndLinks' => __DIR__ .'/GetTabsAndLinks.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetTabsAndLinksResponse' => __DIR__ .'/GetTabsAndLinksResponse.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetTabsAndLinksResult' => __DIR__ .'/GetTabsAndLinksResult.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetTasks' => __DIR__ .'/GetTasks.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetTasksResponse' => __DIR__ .'/GetTasksResponse.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetTasksResult' => __DIR__ .'/GetTasksResult.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetAssignmentsWithoutDate' => __DIR__ .'/GetAssignmentsWithoutDate.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetAssignmentsWithoutDateResponse' => __DIR__ .'/GetAssignmentsWithoutDateResponse.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetAssignmentsWithoutDateResult' => __DIR__ .'/GetAssignmentsWithoutDateResult.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetAssignments' => __DIR__ .'/GetAssignments.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetAssignmentsResponse' => __DIR__ .'/GetAssignmentsResponse.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetAssignmentsResult' => __DIR__ .'/GetAssignmentsResult.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\UpdateAssignments' => __DIR__ .'/UpdateAssignments.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\UpdateAssignmentsResponse' => __DIR__ .'/UpdateAssignmentsResponse.php',
        'Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\StringArray' => __DIR__ .'/StringArray.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_92dd423a5bf7f0ae045cfb85074fc2b4');

// Do nothing. The rest is just leftovers from the code generation.
{
}
