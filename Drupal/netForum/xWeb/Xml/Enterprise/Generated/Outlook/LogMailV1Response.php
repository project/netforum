<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook;

class LogMailV1Response
{

    /**
     * @var boolean $LogMailV1Result
     */
    protected $LogMailV1Result = null;

    /**
     * @param boolean $LogMailV1Result
     */
    public function __construct($LogMailV1Result)
    {
      $this->LogMailV1Result = $LogMailV1Result;
    }

    /**
     * @return boolean
     */
    public function getLogMailV1Result()
    {
      return $this->LogMailV1Result;
    }

    /**
     * @param boolean $LogMailV1Result
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\LogMailV1Response
     */
    public function setLogMailV1Result($LogMailV1Result)
    {
      $this->LogMailV1Result = $LogMailV1Result;
      return $this;
    }

}
