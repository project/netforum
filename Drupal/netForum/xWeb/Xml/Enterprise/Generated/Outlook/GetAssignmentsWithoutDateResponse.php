<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook;

class GetAssignmentsWithoutDateResponse
{

    /**
     * @var GetAssignmentsWithoutDateResult $GetAssignmentsWithoutDateResult
     */
    protected $GetAssignmentsWithoutDateResult = null;

    /**
     * @param GetAssignmentsWithoutDateResult $GetAssignmentsWithoutDateResult
     */
    public function __construct($GetAssignmentsWithoutDateResult)
    {
      $this->GetAssignmentsWithoutDateResult = $GetAssignmentsWithoutDateResult;
    }

    /**
     * @return GetAssignmentsWithoutDateResult
     */
    public function getGetAssignmentsWithoutDateResult()
    {
      return $this->GetAssignmentsWithoutDateResult;
    }

    /**
     * @param GetAssignmentsWithoutDateResult $GetAssignmentsWithoutDateResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetAssignmentsWithoutDateResponse
     */
    public function setGetAssignmentsWithoutDateResult($GetAssignmentsWithoutDateResult)
    {
      $this->GetAssignmentsWithoutDateResult = $GetAssignmentsWithoutDateResult;
      return $this;
    }

}
