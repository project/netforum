<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook;

class LogMailV1
{

    /**
     * @var string $szEmailAddress
     */
    protected $szEmailAddress = null;

    /**
     * @var string $szCstKey
     */
    protected $szCstKey = null;

    /**
     * @var string $szMailSubject
     */
    protected $szMailSubject = null;

    /**
     * @var string $szMailBody
     */
    protected $szMailBody = null;

    /**
     * @param string $szEmailAddress
     * @param string $szCstKey
     * @param string $szMailSubject
     * @param string $szMailBody
     */
    public function __construct($szEmailAddress, $szCstKey, $szMailSubject, $szMailBody)
    {
      $this->szEmailAddress = $szEmailAddress;
      $this->szCstKey = $szCstKey;
      $this->szMailSubject = $szMailSubject;
      $this->szMailBody = $szMailBody;
    }

    /**
     * @return string
     */
    public function getSzEmailAddress()
    {
      return $this->szEmailAddress;
    }

    /**
     * @param string $szEmailAddress
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\LogMailV1
     */
    public function setSzEmailAddress($szEmailAddress)
    {
      $this->szEmailAddress = $szEmailAddress;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzCstKey()
    {
      return $this->szCstKey;
    }

    /**
     * @param string $szCstKey
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\LogMailV1
     */
    public function setSzCstKey($szCstKey)
    {
      $this->szCstKey = $szCstKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzMailSubject()
    {
      return $this->szMailSubject;
    }

    /**
     * @param string $szMailSubject
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\LogMailV1
     */
    public function setSzMailSubject($szMailSubject)
    {
      $this->szMailSubject = $szMailSubject;
      return $this;
    }

    /**
     * @return string
     */
    public function getSzMailBody()
    {
      return $this->szMailBody;
    }

    /**
     * @param string $szMailBody
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\LogMailV1
     */
    public function setSzMailBody($szMailBody)
    {
      $this->szMailBody = $szMailBody;
      return $this;
    }

}
