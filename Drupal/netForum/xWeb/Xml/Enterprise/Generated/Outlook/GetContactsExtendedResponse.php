<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook;

class GetContactsExtendedResponse
{

    /**
     * @var GetContactsExtendedResult $GetContactsExtendedResult
     */
    protected $GetContactsExtendedResult = null;

    /**
     * @param GetContactsExtendedResult $GetContactsExtendedResult
     */
    public function __construct($GetContactsExtendedResult)
    {
      $this->GetContactsExtendedResult = $GetContactsExtendedResult;
    }

    /**
     * @return GetContactsExtendedResult
     */
    public function getGetContactsExtendedResult()
    {
      return $this->GetContactsExtendedResult;
    }

    /**
     * @param GetContactsExtendedResult $GetContactsExtendedResult
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetContactsExtendedResponse
     */
    public function setGetContactsExtendedResult($GetContactsExtendedResult)
    {
      $this->GetContactsExtendedResult = $GetContactsExtendedResult;
      return $this;
    }

}
