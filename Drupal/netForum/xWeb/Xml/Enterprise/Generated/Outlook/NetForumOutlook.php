<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook;

use Drupal\netForum\xWeb\Xml\netForumSoapClient;


/**
 * netForum xWeb and MS Outlook
 */
class NetForumOutlook extends netForumSoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
      'Authenticate' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\Authenticate',
      'AuthenticateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\AuthenticateResponse',
      'AuthorizationToken' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\AuthorizationToken',
      'GetVersion' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetVersion',
      'GetVersionResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetVersionResponse',
      'GetContactsV1' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetContactsV1',
      'ArrayOfString' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\ArrayOfString',
      'GetContactsV1Response' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetContactsV1Response',
      'GetContactsV1Result' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetContactsV1Result',
      'GetMailingDetailV1' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetMailingDetailV1',
      'GetMailingDetailV1Response' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetMailingDetailV1Response',
      'GetMailingDetailV1Result' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetMailingDetailV1Result',
      'GetContactsByEmailV1' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetContactsByEmailV1',
      'GetContactsByEmailV1Response' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetContactsByEmailV1Response',
      'GetContactsByEmailV1Result' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetContactsByEmailV1Result',
      'LogMailV1' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\LogMailV1',
      'LogMailV1Response' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\LogMailV1Response',
      'GetContactsExtended' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetContactsExtended',
      'GetContactsExtendedResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetContactsExtendedResponse',
      'GetContactsExtendedResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetContactsExtendedResult',
      'GetTabsAndLinks' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetTabsAndLinks',
      'GetTabsAndLinksResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetTabsAndLinksResponse',
      'GetTabsAndLinksResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetTabsAndLinksResult',
      'GetTasks' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetTasks',
      'GetTasksResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetTasksResponse',
      'GetTasksResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetTasksResult',
      'GetAssignmentsWithoutDate' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetAssignmentsWithoutDate',
      'GetAssignmentsWithoutDateResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetAssignmentsWithoutDateResponse',
      'GetAssignmentsWithoutDateResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetAssignmentsWithoutDateResult',
      'GetAssignments' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetAssignments',
      'GetAssignmentsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetAssignmentsResponse',
      'GetAssignmentsResult' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\GetAssignmentsResult',
      'UpdateAssignments' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\UpdateAssignments',
      'UpdateAssignmentsResponse' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\UpdateAssignmentsResponse',
      'StringArray' => 'Drupal\\netForum\\xWeb\\Xml\\Enterprise\\Generated\\Outlook\\StringArray',
    );

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
      'features' => 1,
    ), $options);
      if (!$wsdl) {
        $wsdl = 'http://nfupgrade.netforument.com/nfUpgrade2017Demo/xWeb/Outlook/Outlook.asmx?WSDL';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * Passing correct credentials to this method will return an authentication token - without an authentication token, the rest of the xWeb web methods will be inoperable.  The authentication token is governed by the group privileges assigned to the account invoking the xWeb web methods.  Please consult with the administrator of the netForum database to ensure your level of authorization.  This method MUST be the first method invoked at xWeb, as the authentication token that is returned is required for every xWeb method.
     *
     * @param Authenticate $parameters
     * @return AuthenticateResponse
     */
    public function Authenticate(Authenticate $parameters)
    {
      return $this->__soapCall('Authenticate', array($parameters));
    }

    /**
     * This returns the current version of the outlook plugin, primarily used to verfiy the user is using the most recent version of the plugin
     *
     * @param GetVersion $parameters
     * @return GetVersionResponse
     */
    public function GetVersion(GetVersion $parameters)
    {
      return $this->__soapCall('GetVersion', array($parameters));
    }

    /**
     * @param GetContactsV1 $parameters
     * @return GetContactsV1Response
     */
    public function GetContactsV1(GetContactsV1 $parameters)
    {
      return $this->__soapCall('GetContactsV1', array($parameters));
    }

    /**
     * @param GetMailingDetailV1 $parameters
     * @return GetMailingDetailV1Response
     */
    public function GetMailingDetailV1(GetMailingDetailV1 $parameters)
    {
      return $this->__soapCall('GetMailingDetailV1', array($parameters));
    }

    /**
     * @param GetContactsByEmailV1 $parameters
     * @return GetContactsByEmailV1Response
     */
    public function GetContactsByEmailV1(GetContactsByEmailV1 $parameters)
    {
      return $this->__soapCall('GetContactsByEmailV1', array($parameters));
    }

    /**
     * @param LogMailV1 $parameters
     * @return LogMailV1Response
     */
    public function LogMailV1(LogMailV1 $parameters)
    {
      return $this->__soapCall('LogMailV1', array($parameters));
    }

    /**
     * @param GetContactsExtended $parameters
     * @return GetContactsExtendedResponse
     */
    public function GetContactsExtended(GetContactsExtended $parameters)
    {
      return $this->__soapCall('GetContactsExtended', array($parameters));
    }

    /**
     * @param GetTabsAndLinks $parameters
     * @return GetTabsAndLinksResponse
     */
    public function GetTabsAndLinks(GetTabsAndLinks $parameters)
    {
      return $this->__soapCall('GetTabsAndLinks', array($parameters));
    }

    /**
     * @param GetTasks $parameters
     * @return GetTasksResponse
     */
    public function GetTasks(GetTasks $parameters)
    {
      return $this->__soapCall('GetTasks', array($parameters));
    }

    /**
     * @param GetAssignmentsWithoutDate $parameters
     * @return GetAssignmentsWithoutDateResponse
     */
    public function GetAssignmentsWithoutDate(GetAssignmentsWithoutDate $parameters)
    {
      return $this->__soapCall('GetAssignmentsWithoutDate', array($parameters));
    }

    /**
     * @param GetAssignments $parameters
     * @return GetAssignmentsResponse
     */
    public function GetAssignments(GetAssignments $parameters)
    {
      return $this->__soapCall('GetAssignments', array($parameters));
    }

    /**
     * @param UpdateAssignments $parameters
     * @return UpdateAssignmentsResponse
     */
    public function UpdateAssignments(UpdateAssignments $parameters)
    {
      return $this->__soapCall('UpdateAssignments', array($parameters));
    }

}
