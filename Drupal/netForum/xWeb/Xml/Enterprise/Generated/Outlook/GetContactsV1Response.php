<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook;

class GetContactsV1Response
{

    /**
     * @var GetContactsV1Result $GetContactsV1Result
     */
    protected $GetContactsV1Result = null;

    /**
     * @param GetContactsV1Result $GetContactsV1Result
     */
    public function __construct($GetContactsV1Result)
    {
      $this->GetContactsV1Result = $GetContactsV1Result;
    }

    /**
     * @return GetContactsV1Result
     */
    public function getGetContactsV1Result()
    {
      return $this->GetContactsV1Result;
    }

    /**
     * @param GetContactsV1Result $GetContactsV1Result
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\GetContactsV1Response
     */
    public function setGetContactsV1Result($GetContactsV1Result)
    {
      $this->GetContactsV1Result = $GetContactsV1Result;
      return $this;
    }

}
