<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook;

class AuthorizationToken
{

    /**
     * @var string $Token
     */
    protected $Token = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getToken()
    {
      return $this->Token;
    }

    /**
     * @param string $Token
     * @return \Drupal\netForum\xWeb\Xml\Enterprise\Generated\Outlook\AuthorizationToken
     */
    public function setToken($Token)
    {
      $this->Token = $Token;
      return $this;
    }

}
