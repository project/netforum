<?php

namespace Drupal\netForum\xWeb\Xml\Enterprise;

use Drupal\netForum\xWeb\netForumxWebApiConfig;
use Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\Authenticate;
use Drupal\netForum\xWeb\Xml\Enterprise\Generated\Secure\NetForumXML as GeneratedNetForumXML;


/**
 * Class NetForumXML
 *
 * @package Drupal\netForum\xWeb\Xml\Enterprise
 */
class NetForumXML extends GeneratedNetForumXML {

  private static $DoNotUseHeadersFor = array(
    'TestConnection',
    'GetDateTime',
    'Authenticate',
  );

  /**
   * Avectra XML Namespace for netFORUM Enterprise.
   */
  const AVECTRA_NS = 'http://www.avectra.com/2005/';

  public function __construct(netForumxWebApiConfig $netForumxWebApiConfig) {
    $this->netForumxWebApiConfig = $netForumxWebApiConfig;

    $this->SetXMLNamespace(self::AVECTRA_NS);

    $this->setDoNotUseHeaders(self::$DoNotUseHeadersFor);

    $this->setAuthenticationHandler(function () {
      $this->authenticationHandler();
    });

    parent::__construct(array(), NULL);

    $this->authenticationHandler();
  }

  protected function authenticationHandler() {
    $this->Authenticate(new Authenticate($this->netForumxWebApiConfig->getUsername(),
      $this->netForumxWebApiConfig->getPassword()));
  }
}
