<?php

namespace Drupal\netForum\xWeb;

/**
 * Class DrupalNetForumApiConfigTest
 *
 * Used for testing a configuration before it is saved.
 */
class DrupalNetForumApiConfigTest extends netForumxWebApiConfig {

  /**
   * @param array $formValues takes $form_state from a Drupal form.
   */
  public function __construct(array $formValues) {
    $this
      ->setApiEndpoint($formValues['netforum_wsdl_url'])
      ->setUsername($formValues['netforum_xweb_username'])
      ->setPassword($formValues['netforum_xweb_password'])
      ->setRequestTimeout($formValues['netforum_verify_timeout'])
      ->setEnableTCPKeepAlive($formValues['netforum_enable_httpkeepalve'])
      ->setEnableHTTPSCompression($formValues['netforum_enable_httpcompression'])
      ->setIgnoreInvalidCertificate($formValues['netforum_allow_invalidssl'])
      ->setEnableProxy($formValues['netforum_proxy_enabled'])
      ->setProxyHostname($formValues['netforum_proxy_host'])
      ->setProxyPort($formValues['netforum_proxy_port'])
      ->setProxyUsername($formValues['netforum_proxy_user'])
      ->setProxyPassword($formValues['netforum_proxy_pass']);

    if ($formValues['xweb_debug_log_all'] == 1) {
      $this->setDebugLogAllRequests(TRUE);
    }

    // set debugging

  }
}
