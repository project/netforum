<?php

namespace Drupal\netForum\xWeb;

use Drupal\netForum\xWeb\Logging\ILogHandler;

/**
 * Class netForumxWebApiConfig
 *
 * @package Drupal\netForum\xWeb
 */
abstract class netForumxWebApiConfig {

  /**
   * @var string
   */
  protected $apiEndpoint;

  /**
   * @var string
   */
  protected $username;

  /**
   * @var string
   */
  protected $password;

  /**
   * @var int
   */
  protected $requestTimeout;

  /**
   * @var bool
   */
  protected $enableTCPKeepAlive;

  /**
   * @var bool
   */
  protected $enableHTTPSCompression;

  /**
   * @var bool
   */
  protected $ignoreInvalidCertificate;

  /**
   * @var bool
   */
  protected $enableProxy;

  /**
   * @var string
   */
  protected $proxyHostname;

  /**
   * @var int
   */
  protected $proxyPort;

  /**
   * @var string
   */
  protected $proxyUsername;

  /**
   * @var string
   */
  protected $proxyPassword;

  /**
   * @var bool
   */
  protected $debugLogAllRequests;

  /**
   * @var ILogHandler
   */
  protected $debugLogHandler;

  /**
   * @return string
   */
  public function getApiEndpoint() {
    return $this->apiEndpoint;
  }

  /**
   * @param string $apiEndpoint
   *
   * @return netForumxWebApiConfig
   */
  public function setApiEndpoint($apiEndpoint) {
    $this->apiEndpoint = $apiEndpoint;
    return $this;
  }

  /**
   * @return string
   */
  public function getUsername() {
    return $this->username;
  }

  /**
   * @param string $username
   *
   * @return netForumxWebApiConfig
   */
  public function setUsername($username) {
    $this->username = $username;
    return $this;
  }

  /**
   * @return string
   */
  public function getPassword() {
    return $this->password;
  }

  /**
   * @param string $password
   *
   * @return netForumxWebApiConfig
   */
  public function setPassword($password) {
    $this->password = $password;
    return $this;
  }

  /**
   * @return int
   */
  public function getRequestTimeout() {
    return $this->requestTimeout;
  }

  /**
   * @param int $requestTimeout
   *
   * @return netForumxWebApiConfig
   */
  public function setRequestTimeout($requestTimeout) {
    $this->requestTimeout = $requestTimeout;
    return $this;
  }

  /**
   * @return bool
   */
  public function isEnableTCPKeepAlive() {
    return $this->enableTCPKeepAlive;
  }

  /**
   * @param bool $enableTCPKeepAlive
   *
   * @return netForumxWebApiConfig
   */
  public function setEnableTCPKeepAlive($enableTCPKeepAlive) {
    $this->enableTCPKeepAlive = $enableTCPKeepAlive;
    return $this;
  }

  /**
   * @return bool
   */
  public function isEnableHTTPSCompression() {
    return $this->enableHTTPSCompression;
  }

  /**
   * @param bool $enableHTTPSCompression
   *
   * @return netForumxWebApiConfig
   */
  public function setEnableHTTPSCompression($enableHTTPSCompression) {
    $this->enableHTTPSCompression = $enableHTTPSCompression;
    return $this;
  }

  /**
   * @return bool
   */
  public function isIgnoreInvalidCertificate() {
    return $this->ignoreInvalidCertificate;
  }

  /**
   * @param bool $ignoreInvalidCertificate
   *
   * @return netForumxWebApiConfig
   */
  public function setIgnoreInvalidCertificate($ignoreInvalidCertificate) {
    $this->ignoreInvalidCertificate = $ignoreInvalidCertificate;
    return $this;
  }

  /**
   * @return bool
   */
  public function isEnableProxy() {
    return $this->enableProxy;
  }

  /**
   * @param bool $enableProxy
   *
   * @return netForumxWebApiConfig
   */
  public function setEnableProxy($enableProxy) {
    $this->enableProxy = $enableProxy;
    return $this;
  }

  /**
   * @return string
   */
  public function getProxyHostname() {
    return $this->proxyHostname;
  }

  /**
   * @param string $proxyHostname
   *
   * @return netForumxWebApiConfig
   */
  public function setProxyHostname($proxyHostname) {
    $this->proxyHostname = $proxyHostname;
    return $this;
  }

  /**
   * @return int
   */
  public function getProxyPort() {
    return $this->proxyPort;
  }

  /**
   * @param int $proxyPort
   *
   * @return netForumxWebApiConfig
   */
  public function setProxyPort($proxyPort) {
    $this->proxyPort = $proxyPort;
    return $this;
  }

  /**
   * @return string
   */
  public function getProxyUsername() {
    return $this->proxyUsername;
  }

  /**
   * @param string $proxyUsername
   *
   * @return netForumxWebApiConfig
   */
  public function setProxyUsername($proxyUsername) {
    $this->proxyUsername = $proxyUsername;
    return $this;
  }

  /**
   * @return string
   */
  public function getProxyPassword() {
    return $this->proxyPassword;
  }

  /**
   * @param string $proxyPassword
   *
   * @return netForumxWebApiConfig
   */
  public function setProxyPassword($proxyPassword) {
    $this->proxyPassword = $proxyPassword;
    return $this;
  }

  /**
   * @return bool
   */
  public function isDebugLogAllRequests() {
    return $this->debugLogAllRequests;
  }

  /**
   * @param bool $debugLogAllRequests
   *
   * @return netForumxWebApiConfig
   */
  public function setDebugLogAllRequests($debugLogAllRequests) {
    $this->debugLogAllRequests = $debugLogAllRequests;
    return $this;
  }

  /**
   * @return \Drupal\netForum\xWeb\Logging\ILogHandler
   */
  public function getDebugLogHandler() {
    return $this->debugLogHandler;
  }

  /**
   * @param \Drupal\netForum\xWeb\Logging\ILogHandler $debugLogHandler
   *
   * @return netForumxWebApiConfig
   */
  public function setDebugLogHandler($debugLogHandler) {
    $this->debugLogHandler = $debugLogHandler;
    return $this;
  }
}
