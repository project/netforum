<?php

namespace Drupal\netForum\xWeb;

use Drupal\netForum\xWeb\Exceptions\netForumxWebException;


/**
 * Class DrupalNetForumXMLApiConfig
 *
 * Retrieve settings set through the module configuration page in Drupal.
 */
class DrupalNetForumApiConfig extends netForumxWebApiConfig {

  public function __construct() {
    $this
      ->setApiEndpoint(variable_get('netforum_wsdl_url', ''))
      ->setUsername(variable_get('netforum_xweb_username', ''))
      ->setPassword(variable_get('netforum_xweb_password', ''))
      ->setRequestTimeout(variable_get('netforum_verify_timeout', ''))
      ->setEnableTCPKeepAlive(variable_get('netforum_enable_httpkeepalve', ''))
      ->setEnableHTTPSCompression(variable_get('netforum_enable_httpcompression', ''))
      ->setIgnoreInvalidCertificate(variable_get('netforum_allow_invalidssl', ''))
      ->setEnableProxy(variable_get('netforum_proxy_enabled', ''))
      ->setProxyHostname(variable_get('netforum_proxy_host', ''))
      ->setProxyPort(variable_get('netforum_proxy_port', ''))
      ->setProxyUsername(variable_get('netforum_proxy_user', ''))
      ->setProxyPassword(variable_get('netforum_proxy_pass', ''));

    // At a minimum, we need a xWeb WSDL URL, a xWeb Username, a xWeb Password
    //and the netforum_xweb_od variable to be set to consider the module configured.

    if (empty($this->getApiEndpoint() ||
      empty($this->getUsername()) ||
      empty($this->getPassword()))
    ) {
      throw new netForumxWebException('The netForum xWeb module needs to be 
      configured before it can be used. Please visit admin/config/netforum/connection 
      to configure the module.');
    }
  }
}

//todo: figure out logging
//todo: add logging to netForumxWebException
//todo: fix object refreshing issue
